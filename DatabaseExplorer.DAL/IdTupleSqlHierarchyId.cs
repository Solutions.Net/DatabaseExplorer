﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.SqlServer.Types;

using TechnicalTools;
using TechnicalTools.Model.Cache;


namespace DatabaseExplorer.DAL
{
    [DebuggerDisplay("{AsDebugString}")]
    public struct IdTupleSqlHierarchyId : IIdTuple, IEquatable<IdTupleSqlHierarchyId>
    {
        public IdTupleSqlHierarchyId(SqlHierarchyId hid)
        {
            _Id1 = hid;
        }

        public SqlHierarchyId Id { get { return _Id1; } } readonly SqlHierarchyId   _Id1;

        public object[] Keys { get { return new object[] { _Id1 }; } }
        public ITypedId ToTypedId(Type type) { return new TypedId<IdTupleSqlHierarchyId>(type, this); }
        private string AsDebugString { get { return "[" + Keys.Join() + "]"; } }

        #region Equality members

        public bool Equals(IdTupleSqlHierarchyId other)
        {
            return EqualityComparer<SqlHierarchyId>.Default.Equals(_Id1, other._Id1);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            return obj is IdTupleSqlHierarchyId && Equals((IdTupleSqlHierarchyId)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return EqualityComparer<SqlHierarchyId>.Default.GetHashCode(_Id1);
            }
        }

        public static bool operator ==(IdTupleSqlHierarchyId left, IdTupleSqlHierarchyId right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdTupleSqlHierarchyId left, IdTupleSqlHierarchyId right)
        {
            return !left.Equals(right);
        }
        #endregion
    }

}
