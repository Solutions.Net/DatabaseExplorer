﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.SqlServer.Management.Smo;


namespace DatabaseExplorer.DAL
{
    public partial class DatabaseInfo
    {
        public SqlConnectionStringBuilder Connection { get; set; }
               Database                   Db         { get; }

        public IReadOnlyCollection<Schema>              Schemas                 { get { return Db.Schemas.Cast<Schema>().ToList(); } }
        public IReadOnlyCollection<Table>               Tables                  { get { return Db.Tables.Cast<Table>().ToList(); } }
        public IReadOnlyCollection<View>                Views                   { get { return Db.Views.Cast<View>().ToList(); } }
        public IReadOnlyCollection<StoredProcedure>     StoredProcedures        { get { return Db.StoredProcedures.Cast<StoredProcedure>().ToList(); } }
        public IReadOnlyCollection<UserDefinedFunction> UserDefinedFunctions    { get { return Db.StoredProcedures.Cast<UserDefinedFunction>().ToList(); } }
        

        public string SqlServerVersion
        {
            get
            {
                return Db.GetSqlServerVersionName();
            }
        }

        public DatabaseInfo(Database db, SqlConnectionStringBuilder connection)
        {
            Connection = connection;
            Db = db;
        }

        public class Dependency
        {
            public ScriptSchemaObjectBase DependencyObject { get; set; }
            public ScriptSchemaObjectBase DependentObject { get; set; }
        }
        public List<Dependency> GetAllDependencies()
        {
            return null;
        }

        //public List<Constraint> AllConstraints
        //{
        //    get
        //    {
        //        return Schemas.SelectMany(schema => 
        //            schema.PrimaryKeyConstraints.Cast<Constraint>()
        //                                        .Concat(schema.ForeignKeyConstraints)
        //                                        .Concat(schema.DefaultConstraints)
        //                                        .Concat(schema.UniqueConstraints)
        //                                        .Concat(schema.CheckConstraints))

        //                      .ToList();
        //    }
        //}
        //public List<ConstraintFK> ForeignKeyConstraints
        //{
        //    get
        //    {
        //        return Schemas.SelectMany(schema => schema.ForeignKeyConstraints)
        //                      .ToList();
        //    }
        //}

        //public void ImportFromDatabase()
        //{
        //    List <DbObject> all_objects = null;
        //    Connection.Open();
        //    try
        //    {
        //        SqlServerVersion = SQLProcedureModelManager.GetSqlServerVersion(Connection);
        //        all_objects = SQLProcedureModelManager.GetDbObjects(Connection, with_source: true);
        //    }
        //    finally
        //    {
        //        Connection.Close();
        //    }

        //    foreach (var lstInfo in all_objects.GroupBy(obj => obj.Schema))
        //    {
        //        var schema = new Schema(null)
        //        {
        //            Name = lstInfo.Key,
        //            Project = this,
        //        };
        //        schema.ImportFromDatabase(lstInfo);
        //        Schemas.Add(schema);
        //    }

        //}
    }



    public enum eSqlDbType
    {
        SqlServer = 0,
        SqlServerCompactEdition = 10
    }
}