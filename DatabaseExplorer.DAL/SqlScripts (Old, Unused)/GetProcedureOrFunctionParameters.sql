﻿/* Utiliser ce modèle pour récupérer les arguments d'une fonction our procedure.
 * Particulierement utile pour les fonction/procedure d'une CLR dont les arguments
 * ne sont pas accessibles via System.Data.SqlClient.SqlCommandBuilder.DeriveParameters (framework dotNet)
 * Remplace dans ce texte 
 *    $1 [optionnel] Trie sur le nom de la fonction/procedure dont on veut recuperer les arguments
 *                   Si cet argument est vide, tous les paramètres de toutes les procédures/fonctions sont retournés.
 *
*/
SELECT 	R.specific_name as PROCEDURE_NAME, 
		R.routine_type,             -- Peut contenir PROCEDURE ou FUNCTION (et peut être d'autre valeurs... encore jamais rencontre)
		P.ORDINAL_POSITION,         -- Position de l'argument
		P.PARAMETER_NAME,           -- Nom de l'argument prefixé par  '@'
		P.DATA_TYPE,                -- Contient "int", "numeric", "varchar", etc...
		P.CHARACTER_MAXIMUM_LENGTH, -- Contient 255 si le type reel est "varchar(255)"
		P.NUMERIC_PRECISION,        -- Partie entière. Ex: contient 12 si le type est numeric(12,8)
		P.NUMERIC_SCALE,            -- Partie décimale. Ex:  contient  8 si le type est numeric(12,8)
		P.DATETIME_PRECISION        -- Ex: contient 3 si le type est datetime2(3)
		--,P.*
FROM 			INFORMATION_SCHEMA.ROUTINES  as R
LEFT OUTER JOIN	INFORMATION_SCHEMA.PARAMETERS as P ON R.specific_name = P.specific_name 
WHERE 	'$1' in ('', R.specific_name)
AND PARAMETER_NAME is not null -- Les procedures sans argument ont quand même une ligne de retournée (aucune idée sur la raison)
order by PROCEDURE_NAME ASC,
		 ORDINAL_POSITION ASC

