﻿/* Utiliser ce modèle pour récupérer les tables dans l'ordre dans lequel il faut faire les delete 
 * afin de garantir qu'aucune contrainte de clef étrangère ne sera brisé
 *
*/
with Fkeys as (	select distinct
					OnTable       = OnTable.name,
					AgainstTable  = AgainstTable.name 
				from       sysforeignkeys fk
				inner join sysobjects onTable       on onTable.id = fk.fkeyid
				inner join sysobjects againstTable  on againstTable.id = fk.rkeyid
				where AgainstTable.TYPE = 'U'
				  AND OnTable.TYPE = 'U'
				-- ignore self joins; they cause an infinite recursion
				  and OnTable.Name <> AgainstTable.Name
			  ),
	 MyData as(	select 
					OnTable = o.name,
					AgainstTable = FKeys.againstTable
				from sys.objects o
				left join FKeys on  o.name = FKeys.onTable
				where o.type = 'U'
				  and o.name not like 'sys%'
			  ),
	MyRecursion as	(-- base case
						select 
							TableName  = OnTable,
							Lvl        = 1
						from MyData
						where AgainstTable is null
					-- recursive case
					union all 
						select
							TableName  = OnTable,
							Lvl        = r.Lvl + 1
						from		MyData      d
						inner join	MyRecursion r on r.TableName = d.AgainstTable
					)
SELECT
	Lvl = max(Lvl),
	TableName
FROM 
    MyRecursion
GROUP BY
    TableName
ORDER BY Lvl desc,
		 TableName desc
