﻿/* Utiliser ce modèle pour récupérer la plupart des objets et leur définition.
   Il faut remplacer :
      $1 Indique si on doit récupèrer les sources ou non
      $2 Permet de filtrer les type d'objet , valeurs possibles (voir le type DbObjectType) :
        TABLE,
        USER_TABLE,
        VIEW,
        SQL_STORED_PROCEDURE,
        SQL_INLINE_TABLE_VALUED_FUNCTION,
        SQL_SCALAR_FUNCTION,
        SQL_TABLE_VALUED_FUNCTION,
        SQL_TRIGGER,
        PRIMARY_KEY_CONSTRAINT,
        FOREIGN_KEY_CONSTRAINT,
        DEFAULT_CONSTRAINT,
        UNIQUE_CONSTRAINT,
        CHECK_CONSTRAINT,
        SERVICE_QUEUE,
        INTERNAL_TABLE,
        SYSTEM_TABLE,
        CLR_TABLE_VALUED_FUNCTION,
        CLR_SCALAR_FUNCTION,
        CLR_STORED_PROCEDURE
*/
declare @tabs varchar(100); 
set @tabs = char(9)+char(9)+char(9)+char(9)+char(9);
set @tabs = @tabs+@tabs+@tabs+@tabs+@tabs; -- Si pas assez long, un message d'erreur apparait concernant la fonction left

	WITH ForeignKeyInfos AS 

	(
		SELECT 
		   f.name													  AS ConstraintName,
		   OBJECT_NAME (f.referenced_object_id)						  AS PrimaryTableName,
		   COL_NAME(fc.referenced_object_id, fc.referenced_column_id) AS PrimaryColumnName,
		   COL_NAME(fc.parent_object_id, fc.parent_column_id)		  AS ForeignColumnName,
		   OBJECT_NAME(f.parent_object_id)							  AS ForeignTableName

		FROM 
		   sys.foreign_keys AS f
		INNER JOIN 
		   sys.foreign_key_columns AS fc 
			  ON f.OBJECT_ID = fc.constraint_object_id
		INNER JOIN 
		   sys.tables t 
			  ON t.OBJECT_ID = fc.referenced_object_id
		WHERE 
		   f.type_desc = 'FOREIGN_KEY_CONSTRAINT' -- sans doute inutile mais je n'en suis pas sûr
	), ForeignKeySources AS 

	(
		SELECT 
		   ConstraintName,
		   PrimaryTableName,
		   PrimaryColumnName,
		   ForeignColumnName,
		   ForeignTableName,
		   'ALTER TABLE [' + ForeignTableName + ']  WITH CHECK ' + 
		   'ADD  CONSTRAINT [' + ConstraintName + '] FOREIGN KEY([' + ForeignColumnName + '])' +
		   'REFERENCES [' + PrimaryTableName + '] ([' + PrimaryColumnName + '])' AS Source
		from ForeignKeyInfos
	)
	-- SET STATISTICS Time ON ' Est ce vraiment utile ?
	select	so.name			           as 'Name',
			so.type_desc		       as 'ObjectType',
			SCHEMA_NAME(so.schema_id)  as 'SchemaName',
			so.create_date		       as 'DateOfCreation',
			so.modify_date		       as 'DateOfChange', 
			CASE WHEN so.type_desc = 'FOREIGN_KEY_CONSTRAINT' THEN FKS.Source
			     ELSE object_definition(so.object_id) END as 'Source'
	from sys.objects as so
	LEFT OUTER JOIN ForeignKeySources AS FKS ON FKS.ConstraintName = name
	where  '$2' in ('', so.type_desc) 
	  and SCHEMA_NAME(so.schema_id) <> 'sys' -- we dont use is_ms_shipped = 0 because it remove SERVICE_QUEUE defined by user
	  and so.type_desc <> 'USER_TABLE' -- because in union below
union -- Les tables sont gérées autrement

	SELECT so.name         as 'Name',
		   'USER_TABLE'    as 'ObjectType',
		   tc.TABLE_SCHEMA as 'SchemaName',
		   so.create_date  as 'DateOfCreation',
		   so.modify_date  as 'DateOfChange',
		   case when $1 != 0 then 'CREATE TABLE [' + tc.TABLE_SCHEMA + '].[' + so.name + ']'+ char(10) 
		                        + '(' + char(10) 
								+     left(o.list, len(o.list)-2) + char(10) 
								+ ')' +
								  CASE WHEN tc.CONSTRAINT_NAME IS NULL 
									   THEN '' 
									   ELSE 'ALTER TABLE ' + so.name + ' ADD CONSTRAINT ' + tc.CONSTRAINT_NAME + ' PRIMARY KEY ' + ' (' + LEFT(j.list, Len(j.list)-1) + ')'
								  END  
							 else '' end 
				           as 'Source'
	FROM   sys.objects so
	LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS as tc ON tc.TABLE_NAME = so.name
													    AND tc.CONSTRAINT_TYPE = 'PRIMARY KEY'
	CROSS apply (
	
	SELECT '  [' + COLUMN_NAME + '] '
						+ left(@tabs, len(@tabs) - (len('  [' + isnull(COLUMN_NAME,'') + '] ') + 1) / 4) + DATA_TYPE 
						+ CASE DATA_TYPE WHEN 'sql_variant' THEN '' 
										 WHEN 'text' THEN '' 
										 WHEN 'decimal' THEN '(' + Cast(NUMERIC_PRECISION_RADIX AS VARCHAR) + ', ' 
																 + Cast(NUMERIC_SCALE AS VARCHAR) 
																 + ')'
										 ELSE COALESCE('(' + CASE WHEN character_maximum_length = -1 THEN 'MAX'
																  ELSE Cast(character_maximum_length AS VARCHAR) END +')', '')
						  END 
						+ char(9) + ( CASE WHEN IS_NULLABLE = 'NO' THEN 'NOT ' ELSE '' END) + 'NULL' 
						+ CASE WHEN EXISTS(SELECT id 
										   FROM syscolumns 
										   WHERE Object_name(id) = so.name 
											 AND name = COLUMN_NAME 
											 AND Columnproperty(id, name, 'IsIdentity') = 1)
									 THEN char(9) + 'IDENTITY(' + Cast(Ident_seed(tc.TABLE_SCHEMA + '.' + so.name) AS VARCHAR) 
												  + ',' + Cast(Ident_incr(tc.TABLE_SCHEMA + '.' + so.name) AS VARCHAR) 
												  + ')' 
									 ELSE '' END
					   + CASE WHEN INFORMATION_SCHEMA.COLUMNS.COLUMN_DEFAULT IS NOT NULL 
							  THEN char(9) + 'DEFAULT ' + INFORMATION_SCHEMA.COLUMNS.COLUMN_DEFAULT
							  ELSE ''
						 END + ',' + char(10)
				FROM   INFORMATION_SCHEMA.COLUMNS
				WHERE  TABLE_NAME = so.name
				  AND  TABLE_SCHEMA = SCHEMA_NAME(so.schema_id)
				ORDER  BY ORDINAL_POSITION
				FOR xml path('')) as o (list)
	CROSS apply (SELECT '[' + COLUMN_NAME + '], '
				 FROM   INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
				 WHERE  kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME
				 AND  kcu.TABLE_SCHEMA = SCHEMA_NAME(so.schema_id)
				 ORDER  BY ORDINAL_POSITION
				 FOR xml path('')) as j (list)
	WHERE type = 'U'
	  AND name NOT IN ( 'dtproperties' ) 
	  AND '$2' in ('', 'USER_TABLE')




--SELECT name 
--FROM master..spt_values
--WHERE type = 'O9T'
--Output

--AF: aggregate function
--AP: application
--C : check cns
--D : default (maybe cns)
--EN: event notification
--F : foreign key cns
--FN: scalar function
--FS: assembly scalar function
--FT: assembly table function
--IF: inline function
--IS: inline scalar function
--IT: internal table
--L : log
--P : stored procedure
--PC : assembly stored procedure
--PK: primary key cns
--R : rule
--RF: replication filter proc
--S : system table
--SN: synonym
--SQ: queue
--TA: assembly trigger
--TF: table function
--TR: trigger
--U : user table
--UQ: unique key cns
--V : view
--X : extended stored proc
--sysobjects.type, reports