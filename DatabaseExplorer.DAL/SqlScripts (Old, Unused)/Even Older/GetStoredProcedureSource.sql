﻿/* Utiliser ce modele pour recuperer la source SQL d'une procédure stockée.
   Il faut remplacer :
      $1 par le nom du schéma de la base (en général "dbo")
      $2 par le nom de la procédure pour laquelle on veut récupérer la source SQL.
      
   Note : Cette requête provient de SQL server Management Studio 2008 R2 et a été capturé via SQL Profiler.
*/
SELECT
    ISNULL(smsp.definition, ssmsp.definition) as [Definition]
FROM            sys.all_objects        as sp
LEFT OUTER JOIN sys.sql_modules        as smsp ON smsp.object_id = sp.object_id
LEFT OUTER JOIN sys.system_sql_modules as ssmsp ON ssmsp.object_id = sp.object_id
WHERE sp.name=N'$2' 
  and SCHEMA_NAME(sp.schema_id) = N'$1'
  and (sp.type = N'P' OR sp.type = N'RF' OR sp.type='PC')