﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;


namespace DatabaseExplorer.DAL.Old
{
    public static class SQLProcedureModelManager_Old
    {
        private static string LoadModelAndSetArguments(string filename, params string[] replacements)
        {
            string content;
            // Recupère le contenu du fichier et le nettoie
            if (_models_cache.ContainsKey(filename))
                content = _models_cache[filename];
            else
            {
                content = File.ReadAllText(@"SqlScripts\" + filename);
                if (content.StartsWith("/*"))
                    content = content.Substring(content.IndexOf("*/") + 2);
                _models_cache.Add(filename, content);
            }

            // Remplace les arguments
            if (replacements != null)
                for (int r = 1; r <= replacements.Length; ++r)
                    content = content.Replace("$" + r.ToString(), replacements[r].Replace("'", "''"));

            return content;
        }
        static Dictionary<string /* model filename */, string /* file content with no header */> _models_cache = new Dictionary<string, string>();

        #region Tables
        public class GetAllTables_Result
        {
            public string Name;
            public string Schema;

            public string Source;

            public static GetAllTables_Result FromDataRow(DataRow row)
            {
                var result = new GetAllTables_Result();

                result.Name = (string)row["Name"];
                result.Schema = (string)row["Schema"];

                return result;
            }
        }

        /// <summary>
        /// Crée une commande SQL permettant de récupérer toutes les tables d'une base.
        /// </summary>
        /// <param name="connection">Connection pour initialiser la commande SQL</param>
        /// <param name="base_name">Nom de la base dans laquelle chercher les tables</param>
        /// <returns>Une commande prete à être executée</returns>
        public static List<GetAllTables_Result> GetAllTables(ConnectionSetting connection, string base_name)
        {
            throw new NotImplementedException("Modifier le code ci dessous pour implementer cette fonctionalité !");
            //System.Diagnostics.Contracts.Contract.Assert(connection != null);
            //System.Diagnostics.Contracts.Contract.Assert(!string.IsNullOrWhiteSpace(base_name));

            //SqlCommand cmd = connection.Connection.CreateCommand();
            //cmd.CommandType = System.Data.CommandType.Text;
            //cmd.CommandText = LoadModelAndSetArguments("GetAllStoredProcedures.sql", base_name);

            //return connection.ExecuteAndConvertRows(cmd, row => GetAllTables_Result.FromDataRow(row));
        }



        /// <summary>
        /// Crée une commande SQL permettant de récupérer la source SQL d'une table.
        /// </summary>
        /// <param name="connection">Connection pour initialiser la commande SQL</param>
        /// <param name="table_name">Nom de la table à récupérer</param>
        /// <returns></returns>
        public static string GetTableSource(ConnectionSetting connection, string schema_name, string table_name)
        {
            throw new NotImplementedException("Modifier le code ci dessous pour implementer cette fonctionalité !");
            //System.Diagnostics.Contracts.Contract.Assert(connection != null);
            //System.Diagnostics.Contracts.Contract.Assert(!string.IsNullOrWhiteSpace(table_name));

            //SqlCommand cmd = connection.Connection.CreateCommand();
            //cmd.CommandType = System.Data.CommandType.Text;
            //cmd.CommandText = LoadModelAndSetArguments("GetStoredProcedureSource.sql", schema_name, table_name);

            //var source = connection.ExecuteAndConvertRows(cmd, row => (string)row["Definition"]);
            //System.Diagnostics.Debug.Assert(source.Count == 1);
            //return source[0];
        }
        #endregion Tables

        #region Procedures stockées
        public class GetAllStoredProcedures_Result
        {
            public string Name;
            public string Schema;
            public string Urn;
            public int PolicyHealthState;
            public DateTime CreateDate;
            public string Owner;
            public bool IsEncrypted;
            public int ImplementationType;

            public string Source;

            public static GetAllStoredProcedures_Result FromDataRow(DataRow row)
            {
                var result = new GetAllStoredProcedures_Result();

                result.Name = (string)row["Name"];
                result.Schema = (string)row["Schema"];
                result.Urn = (string)row["Urn"];
                result.PolicyHealthState = (int)row["PolicyHealthState"];
                result.CreateDate = (DateTime)row["CreateDate"];
                result.Owner = (string)row["Owner"];
                result.IsEncrypted = (bool)row["IsEncrypted"];
                result.ImplementationType = (int)row["ImplementationType"];

                return result;
            }
        }

        /// <summary>
        /// Crée une commande SQL permettant de récupérer toutes les procèdures stockées d'une base.
        /// </summary>
        /// <param name="connection">Connection pour initialiser la commande SQL</param>
        /// <param name="base_name">Nom de la base dans laquelle chercher les procédures</param>
        /// <returns>Une commande prete à être executée</returns>
        public static List<GetAllStoredProcedures_Result> GetAllStoredProcedures(ConnectionSetting connection, string base_name)
        {
            System.Diagnostics.Contracts.Contract.Assert(connection != null);
            System.Diagnostics.Contracts.Contract.Assert(!string.IsNullOrWhiteSpace(base_name));

            SqlCommand cmd = connection.Connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = LoadModelAndSetArguments("GetAllStoredProcedures.sql", base_name);

            return connection.ExecuteAndConvertRows(cmd, row => GetAllStoredProcedures_Result.FromDataRow(row));
        }



        /// <summary>
        /// Crée une commande SQL permettant de récupérer la source SQL d'une procédure stockée.
        /// </summary>
        /// <param name="connection">Connection pour initialiser la commande SQL</param>
        /// <param name="procedure_name">Nom de la procédure à récupérer</param>
        /// <returns></returns>
        public static string GetStoredProcedureSource(ConnectionSetting connection, string schema_name, string procedure_name)
        {
            System.Diagnostics.Contracts.Contract.Assert(connection != null);
            System.Diagnostics.Contracts.Contract.Assert(!string.IsNullOrWhiteSpace(procedure_name));

            SqlCommand cmd = connection.Connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = LoadModelAndSetArguments("GetStoredProcedureSource.sql", schema_name, procedure_name);

            var source = connection.ExecuteAndConvertRows(cmd, row => (string)row["Definition"]);
            System.Diagnostics.Debug.Assert(source.Count == 1);
            return source[0];
        }
        #endregion Procedures stockées

    }
}
