﻿/* Utiliser ce modele pour recuperer les noms de toutes les procédures stockées.
   Il faut remplacer :
      $1 par le nom de la base / catalogue
      
   Retourne :
     Colonne 1 "Name"               : Nom d'une procédure stockée.
     Colonne 2 "Schema"             : Schéma de la base
     Colonne 3 "Urn"                : ???
     Colonne 4 "PolicyHealthState"  : ???
     Colonne 5 "CreateDate"         : Date de création de la procédure stockée (et/ou date de last update ?)
     Colonne 6 "Owner"              : ???
     Colonne 7 "IsEncrypted"        : ???
     Colonne 8 "ImplementationType" : ???
   Note : Cette requête provient de SQL server Management Studio 2008 R2 et a été capturé via SQL Profiler.
*/
DECLARE @is_policy_automation_enabled bit;
    SET @is_policy_automation_enabled  = (SELECT CONVERT(bit, current_value)
                                          FROM msdb.dbo.syspolicy_configuration
                                          WHERE name = 'Enabled')
      
SELECT
    sp.name as [Name], 
    SCHEMA_NAME(sp.schema_id) as [Schema],
    'Server[@Name=' + quotename(CAST(serverproperty(N'Servername') as sysname),'''') + ']'
    + '/Database[@Name=' + quotename(db_name(),'''') + ']' 
    + '/StoredProcedure[@Name=' + quotename(sp.name,'''') 
    + ' and @Schema=' + quotename(SCHEMA_NAME(sp.schema_id),'''') + ']' as [Urn],
    case when 1 = @is_policy_automation_enabled 
          and exists (select * 
                      from msdb.dbo.syspolicy_system_health_state 
                      where target_query_expression_with_id like 'Server' + '/Database\[@ID=' + convert(nvarchar(20),dtb.database_id) + '\]'+ '/StoredProcedure\[@ID=' + convert(nvarchar(20),sp.object_id) + '\]%' ESCAPE '\') 
        then 1 else 0 end as [PolicyHealthState],
    sp.create_date        as [CreateDate],
    ISNULL(ssp.name, N'') as [Owner],
    CAST(CASE WHEN ISNULL(smsp.definition, ssmsp.definition) IS NULL then 1 else 0 end as bit) as [IsEncrypted],
    CASE WHEN sp.type = N'P'  then 1 
         WHEN sp.type = N'PC' then 2 
                              else 1 END as [ImplementationType]
FROM            master.sys.databases    as dtb,
                sys.all_objects         as sp
LEFT OUTER JOIN sys.database_principals as ssp   ON ssp.principal_id = ISNULL(sp.principal_id, (OBJECTPROPERTY(sp.object_id, 'OwnerId')))
LEFT OUTER JOIN sys.sql_modules         as smsp  ON smsp.object_id = sp.object_id
LEFT OUTER JOIN sys.system_sql_modules  as ssmsp ON ssmsp.object_id = sp.object_id
WHERE     (sp.type = N'P' OR sp.type = N'RF' OR sp.type=N'PC')
      and (CAST(case when sp.is_ms_shipped = 1 then 1
                     when (select major_id 
                           from sys.extended_properties 
                           where major_id = sp.object_id
                             and minor_id = 0
                             and class = 1
                             and name = N'microsoft_database_tools_support') 
                          is not null then 1
                                      else 0 end as bit)=N'0')
      and db_name() = N'$1'
      and db_name() = dtb.name
ORDER BY [Schema] ASC, [Name] ASC;
