﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;


namespace DatabaseExplorer.DAL.Old
{
    
    public class ConnectionSetting
    {
        public const int DefaultSqlPort = 1433;

        #region Properties

        public string Name           { get; set; }
        public bool   HidePassword   { get; set; }
        public bool   IsValid        { get { return !string.IsNullOrEmpty(Server) &&
                                                    !string.IsNullOrEmpty(InitialCatalog) &&
                                                    (UseWindowAuthentication || !string.IsNullOrEmpty(UserId) && !string.IsNullOrEmpty(Password));
                                           } }

        public string Server         { get; set; }
        public int    Port           { get; set; }
        public string InitialCatalog { get; set; }
        public string UserId         { get; set; }
        public string Password       { get; set; }
        public bool   UseWindowAuthentication { get; set; }
        
        public string ConnectionString
        {
            get 
            { 
                return ValueToConnectionStringPropertyValue("Data Source", Server)
                    // Note pour plus tard : quand on definit un named pipe ("/truc" derriere le nom du server), 
                    // on n'a pas besoin de specifier de port... c'est la meme chose !
                    + (Port != DefaultSqlPort ? ValueToConnectionStringPropertyValue("Port", Port.ToString()) : "")
                     + ValueToConnectionStringPropertyValue("Initial Catalog", InitialCatalog)
                     + ValueToConnectionStringPropertyValue("User id", UserId)
                     + ValueToConnectionStringPropertyValue("Password", Password, true)
                     + (UseWindowAuthentication ? ValueToConnectionStringPropertyValue("Integrated Security", "True") : "");
            }
            set 
            {
                int port;
                // TODO : voir  SqlConnectionStringBuilder (Pour les synonyme)
                Server = ExtractPropertyValueFromConnectionString(value, "Data Source"); // Synonyme : "Server" !
                var m = Regex.Match(Server, "(?<server>^.+),(?<port>[0-9]+)$");
                if (m.Success)
                {
                    Server = m.Groups["server"].Value;
                    Port = int.Parse(m.Groups["port"].Value);
                }
                else
                    Port = int.TryParse(ExtractPropertyValueFromConnectionString(value, "Port"), out port) ? port : DefaultSqlPort;
                InitialCatalog = ExtractPropertyValueFromConnectionString(value, "Initial Catalog"); // Synonyme : "Database" !
                UserId         = ExtractPropertyValueFromConnectionString(value, "User id");
                Password       = ExtractPropertyValueFromConnectionString(value, "Password");
                bool v;
                bool.TryParse(ExtractPropertyValueFromConnectionString(value, "Integrated Security", "False"), out v);  // Synonyme : "Trusted_Connection" !
                UseWindowAuthentication = v;
            } 
        }

        private string ValueToConnectionStringPropertyValue(string prop_name, string value, bool is_a_password = false)
        {
            if (string.IsNullOrWhiteSpace(value))
                return "";
            if (is_a_password && HidePassword)
                value = new String('*', value.Length);

            // Ne gère pas le cas (tres improbable) ou un ; et un " sont tous les deux compris dans la valeur
            return prop_name.Trim() + "=" 
                 + (value.Contains(";") ? "\"" + value.Trim() + "\"" : value.Trim())
                 + ";";
        }

        private static string ExtractPropertyValueFromConnectionString(string con_string, string prop_name, string default_value = "")
        {
            // Ne gère pas le cas (tres improbable) ou un ; et un " sont tous les deux compris dans la valeur
            Match m = Regex.Match(con_string, prop_name + "=(\"(?<value>[^\"]*)\"|(?<value>[^;]*))", RegexOptions.IgnoreCase);
            return m.Success ? m.Groups["value"].Value : default_value;
        }

        #endregion Properties

        public ConnectionSetting()
        {
            Port = DefaultSqlPort;
        }

        #region Implement XmlDatabase.TableElement<ConnectionSetting>

        public void ImportElement(XElement root)
        {
            Name = root.Attribute("Name").Value;
            ConnectionString = root.Attribute("ConnectionString").Value;
        }

        public void ExportElement(XElement root)
        {
            root.Add(new XAttribute("Name", Name ?? ""));
            root.Add(new XAttribute("ConnectionString", ConnectionString ?? ""));
        }
        #endregion Implement XmlDatabase.TableElement<ConnectionSetting>

        #region Real Connection Object management
        public SqlConnection Connection { get; private set; }

        public void Open()
        {
            if (Connection != null)
                Close();
            if (Connection == null)
                Connection = new SqlConnection(ConnectionString);
            Connection.Open();
        }

        public void Close()
        {
            if (Connection != null && Connection.State == ConnectionState.Open)
                Connection.Close();
            Connection = null;
        }

        public DataSet ExecuteAndGetDataset(SqlCommand cmd)
        {
            using (var da = new SqlDataAdapter(cmd))
            {
                // Fill the DataSet using default values for DataTable names, etc
                var dataset = new DataSet();
                da.Fill(dataset);

                return dataset;
            }
        }

        public List<T> ExecuteAndConvertRows<T>(SqlCommand cmd, Func<DataRow, T> convert)
        {
            DataSet ds = ExecuteAndGetDataset(cmd);
            System.Diagnostics.Contracts.Contract.Assert(ds.Tables.Count <= 1);
            if (ds.Tables.Count == 0)
                return new List<T>();
            return ds.Tables[0].AsEnumerable().Select(row => convert(row)).ToList();
        }
        #endregion Real Connection Object management
    }
}