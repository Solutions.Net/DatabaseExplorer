﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;


namespace DatabaseExplorer.DAL.Old
{
    public static class SQLProcedureModelManager
    {
        #region Generic tool & static data
        private static string LoadModelAndSetArguments(string filename, params string[] replacements)
        {
            string content;
            // Recupère le contenu du fichier et le nettoie
            if (_models_cache.ContainsKey(filename))
                content = _models_cache[filename];
            else
            {
                content = File.ReadAllText(@"SqlScripts\" + filename);
                if (content.StartsWith("/*"))
                    content = content.Substring(content.IndexOf("*/") + 2);
                _models_cache.Add(filename, content);
            }

            // Remplace les arguments
            if (replacements != null)
                for (int r = 1; r <= replacements.Length; ++r)
                    content = content.Replace("$" + r.ToString(), replacements[r-1].Replace("'", "''"));

            return content;
        }
        static Dictionary<string /* model filename */, string /* file content with no header */> _models_cache = new Dictionary<string, string>();
        #endregion

        #region Recuperation des objets SQL

        public enum DbObjectType
        {
            UNKNOWN = 0,
            // Les noms qui suivent doivent être en concordance avec les valeurs renvoyées par le script SQL Server !
            //TABLE, // reservé au table system ?
            USER_TABLE,
            VIEW,
            SQL_STORED_PROCEDURE,
            SQL_INLINE_TABLE_VALUED_FUNCTION,
            SQL_SCALAR_FUNCTION,
            SQL_TABLE_VALUED_FUNCTION,
            SQL_TRIGGER,
            PRIMARY_KEY_CONSTRAINT,
            FOREIGN_KEY_CONSTRAINT,
            DEFAULT_CONSTRAINT,
            UNIQUE_CONSTRAINT,
            CHECK_CONSTRAINT,
            SERVICE_QUEUE,
            INTERNAL_TABLE,
            SYSTEM_TABLE,
            CLR_TABLE_VALUED_FUNCTION,
            CLR_SCALAR_FUNCTION,
            CLR_STORED_PROCEDURE
        }

        [DebuggerDisplay("Name={Name}, Type={Type}, Args={Args}")]
        public class DbObject // TODO : migrer vers SqlObject
        {
            // TODO : Faire une hierarchie DbObject => DbDefinition  et DbObject => SqlScript pour savoir si le code vinet de la base sql (generation d'un objet) ou d'u nscript (fichier sql)
            public string            Name;
            public DbObjectType      Type;
            public string            Schema;
            public DateTime          DateOfCreation;
            public DateTime          DateOfChange;
            public string            Source;
            public List<DbObjectArg> Args;

            internal static DbObject FromDataRow(DataRow row)
            {
                var result = new DbObject();

                result.Name = (string)row["Name"];
                result.Type = (DbObjectType)Enum.Parse(typeof(DbObjectType), (string)row["ObjectType"]);
                result.Schema = row["SchemaName"] == DBNull.Value ? "" : (string)row["SchemaName"];
                result.DateOfCreation = (DateTime)row["DateOfCreation"];
                result.DateOfChange = (DateTime)row["DateOfChange"];
                result.Source = row["Source"] == DBNull.Value ? "" : (string)row["Source"];

                return result;
            }

            public static DbObject FromXElement(XElement root)
            {
                var res = new DbObject();
                res.Import(root);
                return res;
            }
            public void Import(XElement root)
            {
                DateOfChange = DateTime.Parse(root.Attribute("DateOfChange").Value);
                DateOfCreation = DateTime.Parse(root.Attribute("DateOfCreation").Value);
                Name = root.Attribute("Name").Value;
                Schema = root.Attribute("Schema").Value;
                Type = (DbObjectType)Enum.Parse(typeof(DbObjectType), root.Attribute("Type").Value);
                Source = root.Attribute("Source").Value;
            }

            public XElement Export(XElement root = null)
            {
                root = root ?? new XElement("DbOjbect");
                root.Add(new XAttribute("DateOfChange", DateOfChange.ToString()));
                root.Add(new XAttribute("DateOfCreation", DateOfCreation.ToString()));
                root.Add(new XAttribute("Name", Name));
                root.Add(new XAttribute("Schema", Schema));
                root.Add(new XAttribute("Type", Type.ToString()));
                root.Add(new XAttribute("Source", Source));
                return root;
            }
        }

        [DebuggerDisplay("Index={Index}, Name={Name}, Type={Type}")]
        public class DbObjectArg
        {
            public int    Index;
            public string Name;
            public string Type;
        }

        public static List<DbObject> GetDbObjects(ConnectionSetting connection, bool with_source = false, DbObjectType? only_this_type = null)
        {
            System.Diagnostics.Contracts.Contract.Assert(connection != null);
            SqlCommand cmd = connection.Connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = LoadModelAndSetArguments("GetObjectSources.sql", 
                with_source ? "1" : "0",
                only_this_type.HasValue ? only_this_type.ToString() : "");

            var all = connection.ExecuteAndConvertRows(cmd, DbObject.FromDataRow);

            FillParametersInfo(connection, all);

            return all;
        }


        private static void FillParametersInfo(ConnectionSetting connection, List<DbObject> objects)
        {
            foreach (DbObject obj in objects)
                if (obj.Type == DbObjectType.SQL_STORED_PROCEDURE ||
                    obj.Type == DbObjectType.SQL_SCALAR_FUNCTION ||
                    obj.Type == DbObjectType.SQL_INLINE_TABLE_VALUED_FUNCTION || 
                    obj.Type == DbObjectType.SQL_TABLE_VALUED_FUNCTION || 
                    obj.Type == DbObjectType.CLR_TABLE_VALUED_FUNCTION)
                {
                    // TODO : Comparer le code suivant :
                    SqlCommand cmd = connection.Connection.CreateCommand();
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.CommandText = LoadModelAndSetArguments("GetProcedureOrFunctionParameters.sql", obj.Name);
                    obj.Args = connection.ExecuteAndConvertRows(cmd, row => new DbObjectArg()
                                          {
                                            Index = (int)row["ORDINAL_POSITION"],
                                            Name  = (string)row["PARAMETER_NAME"],
                                            // TODO : A ameliorer de facon empirique
                                            Type  = (string)row["DATA_TYPE"] == "varchar"   ? "varchar("   + (int)row["CHARACTER_MAXIMUM_LENGTH"] + ")"
                                                  : (string)row["DATA_TYPE"] == "numeric"   ? "numeric("   + (byte)row["NUMERIC_PRECISION"] + "," + (int)row["NUMERIC_SCALE"] + ")"
                                                  : (string)row["DATA_TYPE"] == "datetime2" ? "datetime2(" + (short)row["DATETIME_PRECISION"] + ")"
                                                  : (string)row["DATA_TYPE"]
                                          })
                                         .ToList();
                    // Avec :
                    //SqlCommand cmd = new SqlCommand(spName, connection);
                    //cmd.CommandType = CommandType.StoredProcedure;
                    //SqlCommandBuilder.DeriveParameters(cmd);
                    //if (!includeReturnValueParameter)
                    //    cmd.Parameters.RemoveAt(0);

                    //obj.Args = cmd.Parameters.Cast<SqlParameter>().Select(par => new DbObjectArg()
                    //    {
                    //        // A ecrire 
                    //    }).ToList();

                }
        }

        public static SqlServerVersion GetSqlServerVersion(ConnectionSetting connection)
        {
            System.Diagnostics.Contracts.Contract.Assert(connection != null);
            SqlCommand cmd = connection.Connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = LoadModelAndSetArguments("GetSqlServerVersion.sql");

            var sqlServerVersions = connection.ExecuteAndConvertRows(cmd, row =>
            {
                var result = new SqlServerVersion();
                result.MajorVersion   = (string)row["MajorVersion"];
                result.ProductLevel   = (string)row["ProductLevel"];
                result.Edition        = (string)row["Edition"];
                result.ProductVersion = (string)row["ProductVersion"];
                return result;
            });

            return sqlServerVersions.First();
        }
        public class SqlServerVersion
        {
            public string MajorVersion   { get; internal set; }
            public string ProductLevel   { get; internal set; }
            public string Edition        { get; internal set; }
            public string ProductVersion { get; internal set; }
        }

        /// <summary>
        /// Trie les tables dans l'ordre dans lequel il faut faire les delete 
        /// afin de garantir qu'aucune contrainte de clef étrangère ne sera brisé.
        /// TODO : a remplacer par une analyse locale des clefs ?
        /// </summary>
        public static List<T> SortTableInDeleteOrder<T>(ConnectionSetting connection, List<T> objects, Func<T, string> get_name)
        {
            connection.Open();
            SqlCommand cmd = connection.Connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = LoadModelAndSetArguments("GetTablesInDeleteOrder.sql");
            var orders = connection.ExecuteAndConvertRows(cmd, row => new
                {
                    Level = (int)row["Lvl"],
                    TableName = (string)row["TableName"]
                });
            connection.Close();
            var res = orders.Select(order => objects.FirstOrDefault(obj => get_name(obj) == order.TableName))
                            .Where(obj => obj != null)
                            .ToList();
            if (orders.Count != res.Count)
                throw new Exception("DataBase is not sync with current data !");

            return res;
        }
        #endregion

        #region Récupération des reference
        public enum DbReferenceType
        {
            OBJECT = 1,
            DATABASE_DDL_TRIGGER = 12,
            SERVER_DDL_TRIGGER = 13,
            // Non spécifié dans la spec de microsoft (?!) :(
            OBJECT_OR_COLUMN
        }

        public class DbReference
        { // from sys.dm_sql_referencing_entities (Transact-SQL) doc : 
          // http://technet.microsoft.com/en-us/library/bb630351.aspx

            /// <summary>
            /// Schema in which the referencing entity belongs. Is nullable.
            /// NULL for database-level and server-level DDL triggers.
            /// </summary>
            public string ReferencingSchemaName; // nullable
            /// <summary>
            /// Name of the referencing entity. Is not nullable.
            /// </summary>
            public string ReferencingEntityName;

            /// <summary>
            /// ID of the referencing entity. Is not nullable.
            /// </summary>
            public int ReferencingId;

            public DbReferenceType ReferencingClass;

            /// <summary>
            /// Class of the referencing entity. Is not nullable.
            /// 1 = Object
            /// 12 = Database-level DDL trigger
            /// 13 = Server-level DDL trigger
            /// </summary>
            public byte Type;

            /// <summary>
            /// Description of class of referencing entity.
            /// OBJECT
            /// DATABASE_DDL_TRIGGER
            /// SERVER_DDL_TRIGGER
            /// </summary>
            public DbReferenceType ReferencingClassDescription;

            /// <summary>
            /// Indicates the resolution of the referenced entity ID occurs at run time because it depends on the schema of the caller.
            /// 1 = The referencing entity has the potential to reference the entity; however, resolution of the referenced entity ID is caller dependent and cannot be determined. This occurs only for non-schema-bound references to a stored procedure, extended stored procedure, or user-defined function called in an EXECUTE statement.
            /// 0 = Referenced entity is not caller dependent.
            /// </summary>
            public bool  IsCallerDependent;

            public static DbReference FromDataRow(DataRow row)
            {
                var result = new DbReference();

                result.ReferencingSchemaName       = (string)row["referencing_schema_name"];
                result.ReferencingEntityName       = (string)row["referencing_entity_name"];
                result.ReferencingId               = (int)row["referencing_id"];
                result.ReferencingClass            = (DbReferenceType)(byte)row["referencing_class"];
                result.ReferencingClassDescription = (DbReferenceType)Enum.Parse(typeof(DbReferenceType), 
                                                                                 (string)row["referencing_class_desc"]);  // Max 60 char
                result.IsCallerDependent           = (bool)row["is_caller_dependent"];

                return result;
            }
        }

        public static List<DbReference> GetDbObjectDependencies(ConnectionSetting connection, string schema_name)
        {
            System.Diagnostics.Contracts.Contract.Assert(connection != null);
            System.Diagnostics.Contracts.Contract.Assert(!string.IsNullOrWhiteSpace(schema_name));

            SqlCommand cmd = connection.Connection.CreateCommand();
            cmd.CommandType = System.Data.CommandType.Text;
            // Taille max des arguments : /@name nvarchar(517), @referenced_class nvarchar(60)
            cmd.CommandText = "SELECT * FROM sys.dm_sql_referencing_entities('" + schema_name + ".SHARES', 'OBJECT')";

            var all_but_tables = connection.ExecuteAndConvertRows(cmd, row => DbReference.FromDataRow(row));

            return all_but_tables;
        }
        #endregion
    }
}
