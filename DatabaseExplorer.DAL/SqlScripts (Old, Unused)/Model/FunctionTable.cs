﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.SQL_TABLE_VALUED_FUNCTION)]
    public class TableFunction : SqlObject
    {
        public TableFunction(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }

    }
}
