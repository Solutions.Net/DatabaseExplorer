﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.SQL_STORED_PROCEDURE)]
    public class StoredProcedure : SqlObject
    {
        public StoredProcedure(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }
    }
}
