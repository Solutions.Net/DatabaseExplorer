﻿using System;
using System.Collections.Generic;


namespace DatabaseExplorer.DAL.Old
{
    public interface IHasColumns : IHasName
    {
        List<Column> Columns { get; }
    }
}
