﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.SQL_INLINE_TABLE_VALUED_FUNCTION)]
    public class TableFunctionInline : SqlObject
    {
        public TableFunctionInline(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }
    }
}
