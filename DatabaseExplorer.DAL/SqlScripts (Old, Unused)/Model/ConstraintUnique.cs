﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.UNIQUE_CONSTRAINT)]
    public class ConstraintUnique : Constraint
    {
        public ConstraintUnique(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }
    }
}
