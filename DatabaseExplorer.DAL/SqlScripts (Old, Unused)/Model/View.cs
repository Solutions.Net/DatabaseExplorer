﻿using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace DatabaseExplorer.DAL.Old
{
    [DebuggerDisplay("Name={Name}, Fields={FieldsAsOneLineString}")]
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.VIEW)]
    public class View : SqlObject, IHasColumns
    {
        public Schema Owner { get; set; }
        public override string Name { get { return _Name ?? RawInfo?.Name; } set { _Name = value; } } string _Name;

        public List<Column> Columns { get; set; } = new List<Column>();

        public void CheckInvariants()
        {
            Debug.Assert(!string.IsNullOrWhiteSpace(Name));
            Debug.Assert(Name == Name.Trim());
            Debug.Assert(Columns != null && Columns.Count >= 1);
        }

        public View(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }

    }
}
