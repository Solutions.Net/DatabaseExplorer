﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    public class SqlObjectTypeAttribute : Attribute
    {
        public SQLProcedureModelManager.DbObjectType RawType { get; private set; }

        public SqlObjectTypeAttribute(SQLProcedureModelManager.DbObjectType rawType)
        {
            RawType = rawType;
        }
    }
}
