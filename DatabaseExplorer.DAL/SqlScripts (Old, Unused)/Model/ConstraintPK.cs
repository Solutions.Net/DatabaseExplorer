﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.PRIMARY_KEY_CONSTRAINT)]
    public class ConstraintPK : Constraint
    {
        public List<Column> Columns { get; set; }        public List<Column> PKFields { get { return Columns; } set { Columns = value; } }

        public ConstraintPK()
            : base(null)
        {
        }


        public ConstraintPK(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
        }

        private void Invariants()
        {
            Debug.Assert(PKFields != null);
            Debug.Assert(PKFields.Count == 0 || PKFields.All(f => f.Owner == PKFields[0].Owner));
        }

        public string SQL_AsDeclaration(string pk_constraint_name = null, bool clustered = true)
        {
            Invariants();
            return "ALTER TABLE " + Table.SQL_AsProtectedName + Environment.NewLine +
                   "DROP PRIMARY KEY, " + Environment.NewLine +
                   "ADD " + (string.IsNullOrWhiteSpace(pk_constraint_name) ? "" : "CONSTRAINT [" + pk_constraint_name.Trim() + "]" + Environment.NewLine) +
                   "PRIMARY KEY " + (clustered ? "CLUSTERED" : "NONCLUSTERED") +
                   "(" + string.Join(", ", PKFields.Select(f => f.SQL_AsProtectedName).ToList()) + ")";
        }
    }
}
