﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.CLR_STORED_PROCEDURE)]
    public class StoredProcedureCLR : SqlObject
    {
        public StoredProcedureCLR(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }
    }
}
