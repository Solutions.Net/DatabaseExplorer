﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;


namespace DatabaseExplorer.DAL.Old
{
    [DebuggerDisplay("{AsDebugString,nq}")]
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.FOREIGN_KEY_CONSTRAINT)]
    public class ConstraintFK : Constraint
    {
        public Column[]  ForeignColumns    { get; set; }
        public Column[]  ReferencedColumns { get; set; }

        public ConstraintFK()
            : base(null)
        {
        }




        public enum eOrigin
        {
            Explicit, // La contrainte est explicitement defini
            Guessed // la contrainte n'est pas defini explicitement, elle a ete deviné en fonction de l'utilisation par le code SQL, dans ce cas Name retourne null !
        }
        public eOrigin Origin { get; set; }
        public Column PrimaryKeyField { get { if (ReferencedColumns.Length > 1) throw new Exception("not handled !"); return ReferencedColumns.First(); }
                                        set { ReferencedColumns = new Column[] { value }; } }
        public Column ForeignKeyField { get { if (ForeignColumns.Length > 1) throw new Exception("not handled !"); return ForeignColumns.First(); }
                                        set { ForeignColumns = new Column[] { value }; } }


        public ConstraintFK(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
        }

        public IEnumerable<T> ForEach<T>(Func<Column, Column, T> fkRefColumnSelector)
        {
            return ForeignColumns.Zip(ReferencedColumns, fkRefColumnSelector);
        }

        string AsDebugString
        {
            get { return Name + " as [" + ForeignColumns.First().Owner.Name + "](" + ForeignColumns.Select(col => col.Name).Join() + ") => " + ReferencedColumns[0].Owner.Name + " (" + ReferencedColumns.Select(col => col.Name).Join() + ")"; }
        }

        public override string ToString()
        {
            return PrimaryKeyField.Table.Name + "." + PrimaryKeyField.Name
                 + " = "
                 + ForeignKeyField.Table.Name + "." + ForeignKeyField.Name;
        }
        public string ToStringReversed()
        {
            return new ConstraintFK(RawInfo)
            {
                PrimaryKeyField = ForeignKeyField,
                ForeignKeyField = PrimaryKeyField
            }.ToString();
        }

        public void CheckInvariants()
        {
            Debug.Assert(Origin == eOrigin.Explicit && RawInfo != null ||
                         Origin == eOrigin.Guessed && RawInfo == null);
            Debug.Assert(ForeignColumns.Any());
            Debug.Assert(ReferencedColumns.Any());
        }
    }


}
