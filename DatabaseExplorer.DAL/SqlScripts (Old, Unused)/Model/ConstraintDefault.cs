﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.DEFAULT_CONSTRAINT)]
    public class ConstraintDefault : Constraint
    {
        public ConstraintDefault(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }
    }
}
