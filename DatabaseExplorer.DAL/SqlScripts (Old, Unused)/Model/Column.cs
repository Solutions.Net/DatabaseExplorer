﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

using TechnicalTools;


namespace DatabaseExplorer.DAL.Old
{
    [DebuggerDisplay("{" + nameof(Name) + ",nq}}")]
    public class Column : SqlObject
    {
        public IHasColumns  Owner             { get; set; }
        public string       SqlTypeAsString   { get { return _SqlTypeAsString ?? (SqlType == SqlType.String ? (HasLengthLimit ? "nvarchar(" + ArrayLength + ")" : "nvarchar(max)")
                                                                                : SqlType == SqlType.VarBinary ? (HasLengthLimit ? "varbinary(" + ArrayLength + ")" : "varbinary(max)")
                                                                                : SqlType.CSharp_SqlDbType); } 
                                                set { _SqlTypeAsString = value; } } string _SqlTypeAsString;
        public SqlType      SqlType           { get; set; } public string Type { get { return SqlType.CSharp_SqlDbType; } }
        
        public bool         IsNullable        { get; set; }
        public bool         IsIdentity        { get; set; }
        public bool         IsComputed        { get; set; }
        public bool         IsFileStream      { get; set; }
        public bool         IsSparse          { get; set; }
        public bool         IsColumnSet       { get; set; }
        public string       DefaultValue      { get; set; }
        public int          OrdinalPosition   { get; set; }
        public int          ArrayLength       { get; set; }
        public bool         HasLengthLimit    { get; set; }

        public bool         IsAutoIncremented { get; set; }
        public ConstraintPK PkDefinition      { get { return Owner.Pk != null && Owner.Pk.Columns.Any(col => col == this) ? Owner.Pk : null; } }
        public bool         IsInPk            { get { return PkDefinition != null; } }

        public ConstraintFK FkDefinition      { get { return Owner.FkConstraints.FirstOrDefault(fkCol => fkCol.ForeignColumns.Any(col => col == this)); } }
        public bool         IsFK              { get { return FkDefinition != null; } }

        public Column ReferencedColumn        { get { Debug.Assert(IsFK); return FkDefinition.ReferencedColumns[FkDefinition.ForeignColumns.IndexOf(this)]; } }
        public Type CsType
        {
            get
            {
                var mapping = SqlType.FromSqlType(SqlTypeAsString);
                Debug.Assert(mapping != null);
                if (mapping == SqlType.Xml)
                    mapping = SqlType.String;
                if (IsNullable && !mapping.CSharpType.IsClass)
                    return typeof(Nullable<>).MakeGenericType(mapping.CSharpType);
                return mapping.CSharpType;
            }
        }
        public Type CsTypeNotNullable
        {
            get
            {
                return CsType.TryGetNullableType() ?? CsType;
            }
        }
        
        private string CsTypeAsString { get { return TypeToString(CsType); } }

        public static string TypeToString(Type type)
        {
            return  type.ToSmartString();
        }

        public Column()
            : base(null)
        {
        }

        #region Legacy

        public Column(IHasColumns parent, string name, string sqlType, bool nullable, bool isIdentity)
            : base(parent.RawInfo)
        {
            Table = parent;
            Name = name;
            SqlType = SqlType.FromSqlType(sqlType);
            IsNullable = nullable;
            IsIdentity = isIdentity;
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            ToString(sb);
            return sb.ToString();
        }
        public void ToString(StringBuilder sb)
        {
            sb.Append(Name);
        }

        /// <summary>
        /// Heuristique calculant la probabilité (en %) que ce champs soit la colonne identité de la table
        /// </summary>
        public double PotentialIdentityFieldScore
        {
            get
            {
                if (IsIdentity) return 100;

                Debug.Assert(Table != null);
                Table.CheckInvariants();

                double res;
                int biais;
                string name = Name.ToUpperInvariant();
                if (name == "ID") return 99;
                if (name.StartsWith("ID_") || name.EndsWith("_ID")) { res = 90; biais = 3 - 1; }
                else if (name.StartsWith("COD_") || name.EndsWith("_COD")) { res = 80; biais = 4 - 1; }
                else if (name.StartsWith("CODE_") || name.EndsWith("_CODE")) { res = 80; biais = 5 - 1; }
                else if (name.StartsWith("ID") || name.EndsWith("ID")) { res = 70; biais = 2; }
                else if (name.StartsWith("COD") || name.EndsWith("COD")) { res = 60; biais = 3; }
                else if (name.StartsWith("CODE") || name.EndsWith("CODE")) { res = 60; biais = 4; }
                else { res = 10; biais = 0; }

                // Ajuste la valeur pour comparer en cas de plusieurs clé de même score.
                // La clef dont la longueur du nom est la plus proche du nom de la table est avantagée.
                int distance = name.Length - biais
                             - (Table == null ? 4 : Table.Name.Length);
                res += 1.0 / Math.Max(1, Math.Abs(distance));

                return res;
            }
        }



        public string PropertyName
        {
            get
            {
                bool belongToPk = IsIdentity;
                bool isId = !Table.PrimaryKeyIsCluster && belongToPk;

                if (isId/* || Name.ToLower() == "Id" && !IsNullable*/)
                    return "ID"; // Permet d'overrider la propriété "Id" du type de base : oTypedDataRow !

                //string result = CultureInfo.CurrentCulture.TextInfo
                //                           .ToTitleCase(Name.Replace("_", ""))
                //                           .Replace(" ", "");
                string result = Name;
                //if (result.EndsWith("ID") && result.Length >= 3 && (char.IsLower(result[result.Length - 3]) || result[result.Length - 3] == '_'))
                //    result = result.Remove(result.Length - 2) + "Id";
                // TODO : A améliorer (par exemple avec un dictionnaire de mot fonctionnel pour trouver la separation entre les mots)
                result = result.Replace("_ID", "*ID");
                result = result.Replace("_", "");
                result = result.Replace("*ID", "_ID");
                return result;
            }
        }

        public string BackingStoreFieldName
        {
            get
            {
                return "_" + PropertyName;
            }
        }
        public string PropertyTypeCS
        {
            get
            {
                return SqlType.FromSqlType(Type).ToCSharp(IsNullable);
            }
        }

        public string AsDebugInfo
        {
            get
            {
                return PropertyName + "={" + PropertyName + "}";
            }
        }

        #region Natixis (C#)
        public string Natixis_PropertyName
        {
            get
            {
                if (IsIdentity || (Name.ToLower() == "Id" && !IsNullable))
                    return "Id"; // Permet d'overrider la propriété "Id" du type de base : oTypedDataRow !

                string result = CultureInfo.CurrentCulture.TextInfo
                                           .ToTitleCase(Name.ToLower().Replace("_", " "))
                                           .Replace(" ", "");
                if (!Name.Contains("_"))
                {
                    // TODO : A améliorer (par exemple avec un dictionnaire de mot fonctionnel pour trouver la separation entre les mots)
                }
                return result;
            }
        }

        public string Natixis_AsPropertyDeclaration
        {
            get
            {
                return "public " + (Natixis_PropertyName == "Id" ? "override " : "")
                    + PropertyTypeCS.PadRight(Table.Stats.CSharp_PropertyTypeMaxLength)
                    + " "
                    + Natixis_PropertyName.PadRight(Table.Stats.CSharp_PropertyNameMaxLength)
                    + " {"
                    + (" get { return Get<" + PropertyTypeCS + ">").PadRight(19 + Table.Stats.CSharp_PropertyTypeMaxLength)
                    + ("(\"" + Name + "\"); }").PadRight(7 + Table.Stats.CSharp_PropertyNameMaxLength)
                    + " set { Set(\"" + Name + "\", value); }"
                    + " }";
            }
        }

        public string Natixis_AsInitStatement
        {
            get
            {
                return Natixis_PropertyName + " = " + SqlType.FromSqlType(Type).CSharp_DefaultValue(IsNullable) + ";";
            }
        }

        public string Natixis_AsQueriesHelperParam
        {
            get
            {
                return "QueriesHelper.param" + (IsNullable ? "Nullable" : "")
                     + "(\"@" + Natixis_PropertyName + "\",                SqlDbType." + SqlType.FromSqlType(Type).CSharp_SqlDbType + ",     " + Natixis_PropertyName + ")";
            }
        }
        #endregion Natixis (C#)

        #region SQL
        public string SQL_AsColumnDeclaration
        {
            get
            {
                string str = SQL_AsProtectedName.PadRight(Table.Stats.SQL_ColumnNameMaxLength + 2)
                     + "\t" + Type.PadRight(15)
                     + "\t" + (IsNullable ? "    " : "NOT ") + "NULL"
                     + (IsIdentity ? "\tIDENTITY(1,1)" : "");
                return str;
            }
        }
        public string SQL_AsProtectedName
        {
            get
            {
                return "[" + Name + "]";
            }
        }

        public string SQL_AsArgumentName
        {
            get
            {
                return Natixis_PropertyName;
            }
        }
        public string SQL_AsArgumentDeclaration
        {
            get
            {
                return "@" + SQL_AsArgumentName.PadRight(Table.Stats.SQL_ColumnNameMaxLength + 3)
                     + "\t" + Type;
            }
        }
        #endregion SQL
        #endregion

    }
}
