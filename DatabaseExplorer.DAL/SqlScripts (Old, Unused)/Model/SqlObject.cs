﻿using System;
using System.Diagnostics;


namespace DatabaseExplorer.DAL.Old
{
    public abstract class SqlObject : IHasName
    {
        public DatabaseInfo Project { get; set; }

        public SQLProcedureModelManager.DbObject RawInfo { get; set; } // Peut etre à null si l'objet n'est pas lié à une definition Sql (TODO : renommer en SqlDef)

        public virtual string Name { get; set; }

        public bool Parsed { get; protected set; }

        public SQLProcedureModelManager.DbObjectType RawType
        {
            get { return GetRawType(GetType()).Value; }
        }



        public static SQLProcedureModelManager.DbObjectType GetRawType<T>()
            where T : SqlObject
        {
            return GetRawType(typeof(T)).Value;
        }
        public static SQLProcedureModelManager.DbObjectType? GetRawType(Type type, bool null_allowed = false)
        {
            var atts = type.GetCustomAttributes(typeof(SqlObjectTypeAttribute), false);
            if (atts.Length == 0)
            {
                if (!null_allowed)
                    throw new Exception("Les classes heritant de SqlObject doivent avoir un attribut " + typeof (SqlObjectTypeAttribute).Name);
                return null;
            }
            return ((SqlObjectTypeAttribute) atts[0]).RawType;
        }
    
        protected SqlObject(SQLProcedureModelManager.DbObject raw_info)
        {
            RawInfo = raw_info;
        }


        public void EnsureIsParsed()
        {
            if (!Parsed)
                Parse();
        }
        
        // Remplit les proprietes de l'objet courant en utilisant RawInfo
        // Passe Parsed à true une fois cela fait
        protected virtual void Parse()
        {
            if (Debugger.IsAttached)
                Debugger.Break(); // Il est temps d'overrider "Parse()" :)
            Parsed = true;
        }

    }
}
