﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace DatabaseExplorer.DAL.Old
{
    [DebuggerDisplay("{Owner?.Name,nq}.{Name,nq}")]
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.USER_TABLE)]
    public class Table : SqlObject, IHasColumns
    {
        public Schema Owner { get; set; }
        public string Description { get; set; }
        public List<Column> Columns { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ChangeDate { get; set; }

        public ConstraintPK Pk { get; set; } // nullable !
        public List<ConstraintFK> FkConstraints { get; set; } // non null mais potentiellement vide
        
        public List<ConstraintFK> FkReferencing { get; set; } // non null mais potentiellement vide

        public string FullName { get { return string.IsNullOrEmpty(Owner.Name) ? Name : Owner.Name + "." + Name; } }

        //public Table BaseTable { get; set; } // heritage Pour le moment c'est dans TableData

        public Table()
            : base(null)
        {
        }


        #region Legacy

        public Table(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
        }

        public List<Column> FieldsWithIdFieldInFirst(Column fieldUsedAsId)
        {
            Debug.Assert(fieldUsedAsId != null);
            Debug.Assert(Columns.Contains(fieldUsedAsId));

            var sorted_fields = new List<Column>(Columns);
            sorted_fields.Remove(fieldUsedAsId);
            sorted_fields.Insert(0, fieldUsedAsId);
            return sorted_fields;
        }

        public bool PrimaryKeyIsCluster
        {
            get { return Columns.Count(field => field.IsIdentity) > 1; }
        }

        public Column FieldWithIdentity
        {
            get
            {
                Debug.Assert(Columns.Where(f => f.IsIdentity).Count() <= 1);
                return Columns.FirstOrDefault(f => f.IsIdentity);
            }
        }

        public Column FieldWhichIsProbablyIdentity
        {
            get
            {
                return Columns.OrderByDescending(f => f.PotentialIdentityFieldScore)
                              .FirstOrDefault();
            }
        }

        public void CheckInvariants()
        {
            Debug.Assert(!string.IsNullOrWhiteSpace(Name));
            Debug.Assert(Name == Name.Trim());
            Debug.Assert(Columns != null && Columns.Count >= 1);
        }

        #region Code Generation

        #region C#
        private string CSharp_ClassNameBase
        {
            get
            {
                string singular_name = Name.ToLower().EndsWith("s")
                                     ? Name.Remove(Name.Length - 1)
                                     : Name;
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                singular_name = singular_name.ToLower();
                singular_name = singular_name.Replace("_", " ");
                singular_name = textInfo.ToTitleCase(singular_name);
                singular_name = singular_name.Replace(" ", "");
                return singular_name;
            }
        }
        public string CSharp_ClassName { get { return "o" + CSharp_ClassNameBase; } }


        #endregion C#

        public string NameSingular
        {
            get
            {
                string singular_name = Name.ToLower().EndsWith("ies")
                                     ? Name.Remove(Name.Length - 3) + "y"
                                     : Name.ToLower().EndsWith("s")
                                     ? Name.Remove(Name.Length - 1)
                                     : Name;
                CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
                TextInfo textInfo = cultureInfo.TextInfo;
                singular_name = singular_name.Replace("_", " ");
                //singular_name = textInfo.ToTitleCase(singular_name);
                singular_name = singular_name.Replace(" ", "");
                return singular_name;
            }
        }
        public string NamePlural
        {
            get
            {
                return Name + (Name.ToLower().EndsWith("s") ? "" : "s");
            }
        }

        #region SQL
        public string SQL_AsProtectedName
        {
            get
            { // TODO : Mettre le chemin complet vers la table (schema etc)
                return "[" + Name + "]";
            }
        }
        public string SQL_SaveProcedureName(string functionalDomain)
        {
            Debug.Assert(!string.IsNullOrEmpty(functionalDomain));
            return "p" + functionalDomain.ToUpper() + "_" + CSharp_ClassNameBase + "_SaveOrUpdate";
        }

        public string SQL_GetByIdProcedureName(string functionalDomain)
        {
            Debug.Assert(!string.IsNullOrEmpty(functionalDomain));
            return "p" + functionalDomain.ToUpper() + "_" + CSharp_ClassNameBase + "_GetById";
        }
        public string SQL_DeleteProcedureName(string functionalDomain)
        {
            Debug.Assert(!string.IsNullOrEmpty(functionalDomain));
            return "p" + functionalDomain.ToUpper() + "_" + CSharp_ClassNameBase + "_DeleteById";
        }

        public string SQL_AsDeclaration
        {
            get
            {
                string result = "CREATE TABLE " + SQL_AsProtectedName + Environment.NewLine +
                                "(" + Environment.NewLine +
                                "\t" + string.Join("," + Environment.NewLine + "\t", Columns.Select(field => field.SQL_AsColumnDeclaration)) + Environment.NewLine +
                                ") ON [PRIMARY] ";
                return result;
            }
        }
        #endregion SQL

        public class StatisticsInfo
        {
            public int SQL_ColumnNameMaxLength = 0;
            public int CSharp_PropertyTypeMaxLength = 0;
            public int CSharp_PropertyNameMaxLength = 0;
        }

        public StatisticsInfo Stats
        {
            get
            {
                if (_stats == null)
                    RefreshStatisticsInfo();
                return _stats;
            }
        }
        private StatisticsInfo _stats;
        public void RefreshStatisticsInfo()
        {
            _stats = new StatisticsInfo()
            {
                SQL_ColumnNameMaxLength = Columns.Select(f => f.Name.Length).Max(),
                CSharp_PropertyTypeMaxLength = Columns.Select(f => f.PropertyTypeCS.Length).Max(),
                CSharp_PropertyNameMaxLength = Columns.Select(f => f.PropertyName.Length).Max()
            };
        }
        #endregion Code Generation


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            ToString(sb);
            return sb.ToString();
        }
        public void ToString(StringBuilder sb)
        {
            sb.AppendLine(Name);
            sb.AppendLine("{");
            foreach (Column col in Columns)
            {
                sb.Append("\t");
                col.ToString(sb);
                sb.AppendLine();
            }
            sb.Append("}");
        }
        private string FieldsAsOneLineString { get { return "{ " + string.Join(", ", Columns.Select(f => f.SQL_AsColumnDeclaration.Replace("\t", "  "))) + " }"; } }
        
        #endregion Legacy
    }
}
