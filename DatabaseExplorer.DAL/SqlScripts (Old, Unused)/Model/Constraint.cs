﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    public abstract class Constraint : SqlObject
    {
        public Table Owner { get; set; }  public Table Table { get { return Owner; } set { Owner = value; } }

        protected Constraint(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
        }
    }
}
