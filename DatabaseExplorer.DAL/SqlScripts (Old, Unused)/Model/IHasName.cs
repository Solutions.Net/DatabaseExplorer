﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    public interface IHasName
    {
        string Name { get; }
    }
}
