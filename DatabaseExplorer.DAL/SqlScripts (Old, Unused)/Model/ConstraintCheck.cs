﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.CHECK_CONSTRAINT)]
    public class ConstraintCheck : Constraint
    {
        public ConstraintCheck(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }
    }
}
