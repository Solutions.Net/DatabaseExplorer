﻿using System;


namespace DatabaseExplorer.DAL.Old
{
    [SqlObjectType(SQLProcedureModelManager.DbObjectType.SQL_SCALAR_FUNCTION)]
    public class ScalarFunction : SqlObject
    {
        public ScalarFunction(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            
        }
    }
}
