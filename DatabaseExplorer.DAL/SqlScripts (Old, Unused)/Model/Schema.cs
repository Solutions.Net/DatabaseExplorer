﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using TechnicalTools.Algorithm.Graph;

using DbObject = DatabaseExplorer.DAL.SQLProcedureModelManager.DbObject;
using DbObjectType = DatabaseExplorer.DAL.SQLProcedureModelManager.DbObjectType;


namespace DatabaseExplorer.DAL.Old
{
    [DebuggerDisplay("{Name,nq}")]
    public class Schema : SqlObject
    {
        public List<Table>                Tables                { get; } = new List<Table>();
        public List<View>                 Views                 { get; } = new List<View>();
        public List<StoredProcedure>      Procedures            { get; } = new List<StoredProcedure>();
        public List<StoredProcedureCLR>   ProceduresInClr       { get; } = new List<StoredProcedureCLR>();
        public List<TableFunctionInline>  InlineTableFunction   { get; } = new List<TableFunctionInline>();
        public List<ScalarFunction>       ScalarFunctions       { get; } = new List<ScalarFunction>();
        public List<TableFunction>        TableFunctions        { get; } = new List<TableFunction>();
        public List<ConstraintPK>         PrimaryKeyConstraints { get; } = new List<ConstraintPK>();
        public List<ConstraintFK>         ForeignKeyConstraints { get; } = new List<ConstraintFK>();
        public List<ConstraintDefault>    DefaultConstraints    { get; } = new List<ConstraintDefault>();
        public List<ConstraintUnique>     UniqueConstraints     { get; } = new List<ConstraintUnique>();
        public List<ConstraintCheck>      CheckConstraints      { get; } = new List<ConstraintCheck>();
        
        public Schema(SQLProcedureModelManager.DbObject raw_info)
            : base(raw_info)
        {
            ExportableLists = new[]
            {
                new ListInfo("Tables",                Tables,                DbObjectType.USER_TABLE),
                new ListInfo("Procedures",            Procedures,            DbObjectType.SQL_STORED_PROCEDURE),
                new ListInfo("Views",                 Views,                 DbObjectType.VIEW),
                new ListInfo("InlineTableFunction",   InlineTableFunction,   DbObjectType.SQL_INLINE_TABLE_VALUED_FUNCTION),
                new ListInfo("ScalarFunctions",       ScalarFunctions,       DbObjectType.SQL_SCALAR_FUNCTION),
                new ListInfo("TableFunctions",        TableFunctions,        DbObjectType.SQL_TABLE_VALUED_FUNCTION),
                new ListInfo("PrimaryKeyConstraints", PrimaryKeyConstraints, DbObjectType.PRIMARY_KEY_CONSTRAINT),
                new ListInfo("ForeignKeyConstraints", ForeignKeyConstraints, DbObjectType.FOREIGN_KEY_CONSTRAINT),
                new ListInfo("DefaultConstraints",    DefaultConstraints,    DbObjectType.DEFAULT_CONSTRAINT),
                new ListInfo("UniqueConstraints",     UniqueConstraints,     DbObjectType.UNIQUE_CONSTRAINT),
                new ListInfo("CheckConstraints",      CheckConstraints,      DbObjectType.CHECK_CONSTRAINT)
            }.ToList();
        }
        List<ListInfo> ExportableLists;

        public class ListInfo
        {
            public string Folder { get; private set; }
            public DbObjectType Type { get; private set; }
            public IList Objects { get; private set; }
            public ListInfo(string folder, IList objects, DbObjectType type)
            {
                Folder = folder;
                Type = type;
                Objects = objects;
            }
        }


        public void ImportFromDatabase(IEnumerable<DbObject> objects)
        {
            var byTypes = objects.ToLookup(obj => obj.Type);
            foreach (var lstInfo in ExportableLists)
            {
                lstInfo.Objects.Clear();
                var objType = lstInfo.Type;
                foreach (DbObject db_obj in byTypes[objType].OrderBy(r => r.Name))
                {
                    var constructor = SqlObjectConstructorFor[objType];
                    var wrapping_business_object = constructor(db_obj);
                    wrapping_business_object.Project = Project;
                    lstInfo.Objects.Add(wrapping_business_object);
                }
            }

        }
        static Dictionary<DbObjectType, Func<DbObject, SqlObject>> SqlObjectConstructorFor
        {
            get
            {
                if (_CreateSqlObjectFor == null)
                {
                    _CreateSqlObjectFor = new Dictionary<DbObjectType, Func<DbObject, SqlObject>>();

                    foreach (Type t in typeof(SqlObject).Assembly.GetTypes().Where(t => typeof(SqlObject).IsAssignableFrom(t) && !t.IsAbstract))
                    {
                        DbObjectType? key = SqlObject.GetRawType(t, true);
                        if (!key.HasValue)
                            continue;
                        var cons = t.GetConstructor(new[] { typeof(DbObject) });
                        Debug.Assert(cons != null);
                        Func<DbObject, SqlObject> value = null;
                        if (cons != null)
                            value = (DbObject raw_info) => (SqlObject)cons.Invoke(new object[] { raw_info });
                        _CreateSqlObjectFor.Add(key.Value, value);
                    }
                }
                return _CreateSqlObjectFor;
            }
        }
        static Dictionary<DbObjectType, Func<DbObject, SqlObject>> _CreateSqlObjectFor;


        // Tri les table afin que celle dont depends les autre est en premier
        // (ie: Ordre de creation des tables et de leur FK)
        public void SortTableByTopologicalOrder()
        {
            var tri = GetTableByTopologicalOrder(Tables);
            Tables.Clear();
            Tables.AddRange(tri);
        }
        public static List<Table> GetTableByTopologicalOrder(List<Table> tables)
        {
            Func<Table, IEnumerable<Table>> getReferencedTable = t => t.FkConstraints.Where(fkc => fkc.LinkType != eLinkType.BackwardLink)
                                    .SelectMany(fkc => fkc.ReferencedColumns)
                                    .Select(col => col.Owner)
                                    .Distinct()
                                    .ToList();
            Func<Table, string> getName = t => t.Name.Contains(".") ? t.Name.Substring(t.Name.LastIndexOf('.') + 1) : t.Name;

            // http://dl9obn.darc.de/programming/python/dottoxml/
            string nl = Environment.NewLine;
            string dot = "digraph ethane {" + nl;
            foreach (var t in tables)
                foreach (var n in getReferencedTable(t))
                    dot += getName(t) + " -> " + getName(n) + nl;
            dot += "}";

            //string dot = TopologicalOrderSimple.ToDot(tables, ,
            //    );
            var tri = TopologicalOrderSimple.tri_topo(tables, getReferencedTable).Reverse<Table>().ToList();

            Debug.Assert(tables.Count == tri.Count);
            return tri;
        }


    }
}
