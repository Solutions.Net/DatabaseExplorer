﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

using Smo = Microsoft.SqlServer.Management.Smo;


namespace DatabaseExplorer.DAL.Old
{
    public class Loader
    {
        public void LoadFromSmo(ConnectionSetting connection)
        {
            var conn = connection.ConnectionString;
            var srv = new Smo.Server();
            srv.ConnectionContext.ServerInstance = connection.Server;
            srv.ConnectionContext.LoginSecure = false;
            srv.ConnectionContext.Login = connection.UserId;
            srv.ConnectionContext.Password = connection.Password;

            var db = srv.Databases[connection.InitialCatalog];
            db.PrefetchObjects();
            var dbInfo = new DatabaseInfo(db, new SqlConnectionStringBuilder(connection.ConnectionString));
            var schemaByName = db.Tables.Cast<Smo.ScriptSchemaObjectBase>()
                       .Concat(db.Views.Cast<Smo.View>())
                       .Concat(db.Views.Cast<Smo.StoredProcedure>())
                       .GroupBy(o => o.Schema)
                       .ToDictionary(grp => grp.Key, grp => new Schema(null) { Name = grp.Key });
            dbInfo.Schemas.AddRange(schemaByName.Values);


            foreach (var tableBySchema in db.Tables.Cast<Smo.Table>().GroupBy(t => t.Schema))
            {
                var schema = schemaByName[tableBySchema.Key];
                foreach (var t in tableBySchema)
                {
                    var table = new Table(null)
                    {
                        Owner = schema,
                        Name = t.Name,
                        CreationDate = t.CreateDate,
                        ChangeDate = t.DateLastModified,
                        Columns = new List<Column>(),
                    };

                    var pk = new ConstraintPK() { Columns = new List<Column>() };
                    int ordinalPosition = 0;
                    foreach (var col in t.Columns.Cast<Smo.Column>())
                    {
                        var column = new Column(table, col.Name, col.DataType.Name, col.Nullable, col.Identity)
                        {
                            IsComputed = col.Computed,
                            IsFileStream = col.IsFileStream,
                            IsSparse = col.IsSparse,
                            IsColumnSet = col.IsColumnSet,
                            DefaultValue = col.Default,
                            OrdinalPosition = Convert.ToInt32(col.ExtendedProperties["OrdinalPosition"]?.Value ?? ordinalPosition),
                            //ArrayLength = col.ArrayLength,
                            //HasLengthLimit = col.HasLengthLimit,
                            IsAutoIncremented = col.IdentityIncrement != 0
                        };
                        ++ordinalPosition;
                        if (col.InPrimaryKey)
                            pk.Columns.Add(column);
                        table.Columns.Add(column);
                    }
                    table.Columns = table.Columns.OrderBy(c => c.OrdinalPosition).ToList();
                    if (pk.Columns.Count > 0)
                    {
                        pk.Columns = pk.Columns.OrderBy(c => c.OrdinalPosition).ToList();
                        table.Pk = pk;
                    }
                    schema.Tables.Add(table);
                }
            }
            var tablebySchemaByName = dbInfo.Schemas
                                            .SelectMany(s => s.Tables.SelectMany(t => t.Columns))
                                            .GroupByToDictionary3(c => c.Owner.Owner.Name, c => c.Owner.Name, c => c.Name, t => t.Single());
            foreach (var tableBySchema in db.Tables.Cast<Smo.Table>().GroupBy(t => t.Schema))
                foreach (var t in tableBySchema)
                    foreach (var fk in t.ForeignKeys.Cast<Smo.ForeignKey>())
                    {
                        var table = tablebySchemaByName[tableBySchema.Key][t.Name].First().Value.Owner;
                        table.FkConstraints = new List<ConstraintFK>();
                        table.FkConstraints.Add(new ConstraintFK()
                        {
                            LinkType = eLinkType.Unknown,
                            ForeignColumns = fk.Columns.Cast<Smo.ForeignKeyColumn>().Select(col => tablebySchemaByName[tableBySchema.Key][t.Name][col.Name]).ToArray(),
                            ReferencedColumns = fk.Columns.Cast<Smo.ForeignKeyColumn>().Select(col => tablebySchemaByName[fk.ReferencedTableSchema][fk.ReferencedTable][col.ReferencedColumn]).ToArray(),
                        });
                    }

            foreach (var viewBySchema in db.Views.Cast<Smo.View>().GroupBy(v => v.Schema))
            {
                var schema = schemaByName[viewBySchema.Key];
                foreach (var v in viewBySchema)
                {
                    var view = new View(null)
                    {
                        Owner = schema,
                        Name = v.Name,
                    };
                    view.Columns = v.Columns.Cast<Smo.Column>().Select((col, i) =>
                        new Column(view, col.Name, col.DataType.Name, col.Nullable, col.Identity)
                        {
                            IsComputed = col.Computed,
                            IsFileStream = col.IsFileStream,
                            IsSparse = col.IsSparse,
                            IsColumnSet = col.IsColumnSet,
                            DefaultValue = col.Default,
                            OrdinalPosition = Convert.ToInt32(col.ExtendedProperties["OrdinalPosition"]?.Value ?? i),
                            //ArrayLength = col.ArrayLength,
                            //HasLengthLimit = col.HasLengthLimit,
                            IsAutoIncremented = col.IdentityIncrement != 0
                        }).ToList();
                    schema.Views.Add(view);
                }
            }
        }
    }
}
