﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.SqlServer.Types;

using TechnicalTools;
using TechnicalTools.Model.Cache;


namespace DatabaseExplorer.DAL
{
    [DebuggerDisplay("{AsDebugString}")]
    public struct IdTupleWithSqlHierarchyId<T2> : IIdTuple, IEquatable<IdTupleWithSqlHierarchyId<T2>>
        where T2 : IEquatable<T2>
    {
        public IdTupleWithSqlHierarchyId(SqlHierarchyId hid, T2 id2)
        {
            _Id1 = hid;
            _Id2 = id2;
        }

        public SqlHierarchyId Id { get { return _Id1; } } readonly SqlHierarchyId   _Id1;
        public T2 Id2 { get { return _Id2; } } readonly T2   _Id2;

        public object[] Keys { get { return new object[] { _Id1, _Id2 }; } }
        public ITypedId ToTypedId(Type type) { return new TypedId<IdTupleWithSqlHierarchyId<T2>>(type, this); }
        private string AsDebugString { get { return "[" + Keys.Join() + "]"; } }

        #region Equality members

        public bool Equals(IdTupleWithSqlHierarchyId<T2> other)
        {
            return EqualityComparer<SqlHierarchyId>.Default.Equals(_Id1, other._Id1) &&
                   EqualityComparer<T2>.Default.Equals(_Id2, other._Id2);
        }
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            return obj is IdTupleWithSqlHierarchyId<T2> && Equals((IdTupleWithSqlHierarchyId<T2>)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = EqualityComparer<SqlHierarchyId>.Default.GetHashCode(_Id1);
                hashCode = (hashCode * 397) ^ EqualityComparer<T2>.Default.GetHashCode(_Id2);
                return hashCode;
            }
        }

        public static bool operator ==(IdTupleWithSqlHierarchyId<T2> left, IdTupleWithSqlHierarchyId<T2> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(IdTupleWithSqlHierarchyId<T2> left, IdTupleWithSqlHierarchyId<T2> right)
        {
            return !left.Equals(right);
        }
        #endregion
    }

}
