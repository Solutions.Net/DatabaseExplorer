﻿using System;
using System.Drawing;
using Microsoft.SqlServer.Management.Smo;
using TechnicalTools;


namespace DatabaseExplorer.DAL
{
    public class SqlType
    {
        public string CSTypeName       { get; private set; }
        public string VBTypeName       { get; private set; }
        public bool   IsStructType     { get; private set; }
        public string CSharp_SqlDbType { get; private set; } // Voir System.Data.SqlDbType
        public Type   CSharpType       { get; private set; } // Voir System.Data.SqlDbType

        public object MinValue         { get; private set; }
        public object MaxValue         { get; private set; }

        public Func<string, string> CastToSqlValue    { get; private set; }
        public Func<string, string> CastToCSharpValue { get; private set; }

        // TODO : ajouter : IsLikeString
        // TODO : ajouter : IsLikeDateTime
        // TODO : ajouter : IsLikeDecimal

        private SqlType(string cs_type_name, string vb_type_name, bool isStructType, string csharp_SqlDbType, Type csharp_type)
        {
            CSTypeName = cs_type_name;
            VBTypeName = vb_type_name;
            IsStructType = isStructType;
            CSharp_SqlDbType = csharp_SqlDbType;
            CSharpType = csharp_type;

            CastToCSharpValue = variable => "(" + cs_type_name + ")" + variable;
            CastToSqlValue = variable => variable;
        }

        public static readonly SqlType Text             = new SqlType("string",  "String",   false, "Text", typeof(string));
        public static readonly SqlType NChar            = new SqlType("string",  "String",   false, "NChar", typeof(string));
        public static readonly SqlType String           = new SqlType("string",  "String",   false, "NVarChar", typeof(string));
        
        public static readonly SqlType Int              = new SqlType("int",     "Integer",  true,  "Int", typeof(int));
        public static readonly SqlType Long             = new SqlType("long",    "Long",     true,  "BigInt", typeof(Int64));
        public static readonly SqlType Numeric          = new SqlType("decimal", "Decimal",  true,  "numeric", typeof(decimal));
        public static readonly SqlType Decimal          = new SqlType("decimal", "Decimal",  true,  "decimal", typeof(decimal));
        public static readonly SqlType Double           = new SqlType("double",  "Double",   true,  "Float", typeof(double));
        public static readonly SqlType Real             = new SqlType("Single",  "Single",   true,  "Real", typeof(Single));
        public static readonly SqlType Bool             = new SqlType("bool",    "Boolean",  true,  "Bit", typeof(bool));
        public static readonly SqlType Date             = new SqlType("DateTime","Date",     true,  "Date", typeof(DateTime))           { MinValue = new DateTime(1, 1, 1), MaxValue = new DateTime(9999, 12, 31) }; // Pas de precision en dessous du jour
        public static readonly SqlType DateTime         = new SqlType("DateTime","DateTime", true,  "DateTime", typeof(DateTime))       { MinValue = new DateTime(1753, 1, 1), MaxValue = new DateTime(9999, 12, 31, 23, 59, 59, 997) };  // precision a 3 ou 7 ms, default value sql : 1900-01-01 00:00:00,
        public static readonly SqlType DateTime2        = new SqlType("DateTime","DateTime", true,  "DateTime2", typeof(DateTime))      { MinValue = new DateTime(1, 1, 1), MaxValue = new DateTime(9999, 12, 31, 23, 59, 59, 999/*, 9999*/) }; // precison : 100 ns, default value sql : 1900-01-01 00:00:00,
        public static readonly SqlType DateTimeOffset   = new SqlType("DateTime","DateTime", true,  "DateTimeOffset", typeof(DateTime)) { MinValue = new DateTime(1, 1, 1), MaxValue = new DateTime(9999, 12, 31, 23, 59, 59, 999/*, 9999*/) }; // precision : variable
        public static readonly SqlType SmallDateTime    = new SqlType("DateTime","DateTime", true,  "SmallDateTime", typeof(DateTime))  { MinValue = new DateTime(1900, 1, 1), MaxValue = new DateTime(2079, 6, 6, 23, 59, 00) }; // precision : la minute
        public static readonly SqlType Time             = new SqlType("Time",    "TimeSpan", true,  "Time", typeof(TimeSpan)); // precision 100ns
        public static readonly SqlType VarBinary        = new SqlType("byte[]",  "Byte()",   false, "varbinary", typeof(byte[]));
        public static readonly SqlType Binary           = new SqlType("byte[]",  "Byte()",   false, "binary", typeof(byte[]));
        public static readonly SqlType TimeStamp        = new SqlType("byte[]",  "Byte()",   false, "timestamp", typeof(byte[])); // Longueur de 8 http://stackoverflow.com/questions/6334777/what-does-a-timestamp-in-t-sql-mean-in-c?lq=1
        public static readonly SqlType Money            = new SqlType("decimal", "Decimal",  true,  "money", typeof(decimal));
        public static readonly SqlType SmallMoney       = new SqlType("decimal", "Decimal",  true,  "SmallMoney", typeof(decimal));
        public static readonly SqlType SmallInt         = new SqlType("short",   "Short",    true,  "SmallInt", typeof(short));
        public static readonly SqlType TinyInt          = new SqlType("byte",    "Byte",     true,  "TinyInt", typeof(byte));
        public static readonly SqlType UniqueIdentifier = new SqlType("Guid",    "Guid",     true,  "UniqueIdentifier", typeof(Guid));
        public static readonly SqlType RowVersion       = new SqlType("byte[]",  "Byte()",   false, "RowVersion", typeof(byte[]));
        public static readonly SqlType Image            = new SqlType("byte[]",  "Byte()",   false, "Image", typeof(byte[]));
        public static readonly SqlType Xml              = new SqlType("System.Data.SqlTypes.SqlXml", "System.Data.SqlTypes.SqlXml", false, "XML", typeof(System.Data.SqlTypes.SqlXml));
        //public static readonly SqlType HierarchyId      = new SqlType("Microsoft.SqlServer.Types.SqlHierarchyId", "Microsoft.SqlServer.Types.SqlHierarchyId", true, "HierarchyId", typeof(Microsoft.SqlServer.Types.SqlHierarchyId));
        //public static readonly SqlType Geometry         = new SqlType("Microsoft.SqlServer.Types.SqlGeometry", "Microsoft.SqlServer.Types.SqlGeometry", false, "HierarchyId", typeof(Microsoft.SqlServer.Types.SqlGeometry));

        //public static bool ForSqlCompactEdition { get; set; }
        //public static readonly SqlType SqlCEString      = new SqlType("string",  "String",   false, "ntext",  typeof(string));
        //public static readonly SqlType SqlCETimeAslong  = new SqlType("long",    "Long",     true,  "BigInt", typeof(Int64)) { CastToCSharpValue = variable => "new TimeSpan(" + variable  +")", CastToSqlValue = variable => variable + ".Ticks" };
        //public static readonly SqlType SqlCEVarBinary   = new SqlType("byte[]",  "Byte()",   false, "image",  typeof(byte[]));

        public static SqlType FromSqlType(string sqlType)
        {
            sqlType = sqlType.ToLower();
            if (sqlType.Contains("varchar") || sqlType.Contains("nvarchar")) // works with nvarchar too
                return SqlType.String;
            if (sqlType.In("char", "nchar"))
                return SqlType.NChar;
            if (sqlType.Contains("text"))
                return SqlType.Text;
            // Voir http://social.msdn.microsoft.com/Forums/sqlserver/en-US/d496c385-e0d9-4b98-81a4-1181270ea030/sql-server-vs-c-data-types?forum=sqlexpress
            // http://stackoverflow.com/questions/425389/c-sharp-equivalent-of-sql-server-2005-datatypes
            // Tableau deja fait : http://stackoverflow.com/questions/425389/c-sharp-equivalent-of-sql-server-2005-datatypes/968734#968734
            // officiel (avec comparatif de version) http://msdn.microsoft.com/en-us/library/ms131092%28v=sql.110%29.aspx
            // Attention à l'ordre des tests
            return sqlType.Contains("smallint")         ? SqlType.SmallInt
                 : sqlType.Contains("tinyint")          ? SqlType.TinyInt 
                 : sqlType.Contains("bigint")           ? SqlType.Long
                 : sqlType.Contains("int")              ? SqlType.Int

                 : sqlType.Contains("datetime2")        ? SqlType.DateTime2
                 : sqlType.Contains("smalldatetime") ? SqlType.SmallDateTime
                 : sqlType.Contains("datetimeoffset") ? SqlType.DateTimeOffset
                 : sqlType.Contains("datetime")         ? SqlType.DateTime
                 : sqlType.Contains("date")             ? SqlType.Date
                 : sqlType.Contains("timestamp")        ? SqlType.TimeStamp
                 : sqlType.Contains("time")             ? SqlType.Time
                 
                 : sqlType.Contains("numeric")          ? SqlType.Numeric
                 : sqlType.Contains("decimal")          ? SqlType.Decimal
                 : sqlType.Contains("float")            ? SqlType.Double
                 : sqlType.Contains("real")             ? SqlType.Real
                 : sqlType.Contains("bit")              ? SqlType.Bool

                 : sqlType.Contains("varbinary")        ? SqlType.VarBinary
                 : sqlType.Contains("binary")           ? SqlType.Binary

                 : sqlType.Contains("smallmoney")       ? SqlType.Decimal
                 : sqlType.Contains("money")            ? SqlType.Money

                 : sqlType.Contains("uniqueidentifier") ? SqlType.UniqueIdentifier
                 : sqlType.Contains("rowversion")       ? SqlType.RowVersion
                 : sqlType.Contains("image")            ? SqlType.Image
                 : sqlType.Contains("xml")              ? SqlType.Xml
                 //: sqlType.Contains("geometry")         ? SqlType.Geometry
                 //: sqlType.Contains("hierarchyid")      ? SqlType.HierarchyId
                                                        : null;
        }
        
        public static SqlType FromCSharpType(Type type)
        {
            if (type == typeof(string))
                //if (ForSqlCompactEdition)
                //    return SqlType.SqlCEString;
                //else
                    return SqlType.String;
            else if (type == typeof(bool))
                return SqlType.Bool;
            else if (type == typeof(int) || type == typeof(uint))
                return SqlType.Int;
            else if (type == typeof(long) || type == typeof(ulong))
                return SqlType.Long;
            else if (type == typeof(short) || type == typeof(ushort))
                return SqlType.SmallInt;
            else if (type == typeof(sbyte) || type == typeof(byte))
                return SqlType.TinyInt;
            else if (type == typeof(DateTime))
                return SqlType.DateTime2;
            else if (type == typeof(TimeSpan))
                //if (ForSqlCompactEdition)
                //    return SqlType.SqlCETimeAslong;
                //else
                    return SqlType.Time;
            else if (type == typeof(double))
                return SqlType.Double;
            else if (type == typeof(decimal))
                return SqlType.Decimal;
            else if (type == typeof(Single))
                return SqlType.Real;
            else if (type == typeof(Guid))
                return SqlType.UniqueIdentifier;
            else if (type == typeof(byte[]))
                //if (ForSqlCompactEdition)
                //    return SqlType.SqlCEVarBinary;
                //else
                    return SqlType.VarBinary;
            else if (type == typeof(Color))
                return SqlType.Int;
            else if (type.IsEnum)
                return FromCSharpType(Enum.GetUnderlyingType(type));
            throw new Exception("Not Handled");
        }
       
        public string CSharp_DefaultValue(bool isNullable)
        {
            return isNullable || !IsStructType 
                 ? "null" 
                 : "default(" + ToCSharp(isNullable) + ")"; 
        }


        public string ToCSharp(bool isNullable)
        {
            return IsStructType && isNullable
                    ? CSTypeName + "?"
                    : CSTypeName;
        }

    }
}
