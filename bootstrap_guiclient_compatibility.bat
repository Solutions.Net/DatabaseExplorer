﻿@echo off
setlocal 
set nopause=0

:parsing_arg_loop
      :: Thanks to https://stackoverflow.com/a/34552964
      ::-------------------------- has argument ?
	  :: in %~1 - the ~ removes any wrapping " or '.
      if ["%~1"]==[""] (
        goto parsing_arg_end
      )
      ::-------------------------- argument exist ?
	  if ["%~1"]==["--nopause"] set nopause=1

      ::--------------------------
      shift
      goto parsing_arg_loop


:parsing_arg_end

@echo on
REM Gitkraken n'aime pas les jonctions (sur des chemins trop long seulement je pense)
REM Il pense que le dossier n'est pas un repository "compatible" et affiche un chargement infini à l'ouverture
REM On Supprime donc le dossier (lien)
rmdir /S /Q "lib\DevExpress"
REM Et on copie physiquement le dossier
robocopy "ApplicationBase\Dependencies\TechnicalTools.UI.DX\lib\DevExpress" "lib\DevExpress" /e

REM De même le dossier packages n'est pas crée tant que nuget n'a spas retélecharger
REM Donc si on veut ouvrir dans gitkraken avant Visual Studio...
mkdir packages 2>NUL

REM Script termine

@if ["%nopause%"] == ["0"] (
REM DONE! Press any key to leave this script
@pause
)
@endlocal
