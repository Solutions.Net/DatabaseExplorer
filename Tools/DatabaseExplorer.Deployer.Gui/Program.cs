﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using TechnicalTools.Diagnostics;

using ApplicationBase.Deployment;

using DatabaseExplorer.Common;


namespace DatabaseExplorer.Deployer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!DebugTools.IsForDevelopper) // To debug as if you are in prod, set this to false in VS' Immediate Window!
                Debug.Listeners.Clear(); // To not show assertion to user

            RuntimeHelpers.RunClassConstructor(typeof(ExceptionManager).TypeHandle);
            TechnicalTools.Logs.LogManager.Default = new TechnicalTools.Logs.LogManager();

            var bcfg = new BootstrapConfig(typeof(Program));
            BootstrapConfig.Instance = bcfg;
            Debug.Assert(bcfg.DeployerExecutableName.ToLowerInvariant() == Path.GetFileName(Application.ExecutablePath.ToLowerInvariant()));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var frm = new FrmDeployer(new Deployer(bcfg),
                                      new Filter { Domain = bcfg.Domain, EnvName = bcfg.EnvName });
            frm.InstallDone += () => frm.Close();
            Application.Run(frm);
        }
        class Deployer : Common.Deployer
        {
            protected internal Deployer(ApplicationBase.Deployment.Config cfg)
                : base(cfg)
            {
            }
        }
    }
}
