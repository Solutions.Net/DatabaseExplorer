﻿using System;

using SuperBase = ApplicationBase.Common;
using Base = ApplicationBase.Common;


namespace DatabaseExplorer.Common
{
    public class CommonFactory : Base.CommonFactory
    {
        public CommonFactory()
        {
        }

        public override SuperBase.Config CreateConnectionsConfig()
        {
            return new Config();
        }
    }
}
