﻿using System;
using System.Drawing;


namespace DatabaseExplorer.Common
{
    // This class / File is shared (by link) within project TMT Launcher, TMT Deployer and TMT
    // Each project using it must have a ico file named logoTMT.ico
    public class BootstrapConfig : ApplicationBase.Common.BootstrapConfig
    {
        // Please See "Note 01" in file "ApplicationBase.Common/Code Design Notes.txt"
        public new static BootstrapConfig Instance
        {
            get { return _Instance; }
            set
            {
                _Instance = value;
                if (ApplicationBase.Common.BootstrapConfig.Instance == null ||
                    value != null && ApplicationBase.Common.BootstrapConfig.Instance.GetType().IsAssignableFrom(value.GetType()))
                    ApplicationBase.Common.BootstrapConfig.Instance = value;
            }
        }
        static BootstrapConfig _Instance;

        public override string DefaultInitConnectionString { get { return @"Data Source=SERVER\SQLEXPRESS;Initial Catalog=Base;User Id=app_dbe_anonymous;Password=anonymous;Application Name=DatabaseExplorer;"; } }

        public BootstrapConfig(Type programType, string logoFileName = "logo.ico")
            : this(GetIconFromCallingAssembly(programType, logoFileName))
        {
        }
        protected BootstrapConfig(Icon icon = null)
            : base(icon)
        {
            InitialConnectionString = DefaultInitConnectionString;
            //CliExecutableName = "DatabaseExplorer.CLI.exe";
            GuiExecutableName = "DatabaseExplorer.exe";
            LauncherExecutableName = "DatabaseExplorer Launcher.exe";
            DeployerExecutableName = "DatabaseExplorer Deployer.exe";
            CompanyName = "MyCompany";
            ApplicationName = "Database Explorer";
            ApplicationIcon = icon;
            LauncherRelativePathToBinFromMainProjectOutputFolder = @"..\..\..\Tools\DatabaseExplorer.Launcher.Gui\bin";
        }
    }
}
