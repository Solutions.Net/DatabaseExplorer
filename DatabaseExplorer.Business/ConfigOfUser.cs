﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;

using TechnicalTools;

using ApplicationBase.Business;


namespace DatabaseExplorer.Business
{
    // To add a new user setting, just Add public property with attribute  
    // Category , DisplayName, Description and DefaultValue
    public class ConfigOfUser : ApplicationBase.Business.ConfigOfUser
    {
        // To add an editable property by user
        [Category("Display")]
        [DisplayName("Connection String Display")]
        [Description("Indicate how connection must be displayed in treeview.\r\n" +
                     "You can use the following pattern to customize the display:\r\n" +
                     "  %d : Database/catalog name.\r\n" +
                     "  %s : Server Name.\r\n" +
                     "  %u : User Id / Login.\r\n" +
                     DescriptionFooter)]
        [DefaultValue(Display_ConnectionDisplayPattern_Default)]
        public string Display_ConnectionDisplayPattern
        {
            get { return _Display_ConnectionDisplayPattern; }
            set
            {
                ConnectionToString = BuildConnectionDisplay(value);
                SetUserSetting(ref _Display_ConnectionDisplayPattern, value);
            }
        }
        string _Display_ConnectionDisplayPattern;
        public const string Display_ConnectionDisplayPattern_Default = "%s/%d";
        public Func<SqlConnectionStringBuilder, string> ConnectionToString { get; private set; }
        Func<SqlConnectionStringBuilder, string> BuildConnectionDisplay(string pattern)
        {
            return BuildPartnerEnvironmentDisplay(pattern.IfBlankUse(Display_ConnectionDisplayPattern_Default), _mappings);
        }
        readonly Dictionary<char, Func<SqlConnectionStringBuilder, string>> _mappings = new Dictionary<char, Func<SqlConnectionStringBuilder, string>>
            {
                { 'd', csb => csb.InitialCatalog},
                { 's', csb => csb.DataSource },
                { 'u', pe => pe.UserID },
            };

        protected internal ConfigOfUser(AuthenticationManager manager, StorageTraits traits = null)
            : base(manager, traits)
        {
        }
        protected ConfigOfUser(StorageTraits traits = null)
            : base(traits)
        { }

        protected override ApplicationBase.Business.ConfigOfUser CreateNewInstance(StorageTraits traits)
        {
            return new ConfigOfUser(traits);
        }
        protected override void CopyFrom(ApplicationBase.Business.ConfigOfUser from)
        {
            base.CopyFrom(from);
            var source = (ConfigOfUser)from;
            Display_ConnectionDisplayPattern = source.Display_ConnectionDisplayPattern;
        }
    }
}
