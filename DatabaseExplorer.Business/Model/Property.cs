﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Model;

using DatabaseExplorer.DAL;


namespace DatabaseExplorer.Business.Model
{
    [DebuggerDisplay("{AsDebugString,nq}")]
    public class Property : PropertyNotifierObject
    {
        public EntityClassOrStruct Owner       { get { return _Owner; } internal set { SetTechnicalProperty(ref _Owner, value);} } EntityClassOrStruct _Owner;
                                               
        public string              Name        { get { return _Name; } set { SetProperty(ref _Name, value, onChanging: OnNameChanging); } } string _Name;
                                               
        public PropertyType        Type        { get { return _Type; } set { SetProperty(ref _Type, value); } } PropertyType _Type;
                                               
        public bool                Nullable    { get { return _Nullable; } set { SetProperty(ref _Nullable, value); } } bool _Nullable;
        public bool                IsComposite { get; set; }
        public bool                IsAggregate { get { return !IsComposite; } }

        public eAccessibility   IsGetterPublic { get { return _IsGetterPublic; } set { SetProperty(ref _IsGetterPublic, value); } } eAccessibility _IsGetterPublic;
        public eAccessibility   IsSetterPublic { get { return _IsSetterPublic; } set { SetProperty(ref _IsSetterPublic, value); } } eAccessibility _IsSetterPublic;

        public eLinkType LinkType { get; set; }

        public string AsDebugString
        {
            get
            {
                var maxAccessibility = IsGetterPublic.MostAccessible(IsSetterPublic);
                return maxAccessibility.GetDescription()  + " "
                    + Type.AsCodeType + " " + Name 
                    + " { " 
                    + (maxAccessibility == IsGetterPublic ? "" : IsGetterPublic.GetDescription() + " ") + "get; " 
                    + (maxAccessibility == IsSetterPublic ? "" : IsSetterPublic.GetDescription() + " ")  + "set; " 
                    + "}";
            }
        }
        public Property(string name)
        {
            Name = name;
            Type = PropertyTypeNative.Int32;
        }
        public Property(XElement node, EntityClassOrStruct owner)
        {
            FromXml(node, owner);
        }

        void OnNameChanging(string newValue)
        {
            if (string.IsNullOrWhiteSpace(newValue))
                throw new Exception("La propriété doit avoir un nom");
        }

        #region Import / Export

        public void FromXml(XElement nProperty, EntityClassOrStruct owner)
        {
            Name = nProperty.Attribute("Name").Value;
            string typeName = nProperty.Attribute("Type").Value;
            Type = owner.Owner.AllTypes.First(t => t.AsCodeType == typeName);
            var att = nProperty.Attribute("Nullable");
            if (att != null)
                Nullable = bool.Parse(att.Value);
            IsGetterPublic = (eAccessibility)Enum.Parse(typeof(eAccessibility), nProperty.Attribute("IsGetterPublic").Value);
            IsSetterPublic = (eAccessibility)Enum.Parse(typeof(eAccessibility), nProperty.Attribute("IsSetterPublic").Value);
            att = nProperty.Attribute("LinkType");
            if (att != null)
                LinkType = (eLinkType)Enum.Parse(typeof(eLinkType), att.Value, true);
        }
        public XElement ToXml(XElement nProperty = null)
        {
            nProperty = nProperty ?? new XElement("Property");
            nProperty.Add(new XAttribute("Name", Name));
            nProperty.Add(new XAttribute("Type", Type.AsCodeType));
            if (Nullable)
                nProperty.Add(new XAttribute("Nullable", Nullable));
            nProperty.Add(new XAttribute("IsGetterPublic", IsGetterPublic.ToString()));
            nProperty.Add(new XAttribute("IsSetterPublic", IsSetterPublic.ToString()));
            nProperty.Add(new XAttribute("LinkType", LinkType));
            return nProperty;
        }

        #endregion Import / Export
    }
}
