﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;

using TechnicalTools;


namespace DatabaseExplorer.Business.Model
{
    [DebuggerDisplay("Name={Name}")]
    public abstract class PropertyType
    {
        public abstract string Name       { get; }
        public virtual  string AsCodeType { get { return Name; } }
        public            bool IsNative   { get; protected set;  }
        public    PropertyType ItSelf     { get { return this; } }
    }
    public abstract class PropertyTypeScalar : PropertyType
    {
    }

    [DebuggerDisplay("Name={Name}, Type={Type}")]
    public class PropertyTypeNative : PropertyTypeScalar
    {
        public            Type Type       { get; private set; }
        public override string Name       { get { return _Name; } } string _Name;
        public override string AsCodeType { get { return Type.ToSmartString(); } }

        public static readonly PropertyTypeNative Void          = new PropertyTypeNative() { _Name = "",              IsNative = true, Type = typeof(void) };
        public static readonly PropertyTypeNative Boolean       = new PropertyTypeNative() { _Name = "Boolean",       IsNative = true, Type = typeof(bool) };
        public static readonly PropertyTypeNative String        = new PropertyTypeNative() { _Name = "String",        IsNative = true, Type = typeof(string) };
        public static readonly PropertyTypeNative Int08         = new PropertyTypeNative() { _Name = "Int08",         IsNative = true, Type = typeof(byte) };
        public static readonly PropertyTypeNative Int08Unsigned = new PropertyTypeNative() { _Name = "Int08Unsigned", IsNative = true, Type = typeof(sbyte) };
        public static readonly PropertyTypeNative Int16         = new PropertyTypeNative() { _Name = "Int16",         IsNative = true, Type = typeof(short) };
        public static readonly PropertyTypeNative Int16Unsigned = new PropertyTypeNative() { _Name = "Int16Unsigned", IsNative = true, Type = typeof(ushort) };
        public static readonly PropertyTypeNative Int32         = new PropertyTypeNative() { _Name = "Int32",         IsNative = true, Type = typeof(int) };
        public static readonly PropertyTypeNative Int32Unsigned = new PropertyTypeNative() { _Name = "Int32Unsigned", IsNative = true, Type = typeof(uint) };
        public static readonly PropertyTypeNative Int64         = new PropertyTypeNative() { _Name = "Int64",         IsNative = true, Type = typeof(long) };
        public static readonly PropertyTypeNative Int64Unsigned = new PropertyTypeNative() { _Name = "Int64Unsigned", IsNative = true, Type = typeof(ulong) };
        public static readonly PropertyTypeNative Time          = new PropertyTypeNative() { _Name = "Time",          IsNative = true, Type = typeof(TimeSpan) };
        public static readonly PropertyTypeNative DateTime      = new PropertyTypeNative() { _Name = "DateTime",      IsNative = true, Type = typeof(DateTime) };
        public static readonly PropertyTypeNative Date          = new PropertyTypeNative() { _Name = "Date",          IsNative = true, Type = typeof(DateTime) };
        public static readonly PropertyTypeNative Color         = new PropertyTypeNative() { _Name = "Color",         IsNative = true, Type = typeof(Color) };

        public static readonly PropertyTypeNative ByteArray     = new PropertyTypeNative() { _Name = "ArrayByte",     Type = typeof(byte[]) };

        public static IEnumerable<PropertyTypeNative> AllTypes
        {
            get
            {
                return new List<PropertyTypeNative>() {
                    Void,
                    Boolean,
                    String,
                    Int08,
                    Int08Unsigned,
                    Int16,
                    Int16Unsigned,
                    Int32,
                    Int32Unsigned,
                    Int64,
                    Int64Unsigned,
                    Time,
                    DateTime,
                    Date,
                    ByteArray
                };
            }
        }

        internal static PropertyTypeNative FromCSharpType(Type type)
        {
            return AllTypes.First(t => t.Type == type);
        }
    }

    [DebuggerDisplay("PropertyTypeEntity Name={Name}, Type={Type}")]
    public class PropertyTypeEntity : PropertyTypeScalar
    {
        public         IEntity Type { get; private set; }
        public override string Name { get { return Type.Name; } }

        public PropertyTypeEntity(IEntity type)
        {
            Type = type;
        }
    }

    [DebuggerDisplay("PropertyTypeCollection Name={Name}, Type={Type}")]
    public class PropertyTypeCollection : PropertyType
    {
        public PropertyTypeScalar    Type { get; private set; }
        public override string       Name { get { return "List<" + Type.Name + ">"; } }
        public override string AsCodeType { get { return "List<" + Type.AsCodeType + ">"; } }
        
        private PropertyTypeCollection(PropertyTypeNative type)
        {
            Type = type;
        }

        public PropertyTypeCollection(PropertyTypeEntity type)
        {
            Type = type;
        }

        public static readonly PropertyTypeCollection ListOfBoolean       = new PropertyTypeCollection(PropertyTypeNative.Boolean)       { IsNative = true };
        public static readonly PropertyTypeCollection ListOfString        = new PropertyTypeCollection(PropertyTypeNative.String)        { IsNative = true };
        public static readonly PropertyTypeCollection ListOfInt08         = new PropertyTypeCollection(PropertyTypeNative.Int08)         { IsNative = true };
        public static readonly PropertyTypeCollection ListOfInt08Unsigned = new PropertyTypeCollection(PropertyTypeNative.Int08Unsigned) { IsNative = true };
        public static readonly PropertyTypeCollection ListOfInt16         = new PropertyTypeCollection(PropertyTypeNative.Int16)         { IsNative = true };
        public static readonly PropertyTypeCollection ListOfInt16Unsigned = new PropertyTypeCollection(PropertyTypeNative.Int16Unsigned) { IsNative = true };
        public static readonly PropertyTypeCollection ListOfInt32         = new PropertyTypeCollection(PropertyTypeNative.Int32)         { IsNative = true };
        public static readonly PropertyTypeCollection ListOfInt32Unsigned = new PropertyTypeCollection(PropertyTypeNative.Int32Unsigned) { IsNative = true };
        public static readonly PropertyTypeCollection ListOfInt64         = new PropertyTypeCollection(PropertyTypeNative.Int64)         { IsNative = true };
        public static readonly PropertyTypeCollection ListOfInt64Unsigned = new PropertyTypeCollection(PropertyTypeNative.Int64Unsigned) { IsNative = true };
        public static readonly PropertyTypeCollection ListOfTime          = new PropertyTypeCollection(PropertyTypeNative.Time)          { IsNative = true };
        public static readonly PropertyTypeCollection ListOfDateTime      = new PropertyTypeCollection(PropertyTypeNative.DateTime)      { IsNative = true };
        public static readonly PropertyTypeCollection ListOfDate          = new PropertyTypeCollection(PropertyTypeNative.Date)          { IsNative = true };

        public static IEnumerable<PropertyTypeCollection> AllTypes
        {
            get
            {
                return new List<PropertyTypeCollection>() {
                    ListOfBoolean,
                    ListOfString,
                    ListOfInt08,
                    ListOfInt08Unsigned,
                    ListOfInt16,
                    ListOfInt16Unsigned,
                    ListOfInt32,
                    ListOfInt32Unsigned,
                    ListOfInt64,
                    ListOfInt64Unsigned,
                    ListOfTime,
                    ListOfDateTime,
                    ListOfDate
                };
            }
        }
 
    }

    [DebuggerDisplay("PropertyTypeDictionary Name={Name}, KeyType={KeyType}, ValueType={ValueType}")]
    public class PropertyTypeDictionary : PropertyType
    {
        public PropertyTypeScalar KeyType { get; private set; }
        public PropertyTypeScalar ValueType { get; private set; }
        public override string Name { get { return "Dictionary<" + KeyType.Name + ", " + ValueType.Name + ">"; } }
        public override string AsCodeType { get { return "Dictionary<" + KeyType.AsCodeType + ", " + ValueType.AsCodeType + ">"; } }

        public PropertyTypeDictionary(PropertyTypeScalar keyType, PropertyTypeScalar valueType)
        {
            KeyType = keyType;
            ValueType = valueType;
        }
    }

}
