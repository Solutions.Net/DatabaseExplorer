﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools.Model;


namespace DatabaseExplorer.Business.Model
{
    public class EntityEnum : PropertyNotifierObject, IEntityWithInternal
    {
        public ModelUML                 Owner     { get { return _Owner;        } internal set { SetTechnicalProperty(ref _Owner, value); } } ModelUML _Owner;
               ModelUML IInternalAccess.Owner     { get { return Owner;         } set { Owner = value; } }

        public string                   Name      { get { return _Name;         }          set { SetProperty(ref _Name,        value, onChanging: OnNameChanging); } } string _Name;
        public string                   Namespace { get { return _Namespace;    }          set { SetProperty(ref _Namespace,   value); } } string _Namespace;

        public PropertyTypeNative       BaseType  { get { return _EnumBaseType; }          set { SetProperty(ref _EnumBaseType, value); } } PropertyTypeNative _EnumBaseType;
        public readonly CheckableBindingList<EnumValue> EnumValues = new CheckableBindingList<EnumValue>();

        private EntityEnum() 
        {
            EnumValues.ItemAdding += OnEnumAdding;
            EnumValues.ItemAdded += OnEnumAdded;
            EnumValues.ItemRemoved += OnEnumRemoved;
        }

        public EntityEnum(string name, string @namespace)
            : this()
        {
            Name = name;
            Namespace = @namespace;
        }
        public EntityEnum(XElement node, ModelUML owner)
            : this()
        {
            FromXml(node, owner);
        }

        void OnNameChanging(string newValue)
        {
            if (string.IsNullOrWhiteSpace(newValue))
                throw new Exception("L'enum doit avoir un nom");
        }




        void OnEnumAdding(object sender, CheckableBindingList<EnumValue>.ItemAddingEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(e.Item.Name))
                throw new Exception("La valeur de l'enum ajoutée doit avoir un nom");
            CheckName(e.Item, e.Item.Name);
        }
        void CheckName(EnumValue p, string name)
        {
            if (EnumValues.Any(prop => prop.Name == name && prop != p))
                throw new Exception("Une valeur de l'enum a déjà ce nom !");
        }

        void OnEnumAdded(object sender, CheckableBindingList<EnumValue>.ItemAddedEventArgs e)
        {
            e.Item.Owner = this;
            e.Item.PropertyChangingFor(entity => entity.Name, EnumNameChanging);
        }

        void OnEnumRemoved(object sender, CheckableBindingList<EnumValue>.ItemRemovedEventArgs e)
        {
            e.Item.PropertyChangingFor_Remove(entity => entity.Name, EnumNameChanging);
            e.Item.Owner = null;
        }
        private void EnumNameChanging(object sender, TechnicalTools.Model.PropertyChangingEventArgs e)
        {
            CheckName(sender as EnumValue, (string)e.NewValue);
        }

        #region Import / Export
        
        public void FromXml(XElement nEntity, ModelUML owner)
        {
            //Properties.Clear();
            //Name = nEntity.Attribute("Name").Value;
            //var att = nEntity.Attribute("Base");
            //Base = att == null ? Entity.None : owner.Entities.First(e => e.Name == att.Value);
            //foreach (var nProperty in nEntity.Nodes().OfType<XElement>().Where(n => n.Name == "Property"))
            //    Properties.Add(new Property(nProperty, this));
        }
        public XElement ToXml(XElement nEntity = null)
        {
            //nEntity = nEntity ?? new XElement("Entity");

            //nEntity.Add(new XAttribute("Name", Name));
            //if (Base != Entity.None)
            //    nEntity.Add(new XAttribute("Base", Base.Name));
            //foreach (var p in Properties)
            //    nEntity.Add(p.ToXml(new XElement("Property")));
            return nEntity;
        }

        #endregion Import / Export
        
    }

    [DebuggerDisplay("{AsDebugString,nq}")]
    public class EnumValue : PropertyNotifierObject
    {
        public EntityEnum Owner { get { return _Owner; } internal set { SetTechnicalProperty(ref _Owner, value); } } EntityEnum _Owner;

        public string Name { get { return _Name; } set { SetProperty(ref _Name, value); } } string _Name;
        public ulong Value { get { return _Value; } set { SetProperty(ref _Value, value); } } ulong _Value;
        private string AsDebugString { get { return Name + ": " + Convert.ToString((long)Value, 2); } }

        public EnumValue(string name, ulong value)
        {
            Name = name;
            Value = value;
        }
    }
}
