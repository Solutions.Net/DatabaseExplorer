﻿using System;
using System.ComponentModel;
using System.Reflection;


namespace DatabaseExplorer.Business.Model
{
    [Flags]
    public enum eAccessibility
    {
        [Description("")]
        None = 0,
        [Description("private")]
        Private = 1,
        [Description("internal")]
        Internal = 2,
        [Description("protected")]
        Protected = 4,
        [Description("protected internal")]
        Protected_Internal = Protected | Internal,
        [Description("public")]
        Public = 8
    }


    public static class eAccessibility_Extensions
    {
        public static eAccessibility MostAccessible(this eAccessibility a, eAccessibility b)
        {
            return (eAccessibility)Math.Max((int)a, (int)b);
        }
        public static eAccessibility? GetGetterAccessibility(this PropertyInfo pi)
        {
            var mi = pi.GetGetMethod(true);
            return GetAccessibility(mi);
        }
        public static eAccessibility? GetSetterAccessibility(this PropertyInfo pi)
        {
            var mi = pi.GetSetMethod(true);
            return GetAccessibility(mi);
        }

        static eAccessibility? GetAccessibility(MethodInfo mi)
        {
            if (mi == null)
                return null;
            if (mi.IsPrivate)
                return eAccessibility.Private;
            if (mi.IsPublic)
                return eAccessibility.Public;
            if (mi.IsFamilyOrAssembly)
                return eAccessibility.Protected_Internal;
            if (mi.IsFamily)
                return eAccessibility.Protected;
            if (mi.IsAssembly)
                return eAccessibility.Internal;
            throw new Exception("Unreachable!");
        }
    }
}
