﻿using System;
using System.Xml.Linq;

using TechnicalTools.Model;


namespace DatabaseExplorer.Business.Model
{
    public class EntityParameter : PropertyNotifierObject, IAbstractEntityWithInternal
    {
        public ModelUML Owner          { get { return _Owner; } internal set { SetTechnicalProperty(ref _Owner, value); } } ModelUML _Owner;
        ModelUML IInternalAccess.Owner { get { return Owner; } set { Owner = value; } }
                                                                          
        public string  Name            { get { return _Name;  }          set { SetProperty(ref _Name,           value, onChanging: OnNameChanging); } } string _Name;

        public EntityParameter(string name)
        {
            Name = name;
        }
        public EntityParameter(XElement node, ModelUML owner)
        {
            FromXml(node, owner);
        }

        void OnNameChanging(string newValue)
        {
            if (string.IsNullOrWhiteSpace(newValue))
                throw new Exception("L'entité parametre doit avoir un nom");
        }

        #region Import / Export
        
        public void FromXml(XElement nEntity, ModelUML owner)
        {
            // TODO
            //Properties.Clear();
            //Name = nEntity.Attribute("Name").Value;
            //var att = nEntity.Attribute("Base");
            //Base = att == null ? EntityClass.None : owner.Entities.First(e => e.Name == att.Value);
            //foreach (var nProperty in nEntity.Nodes().OfType<XElement>().Where(n => n.Name == "Property"))
            //    Properties.Add(new Property(nProperty, this));
        }
        public XElement ToXml(XElement nEntity = null)
        {
            // TODO
            //nEntity = nEntity ?? new XElement("Entity");

            //nEntity.Add(new XAttribute("Name", Name));
            //if (Base != EntityClass.None)
            //    nEntity.Add(new XAttribute("Base", Base.Name));
            //foreach (var p in Properties)
            //    nEntity.Add(p.ToXml(new XElement("Property")));
            return nEntity;
        }

        #endregion Import / Export
        
    }

}
