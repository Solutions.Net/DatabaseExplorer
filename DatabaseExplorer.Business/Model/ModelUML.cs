﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Model;


namespace DatabaseExplorer.Business.Model
{
    public class ModelUML : PropertyNotifierObject
    {
        public string Name { get { return _Name; } set { SetProperty(ref _Name, value, onChanging: OnNameChanging); } } string _Name;

        public readonly CheckableBindingList<IEntity>      Entities = new CheckableBindingList<IEntity>();

        public readonly CheckableBindingList<PropertyType> AllTypes = new CheckableBindingList<PropertyType>();

        private ModelUML()
        {
            Entities.ItemAdding += OnEntityAdding;
            Entities.ItemAdded += OnEntityAdded;
            Entities.ItemRemoved += OnEntityRemoved;
        }
        public ModelUML(string name)
            : this()
        {
            Name = name;
            AllTypes.AddRange(PropertyTypeNative.AllTypes.OrderBy(t => t.Name));
            AllTypes.AddRange(PropertyTypeCollection.AllTypes.OrderBy(t => t.Name));
        }

        void OnNameChanging(string newValue)
        {
            if (string.IsNullOrWhiteSpace(newValue))
                throw new Exception("Le model doit avoir un nom");
        }

        void OnEntityAdding(object sender, CheckableBindingList<IEntity>.ItemAddingEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(e.Item.Name))
                throw new Exception("L'entité ajoutée doit avoir un nom");
            CheckName(e.Item, e.Item.Name);
        }
        void CheckName(IEntity e, string name)
        {
            if (Entities.Any(entity => entity.Name == name && entity != e))
                throw new Exception("Une entité du même nom existe déjà");
        }
        void OnEntityAdded(object sender, CheckableBindingList<IEntity>.ItemAddedEventArgs e)
        {
            (e.Item as IInternalAccess).Owner = this;
            e.Item.PropertyChangingFor(entity => entity.Name, EntityNameChanging);
            if (_importing)
                return;
            var pte = new PropertyTypeEntity(e.Item);
            int index = AllTypes.FindIndex(t => t is PropertyTypeCollection || t is PropertyTypeEntity && !(t as PropertyTypeEntity).IsNative && (t as PropertyTypeEntity).Name.CompareTo(pte.Name) > 0);
            if (index == -1)
                AllTypes.Add(pte);
            else
                AllTypes.Insert(index, pte);
            var cle = new PropertyTypeCollection(pte);
            index = AllTypes.FindIndex(t => t is PropertyTypeCollection && !(t as PropertyTypeCollection).IsNative && (t as PropertyTypeCollection).Name.CompareTo(cle.Name) > 0);
            if (index == -1)
                AllTypes.Add(cle);
            else
                AllTypes.Insert(index, cle);
        }

        void OnEntityRemoved(object sender, CheckableBindingList<IEntity>.ItemRemovedEventArgs e)
        {
            var pte = AllTypes.OfType<PropertyTypeEntity>().First(entity => entity.Type == e.Item);
            e.Item.PropertyChangingFor_Remove(entity => entity.Name, EntityNameChanging);
            (e.Item as IInternalAccess).Owner = null;
            AllTypes.Remove(AllTypes.OfType<PropertyTypeCollection>().First(entity => entity.Type == pte));
            AllTypes.Remove(pte);
        }
    
        void EntityNameChanging(object sender, TechnicalTools.Model.PropertyChangingEventArgs e)
        {
            CheckName(sender as EntityClass, (string)e.NewValue);
        }

        public List<string> Validate()
        {
            var errors = new List<string>();
            foreach (var e in Entities.OfType<EntityClass>())
                foreach (var p in e.Properties)
                    if (p.Type == PropertyTypeNative.Void)
                        errors.Add(string.Format("Property {0} of Entity {1} has no type defined!", p.Name, e.Name));
            return errors;
        }

        void CopyFrom(ModelUML source)
        {
            Name = source.Name;
            Entities.Clear();
            AllTypes.Clear();
            AllTypes.AddRange(source.AllTypes);
            Entities.AddRange(source.Entities);
        }
        #region Import / Export

        public XElement SaveToXml(XElement nModel = null)
        {
            nModel = nModel ?? new XElement("Model");
            nModel.Add(new XAttribute("Name", Name));

            foreach (var e in Entities)
                nModel.Add(e.ToXml(new XElement("Entity")));
            return nModel;
        }

        public void LoadFromXml(XElement nModel)
        {
            _importing = true;
            try
            {
                Debug.Assert(nModel != null);
            
                var model = new ModelUML(nModel.Attribute("Name").Value);
                var nEntities = nModel.Nodes().OfType<XElement>().Where(n => n.Name == "Entity").ToList();
                foreach (var nEntity in nEntities)
                    model.Entities.Add(new EntityClass(nEntity.Attribute("Name").Value));
                foreach (var nEntity in nEntities)
                {
                    var entity = model.Entities.FirstOrDefault(e => e.Name == nEntity.Attribute("Name").Value);
                    entity.FromXml(nEntity, model);
                }
                    
                CopyFrom(model);
            }
            finally
            {
                _importing = false;
            }
        }
        bool _importing;

        #endregion Import / Export
    }


}
