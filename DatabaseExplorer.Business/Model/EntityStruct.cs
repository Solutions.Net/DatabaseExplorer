﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;
using System.Linq;

using TechnicalTools;


namespace DatabaseExplorer.Business.Model
{
    [DebuggerDisplay("{AsDebugString,nq}")]
    public class EntityStruct : EntityClassOrStruct
    {
        public int  SizeOfStruct    { get { return _SizeOfStruct; } set { SetProperty(ref _SizeOfStruct, value); } } int  _SizeOfStruct;

        public readonly Dictionary<Property, int> PropertyOffsets = new Dictionary<Property, int>();

        public EntityStruct(string name)
            : base(name)
        {
        }
        public EntityStruct(XElement node, ModelUML owner)
            : base(node, owner)
        {
        }

        private string AsDebugString
        {
            get
            {
                return "EntityStruct " + Name + (Interfaces.Count > 0 ? ", " : "") + Interfaces.Join()
                    + " { "
                    + Properties.Select(p => p.Type.AsCodeType + " " + p.Name).Join()
                    + " } ";
            }
        }

        #region Import / Export

        public override void FromXml(XElement nEntity, ModelUML owner)
        {
            if (Debugger.IsAttached)
                Debugger.Break();
            base.FromXml(nEntity, owner);
            // TODO
            //Properties.Clear();
            //Name = nEntity.Attribute("Name").Value;
            //var att = nEntity.Attribute("Base");
            //Base = att == null ? EntityClass.None : owner.Entities.First(e => e.Name == att.Value);
            //foreach (var nProperty in nEntity.Nodes().OfType<XElement>().Where(n => n.Name == "Property"))
            //    Properties.Add(new Property(nProperty, this));
        }
        public override XElement ToXml(XElement nEntity = null)
        {
            if (Debugger.IsAttached)
                Debugger.Break();
            return base.ToXml(nEntity);
            // TODO
            //nEntity = nEntity ?? new XElement("Entity");

            //nEntity.Add(new XAttribute("Name", Name));
            //if (Base != EntityClass.None)
            //    nEntity.Add(new XAttribute("Base", Base.Name));
            //foreach (var p in Properties)
            //    nEntity.Add(p.ToXml(new XElement("Property")));
            //return nEntity;
        }

        #endregion Import / Export
        
    }
}
