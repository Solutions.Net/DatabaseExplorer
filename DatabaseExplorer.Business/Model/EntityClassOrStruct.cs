﻿using System;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Annotations.Design;
using TechnicalTools.Model;


namespace DatabaseExplorer.Business.Model
{
    public abstract class EntityClassOrStruct : PropertyNotifierObject, IEntityWithInternal
    {
        public ModelUML                 Owner { get { return _Owner; } internal set { SetTechnicalProperty(ref _Owner, value); } } ModelUML _Owner;
               ModelUML IInternalAccess.Owner { get { return Owner; } set { Owner = value; } }
                                                                          
        public string                   Name  { get { return _Name;  }          set { SetProperty(ref _Name,           value, onChanging: OnNameChanging); } } protected string _Name;
        public readonly CheckableBindingList<IAbstractEntity> BaseGenericArguments = new CheckableBindingList<IAbstractEntity>();
        
        public readonly CheckableBindingList<string> Interfaces = new CheckableBindingList<string>();
        public readonly CheckableBindingList<WhereParamImplements> GenericConstraints = new CheckableBindingList<WhereParamImplements>();

        public readonly CheckableBindingList<Property> Properties = new CheckableBindingList<Property>();

        public EntityClassOrStruct ItSelf { get { return this; } }
        
        //public string InheritancePath { get { return Base == null ? "" : Base.InheritancePath + "-->" + Name; } }
        //public bool IsAncestorOf(Entity entity)
        //{
        //    var e = this;
        //    while (e != null && e != entity)
        //        e = e.Base;
        //    return e != null;
        //}

        

        protected EntityClassOrStruct() 
        {
            Properties.ItemAdding += OnPropertyAdding;
            Properties.ItemAdded += OnPropertyAdded;
            Properties.ItemRemoved += OnPropertyRemoved;
        }
        public EntityClassOrStruct(string name)
            : this()
        {
            Name = name;
        }
        public EntityClassOrStruct(XElement node, ModelUML owner)
            : this()
        {
            FromXml(node, owner);
        }

        void OnNameChanging(string newValue)
        {
            if (string.IsNullOrWhiteSpace(newValue))
                throw new Exception("L'entité doit avoir un nom");
        }
        void OnPropertyAdding(object sender, CheckableBindingList<Property>.ItemAddingEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(e.Item.Name))
                throw new Exception("La propriété ajoutée doit avoir un nom");
            CheckName(e.Item, e.Item.Name);
        }
        void CheckName(Property p, string name)
        {
            if (Properties.Any(prop => prop.Name == name && prop != p))
                throw new Exception("Une propriété du même nom existe déjà");
        }
        void OnPropertyAdded(object sender, CheckableBindingList<Property>.ItemAddedEventArgs e)
        {
            e.Item.Owner = this;
            e.Item.PropertyChangingFor(entity => entity.Name, PropertyNameChanging);
        }
        void OnPropertyRemoved(object sender, CheckableBindingList<Property>.ItemRemovedEventArgs e)
        {
            e.Item.PropertyChangingFor_Remove(entity => entity.Name, PropertyNameChanging);
            e.Item.Owner = null;
        }

        private void PropertyNameChanging(object sender, TechnicalTools.Model.PropertyChangingEventArgs e)
        {
            CheckName(sender as Property, (string)e.NewValue);
        }


        #region Import / Export
        
        public virtual void FromXml(XElement nEntity, ModelUML owner)
        {
            Properties.Clear();
            Name = nEntity.Attribute("Name").Value;
            foreach (var nProperty in nEntity.Nodes().OfType<XElement>().Where(n => n.Name == "Property"))
                Properties.Add(new Property(nProperty, this));
        }
        public virtual XElement ToXml(XElement nEntity = null)
        {
            nEntity = nEntity ?? new XElement("Entity");

            nEntity.Add(new XAttribute("Name", Name));
            foreach (var p in Properties)
                nEntity.Add(p.ToXml(new XElement("Property")));
            return nEntity;
        }

        #endregion Import / Export
    }
}
