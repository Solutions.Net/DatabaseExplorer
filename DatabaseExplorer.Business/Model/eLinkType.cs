﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseExplorer.Business.Model
{
    public enum eLinkType
    {
        Unknown = 0,      // We can assume by default the type is ForwardLink
        ForwardLink = 1,  // Un objet a besoin d'un autre objet (composition ou aggregation)
        BackwardLink = 2, // Exemple : un objet reference son Owner.
    }
}
