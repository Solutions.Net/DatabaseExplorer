﻿using System;
using System.Diagnostics;
using System.Xml.Linq;

using TechnicalTools.Model;


namespace DatabaseExplorer.Business.Model
{
    [DebuggerDisplay("{AsDebugString,nq}")]
    public class EntityInterface : PropertyNotifierObject, IEntityWithInternal
    {
        public ModelUML                 Owner    { get { return _Owner; } internal set { SetTechnicalProperty(ref _Owner, value); } } ModelUML _Owner;
               ModelUML IInternalAccess.Owner    { get { return Owner; } set { Owner = value; } }

        public string                   Name     { get { return _Name;     } set { SetProperty(ref _Name,     value, onChanging: OnNameChanging); } } string _Name;
        public string                   FullName { get { return _FullName; } set { SetProperty(ref _FullName, value, onChanging: OnNameChanging); } } string _FullName;

        public readonly CheckableBindingList<EntityClass> ImplementingEntities = new CheckableBindingList<EntityClass>();

        // Indique si l'interface est defini dans le model (et dans ce cas on devra generer du code) ou a l'exterieur (rien à générer)
        public bool IsInModeling { get { return _IsInModeling; } set { SetProperty(ref _IsInModeling, value); } } bool _IsInModeling;

        private EntityInterface() 
        {
            ImplementingEntities.ItemAdding += OnImplementingEntitiesAdding;
        }

        public EntityInterface(string name)
            : this()
        {
            Name = name;
        }
        public EntityInterface(XElement node, ModelUML owner)
            : this()
        {
            FromXml(node, owner);
        }

        void OnNameChanging(string newValue)
        {
            if (string.IsNullOrWhiteSpace(newValue))
                throw new Exception("L'interface doit avoir un nom");
        }

        void OnImplementingEntitiesAdding(object sender, CheckableBindingList<EntityClass>.ItemAddingEventArgs e)
        {
              if (ImplementingEntities.Contains(e.Item))
                  throw new Exception("Doublon non permis !");
        }

        private string AsDebugString
        {
            get
            {
                return "EntityInterface " + Name;
            }
        }

        #region Import / Export

        public void FromXml(XElement nEntity, ModelUML owner)
        {
            //Properties.Clear();
            //Name = nEntity.Attribute("Name").Value;
            //var att = nEntity.Attribute("Base");
            //Base = att == null ? Entity.None : owner.Entities.First(e => e.Name == att.Value);
            //foreach (var nProperty in nEntity.Nodes().OfType<XElement>().Where(n => n.Name == "Property"))
            //    Properties.Add(new Property(nProperty, this));
        }
        public XElement ToXml(XElement nEntity = null)
        {
            //nEntity = nEntity ?? new XElement("Entity");

            //nEntity.Add(new XAttribute("Name", Name));
            //if (Base != Entity.None)
            //    nEntity.Add(new XAttribute("Base", Base.Name));
            //foreach (var p in Properties)
            //    nEntity.Add(p.ToXml(new XElement("Property")));
            return nEntity;
        }

        #endregion Import / Export

    }

}
