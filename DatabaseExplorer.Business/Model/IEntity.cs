﻿using System.Xml.Linq;

using TechnicalTools.Model;


namespace DatabaseExplorer.Business.Model
{
    public interface IAbstractEntity : IPropertyNotifierObject
    {
        ModelUML Owner { get; }
        string Name { get; }
        void FromXml(XElement nEntity, ModelUML owner);
        XElement ToXml(XElement nEntity = null);
    }
    public interface IEntity : IAbstractEntity
    {
    }

    internal interface IInternalAccess
    {
        ModelUML Owner { get; set; }
    }

    interface IAbstractEntityWithInternal : IAbstractEntity, IInternalAccess
    {
    }
    internal interface IEntityWithInternal : IEntity, IInternalAccess
    {
    }

}
