﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Model;


namespace DatabaseExplorer.Business.Model
{
    [DebuggerDisplay("{AsDebugString,nq}")]
    public class EntityClass : EntityClassOrStruct
    {
        public bool               IsAbstract  { get; set; }
        public EntityClass              Base  { get { return _Base;  }          set { SetProperty(ref _Base,           value, onChanging: OnBaseChanging); } } EntityClass _Base = EntityClass.None;
        public static readonly EntityClass None = new EntityClass() { _Name = "" };

        public readonly List<EntityClass> ChildClasses = new List<EntityClass>();
        public IEnumerable<EntityClass> GetConcreteChildClasses() 
        {
            // Ajoute this en dernier comme ca on laisse le temps aux algos qui utilise GetConcreteChildClasses
            // de faire des tests avec les classes specialisées avant de tester avec le type le plus generique.
            return ChildClasses.SelectMany(e => e.GetConcreteChildClasses())
                               .Concat(IsAbstract ? new EntityClass[0] : new[] { this }); 
        }
        

        private EntityClass()
        {
        }
        public EntityClass(string name)
            : base(name)
        {
        }
        public EntityClass(XElement node, ModelUML owner)
            : base(node, owner)
        {
        }

        void OnBaseChanging(EntityClass newValue)
        {
            if (newValue == null)
                throw new Exception("Base value cannot be null, it must be Entity.None!");
            var value = newValue;
            while (value != None)
                if (value == this)
                    throw new Exception("Entity inheritance cannot be recursive!");
                else
                    value = value.Base;
            // Modification acceptée
            if (Base != None)
                Base.ChildClasses.Remove(this);
            if (newValue != None)
                newValue.ChildClasses.Add(this);
        }

        private string AsDebugString
        {
            get
            {
                return "EntityClass " + Name + (Base == EntityClass.None ? "" : " : " + Base.Name)
                                             + (Interfaces.Count > 0 ? ", " : "") + Interfaces.Join()
                    + " { "
                    + Properties.Select(p => p.Type.AsCodeType + " " + p.Name).Join()
                    + " } ";
            }
        }

        public bool IsA(EntityClass ancestor)
        {
            var b = this;
            do
            {
                if (b == ancestor)
                    return true;
                b = b.Base;
            } while (b != EntityClass.None);
            return false;
        }
        public bool Implements(string interfaceName)
        {
            var b = this;
            do
            {
                // TODO : Si les interfaces peuvent aussi avoir de l'héritage il faudra plutot faire une methode IsAssignableFrom
                if (b.Interfaces.Any(i => i == interfaceName))
                    return true;
                b = b.Base;
            } while (b != EntityClass.None);
            return false;
        }

        public override void FromXml(XElement nEntity, ModelUML owner)
        {
            base.FromXml(nEntity, owner);
            var att = nEntity.Attribute("Base");
            Base = att == null ? EntityClass.None : owner.Entities.OfType<EntityClass>().First(e => e.Name == att.Value);
        }

        public override XElement ToXml(XElement nEntity = null)
        {
            nEntity = base.ToXml(nEntity);
            if (Base != EntityClass.None)
                nEntity.Add(new XAttribute("Base", Base.Name));
            return nEntity;
        }

    }

}
