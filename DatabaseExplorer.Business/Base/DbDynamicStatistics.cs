﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using TechnicalTools;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public class DbDynamicStatistics
    {
        public static DbDynamicStatistics GetStatisticsFor(GenericGeneratedDAO dao)
        {
            return _statistics.GetOrAdd(dao, key =>
            {
                var tmp = new DbDynamicStatistics();
                tmp.ForceInit(dao);
                return tmp;
            });
        }
        static readonly ConcurrentDictionary<GenericGeneratedDAO, DbDynamicStatistics> _statistics = new ConcurrentDictionary<GenericGeneratedDAO, DbDynamicStatistics>();

        Dictionary<string, long> RowCounts;

        public long GetRowCountFor(GenericGeneratedDAO dao, string tablename, string schema = null)
        {
            ForceInit(dao);
            return RowCounts[new[] { schema, tablename }.NotBlank().Join(".")];
        }
        public void ForceInit(GenericGeneratedDAO dao)
        {
            if (RowCounts == null)
                RowCounts = GetCountsOfAllTables(dao);
        }
        Dictionary<string, long> GetCountsOfAllTables(GenericGeneratedDAO dao)
        {
            var dt = dao.WithConnection(con =>
            {
                if (con is GenericDBConnectionSqlCE)
                    return con.GetDatatable(DbDynamicStatisticsNew.rqGetTableRowCountForSqlCe);
                return con.GetDatatable(DbDynamicStatisticsNew.rqGetTableRowCountForSql);
            });
            return dt.AsEnumerable().ToDictionary(row => new[] { row["TABLE_SCHEMA"] == DBNull.Value ? null : (string)row["TABLE_SCHEMA"],
                                                                 (string)row["TABLE_NAME"] }
                                                        .NotBlank().Join("."),
                                                  row => (long)row["CARDINALITY"]);
        }
    }
}
