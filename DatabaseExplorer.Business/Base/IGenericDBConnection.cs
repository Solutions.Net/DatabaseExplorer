﻿using System;
using System.Data;
using System.Data.Common;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public interface IGenericDBConnection : IDisposable
    {
        DataTable GetDatatable(string sql, bool withSchema = false);
        T? ExecuteScalar<T>(string sql, T? defaultValue = null) where T : struct;
        object ExecuteScalar(string sql, object defaultValue = null);
        void ExecuteNonQuery(string sql);
        void ReadDataWith(string sql, Action<DbDataReader> readWith);

        GenericDBTransaction StartTransaction();

        GenericDBTransaction CurrentActiveTransaction { get; }

        string ConnectionString { get; }
    }
}
