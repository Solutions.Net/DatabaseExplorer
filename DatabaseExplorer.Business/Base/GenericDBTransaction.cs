﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Globalization;

using TechnicalTools.Diagnostics;
using TechnicalTools.Tools.Threading;

using DataMapper;
using TechnicalTools.Logs;

namespace DatabaseExplorer.Business.BaseDALClass
{
    public class GenericDBTransaction : IDbTransaction
    {
        public bool IsRealTransaction
        {
            get { return _cn != null; }
        }

        public bool IsOpen { get; private set; } // Indique si la transaction (reelle ou fictive) est ouverte/ Dans ce cas il faut appeller soit Commit, Rollback ou Dispose pour la fermer

        readonly IGenericDBConnection     _cn;                         // Lien vers la connection utilisée pour faire la transaction
        readonly GenericDBTransaction     _outerTransaction;           // Transaction immediatement superieur (englobante)
        GenericDBTransaction              _innerRollbackedTransaction; // Transaction interieur, origine du rollback de la transaction courante
        int?                           _transaction_id;             // Permet d'identifier la transaction courante
        static int                     _transactionIdSeed;          // Permet de générer l'id
        Action                         _realCommit;
        Action                         _realRollback;

        public event Action OnCommit
        {
            add
            {
                Debug.Assert(_outerTransaction != null || IsRealTransaction, "You can't rely on this event because the current transaction is fictive!");
                if (_outerTransaction == null)
                    OnRealCommit += value;
                else
                    _outerTransaction.OnCommit += value;
            }
            remove
            {
                if (_outerTransaction == null)
                    OnRealCommit -= value;
                else
                    _outerTransaction.OnCommit -= value;
            }
        }
        event Action OnRealCommit;

        public GenericDBTransaction GetRealTransaction()
        {
            return IsRealTransaction ? this : _outerTransaction.GetRealTransaction();
        }

        public event Action OnRollback
        {
            add
            {
                Debug.Assert(_outerTransaction != null || IsRealTransaction, "You can't rely on this event because the current transaction is fictive!");
                if (_outerTransaction == null)
                    OnRealRollback += value;
                else
                    _outerTransaction.OnRollback += value;
            }
            remove
            {
                if (_outerTransaction == null)
                    OnRealRollback -= value;
                else
                    _outerTransaction.OnRollback -= value;
            }
        }
        event Action OnRealRollback;

        public static GenericDBTransaction CreateRealTransaction(IGenericDBConnection cn, Action realCommit, Action realRollback) 
        { 
            var tran = new GenericDBTransaction(cn, null);
            tran._realCommit = realCommit;
            tran._realRollback = realRollback;
            return tran;
        }
        public static GenericDBTransaction CreateFictiveInnerTransaction(GenericDBTransaction outerParent) { return new GenericDBTransaction(null, outerParent); }

        private GenericDBTransaction(IGenericDBConnection cn, GenericDBTransaction outerTransaction)
        {
            _cn = cn;
            _outerTransaction = outerTransaction;
#if SUPER_DEBUG
            _CreationStackTrace = new StackTrace(1, true);
        }
        readonly StackTrace _CreationStackTrace;
#else
        }
#endif

        public void Begin()
        {
            if (_transaction_id.HasValue)
                DebugTools.Break("Ne pas recycler les transactions SVP !");
            _transaction_id = System.Threading.Interlocked.Increment(ref _transactionIdSeed);
            IsOpen = true;
            Log("Ouverture d'une transaction " + (IsRealTransaction ? "RÉELLE" : "fictive"));
            if (IsRealTransaction)
            {
                CurrentMainTransactions[CurrentThread] = this;
                //_messageHolder = AppQueue.CreatePostponedActionHolder(new[] { trkQueueDomain.ErrorMsg, trkQueueDomain.InfoMsg });
                //_otherHolder = AppQueue.CreatePostponedActionHolder(Enum.GetValues(typeof(trkQueueDomain)).Cast<trkQueueDomain>().Except(_messageHolder.LockedDomains));
            }
        }

        public static GenericDBTransaction CurrentMainTransaction
        {
            get
            {
                GenericDBTransaction tran;
                CurrentMainTransactions.TryGetValue(CurrentThread, out tran);
                return tran;
            }
        }

        // Avant on utilisat nos Thread, pas ceux du framework
        // Mais ca plante si on execute destest depuis visual studio qui cree SES thread
        // TODO (?) stocker l'interface _Thread dans le dico et adapté pour que les deux fonctionne ?
        static readonly ConcurrentDictionary<System.Threading.Thread, GenericDBTransaction> CurrentMainTransactions = new ConcurrentDictionary<System.Threading.Thread, GenericDBTransaction>();
        static System.Threading.Thread CurrentThread { get { return System.Threading.Thread.CurrentThread; } }

        //static readonly ConcurrentDictionary<Thread, GenericDBTransaction> CurrentMainTransactions = new ConcurrentDictionary<Thread, GenericDBTransaction>();
        //static Thread CurrentThread { get { return Thread.CurrentThread; } }


        public void Commit()
        {
            Debug.Assert(IsOpen, "Ce Commit n'est d'aucune utilisé ! Car la transaction est deja fermée !");
            Debug.Assert(_innerRollbackedTransaction == null, "Probleme : Une inner transaction a été rollbacké, mais sa parent (outer) tente de commiter..." +
                                                              "C'est impossible à realiser dans le logiciel actuellement !! Tout doit être rollbacké");

            Log("Commit d'une transaction " + (IsRealTransaction ? "RÉELLE" : "fictive"));
            if (IsRealTransaction)
            {
                GenericDBTransaction dummy;
                CurrentMainTransactions.TryRemove(CurrentThread, out dummy);
                Debug.Assert(dummy == this);

                _realCommit();
            }

            IsOpen = false;

            // On ne doit declencher l'event que lorsque le VRAI commit a eu lieu
            if (IsRealTransaction)
            {
                //_messageHolder.Dispose();
                //_otherHolder.Dispose();
                if (OnRealCommit != null)
                    OnRealCommit();
            }
        }



        public void RollBack()
        {
            Debug.Assert(IsOpen, "Ce RollBack n'est d'aucune utilité ! Car la transaction est deja fermée !");

            Log("RollBack d'une transaction " + (IsRealTransaction ? "RÉELLE" : "fictive"));
            if (IsRealTransaction)
            {
                _realRollback();
                GenericDBTransaction dummy;
                CurrentMainTransactions.TryRemove(CurrentThread, out dummy);
                Debug.Assert(dummy == this);
            }

            IsOpen = false;
            if (_outerTransaction != null)
                _outerTransaction._innerRollbackedTransaction = this;

            if (IsRealTransaction)
            {
                //_messageHolder.Dispose();
                //AppQueue.Cancel();
                //_otherHolder.Dispose();
            }
            // On peut déclencher le rollback même si ce n'est pas la vrai transaction
            if (OnRealRollback != null)
                OnRealRollback();
        }

        public void Dispose()
        {
            if (IsOpen)
                RollBack();
        }

        [Conditional("SUPER_DEBUG")]
        void Log(string msg)
        {
            _log.Info(string.Format("Thread \"{0}\" (Task {1}) (tran:" + (_transaction_id.HasValue ? _transaction_id.Value.ToString(CultureInfo.InvariantCulture) : "/") + ") : {2}",
                      Thread.CurrentThread.Name, Thread.CurrentThread.TaskId, msg));
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(GenericDBTransaction));

#if SUPER_DEBUG
        ~GenericDBTransaction()
        {
            if (IsOpen)
                // Try Catch parce que quand l'application se ferme, le GC peut decider de supprimer une partie des données privées gérées par les lignes de code ci-dessous 
                // Avant d'executer ce finalizer
                try
                {
                    var msg = String.Format("An instance of {0} was not disposed correctly, stack trace at creation is:{1}", GetType().FullName, _CreationStackTrace);
                    DebugTools.Break("To be investigated on why instance was not disposed correctly. No exception is thrown for the moment.");
                }
                catch
                {
                    // Rien !
                }
        }
#endif
    }

}
