﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using TechnicalTools.Model;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public partial class CheckableCompositeListGenerated<TItem> : CompositeList<TItem>
    {
        public bool         HasBeenLoaded   { get; private set; }
        //public event Action OnReload;
        public Func<IEnumerable<TItem>> LoadItems { get; set; }

        public CheckableCompositeListGenerated()
        {
        }

        public void Assign(IEnumerable<TItem> values)
        {
            Clear();
            AddRange(values);
        }
        public void RemoveAll(Func<TItem, bool> predicate)
        {
            for (int i = Count - 1; i >= 0; --i)
                if (predicate(this[i]))
                    RemoveAt(i);
        }

        public override void Reset()
        {
            HasBeenLoaded = false;
            base.Reset();
        }

        public void Reload()
        {
            if (LoadItems != null)
            {
                LockWhileUpdatingWith(DoReload);
                UpdateState(eState.Synchronized);
            }
        }
        private void DoReload()
        {
            Reset(); // set State to New (if not called inside LockWhileUpdatingWith)
            AddRange(LoadItems());
            HasBeenLoaded = true;
            UpdateState(eState.Synchronized);
        }
        
        public void ResetEdits()
        {
            LockWhileUpdatingWith(() =>
            {
                Reset();
                Reload();
            });
            InitializeState(eState.Synchronized);
        }


        #region From CheckableBindingList

        [DebuggerHidden, DebuggerStepThrough]
        protected override void InsertItem(int index, TItem item)
        {
            var h = ItemAdding;
            var h2 = ItemAdded;
            ItemAddedEventArgs e = null;
            if (h != null || h2 != null)
                e = new ItemAddedEventArgs(item, index);
            if (h != null)
                h(this, e);

            base.InsertItem(index, item);
            
            if (h2 != null)
                h2(this, e);
        }
        public event EventHandler<ItemAddingEventArgs>   ItemAdding;
        public event EventHandler<ItemAddedEventArgs>   ItemAdded;
        public class ItemAddingEventArgs : EventArgs
        {
            public TItem Item { get { return _Item; } } readonly TItem   _Item;
            public int Index { get { return _Index; } } readonly int _Index;

            [DebuggerHidden, DebuggerStepThrough]
            public ItemAddingEventArgs(TItem item, int index)
            {
                _Item = item;
                _Index = index;
            }
        }
        public class ItemAddedEventArgs : ItemAddingEventArgs
        {
            [DebuggerHidden, DebuggerStepThrough]
            public ItemAddedEventArgs(TItem item, int index)
                : base(item, index)
            {
            }
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected override void RemoveItem(int index)
        {
            var h = ItemRemoving;
            var h2 = ItemRemoved;
            ItemRemovedEventArgs e = null;
            if (h != null || h2 != null)
                e = new ItemRemovedEventArgs(this[index], index);
            if (h != null)
                h(this, e);
            base.RemoveItem(index);
            if (h2 != null)
                h2(this, e);
        }
        public event EventHandler<ItemRemovingEventArgs> ItemRemoving;
        public event EventHandler<ItemRemovedEventArgs> ItemRemoved;
        public class ItemRemovingEventArgs : EventArgs
        {
            public TItem   Item  { get { return _Item;  } } readonly TItem   _Item;
            public int Index { get { return _Index; } } readonly int _Index;

            [DebuggerHidden, DebuggerStepThrough]
            public ItemRemovingEventArgs(TItem item, int index)
            {
                _Item = item;
                _Index = index;
            }
        }
        public class ItemRemovedEventArgs : ItemRemovingEventArgs
        {
            [DebuggerHidden, DebuggerStepThrough]
            public ItemRemovedEventArgs(TItem item, int index)
                : base(item, index)
            {
            }
        }

        [DebuggerHidden, DebuggerStepThrough]
        protected override void ClearItems()
        {
            var h = Clearing;
            if (h != null)
                h(this, EventArgs.Empty);
            base.ClearItems();
            var h2 = Cleared;
            if (h2 != null)
                h2(this, EventArgs.Empty);
        }
        public event EventHandler Clearing;
        public event EventHandler Cleared;

        #endregion From CheckableBindingList
    }
}
