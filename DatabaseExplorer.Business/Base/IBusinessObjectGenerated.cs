﻿using System;

using TechnicalTools.Model;



namespace DatabaseExplorer.Business.BaseDALClass
{
    public interface IBusinessObjectGenerated : IPropertyNotifierObject
    {
        long Id { get; }

        void ResetDataSourceEdits();
        void SaveDataSourceEdits();
    }
}
