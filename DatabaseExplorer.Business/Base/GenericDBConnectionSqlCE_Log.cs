﻿using System;
using System.Collections.Concurrent;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using TechnicalTools;
using TechnicalTools.Model;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public partial class GenericDBConnectionSqlCE : IDisposable
    {
        public delegate void RequestBeginEventHandler(object sender, QueryLog log);
        public delegate void RequestEndEventHandler(object sender, QueryLog log);
        protected void RaiseRequestBegin(QueryLog log)
        {
            if (RequestBegin != null)
                RequestBegin(this, log);
        }
        protected void RaiseRequestEnd(QueryLog log)
        {
            if (RequestEnd != null)
                RequestEnd(this, log);
        }
        public static event    RequestBeginEventHandler RequestBegin;
        public static event    RequestEndEventHandler   RequestEnd;
       

        protected T LogQuery<T>(string query, eDbRequestType type, Func<T> doRequest)
        {
            var log = QueryLog.Create();
            log.RequestType = type;
            log.Query = query;
            RaiseRequestBegin(log);
            T res = default(T);
            try
            {
                log.StartUTC = DateTime.UtcNow;
                res = doRequest();
                log.EndUTC = DateTime.UtcNow;
                log.Result = res;
                return res;
            }
            catch (Exception ex)
            {
                log.EndUTC = DateTime.UtcNow;                
                log.Exception = ex;
                throw;
            }
            finally
            {
                RaiseRequestEnd(log);
                log.Dispose();
            }
        }
    }
}
