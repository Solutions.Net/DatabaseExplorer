﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlServerCe;

using TechnicalTools;
using TechnicalTools.Model;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public partial class GenericDBConnectionSqlCE : IGenericDBConnection
    {
        protected string _connectionString;
        protected SqlCeConnection _con;

        public string ConnectionString { get { return _con.ConnectionString; } }
        public SqlCeConnection Cn { get { return _con; } }

        public GenericDBConnectionSqlCE(string connectionString)
        {
            _connectionString = connectionString;
            _con = CreationSqlConnection();
        }

        protected virtual SqlCeConnection CreationSqlConnection()
        {
            return new SqlCeConnection(_connectionString);
        }
        
        public void Dispose()
        {
            if (_con != null)
                _con.Dispose();
        }

        public virtual void EnsureIsOpen()
        {
            if (_con == null)
                _con = new SqlCeConnection(_connectionString);
            if (_con.State == ConnectionState.Broken)
            {
                _con.Dispose();
                _con = null;
                _con = new SqlCeConnection(_connectionString);
            }
            if (_con.State != ConnectionState.Open)
                //try
                //{
                    _con.Open();
                //}
                //catch (SqlException ex)
                //{
                //    if (ex.Message.Contains(Login))
                //        throw new FunctionalException(ex.Message.Replace(login, _userLogin), ex);
                //    throw;
                //}
        }

        public DataTable GetDatatable(string sql, bool withSchema = false)
        {
            EnsureIsOpen();

            var dt = new DataTable();
            using (SqlCeDataAdapter da = new SqlCeDataAdapter(sql, _con))
            {
                da.SelectCommand.Transaction = _PhysicalTransaction;
                // da.SelectCommand.CommandTimeout = 120; //Default is 30 (in seconds)
                if (withSchema)
                    HandleRetry(da.SelectCommand, () => LogQuery(sql, eDbRequestType.GetDatatable, () => da.FillSchema(dt, SchemaType.Mapped)));
                HandleRetry(da.SelectCommand, () => LogQuery(sql, eDbRequestType.GetDatatable, () => { da.Fill(dt); return dt; }));
                return dt;
            }
        }

        public T? ExecuteScalar<T>(string sql, T? defaultValue = null)
            where T : struct
        {
            EnsureIsOpen();

            using (var cmd = _con.CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.Transaction = _PhysicalTransaction;

                var returnValue = HandleRetry(cmd, () => LogQuery(sql, eDbRequestType.ExecuteScalar, cmd.ExecuteScalar));
                if (returnValue != null)
                    return (T)returnValue;

                return defaultValue;
            }
        }
        public object ExecuteScalar(string sql, object defaultValue = null)
        {
            EnsureIsOpen();

            using (var cmd = _con.CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.Transaction = _PhysicalTransaction;

                var returnValue = HandleRetry(cmd, () => LogQuery(sql, eDbRequestType.ExecuteScalar, cmd.ExecuteScalar));
                if (returnValue != null)
                    return returnValue;

                return defaultValue;
            }
        }

        public void ExecuteNonQuery(string sql)
        {
            EnsureIsOpen();

            using (var cmd = _con.CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.Transaction = _PhysicalTransaction;
                HandleRetry(cmd, () => LogQuery(sql, eDbRequestType.ExecuteNonQuery, cmd.ExecuteNonQuery));
            }
        }

        void IGenericDBConnection.ReadDataWith(string sql, Action<DbDataReader> readWith)
        {
            ReadDataWith(sql, dbReader => readWith(dbReader));
        }
        public void ReadDataWith(string sql, Action<SqlCeDataReader> readWith)
        {
            EnsureIsOpen();

            using (var cmd = _con.CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.Transaction = _PhysicalTransaction;
                HandleRetry(cmd, () => LogQuery(sql, eDbRequestType.ReadDataWith, () =>
                {
                    using (var reader = cmd.ExecuteReader())
                        readWith(reader);
                    return (object)null;
                }));
            }
        }

        #region Gestion Deconnexion & Retry

        public static event EventHandler<PropertyChangingEventArgs> OnDisconnect;
        public static event EventHandler<PropertyChangingEventArgs> OnTimeOut;

        const int MaxTry = 3;

        protected T HandleRetry<T>(SqlCeCommand cmd, Func<T> process)
        {
            int remainingTries = MaxTry;
            //cmd.CommandTimeout = 30; // inutile c'est deja la valeur par defaut
            while (true)
            {
                try
                {
                    return process();
                }
                catch (Exception ex)
                {
                    if (--remainingTries == 0)
                    {
                        ex.EnrichDiagnosticWith("Executing SQL :" + Environment.NewLine + cmd.CommandText, eExceptionEnrichmentType.Technical);
                        throw;
                    }

                    var sqlex = ex as SqlException;
                    if (sqlex == null)
                    {
                        ex.EnrichDiagnosticWith("Executing SQL :" + Environment.NewLine + cmd.CommandText, eExceptionEnrichmentType.Technical);
                        throw;
                    }

                    int? newTimeOut = null;
                    if (sqlex.Number == -2) // TimeOut
                        newTimeOut = ReconnectAfterTimeOut() ? cmd.CommandTimeout * 10 : (int?)null;
                    else if (sqlex.Number == -1 | sqlex.Number == 1236) // Perte de connexion
                        newTimeOut = ReconnectAfterLostConnection() ? cmd.CommandTimeout : (int?)null;
                    if (!newTimeOut.HasValue)
                    {
                        ex.EnrichDiagnosticWith("Executing SQL :" + Environment.NewLine + cmd.CommandText, eExceptionEnrichmentType.Technical);
                        throw;
                    }

                    cmd.CommandTimeout = newTimeOut.Value;


                    if (cmd.Connection.State != ConnectionState.Open)
                        cmd.Connection.Open();
                }
            }
        }

        bool ReconnectAfterLostConnection()
        {
            var e = new PropertyChangingEventArgs("", null, null) { Cancel = true };
            if (OnDisconnect != null)
                OnDisconnect(this, e);
            return !e.Cancel;
        }

        bool ReconnectAfterTimeOut()
        {
            var e = new PropertyChangingEventArgs("", null, null) { Cancel = true };
            if (OnTimeOut != null)
                OnTimeOut(this, e);
            return !e.Cancel;
        }


        #endregion

        #region Transaction management

        // Start a new transaction
        public GenericDBTransaction StartTransaction()
        {
            lock (_ActiveTransactionLock)
            {
                if (_ActiveTransaction == null)
                {
                    EnsureIsOpen();

                    _PhysicalTransaction = _con.BeginTransaction();
                    _ActiveTransaction = GenericDBTransaction.CreateRealTransaction(this,
                        realCommit: () =>
                        {
                            _PhysicalTransaction.Commit();
                            _PhysicalTransaction = null;
                            _ActiveTransaction = null;
                        },
                        realRollback: () =>
                        {
                            _PhysicalTransaction.Rollback();
                            _PhysicalTransaction = null;
                            _ActiveTransaction = null;
                        });
                    _ActiveTransaction.Begin();
                    return _ActiveTransaction;
                }
                else
                {
                    var dbt = GenericDBTransaction.CreateFictiveInnerTransaction(_ActiveTransaction);
                    dbt.Begin();
                    return dbt;
                }
            }
        }
        private readonly object _ActiveTransactionLock = new object();
        private GenericDBTransaction _ActiveTransaction;
        public  GenericDBTransaction CurrentActiveTransaction { get { return _ActiveTransaction; } }
        private SqlCeTransaction _PhysicalTransaction;


        #endregion Transaction management

    }
}
