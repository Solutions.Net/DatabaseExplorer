﻿using System;
using System.Diagnostics;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public abstract partial class GenericGeneratedDAO
    {
        protected internal class GenericDALIdManager
        {
            ulong _next_id;
            ulong _next_limit;
            GenericGeneratedDAO _dao;

            readonly string _idManagerTableName;
            readonly string _seedOfIdsColumnName;

            public GenericDALIdManager(GenericGeneratedDAO dao, string idManagerTableName, string seedOfIdsColumnName)
            {
                _dao = dao;
                _idManagerTableName = idManagerTableName;
                _seedOfIdsColumnName = seedOfIdsColumnName;
            }

            public ulong CreateUniqueId()
            {
                const int reserve = 10000;
                lock (_lock)
                {
                    if (_next_id == _next_limit)
                    {
                        // snapshot isolation level et read committed etc : http://blog.sqlauthority.com/2015/07/03/sql-server-difference-between-read-committed-snapshot-and-snapshot-isolation-level/

                        // La table "TechnicalSettings" n'a pas de schema (ou bien est dans "dbo" par defaut) 
                        // car On doit pouvoir générer plusieur DAL (assembly) pour accéder aux même donnés
                        // Ces assemblies doivent donc partager la même table TechnicalSettings
                        // TODO : Mettre ca dans une proc stoc un jour ?
                        ulong next_id = 0;
                        ulong next_limit = 0;

                        _dao.WithConnection(con =>
                        {
                            var conCe = con as GenericDBConnectionSqlCE;
                            if (conCe != null)
                                using (var tran = conCe.StartTransaction())
                                {
                                    next_id = (ulong)(long)conCe.ExecuteScalar(string.Format("SELECT {0} FROM {1};", _seedOfIdsColumnName, _idManagerTableName));
                                    conCe.ExecuteNonQuery(string.Format("UPDATE {1} SET {0} = {0} + {2};", _seedOfIdsColumnName, _idManagerTableName, reserve));
                                    next_limit = (ulong)(long)conCe.ExecuteScalar(string.Format("SELECT {0} FROM {1};", _seedOfIdsColumnName, _idManagerTableName));
                                    if (next_limit != next_id + reserve) // Si y'a eu un acces concurentiel
                                        throw new Exception("Rare and not handled error, try again !"); // TODO : ! 
                                    tran.Commit();
                                }
                            else
                            {
                                var dt = con.GetDatatable(string.Format("declare @previous table(newSeed bigint, oldSeed bigint)" + nl +
                                                                        "BEGIN TRAN" + nl +
                                                                        "UPDATE " + _idManagerTableName + nl +
                                                                        "SET {0} = {0} + {1}" + nl +
                                                                        "OUTPUT deleted.{0}, inserted.{0} into @previous " + nl +
                                                                        "COMMIT TRAN" + nl +
                                                                        "SELECT * FROM @previous", _seedOfIdsColumnName, reserve));
                                Debug.Assert(dt.Rows.Count == 1);
                                next_id = (ulong)(long)dt.Rows[0]["oldSeed"];
                                next_limit = (ulong)(long)dt.Rows[0]["newSeed"];
                            }
                        });

                        _next_id = next_id;
                        _next_limit = next_limit;
                    }
                    return _next_id++;
                }
            }
            string nl = Environment.NewLine;

            readonly object _lock = new object();
            public void TryRecycleId(ulong id)
            {
                lock (_lock)
                {
                    if (id + 1 == _next_id)
                        --_next_id;
                    else
                        System.Threading.Interlocked.Increment(ref _WastedIdCount);
                }
            }
            long _WastedIdCount;
            public long WastedIdCount { get { return _WastedIdCount; } }
        }
    }
}