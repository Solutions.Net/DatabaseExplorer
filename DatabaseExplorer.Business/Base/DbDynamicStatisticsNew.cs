﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Diagnostics;

using DataMapper;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public class DbDynamicStatisticsNew
    {
        public static DbDynamicStatisticsNew GetStatisticsFor(IDbMapper dao)
        {
            return _statistics.GetOrAdd(dao, key =>
            {
                var tmp = new DbDynamicStatisticsNew();
                tmp.ForceInit(dao);
                return tmp;
            });
        }
        static readonly ConcurrentDictionary<IDbMapper, DbDynamicStatisticsNew> _statistics = new ConcurrentDictionary<IDbMapper, DbDynamicStatisticsNew>();
        Dictionary<string, long> RowCounts;

        public long GetRowCountFor(IDbMapper dao, string tablename, string schema = null)
        {
            ForceInit(dao);
            return RowCounts[new[] { schema, tablename }.NotBlank().Join(".")];
        }
        public void ForceInit(IDbMapper dao)
        {
            if (RowCounts == null)
                RowCounts = GetCountsOfAllTables(dao);
        }
        Dictionary<string, long> GetCountsOfAllTables(IDbMapper dao)
        {
            var dt = dao.GetDataTable(rqGetTableRowCountForSql);
            return dt.AsEnumerable().ToDictionary(row => new[] { row["TABLE_SCHEMA"] == DBNull.Value ? null : (string)row["TABLE_SCHEMA"],
                                                                 (string)row["TABLE_NAME"] }
                                                        .NotBlank().Join("."),
                                                  row => (long)row["CARDINALITY"]);
        }

        internal static string rqGetTableRowCountForSql
        {
            get
            {
                return @"-- from http://stackoverflow.com/a/19631059
SELECT 
    sc.name as TABLE_SCHEMA,
    ta.name as TABLE_NAME,
    SUM(pa.rows)          as CARDINALITY
FROM 
    sys.tables ta
INNER JOIN sys.partitions pa
    ON pa.OBJECT_ID = ta.OBJECT_ID
INNER JOIN sys.schemas sc
    ON ta.schema_id = sc.schema_id
WHERE ta.is_ms_shipped = 0 AND pa.index_id IN (1,0)
GROUP BY sc.name,ta.name
ORDER BY SUM(pa.rows) DESC, TABLE_NAME".SkipLines(DebugTools.IsDebug ? 0 : 1);
            }
        }

        internal static string rqGetTableRowCountForSqlAlternative
        {
            get
            {
                return @"-- alternative from http://stackoverflow.com/a/2836803
SELECT '??? TODO'     as TABLE_SCHEMA,
       o.name         as TABLE_NAME,
       ddps.row_count as CARDINALITY 
FROM sys.indexes AS i
  INNER JOIN sys.objects AS o ON i.OBJECT_ID = o.OBJECT_ID
  INNER JOIN sys.dm_db_partition_stats AS ddps ON i.OBJECT_ID = ddps.OBJECT_ID
  AND i.index_id = ddps.index_id 
WHERE i.index_id < 2  AND o.is_ms_shipped = 0 
ORDER BY ddps.row_count  DESC, TABLE_NAME
".SkipLines(DebugTools.IsDebug ? 0 : 1);
            }
        }

        internal static string rqGetTableRowCountForSqlCe
        {
            get
            {
                return @"SELECT TABLE_SCHEMA, TABLE_NAME, CARDINALITY FROM INFORMATION_SCHEMA.INDEXES";
            }
        }
    }
}
