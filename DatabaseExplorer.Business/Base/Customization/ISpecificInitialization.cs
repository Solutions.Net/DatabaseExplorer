﻿using System;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public interface ISpecificInitialization
    {
        void InitializeModelValuesUsingBusinessKnowledge();
    }
}
