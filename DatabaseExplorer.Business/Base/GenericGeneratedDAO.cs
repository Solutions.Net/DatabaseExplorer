﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;


using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Threading;

using DataMapper;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public abstract partial class GenericGeneratedDAO : IIsNamed
    {
        protected readonly Func<IGenericDBConnection> _CreateDBConnection;
        public IDbMapper Mapper { get; }

        public string Name { get { return "GenericGeneratedDAO"; } }

        protected GenericGeneratedDAO(Func<IGenericDBConnection> createDBConnection)
        {
            _CreateDBConnection = createDBConnection;
            using (var con = createDBConnection())
                Mapper = DbMapperFactory.CreateSqlMapper(this, con.ConnectionString);
        }

        public IGenericDBConnection CurrentDBConnection { get; private set; }
        
        public abstract Dictionary<Type, bool> AllTypes { get; }

        // Create a connection that must be disposed
        public virtual IGenericDBConnection CreateConnection()
        {
            return _CreateDBConnection();
        }
        public virtual void WithConnection(Action<IGenericDBConnection> action)
        {
            if (CurrentDBConnection != null)
                action(CurrentDBConnection);
            else
                using (var con = _CreateDBConnection())
                {
                    CurrentDBConnection = con;
                    try
                    {
                        //con.EnsureIsOpen();
                        action(con);
                    }
                    finally
                    {
                        CurrentDBConnection = null;
                    }
                }
        }
        public void WithConnectionAndTransaction(Action<IGenericDBConnection, GenericDBTransaction> action)
        {
            WithConnection(con =>
            {
                using (var tran = con.StartTransaction())
                {
                    action(con, tran);
                }
            });
        }
        public virtual T WithConnection<T>(Func<IGenericDBConnection, T> func)
        {
            using (var con = _CreateDBConnection())
            {
                CurrentDBConnection = con;
                try
                {
                    return func(con);
                }
                finally
                {
                    CurrentDBConnection = null;
                }
            }
        }
        public T WithConnectionAndTransaction<T>(Func<IGenericDBConnection, GenericDBTransaction, T> func)
        {
            return WithConnection(con =>
            {
                using (var tran = con.StartTransaction())
                {
                    return func(con, tran);
                }
            });
        }


        public IList GetAllObjects(Type type)
        {
            Debug.Assert(type.BaseType != null);
            Debug.Assert(type.BaseType.BaseType != null);
            Debug.Assert(type.BaseType.BaseType.IsGenericType && type.IsSubclassOfRawGeneric(typeof(BaseDTO<>)));
            var m = typeof(GenericGeneratedDAO).GetMethod(nameof(GetAllObjectsOfType), BindingFlags.Instance | BindingFlags.NonPublic);
            var mt = m.MakeGenericMethod(new[] { type });
            return (IList)mt.Invoke(this, null);
        }
        IList GetAllObjectsOfType<TObject>()
            where TObject : class, IHasMutableTypedId, ILoadableById, IPropertyNotifierObject, IAutoMappedDbObject, new()
        {
            var getIdsWhere = GetGetIdsWhereMethod<TObject>();
            if (getIdsWhere == null) // Pas d'id sur ce type à priori (ex : les logs)
                return GetAllObjectWithoutCacheHandling<TObject>();

            var ids = getIdsWhere(null);
            var prop = typeof(TObject).GetField("IdType", BindingFlags.Static | BindingFlags.Public);
            prop.GetValue(null);
            var m = typeof(GenericGeneratedDAO).GetMethod("GetAllByDbMapper", BindingFlags.Instance | BindingFlags.Public);
            var mt = m.MakeGenericMethod(new[] { typeof(TObject), (Type)prop.GetValue(null) });
            return (IList)mt.Invoke(this, new object[] { ids, false });
        }

        Action<DbDataReader, List<TObject>> GetFillFromReaderMethod<TObject>()
            where TObject : class, IHasMutableTypedId, ILoadableById, IPropertyNotifierObject, IAutoMappedDbObject, new()
        {
            Type type = typeof(TObject);
            var m = type.GetMethod(FillFromReaderMethodName);
            return (reader, objs) => m.Invoke(null, new object[] { reader, objs });
        }
        public const string FillFromReaderMethodName = "FillFromReader";

        Func<string, IEnumerable> GetGetIdsWhereMethod<TObject>()
            where TObject : class, IHasMutableTypedId, ILoadableById, IPropertyNotifierObject, IAutoMappedDbObject, new()
        {
            Type type = typeof(TObject);
            var m = type.GetMethod(GetIdsWhereMethodName, BindingFlags.Static | BindingFlags.NonPublic);
            if (m == null)
                return null;
            return str => (IEnumerable)m.Invoke(null, new object[] { str });
        }
        public const string GetIdsWhereMethodName = "GetIdsWhere";

        //DedicatedThreadPool _pool = new DedicatedThreadPool("DAO parallel loading", Environment.ProcessorCount);
        List<TObject> GetAllObjectWithoutCacheHandling<TObject>()
            where TObject : class, IHasMutableTypedId, ILoadableById, IPropertyNotifierObject, IAutoMappedDbObject, new()
        {
#if DEBUG
            var stopWatch = new Stopwatch();
            stopWatch.Start();
#endif
            var mtable = Mapper.GetTableMapping(typeof(TObject));
            var fillFromReader = GetFillFromReaderMethod<TObject>();

            const int maxRecordByPart = 100000;
            int cnt = 0;
            if (!EnabledMultiThreading || Environment.ProcessorCount == 1) // Either a single CPU or hyperthreading not enabled in the OS or the BIOS.
                cnt = 1;
            else
                WithConnection(con => cnt = con.ExecuteScalar<int>("SELECT count(*) as Count FROM " + mtable.TableName).Value);

            var objects = new List<TObject>(cnt);
            if (cnt <= maxRecordByPart || !EnabledMultiThreading || Environment.ProcessorCount == 1) // Either a single CPU or hyperthreading not enabled in the OS or the BIOS.
            {
                WithConnection(con => con.ReadDataWith("SELECT * FROM " + mtable.TableName, reader => fillFromReader(reader, objects)));
            }
            else
            {
                var nbParts = (cnt - 1) / maxRecordByPart + 1;
                var partSize = cnt / nbParts; // NOTE : partSize * nbParts can be < cnt, so the last or first part will have to handle the remaining records

                var works = new List<Func<List<TObject>>>(nbParts);
                for (int i = 0; i < nbParts; ++i)
                {
                    int ii = i;
                    works.Add(() =>
                    {
                        // a optimiser peut etre si c'est lent avec http://sqlperformance.com/2015/01/t-sql-queries/pagination-with-offset-fetch
                        var sb = new StringBuilder();
                        sb.AppendLine("WITH Results_CTE AS");
                        sb.AppendLine("(");
                        sb.AppendLine("    SELECT *, ROW_NUMBER() OVER (ORDER BY (SELECT 0)) AS RowNum"); // interet du Select 0 => Pas de tri 
                        sb.AppendLine("    FROM " + mtable.TableName);
                        sb.AppendLine(")");
                        sb.AppendLine("SELECT *");
                        sb.AppendLine("FROM Results_CTE");
                        var offset = ii * partSize;
                        sb.Append("WHERE RowNum > "); sb.Append(offset); sb.AppendLine();
                        sb.Append("  AND RowNum <= "); sb.Append(ii == nbParts - 1 ? cnt : offset + partSize); sb.AppendLine();

                        var objs = new List<TObject>();
                        WithConnection(con => con.ReadDataWith(sb.ToString(), reader => fillFromReader(reader, objs)));
                        return objs;
                    });
                };
                var results = RunBackgroundThreadsAndWait("Loading " + mtable.TableName, works);
                for (int i = 0; i < nbParts; ++i)
                    objects.AddRange(results[i]);
            }

#if DEBUG
            stopWatch.Stop();
            Debug.WriteLine("Execution & Load requete : " + stopWatch.Elapsed.ToHumanReadableShortNotation());
            stopWatch.Reset();
            stopWatch.Start();
#endif
            foreach (var newObj in objects)
                CacheManager.Instance.Add(newObj);
#if DEBUG
            stopWatch.Stop();
            Debug.WriteLine("Ajout au cache : " + stopWatch.Elapsed.ToHumanReadableShortNotation());
#endif
            return objects;
        }

        //[DebuggerHidden, DebuggerStepThrough]
        public List<TObject> GetAllByDbMapper<TObject, TIdTuple>(IEnumerable<TIdTuple> ids, bool preventLoading = false)
            where TObject : class, IHasMutableTypedId, ILoadableById, IPropertyNotifierObject, IAutoMappedDbObject, new()
            where TIdTuple : struct, IIdTuple
        {
#if DEBUG
            var stopWatch = new Stopwatch();
            stopWatch.Start();
#endif
            var idsToLoad = new HashSet<TIdTuple>(ids);
            var objectsInCache = CacheManager.Instance.GetObjectsInMemory<TObject>();
            var objects = new List<TObject>(idsToLoad.Count); // resultats
            objects.AddRange(objectsInCache.Where(obj => idsToLoad.Remove((TIdTuple)obj.IdTuple)));
#if DEBUG
            stopWatch.Stop();
            Debug.WriteLine("Finding object in cache : " + stopWatch.Elapsed.ToHumanReadableShortNotation() + ". There are " + idsToLoad.Count + " objects to get from database");
            stopWatch.Reset();
            stopWatch.Start();
#endif
            if (preventLoading || idsToLoad.Count == 0)
                return objects;

            const int maxRecordByPart = 100000;
            if (idsToLoad.Count <= maxRecordByPart || !EnabledMultiThreading || Environment.ProcessorCount == 1) // Either a single CPU or hyperthreading not enabled in the OS or the BIOS.
            {
                var objs = LoadFromDb<TObject, TIdTuple>(idsToLoad.ToArray(), 0, idsToLoad.Count, objectsInCache.Select(obj => (TIdTuple)obj.IdTuple));
                objects.AddRange(objs);
            }
            else
            {
                var arraysToSplit = idsToLoad.ToArray();
                var nbParts = (idsToLoad.Count - 1) / maxRecordByPart + 1;
                var partSize = idsToLoad.Count / nbParts; // NOTE : partSize * nbParts can be < cnt, so the last or first part will have to handle the remaining records

                var works = new List<Func<List<TObject>>>(nbParts);
                for (int i = 0; i < nbParts; ++i)
                {
                    int ii = i;
                    works.Add(() =>
                    {
                        var offset = ii * partSize;
                        return LoadFromDb<TObject, TIdTuple>(arraysToSplit, offset, ii == nbParts - 1 ? idsToLoad.Count : offset + partSize, objectsInCache.Select(obj => (TIdTuple)obj.IdTuple));
                    });
                }
                var results = RunBackgroundThreadsAndWait("Loading " + typeof(TObject).Name + " objects", works);
                for (int i = 0; i < nbParts; ++i)
                    objects.AddRange(results[i]);
            }
#if DEBUG
            stopWatch.Stop();
            Debug.WriteLine("Execution & Load requete : " + stopWatch.Elapsed.ToHumanReadableShortNotation());
            stopWatch.Reset();
            stopWatch.Start();
#endif
            var objectsInCache2 = CacheManager.Instance.GetObjectsInMemory<TObject>();
            if (objectsInCache2.Count - objectsInCache.Count == objects.Count)
            {
#if SUPER_DEBUG
                 var idMissing = idsToLoad.Except(objectsInCache2.Select(o => (TIdTuple)o.Id)).ToList();
                Debug.Assert(idMissing.Count == 0);
#endif
                // Rien à faire ! Les objets ont déjà été ajouté au cache
            }
            else if (objectsInCache2.Count == objectsInCache.Count)
                for (int i = objectsInCache.Count; i < objects.Count; ++i)
                    CacheManager.Instance.Add(objects[i]);
            else // cas foireux ou on est entre les deux !
            {
                DebugTools.Break();
                throw new Exception("Unexpected behavior!");
            }
#if DEBUG
            stopWatch.Stop();

            if (objects.Count - objectsInCache.Count > 0)
            {
                Debug.WriteLine("Ajout au cache : " + stopWatch.Elapsed.ToHumanReadableShortNotation());
                var mtable = Mapper.GetTableMapping(typeof (TObject));
                Debug.WriteLine("   temps par propriété : " + new TimeSpan(stopWatch.Elapsed.Ticks/((objects.Count - objectsInCache.Count)*mtable.AllFields.Count)).ToString());
            }
#endif
            return objects;
        }





        private List<TObject> LoadFromDb<TObject, TIdTuple>(TIdTuple[] idsToLoad, int from, int to, IEnumerable<TIdTuple> idsInCache)
            where TObject : class, IHasMutableTypedId, ILoadableById, IPropertyNotifierObject, IAutoMappedDbObject, new()
            where TIdTuple : struct, IIdTuple
        {
            Debug.Assert(from < to); // au moins 1 à charger
            var mtable = Mapper.GetTableMapping(typeof(TObject));
#if DEBUG
            var stopWatch = new Stopwatch();
            stopWatch.Start();
#endif
            var sb = new StringBuilder("SELECT * FROM " + mtable.TableName);

            //if (idsToLoad.Count > 100000)
            //    LoadFromDb
            if (mtable.PkFields.Count == 1) // Permet de diviser par trois la taille du texte a envoyer
            {
                var dbDynamicStatistics = DbDynamicStatistics.GetStatisticsFor(this);
                bool weWantAllRow = dbDynamicStatistics.GetRowCountFor(this, mtable.TableName) == to - from &&
                                    idsInCache.Count() == 0; // complexe à faire sans cette condition car il faudrait locker le cache le temps d'identifier les row à ne pas prendre en compte etc ..
                if (!weWantAllRow) // Pour SqlCe permet de reduire le temps d'execution de QueryExecutionPlan quand il a bcp d'Id
                {
                    sb.Append(" WHERE ");
                    sb.Append(mtable.PkFields[0].ColumnNameProtected);
                    sb.Append(" IN (");
                    for (int i = from; i < to; ++i)
                    {
                        sb.Append(DbMappedField.ToSql(idsToLoad[i].Keys[0]));
                        sb.Append(", ");
                    }
                    sb.Length = sb.Length - 2;
                    sb.Append(")");
                }
            }
            else if (mtable.PkFields.Count == 2) // Permet de diviser par deux la taille du texte a envoyer
            {
                sb.Append(" WHERE EXISTS(");
                for (int i = from; i < to; ++i)
                {
                    int k;
                    sb.Append(" SELECT ");
                    for (k = 0; k < mtable.PkFields.Count; ++k)
                    {
                        sb.Append(DbMappedField.ToSql(idsToLoad[i].Keys[k]));
                        sb.Append(", ");
                    }
                    if (k > 0)
                        sb.Length = sb.Length - 2;
                    sb.Append(" UNION ALL\r\n");
                }
                sb.Length = sb.Length - 12;
                sb.Append(")");
            }
            else
            {
                sb.Append(" WHERE ");
                for (int i = from; i < to; ++i)
                {
                    int k;
                    for (k = 0; k < mtable.PkFields.Count; ++k)
                    {
                        sb.Append(mtable.PkFields[k].ColumnNameProtected);
                        sb.Append(" = ");
                        sb.Append(DbMappedField.ToSql(idsToLoad[i].Keys[k]));
                        sb.Append(" AND ");
                    }
                    if (k > 0)
                        sb.Length = sb.Length - 5;
                    sb.Append(" OR ");
                }
                sb.Length = sb.Length - 4;
            }
#if DEBUG
            stopWatch.Stop();
            Debug.WriteLine("Generation requete (Length = " + sb.Length + ") : " + stopWatch.Elapsed.ToHumanReadableShortNotation());
#endif

            var objs = new List<TObject>();
            var m = typeof(TObject).GetMethod(FillFromReaderMethodName);
            var fillFromReader = GetFillFromReaderMethod<TObject>();
            WithConnection(con => con.ReadDataWith(sb.ToString(), reader => fillFromReader(reader, objs)));
            return objs;
        }

        public bool EnabledMultiThreading = false; // experimental donc false par defaut
        protected T[] RunBackgroundThreadsAndWait<T>(string actionName, List<Func<T>> actions)
        {
            var evts = new List<ManualResetEvent>();
            var exceptions = new List<Exception>();
            var results = new T[actions.Count];
            for (int i = 0; i < actions.Count; ++i)
            {
                int ii = i;
                var action = actions[i];

                var evt = new ManualResetEvent(false);
                evts.Add(evt);
                var bck = new BackgroundWorker(actionName + ii);
                bck.DoWork += (sender, e) =>
                {
                    // Ca marche tres bien sans cette ligne (le problème vient de la bande passante)
                    //SetThreadAffinityMask(GetCurrentThread(), new IntPtr(1 << ii)); // From http://omegacoder.com/?p=94
                    results[ii] = action();
                    evt.Set();
                };
                bck.RunWorkerCompleted += (sender, e) =>
                {
                    if (e.Error != null)
                        exceptions.Add(e.Error);
                };
                bck.RunWorkerAsync();
            }
            for (int i = 0; i < actions.Count; ++i)
                evts[i].WaitOne();
            if (exceptions.Count > 0)
                throw new AggregateException(exceptions);
            return results;
        }
        //[DllImport("kernel32.dll")]
        //static extern IntPtr SetThreadAffinityMask(IntPtr hThread, IntPtr dwThreadAffinityMask);
        //[DllImport("kernel32.dll")]
        //static extern IntPtr GetCurrentThread();

    }
}
