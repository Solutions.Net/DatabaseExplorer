﻿using System;
using System.Collections.Generic;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public class BusinessDeleteActionDependency
    {
        /// <summary>Object that references "this"</summary>
        public Func<IEnumerable<IBusinessObjectGenerated>> GetReferencers { get; private set; }
        /// <summary>The property that holds the reference</summary>
        public string                   PropertyName      { get; private set; }
        /// <summary>
        /// An action to make the reference disappears in the property (ie: null asignation or remove operation on the property).
        /// This action returns an undo operation.
        /// </summary>
        public Func<Action>             DeleteAndGetUndo  { get; private set; }
        /// <summary>
        /// A Boolean that indicates if the object that references \"this\" needs to be deleted too.
        /// In this case, calling the action will delete the object in cascade (and potentially a lot more because of recursivity).
        /// This cascading occurs if \"this\" is pointed by non null property.
        /// </summary>
        public bool                     DeleteIsCascading { get; private set; }
    }
}
