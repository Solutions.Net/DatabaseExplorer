﻿using System;
using System.Collections.Concurrent;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using TechnicalTools;
using TechnicalTools.Model;


namespace DatabaseExplorer.Business.BaseDALClass
{
    public partial class GenericDBConnection : IDisposable
    {
        public delegate void RequestBeginEventHandler(object sender, QueryLog log);
        public delegate void RequestEndEventHandler(object sender, QueryLog log);
        protected void RaiseRequestBegin(QueryLog log)
        {
            if (RequestBegin != null)
                RequestBegin(this, log);
        }
        protected void RaiseRequestEnd(QueryLog log)
        {
            if (RequestEnd != null)
                RequestEnd(this, log);
        }
        public static event    RequestBeginEventHandler RequestBegin;
        public static event    RequestEndEventHandler   RequestEnd;
       

        protected T LogQuery<T>(string query, eDbRequestType type, Func<T> doRequest)
        {
            var log = QueryLog.Create();
            log.RequestType = type;
            log.Query = query;
            RaiseRequestBegin(log);
            T res = default(T);
            try
            {
                log.StartUTC = DateTime.UtcNow;
                res = doRequest();
                log.EndUTC = DateTime.UtcNow;
                log.Result = res;
                return res;
            }
            catch (Exception ex)
            {
                log.EndUTC = DateTime.UtcNow;                
                log.Exception = ex;
                throw;
            }
            finally
            {
                RaiseRequestEnd(log);
                log.Dispose();
            }
        }
    }

    public enum eDbRequestType
    {
        None = 0,
        GetDatatable,
        ExecuteNonQuery,
        ExecuteScalar,
        ReadDataWith
    }

    public class QueryLog : IDisposable
    {
        #region Constructor

        public static QueryLog Create()
        {
            QueryLog instance;
            if (!__instances.TryPop(out instance))
                instance = new QueryLog();
            instance.QueryId = Interlocked.Increment(ref __queryIdGenerator);
            instance.TakeOwnership();
            return instance;
        }
        static int __queryIdGenerator;

        private QueryLog() { }

        static ConcurrentStack<QueryLog> __instances = new ConcurrentStack<QueryLog>();

        void Clear()
        {
            ClientConnectionId = Guid.Empty;
            QueryId = 0;
            RequestType = eDbRequestType.None;
            Query = null;
            Result = null;
            Exception = null;
            StartUTC = DateTime.MinValue;
            EndUTC = null;
        }

        int _referenceCounter;
        #endregion Constructor
        
        public void TakeOwnership()
        {
            ++_referenceCounter;
        }

        public void Dispose()
        {
            if (--_referenceCounter == 0)
                if (__instances.Count < 100)
                {
                    Clear();
                    __instances.Push(this);
                }
        }

        public Guid ClientConnectionId { get; set; }
        public int QueryId { get; private set; }
        public eDbRequestType RequestType { get; set; }
        public string Query { get; set; }
        public object Result { get; set; }
        public Exception Exception { get; set; }
        public DateTime StartUTC { get; set; } // DateTime.UtcNow est bien plus rapide que DateTime.Now
        public DateTime? EndUTC { get; set; }
        public TimeSpan? Duration { get { return EndUTC - StartUTC; } }


        public string AsString
        {
            get
            {
                string nl = Environment.NewLine;
                return "   - ClientConnectionId: " + ClientConnectionId + " | " +
                            "QueryId: " + QueryId + " | " +
                            "RequestType: " + RequestType + " | " +
                            "Duration: " + (Duration.HasValue ? Duration.Value.ToHumanReadableShortNotation() : "???") + " | " +
                    (Exception != null ? " | " + "Exception: " + Exception.Message : "") + nl +
                    Query.Replace(nl, nl + "     ").TrimEnd();
            }
        }
    }
}
