﻿using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.SqlServer.Management.Smo;
using System.Runtime.CompilerServices;

namespace DatabaseExplorer.Business.Extensions
{
    public static class Schema_Extensions
    {
        public static IReadOnlyCollection<Table> Tables(this Schema s) { return ExtendedData(s).Tables; }
        public static IReadOnlyCollection<View> Views(this Schema s) { return ExtendedData(s).Views; }
        public static IReadOnlyCollection<StoredProcedure> StoredProcedures(this Schema s) { return ExtendedData(s).StoredProcedures; }
        public static IReadOnlyCollection<UserDefinedFunction> UserDefinedFunctions(this Schema s) { return ExtendedData(s).UserDefinedFunctions; }

        public class Data
        {
            Schema _schema;
            public IReadOnlyCollection<Table> Tables { get { return _schema.Parent.Tables.Cast<Table>().Where(t => t.Schema == _schema.Name).ToList(); } }
            public IReadOnlyCollection<View> Views  { get { return _schema.Parent.Views.Cast<View>().Where(v => v.Schema == _schema.Name).ToList(); } }
            public IReadOnlyCollection<StoredProcedure> StoredProcedures { get { return _schema.Parent.StoredProcedures.Cast<StoredProcedure>().Where(sp => sp.Schema == _schema.Name).ToList(); } }
            public IReadOnlyCollection<UserDefinedFunction> UserDefinedFunctions { get { return _schema.Parent.UserDefinedFunctions.Cast<UserDefinedFunction>().Where(sp => sp.Schema == _schema.Name).ToList(); } }

            internal Data(Schema schema)
            {
                _schema = schema;
            }
        }
        static Data ExtendedData(this Schema schema)
        {
            return _data.GetValue(schema, BuildData);
        }
        static readonly ConditionalWeakTable<Schema, Data> _data = new ConditionalWeakTable<Schema, Data>();
        static Data BuildData(Schema schema)
        {
            return new Data(schema);
        }
    }
}
