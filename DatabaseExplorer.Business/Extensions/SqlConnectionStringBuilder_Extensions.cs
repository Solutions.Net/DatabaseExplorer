﻿using System;
using System.Data.SqlClient;


namespace DatabaseExplorer.Business.Extensions
{
    public static class SqlConnectionStringBuilder_Extensions
    {
        public static bool IsValid(this SqlConnectionStringBuilder con)
        {
            return !string.IsNullOrEmpty(con.DataSource) 
                && !string.IsNullOrEmpty(con.InitialCatalog) 
                && (   con.IntegratedSecurity 
                    || !string.IsNullOrEmpty(con.UserID) && !string.IsNullOrEmpty(con.Password));
        }
    }

}
