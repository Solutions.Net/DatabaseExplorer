﻿using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.SqlServer.Management.Smo;
using System.Runtime.CompilerServices;
using TechnicalTools;

namespace DatabaseExplorer.Business.Extensions
{
    public static class Table_Extensions
    {
        public static Schema OwnerSchema(this Table t) { return t.Parent.Schemas[t.Schema]; }
        public static string FullName(this Table t) { return t.Schema + "." + t.Name; }
        public static IReadOnlyList<Column> PkColumns(this Table t) { return _pkColumns.GetValue(t, BuildPkColumns); }
        static readonly ConditionalWeakTable<Table, IReadOnlyList<Column>> _pkColumns = new ConditionalWeakTable<Table, IReadOnlyList<Column>>();
        static IReadOnlyList<Column> BuildPkColumns(Table t)
        {
            return t.Columns.Cast<Column>().Where(c => c.InPrimaryKey).ToList();
        }

        public static List<ConstraintFK> FkConstraints(this Table table) { return _fkContraints.GetValue(table, t => { BuildAllFkContraints(t.Parent); return FkConstraints(t); }); }
        public static List<ConstraintFK> FkReferencing(this Table table) { return _fkreferencings.GetValue(table, t => { BuildAllFkContraints(t.Parent); return FkReferencing(t); }); }
        static void BuildAllFkContraints(Database db)
        {
            var fkContraints = new ConditionalWeakTable<Table, List<ConstraintFK>>();
            var fkReferencings = new ConditionalWeakTable<Table, List<ConstraintFK>>();

            foreach (Table table in db.Tables)
            {
                fkContraints.Add(table, new List<ConstraintFK>());
                fkReferencings.Add(table, new List<ConstraintFK>());
            }
            foreach (Table table in db.Tables)
            {
                List<ConstraintFK> fks;
                fkContraints.TryGetValue(table, out fks);
                foreach (var ofk in table.ForeignKeys.Cast<ForeignKey>())
                {
                    var rtable = db.Tables[ofk.ReferencedTable, ofk.ReferencedTableSchema];
                    var fk = new ConstraintFK()
                    {
                        Original = ofk,
                        ForeignColumns = ofk.Columns.Cast<ForeignKeyColumn>().Select(col => table.Columns[col.Name]).ToArray(),
                        ReferencedColumns = ofk.Columns.Cast<ForeignKeyColumn>().Select(col => rtable.Columns[col.ReferencedColumn]).ToArray(),
                    };
                    fks.Add(fk);
                    List<ConstraintFK> rfks;
                    fkReferencings.TryGetValue(rtable, out rfks);
                    rfks.Add(fk);
                }
            }
            _fkContraints = fkContraints;
            _fkreferencings = fkReferencings;
        }
        static ConditionalWeakTable<Table, List<ConstraintFK>> _fkContraints = new ConditionalWeakTable<Table, List<ConstraintFK>>();
        static ConditionalWeakTable<Table, List<ConstraintFK>> _fkreferencings = new ConditionalWeakTable<Table, List<ConstraintFK>>();
    }
    public class ConstraintFK
    {
        public ForeignKey Original          { get; set; }
        public string     Name              { get { return Original.Name; } }

        public Column[]   ForeignColumns    { get; set; }
        public Column[]   ReferencedColumns { get; set; }

        public IEnumerable<T> ForEach<T>(Func<Column, Column, T> fkRefColumnSelector)
        {
            return ForeignColumns.Zip(ReferencedColumns, fkRefColumnSelector);
        }
    }
}
