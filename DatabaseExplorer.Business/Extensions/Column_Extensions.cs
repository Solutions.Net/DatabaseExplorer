﻿using System;
using System.Diagnostics;
using System.Linq;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;
using TechnicalTools.Diagnostics;

using DatabaseExplorer.DAL;
using System.Collections.Generic;
using DatabaseExplorer.Business.Model;

namespace DatabaseExplorer.Business.Extensions
{
    public static class Column_Extensions
    {
        public static Database GetDb(this Column c) { return (c.Parent as Table).Parent ?? (c.Parent as View)?.Parent; }

        public static string OwnerName(this Column c)  { return c.OwnerTable()?.Name ?? c.OwnerView().Name; }
        public static Table OwnerTable(this Column c) { return c.Parent as Table; }
        public static View OwnerView(this Column c) { return c.Parent as View; }
        public static Schema Schema(this Column c) { return GetDb(c).Schemas[(c.Parent as Table)?.Schema ?? (c.Parent as View)?.Schema]; }
        public static string FullName(this Column c) { return Schema(c).Name + "." + OwnerName(c) + "." + c.Name; }
        public static Column ReferencedColumn(this Column c)
        {
            if (!c.IsForeignKey)
                throw new Exception("referenced foreign key not found! Are you sure column is a foreign key ?");
            var table = c.Parent as Table;
            Debug.Assert(table != null);
            foreach (var fk in table.ForeignKeys.Cast<ForeignKey>())
            {
                //ForeignColumns = fk.Columns.Cast<ForeignKeyColumn>().Select(col => tablebySchemaByName[tableBySchema.Key][t.Name][col.Name]).ToArray(),
                //ReferencedColumns = fk.Columns.Cast<ForeignKeyColumn>().Select(col => tablebySchemaByName[fk.ReferencedTableSchema][fk.ReferencedTable][col.ReferencedColumn]).ToArray(),
                int i = -1;
                while (++i < fk.Columns.Count)
                    if (ReferenceEquals(fk.Columns[i], c))
                    {
                        var db = table.Parent;
                        var fkTable = db.Tables[schema: fk.ReferencedTableSchema, name: fk.ReferencedTable];
                        return fkTable.Columns[(fk.Columns[i] as ForeignKeyColumn).ReferencedColumn];
                    }
            }
            throw new Exception("referenced foreign key not found! Are you sure column is a foreign key ?");
        }
        
        // Ordinal position begins at 1, not zero
        public static int OrdinalPosition(this Column c)
        {
            int i = 1;
            foreach (var col in c.OwnerTable().Columns)
                if (col == c)
                    return i;
                else
                    ++i;
            throw new ArgumentException();
        }
        
        public static string DefaultValue(this Column c)
        {
            if (c.Default == "")
                return null;
            return c.Default;
        } 
        public static bool IsFK(this Column c)
        {
            return c.IsForeignKey;
        }
        

        public static bool HasLengthLimit(this Column c)
        {
            if (!c.DataType.IsStringType && c.DataType.SqlDataType.NotIn(SqlDataType.SysName, SqlDataType.Binary, SqlDataType.VarBinary, SqlDataType.VarBinaryMax))
                return false;
            if (c.DataType.MaximumLength == -1)
                return false;
            return true;
        }
        public static int ArrayLength(this Column c)
        {
            return c.DataType.MaximumLength;
        }

        public static void MakePrimaryKeys(this IEnumerable<Column> cols)
        {
            var t = cols.Select(rcol => rcol.OwnerTable()).Distinct().Single();
            Index index = new Index(t, "PK__" + t.Name + "__" + cols.Select(c => c.Name).Join("_"));
            index.IndexKeyType = IndexKeyType.DriPrimaryKey;
            index.IsClustered = true;
            index.IsUnique = true;
            foreach (var col in cols.Cast<Column>())
                index.IndexedColumns.Add(new IndexedColumn(index, col.Name));
        }
        public static void MakePrimaryKey(this Column col)
        {
            MakePrimaryKeys(col.WrapInList());
        }
        public static void MakeForeignKeysTo(this IEnumerable<Column> cols, IEnumerable<Column> rcols, eLinkType linkType = eLinkType.Unknown)
        {
            var t = cols.Select(rcol => rcol.OwnerTable()).Distinct().Single();
            var rTable = rcols.Select(rcol => rcol.OwnerTable()).Distinct().Single();
            var fk2 = new ForeignKey(t, "FK__" + t.Name + "__" + rTable.Name + "__" + t.Columns.Cast<Column>().Last().Name + "__" + rTable.PkColumns().Single().Name + (linkType == eLinkType.Unknown ? "" : "__As" + linkType.ToString()));
            fk2.ReferencedTableSchema = rTable.OwnerSchema().Name;
            fk2.ReferencedTable = rTable.Name;
            fk2.Columns.Add(new ForeignKeyColumn(fk2, t.Columns.Cast<Column>().Last().Name, rcols.Single().Name));
            t.ForeignKeys.Add(fk2);
        }
        public static void MakeForeignKeysTo(this Column c, Column rcol, eLinkType linkType = eLinkType.Unknown)
        {
            MakeForeignKeysTo(c.WrapInList(), rcol.WrapInList(), linkType);
        }

        public static object MinValue(this Column c)
        {
            return c.DataType.SqlDataType.MinValue();
        }
        public static object MaxValue(this Column c)
        {
            return c.DataType.SqlDataType.MaxValue();
        }

        public static Type CsType(this Column c)
        {
            //var mapping = SqlType.FromSqlType(SqlTypeAsString);
            //Debug.Assert(mapping != null);
            if (c.Nullable && !c.DataType.ToCSharp().IsClass)
                return typeof(Nullable<>).MakeGenericType(c.DataType.ToCSharp());
            return c.DataType.ToCSharp();
        }
        public static Type CsTypeNotNullable(this Column c)
        {
            return c.CsType().TryGetNullableType() ?? c.CsType();
        }

        //private static string CsTypeAsString { get { return TypeToString(CsType); } }
        public static string TypeToString(Type type)
        {
            return type.ToSmartString();
        }
    }
}
