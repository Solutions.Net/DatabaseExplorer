﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Microsoft.SqlServer.Management.Smo;


namespace DatabaseExplorer.Business
{
    public class ModelGenerator
    {
        public bool   OneFilePerClass { get; set; }

        public string                    AssemblyName     { get; private set; }
        public ReadOnlyCollection<Table> Tables           { get { return _Tables.AsReadOnly(); } } readonly List<Table> _Tables;

        protected ModelGenerator(string assName, List<Table> tables)
        {
            AssemblyName = assName;
            _Tables = tables;
        }
    }

   
}
