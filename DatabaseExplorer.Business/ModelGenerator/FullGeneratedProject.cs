﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Microsoft.SqlServer.Management.Smo;

using SubSonic.Extensions;

using TechnicalTools;

using DatabaseExplorer.DAL;
using DatabaseExplorer.Business.Model;
using DatabaseExplorer.Business.Model.Local;
using DatabaseExplorer.Business.Extensions;

using Property = DatabaseExplorer.Business.Model.Property;

namespace DatabaseExplorer.Business
{
    public partial class FullGeneratedProject : Project
    {
        public ModelUML Model     { get; private set; }
        public string   SqlModelCreation { get; set; }
        public string   SqlModelDeletion { get; set; }
        public string   SqlDataInitialization { get; set; }

        protected string                  GeneratedModelAssemblyName { get; private set; }
        public Dictionary<string, string> ModelClasses { get; set; } // FileName +extension (not path) => content

        public FullGeneratedProject(ModelUML model)
        {
            Model = model;
        }
        public override void RunImport()
        {
            //base.RunImport();
            DbInfos = GenerateSQLModel();
            Warnings = "";
        }
        DatabaseInfo GenerateSQLModel()
        {
            var db = new Database();
            var schema = new Schema(db, "dbo");
            var entities = Model.Entities;
            var now = DateTime.Now;

            // Verifications
            //Debug.Assert(types.All(t => !t.IsInterface), "Les interfaces doivent être dans le model !");
            _TypeDatas.Clear();
            foreach (var entity in entities)
            {
                var table = new Table(db, entity.Name, schema.Name);
                DataFor(entity).Table = table;
                //table.Description = null;
                //table.CreationDate = now;
                //table.ChangeDate = table.CreationDate;
               
                if (entity is EntityStruct)
                {
                    // rien a faire à priori puisque les struct seront enregistré comme des varbinary
                    continue;
                }
                else
                {
                    var column = new Column(table, "Id_" + table.Name);
                    table.Columns.Add(column);
                    column.MakePrimaryKey();

                    if (entity is EntityEnum)
                    {
                        column.DataType = (entity as EntityEnum).BaseType.Type.ToDataType();
                        column = new Column(table, "Name");
                        table.Columns.Add(column);
                    }
                    else
                    {
                        column.DataType = typeof(ulong).ToDataType();
                    }
                }
                db.Tables.Add(table);
            }
            
            var joinTables = new List<Tuple<Table, Table>>(); // table de jointure et table owner (si la table de jointure est pour de la compostiion et non des références)
            foreach (var entity in entities)
            {
                var table = DataFor(entity).Table;

                if (entity is EntityStruct)
                {
                    // rien a faire à priori puisque les struct seront enregistré comme des varbinary
                    continue;
                }
                else if (entity is EntityEnum)
                {
                    // Deja FAIT
                    continue;
                }
                else if (entity is EntityInterface)
                {
                    // Rien à faire puisque c'est une interface il n'y a rien à stocker
                    continue;
                }
                foreach (var p in (entity as EntityClass).Properties)
                {
                    var column = new Column(table, p.Name);
                    column.Nullable = p.Nullable;
                    if (p.Type is PropertyTypeNative)
                    {
                        column.DataType = (p.Type as PropertyTypeNative).Type.ToDataType();
                        //Debug.Assert(column.SqlType.CSharpType == propType); // TODO Long et ulong pas pareil !
                    }
                    else if (p.Type is PropertyTypeEntity)
                    {
                        var pe = p.Type as PropertyTypeEntity;
                        if (pe.Type is EntityStruct)
                        {
                            column.DataType = new DataType(SqlDataType.VarBinary, (pe.Type as EntityStruct).SizeOfStruct);
                        }
                        else
                        {
                            if (pe.Type is EntityEnum)
                                column.DataType = (pe.Type as EntityEnum).BaseType.Type.ToDataType();
                            else
                                column.DataType = typeof(ulong).ToDataType(); // ID !

                            column.Name += "_Id";

                            if (p.LinkType == eLinkType.BackwardLink) 
                                continue; // On zappe car il s'agit d'une propriete indiquant le propriétaire de l'objet courant
                            column.MakeForeignKeysTo(DataFor(pe.Type).Table.PkColumns().First(), p.LinkType);
                        }
                    }
                    else if (p.Type is PropertyTypeCollection)
                    {
                        column = null;
                        var linkTable = new Table(db, table.Name + "_" + p.Name, schema.Name);
                        linkTable.Columns.Add(new Column(linkTable, "Id_" + linkTable.Name, typeof(ulong).ToDataType()));
                        Column_Extensions.MakePrimaryKeys(linkTable.Columns.Cast<Column>()); // Permet les doublons  dans les listes!

                        var fkCol = new Column(linkTable, table.Name + "_Id", typeof(ulong).ToDataType());
                        linkTable.Columns.Add(fkCol);
                        fkCol.MakeForeignKeysTo(table.PkColumns()[0], p.LinkType);

                        var pc = p.Type as PropertyTypeCollection;
                        if (pc.Type is PropertyTypeNative)
                        {
                            linkTable.Columns.Add(new Column(linkTable, p.Name.MakeSingular(), (pc.Type as PropertyTypeNative).Type.ToDataType()));
                        }
                        else if (pc.Type is PropertyTypeEntity)
                        {
                            var pce = pc.Type as PropertyTypeEntity;
                            if (pce.Type is EntityStruct)
                            {
                                linkTable.Columns.Add(new Column(linkTable, p.Name.MakeSingular(), typeof(byte[]).ToDataType()));
                            }
                            else
                            {
                                fkCol = new Column(linkTable, p.Name.MakeSingular() + "_Id", typeof(long).ToDataType());
                                fkCol.MakeForeignKeysTo(DataFor(pce.Type).Table.PkColumns().Single());
                            }
                        }
                        else
                            throw new Exception("Not Handled !");

                        DataFor(p).JoinTable = linkTable;
                        joinTables.Add(Tuple.Create(linkTable, table)); // On considère pour le moment que les listes sont des listes de composition
                        db.Tables.Add(linkTable);
                    }
                    else if (p.Type is PropertyTypeDictionary)
                    {
                        column = null;
                        var dicoTable = new Table(db, table.Name + "_" + p.Name, schema.Name);
                        var colPK = new Column(dicoTable, "Id_" + dicoTable.Name, typeof(long).ToDataType());
                        dicoTable.Columns.Add(colPK);
                        colPK.MakePrimaryKey(); // Permet les doublons dans les listes! et aussi d'eviter de mettre une PK avec la clef du dico car les index sont limité a 900 byte en SQL

                        var fkCol = new Column(dicoTable, "Owner_Id", typeof(long).ToDataType());
                        dicoTable.Columns.Add(fkCol);
                        fkCol.MakeForeignKeysTo(table.PkColumns().First());

                        var pd = p.Type as PropertyTypeDictionary;
                        
                        if (pd.KeyType is PropertyTypeNative)
                        {
                            var datatype = (pd.KeyType as PropertyTypeNative).Type.ToDataType();
                            // https://blogs.msdn.microsoft.com/bartd/2011/01/06/living-with-sqls-900-byte-index-key-length-limit/
                            // http://stackoverflow.com/a/2864109/1942901
                            if (datatype.IsStringType)
                            {
                                datatype.MaximumLength = (900 - sizeof(long)) / 2; // set ArrayLength
                                //datatype.??? = (900 - sizeof(long)) / 2; // set HasLengthLimit = true
                            }
                            var col = new Column(dicoTable, "Key", datatype);
                            dicoTable.Columns.Add(col);
                        }
                        else if (pd.KeyType is PropertyTypeEntity)
                        {
                            var pce = pd.KeyType as PropertyTypeEntity;
                            if (pce.Type is EntityEnum)
                                dicoTable.Columns.Add(new Column(dicoTable, pce.Name + "Key_Id", (pce.Type as EntityEnum).BaseType.Type.ToDataType()));
                            else
                                dicoTable.Columns.Add(new Column(dicoTable, pce.Name + "Key_Id", typeof(long).ToDataType()));
                            dicoTable.Columns.Cast<Column>().Last().MakeForeignKeysTo(DataFor(pce.Type).Table.PkColumns().Single());
                        }
                        else
                            throw new Exception("Not Handled !");

                        //dicoTable.Pk = new ConstraintPK();  // On considère pour le moment que les listes sont des listes de composition
                        //dicoTable.Pk.Columns = dicoTable.Columns.ToList();

                        if (pd.ValueType is PropertyTypeNative)
                        {
                            dicoTable.Columns.Add(new Column(dicoTable, p.Name.MakeSingular(), (pd.ValueType as PropertyTypeNative).Type.ToDataType()));
                        }
                        else if (pd.ValueType is PropertyTypeEntity)
                        {
                            var pce = pd.ValueType as PropertyTypeEntity;
                            if (pce.Type is EntityEnum)
                                dicoTable.Columns.Add(new Column(dicoTable, p.Name.MakeSingular() + "_Id", (pce.Type as EntityEnum).BaseType.Type.ToDataType()));
                            else
                                dicoTable.Columns.Add(new Column(dicoTable, p.Name.MakeSingular() + "_Id", typeof(long).ToDataType()));
                            dicoTable.Columns.Cast<Column>().Last().MakeForeignKeysTo(DataFor(pce.Type).Table.PkColumns().Single());
                        }
                        else
                            throw new Exception("Not Handled !");

                        DataFor(p).JoinTable = dicoTable;
                        joinTables.Add(Tuple.Create(dicoTable, table)); // On considère pour le moment que les listes sont des listes de composition
                        db.Tables.Add(dicoTable);
                    }
                    if (column != null)
                        table.Columns.Add(column);
                }
            }

            // TODO : Passer sur toute les table et verifier que leur nom sont uniques
            // TODO : Passer sur toute les colonnes et verifier que leur noms sont uniques

            return new DatabaseInfo(db, new System.Data.SqlClient.SqlConnectionStringBuilder());
        }

        EntityMetaData DataFor(IEntity e)
        {
            EntityMetaData data;
            if (!_TypeDatas.TryGetValue(e, out data))
            {
                data = new EntityMetaData() { For = e };
                _TypeDatas.Add(e, data);
            }
            return data;
        }
        readonly Dictionary<IEntity, EntityMetaData> _TypeDatas = new Dictionary<IEntity, EntityMetaData>();
        class EntityMetaData
        {
            public IEntity For    { get; set; }

            public Table   Table  { get; set; }
        }

        PropertyMetaData DataFor(Property p)
        {
            PropertyMetaData data;
            if (!_PropertyDatas.TryGetValue(p, out data))
            {
                data = new PropertyMetaData() { For = p };
            }
            return data;
        }
        readonly Dictionary<Property, PropertyMetaData> _PropertyDatas = new Dictionary<Property, PropertyMetaData>();

        class PropertyMetaData
        {
            public Property For { get; set; }

            public Table JoinTable { get; set; } // Table de jointure crée dans le cas où le membre et une collection
        }

        public void GenerateModelSourceCodeFilesForKronos(ModelUML model, string modelAssemblyName, string dalAssemblyName, string dbName, bool oneFilePerClass = false, bool navigationProperties = false, Type entitySpecificBaseClass = null)
        {
            var all_tables = DbInfos.Tables.Where(t => t.OwnerSchema().Name == dbName).ToList();
            var generator = new DynamicTypeModelGenerator(model, modelAssemblyName, dalAssemblyName, all_tables, Connection == null ? null : Connection.ConnectionString, entitySpecificBaseClass)
            {
                ForSqlCE = ForSqlCE,
                OneFilePerClass = oneFilePerClass,
                GenerateReferencedCollectionProperties = navigationProperties,
                GenerateReferencingCollectionProperties = navigationProperties
            };

            GeneratedSourceCode = generator.GenerateSourceCode();
            GeneratedAssemblyName = dalAssemblyName;

            ModelClasses = generator.GenerateModelSourceCodeFilesForKronos();
            GeneratedModelAssemblyName = modelAssemblyName;
        }

    }
    /*
    /// <summary>
    /// Représente une Salle (ressource) ou un espace permettant une activite (exemple : salle de cuisine, ou bien un parc)
    /// </summary>
    public class Salle : Lieu, IRessourceMaterielle
    {
        protected override Type DataSourceType { get { return typeof(DAL.Corbeil.Salle); } }
        private new DAL.Corbeil.Salle DataSource { get { return (DAL.Corbeil.Salle)base.DataSource; } set { base.DataSource = value; } }

        // Nombre de personne que peut accueilir la salle (sans compter la/les personnes dirigeant l'activité s'y déroulant)
        //public string Name { get { return DataSource.Name; } set { SetProperty(ref _Structure, value); } } Structure             _Structure;
        //public Structure                Structure        { get { return _Structure;        } set { SetProperty(ref _Structure, value);        } } Structure             _Structure;
        //public int                      NombreDePlace    { get { return _NombreDePlace;    } set { SetProperty(ref _NombreDePlace, value);    } } int                   _NombreDePlace;
        //public List<Caracteristique>    Caracteristiques { get { return _Caracteristiques; } set { SetProperty(ref _Caracteristiques, value); } } List<Caracteristique> _Caracteristiques;
        public Structure Structure { get { return GetBO<Structure>(DataSource.Structure_Id); } set { DataSource.Structure_Id = (long)Id; } } Structure             _Structure;
        public int NombreDePlace { get { return _NombreDePlace; } set { SetProperty(ref _NombreDePlace, value); } } int                   _NombreDePlace;
        public List<Caracteristique> Caracteristiques { get { return _Caracteristiques; } set { SetProperty(ref _Caracteristiques, value); } } List<Caracteristique> _Caracteristiques;

        internal Salle()
        {
            NombreDePlace = 50; // Non géré pour le moment donc on met un nombre suffisant
            Caracteristiques = new List<Caracteristique>();
        }
    }
    */
}
