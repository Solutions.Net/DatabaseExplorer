﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;
using TechnicalTools.Algorithm.Graph;
using TechnicalTools.Diagnostics;

using DatabaseExplorer.Business.Model;
using DatabaseExplorer.Business.Extensions;


namespace DatabaseExplorer.Business
{
    public partial class DynamicTypeModelGenerator : DynamicTypeCodeGeneratorForKronos
    {
        public ModelUML Model {get; private set;}
        public string ModelAssemblyName { get; private set; }

        public DynamicTypeModelGenerator(ModelUML model, string modelAssemblyName, string assName, List<Table> tables, string connectionString, Type entitySpecificBaseClass = null)
            : base(assName, tables, connectionString, entitySpecificBaseClass)
        {
            WithIdManager = true;
            Model = model;
            ModelAssemblyName = modelAssemblyName;
        }

        public override string UsingsNamespacesFormatted
        {
            get
            {
                return new[] 
                {
                    base.UsingsNamespacesFormatted,
                   GetEnumNamespaces().Select(ns => "using " + ns +";").Join(Environment.NewLine)
                }.NotBlank().Join(Environment.NewLine + Environment.NewLine);
            }
        }
        List<string> GetEnumNamespaces()
        {
            return Model.Entities.OfType<EntityEnum>().Select(e => e.Namespace).Distinct().ToList();
        }
        static void Break(string msg)
        {
            Console.WriteLine(msg);
            //if (Debugger.IsAttached)
            //    Debugger.Break();
        }
        public Dictionary<string, string> GenerateModelSourceCodeFilesForKronos(bool oneFilePerClass = false)
        {
            var results = new Dictionary<string, string>();

            results.Add("BusinessObjectGenerated.cs", GenerateBusinessObjectGeneratedBaseClass());

            foreach (var entity in Model.Entities)
            {
                if (entity is EntityEnum)
                    continue; // rien à faire
                if (entity is EntityInterface)
                    continue; // rien à faire non plus

                string filename = entity.Name.Replace("<", "Of").Replace(">", "").Replace(",", "_").Replace(" ", "") + ".cs";
                List<string> usings = new List<string>();
                string content = "";
                usings.Add("System");

                var eStruct = entity as EntityStruct;
                var eClass  = entity as EntityClass;
                var eInterface = entity as EntityInterface;
                if (eStruct != null) // a tester en premier car une EntityStruct est une EntityClass
                    content = GenerateStruct(eStruct, usings);
                else if (eClass != null)
                    content = GenerateClass(eClass, usings);
                else if (eInterface != null)
                    continue;
                else
                    throw new Exception("Unknown case !");

                content = usings.Distinct().OrderBy(u => u).Select(u => "using " + u + ";").Join(nl)
                        + nl
                        + nl
                        + "namespace " + ModelAssemblyName + nl
                        + "{" + nl
                        + content + nl
                        + "}" + nl;
                results.Add(filename, content);
            }
            
            results.Add("BusinessObjectGenerated_DependencyOrder.cs", GenerateDependencyOrderClass()); // Doit etre fait apres les classes
            
            
            return results;
        }
        public string GenerateStruct(EntityStruct eStruct, List<string> usings)
        {
            string content = "";
            usings.Add("System.Runtime.InteropServices");

            content += "    [StructLayout(LayoutKind.Explicit)]" + nl;
            content += "    public partial struct " + eStruct.Name + " : " + eStruct.Interfaces.Concat(new[] { "IBusinessStructGenerated" }).Join() + nl;
            content += "    {" + nl;

            foreach (var p in eStruct.Properties)
            {
                var pt = p.Type as PropertyTypeNative;
                var maxAccessibility = p.IsGetterPublic.MostAccessible(p.IsSetterPublic);
                content += "        " + maxAccessibility.GetDescription() + " "
                    + pt.Type.ToSmartString() + " " + p.Name + " { "
                    + (maxAccessibility == p.IsGetterPublic ? "" : p.IsGetterPublic.GetDescription() + " ") + "get { return _" + p.Name + "; } "
                    + (maxAccessibility == p.IsSetterPublic ? "" : p.IsSetterPublic.GetDescription() + " ") + "set { _" + p.Name + " = value; } "
                    + " } "
                    + "[FieldOffset(" + eStruct.PropertyOffsets[p] + ")] " + pt.Type.ToSmartString() + " _" + p.Name + ";" + nl;
            }
            content += nl;
            // D'apres le lien ci dessous, utiliser BinaryWriter est un peu plus rapide (surtout pour les grosse structure => 40 bytes)
            // Mais utiliser Marshal rend le code plus maintenable
            // ( voir en dessous pour un exemple avec BinaryWriter
            // http://genericgamedev.com/general/converting-between-structs-and-byte-arrays/
            content += "        public byte[] Serialize()" + nl
                     + "        {" + nl
                     + "            var size = Marshal.SizeOf(typeof(" + eStruct.Name + "));" + nl
                     + "            var array = new byte[" + eStruct.SizeOfStruct + "];" + nl
                     + "            var ptr = Marshal.AllocHGlobal(size);" + nl
                     + "            Marshal.StructureToPtr(this, ptr, false);" + nl // false ou true ? http://stackoverflow.com/a/15321330/1942901
                     + "            Marshal.Copy(ptr, array, 0, " + eStruct.SizeOfStruct + ");" + nl
                     + "            Marshal.DestroyStructure(ptr, typeof(" + eStruct.Name + "));" + nl
                     + "            Marshal.FreeHGlobal(ptr);" + nl
                     + "            return array;" + nl
                     + "        }" + nl;

            content += "        public static " + eStruct.Name + " Deserialize(byte[] array)" + nl
                     + "        {" + nl
                     + "            System.Diagnostics.Debug.Assert(array.Length == " + eStruct.SizeOfStruct + ");" + nl
                     + "            var size = Marshal.SizeOf(typeof(" + eStruct.Name + "));" + nl
                     + "            var ptr = Marshal.AllocHGlobal(size);" + nl
                     + "            Marshal.Copy(array, 0, ptr, " + eStruct.SizeOfStruct + ");" + nl
                     + "            var s = (" + eStruct.Name + ")Marshal.PtrToStructure(ptr, typeof(" + eStruct.Name + "));" + nl
                     + "            Marshal.FreeHGlobal(ptr);" + nl
                     + "            return s;" + nl
                     + "        }" + nl;
            content += nl;
            content += "        public string AsDebugString" + nl
                     + "        {" + nl
                     + "            get" + nl
                     + "            {" + nl
                     + "                return \"{\"" + nl;
            foreach (var p in eStruct.Properties)
                content += "                     + \" " + p.Name + ": \" + " + p.Name + nl;
            content += "                     + \" }\";" + nl;
            content += "            }" + nl;
            content += "        }" + nl;


            /* Exemple de code avec BinaryWriter
                int anInteger;
                float aFloat;
                long aLong;

                public byte[] ToArray()
                {
                    var stream = new MemoryStream();
                    var writer = new BinaryWriter(stream);

                    writer.Write(this.anInteger);
                    writer.Write(this.aFloat);
                    writer.Write(this.aLong);

                    return stream.ToArray();
                }

                public static MyStruct FromArray(byte[] bytes)
                {
                    var reader = new BinaryReader(new MemoryStream(bytes));

                    var s = default(MyStruct);

                    s.anInteger = reader.ReadInt32();
                    s.aFloat = reader.ReadSingle();
                    s.aLong = reader.ReadInt64();

                    return s;
                }
            */
            content += "    }";
            return content;
        }
        public string GenerateClass(EntityClass eClass, List<string> usings)
        {
            var table = Tables.First(t => t.Name == eClass.Name);
            var tData = DataFor(table);

            usings.Add("System.Collections.Generic");
            usings.Add("System.Diagnostics");
            usings.Add("System.Linq");
            usings.Add("TechnicalTools");
            usings.Add("TechnicalTools.Model");
            usings.Add("TechnicalTools.Model.Cache");
            usings.Add("TechnicalTools.Tools");
            usings.Add("GenericDALBasics");
            usings.AddRange(GetEnumNamespaces());

            usings.Add(AssemblyName);
            usings.Add(AssemblyName + "." + table.OwnerSchema().Name.Capitalize());

            string content = "";
            string baseClass = eClass.Base == EntityClass.None ? "BusinessObjectGenerated" : eClass.Base.Name;
            content += "    public " + (eClass.IsAbstract ? "abstract " : "") + "partial class " + eClass.Name + " : " + baseClass 
                    + (eClass.Interfaces.Count > 0 ? ", " + eClass.Interfaces.Join() : "") + nl;
            content += "    {" + nl;
            var dalClassName = AssemblyName + "." + DataFor(table).FullClassName;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).PropDeclaration))
                    content += "        " + DataFor(p).PropDeclaration.Replace(nl, nl + "        ") + nl;
            content += nl;
            content += nl;
            content += "        public override long Id { get { return DataSource." + DataFor(table.PkColumns()[0]).PropName + "; } }" + nl;
            content += "        public override bool IsModified" + nl;
            content += "        {" + nl;
            content += "            get" + nl;
            content += "            {" + nl;
            content += "                return base.IsModified";
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).ComparisonToDataSource))
                    content += " ||" + nl +"                       " + DataFor(p).ComparisonToDataSource;
            content += ";" + nl;
            content += "                // TODO : Casse couille pour les structures ! => Modifier CompositeList.DeletedObject en dictionaire (ca ne regle pas le probleme des liste pouvant avoir des doublon) Et faire une version de CompositeListpour les structs !" + nl;
            content += "            }" + nl;
            content += "        }" + nl;
            content += "        protected internal override string Dump(bool all)" + nl;
            content += "        {" + nl;
            content += "            if (!all && (State == eState.New || State == eState.Synchronized))" + nl;
            content += "                return base.Dump(all);" + nl;
            content += "            return new[]" + nl;
            content += "            {" + nl;
            content += "                base.Dump(all)";
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
            {
                // Afficher les changements (Changes) seulement si les objets sont composites sinon risque de recursivité infini entre objets se referencant)
                // Dans le cas contraire on affiche juste l'Id et Added / Removed
                // Si les objets sont des structures, on appelle ToString (et on croise les doigts pour pas de récursivité : Normalement non les structure doivent afficher les Id des objet pas les objet eux meme)
                // Overrider ToString pour l'affichage. L'affichage fonctionnel devrait etre dans des methodes spécifiques pas dans ToString
                if (!string.IsNullOrWhiteSpace(DataFor(p).DumpIfChanged))
                    content += "," + nl + "                " + (p.Type is PropertyTypeCollection || p.Type is PropertyTypeDictionary ? "" : "all || ") + DataFor(p).DumpIfChanged;
            }
            content += nl;
            content += "            }.NotBlank().Join(ChangeIndent);" + nl;
            content += "        }" + nl;
            content += nl;
            content += "        protected override Type ConcreteDataSourceType { get { return typeof(" + dalClassName + "); } }" + nl;
            content += "        private " + dalClassName + " DataSource { get; set; }" + nl;
            content += "        protected override IGeneratedDALEntity GetDataSource() { return DataSource; }" + nl;
            content += "        protected override List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null)" + nl;
            content += "        {" + nl;
            content += "            lst = base.GetDataSources(lst);" + nl;
            content += "            lst.Add(DataSource);" + nl;
            content += "            return lst;" + nl;
            content += "        }" + nl;
            content += nl;
            content += nl;
            content += "        public " + eClass.Name + "() { }" + nl;
            content += "        public " + eClass.Name + "(long id) : base(id) { }" + nl;
            content += nl;
            content += nl;


            content += "        protected override void Init()" + nl;
            content += "        {" + nl;
            content += "            base.Init();" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).Init))
                    content += "            " + DataFor(p).Init.Replace(nl, nl + "            ") + nl;
            content += "        }" + nl;
            content += nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override void CreateDataSource() { base.CreateDataSource(); DoCreateDataSource(); }" + nl;
            content += "        private void DoCreateDataSource()" + nl;
            content += "        {" + nl;
            content += "            DataSource = new " + dalClassName + "(true" + (eClass.Base == EntityClass.None ? "" : ", new IdTuple<long>(base.Id)") + ");" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).CreateDataSource))
                    content += "            " + DataFor(p).CreateDataSource.Replace(nl, nl + "            ") + nl;
            content += "        }" + nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override void LoadDataSource(long id) { base.LoadDataSource(id); DoLoadDataSource(id); }" + nl;
            content += "        private void DoLoadDataSource(long id)" + nl;
            content += "        {" + nl;
            content += "            // Le Load ne modifie pas les valeurs, il se contente de reconstruire la hierarchie de datasource" + nl;
            content += "            DataSource = CacheManager.Instance.Get<" + dalClassName + ">(new IdTuple<long>(id));" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).LoadDataSource))
                    content += "            " + DataFor(p).LoadDataSource.Replace(nl, nl + "            ") + nl;
            content += "        }" + nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override void ResetDataSourceEdits() { LockWhileUpdatingWith(() => { base.ResetDataSourceEdits(); DoResetDataSourceEdits(DataSource); }); if (!LockState && State.In(eState.Unsynchronized, eState.ToDelete)) InitializeState(eState.Synchronized); }" + nl;
            content += "        private void DoResetDataSourceEdits(" + dalClassName + " ds)" + nl;
            content += "        {" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).DoResetDataSourceEdits("ds")))
                    content += "            " + DataFor(p).DoResetDataSourceEdits("ds").Replace(nl, nl + "            ") + nl;
            content += "        }" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).LoadDataSourceFunctions))
                    content += "        " + DataFor(p).LoadDataSourceFunctions.Replace(nl, nl + "        ") + nl;
            content += nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] public override List<string> GetModelErrors() { return base.GetModelErrors().ConcatNullable(DoGetModelErrors()).ToList(); }" + nl;
            content += "        private List<string> DoGetModelErrors()" + nl;
            content += "        {" + nl;
            content += "            var errors = new List<string>();" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (DataFor(p).Validations.Count > 0)
                    content += "            " + DataFor(p).Validations.Select(pair => "if (" + pair.Item1  +")" + nl + "    errors.Add(" + pair.Item2 + ");").Join(nl).Replace(nl, nl + "            ") + nl;
            content += "            return errors;" + nl;
            content += "        }" + nl;
            content += nl;
            content += nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override void SaveDataSourceEdits() { base.SaveDataSourceEdits(); DoSaveDataSourceEdits(DataSource); }" + nl;
            content += "        private void DoSaveDataSourceEdits(" + dalClassName + " ds)" + nl;
            content += "        {" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).DoSaveDataSourceEdits("ds")))
                    content += "            " + DataFor(p).DoSaveDataSourceEdits("ds").Replace(nl, nl + "            ") + nl;
            content += "        }" + nl;
            content += nl;
            content += nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override void SaveScalarComposites() { base.SaveScalarComposites(); DoSaveScalarComposites(DataSource); }" + nl;
            content += "        private void DoSaveScalarComposites(" + dalClassName + " ds)" + nl;
            content += "        {" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).SaveScalarComposites))
                    content += "            " + DataFor(p).SaveScalarComposites.Replace(nl, nl + "            ") + nl;
            content += "        }" + nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override void SaveLinkComposites() { base.SaveLinkComposites(); DoSaveLinkComposites(); }" + nl;
            content += "        private void DoSaveLinkComposites()" + nl;
            content += "        {" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).SaveLinkComposites))
                    content += "            " + DataFor(p).SaveLinkComposites.Replace(nl, nl + "            ") + nl;
            content += "        }" + nl;
            content += nl;
            content += nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteLinkComposites() { DoDeleteLinkComposites(); base.DeleteLinkComposites(); }" + nl;
            content += "        private void DoDeleteLinkComposites()" + nl;
            content += "        {" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).DeleteLinkComposites))
                    content += "            " + DataFor(p).DeleteLinkComposites.Replace(nl, nl + "            ") + nl;
            content += "        }" + nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteScalarComposites() { DoDeleteScalarComposites(DataSource); base.DeleteScalarComposites(); }" + nl;
            content += "        private void DoDeleteScalarComposites(" + dalClassName + " ds)" + nl;
            content += "        {" + nl;
            bool any = false;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (!string.IsNullOrWhiteSpace(DataFor(p).DeleteScalarComposites))
                {
                    content += "            " + DataFor(p).DeleteScalarComposites.Replace(nl, nl + "            ") + nl;
                    any = true;
                }
            if (any)
                content += "            DoSaveScalarComposites(ds);" + nl;
            content += "        }" + nl;
            content += nl;
            content += nl;
            content += "        [DebuggerHidden, DebuggerStepThrough] protected override IEnumerable<BusinessObjectGenerated> GetAllModelComposites() { return base.GetAllModelComposites().Concat(DoGetAllModelComposites()); }" + nl;
            content += "        private IEnumerable<BusinessObjectGenerated> DoGetAllModelComposites()" + nl;
            content += "        {" + nl;
            content += "            var composites = new List<BusinessObjectGenerated>();" + nl;
            foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
                if (DataFor(p).GetAllModelComposites != null)
                    content += "            " + DataFor(p).GetAllModelComposites("composites").Replace(nl, nl + "            ") + nl;
            content += "            return composites.NotNull();" + nl;
            content += "        }" + nl;
            content += nl;
            content += nl;

            //protected virtual IEnumerable<BusinessObjectGenerated> GetAllModelComposites() { return new BusinessObjectGenerated[0]; }
            //content += nl;
            //content += nl;
            //content += "        [DebuggerHidden, DebuggerStepThrough] protected internal virtual IEnumerable<IGeneratedDALEntity> GetAllDAlObjectsToSynchronize() { return base.GetAllDAlObjectsToSynchronize().Concat(DoGetAllDAlObjectsToSynchronize()); }" + nl;
            //content += "        IEnumerable<IGeneratedDALEntity> DoGetAllDAlObjectsToSynchronize()" + nl;
            //content += "        {" + nl;
            //foreach (var p in eClass.Properties.Where(p => p.LinkType != eLinkType.BackwardLink))
            //    if (!string.IsNullOrWhiteSpace(DataFor(p).GetAllDAlObjectsToSynchronize))
            //        content += "            " + DataFor(p).GetAllDAlObjectsToSynchronize.Replace(nl, nl + "            ") + nl;
            //content += "        }" + nl;


            //content += "        [DebuggerHidden, DebuggerStepThrough] protected internal virtual IEnumerable<IGeneratedDALEntity> GetAllDAlObjectsToSynchronize() { return base.GetAllDAlObjectsToSynchronize().Concat(DoGetAllDAlObjectsToSynchronize()); }" + nl;
            content += "        public override List<BusinessDeleteActionDependency> GetEditToDoBeforeDelete()" + nl;
            content += "        {" + nl;
            content += "            var edits = base.GetEditToDoBeforeDelete();" + nl;
            // Parcours les types qui referencent l'objet courant ou un de ses ancêtres 
            var e = eClass;
            while (e != EntityClass.None)
            {
                //foreach (var pair in DataFor(eClass).Referencers)
                //    content += "            edits.AddRange(" + pair.Value.Replace(nl, nl + "            ") + ");" + nl;
                e = e.Base;
            }
            content += "            return edits;" + nl;
            content += "        }" + nl;

            content += "    }";
            return content;
        }
        public string GenerateDependencyOrderClass()
        {
            // Ce qui suit est foireux car si une entité "A" a une liste d'autre entité par aggregation,
            // les entités referencés doivent etre sauvegardés avant l'entité "A". Cela implique une dependence
            // En revanche si la liste etait une composition,on se moque de l'ordre puisque A s'assurera d'elle meme 
            // que les entites composite sont sauvegardé avant elle meme
            //var tri = Model.Entities.Where(e => DataFor(e).Table != null)
            //                        .Where(e => !(e is EntityEnum))
            //                        .OrderBy(e => DataFor(e).Table.Owner.Tables.IndexOf(DataFor(e).Table));

            Func<IEntity, IEnumerable<IEntity>> getDependencies = null;
            getDependencies = e =>
                {
                    var lst = DataFor(e).Dependencies.Keys.ToList();
                    //foreach(kvp in DataFor(e).Dependencies)
                    //    if ()
                    if (e is EntityClass && (e as EntityClass).Base != EntityClass.None)
                        lst.AddRange(getDependencies((e as EntityClass).Base));
                    for (int i = 0; i < lst.Count; ++i)
                        if (lst[i] is EntityClass)
                            lst.AddRange((lst[i] as EntityClass).ChildClasses);
                        else if (lst[i] is EntityInterface)
                        {
                            DebugTools.Break();
                            // TODO : Il faudrait aussi trouverl es interface qui herite de lst[i] et faire la meme chose recursivement !
                            lst.AddRange(Model.Entities.OfType<EntityClass>().Where(ec => ec.Interfaces.Contains((lst[i] as EntityInterface).Name)));
                        }
                    return lst;
                };
            //Func<IEntity, IEnumerable<IEntity>> getDependencies = null;
            //getDependencies = e => DataFor(e).Dependencies2;

            var tri = TopologicalOrderSimple.DoTopologicalSort(Model.Entities.ToList(), getDependencies)
                                            .Reverse<IEntity>();
            
            // Juste temporaire pour m'aider  a generer du code dans SchedulingProblem
            var sb = new StringBuilder();
            var mainTables = tri.OfType<EntityClass>().Select(e => DataFor(e).Table).ToList();
            foreach (var t in mainTables.Concat(Tables.Except(mainTables)))
                sb.AppendLine("dalInstances.AddRange(DAL." + DataFor(t).FullClassName + ".GetEntitiesWhere(null));");


            return "using System;" + nl
                 + "using System.Collections.Generic;" + nl
                 + GetEnumNamespaces().Select(ns => "using " + ns + ";" + nl).Join("")
                 + (Model.Entities.Count == 0 ? "" :
                    "using " + AssemblyName + "." + DataFor(Model.Entities.First()).Table.OwnerSchema().Name.Capitalize() + ";" + nl)
                 + nl
                 + nl
                 + "namespace " + ModelAssemblyName + nl
                 + "{" + nl
                 + "    public partial class BusinessObjectGenerated" + nl
                 + "    {" + nl
                 + "        public static readonly Dictionary<Type, int> AllEntityTypesInCreationOrder = new Dictionary<Type, int>()" + nl
                 + "        {" + nl
                 + tri.Where(e => !(e is EntityEnum)).Select((e, i) => "            { typeof(" + DataFor(e).ClassName + "), " + (i + 1) + "}," + nl).Join("")
                 + "        };" + nl
                 + "        public static readonly Dictionary<Type, int> AllEnumTypesInCreationOrder = new Dictionary<Type, int>()" + nl
                 + "        {" + nl
                 + tri.Where(e => e is EntityEnum).Select((e, i) => "            { typeof(" + e.Name + "), " + (i + 1) + "}," + nl).Join("")
                 + "        };" + nl
                 + "        public static readonly List<Type> AllDalLinkClasses = new List<Type>()" + nl
                 + "        {" + nl
                 + Tables.Except(tri.Where(e => e is EntityClass || e is EntityEnum || e is EntityStruct)
                                    .Select(e => DataFor(e).Table))
                         .Select(t => "            typeof(" + DataFor(t).ClassName + ")," + nl).Join("")
                 + "        };" + nl

                 + "        public static Type ToDalType(Type t)" + nl
                 + "        {" + nl
                 + "            Type dalType;" + nl
                 + "            _ToDalType.TryGetValue(t, out dalType);" + nl
                 + "            return dalType;" + nl
                 + "        }" + nl
                 + "        static readonly Dictionary<Type, Type> _ToDalType = new Dictionary<Type, Type>()" + nl
                 + "        {" + nl
                 + tri.Where(e => e is EntityClass).Select(e => "            { typeof(" + DataFor(e).ClassName + "), typeof(" + AssemblyName + "." + DataFor(DataFor(e).Table).FullClassName + ") }," + nl).Join("")
                 + "        };" + nl

                 + "        public static Type ToModelType(Type t)" + nl
                 + "        {" + nl
                 + "            Type modelType;" + nl
                 + "            _ToModelType.TryGetValue(t, out modelType);" + nl
                 + "            return modelType;" + nl
                 + "        }" + nl
                 + "        static readonly Dictionary<Type, Type> _ToModelType = new Dictionary<Type, Type>()" + nl
                 + "        {" + nl
                 + tri.Where(e => e is EntityClass).Select(e => "            { typeof(" + AssemblyName + "." + DataFor(DataFor(e).Table).FullClassName + "), typeof(" + DataFor(e).ClassName + ") }," + nl).Join("")
                 + "        };" + nl


                 + "    }" + nl
                 + "}" + nl;
        }

        public string GenerateBusinessObjectGeneratedBaseClass()
        {
            return
@"using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Model.Composition;

using GenericDALBasics;

using " + AssemblyName + @";


namespace " + ModelAssemblyName + (@"
{
    // TODO : https://fr.wikipedia.org/wiki/Mémento_(patron_de_conception)
    public abstract partial class BusinessObjectGenerated : Entity<IdTuple<long>>, IBusinessObjectGenerated
    {
        protected abstract Type ConcreteDataSourceType { get; }
        protected abstract IGeneratedDALEntity GetDataSource();
        protected virtual List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null) // GetDataSources().Last() == GetDataSource()
        {
            return lst ?? new List<IGeneratedDALEntity>();
        }

        public abstract long Id          { get; }       long IBusinessObjectGenerated.Id { get { return Id; } }
        public virtual bool IsModified   { get { return State.HasPendingChanges(); } }
        public string Changes            { get { return Dump(false); } }
        public string Dump()             { return Dump(true); }
        public string ToJSON()           { return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(this); } // Include in project System.Web.Extensions // TODO : voir http://stackoverflow.com/a/26406504

        protected internal virtual string Dump(bool all)
        {
            return all ? TypedId.ToSmartString() + {DQ} {DQ} + State.GetDescription()
                 : State == eState.Deleted || State == eState.Synchronized
                 ? string.Empty
                 : State.GetDescription() + (State == eState.ToDelete
                                            ? {DQ} {DQ} + TypedId.ToSmartString()
                                            : string.Empty);
        }

        protected static readonly string ChangeIndent = Environment.NewLine + {DQ}   {DQ};

        public BusinessObjectGenerated(long id)
        {
            //InitDirtynessRelays();
            Init();
            LoadById(id);
        }
        protected BusinessObjectGenerated()
        {
            //InitDirtynessRelays();
            Init();
            CreateDataSource();
            ResetDataSourceEdits();
            InitializeState(eState.New);
            RaiseIdChanged();
        }
        protected virtual void Init() { }
        //void InitDirtynessRelays()
        //{
        //    if (DirtynessRelays != null)
        //        foreach (var prop in DirtynessRelays)
        //            AddDirtynessRelayFor(prop, null);
        //}
        //protected static readonly string[] _DirtynessRelays = new string[] { };
        protected void AssignWithDirtynessRelay<T>(ref T backField, T newValue)
            where T : class, IPropertyNotifierObject
        {
            if (backField != null)
                ((IPropertyNotifierObject)backField).DirtyChanged -= SubObject_DirtyChanged;
            backField = newValue;
            if (newValue != null)
                ((IPropertyNotifierObject)newValue).DirtyChanged += SubObject_DirtyChanged;
        }


        public void LoadById(long id)
        {
            bool idChanged = GetDataSource() == null || id != Id;
            LoadDataSource(id);
            ResetDataSourceEdits();
            InitializeState(eState.Synchronized);
            if (idChanged)
                RaiseIdChanged();
        }
        public override void SynchronizeToDatabase()
        {
            if (State == eState.Deleted || State == eState.Synchronized)
                return;
            if (State == eState.New || State == eState.Unsynchronized)
            {
                Validate();
                SaveDataSourceEdits(); // Sauvegarde la Datasource (qui peut referencer les composites scalar)
                SaveScalarComposites(); // Sauvegarde les relations 1 - 1
                foreach (var ds in GetDataSources())
                    ds.SynchronizeToDatabase();
                SaveLinkComposites(); // Sauvegarde les links (table de jointure, relation n-n ou 1-n) qui referencent les composants scalar et this
            }
            else if (State == eState.ToDelete)
            {
                DeleteLinkComposites();
                foreach (var ds in GetDataSources().Reverse<IGeneratedDALEntity>())
                    ds.Delete();
                DeleteScalarComposites(); // Ce sont les ds qui reference les objets composites
            }
            UpdateState();
        }
        public virtual List<string> GetModelErrors()
        {
            return new List<string>();
        }
        public virtual void Validate()
        {
            var errors = GetModelErrors();
            if (errors != null && errors.Count > 0)
                " + "throw new FunctionalException(errors.Select(e => \" -  \" + e.Replace(nl, nl + \"    \")).Join(nl), null);" + @"
        }

        //protected internal virtual IEnumerable<IGeneratedDALEntity> GetAllDAlObjectToSynchronize()
        //{
        //    return GetDataSources().Concat(get());
        //}

        //private IEnumerable<IGeneratedDALEntity> get()
        //{
        //    yield return null;
        //}
        //public static void SynchronizeToDatabase(List<BusinessObjectGenerated> objects)
        //{
        //    var allObjects = objects.ToList();
        //    var queue = new Queue<BusinessObjectGenerated>(objects);
        //    while (queue.Count > 0)
        //    {
        //        var obj = queue.Dequeue();
        //        obj.GetAllComposites();
        //    }
        //    foreach(var obj in allObjects)
        //        allObjects.AddRange(obj.GetAllComposites())
        //    Topolo
        //}
        public static List<BusinessObjectGenerated> GetAllChanges()
        {
            var objs = CacheManager.Instance.GetObjectsInMemory()
                                   .OfType<BusinessObjectGenerated>()
                                   .Where(obj => obj.State.In(eState.New, eState.Unsynchronized, eState.ToDelete))
                                   .Except(_IgnoredConstObjects)
                                   .ToList();
            return objs;
        }

        public static void SynchronizeToDatabase(List<BusinessObjectGenerated> objects)
        {
            var tri = objects.OrderBy(o => AllEntityTypesInCreationOrder[o.GetType()]).ToList();
            
            var errors = tri.Select(obj => obj.GetModelErrors().Select(e => {DQ} - {DQ} + e.Replace(nl, nl + {DQ}    {DQ})).Join(nl)
                                             .PrefixIfNotEmptyWith(obj.TypeName + {DQ} \{DQ}{DQ} + obj.UserFriendlyName + {DQ}\{DQ}{DQ} + nl))
                            .NotBlank()
                            .Join(nl + nl);
            if (!string.IsNullOrWhiteSpace(errors))
                throw new FunctionalException(errors, null);
            GeneratedDAO.Instance.WithConnectionAndTransaction((con, tran) =>
            {
                int i = 0;
                foreach (var obj in tri)
                {
                    Debug.WriteLine((i + 1).ToString() + {DQ} / {DQ} + tri.Count);
                    obj.SynchronizeToDatabase();
                    ++i;
                }
                tran.Commit();
            });
        }
        public static T DeclareAsStaticImmutable<T>(T obj)
            where T : BusinessObjectGenerated
        {
            var allObjects = new[] { obj }.Concat(obj.GetAllModelCompositesRecursively()).ToList();
            foreach (var o in allObjects)//.AsEnumerable().Reverse())
            {
                if (o.IsNew)
                    o.InitializeState(eState.New); // Remet IsDirty à false;
                o.IsReadOnly = true; // Securité
                _IgnoredConstObjects.Add(o); // S'assure que l'objet ne changera pas pour ne pas se retrouver dans la liste généré par BusinessObjectGenerated.GetAllChanges()
            }
            return obj;
        }
        static readonly List<BusinessObjectGenerated> _IgnoredConstObjects = new List<BusinessObjectGenerated>();


        protected virtual string[] DirtynessRelays { get { return null; } }
        protected virtual void CreateDataSource() { }
        protected virtual void LoadDataSource(long id) { }
        // Copie les données du/des datasource(s) vers l'objet métier
        protected virtual void ResetDataSourceEdits() { }  void IBusinessObjectGenerated.ResetDataSourceEdits() { ResetDataSourceEdits(); } 
        protected static     T ResetDataSourceEditsRecursive<T>(T bo) where T : IBusinessObjectGenerated { if (bo != null) bo.ResetDataSourceEdits(); return bo; } // Permet d'appeler une methode sur un expression inline
        // Copie les données de l'objet metier vers le/les datasource(s)
        // Copie les données de l'objet métier vers le(s) datasource(s)
        protected virtual void SaveDataSourceEdits() { } void IBusinessObjectGenerated.SaveDataSourceEdits() { SaveDataSourceEdits(); } 
        protected static     T SaveDataSourceEditsRecursive<T>(T bo) where T : IBusinessObjectGenerated { if (bo != null) bo.SaveDataSourceEdits(); return bo; } // Permet d'appeler une methode sur un expression inline
        protected virtual void SaveScalarComposites() { }
        protected virtual void SaveLinkComposites() { }
        protected virtual void DeleteScalarComposites() { }
        protected virtual void DeleteLinkComposites() { }

        protected T CreateCompositeObject<T>()
            where T : BusinessObjectGenerated, new()
        {
            var t = new T();
            if (OnCompositeObjectCreated != null)
                OnCompositeObjectCreated(this, t);
            return t;
        }
        public static event Action<BusinessObjectGenerated, BusinessObjectGenerated> OnCompositeObjectCreated;

        protected static  IEnumerable<BusinessObjectGenerated> GetAllModelComposites(BusinessObjectGenerated bo) { return bo.GetAllModelComposites(); }
        protected virtual IEnumerable<BusinessObjectGenerated> GetAllModelComposites() { return new BusinessObjectGenerated[0]; }
        protected virtual IEnumerable<BusinessObjectGenerated> GetAllModelCompositesRecursively()
        {
            return GetAllModelComposites().SelectMany(o => new[] { o }.Concat(o.GetAllModelCompositesRecursively()));
        }



        protected override IdTuple<long>? ClosedId { get { return Id == 0 ? (IdTuple<long>?)null : new IdTuple<long>(Id); }
                                                     set { Debug.Assert(Id == (long)value.Value.Keys[0]); } }
        public sealed override void LoadById(IdTuple<long> id)
        {
            LoadById((long)id.Keys[0]);
        }
        public override void Reload()
        {
            CancelChanges();
        }
        public void Delete()
        {
            if (State == eState.Deleted)
                return;
            UpdateState(eState.ToDelete);
            SynchronizeToDatabase();
        }

        // Les deux arguments de tyoe TBO sont en realité une référence à this. Cela permet simplement que les lambda générés soient static et donc ne fassent pas d'allocation.
        // http://stackoverflow.com/questions/5568294/compiled-c-sharp-lambda-expressions-performance
        public List<TDal> GetEntitiesWhere<TBO, TDal>(Func<TBO, TDal, bool> predicate, Func<TBO, List<TDal>> dbCall)
            where TDal : class, IHasTypedId, ILoadableById, IGeneratedDALEntity // first constraints are mandatory (CacheManager)
        {
            var @this = (TBO)(object)this;
            if (TakeObjectsFromMemoryInsteadQueryDB)
                return CacheManager.Instance.GetObjectsInMemory<TDal>().Where(dal => predicate(@this, dal)).ToList();
            else
                return dbCall(@this);
        }
        public static bool TakeObjectsFromMemoryInsteadQueryDB = false;

        
        protected static TBO GetObjectImplementingAmong<TBO>(long id, params Type[] types)
        {
            foreach (var t in types)
            {
                var res = CacheManager.Instance.Get(new TypedId<IdTuple<long>>(t, new IdTuple<long>(id)), true);
                if (res != null)
                    return (TBO)res;
            }

            foreach (var t in types)
                try { return (TBO)CacheManager.Instance.Get(new TypedId<IdTuple<long>>(t, new IdTuple<long>(id))); }
                catch { }
            throw new Exception({DQ}Not found !{DQ});
        }
        protected static NotifyCollectionChangedEventHandler BuildDictionaryCollectionChangedHandler<TKey, TValue, TDalObject>(Func<bool> isReloading, IDictionary<TKey, TValue> Dico, IDictionary<TKey, TDalObject> _Dico, Func<KeyValuePair<TKey, TValue>, TDalObject> createDalObject)
            where TDalObject : GeneratedDALEntity<IdTuple<long>>
        {
            return (_, e) =>
            {
                if (isReloading())
                    return;
                if (e.Action == NotifyCollectionChangedAction.Add)
                    foreach (KeyValuePair<TKey, TValue> kvp in e.NewItems)
                        _Dico.Add(kvp.Key, createDalObject(kvp));
                else if (e.Action == NotifyCollectionChangedAction.Remove)
                    foreach (KeyValuePair<TKey, TValue> kvp in e.OldItems)
                        _Dico.Remove(kvp.Key);
                else if (e.Action == NotifyCollectionChangedAction.Replace)
                {
                    foreach (KeyValuePair<TKey, TValue> kvp in e.OldItems)
                        _Dico.Remove(kvp.Key); // delete before add
                    foreach (KeyValuePair<TKey, TValue> kvp in e.NewItems)
                        _Dico.Add(kvp.Key, createDalObject(kvp));
                }
                else if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    if (Dico.Count == 0)
                        _Dico.Clear();
                    else
                        Debug.Assert(false, {DQ}todo !{DQ});
                }
            };
        }

        public static string DumpAllObjectsInMemory(List<object> potentialObjs = null)
        {
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            return DumpAllDALObjectsInMemory(objs)
                 + Environment.NewLine
                 + DumpAllBusinessObjectsInMemory(objs);
        }

        public virtual List<BusinessDeleteActionDependency> GetEditToDoBeforeDelete()
        {
            return new List<BusinessDeleteActionDependency>();
        }

        public static string DumpAllDALObjectsInMemory(List<object> potentialObjs = null)
        {
            var sb = new StringBuilder();
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            sb.AppendLine({DQ}DAL Objects:{DQ});
            foreach (var obj in objs)
            {
                var de = obj as IGeneratedDALEntity;
                if (de == null)
                    continue;
                sb.Append({DQ}   {DQ});
                sb.AppendLine(de.TypedId.AsDebugString);
            }
            return sb.ToString();
        }
        public static string DumpAllBusinessObjectsInMemory(List<object> potentialObjs = null)
        {
            var sb = new StringBuilder();
            sb.AppendLine({DQ}Business Objects:{DQ});
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            foreach (var obj in objs)
            {
                var bo = obj as BusinessObjectGenerated;
                if (bo == null)
                    continue;
                sb.Append({DQ}   {DQ});
                sb.AppendLine(bo.TypedId.AsDebugString);
            }
            return sb.ToString();
        }
        protected static readonly string nl = Environment.NewLine;
    }
    public interface IBusinessStructGenerated
    {
        string AsDebugString { get; }
    }
    public static class IBusinessStructGenerated_Extensions
    {
        public static string DisplayDiff<T>(CheckableCompositeListGenerated<T> links, bool all)
            where T : IGeneratedDALEntity
        {
            Debug.Assert(typeof(T).TryGetNullableType() == null); // T n'est pas nullable
            var sb = new StringBuilder();
            Func<T, string> display = v => typeof(IBusinessStructGenerated).IsAssignableFrom(typeof(T)) ? ((IBusinessStructGenerated)v).AsDebugString : v.ToString();
            foreach (T link in links.DeletedObjects.Concat(links))
                if (link.State.NotIn(eState.Synchronized, eState.Deleted) || all)
                    sb.AppendLine({DQ}   - {DQ} + link.State + {DQ} {DQ} + link.TypedId.AsDebugString);
            return sb.ToString();
        }
        public static string DisplayDiff<TKey, TValue>(string propName, CompositeDictionary<TKey, TValue> dico, bool all)
            where TValue : IGeneratedDALEntity
        {
            Debug.Assert(typeof(TValue).TryGetNullableType() == null); // TValue n'est pas nullable
            var sb = new StringBuilder();
            Func<TValue, string> display = v => typeof(IBusinessStructGenerated).IsAssignableFrom(typeof(TValue)) ? ((IBusinessStructGenerated)v).AsDebugString : v.ToString();
            foreach (var kvp in dico.DeletedObjects.OrderBy(kvp => kvp.Key).Concat(dico.OrderBy(kvp => kvp.Key))) // Tri par clef pour avoir des dump deterministe
                if (kvp.Value.State.NotIn(eState.Synchronized, eState.Deleted) || all)
                    sb.AppendLine({DQ}   - {DQ} + kvp.Value.State + {DQ} {DQ} + kvp.Value.TypedId.AsDebugString);
            if (sb.Length > 0)
                return propName + {DQ}: {DQ} + Environment.NewLine + sb.ToString();
            return sb.ToString();
        }
        //public static string DisplayDiff<T>(List<T> values, IEnumerable<T> before_)
        //    where T : struct
        //{
        //    Debug.Assert(typeof(T).TryGetNullableType() == null); // T n'est pas nullable
        //    var sb = new StringBuilder();
        //    var before = before_.ToList();
        //    Func<T, string> display = v => typeof(IBusinessStructGenerated).IsAssignableFrom(typeof(T)) ? ((IBusinessStructGenerated)v).AsString : v.ToString();
        //    for (int v = 0; v < values.Count; ++v)
        //    {
        //        bool found = false;
        //        for (int b = 0; b < before.Count; ++b)
        //            if (object.Equals(values[v], before[b]))
        //            {
        //                sb.AppendLine({DQ}[=] {DQ} + display(values[v]));
        //                values.RemoveAt(v);
        //                --v;
        //                found = true;
        //                break;
        //            }
        //        if (!found)
        //            sb.AppendLine({DQ}[+] {DQ} + display(values[v]));
        //    }
        //    for (int b = 0; b < before.Count; ++b)
        //        sb.AppendLine({DQ}[-] {DQ} + display(before[b]));
        //    return sb.ToString();
        //}
    }


    public static class EnumConversionExtensions
    {
        public static TEnum? ToEnum<TEnum>(this int? v)
            where TEnum : struct
        {
            if (v.HasValue)
                return (TEnum)(object)v.Value;
            return null;
        }

    }
}
").Replace("{DQ}", "\"");

        }
    }
}
