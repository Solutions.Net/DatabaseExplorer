﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Microsoft.SqlServer.Management.Smo;

using SubSonic.Extensions;

using TechnicalTools;

using DatabaseExplorer.DAL;
using DatabaseExplorer.Business.Model;
using DatabaseExplorer.Business.Model.Local;
using DatabaseExplorer.Business.Extensions;

using Property = DatabaseExplorer.Business.Model.Property;

namespace DatabaseExplorer.Business
{
    public partial class FullGeneratedProject : Project
    {
        public override string GenerateSqlDataInitialization()
        {
            var sb = new StringBuilder(base.GenerateSqlDataInitialization());
            sb.AppendLine();
            sb.AppendLine("------------------------------ Enum Values ------------------------------");
            sb.AppendLine();
            foreach (var e in Model.Entities.OfType<EntityEnum>())
            {
                foreach (var v in e.EnumValues)
                {
                    sb.Append("INSERT INTO " + (ForSqlCE ? "" : "[" + DataFor(e).Table.OwnerSchema().Name + "].") + "[" + DataFor(e).Table.Name + "] ");
                    sb.Append("( " + DataFor(e).Table.Columns.Cast<Column>().Select(c => "[" + c.Name + "]").Join() + ") ");
                    sb.AppendLine("VALUES ( " + v.Value + ", '" + v.Name + "');");
                    sb.AppendLine("GO");
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
