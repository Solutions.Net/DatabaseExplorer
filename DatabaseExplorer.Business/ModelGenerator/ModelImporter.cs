﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using System.Drawing;

using TechnicalTools;
using TechnicalTools.Annotations.Design;
using TechnicalTools.Diagnostics;

using DatabaseExplorer.DAL;
using DatabaseExplorer.Business.Model;


namespace DatabaseExplorer.Business
{
    public class ModelImporter
    {
        public ModelUML RunImport(Assembly designAssembly, string schemaName = "dbo")
        {
            var m = new ModelUML(schemaName);
            var types = designAssembly.GetTypes().ToList();

            var typeToEntities = new Dictionary<Type, IEntity>();
            foreach(var type in types)
            {
                if (FindEnumAndCreateEntity(m, typeToEntities, type))
                    continue; // enum created !
                if (FindInterfaceAndCreateEntity(m, typeToEntities, type, designAssembly))
                    continue; // interface created !
                EntityClassOrStruct entity;
                string name = Regex.Replace(type.ToSmartString(), "`", "_"); // Gère les interfaces génériques  Foo<T> => Foo_T (TODO : tester un type avec deux arguments comme Dictionary<int, int>)
                if (name.Contains("<"))
                    name = name.Replace("<", "Of").Replace(">", "").Replace(",", "_").Replace(" ", "");
                if (type.IsValueType) // Ce ne peut etre qu'une struct
                    entity = new EntityStruct(name);
                else
                    entity = new EntityClass(name) { IsAbstract = type.IsAbstract };
                typeToEntities.Add(type, entity);
                m.Entities.Add(entity);
            }

            foreach (var type in types)
            {
                if (!(typeToEntities[type] is EntityClassOrStruct))
                    continue;
                var entity = (EntityClassOrStruct)typeToEntities[type];

                if (type.BaseType == typeof(object) || type.BaseType == typeof(ValueType))
                {
                    //entity.Base = EntityClass.None;
                }
                else if (entity is EntityClass)
                {
                    if (!type.BaseType.IsGenericType)
                        ((EntityClass)entity).Base = (EntityClass)typeToEntities[type.BaseType];
                    else
                    {
                        ((EntityClass)entity).Base = (EntityClass)typeToEntities[type.BaseType.GetGenericTypeDefinition()];
                        foreach (var arg in type.BaseType.GetGenericArguments())
                        {
                            //if (arg.IsSpe)
                            //entity.BaseGenericArguments
                        }

                        //BaseGenericArguments
                    }
                }
                foreach (var i in type.GetInterfaces())
                {
                    var name = Regex.Replace(i.ToSmartString(), "`", "_");
                    if (!type.BaseType.GetInterfaces().Select(ii => ii.FullName).Contains(name))
                    {
                        entity.Interfaces.Add(name);
                        // TODO : Ajouter dans EntityInterfaec.ChildClasses, l'entité en cours
                    }
                }
                
                var designAtts = type.GetCustomAttributes<TechnicalTools.Annotations.Design.EntityAttribute>();
                foreach (var att in designAtts)
                {
                    att.Build(type);
                    if (att is Implements)
                        entity.Interfaces.Add(Regex.Replace((att as Implements).Interface.ToSmartString(true), "`", "_"));
                    else if (att is WhereParamImplements)
                        entity.GenericConstraints.Add(att as WhereParamImplements);
                    //else if (att is Inherits)
                    //    entity.Base = (att as Inherits).BaseClass;
                }

                var properties = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                string props_ko = properties.Where(p => !p.IsAutoProperty()).Select(p => p.Name).Join();
                if (!string.IsNullOrEmpty(props_ko))
                    throw new Exception(string.Format("Les propriétés suivantes appartenant à la classe {0}, doivent être auto-implémentées :", type.Name) + Environment.NewLine + props_ko);
                var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                string fields_ko = fields.Where(f => !f.IsBackingFieldOfAutoProperty()).Select(f => f.Name).Join();
                if (!string.IsNullOrEmpty(fields_ko))
                    throw new Exception(string.Format("Les champs suivants appartenant à la classe {0}, doivent être écrit sous forme d'auto property !", type.Name) + Environment.NewLine + fields_ko);
                foreach (var prop in properties)
                {
                    var propType = prop.PropertyType.TryGetNullableType() ?? prop.PropertyType;
                    var property = new Property(prop.Name);

                    FindEnumAndCreateEntity(m, typeToEntities, prop.PropertyType);
                    FindInterfaceAndCreateEntity(m, typeToEntities, prop.PropertyType, designAssembly);

                    property.IsGetterPublic = prop.GetGetterAccessibility() ?? eAccessibility.Private;
                    property.IsSetterPublic = prop.GetSetterAccessibility() ?? eAccessibility.Private;
                    property.LinkType = prop.GetCustomAttribute<IsTechnicalOwner>() != null ? eLinkType.BackwardLink : eLinkType.ForwardLink;

                    if (propType.IsPrimitive || propType.IsEnum || propType.In(typeof(string), typeof(DateTime), typeof(TimeSpan)))
                    {
                        property.Type = propType.IsEnum 
                                        ? m.AllTypes.OfType<PropertyTypeEntity>().First(t => t.Type == typeToEntities[propType])
                                        : (PropertyType)PropertyTypeNative.FromCSharpType(propType);
                        property.Nullable = prop.PropertyType.TryGetNullableType() != null || propType == typeof(string);
                        //property.IsComposite = true;
                    }
                    else if (propType == typeof(System.Drawing.Color))
                    {
                        property.Type = PropertyTypeNative.Int32;
                        property.Nullable = prop.PropertyType.TryGetNullableType() != null;
                        //property.IsComposite = true;
                    }
                    else if (propType.IsValueType &&   // Cas d'une struct 
                             types.Contains(propType)) // mais user defined (pas du system ou d'une autre lib)
                    {
                        property.Type = m.AllTypes.OfType<PropertyTypeEntity>().First(t => t.Type == typeToEntities[propType]);
                        property.Nullable = prop.PropertyType.TryGetNullableType() != null;
                        //property.IsComposite = true;
                    }
                    else if (propType.IsArray)
                    {
                        var eltType = propType.GetElementType();
                        if (!eltType.IsValueType)
                            throw new Exception("Si type d'element de l'array est de type reference alors utilisez une liste !");
                        property.Type = PropertyTypeNative.ByteArray;
                        property.Nullable = prop.GetCustomAttribute<NotNullAttribute>() == null;
                    }
                    else if (propType.IsGenericType && propType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        var eltType = propType.GetGenericArguments()[0];

                        FindEnumAndCreateEntity(m, typeToEntities, eltType);
                        FindInterfaceAndCreateEntity(m, typeToEntities, eltType, designAssembly);

                        if (eltType.TryGetNullableType() != null)
                            throw new Exception("Si les collections prennent des value types, le type ne doit pas être nullable!");
                        eltType = eltType.TryGetNullableType() ?? eltType;
                        var eltEntity = (PropertyTypeScalar)m.AllTypes.OfType<PropertyTypeNative>().FirstOrDefault(t => t.Type == eltType)
                                                         ?? m.AllTypes.OfType<PropertyTypeEntity>().First(t => t.Type == typeToEntities[eltType]);

                        property.Type = m.AllTypes.OfType<PropertyTypeCollection>().FirstOrDefault(c => c.Type == eltEntity);
                        if (property.Type == null)
                        {
                            Debug.Assert(eltEntity is PropertyTypeEntity, "Les collections pour les types natifs ont déjà été créée !");
                            property.Type = new PropertyTypeCollection((PropertyTypeEntity)eltEntity);
                            m.AllTypes.Add(property.Type);
                        }
                        property.Nullable = true;
                        property.IsComposite = prop.GetCustomAttribute<CompositeAttribute>() != null ;
                                            //|| eltEntity is PropertyTypeNative
                                            //|| eltEntity is PropertyTypeEntity && (eltEntity as PropertyTypeEntity).Type is EntityStruct;
                    }
                    else if (propType.IsGenericType && propType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
                    {
                        var keyType = propType.GetGenericArguments()[0];
                        FindEnumAndCreateEntity(m, typeToEntities, keyType);
                        FindInterfaceAndCreateEntity(m, typeToEntities, keyType, designAssembly);
                        keyType = keyType.TryGetNullableType() ?? keyType;
                        var keyEntity = (PropertyTypeScalar)m.AllTypes.OfType<PropertyTypeNative>().FirstOrDefault(t => t.Type == keyType)
                                                         ?? m.AllTypes.OfType<PropertyTypeEntity>().First(t => t.Type == typeToEntities[keyType]);

                        var valueType = propType.GetGenericArguments()[1];
                        FindEnumAndCreateEntity(m, typeToEntities, valueType);
                        FindInterfaceAndCreateEntity(m, typeToEntities, valueType, designAssembly);
                        valueType = valueType.TryGetNullableType() ?? valueType;
                        var valueEntity = (PropertyTypeScalar)m.AllTypes.OfType<PropertyTypeNative>().FirstOrDefault(t => t.Type == valueType)
                                                           ?? m.AllTypes.OfType<PropertyTypeEntity>().First(t => t.Type == typeToEntities[valueType]);

                        property.Type = m.AllTypes.OfType<PropertyTypeDictionary>().FirstOrDefault(d => d.KeyType == keyEntity && d.ValueType == valueEntity);
                        if (property.Type == null)
                        {
                            property.Type = new PropertyTypeDictionary(keyEntity, valueEntity);
                            m.AllTypes.Add(property.Type);
                        }
                        property.Nullable = true;
                        property.IsComposite = prop.GetCustomAttribute<CompositeAttribute>() != null;
                    }
                    else if (propType.IsClass && typeToEntities.ContainsKey(propType))
                    {
                        property.Type = m.AllTypes.OfType<PropertyTypeEntity>().First(t => t.Type == typeToEntities[propType]);
                        property.Nullable = prop.GetCustomAttribute<NotNullAttribute>() == null;
                        property.IsComposite = prop.GetCustomAttribute<CompositeAttribute>() != null;
                    }
                    else if (propType.IsInterface)
                    {
                        property.Type = m.AllTypes.OfType<PropertyTypeEntity>().First(t => t.Type == typeToEntities[propType]);
                        property.Nullable = prop.GetCustomAttribute<NotNullAttribute>() == null;
                        property.IsComposite = prop.GetCustomAttribute<CompositeAttribute>() != null;
                        //complexProperties.Add(prop, table);
                    }
                    else
                        throw new Exception("Unknown case (yet)!");
                    entity.Properties.Add(property);
                }

                var eStruct = entity as EntityStruct;
                if (eStruct != null)
                {
                    eStruct.SizeOfStruct = 0;
                    eStruct.PropertyOffsets.Clear();
                    if (properties.Any(p => p.GetCustomAttribute<PropertyOffset>() == null))
                        throw new Exception(string.Format("Les propriétés de la structure {0} doivent toutes avoir l'attribut PropertyOffset afin d'indiquer la position des champs au sein de la structure !", type.Name));
                    int curOffset = 0;
                    foreach(var p in properties.OrderBy(p => p.GetCustomAttribute<PropertyOffset>().FieldOffset))
                    {
                        var attOffset = p.GetCustomAttribute<PropertyOffset>();
                        if (p.PropertyType == typeof(string))
                            throw new Exception("Le type string n'est pas autorisé dans les struct, utiliser char[]!"); // La raison etant qu'on souhaite avoir des structure de taille fixe (notemment pour l'enregistrement en BDD)
                        if (p.PropertyType.TryGetNullableType() != null)
                            throw new Exception("Les types nullables ou référence ne sont pas autorisé au sein d'une structure ! Ajouter un booleen indiquant si la valeur a du sens");
                        if (!p.PropertyType.IsPrimitive && p.PropertyType.NotIn(typeof(DateTime), typeof(TimeSpan), typeof(Color)))
                            throw new Exception("Les types nullables ou référence ne sont pas autorisé au sein d'une structure !");

                        if (attOffset.FieldOffset < curOffset)
                            throw new Exception(string.Format("La propriete {0} au sein de la structure {1} est mal positionné (offset) Ele ecrasera le champs precedent !", p.Name, eStruct.Name));
                        eStruct.PropertyOffsets.Add(eStruct.Properties.First(p2 => p2.Name == p.Name), attOffset.FieldOffset);
                        curOffset = attOffset.FieldOffset + Marshal.SizeOf(p.PropertyType);
                        //object value = Activator.CreateInstance(p.PropertyType);
                        //entity.SizeOfStruct += 
                    }
                    // Max au cas ou on specifierait la taille de la structure un jour 
                    // (genre la structure est plus grande que necessaire car il y a du padding a la fin)
                    eStruct.SizeOfStruct += Math.Max(eStruct.SizeOfStruct, curOffset);
                }
            }
            return m;
        }

        bool FindEnumAndCreateEntity(ModelUML m, Dictionary<Type, IEntity> dico, Type type)
        {
            type = type.TryGetNullableType() ?? type;
            if (type.IsEnum && !dico.ContainsKey(type))
            {
                var entity = new EntityEnum(type.Name, type.Namespace);
                entity.BaseType = PropertyTypeNative.FromCSharpType(Enum.GetUnderlyingType(type));
                var distinct_values = Enum.GetValues(type).Cast<Enum>()
                                          .Select(v => (ulong)Convert.ChangeType(v, typeof(ulong)))
                                          .Distinct() // Elimine les doublons (valeur d'enum ayant la meme representation)
                                          .OrderBy(v => // Tri par nombre de bit setté afin de plus loin prendre en priorite les flag les plus simples
                                           {
                                               int count = 0; while (v > 0)
                                               {
                                                   v &= v - 1;
                                                   count++;
                                               }
                                               return count;
                                           })
                                           .ToList();

                var combinedValues = new List<EnumValue>();

                var minimalValueSet = new List<ulong>();
                // Ajoute d'office la premiere valeur car si il s'agit de 0, la boucle ci dessous ne fonctionnera pas 
                if (distinct_values.Count > 0 && distinct_values[0] == 0)
                { 
                    combinedValues.Add(new EnumValue( Enum.GetName(type, 0), 0));
                    distinct_values.RemoveAt(0);
                }
                // ex: les value de l'enum : 1 2 3 4 6 ==> Il faut virer 1 (le deuxieme), 3 et 6 qui peuvent etre composé par combinaison des valeur precedentes
                for (int i = 0; i < distinct_values.Count; ++i) 
                {
                    ulong v = distinct_values[i];
                    Debug.Assert(v != 0);
                    for (int j = 0; j < i; ++j)
                        v &= ~distinct_values[j];
                    if (v != 0)
                        minimalValueSet.Add(distinct_values[i]);
                }
                
                if (minimalValueSet.Count > 16) // plus une securité qu'une vrai limitation. C'est juste que ca genere beaucoup de valeur apres ...
                    throw new Exception(string.Format("Too many values for an enum {0}!", type.Name)); // TODO : mettre une option pour autoriser...

                var name = new List<string>();
                for (ulong n = 1; n < (1UL << minimalValueSet.Count); ++n) // i parcours le nombre de combinaison possible.
                {
                    ulong v = 0;
                    name.Clear();
                    for (int b = 0; b < minimalValueSet.Count; ++b) // Chaque bit du nombre i indique quel valeur de l'enum il faut combiner 
                        if ((n & (1UL << b)) != 0) // le i-ieme bit indique qu'il faut ajouter le i-ieme enum a la valeur a générer
                        {
                            name.Add(Enum.GetName(type, minimalValueSet[b]));
                            v = v | minimalValueSet[b];
                        }
                    combinedValues.Add(new EnumValue(name.Reverse<string>().Join(" | "), v));
                }
                entity.EnumValues.AddRange(combinedValues);
                dico.Add(type, entity);
                m.Entities.Add(entity);
                return true;
            }
            else if (type.IsGenericType)
            {
                foreach (var argType in type.GetGenericArguments())
                    return FindEnumAndCreateEntity(m, dico, argType);
            }
            return false;
        }

        bool FindInterfaceAndCreateEntity(ModelUML m, Dictionary<Type, IEntity> dico, Type type, Assembly designAssembly)
        {
            if (type.IsInterface && !dico.ContainsKey(type))
            {
                if (type.IsGenericType)
                {
                    // TODO ?!
                    DebugTools.Break();
                }
                string name = Regex.Replace(type.ToSmartString(true), "`", "_"); // Gère les interfaces génériques  Foo<T> => Foo_T (TODO : tester un type avec deux argument comme Dictionary<int, int>)
                var entity = new EntityInterface(name);
                entity.FullName = type.FullName;
                entity.IsInModeling = type.Assembly == designAssembly;
                dico.Add(type, entity);
                m.Entities.Add(entity);
                return true;
            }
            else if (type.IsGenericType)
            {
                foreach(var argType in type.GetGenericArguments())
                    return FindInterfaceAndCreateEntity(m, dico, argType, designAssembly);
            }
            return false;
        }
    }
}
