﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;


using DatabaseExplorer.Business.Model;

using Property = DatabaseExplorer.Business.Model.Property;
using DatabaseExplorer.Business.Extensions;

namespace DatabaseExplorer.Business
{
    public partial class DynamicTypeModelGenerator : DynamicTypeCodeGeneratorForKronos
    {
        readonly Dictionary<IEntity, EntityData>   _entityDatas = new Dictionary<IEntity, EntityData>();
        readonly Dictionary<Property, PropertyData> _propertyDatas = new Dictionary<Property, PropertyData>();


        protected EntityData DataFor(IEntity e)
        {
            EntityData data;
            if (!_entityDatas.TryGetValue(e, out data))
            {
                data = new EntityData(e, (Property p) => DataFor(p), Tables);

                _entityDatas.Add(e, data);
            }
            return data;
        }
        protected class EntityData
        {
            public readonly IEntity Entity;
            public readonly Table Table;

            // Pour s'assurer que l'entite peut être sauvegardé / supprimé
            // Pair<Entité devant etre crée avant un objet de type \"Entity\", flas indiquant si la relation est du a une composition ou une aggregation>
            public Dictionary<IEntity, eRelationKind> Dependencies = new Dictionary<IEntity, eRelationKind>();
            public void AddDependency(IEntity e, eRelationKind t)
            {
                if (Dependencies.ContainsKey(e))
                    Dependencies[e] |= t;
                else
                    Dependencies.Add(e, t);
            }

            // Entities that have "Entity" in their Dependencies list
            public Dictionary<IEntity, string> Referencers = new Dictionary<IEntity, string>();
            public void AddReferencer(IEntity referencer, string code)
            {
                if (Referencers.ContainsKey(referencer))
                    Referencers[referencer] += nl + code;
                else
                    Referencers.Add(referencer, code);
            }
            
            public readonly string ClassName;
            public readonly string FileName;
            //public readonly string ClassNamePlural;
            //public readonly string PkColumnsSqlList;
            //public readonly Func<string,string> CreateIdFromDataRow;
            //public readonly string IdType;

            // Etant donne une colonne "c" appartenant à "Table", renvoie le nom de la propriete généré de type Ienumerable<tuple<...>) et qui renvoie les ids des objets dont this detient des reference
            public readonly Dictionary<ConstraintFK, string> NavPropertiesVia = new Dictionary<ConstraintFK, string>();
            // Idem mais à l'envers : Renvoie le nom de la prorpiété qui renvoie les id des objet referencant this
            public readonly Dictionary<ConstraintFK, string> NavPropertiesFrom = new Dictionary<ConstraintFK, string>(); 

            public EntityData(IEntity e, Func<Property, PropertyData> dataFor, IEnumerable<Table> tables)
            {
                Entity = e;
                if (!(e is EntityStruct))
                    Table = tables.First(t => t.Name == e.Name);
                ClassName = e.Name.Replace("$", "");
                ClassName = ClassName.Capitalize();
                //if (ClassName.EndsWith("ies"))
                //    ClassName = ClassName.Remove(ClassName.Length - 3) + "y";
                //if (ClassName.EndsWith("xes"))
                //    ClassName = ClassName.Remove(ClassName.Length - 3) + "x";
                //if (ClassName.EndsWith("sses"))
                //    ClassName = ClassName.Remove(ClassName.Length - 2);
                //if (ClassName.EndsWith("s") && !ClassName.EndsWith("ss"))
                //    ClassName = ClassName.Remove(ClassName.Length - 1);
                
                //FullClassName = e.Owner.Name.Capitalize() + "." + ClassName;
                FileName = ClassName.Replace("<", "Of").Replace(">", "").Replace(",", "_").Replace(" ", "");
                //IdType = e.Pk == null ? typeof(IdAsRef).Name
                //            : "IdTuple<" + e.Pk.Columns.Select(col => TypeToString(col.CsType)).Join() + ">";

                //if (IdType == "IdTuple<SqlHierarchyId>") // cas particulier car SqlHierarchyId n'est pas Equatable
                //    IdType = "IdTupleSqlHierarchyId";
                //IdType = IdType.Replace("IdTuple<SqlHierarchyId, ", "IdTupleWithSqlHierarchyId<"); // Idem

                ////ClassNamePlural = ClassName.MakePlural();
                //if (e.Pk != null)
                //    PkColumnsSqlList = e.Pk.Columns.Select(col => col.Name).Join();

                //Func<Column, string, string> colToCsValue = (col, rowVar) => dataFor(col).ConvertSqlValueToCsValue(rowVar + "[\"" + col.Name + "\"]");
                //if (e.Pk != null)
                //    CreateIdFromDataRow = rowVar => "new " + IdType + "(" + e.Pk.Columns.Select(col => colToCsValue(col, rowVar)).Join() + ")";


                //if (e.Pk != null)
                //{
                //    BaseTableFk = e.FkConstraints.SingleOrDefault(fk => fk.ForeignColumns.OrderBy(col => col.Name)
                //                                                             .SequenceEqual(
                //                                                             e.Pk.Columns.OrderBy(col => col.Name)));
                //    if (BaseTableFk != null)
                //        BaseTable = BaseTableFk.ForeignColumns[0].Owner;
                //}
            }
        }

        [Flags]
        public enum eRelationKind
        {
            None = 0,
            Composition = 1,  // Un objet a besoin d'un autre objet (composition ou aggregation)
            Aggregation = 2,  // Exemple : un objet reference son Owner.
            Both = Composition | Aggregation
        }

        protected new ColumnData DataFor(Column c)
        {
            return base.DataFor(c);
        }
        // Contenu dupliqué de DataFor(Column p)
        protected PropertyData DataFor(Property p)
        {
            PropertyData data;
            if (_propertyDatas.TryGetValue(p, out data))
                return data;

            data = new PropertyData(p);

            data.PropName = Regex.Replace(p.Name, "[][ '()?,&/.]|-", "_");
            data.PropName = data.PropName.Capitalize(); // Permet aussi de gérer le cas ou PropName est un mot clef du C#, permet egalement d'utiliser ce nom comme des arugment quand on met tout en minuscule, evite ainsi d'avoir d'ambuiguite entre this.PropertyName et PropertyName (l'argument)
            data.AsArgName = data.PropName.ToLower() + "_"; // ajoute "_" pour eviter les mot clef du C#
            data.FieldName = "_" + data.PropName;
            data.PropDeclaration = "";
            var x = char.ToLower(data.PropName[0]);

            if (p.Type is PropertyTypeNative ||
                p.Type is PropertyTypeEntity && ((p.Type as PropertyTypeEntity).Type is EntityEnum
                                                || (p.Type as PropertyTypeEntity).Type is EntityClass
                                                || (p.Type as PropertyTypeEntity).Type is EntityStruct
                                                || (p.Type as PropertyTypeEntity).Type is EntityInterface))
            {
                var typeName = p.Type.AsCodeType;
                if (p.Nullable && (p.Type is PropertyTypeNative && (p.Type as PropertyTypeNative).Type != typeof(string)
                                || p.Type is PropertyTypeEntity && ((p.Type as PropertyTypeEntity).Type is EntityStruct || (p.Type as PropertyTypeEntity).Type is EntityEnum)) &&
                    p.Type != PropertyTypeNative.ByteArray)
                    typeName += "?";

                var maxAccessibility = p.IsGetterPublic.MostAccessible(p.IsSetterPublic);
                data.PropDeclaration += maxAccessibility.GetDescription() + " ";
                data.PropDeclaration += typeName + " " + p.Name + " { ";
                data.PropDeclaration += (maxAccessibility == p.IsGetterPublic ? "" : p.IsGetterPublic.GetDescription() + " ");
                data.PropDeclaration += "get { return " + "_" + p.Name + "; } ";

                if (!p.IsComposite || p.Nullable) // les éléments non nullable et composite sont automatiquement géré
                {
                    data.PropDeclaration += (maxAccessibility == p.IsSetterPublic ? "" : p.IsSetterPublic.GetDescription() + " ");

                    data.PropDeclaration += "set { ";
                    int toSetBegin = data.PropDeclaration.Length;
                    //var isClass = p.Type is PropertyTypeEntity && (p.Type as PropertyTypeEntity).Type is EntityClass;
                    if (p.IsComposite && p.Nullable)
                    {
                        data.PropDeclaration += "if (value == " + data.FieldName + ") return;" + nl + new string(' ', toSetBegin);
                        data.PropDeclaration += "if (value.State != eState.New) throw new Exception(\"Value to set for " + p.Name + " property must be an object with \\\"New\\\" State\");" + nl + new string(' ', toSetBegin);
                    }
                    data.PropDeclaration += "SetProperty(ref " + data.FieldName + ", value);"; // setProperty necessaire pour les dirtyness relays !
                    data.PropDeclaration += " } ";
                }
                data.PropDeclaration += "} " + typeName + " _" + p.Name + ";";
            }

            var pEnum = !(p.Type is PropertyTypeEntity) ? null : (p.Type as PropertyTypeEntity).Type as EntityEnum;
            if (p.Type is PropertyTypeNative || pEnum != null)
            {
                data.Column = DataFor(p.Owner).Table.Columns.Cast<Column>().First(c => c.Name == data.PropName + (pEnum != null ? "_Id" : ""));
                var enumToIntCast = pEnum == null ? "" : "(" + pEnum.BaseType.AsCodeType + (p.Nullable ? "?" : "") + ")";
                var intToEnumCast = pEnum == null ? "" : "(" + pEnum.Name + (p.Nullable ? "?" : "") + ")";
                if (p.Type == PropertyTypeNative.ByteArray)
                    data.ComparisonToDataSource = "!ByteArrayCompare(" + data.FieldName + ", " + "DataSource." + DataFor(data.Column).PropName + ")";
                else
                    data.ComparisonToDataSource = data.FieldName + " != " + intToEnumCast + "DataSource." + DataFor(data.Column).PropName;
                    
                data.DumpIfChanged = data.ComparisonToDataSource + " ? \"" + data.PropName + ": \" + " + "("
                    + (p.Type == PropertyTypeNative.ByteArray ? "DisplayArray(" + data.FieldName + ")" : data.FieldName)
                                   + (data.Column.Nullable ? " ?? (object)\"\"" : "") + ").ToString() : \"\"";
                if (p.Type == PropertyTypeNative.ByteArray)
                {
                    data._DoResetDataSourceEdits = dsVarName => data.FieldName + " = CloneArray(" + dsVarName + "." + DataFor(data.Column).PropName + ");";
                    data._DoSaveDataSourceEdits = dsVarName => dsVarName + "." + DataFor(data.Column).PropName + " = CloneArray(" + data.FieldName + ");";
                }
                else
                {
                    data._DoResetDataSourceEdits = dsVarName => data.FieldName + " = " + intToEnumCast + dsVarName + "." + DataFor(data.Column).PropName + ";";
                    data._DoSaveDataSourceEdits = dsVarName => dsVarName + "." + DataFor(data.Column).PropName + " = " + enumToIntCast + data.FieldName + ";";
                }
                
            }
            else if (p.Type is PropertyTypeEntity && (p.Type as PropertyTypeEntity).Type is EntityStruct)
            {
                var pStruct = (p.Type as PropertyTypeEntity).Type as EntityStruct;
                data.Column = DataFor(p.Owner).Table.Columns.Cast<Column>().First(c => c.Name == data.PropName);
                data.ComparisonToDataSource = p.Nullable
                                            ? "!object.Equals(" + data.FieldName + ", " + "DataSource." + DataFor(data.Column).PropName + " == null ? (" + pStruct.Name + "?)null : " + pStruct.Name + ".Deserialize(DataSource." + DataFor(data.Column).PropName + "))"
                                            : "!object.Equals(" + data.FieldName + ", " + "DataSource." + DataFor(data.Column).PropName + " == null ? new " + pStruct.Name + "() : " + pStruct.Name + ".Deserialize(DataSource." + DataFor(data.Column).PropName + "))";
                data.DumpIfChanged = p.Nullable
                                                    ? data.ComparisonToDataSource + " ? \"" + data.PropName + ": \" + (" + data.FieldName + " == null ? \"\" : " + data.FieldName + ".Value.AsDebugString) : \"\""
                                                    : data.ComparisonToDataSource + " ? \"" + data.PropName + ": \" + " + data.FieldName + ".AsDebugString : \"\"";
                data._DoResetDataSourceEdits = dsVarName =>
                    p.Nullable ? data.PropName + " = " + dsVarName + "." + DataFor(data.Column).PropName + " == null ? (" + pStruct.Name + "?)null : " + pStruct.Name + ".Deserialize(" + dsVarName + "." + DataFor(data.Column).PropName + ");"
                                : data.PropName + " = " + dsVarName + "." + DataFor(data.Column).PropName + " == null ? new " + pStruct.Name + "() : " + pStruct.Name + ".Deserialize(" + dsVarName + "." + DataFor(data.Column).PropName + ");";
                data._DoSaveDataSourceEdits = dsVarName =>
                    p.Nullable ? dsVarName + "." + DataFor(data.Column).PropName + " = " + data.FieldName + " == null ? (byte[])null : " + data.FieldName + ".Value.Serialize();"
                                : dsVarName + "." + DataFor(data.Column).PropName + " = " + data.FieldName + ".Serialize();";
            }
            else if (p.Type is PropertyTypeEntity && ((p.Type as PropertyTypeEntity).Type is EntityClassOrStruct
                                                   || (p.Type as PropertyTypeEntity).Type is EntityInterface))
            {
                var asInterface = (p.Type as PropertyTypeEntity).Type as EntityInterface;
                var asClass = (p.Type as PropertyTypeEntity).Type as EntityClass;
                data.Column = DataFor(p.Owner).Table.Columns.Cast<Column>().First(c => c.Name == data.PropName + "_Id");
                var IdOfFieldName = //isInterface 
                                    //? "((BusinessObjectGenerated)" + data.FieldName + ")" : 
                                    data.FieldName;
                IdOfFieldName += ".Id";
                data.ComparisonToDataSource = "(" + data.FieldName + " == null ? (long?)null : " + IdOfFieldName + ") != DataSource." + DataFor(data.Column).PropName;
                data.DumpIfChanged = data.ComparisonToDataSource + " ? \"" + data.PropName + ": \" + (" + data.FieldName + " == null ? \"\" : " + IdOfFieldName + ".ToString()) : \"\"";
                if (p.IsComposite)
                {
                    if (asClass != null)
                    {
                        data.Init = "AddDirtynessRelayFor(GetMemberName.For<" + p.Owner.Name + ">(x => x." + data.PropName + "), null);";
                        if (p.Nullable)
                            data.CreateDataSource = "            " + data.FieldName + " = null;";
                        else
                        {
                            data.CreateDataSource = "if (" + data.FieldName + " == null || " + data.FieldName + ".State != eState.New)" + nl;
                            data.CreateDataSource += "{" + nl;
                            data.CreateDataSource += "    " + data.FieldName + " = CreateCompositeObject<" + p.Type.AsCodeType + ">();" + nl;
                            data.CreateDataSource += "    DataSource." + DataFor(data.Column).PropName + " = " + IdOfFieldName + ";" + nl;
                            data.CreateDataSource += "}";
                        }
                        data.LoadDataSource = "AssignWithDirtynessRelay(ref " + data.FieldName + ", " + (p.Nullable ? "DataSource." + DataFor(data.Column).PropName + " == null ? null : " : "")
                                                    + "CacheManager.Instance.Get<" + p.Type.AsCodeType + ">(new IdTuple<long>(" + "DataSource." + DataFor(data.Column).PropName + (p.Nullable ? ".Value" : "") + ")));";
                        data.SaveScalarComposites = data.PropName + ".SynchronizeToDatabase();";
                        if (p.Nullable)
                            data.SaveScalarComposites = "if (" + data.PropName + " != null)" + nl + "    " + data.SaveScalarComposites;
                    }
                    data.GetAllModelComposites = lstName => lstName + ".Add(" + data.FieldName + ");";
                }
                data._DoResetDataSourceEdits = dsVarName =>
                    {
                        string res = "";
                        //if (!p.Nullable && p.IsComposite)
                        //    res += "if ("+ dsVarName + "." + DataFor(data.Column).FieldName + " != 0) // Lors du chargement initiale" + nl
                        //        + "    ";
                        res = p.Nullable ? dsVarName + "." + DataFor(data.Column).PropName + " == null ? null : "
                            // si l'element n'est pas composite, et non null. l'Element peut donc etre nul lors de la creation d'un Book (et plus apres)
                            : !p.IsComposite && !p.Nullable ? dsVarName + "." + DataFor(data.Column).PropName + " == 0 ? null : "
                            : "";
                        var dsId = dsVarName + "." + DataFor(data.Column).PropName + (p.Nullable ? ".Value" : "");
                        if (asClass != null)
                            if (!asClass.IsAbstract)
                                res += "CacheManager.Instance.Get<" + p.Type.AsCodeType + ">(new IdTuple<long>(" + dsId + "))";
                            else
                                res += "GetObjectImplementingAmong<" + p.Type.AsCodeType + ">(" + dsId + ", "
                                    + Model.Entities.OfType<EntityClass>()
                                                    .Where(e => !e.IsAbstract && e.IsA(asClass))
                                                    .Select(e => "typeof(" + DataFor(e).ClassName + ")").Join() + ")";
                        else //isInterface
                            res += "GetObjectImplementingAmong<" + p.Type.AsCodeType + ">(" + dsId + ", "
                                + Model.Entities.OfType<EntityClass>()
                                                .Where(e => !e.IsAbstract && e.Implements(p.Type.AsCodeType))
                                                .Select(e => "typeof(" + DataFor(e).ClassName + ")").Join() + ")";
                        if (p.IsComposite)
                        {
                            res = "ResetDataSourceEditsRecursive(" + res + ")";
                            res = "AssignWithDirtynessRelay(ref " + data.FieldName + ", " + res + ") /* because there is no setter , so no \"SetProperty\" that would trigger dirtyness relay to update */";
                        }
                        else
                            res = data.FieldName + " = " + res;
                        return res + ";";
                    };

                if (!p.IsComposite && !p.Nullable)
                    data.Validations.Add(Tuple.Create("" + data.FieldName + " == null", "\"" + data.PropName + " must be specified !\""));

                data._DoSaveDataSourceEdits = dsVarName =>
                {
                    return dsVarName + "." + DataFor(data.Column).PropName + " = " + (p.Nullable ? data.FieldName + " == null ? (long?)null : " : "") + IdOfFieldName + ";";
                };
                if (p.IsComposite)
                {
                    data.DeleteScalarComposites = "if (" + data.PropName + " != null)" + nl;
                    //if ((p.Type as PropertyTypeEntity).Type is EntityInterface)
                    //    data.DeleteScalarComposites += "    ((BusinessObjectGenerated)" + data.PropName + ").UpdateState(eState.ToDelete);";
                    //else
                    data.DeleteScalarComposites += "    " + data.PropName + ".UpdateState(eState.ToDelete);";
                }

                data.GetAllDAlObjectsToSynchronize  = "foreach(var ds in " + data.PropName + ".GetAllDAlObjectsToSynchronize())" + nl;
                data.GetAllDAlObjectsToSynchronize += "    yield return ds;";
                if (p.Nullable)
                    data.GetAllDAlObjectsToSynchronize = "if (" + data.PropName + " != null)" + nl +
                                                         data.GetAllDAlObjectsToSynchronize.Replace(nl, nl + "    ");
                if (((IEntity)asClass ?? asInterface) != null)
                    DataFor(p.Owner).AddDependency((IEntity)asClass ?? asInterface, p.IsComposite ? eRelationKind.Composition : eRelationKind.Aggregation);

                                                
                

                Debug.Assert(p.Owner is EntityClass);
                var tmp = "GetEntitiesWhere<" + DataFor(p.Owner).ClassName + ", " + AssemblyName + "." + DataFor(DataFor(p.Owner).Table).FullClassName + ">(";
                DataFor((IEntity)asClass ?? asInterface).AddReferencer(p.Owner, 
                                                tmp +
                                                "(@this, dal) => dal." + DataFor(data.Column).PropName + " == @this.Id" +
                                                "," + nl + new string(' ', tmp.Length) +
                                                "@this => " + AssemblyName + "." + DataFor(DataFor(p.Owner).Table).FullClassName + ".GetEntitiesWhere(\"" + data.Column.Name + " = \" + @this.Id))");
            }
            else if (p.Type is PropertyTypeCollection)
            {
                var ptc = p.Type as PropertyTypeCollection;
                if (ptc.Type is PropertyTypeNative ||  // List<int>
                    ptc.Type is PropertyTypeEntity && (ptc.Type as PropertyTypeEntity).Type is EntityStruct) // List<Struct>
                {
                    data.Column = null;
                    data.Table = DataFor(p.Owner).Table.OwnerSchema().Tables().Cast<Table>().First(t => t.Name == DataFor(p.Owner).Table.Name + "_" + data.PropName);

                    data.PropDeclaration += "public readonly CheckableCompositeListGenerated<" + ptc.Type.AsCodeType + "> " + data.PropName + " = new CheckableCompositeListGenerated<" + ptc.Type.AsCodeType + ">() { RememberDeletedObjects = false, ConsiderItemAsComposite = false, ConsiderDirtyOnItemChanged = false, ConsiderDirtyOnAddRemoveEvents = true };" + nl;
                    data.PropDeclaration += "public readonly CheckableCompositeListGenerated<" + AssemblyName + "." + DataFor(data.Table).FullClassName + "> " + data.FieldName + " = new CheckableCompositeListGenerated<" + AssemblyName + "." + DataFor(data.Table).FullClassName + ">() { RememberDeletedObjects = true, ConsiderItemAsComposite = true, ConsiderDirtyOnItemChanged = true, ConsiderDirtyOnAddRemoveEvents = true };" + nl;
                    data.PropDeclaration += "                bool " + data.FieldName + "_reloading;";

                    data.ComparisonToDataSource = data.FieldName + ".State != eState.Synchronized";
                    data.DumpIfChanged = "IBusinessStructGenerated_Extensions.DisplayDiff(" + data.FieldName + ", all)";

                    data.Init = data.FieldName + ".LoadItems = LoadComposites_" + data.PropName + ";" + nl;
                    if (ptc.Type is PropertyTypeNative)
                    {
                        var cast = (ptc.Type as PropertyTypeNative).Type != data.Table.Columns[2].CsType()
                                    ? "(" + (ptc.Type as PropertyTypeNative).Type.ToSmartString() + ")"
                                    : "";
                        data.Init += data.PropName + ".LoadItems = () => " + data.FieldName + ".Select(lnk => " + cast + "lnk." + DataFor(data.Table.Columns[2]).PropName + ");" + nl;
                    }
                    else
                    {
                        var pStruct = (ptc.Type as PropertyTypeEntity).Type as EntityStruct;
                        data.Init += data.PropName + ".LoadItems = () => " + data.FieldName + ".Select(lnk => " + DataFor(pStruct).ClassName + ".Deserialize(lnk." + DataFor(data.Table.Columns[2]).PropName + "));" + nl;
                    }

                    data.Init += data.PropName + ".UpdateState(eState.Synchronized);" + nl;
                    data.Init += data.FieldName + ".UpdateState(eState.Synchronized);" + nl;
                    if (ptc.Type is PropertyTypeNative)
                    {
                        var cast = (ptc.Type as PropertyTypeNative).Type != data.Table.Columns[2].CsType()
                                    ? "(" + data.Table.Columns[2].CsType().ToSmartString() + ")"
                                    : "";
                        data.Init += data.PropName + ".ItemAdded += (_, e) => { if (" + data.FieldName + "_reloading) return; " + data.FieldName + ".Insert(e.Index, new " + AssemblyName + "." + DataFor(data.Table).FullClassName + "() { " + DataFor(data.Table.Columns[1]).PropName + " = Id, " + DataFor(data.Table.Columns[2]).PropName + " = " + cast + "e.Item }); };" + nl;
                    }
                    else
                        data.Init += data.PropName + ".ItemAdded += (_, e) => { if (" + data.FieldName + "_reloading) return; " + data.FieldName + ".Insert(e.Index, new " + AssemblyName + "." + DataFor(data.Table).FullClassName + "() { " + DataFor(data.Table.Columns[1]).PropName + " = Id, " + DataFor(data.Table.Columns[2]).PropName + " = e.Item.Serialize() }); };" + nl;
                    data.Init += data.PropName + ".ItemRemoved += (_, e) => " + data.FieldName + ".RemoveAt(e.Index);" + nl;
                    data.Init += data.PropName + ".Cleared += (_, e) => { if (" + data.FieldName + "_reloading) return; " + data.FieldName + ".Clear(); };" + nl;
                    data.Init += "AddDirtynessRelayFor(GetMemberName.For<" + p.Owner.Name + ">(i => i." + data.FieldName + "), " + data.FieldName + ");";


                    data._DoResetDataSourceEdits = dsVarName =>
                    {
                        string res = "";
                        res += data.FieldName + ".Reload();" + nl;
                        res += "foreach (var " + x + " in " + data.FieldName + "/*LoadComposites_IntsNullable(true)*/) // TODO : on pourra eviter des charger les element pas dans le cache quand on aura des collections lazy" + nl;
                        res += "    " + x + ".CancelChanges(); // Peut provoquer des event state change sur _IntsNullable ? => Faire un test !" + nl;
                        res += data.FieldName + "_reloading = true;" + nl;
                        res += "try { " + data.PropName + ".Reload(); }" + nl;
                        res += "finally { " + data.FieldName + "_reloading = false; }" + nl;
                        res += "Debug.Assert(" + data.FieldName + ".State == eState.Synchronized);" + nl;
                        res += "Debug.Assert(" + data.PropName + ".State == eState.Synchronized);";
                        return res;
                    };
                    data._DoSaveDataSourceEdits = dsVarName => "";
                    data.LoadDataSourceFunctions = "private IEnumerable<" + AssemblyName + "." + DataFor(data.Table).FullClassName + "> LoadComposites_" + p.Name + "() { return LoadComposites_" + p.Name + "(false); }" + nl;
                    data.LoadDataSourceFunctions += "private IEnumerable<" + AssemblyName + "." + DataFor(data.Table).FullClassName + "> LoadComposites_" + p.Name + "(bool preventLoading)" + nl;
                    data.LoadDataSourceFunctions += "{" + nl;
                    data.LoadDataSourceFunctions += "    if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))" + nl;
                    data.LoadDataSourceFunctions += "        return new " + AssemblyName + "." + DataFor(data.Table).FullClassName + "[0];" + nl;
                    var tmp = "    return GetEntitiesWhere<" + DataFor(p.Owner).ClassName + ", " + AssemblyName + "." + DataFor(data.Table).FullClassName + ">(";
                    data.LoadDataSourceFunctions += tmp +
                                                    "(@this, lnk) => lnk." + DataFor(data.Table.Columns[1]).PropName + " == @this.Id" +
                                                    "," + nl + new string(' ', tmp.Length) + 
                                                    "@this => " + AssemblyName + "." + DataFor(data.Table).FullClassName + ".GetEntitiesWhere(\"" + data.Table.Columns[1].Name + " = \" + @this.Id));" + nl;
                    data.LoadDataSourceFunctions += "}";

                    data.SaveLinkComposites = "foreach (var link in " + data.FieldName + ".DeletedObjects)" + nl;
                    data.SaveLinkComposites += "    link.SynchronizeToDatabase();" + nl;
                    data.SaveLinkComposites += "foreach (var link in " + data.FieldName + ")" + nl;
                    data.SaveLinkComposites += "    link.SynchronizeToDatabase();" + nl;
                    data.SaveLinkComposites += data.FieldName + ".AcceptChanges();" + nl;
                    data.SaveLinkComposites += data.PropName + ".AcceptChanges();";

                    data.DeleteLinkComposites = "foreach (var link in " + data.FieldName + ".DeletedObjects)" + nl;
                    data.DeleteLinkComposites += "    link.SynchronizeToDatabase();" + nl;
                    data.DeleteLinkComposites += "foreach (var link in " + data.FieldName + ")" + nl;
                    data.DeleteLinkComposites += "    link.Delete();" + nl;
                    data.DeleteLinkComposites += data.FieldName + ".UpdateState(eState.Deleted);" + nl;
                    data.DeleteLinkComposites += data.PropName + ".UpdateState(eState.Deleted);";
                }
                else if (ptc.Type is PropertyTypeEntity && (ptc.Type as PropertyTypeEntity).Type is EntityClass)  // List<Class>, List<Interface>
                {
                    data.Column = null;
                    data.Table = DataFor(p.Owner).Table.OwnerSchema().Tables().First(t => t.Name == DataFor(p.Owner).Table.Name + "_" + data.PropName);
                    data.PropDeclaration += "public readonly CheckableCompositeListGenerated<" + ptc.Type.AsCodeType + "> " + data.PropName + " = new CheckableCompositeListGenerated<" + ptc.Type.AsCodeType + ">() { RememberDeletedObjects = true, ConsiderItemAsComposite = " + (p.IsComposite ? "true" : "false") + ", ConsiderDirtyOnItemChanged = " + (p.IsComposite ? "true" : "false") + ", ConsiderDirtyOnAddRemoveEvents = true };";
                    data.ComparisonToDataSource = data.PropName + ".State != eState.Synchronized";
                    if (p.IsComposite)
                        data.DumpIfChanged = data.PropName + ".DeletedObjects.Concat(" + data.PropName + ").Select(" + x + " => " + x + ".Dump(all).Replace(nl, nl + \"     \").PrefixIfNotEmptyWith(\"   - \")).NotBlank().Join(nl).Replace(nl, ChangeIndent).PrefixIfNotEmptyWith(\"" + data.PropName + ":\" + ChangeIndent)";
                    else
                        data.DumpIfChanged = data.PropName + ".DeletedObjects.Concat(" + data.PropName + ").Select(" + x + " => (" + x + ".State.GetDescription() + " + x + ".TypedId.ToSmartString()).Replace(nl, nl + \"     \").PrefixIfNotEmptyWith(\"   - \")).NotBlank().Join(nl).Replace(nl, ChangeIndent).PrefixIfNotEmptyWith(\"" + data.PropName + ":\" + ChangeIndent)";

                    data.Init = data.PropName + ".LoadItems = LoadComposites_" + data.PropName + ";" + nl;
                    data.Init += data.PropName + ".UpdateState(eState.Synchronized);" + nl;
                    data.Init += "AddDirtynessRelayFor(GetMemberName.For<" + p.Owner.Name + ">(" + x + " => " + x + "." + data.PropName + "), " + data.PropName + ");";

                    data._DoResetDataSourceEdits = dsVarName =>
                    {
                        string res = "";
                        res += data.PropName + ".Reload();";
                        if (p.IsComposite)
                        {
                            res += nl + "foreach (var " + x + " in LoadComposites_" + data.PropName + "(/*true*/)) // TODO : on pourra eviter des charger les element pas dans le cache quand on aura des collections lazy";
                            res += nl + "    ResetDataSourceEditsRecursive(" + x + ");";
                        }
                        return res;
                    };
                    data._DoSaveDataSourceEdits = dsVarName =>
                    {
                        string res = "";
                        if (p.IsComposite)
                        {
                            res += "foreach (var " + x + " in " + data.PropName + ")" + nl;
                            res += "    SaveDataSourceEditsRecursive(" + x + ");";
                        }
                        return res;
                    };
                    var referencedType = (ptc.Type as PropertyTypeEntity).Type;
                    data.LoadDataSourceFunctions = "private IEnumerable<" + referencedType.Name + "> LoadComposites_" + p.Name + "() { return LoadComposites_" + p.Name + "(false); }" + nl;
                    data.LoadDataSourceFunctions += "private IEnumerable<" + referencedType.Name + "> LoadComposites_" + p.Name + "(bool preventLoading)" + nl;
                    data.LoadDataSourceFunctions += "{" + nl;
                    data.LoadDataSourceFunctions += "    if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))" + nl;
                    data.LoadDataSourceFunctions += "        return new " + ptc.Type.AsCodeType + "[0];" + nl;
                    data.LoadDataSourceFunctions += "    // TODO : A optimiser : on pourrait faire une seule requete au lieu de deux" + nl;
                    var tmp = "    var links = GetEntitiesWhere<" + DataFor(p.Owner).ClassName + ", " + AssemblyName + "." + DataFor(data.Table).FullClassName + ">(";
                    data.LoadDataSourceFunctions += tmp +
                                                    "(@this, lnk) => lnk." + DataFor(data.Table.Columns[1]).PropName + " == @this.Id" +
                                                    "," + nl + new string(' ', tmp.Length) +
                                                    "@this => " + AssemblyName + "." + DataFor(data.Table).FullClassName + ".GetEntitiesWhere(\"" + data.Table.Columns[1].Name + " = \" + @this.Id));" + nl;
                    data.LoadDataSourceFunctions += "    var ids = links.Select(lnk => new IdTuple<long>(lnk." + DataFor(data.Table.Columns[2]).PropName + "));" + nl;
                    var referencedTable = DataFor(referencedType).Table;
                    data.LoadDataSourceFunctions += "    var dalObjects = " + AssemblyName + "." + DataFor(referencedTable).FullClassName + ".GetEntitiesWithIds(ids);" + nl;
                    data.LoadDataSourceFunctions += "    return CacheManager.Instance.GetAll<" + referencedType.Name + ">(ids.Cast<IIdTuple>(), preventLoading);" + nl;
                    data.LoadDataSourceFunctions += "}";

                    data.SaveScalarComposites  = "if (" + data.PropName + ".DeletedObjects.Count > 0)" + nl;
                    data.SaveScalarComposites += "{" + nl;
                    tmp = "    var links = GetEntitiesWhere<" + DataFor(p.Owner).ClassName + ", " + AssemblyName + "." + DataFor(data.Table).FullClassName + ">(";
                    data.SaveScalarComposites += tmp +
                                                "(@this, lnk) => lnk." + DataFor(data.Table.Columns[1]).PropName + " == @this.Id" +
                                                " && @this." + data.PropName + ".DeletedObjects.Any(" + x + " => " + x + ".Id == lnk." + DataFor(data.Table.Columns[2]).PropName + ")" +
                                                "," + nl + new string(' ', tmp.Length) +
                                                "@this => " + AssemblyName + "." + DataFor(data.Table).FullClassName +
                                                              ".GetEntitiesWhere(\"" + data.Table.Columns[1].Name + " = \" + @this.Id + \" AND " +
                                                              data.Table.Columns[2].Name + " in (\" + @this." + data.PropName + ".DeletedObjects.Select(" + x + " => " + x + ".Id).Join() + \")\"));" + nl;

                    data.SaveScalarComposites += "    foreach (var link in links)" + nl;
                    data.SaveScalarComposites += "        link.Delete();" + nl;
                    data.SaveScalarComposites += "}";
                    if (p.IsComposite)
                    {
                        data.SaveScalarComposites += nl;
                        data.SaveScalarComposites += "Debug.Assert(" + data.PropName + ".DeletedObjects.All(" + x + " => " + x + ".State == eState.ToDelete));" + nl;
                        data.SaveScalarComposites += "foreach (var " + x + " in " + data.PropName + ".DeletedObjects)" + nl;
                        data.SaveScalarComposites += "    " + x + ".SynchronizeToDatabase();" + nl;
                        data.SaveScalarComposites += "foreach (var " + x + " in " + data.PropName + ")" + nl;
                        data.SaveScalarComposites += "    " + x + ".SynchronizeToDatabase();";
                    }

                    data.SaveLinkComposites  = "{" + nl;
                    tmp = "    var links = GetEntitiesWhere<" + DataFor(p.Owner).ClassName + ", " + AssemblyName + "." + DataFor(data.Table).FullClassName + ">(";
                    data.SaveLinkComposites += tmp +
                                                "(@this, lnk) => lnk." + DataFor(data.Table.Columns[1]).PropName + " == @this.Id" +
                                                "," + nl + new string(' ', tmp.Length) +
                                                "@this => " + AssemblyName + "." + DataFor(data.Table).FullClassName +
                                                              ".GetEntitiesWhere(\"" + data.Table.Columns[1].Name + " = \" + @this.Id));" + nl;
                    data.SaveLinkComposites += "    foreach (var " + x + "Id in " + data.PropName + ".Select(" + x + " => " + x + ".Id).Except(links.Select(lnk => lnk." + DataFor(data.Table.Columns[2]).PropName + ")))" + nl;
                    data.SaveLinkComposites += "    {" + nl;
                    data.SaveLinkComposites += "        var link = new " + AssemblyName + "." + DataFor(data.Table).FullClassName + "() { " + DataFor(data.Table.Columns[1]).PropName + " = Id, " + DataFor(data.Table.Columns[2]).PropName + " = " + x + "Id };" + nl;
                    data.SaveLinkComposites += "        link.SynchronizeToDatabase();" + nl; ;
                    data.SaveLinkComposites += "    }" + nl;
                    if (p.IsComposite)
                        data.SaveLinkComposites += "    " + data.PropName + ".AcceptChanges();";
                    else
                    { // Comme AcceptChanges mais sans l'appel a UpdateStateItems()
                        data.SaveLinkComposites += "    " + data.PropName + ".DeletedObjects.Clear();" + nl;
                        data.SaveLinkComposites += "    " + data.PropName + ".UpdateState();" + nl;
                    }
                    data.SaveLinkComposites += "}";

                    tmp = "var links" + p.Name + " = GetEntitiesWhere <" + DataFor(p.Owner).ClassName + ", " + AssemblyName + "." + DataFor(data.Table).FullClassName + ">(";
                    data.DeleteLinkComposites += tmp + 
                                                "(@this, lnk) => lnk." + DataFor(data.Table.Columns[1]).PropName + " == @this.Id" +
                                                "," + nl + new string(' ', tmp.Length) +
                                                "@this => " + AssemblyName + "." + DataFor(data.Table).FullClassName + ".GetEntitiesWhere(\"" + data.Table.Columns[1].Name + " = \" + @this.Id));" + nl;


                    data.DeleteLinkComposites += "foreach (var link in links" + p.Name + ")" + nl;
                    data.DeleteLinkComposites += "    link.Delete();";

                    data.DeleteScalarComposites = data.PropName + ".Clear(); // ==> passe tous les objets dans " + data.PropName + ".DeletedObjects";
                    if (p.IsComposite)
                    {
                        data.DeleteScalarComposites += nl;
                        data.DeleteScalarComposites += "foreach (var " + x + " in " + data.PropName + ".DeletedObjects)" + nl;
                        data.DeleteScalarComposites += "    " + x + ".Delete();";
                    }

                    data.GetAllDAlObjectsToSynchronize = "foreach (var obj in " + data.PropName + ".DeletedObjects.Concat(" + data.PropName + "))" + nl;
                    data.GetAllDAlObjectsToSynchronize += "    foreach (var ds in " + data.PropName + ".GetAllDAlObjectsToSynchronize())" + nl;
                    data.GetAllDAlObjectsToSynchronize += "        yield return ds;";

                    DataFor(p.Owner).AddDependency((ptc.Type as PropertyTypeEntity).Type, p.IsComposite ? eRelationKind.Composition : eRelationKind.Aggregation);
                    if (p.IsComposite)
                        data.GetAllModelComposites = lstName => lstName + ".AddRange(" + data.PropName + ");";
                }
                else
                    Break(p.Type.AsCodeType); // TODO
            }
            else if (p.Type is PropertyTypeDictionary)
            {
                var ptd = p.Type as PropertyTypeDictionary;
                if (ptd.KeyType is PropertyTypeNative ||  // Dictionary<int, ???>
                    (ptd.KeyType is PropertyTypeEntity) && (ptd.KeyType as PropertyTypeEntity).Type is EntityStruct)  // Dictionary<struct, ???>
                {
                    //if (ptd.ValueType is PropertyTypeNative ||  // Dictionary<int, int>, Dictionary<struct, int>
                    //    (ptd.ValueType is PropertyTypeEntity) && (ptd.ValueType as PropertyTypeEntity).Type is EntityStruct)  // Dictionary<int, struct>, Dictionary<struct, struct>
                    //{
                    //    Break(p.Type.AsCodeType); // TODO
                    //}
                    //else 
                    if (ptd.ValueType is PropertyTypeNative ||
                        (ptd.ValueType is PropertyTypeEntity) && (ptd.ValueType as PropertyTypeEntity).Type is EntityEnum)  // Dictionary<int, enum>, Dictionary<struct, enum>
                    {
                        var ptdv = ptd.ValueType is PropertyTypeEntity ? (ptd.ValueType as PropertyTypeEntity).Type as EntityEnum : null;
                        data.Column = null;
                        data.Table = DataFor(p.Owner).Table.OwnerSchema().Tables().First(t => t.Name == DataFor(p.Owner).Table.Name + "_" + data.PropName);
                        data.PropDeclaration += "public readonly CompositeDictionary<" + ptd.KeyType.AsCodeType + ", " + ptd.ValueType.AsCodeType + "> " + data.PropName + " = new CompositeDictionary<" + ptd.KeyType.AsCodeType + ", " + ptd.ValueType.AsCodeType + ">() { RememberDeletedObjects = false, ConsiderDirtyOnAddRemoveEvents = true, ConsiderDirtyOnItemChanged = true };" + nl;
                        data.PropDeclaration += "       readonly CompositeDictionary<" + ptd.KeyType.AsCodeType + ", " + AssemblyName +"." + DataFor(data.Table).FullClassName + "> " + data.FieldName + " = new CompositeDictionary<" + ptd.KeyType.AsCodeType + ", " + AssemblyName + "." + DataFor(data.Table).FullClassName + ">() { RememberDeletedObjects = true, ConsiderDirtyOnAddRemoveEvents = true, ConsiderDirtyOnItemChanged = true };" + nl;
                        data.PropDeclaration += "                bool " + data.FieldName + "_reloading;";

                        data.ComparisonToDataSource = data.FieldName + ".State != eState.Synchronized";
                        data.DumpIfChanged = "IBusinessStructGenerated_Extensions.DisplayDiff(\"" + data.PropName + "\", " + data.FieldName + ", all)";

                        data.Init  = data.FieldName + ".LoadItems = () => LoadComposites_" + data.PropName + "().ToDictionary(item => item.Key, item => item);" + nl;
                        data.Init += data.PropName  + ".LoadItems = () => " + data.FieldName + ".ToDictionary(lnk => lnk.Key, lnk => (" + ptd.ValueType.AsCodeType + ")lnk.Value." + DataFor(data.Table.Columns.Cast<Column>().Last()).PropName + ");" + nl;
                        data.Init += data.PropName  + ".UpdateState(eState.Synchronized);" + nl;
                        data.Init += data.FieldName + ".UpdateState(eState.Synchronized);" + nl;
                        data.Init += data.PropName + ".CollectionChanged += BuildDictionaryCollectionChangedHandler(" +
                                                     "() => " + data.FieldName + "_reloading, " + data.PropName + ", " + data.FieldName + "," + nl;
                        data.Init += "                              " + "kvp => new " + AssemblyName + "." + DataFor(data.Table).FullClassName + "() { Owner_Id = Id, Key = kvp.Key, " + DataFor(data.Table.Columns.Cast<Column>().Last()).PropName + " = " + (ptdv == null ? "" : "(" + ptdv.BaseType.AsCodeType + ")") + "kvp.Value });" + nl;
                        data.Init += "AddDirtynessRelayFor(GetMemberName.For<" + p.Owner.Name + ">(" + x + " => " + x + "." + data.FieldName + "), " + data.FieldName + ");";

                        data._DoResetDataSourceEdits = dsVarName =>
                        {
                                string res = "";
                                res += data.FieldName + ".Reload();" + nl;
                                //res += "foreach (var " + x + " in " + data.FieldName + "/*LoadComposites_IntsNullable(true)*/) // TODO : on pourra eviter des charger les element pas dans le cache quand on aura des collections lazy" + nl;
                                //res += "    " + x + ".CancelChanges(); // Peut provoquer des event state change sur _IntsNullable ? => Faire un test !" + nl;
                                res += data.FieldName + "_reloading = true;" + nl;
                                res += "try { " + data.PropName + ".Reload(); }" + nl;
                                res += "finally { " + data.FieldName + "_reloading = false; }" + nl;
                                res += "Debug.Assert(" + data.FieldName + ".State == eState.Synchronized);" + nl;
                                res += "Debug.Assert(" + data.PropName + ".State == eState.Synchronized);";
                                return res;
                        };
                        data._DoSaveDataSourceEdits = null;// dsVarName =>
                        //{
                        //    string res = "";
                        //    if (p.IsComposite)
                        //    {
                        //        res += "foreach (var " + x + " in " + data.PropName + ")" + nl;
                        //        res += "    SaveDataSourceEditsRecursive(" + x + ");";
                        //    }
                        //    return res;
                        //};
                        data.LoadDataSourceFunctions  = "private IEnumerable<" + AssemblyName + "." + DataFor(data.Table).FullClassName + "> LoadComposites_" + p.Name + "() { return LoadComposites_" + p.Name + "(false); }" + nl;
                        data.LoadDataSourceFunctions += "private IEnumerable<" + AssemblyName + "." + DataFor(data.Table).FullClassName + "> LoadComposites_" + p.Name + "(bool preventLoading)" + nl;
                        data.LoadDataSourceFunctions += "{" + nl;
                        data.LoadDataSourceFunctions += "    if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))" + nl;
                        data.LoadDataSourceFunctions += "        return new " + AssemblyName + "." + DataFor(data.Table).FullClassName + "[0];" + nl;
                        //data.LoadDataSourceFunctions += "    return " + AssemblyName + "." + DataFor(data.Table).FullClassName + ".GetEntitiesWhere(\"" + data.Table.Columns[1].Name + " = \" + this.Id);" + nl;
                        var tmp = "    return GetEntitiesWhere<" + DataFor(p.Owner).ClassName + ", " + AssemblyName + "." + DataFor(data.Table).FullClassName + ">(";
                        data.LoadDataSourceFunctions += tmp +
                                                        "(@this, lnk) => lnk." + DataFor(data.Table.Columns[1]).PropName + " == @this.Id" +
                                                        "," + nl + new string(' ', tmp.Length) +
                                                        "@this => " + AssemblyName + "." + DataFor(data.Table).FullClassName + ".GetEntitiesWhere(\"" + data.Table.Columns[1].Name + " = \" + @this.Id));" + nl;
                        data.LoadDataSourceFunctions += "}";

                        data.SaveScalarComposites = null;

                        data.SaveLinkComposites  = "foreach (var dalKvp in " + data.FieldName + ".DeletedObjects.Values)" + nl;
                        data.SaveLinkComposites += "    dalKvp.SynchronizeToDatabase();" + nl;
                        data.SaveLinkComposites += "foreach (var dalKvp in " + data.FieldName + ".Values)" + nl;
                        data.SaveLinkComposites += "    dalKvp.SynchronizeToDatabase();" + nl;
                        data.SaveLinkComposites += data.FieldName + ".AcceptChanges();" + nl;
                        data.SaveLinkComposites += data.PropName + ".AcceptChanges();";

                        data.DeleteLinkComposites  = "foreach (var dalKvp in " + data.FieldName + ".DeletedObjects.Values)" + nl;
                        data.DeleteLinkComposites += "    dalKvp.SynchronizeToDatabase();" + nl;
                        data.DeleteLinkComposites += "foreach (var dalKvp in " + data.FieldName + ".Values)" + nl;
                        data.DeleteLinkComposites += "    dalKvp.Delete();" + nl;
                        data.DeleteLinkComposites += data.FieldName + ".UpdateState(eState.Deleted);" + nl;
                        data.DeleteLinkComposites += data.PropName + ".UpdateState(eState.Deleted);";

                        data.DeleteScalarComposites = null;

                        data.GetAllDAlObjectsToSynchronize = "foreach (var obj in " + data.PropName + ".DeletedObjects.Values.Concat(" + data.PropName + ".Values))" + nl;
                        data.GetAllDAlObjectsToSynchronize += "    foreach (var ds in " + data.PropName + ".GetAllDAlObjectsToSynchronize())" + nl;
                        data.GetAllDAlObjectsToSynchronize += "        yield return ds;";

                        if (ptdv != null)
                            DataFor(p.Owner).AddDependency(ptdv, p.IsComposite ? eRelationKind.Composition : eRelationKind.Aggregation);
                    }
                }
            }
            else
            {
                Break(p.Type.AsCodeType); // TODO
            }
            _propertyDatas.Add(p, data);
            return data;
        }
        protected class PropertyData
        {
            Property Property;
            public Column Column;
            public Table  Table; // Si la colonne a donné lieu à la génération d'une table
            public string PropName;
            public string AsArgName;
            public string FieldName;
            public string PropDeclaration;
            public string ComparisonToDataSource;
            public string DumpIfChanged;
            public string Init;
            public string CreateDataSource;
            public string LoadDataSource;
            public string LoadDataSourceFunctions;
            public string SaveScalarComposites;
            public string SaveLinkComposites;
            public string DeleteLinkComposites;
            public string DeleteScalarComposites;
            public Func<string, string> GetAllModelComposites; // dsVarNameInWhichToAddObject => CompleteInstruction
            public string GetAllDAlObjectsToSynchronize;
            public Func<string, string> _DoResetDataSourceEdits;
            public Func<string, string> _DoSaveDataSourceEdits;
            public readonly List<Tuple<string, string>> Validations = new List<Tuple<string, string>>(); // codeCondition, StringError (Exemple : "a.g == null", "blabla " + uneVariable + " n'est pas bon !")


            public PropertyData(Property p)
            {
                Property = p;
            }
        
            public string DoResetDataSourceEdits(string dsVarName)
            {
                if (_DoResetDataSourceEdits == null)
                {
                    //Break("DoResetDataSourceEdits for " + Property.AsDebugString); // TODO
                    return "";
                }
                else
                    return _DoResetDataSourceEdits("ds");
            }
            public string DoSaveDataSourceEdits(string dsVarName)
            {
                if (_DoSaveDataSourceEdits == null)
                {
                    //Break("DoSaveDataSourceEdits for " + Property.AsDebugString); // TODO
                    return "";
                }
                else
                    return _DoSaveDataSourceEdits("ds");
            }
        }
    }
}
