﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

using Microsoft.CSharp;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Tools;

using DataMapper;

using DatabaseExplorer.Business.BaseDALClass;
using DatabaseExplorer.Business.Extensions;


namespace DatabaseExplorer.Business
{
    public abstract class DynamicTypeAssemblyGeneratorBase
    {
        public string SourceCode { get; private set; }
        public string AssemblyName { get; private set; }

        public DynamicTypeAssemblyGeneratorBase(string assemblyName, string sourceCode)
        {
            AssemblyName = assemblyName;
            SourceCode = sourceCode;
        }

        /// <summary> Generate a constructor of type ((string connectionString) => object dao)) </summary>
        public Func<string, object> GenerateAssemblyAndRetry()
        {
            while (true)
            {
                try
                {
                    return GenerateAssembly();
                }
                catch (Exception ex)
                {
                    if (OnCompilationError == null)
                        throw;
                    var e = new CompilationErrorEventArgs() { SourceCode = SourceCode, Exception = ex };
                    OnCompilationError(this, e);
                    if (!e.Retry)
                        throw;
                    SourceCode = e.SourceCode;
                }
            }
        }
        public EventHandler<CompilationErrorEventArgs> OnCompilationError;
        public class CompilationErrorEventArgs : EventArgs
        {
            public Exception Exception  { get; set; }
            public string    SourceCode { get; set; }
            public bool      Retry      { get; set; }
        }
        Func<string, object> GenerateAssembly()
        {
            var parameters = new CompilerParameters
            {
                GenerateInMemory = true,  // True - memory generation, false - external file generation
                GenerateExecutable = false // True - exe file generation, false - dll file generation
            };
            // Ces trois lignes de code permettent de voir la ligne qui a buggé en live dans Visual Studio!
            parameters.TempFiles = new TempFileCollection(Environment.GetEnvironmentVariable("TEMP"), true);
            parameters.IncludeDebugInformation = true;
            parameters.GenerateInMemory = false;


            parameters.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().Location);

            parameters.ReferencedAssemblies.Add(typeof(System.Int32).Assembly.Location); // System
            parameters.ReferencedAssemblies.Add(typeof(System.IO.Path).Assembly.Location); // System.IO
            parameters.ReferencedAssemblies.Add(typeof(System.Linq.Enumerable).Assembly.Location);  // System.Linq
            parameters.ReferencedAssemblies.Add(typeof(System.Reflection.PropertyInfo).Assembly.Location); // System.Reflection
            parameters.ReferencedAssemblies.Add(typeof(System.Text.ASCIIEncoding).Assembly.Location); // System.Text
            //parameters.ReferencedAssemblies.Add(typeof(System.Windows.Forms.Form).Assembly.Location); // System.Windows.Forms
            parameters.ReferencedAssemblies.Add(typeof(System.Runtime.CompilerServices.MethodImplAttribute).Assembly.Location); // System.Runtime.CompilerServices;
            parameters.ReferencedAssemblies.Add(typeof(System.ComponentModel.INotifyPropertyChanged).Assembly.Location);
            parameters.ReferencedAssemblies.Add(typeof(System.Data.DataTableExtensions).Assembly.Location);
            parameters.ReferencedAssemblies.Add(typeof(TechnicalTools.Model.PropertyNotifierObject).Assembly.Location); // TechnicalTools
            parameters.ReferencedAssemblies.Add(typeof(GenericGeneratedDAO).Assembly.Location); // GenericDALBasics
            parameters.ReferencedAssemblies.Add(typeof(IDbMapper).Assembly.Location); // DataMapper
            parameters.ReferencedAssemblies.Add(typeof(Microsoft.SqlServer.Types.SqlHierarchyId).Assembly.Location); // type pour certaine base de donnés
            parameters.ReferencedAssemblies.Add(typeof(System.Data.SqlTypes.SqlXml).Assembly.Location); // type pour certaine base de donnés
            parameters.ReferencedAssemblies.Add(typeof(System.Xml.Serialization.IXmlSerializable).Assembly.Location);
            parameters.ReferencedAssemblies.Add(typeof(System.ComponentModel.DataAnnotations.MetadataTypeAttribute).Assembly.Location);

            string nl = Environment.NewLine;
            string daoDefinition = GetDaoDefinition();
            CompilerResults results;
            using (var provider = new CSharpCodeProvider())
                results = provider.CompileAssemblyFromSource(parameters, SourceCode + nl + daoDefinition);

            if (results.Errors.HasErrors)
                throw new TechnicalException("Error compiling dynamic types !" + Environment.NewLine +
                                             results.Errors.Cast<CompilerError>().Where(error => !error.IsWarning).Select(error => string.Format("Error {0} (Line {1}, Column {2}) : {3}", error.ErrorNumber, error.Line, error.Column, error.ErrorText)).Join(Environment.NewLine) +
                                             Environment.NewLine +
                                             results.Errors.Cast<CompilerError>().Where(error => error.IsWarning).Select(error => string.Format("Warning {0} (Line {1}, Column {2}) : {3}", error.ErrorNumber, error.Line, error.Column, error.ErrorText)).Join(Environment.NewLine),
                                             null);
            Assembly assembly = results.CompiledAssembly;
            return GetDaoConstructor(assembly);
        }

        protected abstract string GetDaoDefinition();

        protected abstract Func<string, object> GetDaoConstructor(Assembly assembly);

        private List<Type> GenerateAssemblyWithTypesWithIL(string assName, List<Table> tables)
        { // from https://msdn.microsoft.com/fr-fr/library/system.reflection.emit.typebuilder.definegenericparameters(v=vs.110).aspx 
            // form 

            // Define a dynamic assembly to contain the sample type. The
            // assembly will not be run, but only saved to disk, so
            // AssemblyBuilderAccess.Save is specified.
            AppDomain myDomain = AppDomain.CurrentDomain;
            AssemblyName assemblyName = new AssemblyName(assName);
            AssemblyBuilder assemblyBuilder = myDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave);

            // An assembly is made up of executable modules. For a single-
            // module assembly, the module name and file name are the same 
            // as the assembly name. 
            ModuleBuilder module = assemblyBuilder.DefineDynamicModule(assemblyName.Name, assemblyName.Name + ".dll");

            var generatedTypes = new List<Type>();
            foreach (var table in tables)
            {
                // create a new type builder
                TypeBuilder typeBuilder = module.DefineType(table.Name, TypeAttributes.Public | TypeAttributes.Class, typeof(PropertyNotifierObject), new[] { typeof(IAutoMappedDbObject) });
                //var baseNonGenericCtor = typeof(PropertyNotifierObject).GetConstructor(BindingFlags.Public | BindingFlags.Instance, null, new[] { typeof(string) }, null);
                //var baseCtor = TypeBuilder.GetConstructor(typeof(PropertyNotifierObject), baseNonGenericCtor);
                //var ctor = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard | CallingConventions.HasThis, new Type[0]);
                //var ilGenerator = ctor.GetILGenerator();
                //ilGenerator.Emit(OpCodes.Ldarg_0); // push "this"
                //ilGenerator.Emit(OpCodes.Ldind_Ref, (string)null); // push the param
                //ilGenerator.Emit(OpCodes.Call, baseCtor);
                //ilGenerator.Emit(OpCodes.Ret); 




                //create the DbMappedTable attribute
                ConstructorInfo classCtorInfo = typeof(DbMappedTable).GetConstructor(new[] { typeof(string) });
                CustomAttributeBuilder attributeBuilder = new CustomAttributeBuilder(classCtorInfo, new object[] { table.OwnerSchema().Name + "." + table.Name });
                typeBuilder.SetCustomAttribute(attributeBuilder);

                // Loop over the attributes that will be used as the properties names in out new type
                foreach (Column column in table.Columns)
                {
                    string propertyName = column.Name;

                    // Generate a private field
                    FieldBuilder field = typeBuilder.DefineField("_" + propertyName, typeof(string), FieldAttributes.Private);
                    // Generate a public property
                    PropertyBuilder property = typeBuilder.DefineProperty(propertyName,
                                                                          PropertyAttributes.None,
                                                                          column.CsType(),
                                                                          new[] { column.CsType() });

                    // The property set and property get methods require a special set of attributes:
                    MethodAttributes GetSetAttr = MethodAttributes.Public | MethodAttributes.HideBySig;

                    // Define the "get" accessor method for current private field.
                    MethodBuilder currGetPropMthdBldr = typeBuilder.DefineMethod("get_value",
                                                                                 GetSetAttr,
                                                                                 column.CsType(),
                                                                                 Type.EmptyTypes);

                    // Intermediate Language stuff...
                    ILGenerator currGetIL = currGetPropMthdBldr.GetILGenerator();
                    currGetIL.Emit(OpCodes.Ldarg_0);
                    currGetIL.Emit(OpCodes.Ldfld, field);
                    currGetIL.Emit(OpCodes.Ret);

                    // Define the "set" accessor method for current private field.
                    MethodBuilder currSetPropMthdBldr = typeBuilder.DefineMethod("set_value",
                                                                                 GetSetAttr,
                                                                                 null,
                                                                                 new[] { column.CsType() });

                    // Again some Intermediate Language stuff...
                    ILGenerator currSetIL = currSetPropMthdBldr.GetILGenerator();
                    currSetIL.Emit(OpCodes.Ldarg_0);
                    currSetIL.Emit(OpCodes.Ldarg_1);
                    currSetIL.Emit(OpCodes.Stfld, field);
                    currSetIL.Emit(OpCodes.Ret);

                    // Last, we must map the two methods created above to our PropertyBuilder to 
                    // their corresponding behaviors, "get" and "set" respectively. 
                    property.SetGetMethod(currGetPropMthdBldr);
                    property.SetSetMethod(currSetPropMthdBldr);


                    //create the firstName attribute [FieldOrder(0)]
                    ConstructorInfo dbMappedFieldCtor = typeof(DbMappedField).GetConstructor(new[] { typeof(string) });
                    var dbMappedFieldPkPropInfo = typeof(DbMappedField).GetProperty(GetMemberName.For<DbMappedField>(mf => mf.IsPK));

                    bool columnIsPK = false;
                    var dbMappedFieldBuilder = columnIsPK
                                             ? new CustomAttributeBuilder(dbMappedFieldCtor, new object[] { column.Name }, new[] { dbMappedFieldPkPropInfo }, new object[] { false })
                                             : new CustomAttributeBuilder(dbMappedFieldCtor, new object[] { column.Name });
                    property.SetCustomAttribute(dbMappedFieldBuilder);
                }

                // Generate our type
                Type generatedType = typeBuilder.CreateType();
                generatedTypes.Add(generatedType);
            }

            assemblyBuilder.Save(assemblyName + ".dll"); // apparement pas indispensable, mais utile pour voir ce qui est généré avec ILSpy
            return generatedTypes;
            //// Now we have our type. Let's create an instance from it:
            //object generetedObject = Activator.CreateInstance(generetedType);

            //// Loop over all the generated properties, and assign the values from our XML:
            //PropertyInfo[] properties = generetedType.GetProperties();

            //int propertiesCounter = 0;

            //// Loop over the values that we will assign to the properties
            //foreach (XmlNode node in xmlDoc.SelectSingleNode("root").ChildNodes)
            //{
            //    string value = node.InnerText;
            //    properties[propertiesCounter].SetValue(generetedObject, value, null);
            //    propertiesCounter++;
            //}

            //Yoopy ! Return our new genereted object.
            //   return generetedObject;
        }
    }

   
}
