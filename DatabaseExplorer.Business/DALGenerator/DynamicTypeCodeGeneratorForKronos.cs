﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools.Model.Cache;

using DatabaseExplorer.Business.BaseDALClass;
using DataMapper;

namespace DatabaseExplorer.Business
{

    public class DynamicTypeCodeGeneratorForKronos : DynamicTypeCodeGenerator
    {
        public DynamicTypeCodeGeneratorForKronos(string assName, List<Table> tables, string connectionString, Type entitySpecificBaseClass = null)
            : base(assName, tables, connectionString, entitySpecificBaseClass)
        {
        }

        public override IEnumerable<Type> GetUsingsNamespaces()
        {
            return base.GetUsingsNamespaces().Concat(
                new[]
                    {
// ReSharper disable RedundantNameQualifier
                       // typeof (System.Windows.Forms.Form),
                        typeof (BaseDTO<int>),
// ReSharper restore RedundantNameQualifier
                    }).ToList();
        }
    }
}
