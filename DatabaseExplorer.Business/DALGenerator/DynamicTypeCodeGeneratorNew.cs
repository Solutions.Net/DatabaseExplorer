﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;
using TechnicalTools.Model.Cache;

using DataMapper;

using DatabaseExplorer.Business.BaseDALClass;
using DatabaseExplorer.Business.Extensions;

namespace DatabaseExplorer.Business
{
    public class DynamicTypeCodeGeneratorNew : DynamicTypeCodeGeneratorBase
    {
        public bool GenerateReferencedCollectionProperties { get; set; }
        public bool GenerateReferencingCollectionProperties { get; set; }
        public bool IncludeBaseClassAndDAOInGeneratedCode { get; set; } = true;
        public bool ForSqlCE { get; set; }

        public DynamicTypeCodeGeneratorNew(string assName, IReadOnlyCollection<Table> tables, string connectionString)
            : base(assName, tables, connectionString)
        {
            //EntitySpecificBaseClass = entitySpecificBaseClass;
        }

        public override IEnumerable<Type> GetUsingsNamespaces()
        {
            return base.GetUsingsNamespaces().Concat(
                new Type[]
                    {
// ReSharper disable RedundantNameQualifier
                        //typeof (GenericGeneratedDALEntity<>),
// ReSharper restore RedundantNameQualifier
                    }).ToList();
        }

        public static bool SeemsToBeEnum(Table t)
        {
            return t.Name.Length >= 2 && t.Name[0] == 'e' && char.IsUpper(t.Name[1]);
        }
        public override Dictionary<string, string> GenerateSourceCode()
        {
            var results = base.GenerateSourceCode();
            if (!IncludeBaseClassAndDAOInGeneratedCode)
                return results;
            if (OneFilePerClass)
            {
                var sb = new StringBuilder();
                GenerateNamespaceWrappingCode(sb, () => GenerateEntityBaseClass(sb));
                results.Add(GeneratedEntityBaseClassName + ".cs", sb.ToString());

                sb = new StringBuilder();
                GenerateNamespaceWrappingCode(sb, () => GenerateDAO(sb));
                results.Add(GeneratedDAOClassName + ".cs", sb.ToString());
            }
            else
            {
                var sb = new StringBuilder();
                GenerateNamespaceWrappingCode(sb, () =>
                {
                    GenerateEntityBaseClass(sb);
                    GenerateDAO(sb);
                }, withUsings: false);

                Debug.Assert(results.Count == 1);
                results[results.First().Key] += sb.ToString();
            }
            return results;
        }


        public override void GenerateDalClass(StringBuilder sb, Table table)
        {
            string code = "";
            var tData = DataFor(table);
            code += "namespace " + table.OwnerSchema().Name.Capitalize() + nl;
            code += "{" + nl;

            if (table.PkColumns().Count() >= 2 &&  // Empeche que les table dont la clef primaire reference une autre table (heritage) ne soient considéré comme des tables de jointure
                table.PkColumns().All(pkCol => table.FkConstraints().Any(fkCons => fkCons.ForeignColumns.Any(fkCol => fkCol == pkCol))))
                code += $"    [{nameof(DbMappedJoinTable)}";
            else
                code += $"    [{nameof(DbMappedTable)}";
            code += $"(\"{table.Name}\", \"{table.OwnerSchema().Name}\")]" + nl;
            // We use table.Column instead of table.Pk.CTableName olumns because order is not always the same...
            var idTupleType = table.Columns.Cast<Column>().Where(col => col.InPrimaryKey).Select(col => TypeToString(col.CsType())).Join().IfNotBlankSurroundWith(nameof(IdTuple<int>) + "<", ">")
                           ?? nameof(IdAsRef<string>) + "<" + tData.ClassName + ">";
            code += "    public sealed partial class " + tData.ClassName + " : " + nameof(BaseDTO) + "<" + idTupleType + ">";
            code += nl;
            code += "    {" + nl;

            #region Proprietes

            #region Primaire


            var propElts = new List<string[]>();
            int pkMet = 0;
            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);

                string maxLength = col.HasLengthLimit() ? ", DbMaxLength(" + col.ArrayLength() + ")" : "";
               
                bool isSqlCeTimeSpan = ForSqlCE && col.CsTypeNotNullable() == typeof(TimeSpan);
                bool isSqlXml = col.CsTypeNotNullable() == typeof(SqlXml);
                if (col.InPrimaryKey)
                    ++pkMet;
                string attributes = "[DebuggerHidden][DbMappedField(\"" + col.Name + '"'
                                  + (col.Nullable ? ", " + nameof(DbMappedField.IsNullable) + " = true" : "")
                                  + (col.InPrimaryKey && col.Identity ? ", " + nameof(DbMappedField.IsAutoPK) + " = true" 
                                    : col.InPrimaryKey                ? ", " + nameof(DbMappedField.IsPK) + " = true"
                                    : col.Identity                    ? ", " + nameof(DbMappedField.IsAutoIncremented) + " = true"
                                    : "")
                                  + (table.PkColumns().Count() > 1    ? ", KeyOrderIndex = " + pkMet : "")
                                  + ")" + maxLength + "]";
                //if (isSqlXml)
                //{
                //    propElts.Add(new[]
                //    {
                //        "        ",
                //        "",
                //        "",
                //        "",
                //        "",
                //        "",
                //        "                                  // No need to call Dispose. These methods do nothing in these cases!",
                //        "",
                //        "",
                //        "",
                //        "",
                //        "",
                //        "",
                //        ""
                //    });
                //    propElts.Add(new[]
                //    {
                //        "        ",
                //        attributes,
                //        "private",
                //        TypeToString(typeof(SqlXml)),
                //        "___" + colData.PropName,
                //        "{ [DebuggerStepThrough] get { return",
                //        $"new SqlXml({colData.PropName} == null ? null : new XmlTextReader(new StringReader({colData.PropName})));",
                //        "} [DebuggerStepThrough] set { ",
                //        "",
                //        "",
                //        // We use PropName to get the Changed event
                //        colData.FieldName + " = value.IsNull ? null : value.Value;",
                //        "} }",
                //        "",
                //        ""
                //    });
                //}

                if (isSqlCeTimeSpan)
                {
                    propElts.Add(new[]
                    {
                        "        ",
                        attributes,
                        "private",
                        TypeToString(col.CsType().TryGetNullableType() == null ? typeof(long) : typeof(long?)),
                        "_" + colData.PropName  + "AsLong",
                        "{ [DebuggerStepThrough] get { return",
                        (col.Nullable ? colData.FieldName + " == null ? (long?)null : " : "") + colData.FieldName + (col.Nullable ? ".Value" : "") + ".Ticks;",
                        "} [DebuggerStepThrough] set { ",
                        "",
                        "",
                        // We use PropName to get the Changed event
                        colData.PropName + " = " + (col.Nullable ? " value == null ? (TimeSpan?)null : " : "") + "new TimeSpan(value" + (col.Nullable ? ".Value" : "") + ");",
                        "} }",
                        "",
                        ""
                    });
                }

                propElts.Add(new[]
                {
                    "        ",
                    isSqlCeTimeSpan /*|| isSqlXml*/ ? "" : attributes,
                    //"[DAL_Generator.Business.DynamicTypeCodeGenerator.FastDbMappedField(\"" + col.Name + '"' + (col.InPrimaryKey ? ", IsPK = true" : "") + ")]",
                    "public",
                    TypeToString(isSqlXml ? typeof(string) : col.CsType()),
                    colData.PropName,
                    "{ [DebuggerStepThrough] get { return",
                    colData.FieldName + ";",
                    "} [DebuggerStepThrough] set { ",
                    col.CsTypeNotNullable() == typeof(DateTime)                                                       
                                     ? "ChkRange(value, Ranges." + colData.PropName + ");"
                                     : col.HasLengthLimit() /*&& underlyingType != typeof(Microsoft.SqlServer.Types.SqlHierarchyId)*/ ? "ChkFieldLen(ref value, " + col.ArrayLength() + ");"
                                     : "",
                    (col.InPrimaryKey && table.FkConstraints().All(fk => fk.ForeignColumns.Length == 0) ? "SetTechnicalProperty" : "SetProperty") + "(ref ",
                    colData.FieldName + ", value" + (col.InPrimaryKey && table.FkConstraints().All(fk => fk.ForeignColumns.Length == 0) ? ", RaiseIdChanged" : "") + ");",
                    "} }",
                    TypeToString(isSqlXml ? typeof(string) : col.CsType()),
                    colData.FieldName + ";"
                });
            }
            const String_Extensions.ePadding right = String_Extensions.ePadding.Right;
            const String_Extensions.ePadding left = String_Extensions.ePadding.Left;
            const String_Extensions.ePadding none = String_Extensions.ePadding.None;
            code += propElts.AlignAndMerge(new[] { none, right, none, left, right, none, left, none, left, left, left, none, left, right  }, null).ToString() + nl;

            #endregion

            code += nl;
            code += nl;
            code += "        protected override " + idTupleType + " ClosedId" + nl;
            code += "        {" + nl;
            // We use table.Column instead of table.Pk.Columns because order is not always the same...
            code += "           get { return new " + idTupleType + "(" + (table.Columns.Cast<Column>().Where(col => col.InPrimaryKey).Select(col => DataFor(col).PropName).Join().IfBlankUse(null) ?? "") + "); }" + nl;
            code += "           set { " + (table.Columns.Cast<Column>().Where(col => col.InPrimaryKey).Select((col, i) => DataFor(col).PropName + " = value.Id" + (i + 1) + ";").Join(" ").IfBlankUse(null) ?? "") + " }" + nl;
            code += "        }" + nl;

            #endregion

            #region Constructeur et Initialisation
            code += nl;
            code += nl;
            code += "        public " + tData.ClassName + "()" + nl;
            code += "           : this(true)" + nl;
            code += "        {" + nl;
            code += "        }" + nl;
            code += "        public " + tData.ClassName + "(bool initializeModelValues)" + nl;
            code += "        {" + nl;
            code += "            if (!initializeModelValues)" + nl;
            code += "                return;" + nl;
            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);

                if (col.Nullable && col.DefaultValue() != null)
                {
                    Func<string, int, int> getMatchingParenthesisIndex = (str, i) =>
                    {
                        int depth = 0;
                        while (++i < str.Length)
                            if (str[i] == '(')
                                ++depth;
                            else if (str[i] == ')')
                                if (depth == 0)
                                    return i;
                                else
                                    --depth;
                        return 0;
                    };

                    // Si une propriete est null est que la base spécifie une valeur par default pour la colonne associée,
                    // alors la valeur enregistré par les recordset est la valeur par defaut défini en base. LA requete généré par le recordset,
                    // écarte le champs à null et laisse la base accetper la valeur (si la colonne est nullable) ou bien la remplacer si il y a une valeur par defaut)
                    // La méthode Resync() du recordset met celui ci a jour dans le cas ou il y a des modifications en base (valeur par defaut et peut etre, trigger (TODO ?))
                    // donc plutôt que de le faire au moment de l'enregistrement, pour la transparence, on le fait dès la création de l'objet

                    // En revanche DefaultValue est stocke sous forme de chaine donc c'est galere de convertir en valeur C#
                    object defaultValue = col.DefaultValue();
                    if (defaultValue is string) // ne signifie pas que col.CsType est de type string (exemple: ça peut etre "((0.00))" pour initialiser une valeur de type "decimal?")
                        while (((string)defaultValue).StartsWith("(") && getMatchingParenthesisIndex((string)defaultValue, 0) == ((string)defaultValue).Length - 1) // gere le cas "((0))"
                            defaultValue = ((string)defaultValue).Substring(1, ((string)defaultValue).Length - 2);
                    if (col.CsTypeNotNullable() == typeof(bool))
                        defaultValue = col.DefaultValue().Contains("1") ? "true" : "false";
                    if (col.CsType() == typeof(string))
                    {
                        if (((string)defaultValue).StartsWith("'") && ((string)defaultValue).EndsWith("'"))
                            defaultValue = "\"" + ((string)defaultValue).Substring(1, ((string)defaultValue).Length - 2).Replace(@"\", @"\\").Replace("\"", "\\\"") + "\"";
                    }
                    if (defaultValue is string && ((string)defaultValue) == "NULL")
                        defaultValue = "null";
                    if (defaultValue is string && col.CsType() == typeof(long?) && ((string)defaultValue).In("''", "null''")) // Et oui c'est possible une colonne de type bigint nullable peut avoir une valeur ('') definit par defaut !
                        defaultValue = "null";
                    if (col.CsTypeNotNullable() == typeof(decimal) && char.IsDigit(defaultValue.ToString().Last()))
                        defaultValue = (string)defaultValue + "m";
                    if (defaultValue is string && (((string)defaultValue).ToLower() == "getdate()" || ((string)defaultValue).ToLower() == "sysdatetime()"))
                    {
                        //DebugTools.Break("Cas à gérer... ca signifie qu'il faut recharger les objets après les avoir enregistré en base." +
                        //                 "Ca peut être complexe a gerer. Il est plus simple de faire en sorte que les objets créé initialisent eux-même les valeurs");
                        defaultValue = "DateTime.UtcNow";
                        continue;
                    }
                    code += "            " + colData.FieldName + " = " + defaultValue + ";" + nl;
                }
                else if (!col.Nullable && col.CsType() == typeof(string))
                    code += "            " + colData.FieldName + " = string.Empty;" + nl; // le setter interdira de mettre null par la suite
            }
            code += nl;
            code += "        }" + nl;

            #endregion

            #region Chargement

            if (code == null) // code disabled
            {
                code += nl;
                code += nl;
                code += "        public static void " + FillFromReaderMethodName + "(System.Data.Common.DbDataReader reader, List<" + tData.ClassName + "> items)" + nl;
                code += "        {" + nl;
                code += "            #if DEBUG" + nl;
                code += "            int before = items.Count;" + nl;
                code += "            var sw = new System.Diagnostics.Stopwatch();" + nl;
                code += "            sw.Start();" + nl;
                code += "            #endif" + nl;

                code += "            while (reader.Read())" + nl;
                code += "            {" + nl;
                code += "                var item = new " + tData.ClassName + "();" + nl;
                foreach (Column col in table.Columns)
                {
                    var tCol = DataFor(col);
                    code += "                item." + tCol.FieldName + " = ";
                    if (col.Nullable)
                        code += "reader.IsDBNull(" + (col.OrdinalPosition() - 1) + ") ? null : ";
                    if (ForSqlCE && (col.CsType() == typeof(TimeSpan) || col.CsType() == typeof(TimeSpan?)))
                        code += (col.Nullable ? "(" + TypeToString(col.CsType()) + ")" : "")
                                + "new TimeSpan((long)reader.GetValue(" + (col.OrdinalPosition() - 1) + "));" + nl;
                    else
                        code += "(" + TypeToString(col.CsType()) + ")" + "reader.GetValue(" + (col.OrdinalPosition() - 1) + ");";
                    code += " // " + col.Name + nl;
                }

                code += "                items.Add(item);" + nl;
                code += "            }" + nl;

                code += "            #if DEBUG" + nl;
                code += "            if (items.Count - before > 0)" + nl;
                code += "            {" + nl;
                code += "                System.Diagnostics.Debug.WriteLine(\"Chargement des données : \" + sw.Elapsed.ToString());" + nl;
                code += "                System.Diagnostics.Debug.WriteLine(\"  soit, par propriété (" + table.Columns.Count + "): \" + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * " + table.Columns.Count + ")).ToString());" + nl;
                code += "            }" + nl;
                code += "            #endif" + nl;
                code += "        }" + nl;

                #endregion
            }

            #region Cloning
            code += nl;
            code += nl;
            code += "        #region Cloneable" + nl;
            code += nl;
            code += "        public new " + tData.ClassName + " Clone() { return (" + tData.ClassName + ")(this as ICloneable).Clone(); }" + nl;
            code += "        protected override " + nameof(BaseDTO) + " CreateNewInstance() { return new " + tData.ClassName + "(); }" + nl;
            code += "        public override void CopyAllFieldsFrom(" + nameof(BaseDTO) + " source)" + nl;
            code += "        {" + nl;
            code += "            var from = (" + tData.ClassName + ")source;" + nl;
            code += "            // Note : Les champs appartenant à la PK ne sont pas copiés;" + nl;
            foreach (Column col in table.Columns)
                if (!col.InPrimaryKey)
                {
                    var tCol = DataFor(col);
                    code += "            " + tCol.FieldName + " = from." + tCol.FieldName + ";" + nl;
                }
            code += "        }" + nl;
            code += nl;
            code += "        #endregion" + nl;

            #endregion


            #region Value Range

            code += nl;
            code += nl;
            code += "        public static class Ranges" + nl;
            code += "        {" + nl;
            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);
                if (col.CsTypeNotNullable() == typeof(DateTime))
                    code += "            public static readonly RangeOf<DateTime> " + colData.PropName + " = new RangeOf<DateTime>(" + 
                            "new DateTime(" + ((DateTime)col.MinValue()).Ticks + ")," +
                            "new DateTime(" + ((DateTime)col.MaxValue()).Ticks + ")"
                            + "); // from " + ((DateTime)col.MinValue()).ToString("o") +  " to " + ((DateTime)col.MaxValue()).ToString("o") + nl;
            }
            code += "        }" + nl;

            #endregion

            code += @"    }" + nl; // class

            code += @"}" + nl; // Schema namespace  (dbo, ...)

            sb.Append("    " + code.Replace(Environment.NewLine, Environment.NewLine + "    "));
        }
        public string FillFromReaderMethodName { get { return GenericGeneratedDAO.FillFromReaderMethodName; } }

        protected virtual void GenerateEntityBaseClass(StringBuilder sb)
        {
//            Func<string, string> IfWithIdManager = str => WithIdManager ? str : "";
//            sb.AppendLine((@"
//    public interface I" + GeneratedEntityBaseClassName + @" : IGeneratedGenericDALEntity, System.ICloneable
//    {
//        void Delete();
//        void SynchronizeToDatabase();
//    }

//    public abstract partial class " + GeneratedEntityBaseClassName + @"<TId> : GenericGeneratedDALEntity<TId>, I" + GeneratedEntityBaseClassName + @"
//        where TId : struct, IEquatable<TId>, IIdTuple
//    {
//        internal static readonly bool IsPrimaryObject = typeof(TId) == typeof(IdTuple<long>);

//        protected " + GeneratedEntityBaseClassName + @"(TId? id = null)
//        {
//        " + IfWithIdManager(
//      @"    if (IsPrimaryObject)
//                if (id.HasValue)
//                    ClosedId = id;
//                else
//                    ClosedId = (TId)(object)new IdTuple<long>((long)GeneratedDAO.Instance.CreateUniqueId());
//        ") +
//      @"}


//        protected static IEnumerable<T> GetRowsAndConvert<T>(string sqlQueryToGetRows, Func<System.Data.DataRow, T> convert)
//        {
//            var dt = GeneratedDAO.Instance.WithConnection(con => con.GetDatatable(sqlQueryToGetRows));
//            return dt.AsEnumerable().Select(convert);
//        }

//        public override void LoadById(TId id)
//        {
//            " + IfWithIdManager(
//           @"TId? oldId = IsPrimaryObject && State == eState.New && !ClosedId.Value.Equals(id) ? ClosedId : null;
//            ") +
//            @"#pragma warning disable 184
//            // ReSharper disable HeuristicUnreachableCode                                                                                                                             
//            GeneratedDAO.Instance.Mapper.AutoLoadItem(this, id);
//            // ReSharper restore HeuristicUnreachableCode                                                                                                                             
//            #pragma warning restore 184
//            " + IfWithIdManager(
//          @"
//            if (oldId.HasValue)
//                GeneratedDAO.Instance.CancelCreationOf((ulong)(object)((IdTuple<long>)(object)oldId.Value).Id1);
//           ") +
//            @"
//        }
//        public override void SynchronizeToDatabase()
//        {
//            GeneratedDAO.Instance.Mapper.SynchronizeToDatabase(this);
//        }
//        public void Delete()
//        {
//            //if (Debugger.IsAttached)
//            //    Debugger.Break();
//            var tran = GeneratedDAO.Instance.CurrentDBConnection == null ? null
//                     : GeneratedDAO.Instance.CurrentDBConnection.CurrentActiveTransaction;
//            if (tran != null)
//            {
//                var restoreState = CreateActionToRestoreState();
//                tran.OnRollback += restoreState; // Pas besoin de gérer la désinscription car la référence à la transaction disparaitra toute seule                                   
//            }
//            UpdateState(eState.ToDelete);
//            SynchronizeToDatabase();
//        }

//    }
//").Replace("{DQ}", "\""));
        }
        public const string GeneratedEntityBaseClassName = "GeneratedDALEntity";

        protected virtual void GenerateDAO(StringBuilder sb)
        {
            sb.AppendLine(($@"
    public partial class {GeneratedDAOClassName} : {nameof(DbGeneratedMapper)}
    {{
        public {GeneratedDAOClassName}({nameof(IDbMapper)} mapper)
            : base(mapper)
        {{
        }}
        
        public override Dictionary<Type, Type> AllTypes {{ get {{ return _AllTypes; }} }}
        readonly Dictionary<Type, Type> _AllTypes = new List<Type>()
        {{
            {Tables.Where(t => !SeemsToBeEnum(t))
                   .Select(table => "    typeof(" + table.OwnerSchema().Name.Capitalize() + "." + DataFor(table).ClassName + "),")
                   .Join(Environment.NewLine)}
        }}.ToDictionary(t => t);
    }}")/*.Replace("{DQ}", "\"")*/);
        }
        public const string GeneratedDAOClassName = "GeneratedDAO";

    }


}
