﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

using TechnicalTools.Model.Cache;

namespace DatabaseExplorer.Business
{
    [DebuggerDisplay("{" + nameof(AsDebugString) + ",nq}")]
    [Serializable]
    public struct IdAsRef<TRef> : IIdTuple<TRef>, IEquatable<IIdTuple<TRef>>, IEquatable<IdAsRef<TRef>>
        where TRef : class, IEquatable<TRef>, IComparable<TRef>, IComparable
    {
        public IdAsRef(TRef objectRef)
        {
            Ref = objectRef;
        }

        public TRef Ref { get; }  TRef IIdTuple<TRef>.Id1 { get { return Ref; } }

        public         IReadOnlyCollection<Type> KeyTypes { get { return _KeyTypes; } }
        static readonly ReadOnlyCollection<Type> _KeyTypes = new ReadOnlyCollection<Type>(new [] { typeof(TRef) });

        public IReadOnlyList<IComparable> Keys { get { return new IComparable[] { Ref }; } }
        public TypedId<TRef>    ToTypedId(Type type) { return new TypedId<TRef>(type, this); }
              ITypedId IIdTuple.ToTypedId(Type type) { return ToTypedId(type); }
        public  IdTuple<TRef, TNewKey>                ExtendWith<TNewKey>(TNewKey new_key) where TNewKey : IEquatable<TNewKey>, IComparable<TNewKey>, IComparable { return new IdTuple<TRef, TNewKey>(Ref, new_key); }
               IIdTuple<TRef, TNewKey> IIdTuple<TRef>.ExtendWith<TNewKey>(TNewKey new_key) { return ExtendWith(new_key); }
               IIdTuple                IIdTuple      .ExtendWith<TNewKey>(TNewKey new_key) { return ExtendWith(new_key); }
        public IIdTuple<TRef>                         Shortenize() { return null; }
               IIdTuple                IIdTuple      .Shortenize() { return Shortenize(); }

        private string AsDebugString { get { return "[" + (Ref == null ? "NO ID" : nameof(IdAsRef<TRef>) + " = " + GetRandomThenImmutableId.For(Ref)) + "]"; } }
        public override string ToString() { return AsDebugString; }

        #region Equality members

        public override int GetHashCode()
        {
            return EqualityComparer<TRef>.Default.GetHashCode(Ref);
        }
        public override bool Equals(object obj)
        {
            return !(obj is null) && obj is IdAsRef<TRef> && Equals((IdAsRef<TRef>)obj);
        }
        public bool Equals(IIdTuple<TRef> other)
        {
            return other is IdAsRef<TRef> iar && Equals(iar);
        }
        public bool Equals(IdAsRef<TRef> other)
        {
            return ReferenceEquals(Ref, other.Ref);
        }


        public static bool operator ==(IdAsRef<TRef> left, IdAsRef<TRef> right)
        {
            return left.Equals(right);
        }
        public static bool operator !=(IdAsRef<TRef> left, IdAsRef<TRef> right)
        {
            return !left.Equals(right);
        }

        #endregion

        class GetRandomThenImmutableId
        {
            public static int For(TRef obj)
            {
                // En C++, cette methode pourrait simplement retourner l'adresse mémoire de obj
                // En C# cela est impossible car le GC peut deplacer l'objet a n'importe quel moment
                // On est donc obligé d'utiliser une ConditionalWeakTable
                return _debugIds.GetValue(obj, _ => new GetRandomThenImmutableId())._Value;
            }
            private GetRandomThenImmutableId() { }
            readonly int _Value = Interlocked.Increment(ref _valueSeed);
            static int _valueSeed;

            static readonly ConditionalWeakTable<TRef, GetRandomThenImmutableId> _debugIds = new ConditionalWeakTable<TRef, GetRandomThenImmutableId>();
        }

        public static IdAsRef<TRef> Create(TRef obj)
        {
            return new IdAsRef<TRef>(obj);
        }
    }
}
