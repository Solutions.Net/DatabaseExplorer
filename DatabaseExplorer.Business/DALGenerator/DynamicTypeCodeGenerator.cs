﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;
using TechnicalTools.Model.Cache;

using DatabaseExplorer.Business.BaseDALClass;
using DatabaseExplorer.Business.Extensions;
using DataMapper;

namespace DatabaseExplorer.Business
{

    public class DynamicTypeCodeGenerator : DynamicTypeCodeGeneratorBase
    {
        public bool GenerateReferencedCollectionProperties { get; set; }
        public bool GenerateReferencingCollectionProperties { get; set; }
        public Type EntitySpecificBaseClass { get; private set; }
        public bool WithIdManager { get; set; }
        public bool IncludeBaseClassAndDAOInGeneratedCode { get; set; } = true;
        public bool ForSqlCE { get; set; }

        public DynamicTypeCodeGenerator(string assName, IReadOnlyCollection<Table> tables, string connectionString, Type entitySpecificBaseClass = null)
            : base(assName, tables, connectionString)
        {
            EntitySpecificBaseClass = entitySpecificBaseClass;
            if (entitySpecificBaseClass == null)
                entitySpecificBaseClass = typeof(BaseDTO<>);
            // utiliser : http://stackoverflow.com/a/18828085 pour tester
            //else if (!entitySpecificBaseClass.IsGenericType || !entitySpecificBaseClass.IsAssignableFrom(typeof(GeneratedEntityBase<>)))
            //    throw new ArgumentNullException("entitySpecificBaseClass", "entitySpecificBaseClass argument must be a generic type inheriting from " + typeof (GeneratedEntityBase<>).Name);
            EntitySpecificBaseClass = entitySpecificBaseClass;
        }

        public override IEnumerable<Type> GetUsingsNamespaces()
        {
            return base.GetUsingsNamespaces().Concat(
                new[]
                    {
// ReSharper disable RedundantNameQualifier
                        typeof (BaseDTO<>),
                        typeof (System.Data.SqlTypes.SqlXml), // pour certaines base de données comme AdventureWorks
// ReSharper restore RedundantNameQualifier
                    }).ToList();
        }

        public static bool SeemsToBeEnum(Table t)
        {
            return t.Name.Length >= 2 && t.Name[0] == 'e' && char.IsUpper(t.Name[1]);
        }
        public override Dictionary<string, string> GenerateSourceCode()
        {
            var results = base.GenerateSourceCode();
            if (!IncludeBaseClassAndDAOInGeneratedCode)
                return results;
            if (OneFilePerClass)
            {
                var sb = new StringBuilder();
                GenerateNamespaceWrappingCode(sb, () => GenerateEntityBaseClass(sb));
                results.Add(GeneratedEntityBaseClassName + ".cs", sb.ToString());

                sb = new StringBuilder();
                GenerateNamespaceWrappingCode(sb, () => GenerateDAO(sb));
                results.Add(GeneratedDAOClassName + ".cs", sb.ToString());
            }
            else
            {
                var sb = new StringBuilder();
                GenerateNamespaceWrappingCode(sb, () =>
                {
                    GenerateEntityBaseClass(sb);
                    GenerateDAO(sb);
                }, withUsings: false);

                Debug.Assert(results.Count == 1);
                results[results.First().Key] += sb.ToString();
            }
            return results;
        }


        public override void GenerateDalClass(StringBuilder sb, Table table)
        {
            string code = "";
            var tData = DataFor(table);
            code += "namespace " + table.Schema.Capitalize() + nl;
            code += "{" + nl;

            if (table.PkColumns().Count() >= 2 &&  // Empeche que les table dont la clef primaire reference une autre table (heritage) ne soient considéré comme des tables de jointure
                table.PkColumns().All(pkCol => table.FkConstraints().Any(fkCons => fkCons.ForeignColumns.Any(fkCol => fkCol == pkCol))))
                code += "    [DbMappedJoinTable(" + '"' + (ForSqlCE ? "" : table.OwnerSchema().Name + ".") + table.Name + "\")]" + nl;
            else
                code += "    [DbMappedTable(" + '"' + (ForSqlCE ? "" : table.OwnerSchema().Name + ".") + table.Name + "\")]" + nl;
            code += "    [MetadataType(typeof(" + tData.ClassName + ".Metadata))]" + nl;
            code += "    public sealed partial class " + tData.ClassName + " : " + GeneratedEntityBaseClassName + "<" + tData.IdType + ">, " + "HasPropertiesOf." + table.OwnerSchema().Name.Capitalize() + "." + tData.ClassName;
            if (tData.BaseTable != null)
                code += ", IHasDALEntityAncestor";
            code += nl;
            code += "    {" + nl;

            #region Gestion ClosedId et Type
            code += "        protected override " + tData.IdType + "? ClosedId";
            if (table.PkColumns().Any())
            {
                code += nl;
                code += "        {" + nl;
                code += "            get" + nl;
                code += "            {" + nl;
                code += "                return new " + tData.IdType + "(" + table.PkColumns().Select(col => DataFor(col).PropName).Join() + ");" + nl;
                code += "            }" + nl;
                code += "            set" + nl;
                code += "            {" + nl;
                code += "                System.Diagnostics.Debug.Assert(value.HasValue, \"La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.\");" + nl;
                for (int c = 0; c < table.PkColumns().Count(); ++c)
                    code += "                " + DataFor(table.PkColumns()[c]).PropName + " = (" + TypeToString(table.PkColumns()[c].CsType()) + ")value.Value.Keys[" + c + "];" + nl;
                code += "            }" + nl;
                code += "        }" + nl;
                code += "        " + tData.IdType + "? HasPropertiesOf." + table.OwnerSchema().Name.Capitalize() + "." + tData.ClassName + ".ClosedId { get { return ClosedId; } }" + nl;
            }
            else
            {
                code += " { get; set; }" + nl;
            }
            code += "        public static readonly Type IdType = typeof(" + tData.IdType + ");" + nl;
            code += nl;

            if (table.PkColumns().Any())
            {
                code += "        public static " + tData.IdType + " MakeClosedId(" + table.PkColumns().Select(col => TypeToString(col.CsType()) + " " + DataFor(col).AsArgName).Join() + ")" + nl;
                code += "        {" + nl;
                code += "            return new " + tData.IdType + "(" + table.PkColumns().Select(col => DataFor(col).AsArgName).Join() + ");" + nl;
                code += "        }" + nl;
                code += nl;

                code += nl;
                code += nl;
                code += "        public static IEnumerable<" + tData.IdType + "> " + GetIdsWhereMethodName + "(string whereCondition)" + nl;
                code += "        {" + nl;
                code += "            return GetRowsAndConvert" +/*"<" + tData.IdType + ">"+*/"(\"SELECT " + tData.PkColumnsSqlList + " FROM " + (ForSqlCE ? table.Name : table.FullName()) + "\" + (string.IsNullOrWhiteSpace(whereCondition) ? \"\" : \" WHERE \" + whereCondition)," + nl;
                code += "                                     " + new String(' ', tData.IdType.Length) + "  row => " + tData.CreateIdFromDataRow("row") + ");" + nl;
                code += "        }" + nl;

                code += nl;
                code += nl;
                code += "        public static List<" + tData.ClassName + "> GetEntitiesWhere(string sqlFilter)" + nl;
                code += "        {" + nl;
                code += "            return GetEntitiesWithIds(GetIdsWhere(sqlFilter));" + nl;
                code += "        }" + nl;

                code += nl;
                code += nl;
                code += "        public static List<" + tData.ClassName + "> GetEntitiesWithIds(IEnumerable<" + tData.IdType + "> ids)" + nl;
                code += "        {" + nl;
                code += "            var entities = " + GeneratedDAOClassName + ".Instance.GetAllByDbMapper<" + tData.ClassName + ", " + tData.IdType + ">(ids);" + nl;
                code += "            return entities;" + nl;
                code += "        }" + nl;
                if (table.PkColumns().Count == 1 && (table.PkColumns()[0].CsType().IsValueType || table.PkColumns()[0].CsType() == typeof(string)))
                {
                    code += nl;
                    code += nl;
                    code += "        public static List<" + tData.ClassName + "> GetEntitiesWithIds(IEnumerable<" + TypeToString(table.PkColumns()[0].CsType()) + "> ids)" + nl;
                    code += "        {" + nl;
                    code += "            var entities = " + GeneratedDAOClassName + ".Instance.GetAllByDbMapper<" + tData.ClassName + ", " + tData.IdType + ">(ids.Select(id => new " + tData.IdType + "(id))" + ");" + nl;
                    code += "            return entities;" + nl;
                    code += "        }" + nl;
                }

            }
            #endregion

            #region Proprietes

            #region Heritage

            // La clef primaire référence une autre table
            if (tData.BaseTable != null)
            {
                var tBaseData = DataFor(tData.BaseTable);

                code += nl;
                code += nl;
                code += "        [Browsable(false)]" + nl;
                code += "        public " + tBaseData.ClassName + " Base { get { return _base ?? CacheManager.Instance.Get<" + tBaseData.ClassName + ">(new " + tBaseData.IdType + "(" + tData.BaseTableFk.ForeignColumns.Select(col => DataFor(col).PropName).Join() + ")); } }" + nl;
                code += "        IGeneratedGenericDALEntity IHasDALEntityAncestor.Base { get { return _base; } set { _base = (" + tBaseData.ClassName + ")value; } } " + tBaseData.ClassName + " _base;" + nl;
                code += "        Type IHasDALEntityAncestor.BaseType { get { return typeof(" + tBaseData.ClassName + "); } }" + nl;
            }
            #endregion Heritage

            #region Primaire


            var propElts = new List<string[]>();
            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);

                string maxLength = col.HasLengthLimit() ? ", DbMaxLength(" + col.ArrayLength() + ")" : "";
                var underlyingType = col.CsTypeNotNullable();
                var dbg = col.CsType().Name;
                bool isSqlCeTimeSpan = ForSqlCE && col.CsTypeNotNullable() == typeof(TimeSpan);
                string attributes = "[DebuggerHidden][DbMappedField(\"" + col.Name + '"' + (col.InPrimaryKey ? ", IsPK = true" : "") + (col.Nullable ? ", IsNullable = true" : "") + ")" + maxLength + "]";
                if (isSqlCeTimeSpan)
                {
                    propElts.Add(new[]
                    {
                        "        ",
                         attributes,
                        "private",
                        TypeToString(col.CsType().TryGetNullableType() == null ? typeof(long) : typeof(long?)),
                         "_" + colData.PropName  + "AsLong",
                        "{ [DebuggerStepThrough] get { return",
                        (col.Nullable ? colData.FieldName + " == null ? (long?)null : " : "") + colData.FieldName + (col.Nullable ? ".Value" : "") + ".Ticks;",
                        "} [DebuggerStepThrough] set { ",
                        "",
                        "",
                        // We use PropName to get the Changed event
                        colData.PropName + " = " + (col.Nullable ? " value == null ? (TimeSpan?)null : " : "") + "new TimeSpan(value" + (col.Nullable ? ".Value" : "") + ");",
                        "} }",
                        "",
                        ""
                    });
                }

                propElts.Add(new[]
                {
                    "        ",
                    isSqlCeTimeSpan ? "" : attributes,
                    //"[DAL_Generator.Business.DynamicTypeCodeGenerator.FastDbMappedField(\"" + col.Name + '"' + (col.IsInPk ? ", IsPK = true" : "") + ")]",
                    "public",
                    TypeToString(col.CsType()),
                    colData.PropName,
                    "{ [DebuggerStepThrough] get { return",
                    colData.FieldName + ";",
                    "} [DebuggerStepThrough] set { ",
                    underlyingType == typeof(DateTime)                                                       
                                     ? "ChkRange(value, MinValueOf." + colData.PropName + ", MaxValueOf." + colData.PropName + ");"
                                     : col.HasLengthLimit() /*&& underlyingType != typeof(Microsoft.SqlServer.Types.SqlHierarchyId)*/
                                     ? "ChkFieldLen(ref value, " + col.ArrayLength() + ");"
                                     : "",
                    (col.InPrimaryKey && !col.IsFK() ? "SetTechnicalProperty" : "SetProperty") + "(ref ",
                    colData.FieldName + ", value" + (col.InPrimaryKey ? ", RaiseIdChanged" : "") + ");",
                    "} }",
                    TypeToString(col.CsType()),
                    colData.FieldName + ";"
                });
            }
            code += nl;
            code += nl;
            const String_Extensions.ePadding right = String_Extensions.ePadding.Right;
            const String_Extensions.ePadding left = String_Extensions.ePadding.Left;
            const String_Extensions.ePadding none = String_Extensions.ePadding.None;
            code += propElts.AlignAndMerge(new[] { none, right, none, left, right, none, left, none , left, left, left, none, left, right }, null).ToString() + nl;

            #endregion

            #region Navigation

            if (GenerateReferencedCollectionProperties)
            {
                code += nl;
                foreach (var fkConstraint in table.FkConstraints())
                {
                    var referencingTable = fkConstraint.ReferencedColumns[0].OwnerTable();
                    if (!Tables.Contains(referencingTable))
                        continue;
                    if (!referencingTable.PkColumns().Any()) // pas d'id, on ne peut pas utiliser le cache
                        continue;
                    if (referencingTable == table)
                        continue;
                    if (SeemsToBeEnum(referencingTable))
                        continue;
                    var tRefTable = DataFor(referencingTable);
                    var referencingType = table.Owner == referencingTable.Owner && table.Columns.Cast<Column>().All(c => DataFor(c).PropName != tRefTable.ClassName) ? tRefTable.ClassName : tRefTable.FullClassName;
                    var propName = "Via_" + tRefTable.ClassNamePlural + "__" + fkConstraint.ForEach((referencingCol, _) => DataFor(referencingCol).PropName).Join("And");
                    if (propName.Contains('.'))
                        continue;
                    // Note : Browsable(false) empeche Devexpress de generer automatiquement une colonne
                    string sqlFilter = fkConstraint.ForEach((referencingCol, referencedCol) => (referencingCol.Nullable ? DataFor(referencingCol).PropName + " == null ? \"0=1\" : " : "") +
                                                                                               "\"" + referencedCol.Name + " = \" + " + DataFor(referencingCol).PropName).Join(" + \" AND \" + ");
                    code += "        [Browsable(false)] public IEnumerable<" + tRefTable.IdType + "> " + propName + "Ids { get { return " + referencingType + ".GetIdsWhere(" + sqlFilter + ");" + " } }" + nl;
                    code += "        public List<" + referencingType + "> " + propName +
                            " { get { " +
                            "return (List<" + referencingType + ">)" + GeneratedDAOClassName + ".Instance.GetAllByDbMapper<" + referencingType + ", " + tRefTable.IdType + ">(" + propName + "Ids);" +
                            " } }" + nl; // TODO : mettre le namespace devant le nom du type si les schemas ont des noms différent !
                    tData.NavPropertiesVia.Add(fkConstraint, propName);
                }
            }

            if (GenerateReferencingCollectionProperties)
            {
                code += nl;
                foreach (var fkConstraint in table.FkReferencing())
                {
                    var referencingTable = fkConstraint.ForeignColumns[0].OwnerTable();
                    if (!Tables.Contains(referencingTable))
                        continue;
                    if (referencingTable.PkColumns().IsEmpty()) // pas d'id, on ne peut pas utiliser le cache
                        continue;
                    if (referencingTable == table)
                        continue;
                    var tRefTable = DataFor(referencingTable);
                    var referencingType = table.Owner == referencingTable.Owner && table.Columns.Cast<Column>().All(c => DataFor(c).PropName != tRefTable.ClassName) ? tRefTable.ClassName : tRefTable.FullClassName;
                    var propName = "RefBy" + tRefTable.ClassNamePlural + "_" + fkConstraint.ForEach((referencingCol, _) => DataFor(referencingCol).PropName).Join("And");
                    if (propName.Contains('.'))
                        continue;

                    // Note : Browsable(false) empeche Devexpress de generer automatiquement une colonne
                    string sqlFilter = fkConstraint.ForEach((referencingCol, referencedCol) => (referencedCol.Nullable ? DataFor(referencedCol).PropName + " == null ? \"0=1\" : " : "") +
                                                                                               "\"" + referencingCol.Name + " = \" + " + DataFor(referencedCol).PropName).Join(" + \" AND \" + ");
                    code += "        [Browsable(false)] public IEnumerable<" + tRefTable.IdType + "> " + propName + "Ids { get { return " + referencingType + ".GetIdsWhere(" + sqlFilter + ");" + " } }" + nl;
                    code += "        public List<" + referencingType + "> " + propName +
                            " { get { " +
                            "return (List<" + referencingType + ">)" + GeneratedDAOClassName + ".Instance.GetAllByDbMapper<" + referencingType + ", " + tRefTable.IdType + ">(" + propName + "Ids);" +
                            " } }" + nl; // TODO : mettre le namespace devant le nom du type si les schemas ont des noms différent !
                    tData.NavPropertiesFrom.Add(fkConstraint, propName);
                }
            }

            #endregion

            #endregion

            #region Constructeur et Initialisation
            code += nl;
            code += nl;
            code += "        public " + tData.ClassName + "()" + nl;
            code += "        {" + nl;
            if (table.PkColumns().IsEmpty())
                code += "            ClosedId = " + nameof(IdAsRef<string>) + "<" + tData.ClassName + ">.Create(this);" + nl;
            code += "        }" + nl;
            code += "        public " + tData.ClassName + "(bool initializeModelValues" + (table.PkColumns().Any() ? ", " + tData.IdType + "? id = null" : "") + ")" + nl;
            code += table.PkColumns().IsEmpty() ? "" : "            : base(id)" + nl;
            code += "        {" + nl;
            code += "            if (!initializeModelValues)" + nl;
            code += "                return;" + nl;
            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);

                if (col.Nullable && col.DefaultValue() != null)
                {
                    Func<string, int, int> getMatchingParenthesisIndex = (str, i) =>
                    {
                        int depth = 0;
                        while (++i < str.Length)
                            if (str[i] == '(')
                                ++depth;
                            else if (str[i] == ')')
                                if (depth == 0)
                                    return i;
                                else
                                    --depth;
                        return 0;
                    };

                    // Si une propriete est null est que la base spécifie une valeur par default pour la colonne associée,
                    // alors la valeur enregistré par les recordset est la valeur par defaut défini en base. LA requete généré par le recordset,
                    // écarte le champs à null et laisse la base accetper la valeur (si la colonne est nullable) ou bien la remplacer si il y a une valeur par defaut)
                    // La méthode Resync() du recordset met celui ci a jour dans le cas ou il y a des modifications en base (valeur par defaut et peut etre, trigger (TODO ?))
                    // donc plutôt que de le faire au moment de l'enregistrement, pour la transparence, on le fait dès la création de l'objet

                    // En revanche DefaultValue est stocke sous forme de chaine donc c'est galere de convertir en valeur C#
                    object defaultValue = col.DefaultValue();
                    if (defaultValue is string) // ne signifie pas que col.CsType est de type string (exemple: ça peut etre "((0.00))" pour initialiser une valeur de type "decimal?")
                        while (((string)defaultValue).StartsWith("(") && getMatchingParenthesisIndex((string)defaultValue, 0) == ((string)defaultValue).Length - 1) // gere le cas "((0))"
                            defaultValue = ((string)defaultValue).Substring(1, ((string)defaultValue).Length - 2);
                    if (col.CsTypeNotNullable() == typeof(bool))
                        defaultValue = col.DefaultValue().Contains("1") ? "true" : "false";
                    if (col.CsType() == typeof(string))
                    {
                        if (((string)defaultValue).StartsWith("'") && ((string)defaultValue).EndsWith("'"))
                            defaultValue = "\"" + ((string)defaultValue).Substring(1, ((string)defaultValue).Length - 2).Replace(@"\", @"\\").Replace("\"", "\\\"") + "\"";
                    }
                    if (defaultValue is string && ((string)defaultValue) == "NULL")
                        defaultValue = "null";
                    if (defaultValue is string && col.CsType() == typeof(long?) && ((string)defaultValue).In("''", "null''")) // Et oui c'est possible une colonne de type bigint nullable peut avoir une valeur ('') definit par defaut !
                        defaultValue = "null";
                    if (col.CsTypeNotNullable() == typeof(decimal) && char.IsDigit(defaultValue.ToString().Last()))
                        defaultValue = (string)defaultValue + "m";
                    if (defaultValue is string && (((string)defaultValue).ToLower() == "getdate()" || ((string)defaultValue).ToLower() == "sysdatetime()"))
                    {
                        //DebugTools.Break("Cas à gérer... ca signifie qu'il faut recharger les objets après les avoir enregistré en base." +
                        //                 "Ca peut être complexe a gerer. Il est plus simple de faire en sorte que les objets créé initialisent eux-même les valeurs");
                        defaultValue = "DateTime.UtcNow";
                        continue;
                    }
                    code += "            " + colData.FieldName + " = " + defaultValue + ";" + nl;
                }
                else if (!col.Nullable && col.CsType() == typeof(string))
                    code += "            " + colData.FieldName + " = string.Empty;" + nl; // le setter interdira de mettre null par la suite
            }
            code += nl;
            code += "            // ReSharper disable ConditionIsAlwaysTrueOrFalse;" + nl;
            code += "            var withSpecificInitialization = (object)this as ISpecificInitialization;" + nl;
            code += "            if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)" + nl;
            code += "               withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();" + nl;
            code += "            // ReSharper restore ConditionIsAlwaysTrueOrFalse;" + nl;
            code += "        }" + nl;

            #endregion

            #region Chargement

            code += nl;
            code += nl;
            code += "        public static void " + FillFromReaderMethodName + "(System.Data.Common.DbDataReader reader, List<" + tData.ClassName + "> items)" + nl;
            code += "        {" + nl;
            code += "            #if DEBUG" + nl;
            code += "            int before = items.Count;" + nl;
            code += "            var sw = new System.Diagnostics.Stopwatch();" + nl;
            code += "            sw.Start();" + nl;
            code += "            #endif" + nl;

            code += "            while (reader.Read())" + nl;
            code += "            {" + nl;
            code += "                var item = new " + tData.ClassName + "();" + nl;
            foreach (Column col in table.Columns)
            {
                var tCol = DataFor(col);
                code += "                item." + tCol.FieldName + " = ";
                if (col.Nullable)
                    code += "reader.IsDBNull(" + (col.OrdinalPosition() - 1) + ") ? null : ";
                if (ForSqlCE && (col.CsType() == typeof(TimeSpan) || col.CsType() == typeof(TimeSpan?)))
                    code += (col.Nullable ? "(" + TypeToString(col.CsType()) + ")" : "")
                         + "new TimeSpan((long)reader.GetValue(" + (col.OrdinalPosition() - 1) + "));" + nl;
                else
                    code += "(" + TypeToString(col.CsType()) + ")" + "reader.GetValue(" + (col.OrdinalPosition() - 1) + ");";
                code += " // " + col.Name + nl;
            }
            code += "                item.InitializeState(eState.Synchronized);" + nl;
            code += "                items.Add(item);" + nl;
            code += "                if (IsPrimaryObject && item._OldClosedId.HasValue) // In case _Id_Personne_Presences has been set by using Property" + nl;
            code += "                {" + nl;
            code += "                    GeneratedDAO.Instance.CancelCreationOf((ulong)(long)item._OldClosedId.Value.Keys[0]);" + nl; // TODO : optim / recherche : Faire un genre de traits en utilisant une classe static generique ?
            code += "                    item.RaiseIdChanged(); " + nl;
            code += "                }" + nl;
            code += "            }" + nl;

            code += "            #if DEBUG" + nl;
            code += "            if (items.Count - before > 0)" + nl;
            code += "            {" + nl;
            code += "                System.Diagnostics.Debug.WriteLine(\"Chargement des données : \" + sw.Elapsed.ToHumanReadableShortNotation());" + nl;
            code += "                System.Diagnostics.Debug.WriteLine(\"  soit, par propriété (" + table.Columns.Count + "): \" + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * " + table.Columns.Count + ")).ToString());" + nl;
            code += "            }" + nl;
            code += "            #endif" + nl;
            code += "        }" + nl;
            #endregion

            #region Cloning
            code += nl;
            code += nl;
            code += "        #region Cloneable" + nl;
            code += nl;
            code += "        public " + tData.ClassName + " Clone() { return (" + tData.ClassName + ")(this as ICloneable).Clone(); }" + nl;
            code += "        protected override GenericGeneratedDALEntity<" + tData.IdType + "> CreateNewInstance() { return new " + tData.ClassName + "(); }" + nl;
            code += "        public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<" + tData.IdType + "> source)" + nl;
            code += "        {" + nl;
            code += "            var from = (" + tData.ClassName + ")source;" + nl;
            code += "            // Note : Les champs appartenant à la PK ne sont pas copiés;" + nl;
            foreach (Column col in table.Columns)
                if (!col.InPrimaryKey)
                {
                    var tCol = DataFor(col);
                    code += "            " + tCol.FieldName + " = from." + tCol.FieldName + ";" + nl;
                }
            code += "        }" + nl;
            code += nl;
            code += "        #endregion" + nl;

            #endregion

            #region Memento

            code += nl;
            code += nl;
            code += "        #region Memento" + nl;
            code += nl;
            code += "        public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf." + table.OwnerSchema().Name.Capitalize() + ".ReadOnly" + tData.ClassName + nl;
            code += "        {" + nl;
            code += "        }" + nl;

            code += "        /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento" + nl; // TODO : faire hériter de Memento pour Entity<TId>
            code += "        {" + nl;

            propElts.Clear();

            //if (table.Pk != null)
            //{
            //    propElts.Add(new[]
            //        {
            //            "            ",
            //            "public",
            //            tData.IdType + "?",
            //            "ClosedId",
            //            "{ get; set; }",
            //        });
            //}

            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);

                propElts.Add(new[]
                    {
                        "            ",
                        "public",
                        TypeToString(col.CsType()),
                        colData.PropName,
                        "{ get; set; }",
                    });
            }
            code += propElts.AlignAndMerge(new[] { none, right, left, right, none }, null).ToString() + nl;
            code += "        }" + nl;


            code += "        public override TechnicalTools.Model.IMemento CreateMemento()" + nl;
            code += "        {" + nl;
            code += "            var mem = new Memento();" + nl;
            code += "            FillMemento(mem);" + nl;
            code += "            return mem;" + nl;
            code += "        }" + nl;
            code += "        /*protected*/ void FillMemento(Memento mem)" + nl;
            code += "        {" + nl;
            code += "            base.FillMemento(mem);" + nl;
            code += nl;
            //code += "            mem.ClosedId = ClosedId;" + nl;
            foreach (Column col in table.Columns)
                if (!col.InPrimaryKey)
                {
                    var tCol = DataFor(col);
                    code += "            mem." + tCol.PropName + " = " + tCol.FieldName + ";" + nl;
                }
            //code += "            return mem;" + nl;
            code += "        }" + nl;
            code += "        protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)" + nl;
            code += "        {" + nl;
            code += "            var mem = (Memento)mem_;" + nl;
            code += "            base.RestoreMemento(mem);" + nl;
            code += nl;
            //code += "            mem.ClosedId = ClosedId;" + nl;
            foreach (Column col in table.Columns)
                if (!col.InPrimaryKey)
                {
                    var tCol = DataFor(col);
                    code += "            "  + tCol.FieldName + " = mem." + tCol.PropName + ";" + nl;
                }
            code += "        }" + nl;
            code += nl;
            code += "        #endregion" + nl;

            #endregion

            #region Value Range

            code += nl;
            code += nl;
            code += "        #region Value range" + nl;
            code += nl;
            code += "        static class MinValueOf" + nl;
            code += "        {" + nl;
            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);
                if (col.CsTypeNotNullable() == typeof(DateTime))
                    code += "            public static readonly DateTime " + colData.PropName + " = new DateTime(" + ((DateTime)col.MinValue()).Ticks + "); // " + ((DateTime)col.MinValue()).ToString("o") + nl;
            }
            code += "        }" + nl;
            code += nl;
            code += "        static class MaxValueOf" + nl;
            code += "        {" + nl;
            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);
                if (col.CsTypeNotNullable() == typeof(DateTime))
                    code += "            public static readonly DateTime " + colData.PropName + " = new DateTime(" + ((DateTime)col.MaxValue()).Ticks + "); // " + ((DateTime)col.MaxValue()).ToString("o") + nl;
            }
            code += "        }" + nl;
            code += nl;
            code += "        #endregion" + nl;

            #endregion

            code += "        internal static class Metadata" + nl;
            code += "        {" + nl;
            code += nl;
            code += "        }" + nl;

            code += @"    }" + nl; // class

            code += @"}" + nl; // Schema namespace  (dbo, ...)

            #region Properties interface

            code += "namespace " + "HasPropertiesOf" + nl;
            code += "{" + nl;
            code += "    namespace " + table.OwnerSchema().Name.Capitalize() + nl;
            code += "    {" + nl;
            code += "        public partial interface ReadOnly" + tData.ClassName + nl;
            code += "        {" + nl;

            propElts.Clear();
            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);

                string maxLength = col.HasLengthLimit() ? ", DbMaxLength(" + col.ArrayLength() + ")" : "";
                propElts.Add(new[]
                    {
                        "            ",
                        TypeToString(col.CsType()),
                        colData.PropName,
                        "{ get; }",
                    });
            }
            code += propElts.AlignAndMerge(new[] { none, left, right, none }, null).ToString() + nl;
            code += nl;
            code += nl;
            code += "        }" + nl;

            code += "        public partial interface " + tData.ClassName + " : ReadOnly" + tData.ClassName + nl;
            code += "        {" + nl;

            propElts.Clear();
            if (table.PkColumns().Any())
            {
                propElts.Add(new[]
                    {
                        "            ",
                        "",
                        tData.IdType + "?",
                        "ClosedId",
                        "{ get; }",
                    });
            }

            foreach (Column col in table.Columns)
            {
                var colData = DataFor(col);

                string maxLength = col.HasLengthLimit() ? ", DbMaxLength(" + col.ArrayLength() + ")" : "";
                propElts.Add(new[]
                    {
                        "            ",
                        "new",
                        TypeToString(col.CsType()),
                        colData.PropName,
                        "{ get; set; }",
                    });
            }
            code += propElts.AlignAndMerge(new[] { none, right, left, right, none }, null).ToString() + nl;
            code += nl;
            code += nl;
            code += "        }" + nl;

            code += "    }" + nl; // namespace schema
            code += "}"; // HasPropertyOf

            #endregion

            

            sb.Append("    " + code.Replace(Environment.NewLine, Environment.NewLine + "    "));
        }
        public string GetIdsWhereMethodName { get { return GenericGeneratedDAO.GetIdsWhereMethodName; } }
        public string FillFromReaderMethodName { get { return GenericGeneratedDAO.FillFromReaderMethodName; } }

        void GenerateEntityBaseClass(StringBuilder sb)
        {
            Func <string, string> IfWithIdManager = str => WithIdManager ? str : "";
            sb.AppendLine((@"
    public interface I" + GeneratedEntityBaseClassName + @" : IGeneratedGenericDALEntity, System.ICloneable
    {
        void Delete();
        void SynchronizeToDatabase();
    }

    public abstract partial class " + GeneratedEntityBaseClassName + @"<TId> : GenericGeneratedDALEntity<TId>, I" + GeneratedEntityBaseClassName + @"
        where TId : struct, IEquatable<TId>, IIdTuple
    {
        internal static readonly bool IsPrimaryObject = typeof(TId) == typeof(IdTuple<long>);

        protected " + GeneratedEntityBaseClassName + @"(TId? id = null)
        {
        " + IfWithIdManager(
      @"    if (IsPrimaryObject)
                if (id.HasValue)
                    ClosedId = id;
                else
                    ClosedId = (TId)(object)new IdTuple<long>((long)GeneratedDAO.Instance.CreateUniqueId());
        ") + 
      @"}


        protected static IEnumerable<T> GetRowsAndConvert<T>(string sqlQueryToGetRows, Func<System.Data.DataRow, T> convert)
        {
            var dt = GeneratedDAO.Instance.WithConnection(con => con.GetDatatable(sqlQueryToGetRows));
            return dt.AsEnumerable().Select(convert);
        }

        public override void LoadById(TId id)
        {
            " + IfWithIdManager(
           @"TId? oldId = IsPrimaryObject && State == eState.New && !ClosedId.Value.Equals(id) ? ClosedId : null;
            ") +
            @"#pragma warning disable 184
            // ReSharper disable HeuristicUnreachableCode                                                                                                                             
            GeneratedDAO.Instance.Mapper.AutoLoadItem(this, id);
            // ReSharper restore HeuristicUnreachableCode                                                                                                                             
            #pragma warning restore 184
            " + IfWithIdManager(
          @"
            if (oldId.HasValue)
                GeneratedDAO.Instance.CancelCreationOf((ulong)(object)((IdTuple<long>)(object)oldId.Value).Id1);
           ") +
            @"
        }
        public override void SynchronizeToDatabase()
        {
            GeneratedDAO.Instance.Mapper.SynchronizeToDatabase(this);
        }
        public void Delete()
        {
            //if (Debugger.IsAttached)
            //    Debugger.Break();
            var tran = GeneratedDAO.Instance.CurrentDBConnection == null ? null
                     : GeneratedDAO.Instance.CurrentDBConnection.CurrentActiveTransaction;
            if (tran != null)
            {
                var restoreState = CreateActionToRestoreState();
                tran.OnRollback += restoreState; // Pas besoin de gérer la désinscription car la référence à la transaction disparaitra toute seule                                   
            }
            UpdateState(eState.ToDelete);
            SynchronizeToDatabase();
        }

    }
").Replace("{DQ}", "\""));
        }
        public const string GeneratedEntityBaseClassName = "GeneratedDALEntity";

        void GenerateDAO(StringBuilder sb)
        {
            string GenericDBConnection = ForSqlCE ? typeof(GenericDBConnectionSqlCE).Name : typeof(GenericDBConnection).Name;
            sb.AppendLine((@"
    public partial class " + GeneratedDAOClassName  + @" : GenericGeneratedDAO
    {
        public static GeneratedDAO Instance { get; private set; }

        readonly GenericDALIdManager _IdManager;

        protected GeneratedDAO(string stringConnection)
            : this(() => new " + GenericDBConnection + @"(stringConnection))
        {
        }
        protected GeneratedDAO(Func<" + GenericDBConnection  + @"> createDBConnection)
            : base(createDBConnection)
        {
            if (Instance != null)
                throw new TechnicalException(GetType().Name + {DQ} is supposed to be a singleton! But some code try to instanciate a new one.{DQ}, null);
            Instance = this;
            _IdManager = new GenericDALIdManager(this, {DQ}_IdManager{DQ}, {DQ}SeedOfIds{DQ});
        }

        internal ulong CreateUniqueId() { return _IdManager.CreateUniqueId(); }

        internal void CancelCreationOf(ulong id)
        {
            //ds.UpdateState(eState.Deleted);
            //_IdManager.TryRecycleId((ulong)(long)((IHasClosedId)ds).Id);
            _IdManager.TryRecycleId(id);
        }

        public static void Connect(string connectionString)
        {
            Connect(() => new " + GenericDBConnection + @"(connectionString));
        }
        public static void Connect(Func<" + GenericDBConnection  + @"> createConnection)
        {
            using (var con = createConnection())
                con.ExecuteScalar({DQ}select 1{DQ});
            Instance = new GeneratedDAO(createConnection);
        }

        public override Dictionary<Type, bool> AllTypes { get {
            if (_allTypes != null)
                return _allTypes;
            _allTypes = new Dictionary<Type, bool>();" +
            Tables.Where(t => !SeemsToBeEnum(t)).Select(table => nl + "            _allTypes.Add(typeof(" + table.OwnerSchema().Name.Capitalize() + "." + DataFor(table).ClassName + "), " + table.OwnerSchema().Name.Capitalize() + "." + DataFor(table).ClassName + ".IsPrimaryObject);").Join("") + @"
            return _allTypes;
        } }
        Dictionary<Type, bool> _allTypes;

    }").Replace("{DQ}", "\""));
        }
        public const string GeneratedDAOClassName = "GeneratedDAO";

    }

   
}
