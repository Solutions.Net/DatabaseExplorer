﻿using System;
using System.Diagnostics;
using System.Reflection;

using DataMapper;


namespace DatabaseExplorer.Business
{

    public class DynamicTypeAssemblyGeneratorNew : DynamicTypeAssemblyGeneratorBase
    {
        public DynamicTypeAssemblyGeneratorNew(string assemblyName, string sourceCode)
            : base(assemblyName, sourceCode)
        {
        }

        protected override string GetDaoDefinition()
        {
            return null;
        }

        protected override Func<string, object> GetDaoConstructor(Assembly assembly)
        {
            var daoType = assembly.GetType(AssemblyName + "." + DynamicTypeCodeGeneratorNew.GeneratedDAOClassName);
            Debug.Assert(daoType != null);
            var daoCons = daoType.GetConstructor(new[] { typeof(IDbMapper) });
            TechnicalTools.Diagnostics.DebugTools.Assert(daoCons != null);
            return connectionString => daoCons.Invoke(new object[] { DbMapperFactory.CreateSqlMapper(null, connectionString) });
        }
    }
}
