﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.SqlServer.Management.Smo;

using SubSonic.Extensions;

using TechnicalTools;
using TechnicalTools.Model.Cache;

using DatabaseExplorer.Business.BaseDALClass;
using System.Data.SqlTypes;
using DatabaseExplorer.Business.Extensions;
using DataMapper;

namespace DatabaseExplorer.Business
{

    public abstract class DynamicTypeCodeGeneratorBase
    {
        public bool   OneFilePerClass { get; set; }

        public string                     AssemblyName     { get; private set; }
        public IReadOnlyCollection<Table> Tables           { get; }
        public string                     ConnectionString { get; private set; }

        protected DynamicTypeCodeGeneratorBase(string assName, IReadOnlyCollection<Table> tables, string connectionString)
        {
            AssemblyName = assName;
            Tables = tables;
            ConnectionString = connectionString;
        }
        void CleanTempData()
        {
            _tableDatas.Clear();
            _columnDatas.Clear();
        }

        public virtual Dictionary<string, string> GenerateSourceCode()
        {
            CleanTempData();

            var results = new Dictionary<string, string>();
            if (OneFilePerClass)
            {
                foreach (var table in Tables.OrderBy(t => t.Name))
                {
                    var sb = new StringBuilder();
                    GenerateNamespaceWrappingCode(sb, () => GenerateDalClass(sb, table));
                    results.Add(DataFor(table).FileName + ".cs", sb.ToString());
                }
            }
            else
            {
                var sb = new StringBuilder();
                GenerateNamespaceWrappingCode(sb, () => 
                    {
                        foreach (var table in Tables.OrderBy(t => t.Name))
                        {
                            GenerateDalClass(sb, table);
                            sb.AppendLine();
                            sb.AppendLine();
                        }
                    });
                results.Add("DAL.cs", sb.ToString());
            }
            return results;
        }

        protected void GenerateNamespaceWrappingCode(StringBuilder sb, Action classCodeCreator, bool withUsings = true)
        {
            if (withUsings)
            {
                sb.AppendLine(UsingsNamespacesFormatted);
                sb.AppendLine();
                sb.AppendLine();
            }
            sb.AppendLine("namespace " + AssemblyName);
            sb.AppendLine("{");
            classCodeCreator(); sb.AppendLine();
            sb.AppendLine("}");
        }

        public virtual IEnumerable<Type> GetUsingsNamespaces()
        {
            return new[]
                {
// ReSharper disable RedundantNameQualifier
                    typeof (System.Int32),
                    typeof (System.Collections.Generic.List<int>),
                    typeof (System.Data.DataRow),
                    typeof (System.IO.Path),
                    typeof (System.Linq.Enumerable),
                    typeof (SqlXml),
                    typeof (System.Xml.XmlTextReader), // For handling SqlXml type (ex: AdventureWorks)
                    typeof (System.Reflection.PropertyInfo),
                    typeof (System.Text.ASCIIEncoding),

                    typeof (TechnicalTools.Enum_Extensions),
                    typeof (TechnicalTools.Model.PropertyNotifierObject),
                    typeof (TechnicalTools.Model.Cache.CacheManager),
                    typeof (DataMapper.IDbMapper),

                    typeof (System.Runtime.CompilerServices.MethodImplAttribute),
                    typeof (System.ComponentModel.INotifyPropertyChanged),
                    typeof (System.Data.DataTableExtensions),
                    typeof (System.Diagnostics.Debugger),
                    typeof (System.ComponentModel.DataAnnotations.MetadataTypeAttribute),
// ReSharper restore RedundantNameQualifier
                }.ToList();
        }
        public virtual string UsingsNamespacesFormatted
        {
            get
            {
                return
                    GetUsingsNamespaces().OrderBy(t => !t.Namespace.StartsWith("System"))
                                         .ThenBy(t => t.Namespace)
                                         .GroupBy(t => t.Namespace.IndexOf('.') == -1 ? t.Namespace : t.Namespace.Remove(t.Namespace.IndexOf('.')))
                                         .Select(grp => grp.Select(t => "using " + t.Namespace + ";").Distinct().Join(Environment.NewLine))
                                         .Join(Environment.NewLine + Environment.NewLine);
            }
        }

        public abstract void GenerateDalClass(StringBuilder sb, Table table);

        protected static readonly string nl = Environment.NewLine;

        protected class TableData
        {
            public readonly Table Table;
            public readonly Table BaseTable;
            public readonly ConstraintFK BaseTableFk;

            public readonly string ClassName;
            public readonly string FullClassName;
            public readonly string FileName;
            public readonly string ClassNamePlural;
            public readonly string PkColumnsSqlList;
            public readonly Func<string,string> CreateIdFromDataRow;
            public readonly string IdType;

            // Etant donne une colonne "c" appartenant à "Table", renvoie le nom de la propriete généré de type Ienumerable<tuple<...>) et qui renvoie les ids des objets dont this detient des reference
            public readonly Dictionary<ConstraintFK, string> NavPropertiesVia = new Dictionary<ConstraintFK, string>();
            // Idem mais à l'envers : Renvoie le nom de la prorpiété qui renvoie les id des objet referencant this
            public readonly Dictionary<ConstraintFK, string> NavPropertiesFrom = new Dictionary<ConstraintFK, string>(); 

            public TableData(Table t, Func<Column, ColumnData> dataFor, Func<Table, TableData> dataForTable)
            {
                Table = t;
                ClassName = t.Name.Replace("$", "").Replace("-", "_").Replace(" ", "_"); ;
                ClassName = ClassName.Capitalize();
                ClassName = ClassName.MakeSingular();
                FullClassName = t.OwnerSchema().Name.Capitalize() + "." + ClassName;
                FileName = ClassName.Replace("<", "Of").Replace(">", "").Replace(",", "_").Replace(" ", "");
                IdType = !t.PkColumns().Any() 
                       ? typeof(IdAsRef<string>) + "<" + dataForTable(t).ClassName + ">"
                       : nameof(IdTuple<int>) + "<" + t.PkColumns().Select(col => TypeToString(col.CsType())).Join() + ">";

                //if (IdType == nameof(IdTuple<int>) + "<" + nameof(SqlHierarchyId) + ">") // cas particulier car SqlHierarchyId n'est pas Equatable
                //    IdType = nameof(IdTupleSqlHierarchyId);
                //IdType = IdType.Replace(nameof(IdTuple<int>) + "<" + nameof(SqlHierarchyId) + ", ", nameof(IdTupleSqlHierarchyId) + "<"); // Idem

                ClassNamePlural = ClassName.MakePlural();

                if (t.PkColumns().Any())
                    PkColumnsSqlList = t.PkColumns().Select(col => "[" + col.Name + "]").Join(); // protection car certain nom de colonne sont des mots clefs (exemple "index")

                Func<Column, string, string> colToCsValue = (col, rowVar) => dataFor(col).ConvertSqlValueToCsValue(rowVar + "[\"" + col.Name + "\"]");
                if (t.PkColumns().Any())
                    CreateIdFromDataRow = rowVar => "new " + IdType + "(" + t.PkColumns().Select(col => colToCsValue(col, rowVar)).Join() + ")";


                if (t.PkColumns().Any())
                {
                    BaseTableFk = t.FkConstraints()
                                   .SingleOrDefault(fk => fk.ForeignColumns.OrderBy(col => col.Name)
                                           .SequenceEqual(t.PkColumns().OrderBy(col => col.Name)));
                    if (BaseTableFk != null)
                        BaseTable = BaseTableFk.ForeignColumns[0].OwnerTable();
                }
            }
        }

        protected class ColumnData
        {
            public string PropName;
            public string AsArgName;
            public string FieldName;
            public Func<string, string> ConvertSqlValueToCsValue;
        }

        readonly Dictionary<Table, TableData>   _tableDatas = new Dictionary<Table, TableData>();
        readonly Dictionary<Column, ColumnData> _columnDatas = new Dictionary<Column, ColumnData>();


        protected TableData DataFor(Table t)
        {
            TableData data;
            if (!_tableDatas.TryGetValue(t, out data))
            {
                data = new TableData(t, (Column c) => DataFor(c), (Table t2) => DataFor(t2));
               
                _tableDatas.Add(t, data);
            }
            return data;
        }

        protected ColumnData DataFor(Column c)
        {
            ColumnData data;
            if (!_columnDatas.TryGetValue(c, out data))
            {
                data = new ColumnData();

                // Y'a vraiment des caracteres à la con dans certaines tables ! 
                // TODO : Voir https://msdn.microsoft.com/en-us/library/aa664670.aspx pour autoriser tous les caractere possible en C#
                // Voir aussi ce lien pour savoir comment faire une regex (chercher "Catégorie Unicode négative ou bloc Unicode : \P{}"  ) :
                // https://msdn.microsoft.com/fr-fr/library/20bw873z(v=vs.110).aspx
                data.PropName = Regex.Replace(c.Name, "[][ '()?,&/.]|-", "_"); 
                data.PropName = data.PropName.Capitalize(); // Permet aussi de gérer le cas ou PropName est un mot clef du C#, permet egalement d'utiliser ce nom comme des arugment quand on met tout en minuscule, evite ainsi d'avoir d'ambuiguite entre this.PropertyName et PropertyName (l'argument)
                if (data.PropName == DataFor(c.OwnerTable()).ClassName)  // Un membre de classe ne peut avoir le même nom que la classe
                    data.PropName = "Name";
                if (data.PropName == "IdType")  // "IdType" est utilisé pour generer une propriete static (TODO : la renommer en TypeOfId ==> bcp moins suceptible d'être utilisé comme nom de colonne sur une base de données)
                    data.PropName = "Id_Type";
                data.AsArgName = data.PropName.ToLower() + "_"; // ajoute "_" pour eviter les mot clef du C#
                data.FieldName = "_" + data.PropName;
                data.ConvertSqlValueToCsValue = varName => nameof(DynamicTypeCodeGeneratorBase) + "." + nameof(GetValueAs) + "<" + TypeToString(c.CsType()) + ">(" + varName + ")";

                _columnDatas.Add(c, data);
            }
            return data;
        }
        
        [Obsolete("Only for generated code!")]
        public static T GetValueAs<T>(object value)
        {
            if (value is DBNull)
            {
                if (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>))
                    return (T)(object)null;
                if (!typeof(T).IsValueType)
                    return (T)(object)null;
            }
            return (T)value;
        }

        protected static string TypeToString(Type type)
        {
            return Column_Extensions.TypeToString(type);
        }
    }

   
}
