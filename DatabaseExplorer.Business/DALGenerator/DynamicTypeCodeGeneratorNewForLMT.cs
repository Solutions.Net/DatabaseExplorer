﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;

using DataMapper;

using DatabaseExplorer.DAL;


namespace DatabaseExplorer.Business
{

    public class DynamicTypeCodeGeneratorNewForLMT : DynamicTypeCodeGeneratorNew
    {
        public DynamicTypeCodeGeneratorNewForLMT(string assName, IReadOnlyCollection<Table> tables, string connectionString)
            : base(assName, tables, connectionString)
        {
            IncludeBaseClassAndDAOInGeneratedCode = false;
        }

        public override IEnumerable<Type> GetUsingsNamespaces()
        {
            return base.GetUsingsNamespaces()
                .Except(typeof(BaseDTO)) // or BaseDTO<???>
                .ToList();
        }
    }
}
