﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools.Model.Cache;

using DatabaseExplorer.Business.BaseDALClass;
using DataMapper;

namespace DatabaseExplorer.Business
{

    public class DynamicTypeCodeGeneratorForDemo : DynamicTypeCodeGenerator
    {
        public DynamicTypeCodeGeneratorForDemo(string assName, IReadOnlyCollection<Table> tables, string connectionString, Type entitySpecificBaseClass = null)
            : base(assName, tables, connectionString, entitySpecificBaseClass)
        {
        }

        public override IEnumerable<Type> GetUsingsNamespaces()
        {
            return base.GetUsingsNamespaces().Concat(
                new[]
                    {
// ReSharper disable RedundantNameQualifier
                        //typeof (System.Windows.Forms.Form),
                        //typeof (Microsoft.SqlServer.Types.SqlHierarchyId), // pour certaines base de données comme AdventureWorks
                        typeof (BaseDTO<int>),
// ReSharper restore RedundantNameQualifier
                    }).ToList();
        }
    }
}
