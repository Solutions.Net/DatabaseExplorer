﻿using System;
using System.Diagnostics;
using System.Reflection;

using DatabaseExplorer.Business.BaseDALClass;


namespace DatabaseExplorer.Business
{

    public class DynamicTypeAssemblyGeneratorOld : DynamicTypeAssemblyGeneratorBase
    {
        public DynamicTypeAssemblyGeneratorOld(string assemblyName, string sourceCode)
            : base(assemblyName, sourceCode)
        {
        }

        protected override string GetDaoDefinition()
        {
            return $@"
namespace {AssemblyName}
{{                                                                                                   
    public sealed class DAO : {DynamicTypeCodeGenerator.GeneratedDAOClassName}
    {{
        public static new DAO Instance {{ get {{ return (DAO)GeneratedDAO.Instance; }} }}

        public static void CreateSingleton(string connectionString)
        {{
            var instance = new DAO(connectionString);
            if (Instance != instance)
                throw new TechnicalException(" + "\"Unexpected behavior !\"" + $@", null);
        }}

        private DAO(string connectionString)
            : base(connectionString)
        {{
        }}
    }}
}}";
        }

        protected override Func<string, object> GetDaoConstructor(Assembly assembly)
        {
            var daoType = assembly.GetType(AssemblyName + ".DAO");
            Debug.Assert(daoType != null);
            var daoCreateSingletonMethod = daoType.GetMethod("CreateSingleton", BindingFlags.Static | BindingFlags.Public);
            Debug.Assert(daoCreateSingletonMethod != null);
            return connectionString =>
            {
                daoCreateSingletonMethod.Invoke(null, new object[] { connectionString });
                var daoInstanceField = daoType.GetProperty("Instance", BindingFlags.Static | BindingFlags.Public);
                return (GenericGeneratedDAO)daoInstanceField.GetValue(null);
            };
        }
    }
}
