﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;

using DataMapper;

using DatabaseExplorer.DAL;
using DatabaseExplorer.Business.BaseDALClass;
using DatabaseExplorer.Business.Extensions;

namespace DatabaseExplorer.Business.Model.Local
{
    public partial class Project
    {
        public const string IdManagerTableName = "_IdManager";
        public const string IdManagerSeedOfIdColumnName = "SeedOfIds";

        public string GenerateSqlCodeCreation()
        {
            const String_Extensions.ePadding right = String_Extensions.ePadding.Right;
            //const String_Extensions.ePadding left = String_Extensions.ePadding.Left;
            const String_Extensions.ePadding none = String_Extensions.ePadding.None;
            // https://technet.microsoft.com/en-us/library/ms174642(v=sql.110).aspx
            // https://msdn.microsoft.com/en-us/library/ms172424(SQL.110).aspx
            //https://technet.microsoft.com/fr-fr/library/bb896140(v=sql.110).aspx
            // http://erikej.blogspot.fr/2011/01/comparison-of-sql-server-compact-4-and.html
            // http://erikej.blogspot.fr/2016/01/sql-server-compact-sqlite-toolbox-44-45.html
            var constraintsBack = new Dictionary<Table, string>();
            var sb = new StringBuilder();
            if (ForSqlCE && DbInfos.Schemas.Count > 1)
            {
                string report = DbInfos.Tables.FindDuplicatesAndMakeReport(t => t.Name, t => t.FullName());
                if (!string.IsNullOrEmpty(report))
                    throw new Exception("Sql CE does not support schema, So all tables must be unique !" + Environment.NewLine + report);
            }
            var foreigKeyDeclarationForSqlCE = new List<string>();
            foreach (var schema in DbInfos.Schemas)
            {
                string schemaName = ForSqlCE ? "" : "[" + schema.Name + "].";
                sb.AppendLine("CREATE TABLE " + schemaName + "[" + IdManagerTableName + "] (");
                sb.AppendLine("    [" + IdManagerSeedOfIdColumnName + "] bigint  NOT NULL");
                sb.AppendLine(");");
                sb.AppendLine("GO");

                foreach (var table in schema.Tables())
                {
                    sb.AppendLine("CREATE TABLE " + schemaName + "[" + table.Name + "] (");
                    var content = new List<Tuple<String_Extensions.ePadding, string>[]>();
                    var line = new List<Tuple<String_Extensions.ePadding, string>[]>();
                    foreigKeyDeclarationForSqlCE.Clear();
                    foreach (Column col in table.Columns)
                    {
                        string fkname = col.IsForeignKey ? "FK" + table.Name + "__" + col.ReferencedColumn().Owner.Name + "__" + col.Name : "";
                        string foreigKeyDeclaration = col.IsFK() ? " CONSTRAINT [" + fkname + "] FOREIGN KEY ([" + col.Name + "]) REFERENCES " + schemaName + "[" + col.ReferencedColumn.Owner.Name + "]([" + col.ReferencedColumn.Name + "]) ON DELETE NO ACTION ON UPDATE NO ACTION" : "";
                        line.Add(new[]
                            {
                                Tuple.Create(none, "\t"),
                                Tuple.Create(right, "[" + col.Name + "]"),
                                Tuple.Create(right, ForSqlCE && col.SqlType == SqlType.Time ? "bigint"
                                                  : ForSqlCE && col.SqlType == SqlType.VarBinary ? "image"
                                                  : ForSqlCE && col.SqlType == SqlType.String ? "ntext"
                                                  : ForSqlCE && col.SqlType == SqlType.DateTime2 ? "datetime"
                                                  : col.SqlTypeAsString),
                                Tuple.Create(right, col.IsNullable ? "" : "NOT"),
                                Tuple.Create(right, "NULL"),
                                Tuple.Create(right, col.IsInPk && col.PkDefinition.Columns.Count == 1 ? "PRIMARY KEY" : ""),
                                Tuple.Create(right, !col.IsFK || ForSqlCE ? "" : col.FkDefinition.LinkType == eLinkType.BackwardLink ? "/* foreign key to " + col.ReferencedColumn.Owner.Name + " declared after it */" : foreigKeyDeclaration)
                            });
                        if (col.IsFK && (col.FkDefinition.LinkType == eLinkType.BackwardLink || ForSqlCE))
                        {
                            if (!constraintsBack.ContainsKey(col.ReferencedColumn.Owner) && !ForSqlCE)
                                constraintsBack[col.ReferencedColumn.Owner] = "";
                            string declaration = "ALTER TABLE " + schemaName + "[" + table.Name + "] "
                                               + "ADD " + foreigKeyDeclaration
                                               + Environment.NewLine;
                            if (ForSqlCE)
                                foreigKeyDeclarationForSqlCE.Add(declaration);
                            else
                                constraintsBack[col.ReferencedColumn.Owner] += declaration;
                        }
                    }
                    sb.AppendLine(Regex.Replace(line.AlignAndMerge(), " *" + Environment.NewLine, "," + Environment.NewLine));
                    if (table.Pk != null && table.Pk.Columns.Count > 1)
                    {
                        sb.Append("CONSTRAINT PK_" + table.Name + " PRIMARY KEY (");
                        sb.Append(table.Pk.Columns.Select(c => "[" + c.Name + "]").Join());
                        sb.AppendLine(")");
                    }
                    sb.AppendLine(");");
                    sb.AppendLine("GO");

                    foreach (var declaration in foreigKeyDeclarationForSqlCE)
                    {
                        sb.AppendLine(declaration);
                        sb.AppendLine("GO");
                    }

                        string technicalConstraints;
                    if (constraintsBack.TryGetValue(table, out technicalConstraints))
                    {
                        sb.AppendLine(technicalConstraints);
                        sb.AppendLine("GO");
                    }
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }
      
        public string GenerateSqlCodeDeletion()
        {
            var sb = new StringBuilder();
            
            foreach(var schema in DbInfos.Schemas)
            {
                string schemaName = ForSqlCE ? "" : "[" + schema.Name + "].";
                sb.AppendLine("DROP TABLE " + schemaName + "[" + IdManagerTableName + "];");
                sb.AppendLine("GO");
                foreach (var table in schema.Tables.Reverse<Table>())
                {
                    sb.AppendLine("DROP TABLE " + schemaName + "[" + table.Name + "];");
                    sb.AppendLine("GO");
                }
            }
            return sb.ToString();
        }
        
        public virtual string GenerateSqlDataInitialization()
        {
            var sb = new StringBuilder();
            foreach (var schema in DbInfos.Schemas)
            {
                string schemaName = ForSqlCE ? "" : "[" + schema.Name + "].";
                sb.AppendLine("INSERT " + schemaName + "[" + IdManagerTableName + "] " +
                              "(" + IdManagerSeedOfIdColumnName + ") " + 
                              "VALUES (1);");
                sb.AppendLine("GO");
            }
            return sb.ToString();
        }
    }
}