﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using Microsoft.SqlServer.TransactSql.ScriptDom;

using Microsoft.SqlServer.Management.Smo;
using TechnicalTools;

namespace DatabaseExplorer.Business.Model.Local
{
    public partial class Project
    {
        public void TrySomething()
        {
            var res = ParseSql("Select count(*) from [foo].[bar]");
            var visitor = new CustomVisitor();
            res.Item1.Accept(visitor);
            visitor.Visit(res.Item1);
        }
        private static Tuple<TSqlFragment, IList<ParseError>> ParseSql(string procText)
        {
            // D'apres https://github.com/Microsoft/sqltoolsservice/issues/623
            // TSql150Parser semble être la meilleure alternative pour parser du SQL
            // A Lire aussi https://social.msdn.microsoft.com/Forums/sqlserver/en-US/7f120e36-f4e6-4d44-bb53-0edd1d86a9c6/using-microsoftsqlservertransactsqlscriptdom-for-parse-query-with-errors?forum=sqldatabaseengine
            // https://blogs.msmvps.com/bsonnino/2018/08/28/parsing-sql-server-code-with-c-part-2-advanced-parsing/
            // doc https://docs.microsoft.com/fr-fr/dotnet/api/microsoft.sqlserver.management.sqlparser.metadata.imetadataobject.accept?view=sql-smo-140.17283.0
            // doc https://docs.microsoft.com/en-us/previous-versions/visualstudio/visual-studio-2010/dd194286(v=vs.100) 
            // https://docs.microsoft.com/en-us/dotnet/api/microsoft.sqlserver.transactsql.scriptdom.tsqlparser?view=sql-dacfx-140.3881.1
            // https://www.nuget.org/packages/Microsoft.SqlServer.TransactSql.ScriptDom/ (sous partie de https://www.nuget.org/packages/Microsoft.SqlServer.DacFx.x64/)
            var parser = new TSql150Parser(true);
            using (var textReader = new StringReader(procText))
            {
                var sqlTree = parser.Parse(textReader, out var errors);
                if (errors.Count > 0)
                {
                    TechnicalTools.Diagnostics.DebugTools.Break();
                }
                return Tuple.Create(sqlTree, errors);
            }
        }
        public class CustomVisitor : TSqlFragmentVisitor
        {
            private void DoSomething(dynamic obj)
            {
                foreach (var property in obj.GetType().
                    GetProperties(BindingFlags.Public | BindingFlags.Instance))
                {
                    // Recursively analyse object model to find specific objects
                }
            }
            
            public override void Visit(TSqlStatement node)
            {
                DoSomething(node);
                base.Visit(node);
            }
            // Generic statement
            public override void Visit(SelectStatement node)
            {
                DoSomething(node);
                base.Visit(node);
            }
        }
    }
}