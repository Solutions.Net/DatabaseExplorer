﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Microsoft.SqlServer.Management.Smo;

using TechnicalTools;

using DataMapper;

using DatabaseExplorer.DAL;
using DatabaseExplorer.Business.BaseDALClass;
using DatabaseExplorer.Business.Extensions;
using Microsoft.SqlServer.Management.Common;

namespace DatabaseExplorer.Business.Model.Local
{
    public partial class Project
    {
        public SqlConnectionStringBuilder    Connection                { get; set; }
        public DatabaseInfo                  DbInfos                   { get; protected set; }
        public string                        GeneratedAssemblyName     { get; protected set; }
        public Dictionary<string, string>    GeneratedSourceCode       { get; protected set; } // FileName +extension (not path) => content
        public string                        Warnings                  { get; protected set; }
        public List<string>                  RuntimeCheckedConstraints { get; private set; }
        public bool                          ForSqlCE                  { get { return false; } }
        public bool                          IsNewVersion              { get; set; }
        public GenericGeneratedDAO           GeneratedDAO              { get; private set; } // only if !IsNewVersion
        public DbGeneratedMapper             GeneratedMapper           { get; private set; } // only if IsNewVersion

        public EventHandler<DynamicTypeAssemblyGeneratorBase.CompilationErrorEventArgs> OnCompilationError;

        public virtual void RunImport()
        {
            var server = new Server(Connection.ConnectionString);
            server.ConnectionContext.ServerInstance = Connection.DataSource;

            if (!string.IsNullOrEmpty(Connection.UserID))
            {
                server.ConnectionContext.LoginSecure = false;
                server.ConnectionContext.Login = Connection.UserID;
                server.ConnectionContext.Password = Connection.Password;
            }
            var db = server.Databases[Connection.InitialCatalog];
            //db.Discover();
            DbInfos = new DatabaseInfo(db, Connection);
            Warnings = "";
        }

        public const string IdManagerTableName = "_IdManager";
        public const string IdManagerSeedOfIdColumnName = "SeedOfIds";

        public string GenerateSqlCodeCreation()
        {
            const String_Extensions.ePadding right = String_Extensions.ePadding.Right;
            //const String_Extensions.ePadding left = String_Extensions.ePadding.Left;
            const String_Extensions.ePadding none = String_Extensions.ePadding.None;
            // https://technet.microsoft.com/en-us/library/ms174642(v=sql.110).aspx
            // https://msdn.microsoft.com/en-us/library/ms172424(SQL.110).aspx
            //https://technet.microsoft.com/fr-fr/library/bb896140(v=sql.110).aspx
            // http://erikej.blogspot.fr/2011/01/comparison-of-sql-server-compact-4-and.html
            // http://erikej.blogspot.fr/2016/01/sql-server-compact-sqlite-toolbox-44-45.html
            var constraintsBack = new Dictionary<Table, string>();
            var sb = new StringBuilder();
            if (ForSqlCE && DbInfos.Schemas.Count > 1)
            {
                string report = DbInfos.Tables.FindDuplicatesAndMakeReport(t => t.Name, t => t.FullName());
                if (!string.IsNullOrEmpty(report))
                    throw new Exception("Sql CE does not support schema, So all tables must be unique !" + Environment.NewLine + report);
            }
            var foreigKeyDeclarationForSqlCE = new List<string>();
            foreach (var schema in DbInfos.Schemas)
            {
                string schemaName = ForSqlCE ? "" : "[" + schema.Name + "].";
                sb.AppendLine("CREATE TABLE " + schemaName + "[" + IdManagerTableName + "] (");
                sb.AppendLine("    [" + IdManagerSeedOfIdColumnName + "] bigint  NOT NULL");
                sb.AppendLine(");");
                sb.AppendLine("GO");

                foreach (var table in schema.Tables())
                {
                    sb.AppendLine("CREATE TABLE " + schemaName + "[" + table.Name + "] (");
                    var content = new List<Tuple<String_Extensions.ePadding, string>[]>();
                    var line = new List<string[]>();
                    foreigKeyDeclarationForSqlCE.Clear();
                    foreach (Column col in table.Columns)
                    {
                        string fkname = col.IsFK() ? "FK" + col.OwnerTable().Name + "__" + col.ReferencedColumn().OwnerTable().Name + "__" + col.Name : "";
                        string foreigKeyDeclaration = col.IsFK() ? " CONSTRAINT [" + fkname + "] FOREIGN KEY ([" + col.Name + "]) REFERENCES " + schemaName + "[" + col.ReferencedColumn().OwnerTable().Name + "]([" + col.ReferencedColumn().Name + "]) ON DELETE NO ACTION ON UPDATE NO ACTION" : "";
                        line.Add(new[]
                            {
                                "\t",
                                "[" + col.Name + "]",
                                // A refaire
                                ForSqlCE && col.DataType.SqlDataType == SqlDataType.Time ? "bigint"
                                    : ForSqlCE && col.DataType.SqlDataType == SqlDataType.VarBinary ? "image"
                                    : ForSqlCE && col.DataType.IsStringType ? "ntext"
                                    : ForSqlCE && col.DataType.SqlDataType == SqlDataType.DateTime2 ? "datetime"
                                    : col.DataType.ToString(),
                                col.Nullable ? "" : "NOT",
                                "NULL",
                                col.InPrimaryKey && col.OwnerTable().PkColumns().Count == 1 ? "PRIMARY KEY" : "",
                                !col.IsFK() || ForSqlCE ? "" : /*col.FkDefinition.LinkType() == eLinkType.BackwardLink ? "/* foreign key to " + col.ReferencedColumn.Owner.Name + " declared after it *" + "/" :*/ foreigKeyDeclaration
                            });
                        if (col.IsFK() && (/*col.FkDefinition.LinkType == eLinkType.BackwardLink ||*/ ForSqlCE))
                        {
                            if (!constraintsBack.ContainsKey(col.ReferencedColumn().OwnerTable()) && !ForSqlCE)
                                constraintsBack[col.ReferencedColumn().OwnerTable()] = "";
                            string declaration = "ALTER TABLE " + schemaName + "[" + table.Name + "] "
                                               + "ADD " + foreigKeyDeclaration
                                               + Environment.NewLine;
                            if (ForSqlCE)
                                foreigKeyDeclarationForSqlCE.Add(declaration);
                            else
                                constraintsBack[col.ReferencedColumn().OwnerTable()] += declaration;
                        }
                    }
                    sb.AppendLine(Regex.Replace(line.AlignAndMerge(new[] { none, right, right, right, right, right, right }, null).ToString(), " *" + Environment.NewLine, "," + Environment.NewLine));
                    if (table.PkColumns()?.Count > 1)
                    {
                        sb.Append("CONSTRAINT PK_" + table.Name + " PRIMARY KEY (");
                        sb.Append(table.PkColumns()?.Select(c => "[" + c.Name + "]").Join());
                        sb.AppendLine(")");
                    }
                    sb.AppendLine(");");
                    sb.AppendLine("GO");

                    foreach (var declaration in foreigKeyDeclarationForSqlCE)
                    {
                        sb.AppendLine(declaration);
                        sb.AppendLine("GO");
                    }

                    string technicalConstraints;
                    if (constraintsBack.TryGetValue(table, out technicalConstraints))
                    {
                        sb.AppendLine(technicalConstraints);
                        sb.AppendLine("GO");
                    }
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }

        public string GenerateSqlCodeDeletion()
        {
            var sb = new StringBuilder();

            foreach (var schema in DbInfos.Schemas)
            {
                string schemaName = ForSqlCE ? "" : "[" + schema.Name + "].";
                sb.AppendLine("DROP TABLE " + schemaName + "[" + IdManagerTableName + "];");
                sb.AppendLine("GO");
                foreach (var table in schema.Tables().Reverse())
                {
                    sb.AppendLine("DROP TABLE " + schemaName + "[" + table.Name + "];");
                    sb.AppendLine("GO");
                }
            }
            return sb.ToString();
        }

        public virtual string GenerateSqlDataInitialization()
        {
            var sb = new StringBuilder();
            foreach (var schema in DbInfos.Schemas)
            {
                string schemaName = ForSqlCE ? "" : "[" + schema.Name + "].";
                sb.AppendLine("INSERT " + schemaName + "[" + IdManagerTableName + "] " +
                              "(" + IdManagerSeedOfIdColumnName + ") " +
                              "VALUES (1);");
                sb.AppendLine("GO");
            }
            return sb.ToString();
        }


        public void GenerateDALSourceCodeFiles(string assemblyName, bool oneFilePerClass = false, bool navigationProperties = false, Type entitySpecificBaseClass = null)
        {
            var all_tables = DbInfos.Tables;
            var generator = new DynamicTypeCodeGeneratorForDemo(assemblyName, all_tables, Connection.ConnectionString, entitySpecificBaseClass)
            {
                OneFilePerClass = oneFilePerClass,
                GenerateReferencedCollectionProperties = navigationProperties,
                GenerateReferencingCollectionProperties = navigationProperties
            };
            GeneratedSourceCode = generator.GenerateSourceCode();
            GeneratedAssemblyName = assemblyName;
        }
        public void GenerateDALSourceCodeFilesForKronos(string dalAssemblyName, string dbName, bool oneFilePerClass = false, bool navigationProperties = false, Type entitySpecificBaseClass = null)
        {
            var all_tables = DbInfos.Tables.Where(t => t.OwnerSchema().Name == dbName).ToList();
            var generator = new DynamicTypeCodeGeneratorForKronos(dalAssemblyName, all_tables, Connection == null ? null : Connection.ConnectionString, entitySpecificBaseClass)
            {
                ForSqlCE = ForSqlCE,
                OneFilePerClass = oneFilePerClass,
                GenerateReferencedCollectionProperties = navigationProperties,
                GenerateReferencingCollectionProperties = navigationProperties
            };
            GeneratedSourceCode = generator.GenerateSourceCode();
            GeneratedAssemblyName = dalAssemblyName;
        }
        public void GenerateDALSourceCodeFilesForLMT(string assemblyName, bool oneFilePerClass = false, bool navigationProperties = false)
        {
            var all_tables = DbInfos.Tables;
            var generator = new DynamicTypeCodeGeneratorNewForLMT(assemblyName, all_tables, Connection.ConnectionString)
            {
                OneFilePerClass = oneFilePerClass,
                GenerateReferencedCollectionProperties = navigationProperties,
                GenerateReferencingCollectionProperties = navigationProperties,
                IncludeBaseClassAndDAOInGeneratedCode = true
            };
            GeneratedSourceCode = generator.GenerateSourceCode();
            GeneratedAssemblyName = assemblyName;
        }

        public void GenerateSourceCodeFilesForTracker(bool oneFilePerClass, Type entitySpecificBaseClass = null)
        {
            const string assemblyName = "trk.DAL";
            var generator = new DynamicTypeCodeGenerator(assemblyName, TrackerReferentialTables(), Connection.ConnectionString, entitySpecificBaseClass) { OneFilePerClass = oneFilePerClass };
            GeneratedSourceCode = generator.GenerateSourceCode();
            GeneratedAssemblyName = assemblyName;
        }
        public void GenerateSourceCodeFilesForLMT(bool oneFilePerClass)
        {
            const string assemblyName = "LMT.DAL";
            var all_tables = DbInfos.Tables.ToList();
            var generator = new DynamicTypeCodeGeneratorNewForLMT(assemblyName, all_tables, Connection.ConnectionString) { OneFilePerClass = oneFilePerClass };
            GeneratedSourceCode = generator.GenerateSourceCode();
            GeneratedAssemblyName = assemblyName;

            //var schemas = DbInfos.Schemas.OrderBy(s => s.Name).ToList();
            //DbInfos.Schemas.Clear(); DbInfos.Schemas.AddRange(schemas);
            //foreach (var schema in DbInfos.Schemas)
            //{
            //    var tables = schema.Tables.OrderBy(t => t.Name).ToList();
            //    schema.Tables.Clear(); schema.Tables.AddRange(tables);
            //    foreach (var table in schema.Tables)
            //    {
            //        var all_columns = table.Columns.OrderBy(c => c.Name).ToList();
            //        table.Columns.Clear(); table.Columns.AddRange(all_columns);
            //    }
            //}

            //string tt = GenerateSqlCodeCreation();
        }

        public void CompileOneFileDALForDemo()
        {
            if (IsNewVersion)
            {
                var generator = new DynamicTypeAssemblyGeneratorNew(GeneratedAssemblyName, GeneratedSourceCode.Values.Join(Environment.NewLine));
                generator.OnCompilationError += OnCompilationError;
                var cons = generator.GenerateAssemblyAndRetry();
                GeneratedMapper = (DbGeneratedMapper)cons(Connection.ConnectionString);
                generator.OnCompilationError -= OnCompilationError;
            }
            else
            {
                var generator = new DynamicTypeAssemblyGeneratorOld(GeneratedAssemblyName, GeneratedSourceCode.Values.Join(Environment.NewLine));
                generator.OnCompilationError += OnCompilationError;
                var cons = generator.GenerateAssemblyAndRetry();
                GeneratedDAO = (GenericGeneratedDAO)cons(Connection.ConnectionString);
                generator.OnCompilationError -= OnCompilationError;
            }
        }
        

        public List<Table> TrackerReferentialTables()
        {
            var table_names = new[]
                {
                    "Correspondents", // clsAccount
                    "Assets", // clsAsset
                    "BUnits", // clsBUnit
                    "CDContracts", // clsCDContract
                    "CheckRules", // clsCheckRule
                    "ComplianceChecks", // clsComplianceCheck
                    "TransactionComponents$", // clsComponent
                    "ComplianceRules", // clsComplianceRule
                    "CorporateActions", // clsCorporateAction
                    "FileExchanges", // clsFileImport
                    "Groups", // clsGroup
                    "Indexes", // clsIndex
                    "Orders", // clsOrder
                    "OrderLists", // clsOrderList
                    "Counterparties", // clsParty
                    "Persons", // clsPerson
                    "Portfolios$", // clsPortfolio
                    "PortfolioFeeStructures", // clsPortfolioFeeStructure
                    "PortfolioUsers", // clsPortfolioRestriction
                    "PortfolioInstrumentTypes", // clsPortfolioRestriction !!!!
                    "Profiles", // clsProfile
                    "RSReportInstances", // "clsRSReportInstance",
                    "Strategies", // clsStrategy
                    "CFStructure", // "clsStructure", 
                    "Subscriptions", // "clsSubscription",
                    "Transactions", // "clsTrade",
                    "Users", // clsUser, 
                    "LimitChecks",
                    "LimitChecksDetails",
                    "LimitTemplate",
                    "LimitTemplateProperties",
                    "LimitPropertyValues"
                };

            return DbInfos.Tables.Where(table => table.Name.In(table_names)).ToList();
        }

        public void ProcessRuntimeCheckedConstraints(IReadOnlyCollection<Table> tables)
        {
            var warnings = new List<string>();
            // trouve les tables qui ont 
            var table_with_item_having_id_zero  = new List<Table>();
            foreach (var t in tables)
                foreach (var cons in t.FkConstraints())
                {
                    Debug.Assert(cons.ForeignColumns.Length == 1);
                    var rtable = cons.ReferencedColumns.First().OwnerTable();
                    string sql = "select count(*) from " + rtable.OwnerSchema().Name + "." + rtable.Name + " where " + cons.ReferencedColumns[0].Name + " = 0";
                    using (var con = new SqlConnection(Connection.ConnectionString))
                    {
                        con.Open();
                        var cmd = con.CreateCommand();
                        cmd.CommandText = sql;
                        var res = (int)cmd.ExecuteScalar();
                        if (res > 0)
                            table_with_item_having_id_zero.Add(rtable);
                    }
                }

            foreach (var t in tables)
                foreach (var cons in t.FkConstraints())
                {
                    Debug.Assert(cons.ForeignColumns.Length == 1);
                    foreach (Column c in cons.ForeignColumns)
                        if (c.Nullable && !table_with_item_having_id_zero.Contains(cons.ReferencedColumns.First().OwnerTable()))
                            warnings.Add("Valeur à mettre à null au lieu de zero : " + t.Name + "." + c.Name);
                }

            foreach (var t in tables)
                foreach (Column c in t.Columns)
                    if (!c.Nullable && c.CsType() == typeof(string))
                        warnings.Add("Non nullable string : " + t.Name + "." + c.Name);

            RuntimeCheckedConstraints = warnings;
        }

        public void ExportAllSqlObjectToFolder(string folder)
        {
            //Directory.CreateDirectory(folder);

            //string folder_tables = Path.Combine(folder, "Tables");
            //Directory.CreateDirectory(folder_tables);
            //foreach (var table in DbInfos.Tables)
            //    File.WriteAllText(Path.Combine(folder_tables, table.Name + ".sql"), table.RawInfo.Source);

            //string folder_views = Path.Combine(folder, "Views");
            //Directory.CreateDirectory(folder_views);
            //foreach (var view in DbInfos.Views)
            //    File.WriteAllText(Path.Combine(folder_views, view.Name + ".sql"), view.RawInfo.Source);

            //string folder_procs = Path.Combine(folder, "Procedures");
            //Directory.CreateDirectory(folder_procs);
            //foreach (var proc in DbInfos.Procedures)
            //    File.WriteAllText(Path.Combine(folder_procs, proc.RawInfo.Name + ".sql"), proc.RawInfo.Source);

            //string folder_PKs = Path.Combine(folder, "Constraints - Primary Keys");
            //Directory.CreateDirectory(folder_PKs);
            //foreach (var pk in DbInfos.PrimaryKeyConstraints)
            //    File.WriteAllText(Path.Combine(folder_PKs, pk.RawInfo.Name + ".sql"), pk.RawInfo.Source);

            //string folder_keyForeign = Path.Combine(folder, "Constraints - Foreign Keys");
            //Directory.CreateDirectory(folder_keyForeign);
            //foreach (var keyForeign in DbInfos.ForeignKeyConstraints)
            //    File.WriteAllText(Path.Combine(folder_keyForeign, keyForeign.RawInfo.Name + ".sql"), keyForeign.RawInfo.Source);

            //string folder_chkConstraints = Path.Combine(folder, "Constraints - Checks");
            //Directory.CreateDirectory(folder_chkConstraints);
            //foreach (var chkConstraint in DbInfos.CheckConstraints)
            //    File.WriteAllText(Path.Combine(folder_chkConstraints, chkConstraint.RawInfo.Name + ".sql"), chkConstraint.RawInfo.Source);

            //string folder_dftConstraints = Path.Combine(folder, "Constraints - Defaults");
            //Directory.CreateDirectory(folder_dftConstraints);
            //foreach (var dftConstraint in DbInfos.DefaultConstraints)
            //    File.WriteAllText(Path.Combine(folder_dftConstraints, dftConstraint.RawInfo.Name + ".sql"), dftConstraint.RawInfo.Source);

            //string folder_unqConstraints = Path.Combine(folder, "Constraints - Uniques");
            //Directory.CreateDirectory(folder_unqConstraints);
            //foreach (var unqConstraint in DbInfos.UniqueConstraints)
            //    File.WriteAllText(Path.Combine(folder_unqConstraints, unqConstraint.RawInfo.Name + ".sql"), unqConstraint.RawInfo.Source);

            //string folder_itfuncs = Path.Combine(folder, "Functions - Inline Table");
            //Directory.CreateDirectory(folder_itfuncs);
            //foreach (var itfunc in DbInfos.InlineTableFunction)
            //    File.WriteAllText(Path.Combine(folder_itfuncs, itfunc.RawInfo.Name + ".sql"), itfunc.RawInfo.Source);

            //string folder_scalFuncs = Path.Combine(folder, "Functions - Scalar");
            //Directory.CreateDirectory(folder_scalFuncs);
            //foreach (var sclFunc in DbInfos.ScalarFunctions)
            //    File.WriteAllText(Path.Combine(folder_scalFuncs, sclFunc.RawInfo.Name + ".sql"), sclFunc.RawInfo.Source);

            //string folder_tblFuncs = Path.Combine(folder, "Functions - Tables");
            //Directory.CreateDirectory(folder_tblFuncs);
            //foreach (var tblFunc in DbInfos.TableFunctions)
            //    File.WriteAllText(Path.Combine(folder_tblFuncs, tblFunc.RawInfo.Name + ".sql"), tblFunc.RawInfo.Source);
        }
    }
}