﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.Design
{
    public class LogError
    {
        public DateTime When       { get; set; }
        public string Method       { get; set; }
        public string Args         { get; set; }
        public string ErrorMessage { get; set; }
        public string FullLog      { get; set; }
    }
}
