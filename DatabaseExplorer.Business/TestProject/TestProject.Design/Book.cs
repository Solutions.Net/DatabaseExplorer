﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnicalTools.Annotations.Design;

namespace TestProject.Design
{
    public class Book
    {
                  public string Title  { get; set; }
                  public int PageCount { get; set; }
        [NotNull] public Author TheAuthor { get; set; }
                  public Author CoAuthor { get; set; }
        
        [Composite] public List<Page> Pages   { get; set; }

        [Composite] Dictionary<string, Book> References { get; set; }
    }
}
