﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnicalTools.Annotations.Design;

namespace TestProject.Design
{
    public struct PlanningTimeSlot
    {
        // Les valeurs par default de Debut et Fin definissent l'integralité de la journée (0h - minuit)
        [PropertyOffset(0)]  public TimeSpan  Debut         { get; set; }
        [PropertyOffset(8)]  public TimeSpan  Fin           { get; set; }
        [PropertyOffset(16)] public UInt64    Disponibility { get; set; }
        [PropertyOffset(24)] public int       Flags         { get; set; }
    }
}
