﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.Design
{
    public class Author : Person
    {
        public string NickName { get; set; }
        public PlanningTimeSlot ArrivingTime { get; set; }
        public PlanningTimeSlot? ArrivingTimeNullable { get; set; }
        //public List<PlanningTimeSlot?> TimesNullable { get; set; }
        public List<int> Ints { get; set; }
        public List<PlanningTimeSlot> Times { get; set; }
    }
}
