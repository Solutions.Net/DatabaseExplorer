﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.Design
{
    public class Page
    {
        public int    Index   { get; set; }
        public string Content { get; set; }

        Dictionary<string, string> WordDefinitions { get; set; }
    }

}
