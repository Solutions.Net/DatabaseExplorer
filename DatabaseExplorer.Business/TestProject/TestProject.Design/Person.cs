﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnicalTools.Annotations.Design;
using TestProject.Types;

namespace TestProject.Design
{
    public class Person
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }

        [Composite][NotNull] public Address Address { get; set; }
        [Composite]          public Address Address2 { get; set; }

        public List<Person> Friends { get; set; }


        public Dictionary<DateTime, eDayTask> History { get; set; }
    }
}
