﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestProject.Tests;

namespace TestProject.TestsDbg
{
    class Program
    {
        static void Main(string[] args)
        {
            Config.IsInteractive = true;
            var test = new CreateAuthorAndInheritance();
            test.CleanDatabase();
            test.CreateAuthorAndTestCanReload();
        }
    }

}
