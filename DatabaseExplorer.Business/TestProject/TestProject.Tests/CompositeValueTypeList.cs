﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using DAL_Generator.Business;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;
using TestProject.Model;

namespace TestProject.Tests
{
    // Un seul test par classe !
    [TestClass]
    public class CompositeValueTypeList
    {
        static DAL.GeneratedDAO dao = Config.DAO;
        static readonly string nl = Environment.NewLine;

        [TestInitialize]
        public void CleanDatabase()
        {
            Config.CleanAllTableOfAnyDB();
            Config.RebuildDB();
            Config.BeginTest(this.GetType().Name);
        }
        [TestCleanup]
        public void EndOfTest()
        {
            Config.EndTest();
        }

        [TestMethod]
        public void CreateAuthorAndPlayWithCompositeValueTypeList()
        {
            var author = new Author() { Firstname = "John", Lastname = "Doe" };
            author.Ints.Add(1);
            author.Ints.Add(2);
            author.Ints.Add(3);
            author.SynchronizeToDatabase();
                                                                            Assert.AreEqual(author.State, eState.Synchronized);
                                                                            Assert.AreEqual(author.Ints.State, eState.Synchronized);
            AssertionManager.MachineNameOnWhichFailedAssertionPopupIsDisabled.Add(Environment.MachineName);
            var authorCopy = new Author(author.Id);
            AssertionManager.MachineNameOnWhichFailedAssertionPopupIsDisabled.Remove(Environment.MachineName);

                                                                            Assert.AreEqual(authorCopy.State, eState.Synchronized);
                                                                            Assert.AreEqual(authorCopy.Ints.State, eState.Synchronized);

                                                                            Assert.AreEqual(author.Dump(), authorCopy.Dump());

            author.Ints.RemoveAt(0);
            author.Ints.Remove(3);
            author.Ints.Add(4);
                                                                            Assert.AreEqual(author._Ints.DeletedObjects.Count, 2);
            author.SynchronizeToDatabase();
                                                                            Assert.AreEqual(author._Ints.DeletedObjects.Count, 0);
                                                                            Assert.AreEqual(author.State, eState.Synchronized);
                                                                            Assert.AreEqual(author._Ints.State, eState.Synchronized);
                                                                            Assert.AreEqual(author._Ints.Count, 2);

                                                                            Assert.AreEqual(DAL.Dbo.Author_Int.GetEntitiesWhere(null).Count, 2);
        }


    }
}
