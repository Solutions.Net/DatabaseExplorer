﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using DAL_Generator.Business;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;
using TestProject.Model;

namespace TestProject.Tests
{
    // Un seul test par classe !
    [TestClass]
    public class CreateBookAndCompositeLinks
    {
        static DAL.GeneratedDAO dao = Config.DAO;
        static readonly string nl = Environment.NewLine;

        [TestInitialize]
        public void CleanDatabase()
        {
            Config.CleanAllTableOfAnyDB();
            Config.RebuildDB();
            Config.BeginTest(this.GetType().Name);
        }
        [TestCleanup]
        public void EndOfTest()
        {
            Config.EndTest();
        }

        [TestMethod]
        public void CreateBook()
        {
            long pId = _CreateBook();
            GC.Collect(3, GCCollectionMode.Forced, true);
            var b = CacheManager.Instance.Get<Book>(new IdTuple<long>(pId), true);
            Assert.IsNull(b);
            b = CacheManager.Instance.Get<Book>(new IdTuple<long>(pId));
            Assert.AreEqual(b.Dump(), bookDump.Replace("New", eState.Synchronized.GetDescription()));
            Assert.AreEqual(b.Pages[1].State, eState.Synchronized);
            Assert.AreEqual(b.Pages.State, eState.Synchronized);
            Assert.AreEqual(b.State, eState.Synchronized);
            var removedPage = b.Pages[2];
            b.Pages.Remove(removedPage);
            b.Pages.Clear();
            foreach (var p in b.Pages.DeletedObjects)
                Assert.AreEqual(p.State, eState.ToDelete);
            foreach (var p in b.Pages.DeletedObjects.ToList())
                if (p != removedPage)
                {
                    b.Pages.Add(p);
                    Assert.AreEqual(p.State, eState.Unsynchronized);
                }
            Assert.AreEqual(removedPage.State, eState.ToDelete);
            Assert.AreEqual(b.Pages.State, eState.Unsynchronized);
            Assert.AreEqual(b.State, eState.Unsynchronized);
            b.Pages.Add(removedPage); 
            Assert.AreEqual(b.Pages.DeletedObjects.Count, 0);
            Assert.AreEqual(removedPage.State, eState.Unsynchronized);
            b.Pages.Remove(removedPage); 
            //removedPage.UpdateState(eState.ToDelete); // TODO : Cas complexe à gerer
            //Assert.AreEqual(b.Pages.DeletedObjects.Count, 0); // Les listes ne stockent pas forcément des éléments composites donc si un element change de status, il ne vas pas dans DeletedObject
            Assert.AreEqual(b.Pages.DeletedObjects.Count, 1); 
            Assert.AreEqual(b.Pages.State, eState.Unsynchronized);
            Assert.AreEqual(b.State, eState.Unsynchronized);

            b.SynchronizeToDatabase();
            Assert.AreEqual(b.Pages.State, eState.Synchronized);
            Assert.AreEqual(b.State, eState.Synchronized);
            b.Pages[1].Content += " more content";
            Assert.AreEqual(b.Pages[1].State, eState.Unsynchronized);
            Assert.AreEqual(b.Pages.State, eState.Unsynchronized);
            Assert.AreEqual(b.State, eState.Unsynchronized);

            b.SynchronizeToDatabase();
            Assert.AreEqual(b.Pages[1].State, eState.Synchronized);
            Assert.AreEqual(b.Pages.State, eState.Synchronized);
            Assert.AreEqual(b.State, eState.Synchronized);
            Assert.AreEqual(b.Changes, "");
        }

        public long _CreateBook()
        {
            var p1 = new Page() { Index = 1, Content = "page 1" };
            p1.SynchronizeToDatabase();

            var b = new Book() { Title = "One title", PageCount = 3 };
            b.Pages.Add(p1);
            b.Pages.Add(new Page() { Index = 2, Content = "page 2" });
            b.Pages.Add(new Page() { Index = 3, Content = "page 3" });
            try
            {
                b.SynchronizeToDatabase();
                Assert.Fail("Must throw because author is mandatory");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message.Contains("TheAuthor"));
            }
            b.TheAuthor = new Author() { Firstname = "Author", Lastname = "Of Book" };

            try
            {
                b.SynchronizeToDatabase();
                Assert.Fail("Must throw because author is an aggregation relation. It must be saved before book");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex.Message.Contains("The INSERT statement conflicted with the FOREIGN KEY") ||
                              ex.Message.Contains("L'instruction INSERT est en conflit avec la contrainte FOREIGN KEY"));
            }
            Assert.AreEqual(b.Dump(), bookDump); // TODO : Normalement bookDump devrait contenir que des status New !
            Assert.AreEqual(b.State, eState.New);
            b.TheAuthor.SynchronizeToDatabase();
            Assert.AreEqual(b.TheAuthor.State, eState.Synchronized);
            Assert.AreEqual(b.State, eState.New);
            b.SynchronizeToDatabase();
            Assert.AreEqual(b.Pages.State, eState.Synchronized);
            Assert.AreEqual(b.State, eState.Synchronized);
            Assert.AreEqual(b.Changes, "");
            return b.Id;
        }
        
        static string bookDump = "[Book, 23] New" + nl +
                                      "   Title: One title" + nl +
                                      "   PageCount: 3" + nl +
                                      "   TheAuthor: 26" + nl +
                                      "   CoAuthor: " + nl +
                                      "   Pages:" + nl +
                                      "      - [Page, 22] Synchronized with DB" + nl +
                                      "           Index: 1" + nl +
                                      "           Content: page 1" + nl +
                                      "      - [Page, 24] Synchronized with DB" + nl +
                                      "           Index: 2" + nl +
                                      "           Content: page 2" + nl +
                                      "      - [Page, 25] Synchronized with DB" + nl +
                                      "           Index: 3" + nl +
                                      "           Content: page 3";

    }
}
