﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using DAL_Generator.Business;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;
using TestProject.Model;

namespace TestProject.Tests
{
    // Un seul test par classe !
    [TestClass]
    public class CreateAuthorAndInheritance
    {
        static DAL.GeneratedDAO dao = Config.DAO;
        static readonly string nl = Environment.NewLine;

        [TestInitialize]
        public void CleanDatabase()
        {
            Config.CleanAllTableOfAnyDB();
            Config.RebuildDB();
            Config.BeginTest(this.GetType().Name);
        }
        [TestCleanup]
        public void EndOfTest()
        {
            Config.EndTest();
        }

        [TestMethod]
        public void CreateAuthorAndTestCanReload()
        {
            var pId = CreatePerson();
            GetBackPersonByCacheManager(pId);
            var p = GetBackPersonByDirectLoading(pId);
            TestChanges(p);
            TestWithFriend(p);

            p.Delete();
            Assert.AreEqual(p.State, eState.Deleted);
            Assert.AreEqual(p.Address.State, eState.Deleted);
            Assert.AreEqual(p.Address2.State, eState.Deleted);
        }
        long CreatePerson()
        {
            var p = new Author() { Firstname = "John", Lastname = "Doe", NickName = "NickName" };
            Assert.IsTrue(p.Id != 0, "Un id a du etre créé !");
            Assert.AreEqual(p.State, eState.New);
            p.SynchronizeToDatabase();
            Assert.AreEqual(p.State, eState.Synchronized);
            p.Address.AddressLine1 = "123, Main Street";
            Assert.AreEqual(p.State, eState.Unsynchronized);
            p.Address.City = "Tulsa";
            p.Address.StateOrProvinceOrRegion = "Oklahoma";
            p.Address.ZipPostalCode = "74120"; // phone 918-295-2532
            p.Address.Countr = "U.S.A";
            p.UpdateState(eState.Synchronized); // This line does not have meaning ! This is just to test the next assert
            p.Address2 = new Address();
            Assert.AreEqual(p.State, eState.Unsynchronized);
            p.UpdateState(eState.Synchronized);
            p.Address2.AddressLine1 = "_" + p.Address.AddressLine1;
            p.Address2.City = "_" + p.Address.City;
            p.Address2.StateOrProvinceOrRegion = "_" + p.Address.StateOrProvinceOrRegion;
            p.Address2.ZipPostalCode = "_" + p.Address.ZipPostalCode;
            p.Address2.Countr = "_" + p.Address.Countr;
            Assert.AreEqual(p.State, eState.Unsynchronized);
            p.SynchronizeToDatabase();
            Assert.AreEqual(p.State, eState.Synchronized);
            Assert.AreEqual(p.Address.State, eState.Synchronized);
            Assert.AreEqual(p.Address2.State, eState.Synchronized);

            Assert.AreSame(p, CacheManager.Instance.Get<Author>(new IdTuple<long>(p.Id), true));
            return p.Id;
        }
        void GetBackPersonByCacheManager(long pId)
        {
            GC.Collect();
            Debug.WriteLine(BusinessObjectGenerated.DumpAllObjectsInMemory());
            Assert.IsNull(CacheManager.Instance.Get<Author>(new IdTuple<long>(pId), true));
            var p = CacheManager.Instance.Get<Author>(new IdTuple<long>(pId));
            Assert.AreEqual(p.State, eState.Synchronized);
            Assert.AreEqual(p.Firstname, "John");
            Assert.AreEqual(p.Lastname, "Doe");
            Assert.AreEqual(p.NickName, "NickName");

            Assert.AreEqual(p.Address.AddressLine1, "123, Main Street");
            Assert.AreEqual(p.Address.City, "Tulsa");
            Assert.AreEqual(p.Address.StateOrProvinceOrRegion, "Oklahoma");
            Assert.AreEqual(p.Address.ZipPostalCode, "74120");
            Assert.AreEqual(p.Address.Countr, "U.S.A");

            Assert.AreEqual(p.Address2.AddressLine1, "_123, Main Street");
            Assert.AreEqual(p.Address2.City, "_Tulsa");
            Assert.AreEqual(p.Address2.StateOrProvinceOrRegion, "_Oklahoma");
            Assert.AreEqual(p.Address2.ZipPostalCode, "_74120");
            Assert.AreEqual(p.Address2.Countr, "_U.S.A");
        }
        Author GetBackPersonByDirectLoading(long pId)
        {
            GC.Collect();
            Debug.WriteLine(BusinessObjectGenerated.DumpAllObjectsInMemory());
            Assert.IsNull(CacheManager.Instance.Get<Author>(new IdTuple<long>(pId), true));
            var p = new Author(pId);
            Assert.AreSame(p, CacheManager.Instance.Get<Author>(new IdTuple<long>(pId), true));
            Assert.AreEqual(p.State, eState.Synchronized);
            Assert.AreEqual(p.Firstname, "John");
            Assert.AreEqual(p.Lastname, "Doe");
            Assert.AreEqual(p.NickName, "NickName");

            Assert.AreEqual(p.Address.AddressLine1, "123, Main Street");
            Assert.AreEqual(p.Address.City, "Tulsa");
            Assert.AreEqual(p.Address.StateOrProvinceOrRegion, "Oklahoma");
            Assert.AreEqual(p.Address.ZipPostalCode, "74120");
            Assert.AreEqual(p.Address.Countr, "U.S.A");

            Assert.AreEqual(p.Address2.AddressLine1, "_123, Main Street");
            Assert.AreEqual(p.Address2.City, "_Tulsa");
            Assert.AreEqual(p.Address2.StateOrProvinceOrRegion, "_Oklahoma");
            Assert.AreEqual(p.Address2.ZipPostalCode, "_74120");
            Assert.AreEqual(p.Address2.Countr, "_U.S.A");

            return p;
        }

        void TestChanges(Author p)
        {
            var dump = p.Dump();
            Assert.AreEqual(p.State, eState.Synchronized);
            Assert.AreEqual(p.Changes, "");
            p.Firstname += "2";
            Assert.AreEqual(p.Changes, "Unsynchronized with DB\r\n   Firstname: John2");
            p.Firstname = null;
            Assert.AreEqual(p.Changes, "Unsynchronized with DB\r\n   Firstname: ");
            p.Firstname = "John";
            Assert.AreEqual(p.Changes, "Unsynchronized with DB");

            // TODO : essayer avec les Addresses !

            p.CancelChanges();
            Assert.AreEqual(p.State, eState.Synchronized);
            Assert.AreEqual(p.Changes, "");
            Assert.AreEqual(dump, p.Dump());
        }
        void TestWithFriend(Author p)
        {
            Assert.AreEqual(p.State, eState.Synchronized);
            
            var f1 = new Person() { Firstname = "a", Lastname = "A" };
            p.Friends.Add(f1);
            var f2 = new Person() { Firstname = "b", Lastname = "B" };
            p.Friends.Add(f2);
            var f3 = new Person() { Firstname = "c", Lastname = "C" };
            p.Friends.Add(f3);

            var dump = p.Dump();
            var dumpWithSynchronizedFriends = dump.Replace("New", "Synchronized with DB");
            try
            {
                p.SynchronizeToDatabase();                      Assert.Fail("Should because friend are aggregation link and have not been saved !");
            }
            catch
            {
                                                                Assert.AreEqual(dump, p.Dump());
            }
                                                                

            f1.SynchronizeToDatabase();
            f2.SynchronizeToDatabase();
            f3.SynchronizeToDatabase();
                                                                Assert.AreEqual(dumpWithSynchronizedFriends, p.Dump());
            
            p.SynchronizeToDatabase();                          
                                                                Assert.AreEqual(p.State, eState.Synchronized);

            p.Friends.Remove(f1);                               
                                                                Assert.AreEqual(f1.State, eState.Synchronized);
                                                                Assert.AreEqual(p.Friends.State, eState.Unsynchronized);
                                                                Assert.AreEqual(p.Friends.DeletedObjects.Count, 1);
                                                                Assert.AreEqual(p.State, eState.Unsynchronized);
                                                                Assert.AreEqual(p.Friends.Count, 2);
            p.CancelChanges();
                                                                Assert.AreEqual(p.State, eState.Synchronized);
                                                                Assert.AreEqual(p.Friends.State, eState.Synchronized);
                                                                Assert.AreEqual(p.Friends.Count, 3);                                                    
                                                                Assert.AreEqual(dumpWithSynchronizedFriends.Replace("] Unsynchronized", "] Synchronized"), p.Dump());
                                                                Assert.AreSame(f1, p.Friends.FirstOrDefault(f => f.Id == f1.Id));
                                                                

            f1.Firstname = "test";
                                                                Assert.AreEqual(f1.State, eState.Unsynchronized);
                                                                Assert.AreEqual(p.State, eState.Synchronized);
                                                                Assert.AreEqual(p.Friends.State, eState.Synchronized);
            f1.CancelChanges();
                                                                Assert.AreEqual(f1.State, eState.Synchronized);
                                                                Assert.AreEqual(p.State, eState.Synchronized);
                                                                Assert.AreEqual(p.Friends.State, eState.Synchronized);
            p.Friends.Remove(f1);
            p.SynchronizeToDatabase();
                                                                Assert.AreEqual(f1.State, eState.Synchronized);
                                                                Assert.AreEqual(p.Friends.State, eState.Synchronized);
                                                                Assert.AreEqual(p.Friends.DeletedObjects.Count, 0);
                                                                Assert.AreEqual(p.Friends.Count, 2);
                                                                Assert.AreEqual(p.State, eState.Synchronized);
        }
    }
}
