﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Tools.Sql;

using GenericDALBasics;

using TestProject.Generator;

using System.Diagnostics;
using System.IO;
using System.Data.SqlServerCe;

namespace TestProject.Tests
{
    public class Config
    {
        public static bool ForSqlCE = true;
        public static readonly string SqlCeDbFile = ProjectPath + "db.sdf";
        public static readonly string ConnectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=TestProject;Integrated Security=SSPI;";
        //public static readonly string ConnectionString = KronosSqlAzurConnectionBuilder.BuildRealConnectionString("mickael.labau", "micIfzen0;", "Test");
        //public static readonly string ConnectionString = KronosSqlAzurConnectionBuilder.BuildRealConnectionString("aaa", "password", "Kronos");

        public static void LogLine(string text)
        {
            string loc = ProjectPath + "Tests.log";
            Debug.WriteLine(text);
            System.IO.File.AppendAllText(loc, text + Environment.NewLine);
        }

        public static string ProjectPath
        {
            get
            {
                var ass = System.Reflection.Assembly.GetExecutingAssembly();
                if (ass.Location.LastIndexOf("TestResults") > 0)
                    return ass.Location.Remove(ass.Location.LastIndexOf("TestResults")) + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + @"\";
                return ass.Location.Remove(ass.Location.LastIndexOf("\\bin\\") + 1);
            }
        }
        static Config()
        {
            LogLine("");
            LogLine("-------- Test Start --------");
            LogLine("");
            DebugTools.Initialize();

            TechnicalTools.Tools.Threading.Thread.ForceInitialization();
            ConfigureProject();

            DAO = DAL.GeneratedDAO.Instance;
            Generator = new TestProjectGenerator() { ForSqlCE = ForSqlCE };
            Generator.Build();
        }

        public static DAL.GeneratedDAO DAO;
        public static TestProjectGenerator Generator;

        #region Configuration minimale inter projet
        static void ConfigureProject()
        {
            if (ForSqlCE)
                DAL.GeneratedDAO.Connect(() => new GenericDBConnectionSqlCE("Data Source=\"" + SqlCeDbFile + "\""));
            else
                DAL.GeneratedDAO.Connect(() => new GenericDBConnection(Config.ConnectionString));

            DbMapper.WithConnection = action => DAO.WithConnection(con => action(new ConnectionProviderForDataMapper(con)));
            DbOperationsForMappedItem.LetIdBeGeneratedBySql = false;
        }
        class ConnectionProviderForDataMapper : IConnectionProvider
        {
            IGenericDBConnection _dbConnection;
            public ConnectionProviderForDataMapper(IGenericDBConnection con)
            {
                _dbConnection = con;
            }

            public DataTable GetDataTable(string sql)
            {
                return _dbConnection.GetDatatable(sql);
            }

            public TechnicalTools.Tools.Sql.IDbTransaction StartTransaction()
            {
                return _dbConnection.StartTransaction();
            }

            public void ExecuteNonQuery(string sql)
            {
                _dbConnection.ExecuteNonQuery(sql);
            }

            public T ExecuteScalar<T>(string sql, T default_value = default(T)) where T : struct
            {
                return _dbConnection.ExecuteScalar<T>(sql, default_value).Value;
            }
        }
        #endregion

        public static void CleanAllTableOfAnyDB()
        {
            if (ForSqlCE)
            {
                if (File.Exists(SqlCeDbFile))
                    File.Delete(SqlCeDbFile);
                string connectionString = "Data Source=\"" + SqlCeDbFile + "\"";
                var en = new SqlCeEngine(connectionString);
                en.CreateDatabase();
                return;
            }
            while (true)
                try
                {
                    string nl = Environment.NewLine;
                    string query = "EXEC sp_MSforeachtable @command1 = \"DROP TABLE ?\"";
                    // Une requete qui fonctionne sur SQL azure
                    // From https://edspencer.me.uk/2013/02/25/drop-all-tables-in-a-sql-server-database-azure-friendly/
                    // And http://stackoverflow.com/questions/29851302/sql-remove-all-constraints-azure-friendly
                    query = SqlRequests.DropAllTableAndConstraints();
                               
                    DAO.WithConnection(con => con.ExecuteNonQuery(query));
                                               
                    break;
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("FOREIGN KEY"))
                        continue;
                    throw;
                }
        }
        public static Dictionary<string, int> GetCounts()
        {
            string nl = Environment.NewLine;
            string request = "select  D.type_desc as 'Desc'," + nl +
                             "		(select count(*) from  sys.objects as O where O.type_desc = D.type_desc) as Count" + nl +
                             "from (	select 'USER_TABLE' as type_desc" + nl +
                             "	  union select 'PRIMARY_KEY_CONSTRAINT'" + nl +
                             "	  union select 'FOREIGN_KEY_CONSTRAINT') as D";
            if (ForSqlCE)
                request = @"-- les tables sous SqlServer compact https://technet.microsoft.com/en-us/library/ms174156(v=sql.110).aspx" + nl +
                           "select  'USER_TABLE' AS [Desc], count(*) as [Count] FROM INFORMATION_SCHEMA.TABLES   WHERE TABLE_TYPE='TABLE'" + nl +
                           "union" + nl +
                           "select  'PRIMARY_KEY_CONSTRAINT' AS [Desc], count(*) as [Count] from INFORMATION_SCHEMA.INDEXES where PRIMARY_KEY = 1" + nl +
                           "union" + nl +
                           "select  'FOREIGN_KEY_CONSTRAINT' AS [Desc], count(*) as [Count] from INFORMATION_SCHEMA.TABLE_CONSTRAINTS where CONSTRAINT_TYPE = 'FOREIGN KEY'";
            var dt = DAO.WithConnection(con => con.GetDatatable(request));
            return dt.AsEnumerable().ToDictionary(row => (string)row["Desc"], row => (int)row["Count"]);
        }

        public static void RebuildDB()
        {
            foreach (var query in Config.Generator.Project.SqlModelCreation.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries).Select(query => query.Trim()))
                if (!string.IsNullOrWhiteSpace(query))
                    DAO.WithConnection(con => con.ExecuteNonQuery(query));
            foreach (var query in Config.Generator.Project.SqlDataInitialization.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries).Select(query => query.Trim()))
                if (!string.IsNullOrWhiteSpace(query))
                    DAO.WithConnection(con => con.ExecuteNonQuery(query));
            //DAO.WithConnection(con => con.ExecuteNonQuery(Generator.Project.SqlModelCreation));
            //DAO.WithConnection(con => con.ExecuteNonQuery(Generator.Project.SqlDataInitialization));
        }

        #region Log SQL

        public static void BeginTest(string className)
        {
            GenericDBConnection.RequestBegin += GenericDBConnection_RequestBegin;
            GenericDBConnection.RequestEnd += GenericDBConnection_RequestEnd;
            _sqlRequests.Add(Tuple.Create(className, new List<QueryLog>()));
        }
        public static void EndTest()
        {
            GenericDBConnection.RequestBegin -= GenericDBConnection_RequestBegin;
            GenericDBConnection.RequestEnd -= GenericDBConnection_RequestEnd;
            LogLine(_sqlRequests.Last().Item1 + ": " + _sqlRequests.Last().Item2.Count + " requests have taken " + TotalRequestTimeFor(_sqlRequests.Last().Item2).ToHumanReadableShortNotation());
        }
        static void GenericDBConnection_RequestBegin(object sender, QueryLog log)
        {
            log.TakeOwnership();
            CurrentQueryLog = log;
        }
        static void GenericDBConnection_RequestEnd(object sender, QueryLog log)
        {
            _sqlRequests.Last().Item2.Add(CurrentQueryLog);
        }
        static List<Tuple<string, List<QueryLog>>> _sqlRequests = new List<Tuple<string, List<QueryLog>>>();

        public static QueryLog CurrentQueryLog { get; private set; } // Les tests de visual studio ne sont pas lancé dans des thread separé, donc c'est ok
        public static string AllQueriesLogAsString { 
            get { 
            var nl = Environment.NewLine;
            return _sqlRequests.Select(testCtx => new string('=', 80) + nl + 
                                               (" " + testCtx.Item1 + " ").PadLeftAndRight(80, '=') + nl +
                                               new string('=', 80) + nl +
                                               nl + nl +
                                               testCtx.Item2.Select(log => log.AsString)
                                                            .Join(nl + nl))
                               .Join(nl + nl + nl);
            } 
        }
        public static TimeSpan TotalRequestTime
        {
            get
            {
                return TotalRequestTimeFor(_sqlRequests.SelectMany(testCtx => testCtx.Item2));
            }
        }
        static TimeSpan TotalRequestTimeFor(IEnumerable<QueryLog> logs)
        {
            return logs.Select(log => log.Duration.Value)
                       .DefaultIfEmpty(TimeSpan.Zero)
                       .Sum();
        }
        public static string TotalRequestTimeByTest
        {
            get
            {
                return _sqlRequests.Select(testCtx => testCtx.Item1 + ": " +
                                                      TotalRequestTimeFor(testCtx.Item2).ToHumanReadableShortNotation())
                                   .Join(Environment.NewLine);
            }
        }

        #endregion



        public static bool IsInteractive { get; set; }
        public static void ReadKey(string step)
        {
            if (Config.IsInteractive)
            {
                Console.WriteLine(step + "  (press enter to continue)");
                Console.ReadKey();
            }
        }
    }
}
