﻿using System;
using System.Data;
using System.Linq;
using DAL_Generator.Business;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestProject.Generator;
using TestProject.Model;

namespace TestProject.Tests
{
    // Un seul test par classe !
    [TestClass]
    public class DALTests
    {
        static DAL.GeneratedDAO dao = Config.DAO;

        [ClassInitialize]
        public static void CleanDatabase(TestContext ctx)
        {
            Config.CleanAllTableOfAnyDB();
        }

        [TestInitialize]
        public void CleanDatabase()
        {
            Config.BeginTest(this.GetType().Name);
        }
        [TestCleanup]
        public void EndOfTest()
        {
            Config.EndTest();
        }


        [TestMethod]
        [TestCategory("DAL")]
        public void CreateSqlModel()
        {
            Assert.IsTrue(Config.GetCounts()["USER_TABLE"] == 0);
            foreach(var query in Config.Generator.Project.SqlModelCreation.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries).Select(query => query.Trim()))
                if (!string.IsNullOrWhiteSpace(query))
                    dao.WithConnection(con => con.ExecuteNonQuery(query));
            foreach (var query in Config.Generator.Project.SqlDataInitialization.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries).Select(query => query.Trim()))
                if (!string.IsNullOrWhiteSpace(query))
                    dao.WithConnection(con => con.ExecuteNonQuery(query));
            Assert.IsTrue(Config.GetCounts()["USER_TABLE"] > 0);
        }

        [TestMethod]
        [TestCategory("DAL")]
        public void CleanSqlModel()
        {
            Assert.IsTrue(Config.GetCounts()["USER_TABLE"] > 0);
            foreach (var query in Config.Generator.Project.SqlModelDeletion.Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries).Select(query => query.Trim()))
                if (!string.IsNullOrWhiteSpace(query))
                    dao.WithConnection(con => con.ExecuteNonQuery(query));
            Assert.AreEqual(0, Config.GetCounts()["USER_TABLE"]);
        }

    }
}
