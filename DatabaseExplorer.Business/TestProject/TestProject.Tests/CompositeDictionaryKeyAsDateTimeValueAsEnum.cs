﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using DAL_Generator.Business;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;
using TestProject.Model;
using TestProject.Types;

namespace TestProject.Tests
{
    // Un seul test par classe !
    [TestClass]
    public class CompositeDictionaryKeyAsDateTimeValueAsEnum
    {
        static DAL.GeneratedDAO dao = Config.DAO;
        static readonly string nl = Environment.NewLine;

        [TestInitialize]
        public void CleanDatabase()
        {
            Config.CleanAllTableOfAnyDB();
            Config.RebuildDB();
            Config.BeginTest(this.GetType().Name);
        }
        [TestCleanup]
        public void EndOfTest()
        {
            Config.EndTest();
        }

        [TestMethod]
        public void CreateAuthorAndPlayWithCompositeValueTypeList()
        {
            var idAndState = TestCreation();
            GC.Collect();
            TestEditionAndDelete(idAndState);
        }
        Tuple<long, string> TestCreation()
        {
            var p = new Person() { Firstname = "Someone", Lastname = "SickAllTime" };
            // Simule plusieurs édition désordonnés
            p.History.Add(new DateTime(2016, 5, 1), eDayTask.WorkFreeDay);
            p.History.Add(new DateTime(2016, 5, 2), eDayTask.WorkDay);
            p.History.Add(new DateTime(2016, 5, 3), eDayTask.WorkDay);
            p.History.Add(new DateTime(2016, 5, 4), eDayTask.WorkDay);
            p.History.Add(new DateTime(2016, 5, 5), eDayTask.WorkDay);
            p.History[new DateTime(2016, 5, 6)] = eDayTask.WorkDay;
            p.History.Add(new DateTime(2016, 5, 7), eDayTask.WorkDay);
            p.History.Remove(new DateTime(2016, 5, 7));
            p.History.Remove(new DateTime(2016, 5, 1));
            p.History[new DateTime(2016, 5, 7)] = eDayTask.WorkDay;
            p.History.Remove(new DateTime(2016, 5, 7));
            p.History[new DateTime(2016, 5, 7)] = eDayTask.WorkDay;
                                                                        Assert.AreEqual(p.State, eState.New);
                                                                        Assert.AreEqual(p.History.State, eState.Unsynchronized);
            p.SynchronizeToDatabase();
                                                                        Assert.AreEqual(p.State, eState.Synchronized);
                                                                        Assert.AreEqual(p.History.State, eState.Synchronized);
                                                                        GC.Collect(3, GCCollectionMode.Forced, true);
                                                                        foreach (var dalObj in CacheManager.Instance.GetObjectsInMemory<TestProject.DAL.Dbo.Person_History>())
                                                                            Assert.AreEqual(dalObj.State, eState.Synchronized);

            p.History[new DateTime(2016, 5, 5)] = eDayTask.Sick;
                                                                        Assert.AreEqual(p.State, eState.Unsynchronized);
                                                                        Assert.AreEqual(p.History.State, eState.Unsynchronized);
            p.History.Remove(new DateTime(2016, 5, 5));
            p.History.Remove(new DateTime(2016, 5, 6));
            p.History.Add(new DateTime(2016, 5, 5), eDayTask.WorkFreeDay);
                                                                        Assert.AreEqual(p.State, eState.Unsynchronized);
                                                                        Assert.AreEqual(p.History.State, eState.Unsynchronized);
            p.SynchronizeToDatabase();
                                                                        Assert.AreEqual(p.State, eState.Synchronized);
                                                                        Assert.AreEqual(p.History.State, eState.Synchronized);
                                                                        Assert.AreSame(p, CacheManager.Instance.Get<Person>(new IdTuple<long>(p.Id)));
            return Tuple.Create(p.Id, p.Dump());
        }

        void TestEditionAndDelete(Tuple<long, string> idAndState)
        {
            GC.Collect(3, GCCollectionMode.Forced, true);
            Assert.IsNull(CacheManager.Instance.Get<Person>(new IdTuple<long>(idAndState.Item1), true));
            var p = CacheManager.Instance.Get<Person>(new IdTuple<long>(idAndState.Item1));
                                                                        Assert.IsNotNull(p);

                                                                        Assert.AreEqual(idAndState.Item2, p.Dump());
            p.Delete(); // history continet toujours les elements supprimés car c'est l'objet tout entier qu'on a supprimé
                                                                        Assert.AreEqual(p.State, eState.Deleted);
                                                                        Assert.AreEqual(p.History.State, eState.Deleted);
            p = null;
                                                                        GC.Collect(3, GCCollectionMode.Forced, true);
                                                                        Assert.AreEqual(0, CacheManager.Instance.GetObjectsInMemory<TestProject.DAL.Dbo.Person_History>().Count());
        }
    }
}
