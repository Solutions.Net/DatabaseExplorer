﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Generator.Business;
using DAL_Generator.Model;
using TechnicalTools.Model;

namespace TestProject.Generator
{
    public class TestProjectGenerator
    {
        public string projectName = "TestProject";
        public string dalSubNamespace = "DAL";
        public string modelSubNamespace = "Model";
        public string schemaContainingData = "dbo";

        public ModelImporter        Importer { get; private set; }
        public ModelUML             Model    { get; private set; }
        public FullGeneratedProject Project  { get; private set; }
        public bool ForSqlCE                 { get; set; }

        public void Build(bool writeGenerated = false)
        {
            // Importe le model UML
            Importer = new ModelImporter();
            Model = Importer.RunImport(typeof(TestProject.Design.Book).Assembly, schemaContainingData);

            // Genere le Model SQL
            Project = new FullGeneratedProject(Model);
            Project.RunImport();
            Project.DbInfos.SqlDbType = ForSqlCE ? eSqlDbType.SqlServerCompactEdition : eSqlDbType.SqlServer;
            Project.DbInfos.Schemas[0].Name = schemaContainingData;
            Project.SqlModelCreation = Project.GenerateSqlCodeCreation();
            Project.SqlModelDeletion = Project.GenerateSqlCodeDeletion();
            Project.SqlDataInitialization = Project.GenerateSqlDataInitialization();

            // Genere la partie DAL
            // Genere la partie Business
            //DynamicTypeModelGenerator
            // TODO : faut pas générer de table ICompétence !
            Project.GenerateModelSourceCodeFilesForKronos(Model, projectName + "." + modelSubNamespace,
                                                          projectName + "." + dalSubNamespace,
                                                          schemaContainingData,
                                                          oneFilePerClass: true,
                                                          navigationProperties: true
                //,entitySpecificBaseClass: typeof(TestProject.DAL.KronosGeneratedEntity<>)
                                                          );
            if (writeGenerated)
            {
                WriteGeneratedDALFilesToFolder();
                WriteGeneratedModelFilesToFolder();
            }
        }
        void WriteGeneratedDALFilesToFolder()
        {
            string destFolder = @"..\..\..\" + projectName + "." + dalSubNamespace + @"\Generated"; // remonte Kronos/bin/debug puis va dans Kronos.DAL/Generated
            string entitiesFolder = destFolder + @"\Entities";
            Directory.CreateDirectory(entitiesFolder);
            if (Directory.EnumerateFiles(destFolder, "*.cs").Any(file => new FileInfo(file).IsReadOnly))
                throw new UserUnderstandableException("Vous devez faire un checkout de tous les fichiers du répertoire TestProject.DAL/Generated avant de regénérer les fichiers cs !", null);
            foreach (var kvp in Project.GeneratedSourceCode)
            {
                var folder = new [] { "GeneratedDALEntity.cs", "GeneratedDAO.cs" }.Contains(kvp.Key) ? destFolder : entitiesFolder;
                string filename = Path.Combine(folder, kvp.Key);
                File.WriteAllText(filename, kvp.Value);
            }
        }

        void WriteGeneratedModelFilesToFolder()
        {
            string destFolder = @"..\..\..\" + projectName + "." + modelSubNamespace + @"\Generated"; // remonte Kronos/bin/debug puis va dans Kronos.DAL/Generated
            if (Directory.EnumerateFiles(destFolder, "*.cs").Any(file => new FileInfo(file).IsReadOnly))
                throw new UserUnderstandableException("Vous devez faire un checkout de tous les fichiers du répertoire Kronos.DAL/Generated avant de regénérer les fichiers cs !", null);

            foreach (var kvp in Project.ModelClasses)
            {
                string filename = Path.Combine(destFolder, kvp.Key);
                File.WriteAllText(filename, kvp.Value);
            }
        }

    }
}
