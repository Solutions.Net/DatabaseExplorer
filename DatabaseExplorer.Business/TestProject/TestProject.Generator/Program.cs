﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL_Generator.Business;
using TechnicalTools.Model;

namespace TestProject.Generator
{
    class Program
    {
        static void Main(string[] args)
        {
            var generator = new TestProjectGenerator() { ForSqlCE = true };
            generator.Build(true);

            Console.WriteLine("Done !");
            Console.WriteLine("(Press any key to close)");
            Console.ReadKey();
        }

       

    }

    [Serializable]
        /*[AutoAddInstanceToDatabase]*/
    public class ExcelDataLocation //: PropertyNotifierObject//, IDbObject
    {
        /// <summary> Nom de l'onglet dans excel contenant les informations </summary>
        /*[ExportableProperty] */public string TabTitle         { get; set; }
        /// <summary> Encadrement des titres de colonne au format : "D3:AC3"</summary>
        /*[ExportableProperty] */public string ColumnTitleRange { get; set; }
        /// <summary> Encadrement des titre de ligne au format : "B4:C56" (cet exemple montre que deux colonne servent a stocker les titre de ligne),</summary>
        /*[ExportableProperty] */public string LineTitleRange   { get; set; }
        /// <summary> Encadrement des donnés du tableau sans les titres</summary>
        /*[ExportableProperty] */public string DataRange        { get; set; }

        public ExcelDataLocation() { }
    }

    

}
