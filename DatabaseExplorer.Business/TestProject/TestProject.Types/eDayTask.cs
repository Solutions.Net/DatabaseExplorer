﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject.Types
{
    [Flags]
    public enum eDayTask
    {
        WorkDay = 1,
        WorkFreeDay = 2,
        Sick = 4,
        NoLuck = WorkFreeDay | Sick,

    }
}
