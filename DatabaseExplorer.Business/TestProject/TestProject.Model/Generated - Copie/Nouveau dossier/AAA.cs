using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TestProject.DAL;
using TestProject.DAL.Dbo;

namespace TestProject.Model
{
    public partial class AAA : BusinessObjectGenerated
    {
        public readonly CompositeDictionary<TimeSpan, TestProject.Design.FlagTests> Dico = new CompositeDictionary<TimeSpan, TestProject.Design.FlagTests>() { RememberDeletedObjects = false, ConsiderDirtyOnAddRemoveEvents = false, ConsiderDirtyOnItemChanged = false };
               readonly CompositeDictionary<TimeSpan, TestProject.DAL.Dbo.AAA_Dico> _Dico = new CompositeDictionary<TimeSpan, TestProject.DAL.Dbo.AAA_Dico>() { RememberDeletedObjects = true, ConsiderDirtyOnAddRemoveEvents = true, ConsiderDirtyOnItemChanged = true };
                        bool _Dico_reloading;

        public override long Id { get { return DataSource.Id_AAA; } }
        public override bool IsModified
        {
            get
            {
                return IsModified ||
                       _Dico.State != eState.Synchronized;
                // TODO : Casse couille pour les structures ! => Modifier CompositeList.DeletedObject en dictionaire (ca ne regle pas le probleme des list pouvant avoir des doublon) Et faire une version de CompositeListpour les structs !
            }
        }
        protected internal override string Dump(bool all)
        {
            if (!all && (State == eState.New || State == eState.Synchronized))
                return base.Dump(all);
            return new[]
            {
                base.Dump(all),
                IBusinessStructGenerated_Extensions.DisplayDiff(_Dico, all)
            }.NotBlank().Join(ChangeIndent);
        }

        protected override Type ConcreteDataSourceType { get { return typeof(TestProject.DAL.Dbo.AAA); } }
        private TestProject.DAL.Dbo.AAA DataSource { get; set; }
        protected override IGeneratedDALEntity GetDataSource() { return DataSource; }
        protected override List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null)
        {
            lst = base.GetDataSources(lst);
            lst.Add(DataSource);
            return lst;
        }


        public AAA() { }
        public AAA(long id) : base(id) { }


        protected override void Init()
        {
            base.Init();
            _Dico.LoadItems = () => LoadComposites_Dico().ToDictionary(item => item.Key, item => item);
            Dico.LoadItems = () => _Dico.ToDictionary(lnk => lnk.Key, lnk => (TestProject.Design.FlagTests)lnk.Value.Dico_Id);
            Dico.UpdateState(eState.Synchronized);
            _Dico.UpdateState(eState.Synchronized);
            Dico.CollectionChanged += BuildDictionaryCollectionChangedHandler(() => _Dico_reloading, Dico, _Dico,
                                          kvp => new TestProject.DAL.Dbo.AAA_Dico() { Owner_Id = Id, Key = kvp.Key, Dico_Id = (Int32)kvp.Value });
            AddDirtynessRelayFor(GetMemberName.For<AAA>(d => d._Dico), _Dico);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void CreateDataSource() { base.CreateDataSource(); DoCreateDataSource(); }
        private void DoCreateDataSource()
        {
            DataSource = new TestProject.DAL.Dbo.AAA(true);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void LoadDataSource(long id) { base.LoadDataSource(id); DoLoadDataSource(id); }
        private void DoLoadDataSource(long id)
        {
            // Le Load ne modifie pas les valeurs, il se contente de reconstruire la hierarchie de datasource
            DataSource = CacheManager.Instance.Get<TestProject.DAL.Dbo.AAA>(new IdTuple<long>(id));
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void ResetDataSourceEdits() { base.ResetDataSourceEdits(); DoResetDataSourceEdits(DataSource); }
        private void DoResetDataSourceEdits(TestProject.DAL.Dbo.AAA ds)
        {
            _Dico.Reload();
            _Dico_reloading = true;
            try { Dico.Reload(); }
            finally { _Dico_reloading = false; }
            Debug.Assert(_Dico.State == eState.Synchronized);
            Debug.Assert(Dico.State == eState.Synchronized);
        }
        private IEnumerable<TestProject.DAL.Dbo.AAA_Dico> LoadComposites_Dico() { return LoadComposites_Dico(false); }
        private IEnumerable<TestProject.DAL.Dbo.AAA_Dico> LoadComposites_Dico(bool preventLoading)
        {
            if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))
                return new TestProject.DAL.Dbo.AAA_Dico[0];
            return TestProject.DAL.Dbo.AAA_Dico.GetEntitiesWhere("Owner_Id = " + this.Id);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void SaveDataSourceEdits() { base.SaveDataSourceEdits(); DoSaveDataSourceEdits(DataSource); }
        private void DoSaveDataSourceEdits(TestProject.DAL.Dbo.AAA ds)
        {
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void SaveScalarComposites() { base.SaveScalarComposites(); DoSaveScalarComposites(DataSource); }
        private void DoSaveScalarComposites(TestProject.DAL.Dbo.AAA ds)
        {
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void SaveLinkComposites() { base.SaveLinkComposites(); DoSaveLinkComposites(); }
        private void DoSaveLinkComposites()
        {
            foreach (var dalKvp in _Dico.DeletedObjects.Values)
                dalKvp.SynchronizeToDatabase();
            foreach (var dalKvp in _Dico.Values)
                dalKvp.SynchronizeToDatabase();
            _Dico.AcceptChanges();
            Dico.AcceptChanges();
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteLinkComposites() { DoDeleteLinkComposites(); base.DeleteLinkComposites(); }
        private void DoDeleteLinkComposites()
        {
            foreach (var dalKvp in _Dico.DeletedObjects.Values)
                dalKvp.SynchronizeToDatabase();
            foreach (var dalKvp in _Dico.Values)
                dalKvp.Delete();
            _Dico.UpdateState(eState.Deleted);
            Dico.UpdateState(eState.Deleted);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteScalarComposites() { DoDeleteScalarComposites(DataSource); base.DeleteScalarComposites(); }
        private void DoDeleteScalarComposites(TestProject.DAL.Dbo.AAA ds)
        {
        }
    }
}
