using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TestProject.DAL;
using TestProject.DAL.Dbo;

namespace TestProject.Model
{
    public partial class Person : BusinessObjectGenerated
    {
        public String Firstname { get { return _Firstname; } set { SetProperty(ref _Firstname, value); } } String _Firstname;        public String Lastname { get { return _Lastname; } set { SetProperty(ref _Lastname, value); } } String _Lastname;        public Address Address { get { return _Address; } } Address _Address;        public Address Address2 { get { return _Address2; } set { if (value == _Address2) return;
                                                                  if (value.State != eState.New) throw new Exception("Value to set for Address2 property must be an object with \"New\" State");
                                                                  SetProperty(ref _Address2, value); } } Address _Address2;        public readonly CheckableCompositeListGenerated<Person> Friends = new CheckableCompositeListGenerated<Person>() { RememberDeletedObjects = true, ConsiderItemAsComposite = false, ConsiderDirtyOnItemChanged = false, ConsiderDirtyOnAddRemoveEvents = true };        public readonly CompositeDictionary<DateTime, TestProject.Types.eDayTask> History = new CompositeDictionary<DateTime, TestProject.Types.eDayTask>() { RememberDeletedObjects = false, ConsiderDirtyOnAddRemoveEvents = true, ConsiderDirtyOnItemChanged = true };
               readonly CompositeDictionary<DateTime, TestProject.DAL.Dbo.Person_History> _History = new CompositeDictionary<DateTime, TestProject.DAL.Dbo.Person_History>() { RememberDeletedObjects = true, ConsiderDirtyOnAddRemoveEvents = true, ConsiderDirtyOnItemChanged = true };
                        bool _History_reloading;

        public override long Id { get { return DataSource.Id_Person; } }
        public override bool IsModified
        {
            get
            {
                return IsModified ||
                       _Firstname != DataSource.Firstname ||
                       _Lastname != DataSource.Lastname ||
                       (_Address == null ? (long?)null : _Address.Id) != DataSource.Address_Id ||
                       (_Address2 == null ? (long?)null : _Address2.Id) != DataSource.Address2_Id ||
                       Friends.State != eState.Synchronized ||
                       _History.State != eState.Synchronized;
                // TODO : Casse couille pour les structures ! => Modifier CompositeList.DeletedObject en dictionaire (ca ne regle pas le probleme des list pouvant avoir des doublon) Et faire une version de CompositeListpour les structs !
            }
        }
        protected internal override string Dump(bool all)
        {
            if (!all && (State == eState.New || State == eState.Synchronized))
                return base.Dump(all);
            return new[]
            {
                base.Dump(all),
                all || _Firstname != DataSource.Firstname ? "Firstname: " + (_Firstname ?? (object)"").ToString() : "",
                all || _Lastname != DataSource.Lastname ? "Lastname: " + (_Lastname ?? (object)"").ToString() : "",
                all || (_Address == null ? (long?)null : _Address.Id) != DataSource.Address_Id ? "Address: " + (_Address == null ? "" : _Address.Id.ToString()) : "",
                all || (_Address2 == null ? (long?)null : _Address2.Id) != DataSource.Address2_Id ? "Address2: " + (_Address2 == null ? "" : _Address2.Id.ToString()) : "",
                Friends.DeletedObjects.Concat(Friends).Select(f => (f.State.GetDescription() + f.TypedId.ToSmartString()).Replace(nl, nl + "     ").PrefixIfNotEmptyWith("   - ")).NotBlank().Join(nl).Replace(nl, ChangeIndent).PrefixIfNotEmptyWith("Friends:" + ChangeIndent),
                IBusinessStructGenerated_Extensions.DisplayDiff("History", _History, all)
            }.NotBlank().Join(ChangeIndent);
        }

        protected override Type ConcreteDataSourceType { get { return typeof(TestProject.DAL.Dbo.Person); } }
        private TestProject.DAL.Dbo.Person DataSource { get; set; }
        protected override IGeneratedDALEntity GetDataSource() { return DataSource; }
        protected override List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null)
        {
            lst = base.GetDataSources(lst);
            lst.Add(DataSource);
            return lst;
        }


        public Person() { }
        public Person(long id) : base(id) { }


        protected override void Init()
        {
            base.Init();
            AddDirtynessRelayFor(GetMemberName.For<Person>(x => x.Address), null);
            AddDirtynessRelayFor(GetMemberName.For<Person>(x => x.Address2), null);
            Friends.LoadItems = LoadComposites_Friends;
            Friends.UpdateState(eState.Synchronized);
            AddDirtynessRelayFor(GetMemberName.For<Person>(f => f.Friends), Friends);
            _History.LoadItems = () => LoadComposites_History().ToDictionary(item => item.Key, item => item);
            History.LoadItems = () => _History.ToDictionary(lnk => lnk.Key, lnk => (TestProject.Types.eDayTask)lnk.Value.History_Id);
            History.UpdateState(eState.Synchronized);
            _History.UpdateState(eState.Synchronized);
            History.CollectionChanged += BuildDictionaryCollectionChangedHandler(() => _History_reloading, History, _History,
                                          kvp => new TestProject.DAL.Dbo.Person_History() { Owner_Id = Id, Key = kvp.Key, History_Id = (Int32)kvp.Value });
            AddDirtynessRelayFor(GetMemberName.For<Person>(h => h._History), _History);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void CreateDataSource() { base.CreateDataSource(); DoCreateDataSource(); }
        private void DoCreateDataSource()
        {
            DataSource = new TestProject.DAL.Dbo.Person(true);
            if (_Address == null || _Address.State != eState.New)
            {
                _Address = new Address();
                DataSource.Address_Id = _Address.Id;
            }
                        _Address2 = null;
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void LoadDataSource(long id) { base.LoadDataSource(id); DoLoadDataSource(id); }
        private void DoLoadDataSource(long id)
        {
            // Le Load ne modifie pas les valeurs, il se contente de reconstruire la hierarchie de datasource
            DataSource = CacheManager.Instance.Get<TestProject.DAL.Dbo.Person>(new IdTuple<long>(id));
            AssignWithDirtynessRelay(ref _Address, CacheManager.Instance.Get<Address>(new IdTuple<long>(DataSource.Address_Id)));
            AssignWithDirtynessRelay(ref _Address2, DataSource.Address2_Id == null ? null : CacheManager.Instance.Get<Address>(new IdTuple<long>(DataSource.Address2_Id.Value)));
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void ResetDataSourceEdits() { base.ResetDataSourceEdits(); DoResetDataSourceEdits(DataSource); }
        private void DoResetDataSourceEdits(TestProject.DAL.Dbo.Person ds)
        {
            _Firstname = ds.Firstname;
            _Lastname = ds.Lastname;
            AssignWithDirtynessRelay(ref _Address, ResetDataSourceEditsRecursive(CacheManager.Instance.Get<Address>(new IdTuple<long>(ds.Address_Id)))) /* because there is no setter , so no "SetProperty" that would trigger dirtyness relay to update */;
            AssignWithDirtynessRelay(ref _Address2, ResetDataSourceEditsRecursive(ds.Address2_Id == null ? null : CacheManager.Instance.Get<Address>(new IdTuple<long>(ds.Address2_Id.Value)))) /* because there is no setter , so no "SetProperty" that would trigger dirtyness relay to update */;
            Friends.Reload();
            _History.Reload();
            _History_reloading = true;
            try { History.Reload(); }
            finally { _History_reloading = false; }
            Debug.Assert(_History.State == eState.Synchronized);
            Debug.Assert(History.State == eState.Synchronized);
        }
        private IEnumerable<Person> LoadComposites_Friends() { return LoadComposites_Friends(false); }
        private IEnumerable<Person> LoadComposites_Friends(bool preventLoading)
        {
            if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))
                return new Person[0];
            // TODO : A optimiser : on pourrait faire une seule requete au lieu de deux
            var links = TestProject.DAL.Dbo.Person_Friend.GetEntitiesWhere("Person_Id = " + this.Id);
            var ids = links.Select(lnk => new IdTuple<long>(lnk.Friend_Id));
            var dalObjects = TestProject.DAL.Dbo.Person.GetEntitiesWithIds(ids);
            return CacheManager.Instance.GetAll<Person>(ids.Cast<IIdTuple>(), preventLoading);
        }
        private IEnumerable<TestProject.DAL.Dbo.Person_History> LoadComposites_History() { return LoadComposites_History(false); }
        private IEnumerable<TestProject.DAL.Dbo.Person_History> LoadComposites_History(bool preventLoading)
        {
            if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))
                return new TestProject.DAL.Dbo.Person_History[0];
            return TestProject.DAL.Dbo.Person_History.GetEntitiesWhere("Owner_Id = " + this.Id);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void SaveDataSourceEdits() { base.SaveDataSourceEdits(); DoSaveDataSourceEdits(DataSource); }
        private void DoSaveDataSourceEdits(TestProject.DAL.Dbo.Person ds)
        {
            ds.Firstname = _Firstname;
            ds.Lastname = _Lastname;
            ds.Address_Id = _Address.Id;
            ds.Address2_Id = _Address2 == null ? (long?)null : _Address2.Id;
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void SaveScalarComposites() { base.SaveScalarComposites(); DoSaveScalarComposites(DataSource); }
        private void DoSaveScalarComposites(TestProject.DAL.Dbo.Person ds)
        {
            Address.SynchronizeToDatabase();
            if (Address2 != null)
                Address2.SynchronizeToDatabase();
            if (Friends.DeletedObjects.Count > 0)
            {
                var links = TestProject.DAL.Dbo.Person_Friend.GetEntitiesWhere("Person_Id = " + this.Id + " AND Friend_Id in (" + Friends.DeletedObjects.Select(f => f.Id).Join() + ")");
                foreach (var link in links)
                    link.Delete();
            }
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void SaveLinkComposites() { base.SaveLinkComposites(); DoSaveLinkComposites(); }
        private void DoSaveLinkComposites()
        {
            foreach (var f in Friends)
            {
                var link = new TestProject.DAL.Dbo.Person_Friend() { Person_Id = Id, Friend_Id = f.Id };
                link.SynchronizeToDatabase();
            }
            Friends.DeletedObjects.Clear();
            Friends.UpdateState();
            foreach (var dalKvp in _History.DeletedObjects.Values)
                dalKvp.SynchronizeToDatabase();
            foreach (var dalKvp in _History.Values)
                dalKvp.SynchronizeToDatabase();
            _History.AcceptChanges();
            History.AcceptChanges();
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteLinkComposites() { DoDeleteLinkComposites(); base.DeleteLinkComposites(); }
        private void DoDeleteLinkComposites()
        {
            var linksFriends = TestProject.DAL.Dbo.Person_Friend.GetEntitiesWhere("Person_Id = " + this.Id);
            foreach (var link in linksFriends)
                link.Delete();
            foreach (var dalKvp in _History.DeletedObjects.Values)
                dalKvp.SynchronizeToDatabase();
            foreach (var dalKvp in _History.Values)
                dalKvp.Delete();
            _History.UpdateState(eState.Deleted);
            History.UpdateState(eState.Deleted);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteScalarComposites() { DoDeleteScalarComposites(DataSource); base.DeleteScalarComposites(); }
        private void DoDeleteScalarComposites(TestProject.DAL.Dbo.Person ds)
        {
            if (Address != null)
                Address.UpdateState(eState.ToDelete);
            if (Address2 != null)
                Address2.UpdateState(eState.ToDelete);
            Friends.Clear(); // ==> passe tous les objets dans Friends.DeletedObjects
            DoSaveScalarComposites(ds);
        }
    }
}
