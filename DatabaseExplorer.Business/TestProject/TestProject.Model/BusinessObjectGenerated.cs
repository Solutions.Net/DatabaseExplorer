﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenericDALBasics;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Model.Composition;
using TestProject.DAL;


namespace TestProject.Model
{
    // TODO : https://fr.wikipedia.org/wiki/Mémento_(patron_de_conception)
    public abstract class BusinessObjectGenerated : Entity<IdTuple<long>>, IBusinessObjectGenerated
    {
        protected abstract Type ConcreteDataSourceType { get; }
        protected abstract IGeneratedDALEntity GetDataSource();
        protected virtual List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null) // GetDataSources().Last() == GetDataSource()
        {
            return lst ?? new List<IGeneratedDALEntity>();
        }

        public abstract long Id          { get; }       long IBusinessObjectGenerated.Id { get { return Id; } }
        public virtual bool IsModified   { get { return State.HasPendingChanges(); } }
        public string Changes            { get { return Dump(false); } }
        public string Dump()             { return Dump(true); }
        public string ToJSON()           { return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(this); } // Include in project System.Web.Extensions // TODO : voir http://stackoverflow.com/a/26406504

        protected internal virtual string Dump(bool all)
        {
            return all ? TypedId.ToSmartString() + " " + State.GetDescription()
                 : State == eState.Deleted || State == eState.Synchronized
                 ? string.Empty
                 : State.GetDescription() + (State == eState.ToDelete
                                            ? " " + TypedId.ToSmartString()
                                            : string.Empty);
        }

        protected static readonly string ChangeIndent = Environment.NewLine + "   ";

        public BusinessObjectGenerated(long id)
        {
            //InitDirtynessRelays();
            Init();
            LoadById(id);
        }
        protected BusinessObjectGenerated()
        {
            //InitDirtynessRelays();
            Init();
            CreateDataSource();
            ResetDataSourceEdits();
            InitializeState(eState.New);
            RaiseIdChanged();
        }
        protected virtual void Init() { }
        //void InitDirtynessRelays()
        //{
        //    if (DirtynessRelays != null)
        //        foreach (var prop in DirtynessRelays)
        //            AddDirtynessRelayFor(prop, null);
        //}
        //protected static readonly string[] _DirtynessRelays = new string[] { };
        protected void AssignWithDirtynessRelay<T>(ref T backField, T newValue)
            where T : class, IPropertyNotifierObject
        {
            if (backField != null)
                ((IPropertyNotifierObject)backField).DirtyChanged -= SubObject_DirtyChanged;
            backField = newValue;
            if (newValue != null)
                ((IPropertyNotifierObject)newValue).DirtyChanged += SubObject_DirtyChanged;
        }


        public void LoadById(long id)
        {
            bool idChanged = GetDataSource() == null || id != Id;
            LoadDataSource(id);
            ResetDataSourceEdits();
            InitializeState(eState.Synchronized);
            if (idChanged)
                RaiseIdChanged();
        }
        public override void SynchronizeToDatabase()
        {
            if (State == eState.Deleted || State == eState.Synchronized)
                return;
            if (State == eState.New || State == eState.Unsynchronized)
            {
                SaveDataSourceEdits(); // Sauvegarde la Datasource (qui peut referencer les composites scalar)
                SaveScalarComposites(); // Sauvegarde les relations 1 - 1
                foreach (var ds in GetDataSources())
                    ds.SynchronizeToDatabase();
                SaveLinkComposites(); // Sauvegarde les links (table de jointure, relation n-n ou 1-n) qui referencent les composants scalar et this
            }
            else if (State == eState.ToDelete)
            {
                DeleteLinkComposites();
                foreach (var ds in GetDataSources().Reverse<IGeneratedDALEntity>())
                    ds.Delete();
                DeleteScalarComposites(); // Ce sont les ds qui reference les objets composites
            }
            UpdateState();
        }

        protected virtual string[] DirtynessRelays { get { return null; } }
        protected virtual void CreateDataSource() { }
        protected virtual void LoadDataSource(long id) { }
        // Copie les données du/des datasource(s) vers l'objet métier
        protected virtual void ResetDataSourceEdits() { }  void IBusinessObjectGenerated.ResetDataSourceEdits() { ResetDataSourceEdits(); } 
        protected static     T ResetDataSourceEditsRecursive<T>(T bo) where T : IBusinessObjectGenerated { if (bo != null) bo.ResetDataSourceEdits(); return bo; } // Permet d'appeler une methode sur un expression inline
        // Copie les données de l'objet metier vers le/les datasource(s)
        // Copie les données de l'objet métier vers le(s) datasource(s)
        protected virtual void SaveDataSourceEdits() { } void IBusinessObjectGenerated.SaveDataSourceEdits() { SaveDataSourceEdits(); } 
        protected static     T SaveDataSourceEditsRecursive<T>(T bo) where T : IBusinessObjectGenerated { if (bo != null) bo.SaveDataSourceEdits(); return bo; } // Permet d'appeler une methode sur un expression inline
        protected virtual void SaveScalarComposites() { }
        protected virtual void SaveLinkComposites() { }
        protected virtual void DeleteScalarComposites() { }
        protected virtual void DeleteLinkComposites() { }
        




        protected override IdTuple<long>? ClosedId { get { return Id == 0 ? (IdTuple<long>?)null : new IdTuple<long>(Id); }
                                                     set { Debug.Assert(Id == (long)value.Value.Keys[0]); } }
        public sealed override void LoadById(IdTuple<long> id)
        {
            LoadById((long)id.Keys[0]);
        }
        public override void Reload()
        {
            CancelChanges();
        }
        public void Delete()
        {
            if (State == eState.Deleted)
                return;
            UpdateState(eState.ToDelete);
            SynchronizeToDatabase();
        }

        protected static readonly string nl = Environment.NewLine;

        public static string DumpAllObjectsInMemory(List<object> potentialObjs = null)
        {
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            return DumpAllDALObjectsInMemory(objs)
                 + Environment.NewLine
                 + DumpAllBusinessObjectsInMemory(objs);
        }
        public static string DumpAllDALObjectsInMemory(List<object> potentialObjs = null)
        {
            var sb = new StringBuilder();
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            sb.AppendLine("DAL Objects:");
            foreach (var obj in objs)
            {
                var de = obj as IGeneratedDALEntity;
                if (de == null)
                    continue;
                sb.Append("   ");
                sb.AppendLine(de.TypedId.AsDebugString);
            }
            return sb.ToString();
        }
        public static string DumpAllBusinessObjectsInMemory(List<object> potentialObjs = null)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Business Objects:");
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            foreach (var obj in objs)
            {
                var bo = obj as BusinessObjectGenerated;
                if (bo == null)
                    continue;
                sb.Append("   ");
                sb.AppendLine(bo.TypedId.AsDebugString);
            }
            return sb.ToString();
        }
        protected static T GetObjectImplementingAmong<T>(long id, params Type[] types)
        {
            foreach (var t in types)
                try { return (T)CacheManager.Instance.Get(new TypedId<long>(t, id)); }
                catch { }
            throw new Exception("Not found !");
        }
        protected static NotifyCollectionChangedEventHandler BuildDictionaryCollectionChangedHandler<TKey, TValue, TDalObject>(Func<bool> isReloading, IDictionary<TKey, TValue> Dico, IDictionary<TKey, TDalObject> _Dico, Func<KeyValuePair<TKey, TValue>, TDalObject> createDalObject)
            where TDalObject : GeneratedDALEntity<IdTuple<long>>
        {
            return (_, e) =>
            {
                if (isReloading())
                    return;
                if (e.Action == NotifyCollectionChangedAction.Add)
                    foreach (KeyValuePair<TKey, TValue> kvp in e.NewItems)
                        _Dico.Add(kvp.Key, createDalObject(kvp));
                else if (e.Action == NotifyCollectionChangedAction.Remove)
                    foreach (KeyValuePair<TKey, TValue> kvp in e.OldItems)
                        _Dico.Remove(kvp.Key);
                else if (e.Action == NotifyCollectionChangedAction.Replace)
                {
                    foreach (KeyValuePair<TKey, TValue> kvp in e.OldItems)
                        _Dico.Remove(kvp.Key); // delete before add
                    foreach (KeyValuePair<TKey, TValue> kvp in e.NewItems)
                        _Dico.Add(kvp.Key, createDalObject(kvp));
                }
                else if (e.Action == NotifyCollectionChangedAction.Reset)
                {
                    if (Dico.Count == 0)
                        _Dico.Clear();
                    else
                        Debug.Assert(false, "todo !");
                }
            };
        }
    }
    public interface IBusinessStructGenerated
    {
        string AsDebugString { get; }
    }
    public static class IBusinessStructGenerated_Extensions
    {
        public static string DisplayDiff<T>(CheckableCompositeListGenerated<T> links, bool all)
            where T : IGeneratedDALEntity
        {
            Debug.Assert(typeof(T).TryGetNullableType() == null); // T n'est pas nullable
            var sb = new StringBuilder();
            Func<T, string> display = v => typeof(IBusinessStructGenerated).IsAssignableFrom(typeof(T)) ? ((IBusinessStructGenerated)v).AsDebugString : v.ToString();
            foreach (T link in links.DeletedObjects.Concat(links))
                if (link.State.NotIn(eState.Synchronized, eState.Deleted) || all)
                    sb.AppendLine("   - " + link.State + " " + link.TypedId.AsDebugString);
            return sb.ToString();
        }
        public static string DisplayDiff<TKey, TValue>(string propName, CompositeDictionary<TKey, TValue> dico, bool all)
            where TValue : IGeneratedDALEntity
        {
            Debug.Assert(typeof(TValue).TryGetNullableType() == null); // TValue n'est pas nullable
            var sb = new StringBuilder();
            Func<TValue, string> display = v => typeof(IBusinessStructGenerated).IsAssignableFrom(typeof(TValue)) ? ((IBusinessStructGenerated)v).AsDebugString : v.ToString();
            foreach (var kvp in dico.DeletedObjects.OrderBy(kvp => kvp.Key).Concat(dico.OrderBy(kvp => kvp.Key))) // Tri par clef pour avoir des dump deterministe
                if (kvp.Value.State.NotIn(eState.Synchronized, eState.Deleted) || all)
                    sb.AppendLine("   - " + kvp.Value.State + " " + kvp.Value.TypedId.AsDebugString);
            if (sb.Length > 0)
                return propName + ": " + Environment.NewLine + sb.ToString();
            return sb.ToString();
        }
        //public static string DisplayDiff<T>(List<T> values, IEnumerable<T> before_)
        //    where T : struct
        //{
        //    Debug.Assert(typeof(T).TryGetNullableType() == null); // T n'est pas nullable
        //    var sb = new StringBuilder();
        //    var before = before_.ToList();
        //    Func<T, string> display = v => typeof(IBusinessStructGenerated).IsAssignableFrom(typeof(T)) ? ((IBusinessStructGenerated)v).AsString : v.ToString();
        //    for (int v = 0; v < values.Count; ++v)
        //    {
        //        bool found = false;
        //        for (int b = 0; b < before.Count; ++b)
        //            if (object.Equals(values[v], before[b]))
        //            {
        //                sb.AppendLine("[=] " + display(values[v]));
        //                values.RemoveAt(v);
        //                --v;
        //                found = true;
        //                break;
        //            }
        //        if (!found)
        //            sb.AppendLine("[+] " + display(values[v]));
        //    }
        //    for (int b = 0; b < before.Count; ++b)
        //        sb.AppendLine("[-] " + display(before[b]));
        //    return sb.ToString();
        //}
    }


    public static class EnumConversionExtensions
    {
        public static TEnum? ToEnum<TEnum>(this int? v)
            where TEnum : struct
        {
            if (v.HasValue)
                return (TEnum)(object)v.Value;
            return null;
        }

    }
}
