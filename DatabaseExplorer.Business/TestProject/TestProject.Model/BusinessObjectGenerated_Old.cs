using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Model.Composition;

using TestProject.DAL;


namespace TestProject.Model
{
    // TODO : https://fr.wikipedia.org/wiki/M�mento_(patron_de_conception)
    public abstract class BusinessObjectGenerated : Entity<IdTuple<long>>
    {
        protected abstract Type ConcreteDataSourceType { get; }
        protected abstract IGeneratedDALEntity GetDataSource();
        protected virtual List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null) // GetDataSources().Last() == GetDataSource()
        {
            return lst ?? new List<IGeneratedDALEntity>();
        }

        public abstract  long Id         { get; }
        public virtual bool IsModified   { get { return State.HasPendingChanges(); } }
        public virtual string Changes
        {
            get
            {
                return State == eState.Deleted || State == eState.Synchronized
                     ? string.Empty 
                     : State.GetDescription() + (State == eState.ToDelete 
                     ? TypedId.ToSmartString()
                     : string.Empty);
            }
        }

        public BusinessObjectGenerated(long id)
        {
            InitDirtynessRelays();
            LoadById(id);
        }
        protected BusinessObjectGenerated()
        {
            InitDirtynessRelays();
            CreateDataSource();
            ResetDataSourceEdits();
            InitializeState(eState.New);
            RaiseIdChanged();
        }
        void InitDirtynessRelays()
        {
            if (DirtynessRelays != null)
                foreach (var prop in DirtynessRelays)
                    AddDirtynessRelayFor(prop, null);
        }
        protected static readonly string[] _DirtynessRelays = new string[] { };
        protected void AssignWithDirtynessRelay<T>(ref T backField, T newValue)
            where T : class, IPropertyNotifierObject
        {
            if (backField != null)
                ((IPropertyNotifierObject)backField).DirtyChanged -= SubObject_DirtyChanged;
            backField = newValue;
            if (newValue != null)
                ((IPropertyNotifierObject)newValue).DirtyChanged += SubObject_DirtyChanged;
        }


        public void LoadById(long id)
        {
            bool idChanged = GetDataSource() == null || id != Id;
            LoadDataSource(id);
            ResetDataSourceEdits();
            InitializeState(eState.Synchronized);
            if (idChanged)
                RaiseIdChanged();
        }
        public override void SynchronizeToDatabase()
        {
            if (State == eState.Deleted || State == eState.Synchronized)
                return;
            if (State == eState.New || State == eState.Unsynchronized)
            {
                SaveComposites();
                SaveDataSourceEdits();
                foreach (var ds in GetDataSources())
                    ds.SynchronizeToDatabase();
            }
            else if (State == eState.ToDelete)
            {
                foreach (var ds in GetDataSources().Reverse<IGeneratedDALEntity>())
                    ds.Delete();
                DeleteComposites(); // Ce sont les ds qui reference les objets composites
            }
            UpdateState();
        }

        protected virtual string[] DirtynessRelays { get { return null; } }
        protected virtual void CreateDataSource() { }
        protected virtual void LoadDataSource(long id) { }
        // Copie les donn�es du/des datasource(s) vers l'objet m�tier
        protected virtual void ResetDataSourceEdits() { }
        protected static     T ResetDataSourceEditsRecursive<T>(T bo) where T : BusinessObjectGenerated { if (bo != null) bo.ResetDataSourceEdits(); return bo; } // Permet d'appeler une methode sur un expression inline
        protected virtual void SaveDataSourceEdits() { } // Copie les donn�es de l'objet m�tier vers le(s) datasource(s)
        protected virtual void SaveComposites() { }
        protected virtual void DeleteComposites() { }




        protected override IdTuple<long>? ClosedId { get { return Id == 0 ? (IdTuple<long>?)null : new IdTuple<long>(Id); }
                                                     set { Debug.Assert(Id == (long)value.Value.Keys[0]); } }
        public sealed override void LoadById(IdTuple<long> id)
        {
            LoadById((long)id.Keys[0]);
        }
        public override void Reload()
        {
            CancelChanges();
        }
        public void Delete()
        {
            UpdateState(eState.ToDelete);
            SynchronizeToDatabase();
        }

        protected static readonly string nl = Environment.NewLine;

        public static string DumpAllObjectsInMemory(List<object> potentialObjs = null)
        {
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            return DumpAllDALObjectsInMemory(objs)
                 + Environment.NewLine
                 + DumpAllBusinessObjectsInMemory(objs);
        }
        public static string DumpAllDALObjectsInMemory(List<object> potentialObjs = null)
        {
            var sb = new StringBuilder();
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            sb.AppendLine("DAL Objects:");
            foreach (var obj in objs)
            {
                var de = obj as IGeneratedDALEntity;
                if (de == null)
                    continue;
                sb.Append("   ");
                sb.AppendLine(de.TypedId.AsDebugString);
            }
            return sb.ToString();
        }
        public static string DumpAllBusinessObjectsInMemory(List<object> potentialObjs = null)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Business Objects:");
            var objs = potentialObjs ?? CacheManager.Instance.GetObjectsInMemory();
            foreach (var obj in objs)
            {
                var bo = obj as BusinessObjectGenerated;
                if (bo == null)
                    continue;
                sb.Append("   ");
                sb.AppendLine(bo.TypedId.AsDebugString);
            }
            return sb.ToString();
        }

    }
}
