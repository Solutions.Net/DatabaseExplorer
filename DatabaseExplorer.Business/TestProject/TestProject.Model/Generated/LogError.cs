using GenericDALBasics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TestProject.DAL;
using TestProject.DAL.Dbo;

namespace TestProject.Model
{
    public partial class LogError : BusinessObjectGenerated
    {
        public DateTime When { get { return _When; } set { SetProperty(ref _When, value); } } DateTime _When;
        public String Method { get { return _Method; } set { SetProperty(ref _Method, value); } } String _Method;
        public String Args { get { return _Args; } set { SetProperty(ref _Args, value); } } String _Args;
        public String ErrorMessage { get { return _ErrorMessage; } set { SetProperty(ref _ErrorMessage, value); } } String _ErrorMessage;
        public String FullLog { get { return _FullLog; } set { SetProperty(ref _FullLog, value); } } String _FullLog;


        public override long Id { get { return DataSource.Id_LogError; } }
        public override bool IsModified
        {
            get
            {
                return base.IsModified ||
                       _When != DataSource.When ||
                       _Method != DataSource.Method ||
                       _Args != DataSource.Args ||
                       _ErrorMessage != DataSource.ErrorMessage ||
                       _FullLog != DataSource.FullLog;
                // TODO : Casse couille pour les structures ! => Modifier CompositeList.DeletedObject en dictionaire (ca ne regle pas le probleme des liste pouvant avoir des doublon) Et faire une version de CompositeListpour les structs !
            }
        }
        protected internal override string Dump(bool all)
        {
            if (!all && (State == eState.New || State == eState.Synchronized))
                return base.Dump(all);
            return new[]
            {
                base.Dump(all),
                all || _When != DataSource.When ? "When: " + (_When).ToString() : "",
                all || _Method != DataSource.Method ? "Method: " + (_Method ?? (object)"").ToString() : "",
                all || _Args != DataSource.Args ? "Args: " + (_Args ?? (object)"").ToString() : "",
                all || _ErrorMessage != DataSource.ErrorMessage ? "ErrorMessage: " + (_ErrorMessage ?? (object)"").ToString() : "",
                all || _FullLog != DataSource.FullLog ? "FullLog: " + (_FullLog ?? (object)"").ToString() : ""
            }.NotBlank().Join(ChangeIndent);
        }

        protected override Type ConcreteDataSourceType { get { return typeof(TestProject.DAL.Dbo.LogError); } }
        private TestProject.DAL.Dbo.LogError DataSource { get; set; }
        protected override IGeneratedDALEntity GetDataSource() { return DataSource; }
        protected override List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null)
        {
            lst = base.GetDataSources(lst);
            lst.Add(DataSource);
            return lst;
        }


        public LogError() { }
        public LogError(long id) : base(id) { }


        protected override void Init()
        {
            base.Init();
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void CreateDataSource() { base.CreateDataSource(); DoCreateDataSource(); }
        private void DoCreateDataSource()
        {
            DataSource = new TestProject.DAL.Dbo.LogError(true);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void LoadDataSource(long id) { base.LoadDataSource(id); DoLoadDataSource(id); }
        private void DoLoadDataSource(long id)
        {
            // Le Load ne modifie pas les valeurs, il se contente de reconstruire la hierarchie de datasource
            DataSource = CacheManager.Instance.Get<TestProject.DAL.Dbo.LogError>(new IdTuple<long>(id));
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void ResetDataSourceEdits() { base.ResetDataSourceEdits(); DoResetDataSourceEdits(DataSource); }
        private void DoResetDataSourceEdits(TestProject.DAL.Dbo.LogError ds)
        {
            _When = ds.When;
            _Method = ds.Method;
            _Args = ds.Args;
            _ErrorMessage = ds.ErrorMessage;
            _FullLog = ds.FullLog;
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void SaveDataSourceEdits() { base.SaveDataSourceEdits(); DoSaveDataSourceEdits(DataSource); }
        private void DoSaveDataSourceEdits(TestProject.DAL.Dbo.LogError ds)
        {
            ds.When = _When;
            ds.Method = _Method;
            ds.Args = _Args;
            ds.ErrorMessage = _ErrorMessage;
            ds.FullLog = _FullLog;
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void SaveScalarComposites() { base.SaveScalarComposites(); DoSaveScalarComposites(DataSource); }
        private void DoSaveScalarComposites(TestProject.DAL.Dbo.LogError ds)
        {
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void SaveLinkComposites() { base.SaveLinkComposites(); DoSaveLinkComposites(); }
        private void DoSaveLinkComposites()
        {
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteLinkComposites() { DoDeleteLinkComposites(); base.DeleteLinkComposites(); }
        private void DoDeleteLinkComposites()
        {
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteScalarComposites() { DoDeleteScalarComposites(DataSource); base.DeleteScalarComposites(); }
        private void DoDeleteScalarComposites(TestProject.DAL.Dbo.LogError ds)
        {
        }


        public override List<BusinessDeleteActionDependency> GetEditToDoBeforeDelete()
        {
            var edits = base.GetEditToDoBeforeDelete();
            return edits;
        }
    }
}
