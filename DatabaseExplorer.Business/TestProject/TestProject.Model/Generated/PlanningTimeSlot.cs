using System;
using System.Runtime.InteropServices;

namespace TestProject.Model
{
    [StructLayout(LayoutKind.Explicit)]
    public partial struct PlanningTimeSlot : IBusinessStructGenerated
    {
        public TimeSpan Debut { get { return _Debut; } set { _Debut = value; }  } [FieldOffset(0)] TimeSpan _Debut;
        public TimeSpan Fin { get { return _Fin; } set { _Fin = value; }  } [FieldOffset(8)] TimeSpan _Fin;
        public UInt64 Disponibility { get { return _Disponibility; } set { _Disponibility = value; }  } [FieldOffset(16)] UInt64 _Disponibility;
        public Int32 Flags { get { return _Flags; } set { _Flags = value; }  } [FieldOffset(24)] Int32 _Flags;

        public byte[] Serialize()
        {
            var size = Marshal.SizeOf(typeof(PlanningTimeSlot));
            var array = new byte[28];
            var ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(this, ptr, false);
            Marshal.Copy(ptr, array, 0, 28);
            Marshal.DestroyStructure(ptr, typeof(PlanningTimeSlot));
            Marshal.FreeHGlobal(ptr);
            return array;
        }
        public static PlanningTimeSlot Deserialize(byte[] array)
        {
            System.Diagnostics.Debug.Assert(array.Length == 28);
            var size = Marshal.SizeOf(typeof(PlanningTimeSlot));
            var ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(array, 0, ptr, 28);
            var s = (PlanningTimeSlot)Marshal.PtrToStructure(ptr, typeof(PlanningTimeSlot));
            Marshal.FreeHGlobal(ptr);
            return s;
        }

        public string AsDebugString
        {
            get
            {
                return "{"
                     + " Debut: " + Debut
                     + " Fin: " + Fin
                     + " Disponibility: " + Disponibility
                     + " Flags: " + Flags
                     + " }";
            }
        }
    }
}
