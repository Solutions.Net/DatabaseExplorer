using GenericDALBasics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TestProject.DAL;
using TestProject.DAL.Dbo;

namespace TestProject.Model
{
    public partial class Book : BusinessObjectGenerated
    {
        public String Title { get { return _Title; } set { SetProperty(ref _Title, value); } } String _Title;
        public Int32 PageCount { get { return _PageCount; } set { SetProperty(ref _PageCount, value); } } Int32 _PageCount;
        public Author TheAuthor { get { return _TheAuthor; } set { SetProperty(ref _TheAuthor, value); } } Author _TheAuthor;
        public Author CoAuthor { get { return _CoAuthor; } set { SetProperty(ref _CoAuthor, value); } } Author _CoAuthor;
        public readonly CheckableCompositeListGenerated<Page> Pages = new CheckableCompositeListGenerated<Page>() { RememberDeletedObjects = true, ConsiderItemAsComposite = true, ConsiderDirtyOnItemChanged = true, ConsiderDirtyOnAddRemoveEvents = true };


        public override long Id { get { return DataSource.Id_Book; } }
        public override bool IsModified
        {
            get
            {
                return base.IsModified ||
                       _Title != DataSource.Title ||
                       _PageCount != DataSource.PageCount ||
                       (_TheAuthor == null ? (long?)null : _TheAuthor.Id) != DataSource.TheAuthor_Id ||
                       (_CoAuthor == null ? (long?)null : _CoAuthor.Id) != DataSource.CoAuthor_Id ||
                       Pages.State != eState.Synchronized;
                // TODO : Casse couille pour les structures ! => Modifier CompositeList.DeletedObject en dictionaire (ca ne regle pas le probleme des liste pouvant avoir des doublon) Et faire une version de CompositeListpour les structs !
            }
        }
        protected internal override string Dump(bool all)
        {
            if (!all && (State == eState.New || State == eState.Synchronized))
                return base.Dump(all);
            return new[]
            {
                base.Dump(all),
                all || _Title != DataSource.Title ? "Title: " + (_Title ?? (object)"").ToString() : "",
                all || _PageCount != DataSource.PageCount ? "PageCount: " + (_PageCount).ToString() : "",
                all || (_TheAuthor == null ? (long?)null : _TheAuthor.Id) != DataSource.TheAuthor_Id ? "TheAuthor: " + (_TheAuthor == null ? "" : _TheAuthor.Id.ToString()) : "",
                all || (_CoAuthor == null ? (long?)null : _CoAuthor.Id) != DataSource.CoAuthor_Id ? "CoAuthor: " + (_CoAuthor == null ? "" : _CoAuthor.Id.ToString()) : "",
                Pages.DeletedObjects.Concat(Pages).Select(p => p.Dump(all).Replace(nl, nl + "     ").PrefixIfNotEmptyWith("   - ")).NotBlank().Join(nl).Replace(nl, ChangeIndent).PrefixIfNotEmptyWith("Pages:" + ChangeIndent)
            }.NotBlank().Join(ChangeIndent);
        }

        protected override Type ConcreteDataSourceType { get { return typeof(TestProject.DAL.Dbo.Book); } }
        private TestProject.DAL.Dbo.Book DataSource { get; set; }
        protected override IGeneratedDALEntity GetDataSource() { return DataSource; }
        protected override List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null)
        {
            lst = base.GetDataSources(lst);
            lst.Add(DataSource);
            return lst;
        }


        public Book() { }
        public Book(long id) : base(id) { }


        protected override void Init()
        {
            base.Init();
            Pages.LoadItems = LoadComposites_Pages;
            Pages.UpdateState(eState.Synchronized);
            AddDirtynessRelayFor(GetMemberName.For<Book>(p => p.Pages), Pages);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void CreateDataSource() { base.CreateDataSource(); DoCreateDataSource(); }
        private void DoCreateDataSource()
        {
            DataSource = new TestProject.DAL.Dbo.Book(true);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void LoadDataSource(long id) { base.LoadDataSource(id); DoLoadDataSource(id); }
        private void DoLoadDataSource(long id)
        {
            // Le Load ne modifie pas les valeurs, il se contente de reconstruire la hierarchie de datasource
            DataSource = CacheManager.Instance.Get<TestProject.DAL.Dbo.Book>(new IdTuple<long>(id));
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void ResetDataSourceEdits() { base.ResetDataSourceEdits(); DoResetDataSourceEdits(DataSource); }
        private void DoResetDataSourceEdits(TestProject.DAL.Dbo.Book ds)
        {
            _Title = ds.Title;
            _PageCount = ds.PageCount;
            _TheAuthor = ds.TheAuthor_Id == 0 ? null : CacheManager.Instance.Get<Author>(new IdTuple<long>(ds.TheAuthor_Id));
            _CoAuthor = ds.CoAuthor_Id == null ? null : CacheManager.Instance.Get<Author>(new IdTuple<long>(ds.CoAuthor_Id.Value));
            Pages.Reload();
            foreach (var p in LoadComposites_Pages(/*true*/)) // TODO : on pourra eviter des charger les element pas dans le cache quand on aura des collections lazy
                ResetDataSourceEditsRecursive(p);
        }
        private IEnumerable<Page> LoadComposites_Pages() { return LoadComposites_Pages(false); }
        private IEnumerable<Page> LoadComposites_Pages(bool preventLoading)
        {
            if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))
                return new Page[0];
            // TODO : A optimiser : on pourrait faire une seule requete au lieu de deux
            var links = GetEntitiesWhere<Book, TestProject.DAL.Dbo.Book_Page>((@this, lnk) => lnk.Book_Id == @this.Id,
                                                                              @this => TestProject.DAL.Dbo.Book_Page.GetEntitiesWhere("Book_Id = " + @this.Id));
            var ids = links.Select(lnk => new IdTuple<long>(lnk.Page_Id));
            var dalObjects = TestProject.DAL.Dbo.Page.GetEntitiesWithIds(ids);
            return CacheManager.Instance.GetAll<Page>(ids.Cast<IIdTuple>(), preventLoading);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void SaveDataSourceEdits() { base.SaveDataSourceEdits(); DoSaveDataSourceEdits(DataSource); }
        private void DoSaveDataSourceEdits(TestProject.DAL.Dbo.Book ds)
        {
            ds.Title = _Title;
            ds.PageCount = _PageCount;
            if (_TheAuthor == null)
                throw new FunctionalException("TheAuthor must be specified !", null);
            ds.TheAuthor_Id = _TheAuthor.Id;
            ds.CoAuthor_Id = _CoAuthor == null ? (long?)null : _CoAuthor.Id;
            foreach (var p in Pages)
                SaveDataSourceEditsRecursive(p);
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void SaveScalarComposites() { base.SaveScalarComposites(); DoSaveScalarComposites(DataSource); }
        private void DoSaveScalarComposites(TestProject.DAL.Dbo.Book ds)
        {
            if (Pages.DeletedObjects.Count > 0)
            {
                var links = GetEntitiesWhere<Book, TestProject.DAL.Dbo.Book_Page>((@this, lnk) => lnk.Book_Id == @this.Id && @this.Pages.DeletedObjects.Any(p => p.Id == lnk.Page_Id),
                                                                                  @this => TestProject.DAL.Dbo.Book_Page.GetEntitiesWhere("Book_Id = " + @this.Id + " AND Page_Id in (" + @this.Pages.DeletedObjects.Select(p => p.Id).Join() + ")"));
                foreach (var link in links)
                    link.Delete();
            }
            Debug.Assert(Pages.DeletedObjects.All(p => p.State == eState.ToDelete));
            foreach (var p in Pages.DeletedObjects)
                p.SynchronizeToDatabase();
            foreach (var p in Pages)
                p.SynchronizeToDatabase();
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void SaveLinkComposites() { base.SaveLinkComposites(); DoSaveLinkComposites(); }
        private void DoSaveLinkComposites()
        {
            foreach (var p in Pages)
            {
                var link = new TestProject.DAL.Dbo.Book_Page() { Book_Id = Id, Page_Id = p.Id };
                link.SynchronizeToDatabase();
            }
            Pages.AcceptChanges();
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteLinkComposites() { DoDeleteLinkComposites(); base.DeleteLinkComposites(); }
        private void DoDeleteLinkComposites()
        {
            var linksPages = GetEntitiesWhere <Book, TestProject.DAL.Dbo.Book_Page>((@this, lnk) => lnk.Book_Id == @this.Id,
                                                                                    @this => TestProject.DAL.Dbo.Book_Page.GetEntitiesWhere("Book_Id = " + @this.Id));
            foreach (var link in linksPages)
                link.Delete();
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteScalarComposites() { DoDeleteScalarComposites(DataSource); base.DeleteScalarComposites(); }
        private void DoDeleteScalarComposites(TestProject.DAL.Dbo.Book ds)
        {
            if (TheAuthor != null)
                TheAuthor.UpdateState(eState.ToDelete);
            if (CoAuthor != null)
                CoAuthor.UpdateState(eState.ToDelete);
            Pages.Clear(); // ==> passe tous les objets dans Pages.DeletedObjects
            foreach (var p in Pages.DeletedObjects)
                p.Delete();
            DoSaveScalarComposites(ds);
        }


        public override List<BusinessDeleteActionDependency> GetEditToDoBeforeDelete()
        {
            var edits = base.GetEditToDoBeforeDelete();
            return edits;
        }
    }
}
