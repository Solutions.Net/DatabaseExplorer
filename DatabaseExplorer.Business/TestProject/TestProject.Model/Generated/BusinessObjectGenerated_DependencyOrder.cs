using System;
using System.Collections.Generic;

namespace TestProject.Model
{
    public partial class BusinessObjectGenerated
    {
        public static readonly Dictionary<Type, int> AllEntityTypesInCreationOrder = new Dictionary<Type, int>()
        {
            { typeof(PlanningTimeSlot), 1},
            { typeof(Address), 2},
            { typeof(TestProject.Types.eDayTask), 3},
            { typeof(Author), 4},
            { typeof(Person), 5},
            { typeof(LogError), 6},
            { typeof(Page), 7},
            { typeof(Book), 8},
        };
        public static Type ToDalType(Type t)
        {
            Type dalType;
            _ToDalType.TryGetValue(t, out dalType);
            return dalType;
        }
        static readonly Dictionary<Type, Type> _ToDalType = new Dictionary<Type, Type>()
        {
            { typeof(Address), typeof(TestProject.DAL.Dbo.Address) },
            { typeof(Author), typeof(TestProject.DAL.Dbo.Author) },
            { typeof(Person), typeof(TestProject.DAL.Dbo.Person) },
            { typeof(LogError), typeof(TestProject.DAL.Dbo.LogError) },
            { typeof(Page), typeof(TestProject.DAL.Dbo.Page) },
            { typeof(Book), typeof(TestProject.DAL.Dbo.Book) },
        };
        public static Type ToModelType(Type t)
        {
            Type modelType;
            _ToModelType.TryGetValue(t, out modelType);
            return modelType;
        }
        static readonly Dictionary<Type, Type> _ToModelType = new Dictionary<Type, Type>()
        {
            { typeof(TestProject.DAL.Dbo.Address), typeof(Address) },
            { typeof(TestProject.DAL.Dbo.Author), typeof(Author) },
            { typeof(TestProject.DAL.Dbo.Person), typeof(Person) },
            { typeof(TestProject.DAL.Dbo.LogError), typeof(LogError) },
            { typeof(TestProject.DAL.Dbo.Page), typeof(Page) },
            { typeof(TestProject.DAL.Dbo.Book), typeof(Book) },
        };
    }
}
