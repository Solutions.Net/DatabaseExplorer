using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TestProject.DAL;
using TestProject.DAL.Dbo;

namespace TestProject.Model
{
    public partial class Author : Person
    {
        public String NickName { get { return _NickName; } set { SetProperty(ref _NickName, value); } } String _NickName;
        public PlanningTimeSlot ArrivingTime { get { return _ArrivingTime; } set { SetProperty(ref _ArrivingTime, value); } } PlanningTimeSlot _ArrivingTime;
        public PlanningTimeSlot? ArrivingTimeNullable { get { return _ArrivingTimeNullable; } set { SetProperty(ref _ArrivingTimeNullable, value); } } PlanningTimeSlot? _ArrivingTimeNullable;
        public readonly CheckableCompositeListGenerated<Int32> Ints = new CheckableCompositeListGenerated<Int32>() { RememberDeletedObjects = false, ConsiderItemAsComposite = false, ConsiderDirtyOnItemChanged = false, ConsiderDirtyOnAddRemoveEvents = true };
        public readonly CheckableCompositeListGenerated<TestProject.DAL.Dbo.Author_Int> _Ints = new CheckableCompositeListGenerated<TestProject.DAL.Dbo.Author_Int>() { RememberDeletedObjects = true, ConsiderItemAsComposite = true, ConsiderDirtyOnItemChanged = true, ConsiderDirtyOnAddRemoveEvents = true };
                        bool _Ints_reloading;
        public readonly CheckableCompositeListGenerated<PlanningTimeSlot> Times = new CheckableCompositeListGenerated<PlanningTimeSlot>() { RememberDeletedObjects = false, ConsiderItemAsComposite = false, ConsiderDirtyOnItemChanged = false, ConsiderDirtyOnAddRemoveEvents = true };
        public readonly CheckableCompositeListGenerated<TestProject.DAL.Dbo.Author_Time> _Times = new CheckableCompositeListGenerated<TestProject.DAL.Dbo.Author_Time>() { RememberDeletedObjects = true, ConsiderItemAsComposite = true, ConsiderDirtyOnItemChanged = true, ConsiderDirtyOnAddRemoveEvents = true };
                        bool _Times_reloading;


        public override long Id { get { return DataSource.Id_Author; } }
        public override bool IsModified
        {
            get
            {
                return IsModified ||
                       _NickName != DataSource.NickName ||
                       !object.Equals(_ArrivingTime, DataSource.ArrivingTime == null ? new PlanningTimeSlot() : PlanningTimeSlot.Deserialize(DataSource.ArrivingTime)) ||
                       !object.Equals(_ArrivingTimeNullable, DataSource.ArrivingTimeNullable == null ? (PlanningTimeSlot?)null : PlanningTimeSlot.Deserialize(DataSource.ArrivingTimeNullable)) ||
                       _Ints.State != eState.Synchronized ||
                       _Times.State != eState.Synchronized;
                // TODO : Casse couille pour les structures ! => Modifier CompositeList.DeletedObject en dictionaire (ca ne regle pas le probleme des list pouvant avoir des doublon) Et faire une version de CompositeListpour les structs !
            }
        }
        protected internal override string Dump(bool all)
        {
            if (!all && (State == eState.New || State == eState.Synchronized))
                return base.Dump(all);
            return new[]
            {
                base.Dump(all),
                all || _NickName != DataSource.NickName ? "NickName: " + (_NickName ?? (object)"").ToString() : "",
                all || !object.Equals(_ArrivingTime, DataSource.ArrivingTime == null ? new PlanningTimeSlot() : PlanningTimeSlot.Deserialize(DataSource.ArrivingTime)) ? "ArrivingTime: " + _ArrivingTime.AsString : "",
                all || !object.Equals(_ArrivingTimeNullable, DataSource.ArrivingTimeNullable == null ? (PlanningTimeSlot?)null : PlanningTimeSlot.Deserialize(DataSource.ArrivingTimeNullable)) ? "ArrivingTimeNullable: " + (_ArrivingTimeNullable == null ? "" : _ArrivingTimeNullable.Value.AsString) : "",
                IBusinessStructGenerated_Extensions.DisplayDiff(_Ints, all),
                IBusinessStructGenerated_Extensions.DisplayDiff(_Times, all)
            }.NotBlank().Join(ChangeIndent);
        }

        protected override Type ConcreteDataSourceType { get { return typeof(TestProject.DAL.Dbo.Author); } }
        private TestProject.DAL.Dbo.Author DataSource { get; set; }
        protected override IGeneratedDALEntity GetDataSource() { return DataSource; }
        protected override List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null)
        {
            lst = base.GetDataSources(lst);
            lst.Add(DataSource);
            return lst;
        }


        public Author() { }
        public Author(long id) : base(id) { }


        protected override void Init()
        {
            base.Init();
            _Ints.LoadItems = LoadComposites_Ints;
            Ints.LoadItems = () => _Ints.Select(lnk => lnk.Int);
            Ints.UpdateState(eState.Synchronized);
            _Ints.UpdateState(eState.Synchronized);
            Ints.ItemAdded += (_, e) => { if (_Ints_reloading) return; _Ints.Insert(e.Index, new TestProject.DAL.Dbo.Author_Int() { Author_Id = Id, Int = e.Item }); };
            Ints.ItemRemoved += (_, e) => _Ints.RemoveAt(e.Index);
            Ints.Cleared += (_, e) => { if (_Ints_reloading) return; _Ints.Clear(); };
            AddDirtynessRelayFor(GetMemberName.For<Author>(i => i._Ints), _Ints);
            _Times.LoadItems = LoadComposites_Times;
            Times.LoadItems = () => _Times.Select(lnk => PlanningTimeSlot.Deserialize(lnk.Time));
            Times.UpdateState(eState.Synchronized);
            _Times.UpdateState(eState.Synchronized);
            Times.ItemAdded += (_, e) => { if (_Times_reloading) return; _Times.Insert(e.Index, new TestProject.DAL.Dbo.Author_Time() { Author_Id = Id, Time = e.Item.Serialize() }); };
            Times.ItemRemoved += (_, e) => _Times.RemoveAt(e.Index);
            Times.Cleared += (_, e) => { if (_Times_reloading) return; _Times.Clear(); };
            AddDirtynessRelayFor(GetMemberName.For<Author>(i => i._Times), _Times);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void CreateDataSource() { base.CreateDataSource(); DoCreateDataSource(); }
        private void DoCreateDataSource()
        {
            DataSource = new TestProject.DAL.Dbo.Author(true, new IdTuple<long>(base.Id));
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void LoadDataSource(long id) { base.LoadDataSource(id); DoLoadDataSource(id); }
        private void DoLoadDataSource(long id)
        {
            // Le Load ne modifie pas les valeurs, il se contente de reconstruire la hierarchie de datasource
            DataSource = CacheManager.Instance.Get<TestProject.DAL.Dbo.Author>(new IdTuple<long>(id));
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void ResetDataSourceEdits() { base.ResetDataSourceEdits(); DoResetDataSourceEdits(DataSource); }
        private void DoResetDataSourceEdits(TestProject.DAL.Dbo.Author ds)
        {
            _NickName = ds.NickName;
            ArrivingTime = ds.ArrivingTime == null ? new PlanningTimeSlot() : PlanningTimeSlot.Deserialize(ds.ArrivingTime);
            ArrivingTimeNullable = ds.ArrivingTimeNullable == null ? (PlanningTimeSlot?)null : PlanningTimeSlot.Deserialize(ds.ArrivingTimeNullable);
            _Ints.Reload();
            foreach (var i in _Ints/*LoadComposites_IntsNullable(true)*/) // TODO : on pourra eviter des charger les element pas dans le cache quand on aura des collections lazy
                i.CancelChanges(); // Peut provoquer des event state change sur _IntsNullable ? => Faire un test !
            _Ints_reloading = true;
            try { Ints.Reload(); }
            finally { _Ints_reloading = false; }
            Debug.Assert(_Ints.State == eState.Synchronized);
            Debug.Assert(Ints.State == eState.Synchronized);
            _Times.Reload();
            foreach (var t in _Times/*LoadComposites_IntsNullable(true)*/) // TODO : on pourra eviter des charger les element pas dans le cache quand on aura des collections lazy
                t.CancelChanges(); // Peut provoquer des event state change sur _IntsNullable ? => Faire un test !
            _Times_reloading = true;
            try { Times.Reload(); }
            finally { _Times_reloading = false; }
            Debug.Assert(_Times.State == eState.Synchronized);
            Debug.Assert(Times.State == eState.Synchronized);
        }
        private IEnumerable<TestProject.DAL.Dbo.Author_Int> LoadComposites_Ints() { return LoadComposites_Ints(false); }
        private IEnumerable<TestProject.DAL.Dbo.Author_Int> LoadComposites_Ints(bool preventLoading)
        {
            if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))
                return new TestProject.DAL.Dbo.Author_Int[0];
            return TestProject.DAL.Dbo.Author_Int.GetEntitiesWhere("Author_Id = " + this.Id);
        }
        private IEnumerable<TestProject.DAL.Dbo.Author_Time> LoadComposites_Times() { return LoadComposites_Times(false); }
        private IEnumerable<TestProject.DAL.Dbo.Author_Time> LoadComposites_Times(bool preventLoading)
        {
            if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))
                return new TestProject.DAL.Dbo.Author_Time[0];
            return TestProject.DAL.Dbo.Author_Time.GetEntitiesWhere("Author_Id = " + this.Id);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void SaveDataSourceEdits() { base.SaveDataSourceEdits(); DoSaveDataSourceEdits(DataSource); }
        private void DoSaveDataSourceEdits(TestProject.DAL.Dbo.Author ds)
        {
            ds.NickName = _NickName;
            ds.ArrivingTime = _ArrivingTime.Serialize();
            ds.ArrivingTimeNullable = _ArrivingTimeNullable == null ? (byte[])null : _ArrivingTimeNullable.Value.Serialize();
            
            
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void SaveScalarComposites() { base.SaveScalarComposites(); DoSaveScalarComposites(DataSource); }
        private void DoSaveScalarComposites(TestProject.DAL.Dbo.Author ds)
        {
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void SaveLinkComposites() { base.SaveLinkComposites(); DoSaveLinkComposites(); }
        private void DoSaveLinkComposites()
        {
            foreach (var link in _Ints.DeletedObjects)
                link.SynchronizeToDatabase();
            foreach (var link in _Ints)
                link.SynchronizeToDatabase();
            _Ints.AcceptChanges();
            Ints.AcceptChanges();
            foreach (var link in _Times.DeletedObjects)
                link.SynchronizeToDatabase();
            foreach (var link in _Times)
                link.SynchronizeToDatabase();
            _Times.AcceptChanges();
            Times.AcceptChanges();
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteLinkComposites() { DoDeleteLinkComposites(); base.DeleteLinkComposites(); }
        private void DoDeleteLinkComposites()
        {
            foreach (var link in _Ints.DeletedObjects)
                link.SynchronizeToDatabase();
            foreach (var link in _Ints)
                link.Delete();
            _Ints.UpdateState(eState.Deleted);
            Ints.UpdateState(eState.Deleted);
            foreach (var link in _Times.DeletedObjects)
                link.SynchronizeToDatabase();
            foreach (var link in _Times)
                link.Delete();
            _Times.UpdateState(eState.Deleted);
            Times.UpdateState(eState.Deleted);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteScalarComposites() { DoDeleteScalarComposites(DataSource); base.DeleteScalarComposites(); }
        private void DoDeleteScalarComposites(TestProject.DAL.Dbo.Author ds)
        {
        }
    }
}
