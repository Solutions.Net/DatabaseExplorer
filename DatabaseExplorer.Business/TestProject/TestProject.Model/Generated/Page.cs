using GenericDALBasics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TestProject.DAL;
using TestProject.DAL.Dbo;

namespace TestProject.Model
{
    public partial class Page : BusinessObjectGenerated
    {
        public Int32 Index { get { return _Index; } set { SetProperty(ref _Index, value); } } Int32 _Index;
        public String Content { get { return _Content; } set { SetProperty(ref _Content, value); } } String _Content;
        public readonly CompositeDictionary<String, String> WordDefinitions = new CompositeDictionary<String, String>() { RememberDeletedObjects = false, ConsiderDirtyOnAddRemoveEvents = true, ConsiderDirtyOnItemChanged = true };
               readonly CompositeDictionary<String, TestProject.DAL.Dbo.Page_WordDefinition> _WordDefinitions = new CompositeDictionary<String, TestProject.DAL.Dbo.Page_WordDefinition>() { RememberDeletedObjects = true, ConsiderDirtyOnAddRemoveEvents = true, ConsiderDirtyOnItemChanged = true };
                        bool _WordDefinitions_reloading;


        public override long Id { get { return DataSource.Id_Page; } }
        public override bool IsModified
        {
            get
            {
                return base.IsModified ||
                       _Index != DataSource.Index ||
                       _Content != DataSource.Content ||
                       _WordDefinitions.State != eState.Synchronized;
                // TODO : Casse couille pour les structures ! => Modifier CompositeList.DeletedObject en dictionaire (ca ne regle pas le probleme des liste pouvant avoir des doublon) Et faire une version de CompositeListpour les structs !
            }
        }
        protected internal override string Dump(bool all)
        {
            if (!all && (State == eState.New || State == eState.Synchronized))
                return base.Dump(all);
            return new[]
            {
                base.Dump(all),
                all || _Index != DataSource.Index ? "Index: " + (_Index).ToString() : "",
                all || _Content != DataSource.Content ? "Content: " + (_Content ?? (object)"").ToString() : "",
                IBusinessStructGenerated_Extensions.DisplayDiff("WordDefinitions", _WordDefinitions, all)
            }.NotBlank().Join(ChangeIndent);
        }

        protected override Type ConcreteDataSourceType { get { return typeof(TestProject.DAL.Dbo.Page); } }
        private TestProject.DAL.Dbo.Page DataSource { get; set; }
        protected override IGeneratedDALEntity GetDataSource() { return DataSource; }
        protected override List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null)
        {
            lst = base.GetDataSources(lst);
            lst.Add(DataSource);
            return lst;
        }


        public Page() { }
        public Page(long id) : base(id) { }


        protected override void Init()
        {
            base.Init();
            _WordDefinitions.LoadItems = () => LoadComposites_WordDefinitions().ToDictionary(item => item.Key, item => item);
            WordDefinitions.LoadItems = () => _WordDefinitions.ToDictionary(lnk => lnk.Key, lnk => (String)lnk.Value.WordDefinition);
            WordDefinitions.UpdateState(eState.Synchronized);
            _WordDefinitions.UpdateState(eState.Synchronized);
            WordDefinitions.CollectionChanged += BuildDictionaryCollectionChangedHandler(() => _WordDefinitions_reloading, WordDefinitions, _WordDefinitions,
                                          kvp => new TestProject.DAL.Dbo.Page_WordDefinition() { Owner_Id = Id, Key = kvp.Key, WordDefinition = kvp.Value });
            AddDirtynessRelayFor(GetMemberName.For<Page>(w => w._WordDefinitions), _WordDefinitions);
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void CreateDataSource() { base.CreateDataSource(); DoCreateDataSource(); }
        private void DoCreateDataSource()
        {
            DataSource = new TestProject.DAL.Dbo.Page(true);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void LoadDataSource(long id) { base.LoadDataSource(id); DoLoadDataSource(id); }
        private void DoLoadDataSource(long id)
        {
            // Le Load ne modifie pas les valeurs, il se contente de reconstruire la hierarchie de datasource
            DataSource = CacheManager.Instance.Get<TestProject.DAL.Dbo.Page>(new IdTuple<long>(id));
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void ResetDataSourceEdits() { base.ResetDataSourceEdits(); DoResetDataSourceEdits(DataSource); }
        private void DoResetDataSourceEdits(TestProject.DAL.Dbo.Page ds)
        {
            _Index = ds.Index;
            _Content = ds.Content;
            _WordDefinitions.Reload();
            _WordDefinitions_reloading = true;
            try { WordDefinitions.Reload(); }
            finally { _WordDefinitions_reloading = false; }
            Debug.Assert(_WordDefinitions.State == eState.Synchronized);
            Debug.Assert(WordDefinitions.State == eState.Synchronized);
        }
        private IEnumerable<TestProject.DAL.Dbo.Page_WordDefinition> LoadComposites_WordDefinitions() { return LoadComposites_WordDefinitions(false); }
        private IEnumerable<TestProject.DAL.Dbo.Page_WordDefinition> LoadComposites_WordDefinitions(bool preventLoading)
        {
            if (this.DataSource.State.NotIn(eState.Synchronized, eState.Unsynchronized, eState.ToDelete))
                return new TestProject.DAL.Dbo.Page_WordDefinition[0];
            return GetEntitiesWhere<Page, TestProject.DAL.Dbo.Page_WordDefinition>((@this, lnk) => lnk.Owner_Id == @this.Id,
                                                                                   @this => TestProject.DAL.Dbo.Page_WordDefinition.GetEntitiesWhere("Owner_Id = " + @this.Id));
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void SaveDataSourceEdits() { base.SaveDataSourceEdits(); DoSaveDataSourceEdits(DataSource); }
        private void DoSaveDataSourceEdits(TestProject.DAL.Dbo.Page ds)
        {
            ds.Index = _Index;
            ds.Content = _Content;
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void SaveScalarComposites() { base.SaveScalarComposites(); DoSaveScalarComposites(DataSource); }
        private void DoSaveScalarComposites(TestProject.DAL.Dbo.Page ds)
        {
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void SaveLinkComposites() { base.SaveLinkComposites(); DoSaveLinkComposites(); }
        private void DoSaveLinkComposites()
        {
            foreach (var dalKvp in _WordDefinitions.DeletedObjects.Values)
                dalKvp.SynchronizeToDatabase();
            foreach (var dalKvp in _WordDefinitions.Values)
                dalKvp.SynchronizeToDatabase();
            _WordDefinitions.AcceptChanges();
            WordDefinitions.AcceptChanges();
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteLinkComposites() { DoDeleteLinkComposites(); base.DeleteLinkComposites(); }
        private void DoDeleteLinkComposites()
        {
            foreach (var dalKvp in _WordDefinitions.DeletedObjects.Values)
                dalKvp.SynchronizeToDatabase();
            foreach (var dalKvp in _WordDefinitions.Values)
                dalKvp.Delete();
            _WordDefinitions.UpdateState(eState.Deleted);
            WordDefinitions.UpdateState(eState.Deleted);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteScalarComposites() { DoDeleteScalarComposites(DataSource); base.DeleteScalarComposites(); }
        private void DoDeleteScalarComposites(TestProject.DAL.Dbo.Page ds)
        {
        }


        public override List<BusinessDeleteActionDependency> GetEditToDoBeforeDelete()
        {
            var edits = base.GetEditToDoBeforeDelete();
            return edits;
        }
    }
}
