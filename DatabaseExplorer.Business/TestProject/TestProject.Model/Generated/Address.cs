using GenericDALBasics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools;
using TestProject.DAL;
using TestProject.DAL.Dbo;

namespace TestProject.Model
{
    public partial class Address : BusinessObjectGenerated
    {
        public String AddressLine1 { get { return _AddressLine1; } set { SetProperty(ref _AddressLine1, value); } } String _AddressLine1;
        public String AddressLine2 { get { return _AddressLine2; } set { SetProperty(ref _AddressLine2, value); } } String _AddressLine2;
        public String City { get { return _City; } set { SetProperty(ref _City, value); } } String _City;
        public String StateOrProvinceOrRegion { get { return _StateOrProvinceOrRegion; } set { SetProperty(ref _StateOrProvinceOrRegion, value); } } String _StateOrProvinceOrRegion;
        public String ZipPostalCode { get { return _ZipPostalCode; } set { SetProperty(ref _ZipPostalCode, value); } } String _ZipPostalCode;
        public String Countr { get { return _Countr; } set { SetProperty(ref _Countr, value); } } String _Countr;


        public override long Id { get { return DataSource.Id_Address; } }
        public override bool IsModified
        {
            get
            {
                return base.IsModified ||
                       _AddressLine1 != DataSource.AddressLine1 ||
                       _AddressLine2 != DataSource.AddressLine2 ||
                       _City != DataSource.City ||
                       _StateOrProvinceOrRegion != DataSource.StateOrProvinceOrRegion ||
                       _ZipPostalCode != DataSource.ZipPostalCode ||
                       _Countr != DataSource.Countr;
                // TODO : Casse couille pour les structures ! => Modifier CompositeList.DeletedObject en dictionaire (ca ne regle pas le probleme des liste pouvant avoir des doublon) Et faire une version de CompositeListpour les structs !
            }
        }
        protected internal override string Dump(bool all)
        {
            if (!all && (State == eState.New || State == eState.Synchronized))
                return base.Dump(all);
            return new[]
            {
                base.Dump(all),
                all || _AddressLine1 != DataSource.AddressLine1 ? "AddressLine1: " + (_AddressLine1 ?? (object)"").ToString() : "",
                all || _AddressLine2 != DataSource.AddressLine2 ? "AddressLine2: " + (_AddressLine2 ?? (object)"").ToString() : "",
                all || _City != DataSource.City ? "City: " + (_City ?? (object)"").ToString() : "",
                all || _StateOrProvinceOrRegion != DataSource.StateOrProvinceOrRegion ? "StateOrProvinceOrRegion: " + (_StateOrProvinceOrRegion ?? (object)"").ToString() : "",
                all || _ZipPostalCode != DataSource.ZipPostalCode ? "ZipPostalCode: " + (_ZipPostalCode ?? (object)"").ToString() : "",
                all || _Countr != DataSource.Countr ? "Countr: " + (_Countr ?? (object)"").ToString() : ""
            }.NotBlank().Join(ChangeIndent);
        }

        protected override Type ConcreteDataSourceType { get { return typeof(TestProject.DAL.Dbo.Address); } }
        private TestProject.DAL.Dbo.Address DataSource { get; set; }
        protected override IGeneratedDALEntity GetDataSource() { return DataSource; }
        protected override List<IGeneratedDALEntity> GetDataSources(List<IGeneratedDALEntity> lst = null)
        {
            lst = base.GetDataSources(lst);
            lst.Add(DataSource);
            return lst;
        }


        public Address() { }
        public Address(long id) : base(id) { }


        protected override void Init()
        {
            base.Init();
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void CreateDataSource() { base.CreateDataSource(); DoCreateDataSource(); }
        private void DoCreateDataSource()
        {
            DataSource = new TestProject.DAL.Dbo.Address(true);
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void LoadDataSource(long id) { base.LoadDataSource(id); DoLoadDataSource(id); }
        private void DoLoadDataSource(long id)
        {
            // Le Load ne modifie pas les valeurs, il se contente de reconstruire la hierarchie de datasource
            DataSource = CacheManager.Instance.Get<TestProject.DAL.Dbo.Address>(new IdTuple<long>(id));
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void ResetDataSourceEdits() { base.ResetDataSourceEdits(); DoResetDataSourceEdits(DataSource); }
        private void DoResetDataSourceEdits(TestProject.DAL.Dbo.Address ds)
        {
            _AddressLine1 = ds.AddressLine1;
            _AddressLine2 = ds.AddressLine2;
            _City = ds.City;
            _StateOrProvinceOrRegion = ds.StateOrProvinceOrRegion;
            _ZipPostalCode = ds.ZipPostalCode;
            _Countr = ds.Countr;
        }

        [DebuggerHidden, DebuggerStepThrough] protected override void SaveDataSourceEdits() { base.SaveDataSourceEdits(); DoSaveDataSourceEdits(DataSource); }
        private void DoSaveDataSourceEdits(TestProject.DAL.Dbo.Address ds)
        {
            ds.AddressLine1 = _AddressLine1;
            ds.AddressLine2 = _AddressLine2;
            ds.City = _City;
            ds.StateOrProvinceOrRegion = _StateOrProvinceOrRegion;
            ds.ZipPostalCode = _ZipPostalCode;
            ds.Countr = _Countr;
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void SaveScalarComposites() { base.SaveScalarComposites(); DoSaveScalarComposites(DataSource); }
        private void DoSaveScalarComposites(TestProject.DAL.Dbo.Address ds)
        {
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void SaveLinkComposites() { base.SaveLinkComposites(); DoSaveLinkComposites(); }
        private void DoSaveLinkComposites()
        {
        }


        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteLinkComposites() { DoDeleteLinkComposites(); base.DeleteLinkComposites(); }
        private void DoDeleteLinkComposites()
        {
        }
        [DebuggerHidden, DebuggerStepThrough] protected override void DeleteScalarComposites() { DoDeleteScalarComposites(DataSource); base.DeleteScalarComposites(); }
        private void DoDeleteScalarComposites(TestProject.DAL.Dbo.Address ds)
        {
        }


        public override List<BusinessDeleteActionDependency> GetEditToDoBeforeDelete()
        {
            var edits = base.GetEditToDoBeforeDelete();
            return edits;
        }
    }
}
