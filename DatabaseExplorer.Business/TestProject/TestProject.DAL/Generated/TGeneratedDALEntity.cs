using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    public interface IGeneratedDALEntity : IGeneratedGenericDALEntity, System.ICloneable
    {
        void Delete();
        void SynchronizeToDatabase();
    }

    public abstract partial class GeneratedDALEntity<TId> : GenericGeneratedDALEntity<TId>, IGeneratedDALEntity
        where TId : struct, IEquatable<TId>
    {
        static readonly bool IsPrimaryObject = typeof(TId) == typeof(IdTuple<long>);

        protected GeneratedDALEntity(TId? id = null)
        {
            if (IsPrimaryObject)
                if (id.HasValue)
                    ClosedId = id;
                else
                    ClosedId = (TId)(object)new IdTuple<long>((long)GeneratedDAO.Instance.CreateUniqueId());
        }



        protected static IEnumerable<T> GetRowsAndConvert<T>(string sqlQueryToGetRows, Func<System.Data.DataRow, T> convert)
        {
            var dt = GeneratedDAO.Instance.WithConnection(con => con.GetDatatable(sqlQueryToGetRows));
            return dt.AsEnumerable().Select(convert);
        }

        public override void LoadById(TId id)
        {
            TId? oldId = IsPrimaryObject && State == eState.New && !ClosedId.Value.Equals(id) ? ClosedId : null;

#pragma warning disable 184
            Debug.Assert(id is IIdTuple, "La DAL utilise exclusivement des structures IdTuple implémentant IIdTuple, il n'y pas directement d'int contrairement au logiciel");
            // ReSharper disable HeuristicUnreachableCode                                                                                                                             
            DbOperationsForMappedItemWithState.AutoLoadItem(this, (IIdTuple)id);
            // ReSharper restore HeuristicUnreachableCode                                                                                                                             
#pragma warning restore 184

            if (oldId.HasValue)
                GeneratedDAO.Instance.CancelCreationOf((ulong)((IdTuple<long>)(object)oldId.Value).Id1);
        }
        public override void SynchronizeToDatabase()
        {
            DbOperationsForMappedItemWithState.SynchronizeToDatabase(this);
        }
        public void Delete()
        {
            //if (Debugger.IsAttached)
            //    Debugger.Break();
            var tran = GeneratedDAO.Instance.CurrentDBConnection == null ? null
                     : GeneratedDAO.Instance.CurrentDBConnection.CurrentActiveTransaction;
            if (tran != null)
            {
                var restoreState = CreateActionToRestoreState();
                tran.OnRollback += restoreState; // Pas besoin de gérer la désinscription car la référence à la transaction disparaitra toute seule                                   
            }
            UpdateState(eState.ToDelete);
            SynchronizeToDatabase();
        }

    }

}

