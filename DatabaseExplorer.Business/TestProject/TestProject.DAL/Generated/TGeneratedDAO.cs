using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    public partial class GeneratedDAO : GenericGeneratedDAO
    {
        public static GeneratedDAO Instance { get; private set; }

        readonly GenericDALIdManager _IdManager;

        protected GeneratedDAO(string stringConnection)
            : this(() => new GenericDBConnection(stringConnection))
        {
        }
        protected GeneratedDAO(Func<IGenericDBConnection> createDBConnection)
            : base(createDBConnection)
        {
            if (Instance != null)
                throw new TechnicalException(GetType().Name + " is supposed to be a singleton! But some code try to instanciate a new one.", null);
            Instance = this;
            _IdManager = new GenericDALIdManager(this, "_IdManager", "SeedOfIds");
        }

        internal ulong CreateUniqueId() { return _IdManager.CreateUniqueId(); }

        internal void CancelCreationOf(ulong id)
        {
            //ds.UpdateState(eState.Deleted);
            //_IdManager.TryRecycleId((ulong)(long)((IHasClosedId)ds).Id);
            _IdManager.TryRecycleId(id);
        }

        public static void Connect(string connectionString)
        {
            Connect(() => new GenericDBConnection(connectionString));
        }
        public static void Connect(Func<IGenericDBConnection> createConnection)
        {
            Instance = new GeneratedDAO(createConnection);
        }

        public override Dictionary<Type, bool> AllTypes
        {
            get
            {
                if (_allTypes != null)
                    return _allTypes;
                _allTypes = new Dictionary<Type, bool>();
                // a regenerer !
                return _allTypes;
            }
        }
        Dictionary<Type, bool> _allTypes;

    }

}

