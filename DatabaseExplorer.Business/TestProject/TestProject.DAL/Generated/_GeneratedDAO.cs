﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TechnicalTools.Model;

using GenericDALBasics;


namespace TestProject.DAL
{
    public class GeneratedDAO : GenericGeneratedDAO
    {
        public static GeneratedDAO Instance { get; private set; }

        readonly GenericDALIdManager _IdManager;

        protected GeneratedDAO(Func<GenericDBConnection> createDBConnection)
            : base(createDBConnection)
        {
            if (Instance != null)
                throw new TechnicalException(GetType().Name + " is supposed to be a singleton! But some code try to instanciate a new one.", null);
            Instance = this;
            _IdManager = new GenericDALIdManager(this);
        }

        internal ulong CreateUniqueId() { return _IdManager.CreateUniqueId(); }

        public static void Connect(string connectionString)
        {
            Connect(() => new GenericDBConnection(connectionString));
        }
        public static void Connect(Func<GenericDBConnection> createConnection)
        {
            Instance = new GeneratedDAO(createConnection);
        }

        public override List<Type> AllTypes
        {
            get
            {
                if (_allTypes != null)
                    return _allTypes;
                _allTypes = new List<Type>();
                return _allTypes;
            }
        }
        List<Type> _allTypes;
    }
}
