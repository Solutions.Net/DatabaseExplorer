using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{

    public partial class GeneratedDAO : GenericGeneratedDAO
    {
        public static GeneratedDAO Instance { get; private set; }

        readonly GenericDALIdManager _IdManager;

        protected GeneratedDAO(string stringConnection)
            : this(() => new GenericDBConnectionSqlCE(stringConnection))
        {
        }
        protected GeneratedDAO(Func<GenericDBConnectionSqlCE> createDBConnection)
            : base(createDBConnection)
        {
            if (Instance != null)
                throw new TechnicalException(GetType().Name + " is supposed to be a singleton! But some code try to instanciate a new one.", null);
            Instance = this;
            _IdManager = new GenericDALIdManager(this, "_IdManager", "SeedOfIds");
        }

        internal ulong CreateUniqueId() { return _IdManager.CreateUniqueId(); }

        internal void CancelCreationOf(ulong id)
        {
            //ds.UpdateState(eState.Deleted);
            //_IdManager.TryRecycleId((ulong)(long)((IHasClosedId)ds).Id);
            _IdManager.TryRecycleId(id);
        }

        public static void Connect(string connectionString)
        {
            Connect(() => new GenericDBConnectionSqlCE(connectionString));
        }
        public static void Connect(Func<GenericDBConnectionSqlCE> createConnection)
        {
            using (var con = createConnection())
                con.ExecuteScalar("select 1");
            Instance = new GeneratedDAO(createConnection);
        }

        public override Dictionary<Type, bool> AllTypes { get {
            if (_allTypes != null)
                return _allTypes;
            _allTypes = new Dictionary<Type, bool>();
            _allTypes.Add(typeof(Dbo.Address), Dbo.Address.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Person), Dbo.Person.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.LogError), Dbo.LogError.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Author), Dbo.Author.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Page), Dbo.Page.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Book), Dbo.Book.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Person_Friend), Dbo.Person_Friend.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Person_History), Dbo.Person_History.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Author_Int), Dbo.Author_Int.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Author_Time), Dbo.Author_Time.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Page_WordDefinition), Dbo.Page_WordDefinition.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Book_Page), Dbo.Book_Page.IsPrimaryObject);
            _allTypes.Add(typeof(Dbo.Book_Reference), Dbo.Book_Reference.IsPrimaryObject);
            return _allTypes;
        } }
        Dictionary<Type, bool> _allTypes;

    }

}
