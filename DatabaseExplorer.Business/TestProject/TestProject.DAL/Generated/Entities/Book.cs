using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Book")]
        [MetadataType(typeof(Book.Metadata))]
        public sealed partial class Book : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Book
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Book);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Book = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Book.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_book_)
            {
                return new IdTuple<long>(id_book_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Book FROM dbo.Book" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Book"])));
            }
    
    
            public static List<Book> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Book> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Book, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Book> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Book, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Book", IsPK = true)]           public   long Id_Book      { [DebuggerStepThrough] get { return      _Id_Book; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Book, value, RaiseIdChanged); } }   long _Id_Book;
            [DebuggerHidden][DbMappedField("Title", IsNullable = true)]       public string Title        { [DebuggerStepThrough] get { return        _Title; } [DebuggerStepThrough] set {            SetProperty(ref                    _Title, value); } } string _Title;
            [DebuggerHidden][DbMappedField("PageCount")]                      public    int PageCount    { [DebuggerStepThrough] get { return    _PageCount; } [DebuggerStepThrough] set {            SetProperty(ref                _PageCount, value); } }    int _PageCount;
            [DebuggerHidden][DbMappedField("TheAuthor_Id")]                   public   long TheAuthor_Id { [DebuggerStepThrough] get { return _TheAuthor_Id; } [DebuggerStepThrough] set {            SetProperty(ref             _TheAuthor_Id, value); } }   long _TheAuthor_Id;
            [DebuggerHidden][DbMappedField("CoAuthor_Id", IsNullable = true)] public  long? CoAuthor_Id  { [DebuggerStepThrough] get { return  _CoAuthor_Id; } [DebuggerStepThrough] set {            SetProperty(ref              _CoAuthor_Id, value); } }  long? _CoAuthor_Id;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Authors__TheAuthor_IdIds { get { return Author.GetIdsWhere("Id_Author = " + TheAuthor_Id); } }
            public List<Author> Via_Authors__TheAuthor_Id { get { return (List<Author>)GeneratedDAO.Instance.GetAllByDbMapper<Author, IdTuple<long>>(Via_Authors__TheAuthor_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Authors__CoAuthor_IdIds { get { return Author.GetIdsWhere(CoAuthor_Id == null ? "0=1" : "Id_Author = " + CoAuthor_Id); } }
            public List<Author> Via_Authors__CoAuthor_Id { get { return (List<Author>)GeneratedDAO.Instance.GetAllByDbMapper<Author, IdTuple<long>>(Via_Authors__CoAuthor_IdIds); } }
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByBook_Pages_Book_IdIds { get { return Book_Page.GetIdsWhere("Book_Id = " + Id_Book); } }
            public List<Book_Page> RefByBook_Pages_Book_Id { get { return (List<Book_Page>)GeneratedDAO.Instance.GetAllByDbMapper<Book_Page, IdTuple<long>>(RefByBook_Pages_Book_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByBook_References_Owner_IdIds { get { return Book_Reference.GetIdsWhere("Owner_Id = " + Id_Book); } }
            public List<Book_Reference> RefByBook_References_Owner_Id { get { return (List<Book_Reference>)GeneratedDAO.Instance.GetAllByDbMapper<Book_Reference, IdTuple<long>>(RefByBook_References_Owner_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByBook_References_Reference_IdIds { get { return Book_Reference.GetIdsWhere("Reference_Id = " + Id_Book); } }
            public List<Book_Reference> RefByBook_References_Reference_Id { get { return (List<Book_Reference>)GeneratedDAO.Instance.GetAllByDbMapper<Book_Reference, IdTuple<long>>(RefByBook_References_Reference_IdIds); } }
    
    
            public Book()
            {
            }
            public Book(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Book> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Book();
                    item._Id_Book = (long)reader.GetValue(0);
                    item._Title = reader.IsDBNull(1) ? null : (string)reader.GetValue(1);
                    item._PageCount = (int)reader.GetValue(2);
                    item._TheAuthor_Id = (long)reader.GetValue(3);
                    item._CoAuthor_Id = reader.IsDBNull(4) ? null : (long?)reader.GetValue(4);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (5): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 5)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Book Clone() { return (Book)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Book(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Book)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Title = from._Title;
                _PageCount = from._PageCount;
                _TheAuthor_Id = from._TheAuthor_Id;
                _CoAuthor_Id = from._CoAuthor_Id;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyBook
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public   long Id_Book      { get; set; }
                public string Title        { get; set; }
                public    int PageCount    { get; set; }
                public   long TheAuthor_Id { get; set; }
                public  long? CoAuthor_Id  { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Title = _Title;
                mem.PageCount = _PageCount;
                mem.TheAuthor_Id = _TheAuthor_Id;
                mem.CoAuthor_Id = _CoAuthor_Id;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Title = mem.Title;
                _PageCount = mem.PageCount;
                _TheAuthor_Id = mem.TheAuthor_Id;
                _CoAuthor_Id = mem.CoAuthor_Id;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyBook
            {
                  long Id_Book      { get; }
            string Title        { get; }
               int PageCount    { get; }
              long TheAuthor_Id { get; }
             long? CoAuthor_Id  { get; }
    
    
            }
            public partial interface Book : ReadOnlyBook
            {
                    IdTuple<long>? ClosedId     { get; }
            new           long Id_Book      { get; set; }
            new         string Title        { get; set; }
            new            int PageCount    { get; set; }
            new           long TheAuthor_Id { get; set; }
            new          long? CoAuthor_Id  { get; set; }
    
    
            }
        }
    }
}
