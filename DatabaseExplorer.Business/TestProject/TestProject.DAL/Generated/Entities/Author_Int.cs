using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Author_Ints")]
        [MetadataType(typeof(Author_Int.Metadata))]
        public sealed partial class Author_Int : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Author_Int
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Author_Ints);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Author_Ints = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Author_Int.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_author_ints_)
            {
                return new IdTuple<long>(id_author_ints_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Author_Ints FROM dbo.Author_Ints" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Author_Ints"])));
            }
    
    
            public static List<Author_Int> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Author_Int> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Author_Int, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Author_Int> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Author_Int, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Author_Ints", IsPK = true)] public long Id_Author_Ints { [DebuggerStepThrough] get { return _Id_Author_Ints; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Author_Ints, value, RaiseIdChanged); } } long _Id_Author_Ints;
            [DebuggerHidden][DbMappedField("Author_Id")]                   public long Author_Id      { [DebuggerStepThrough] get { return      _Author_Id; } [DebuggerStepThrough] set {            SetProperty(ref                       _Author_Id, value); } } long _Author_Id;
            [DebuggerHidden][DbMappedField("Int")]                         public  int Int            { [DebuggerStepThrough] get { return            _Int; } [DebuggerStepThrough] set {            SetProperty(ref                             _Int, value); } }  int _Int;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Authors__Author_IdIds { get { return Author.GetIdsWhere("Id_Author = " + Author_Id); } }
            public List<Author> Via_Authors__Author_Id { get { return (List<Author>)GeneratedDAO.Instance.GetAllByDbMapper<Author, IdTuple<long>>(Via_Authors__Author_IdIds); } }
    
    
    
            public Author_Int()
            {
            }
            public Author_Int(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Author_Int> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Author_Int();
                    item._Id_Author_Ints = (long)reader.GetValue(0);
                    item._Author_Id = (long)reader.GetValue(1);
                    item._Int = (int)reader.GetValue(2);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (3): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 3)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Author_Int Clone() { return (Author_Int)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Author_Int(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Author_Int)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Author_Id = from._Author_Id;
                _Int = from._Int;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyAuthor_Int
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public long Id_Author_Ints { get; set; }
                public long Author_Id      { get; set; }
                public  int Int            { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Author_Id = _Author_Id;
                mem.Int = _Int;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Author_Id = mem.Author_Id;
                _Int = mem.Int;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyAuthor_Int
            {
                long Id_Author_Ints { get; }
            long Author_Id      { get; }
             int Int            { get; }
    
    
            }
            public partial interface Author_Int : ReadOnlyAuthor_Int
            {
                    IdTuple<long>? ClosedId       { get; }
            new           long Id_Author_Ints { get; set; }
            new           long Author_Id      { get; set; }
            new            int Int            { get; set; }
    
    
            }
        }
    }
}
