using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Person_Friends")]
        [MetadataType(typeof(Person_Friend.Metadata))]
        public sealed partial class Person_Friend : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Person_Friend
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Person_Friends);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Person_Friends = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Person_Friend.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_person_friends_)
            {
                return new IdTuple<long>(id_person_friends_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Person_Friends FROM dbo.Person_Friends" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Person_Friends"])));
            }
    
    
            public static List<Person_Friend> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Person_Friend> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Person_Friend, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Person_Friend> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Person_Friend, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Person_Friends", IsPK = true)] public long Id_Person_Friends { [DebuggerStepThrough] get { return _Id_Person_Friends; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Person_Friends, value, RaiseIdChanged); } } long _Id_Person_Friends;
            [DebuggerHidden][DbMappedField("Person_Id")]                      public long Person_Id         { [DebuggerStepThrough] get { return         _Person_Id; } [DebuggerStepThrough] set {            SetProperty(ref                          _Person_Id, value); } } long _Person_Id;
            [DebuggerHidden][DbMappedField("Friend_Id")]                      public long Friend_Id         { [DebuggerStepThrough] get { return         _Friend_Id; } [DebuggerStepThrough] set {            SetProperty(ref                          _Friend_Id, value); } } long _Friend_Id;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_People__Person_IdIds { get { return Person.GetIdsWhere("Id_Person = " + Person_Id); } }
            public List<Person> Via_People__Person_Id { get { return (List<Person>)GeneratedDAO.Instance.GetAllByDbMapper<Person, IdTuple<long>>(Via_People__Person_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_People__Friend_IdIds { get { return Person.GetIdsWhere("Id_Person = " + Friend_Id); } }
            public List<Person> Via_People__Friend_Id { get { return (List<Person>)GeneratedDAO.Instance.GetAllByDbMapper<Person, IdTuple<long>>(Via_People__Friend_IdIds); } }
    
    
    
            public Person_Friend()
            {
            }
            public Person_Friend(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Person_Friend> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Person_Friend();
                    item._Id_Person_Friends = (long)reader.GetValue(0);
                    item._Person_Id = (long)reader.GetValue(1);
                    item._Friend_Id = (long)reader.GetValue(2);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (3): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 3)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Person_Friend Clone() { return (Person_Friend)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Person_Friend(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Person_Friend)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Person_Id = from._Person_Id;
                _Friend_Id = from._Friend_Id;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyPerson_Friend
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public long Id_Person_Friends { get; set; }
                public long Person_Id         { get; set; }
                public long Friend_Id         { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Person_Id = _Person_Id;
                mem.Friend_Id = _Friend_Id;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Person_Id = mem.Person_Id;
                _Friend_Id = mem.Friend_Id;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyPerson_Friend
            {
                long Id_Person_Friends { get; }
            long Person_Id         { get; }
            long Friend_Id         { get; }
    
    
            }
            public partial interface Person_Friend : ReadOnlyPerson_Friend
            {
                    IdTuple<long>? ClosedId          { get; }
            new           long Id_Person_Friends { get; set; }
            new           long Person_Id         { get; set; }
            new           long Friend_Id         { get; set; }
    
    
            }
        }
    }
}
