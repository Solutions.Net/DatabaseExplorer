using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Page_WordDefinitions")]
        [MetadataType(typeof(Page_WordDefinition.Metadata))]
        public sealed partial class Page_WordDefinition : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Page_WordDefinition
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Page_WordDefinitions);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Page_WordDefinitions = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Page_WordDefinition.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_page_worddefinitions_)
            {
                return new IdTuple<long>(id_page_worddefinitions_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Page_WordDefinitions FROM dbo.Page_WordDefinitions" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Page_WordDefinitions"])));
            }
    
    
            public static List<Page_WordDefinition> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Page_WordDefinition> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Page_WordDefinition, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Page_WordDefinition> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Page_WordDefinition, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Page_WordDefinitions", IsPK = true)] public   long Id_Page_WordDefinitions { [DebuggerStepThrough] get { return _Id_Page_WordDefinitions; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Id_Page_WordDefinitions, value, RaiseIdChanged); } }   long _Id_Page_WordDefinitions;
            [DebuggerHidden][DbMappedField("Owner_Id")]                             public   long Owner_Id                { [DebuggerStepThrough] get { return                _Owner_Id; } [DebuggerStepThrough] set {                                        SetProperty(ref                                 _Owner_Id, value); } }   long _Owner_Id;
            [DebuggerHidden][DbMappedField("Key"), DbMaxLength(446)]                public string Key                     { [DebuggerStepThrough] get { return                     _Key; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 446);          SetProperty(ref                                      _Key, value); } } string _Key;
            [DebuggerHidden][DbMappedField("WordDefinition")]                       public string WordDefinition          { [DebuggerStepThrough] get { return          _WordDefinition; } [DebuggerStepThrough] set {                                        SetProperty(ref                           _WordDefinition, value); } } string _WordDefinition;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Pages__Owner_IdIds { get { return Page.GetIdsWhere("Id_Page = " + Owner_Id); } }
            public List<Page> Via_Pages__Owner_Id { get { return (List<Page>)GeneratedDAO.Instance.GetAllByDbMapper<Page, IdTuple<long>>(Via_Pages__Owner_IdIds); } }
    
    
    
            public Page_WordDefinition()
            {
            }
            public Page_WordDefinition(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
                _Key = string.Empty;
                _WordDefinition = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Page_WordDefinition> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Page_WordDefinition();
                    item._Id_Page_WordDefinitions = (long)reader.GetValue(0);
                    item._Owner_Id = (long)reader.GetValue(1);
                    item._Key = (string)reader.GetValue(2);
                    item._WordDefinition = (string)reader.GetValue(3);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (4): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 4)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Page_WordDefinition Clone() { return (Page_WordDefinition)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Page_WordDefinition(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Page_WordDefinition)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Owner_Id = from._Owner_Id;
                _Key = from._Key;
                _WordDefinition = from._WordDefinition;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyPage_WordDefinition
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public   long Id_Page_WordDefinitions { get; set; }
                public   long Owner_Id                { get; set; }
                public string Key                     { get; set; }
                public string WordDefinition          { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Owner_Id = _Owner_Id;
                mem.Key = _Key;
                mem.WordDefinition = _WordDefinition;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Owner_Id = mem.Owner_Id;
                _Key = mem.Key;
                _WordDefinition = mem.WordDefinition;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyPage_WordDefinition
            {
                  long Id_Page_WordDefinitions { get; }
              long Owner_Id                { get; }
            string Key                     { get; }
            string WordDefinition          { get; }
    
    
            }
            public partial interface Page_WordDefinition : ReadOnlyPage_WordDefinition
            {
                    IdTuple<long>? ClosedId                { get; }
            new           long Id_Page_WordDefinitions { get; set; }
            new           long Owner_Id                { get; set; }
            new         string Key                     { get; set; }
            new         string WordDefinition          { get; set; }
    
    
            }
        }
    }
}
