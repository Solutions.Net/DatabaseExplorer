using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("dbo.AAA_Dico")]
        [MetadataType(typeof(AAA_Dico.Metadata))]
        public sealed partial class AAA_Dico : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.AAA_Dico
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_AAA_Dico);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_AAA_Dico = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.AAA_Dico.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_aaa_dico_)
            {
                return new IdTuple<long>(id_aaa_dico_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_AAA_Dico FROM dbo.AAA_Dico" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_AAA_Dico"])));
            }
    
    
            public static List<AAA_Dico> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<AAA_Dico> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<AAA_Dico, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<AAA_Dico> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<AAA_Dico, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_AAA_Dico", IsPK = true)] public     long Id_AAA_Dico { [DebuggerStepThrough] get { return _Id_AAA_Dico; } [DebuggerStepThrough] set {                                                   SetTechnicalProperty(ref  _Id_AAA_Dico, value, RaiseIdChanged); } }     long _Id_AAA_Dico;
            [DebuggerHidden][DbMappedField("Owner_Id")]                 public     long Owner_Id    { [DebuggerStepThrough] get { return    _Owner_Id; } [DebuggerStepThrough] set {                                                            SetProperty(ref                     _Owner_Id, value); } }     long _Owner_Id;
            [DebuggerHidden][DbMappedField("Key")]                      public DateTime Key         { [DebuggerStepThrough] get { return         _Key; } [DebuggerStepThrough] set {  ChkRange(value, MinValueOf.Key, MaxValueOf.Key);          SetProperty(ref                          _Key, value); } } DateTime _Key;
            [DebuggerHidden][DbMappedField("Dico_Id")]                  public      int Dico_Id     { [DebuggerStepThrough] get { return     _Dico_Id; } [DebuggerStepThrough] set {                                                            SetProperty(ref                      _Dico_Id, value); } }      int _Dico_Id;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_AAAS__Owner_IdIds { get { return AAA.GetIdsWhere("Id_AAA = " + Owner_Id); } }
            public List<AAA> Via_AAAS__Owner_Id { get { return (List<AAA>)GeneratedDAO.Instance.GetAllByDbMapper<AAA, IdTuple<long>>(Via_AAAS__Owner_IdIds); } }
    
    
    
            public AAA_Dico()
            {
            }
            public AAA_Dico(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<AAA_Dico> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new AAA_Dico();
                    item._Id_AAA_Dico = (long)reader.GetValue(0);
                    item._Owner_Id = (long)reader.GetValue(1);
                    item._Key = (DateTime)reader.GetValue(2);
                    item._Dico_Id = (int)reader.GetValue(3);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (4): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 4)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public AAA_Dico Clone() { return (AAA_Dico)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new AAA_Dico(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (AAA_Dico)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Owner_Id = from._Owner_Id;
                _Key = from._Key;
                _Dico_Id = from._Dico_Id;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime Key = new DateTime(0); // 0001-01-01T00:00:00.0000000
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime Key = new DateTime(3155378975999990000); // 9999-12-31T23:59:59.9990000
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface AAA_Dico
            {
                IdTuple<long>? ClosedId    { get; }
                      long Id_AAA_Dico { get; set; }
                      long Owner_Id    { get; set; }
                  DateTime Key         { get; set; }
                       int Dico_Id     { get; set; }
    
    
            }
        }
    }
}
