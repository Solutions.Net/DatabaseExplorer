using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Page")]
        [MetadataType(typeof(Page.Metadata))]
        public sealed partial class Page : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Page
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Page);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Page = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Page.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_page_)
            {
                return new IdTuple<long>(id_page_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Page FROM dbo.Page" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Page"])));
            }
    
    
            public static List<Page> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Page> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Page, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Page> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Page, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Page", IsPK = true)]       public   long Id_Page { [DebuggerStepThrough] get { return _Id_Page; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Page, value, RaiseIdChanged); } }   long _Id_Page;
            [DebuggerHidden][DbMappedField("Index")]                      public    int Index   { [DebuggerStepThrough] get { return   _Index; } [DebuggerStepThrough] set {            SetProperty(ref                    _Index, value); } }    int _Index;
            [DebuggerHidden][DbMappedField("Content", IsNullable = true)] public string Content { [DebuggerStepThrough] get { return _Content; } [DebuggerStepThrough] set {            SetProperty(ref                  _Content, value); } } string _Content;
    
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByPage_WordDefinitions_Owner_IdIds { get { return Page_WordDefinition.GetIdsWhere("Owner_Id = " + Id_Page); } }
            public List<Page_WordDefinition> RefByPage_WordDefinitions_Owner_Id { get { return (List<Page_WordDefinition>)GeneratedDAO.Instance.GetAllByDbMapper<Page_WordDefinition, IdTuple<long>>(RefByPage_WordDefinitions_Owner_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByBook_Pages_Page_IdIds { get { return Book_Page.GetIdsWhere("Page_Id = " + Id_Page); } }
            public List<Book_Page> RefByBook_Pages_Page_Id { get { return (List<Book_Page>)GeneratedDAO.Instance.GetAllByDbMapper<Book_Page, IdTuple<long>>(RefByBook_Pages_Page_IdIds); } }
    
    
            public Page()
            {
            }
            public Page(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Page> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Page();
                    item._Id_Page = (long)reader.GetValue(0);
                    item._Index = (int)reader.GetValue(1);
                    item._Content = reader.IsDBNull(2) ? null : (string)reader.GetValue(2);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (3): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 3)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Page Clone() { return (Page)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Page(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Page)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Index = from._Index;
                _Content = from._Content;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyPage
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public   long Id_Page { get; set; }
                public    int Index   { get; set; }
                public string Content { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Index = _Index;
                mem.Content = _Content;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Index = mem.Index;
                _Content = mem.Content;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyPage
            {
                  long Id_Page { get; }
               int Index   { get; }
            string Content { get; }
    
    
            }
            public partial interface Page : ReadOnlyPage
            {
                    IdTuple<long>? ClosedId { get; }
            new           long Id_Page  { get; set; }
            new            int Index    { get; set; }
            new         string Content  { get; set; }
    
    
            }
        }
    }
}
