using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Book_Pages")]
        [MetadataType(typeof(Book_Page.Metadata))]
        public sealed partial class Book_Page : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Book_Page
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Book_Pages);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Book_Pages = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Book_Page.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_book_pages_)
            {
                return new IdTuple<long>(id_book_pages_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Book_Pages FROM dbo.Book_Pages" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Book_Pages"])));
            }
    
    
            public static List<Book_Page> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Book_Page> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Book_Page, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Book_Page> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Book_Page, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Book_Pages", IsPK = true)] public long Id_Book_Pages { [DebuggerStepThrough] get { return _Id_Book_Pages; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Book_Pages, value, RaiseIdChanged); } } long _Id_Book_Pages;
            [DebuggerHidden][DbMappedField("Book_Id")]                    public long Book_Id       { [DebuggerStepThrough] get { return       _Book_Id; } [DebuggerStepThrough] set {            SetProperty(ref                        _Book_Id, value); } } long _Book_Id;
            [DebuggerHidden][DbMappedField("Page_Id")]                    public long Page_Id       { [DebuggerStepThrough] get { return       _Page_Id; } [DebuggerStepThrough] set {            SetProperty(ref                        _Page_Id, value); } } long _Page_Id;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Books__Book_IdIds { get { return Book.GetIdsWhere("Id_Book = " + Book_Id); } }
            public List<Book> Via_Books__Book_Id { get { return (List<Book>)GeneratedDAO.Instance.GetAllByDbMapper<Book, IdTuple<long>>(Via_Books__Book_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Pages__Page_IdIds { get { return Page.GetIdsWhere("Id_Page = " + Page_Id); } }
            public List<Page> Via_Pages__Page_Id { get { return (List<Page>)GeneratedDAO.Instance.GetAllByDbMapper<Page, IdTuple<long>>(Via_Pages__Page_IdIds); } }
    
    
    
            public Book_Page()
            {
            }
            public Book_Page(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Book_Page> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Book_Page();
                    item._Id_Book_Pages = (long)reader.GetValue(0);
                    item._Book_Id = (long)reader.GetValue(1);
                    item._Page_Id = (long)reader.GetValue(2);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (3): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 3)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Book_Page Clone() { return (Book_Page)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Book_Page(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Book_Page)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Book_Id = from._Book_Id;
                _Page_Id = from._Page_Id;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyBook_Page
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public long Id_Book_Pages { get; set; }
                public long Book_Id       { get; set; }
                public long Page_Id       { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Book_Id = _Book_Id;
                mem.Page_Id = _Page_Id;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Book_Id = mem.Book_Id;
                _Page_Id = mem.Page_Id;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyBook_Page
            {
                long Id_Book_Pages { get; }
            long Book_Id       { get; }
            long Page_Id       { get; }
    
    
            }
            public partial interface Book_Page : ReadOnlyBook_Page
            {
                    IdTuple<long>? ClosedId      { get; }
            new           long Id_Book_Pages { get; set; }
            new           long Book_Id       { get; set; }
            new           long Page_Id       { get; set; }
    
    
            }
        }
    }
}
