using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("dbo.AAA")]
        [MetadataType(typeof(AAA.Metadata))]
        public sealed partial class AAA : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.AAA
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_AAA);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_AAA = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.AAA.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_aaa_)
            {
                return new IdTuple<long>(id_aaa_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_AAA FROM dbo.AAA" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_AAA"])));
            }
    
    
            public static List<AAA> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<AAA> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<AAA, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<AAA> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<AAA, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_AAA", IsPK = true)] public long Id_AAA { [DebuggerStepThrough] get { return _Id_AAA; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_AAA, value, RaiseIdChanged); } } long _Id_AAA;
    
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByAAA_Dicos_Owner_IdIds { get { return AAA_Dico.GetIdsWhere("Owner_Id = " + Id_AAA); } }
            public List<AAA_Dico> RefByAAA_Dicos_Owner_Id { get { return (List<AAA_Dico>)GeneratedDAO.Instance.GetAllByDbMapper<AAA_Dico, IdTuple<long>>(RefByAAA_Dicos_Owner_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByAAA_Dico2s_Owner_IdIds { get { return AAA_Dico2.GetIdsWhere("Owner_Id = " + Id_AAA); } }
            public List<AAA_Dico2> RefByAAA_Dico2s_Owner_Id { get { return (List<AAA_Dico2>)GeneratedDAO.Instance.GetAllByDbMapper<AAA_Dico2, IdTuple<long>>(RefByAAA_Dico2s_Owner_IdIds); } }
    
    
            public AAA()
            {
            }
            public AAA(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<AAA> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new AAA();
                    item._Id_AAA = (long)reader.GetValue(0);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (1): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 1)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public AAA Clone() { return (AAA)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new AAA(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (AAA)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface AAA
            {
                IdTuple<long>? ClosedId { get; }
                      long Id_AAA   { get; set; }
    
    
            }
        }
    }
}
