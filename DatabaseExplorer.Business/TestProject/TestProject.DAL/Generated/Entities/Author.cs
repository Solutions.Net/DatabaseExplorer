using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Author")]
        [MetadataType(typeof(Author.Metadata))]
        public sealed partial class Author : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Author
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Author);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Author = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Author.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_author_)
            {
                return new IdTuple<long>(id_author_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Author FROM dbo.Author" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Author"])));
            }
    
    
            public static List<Author> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Author> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Author, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Author> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Author, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Author", IsPK = true)]                  public   long Id_Author            { [DebuggerStepThrough] get { return            _Id_Author; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Author, value, RaiseIdChanged); } }   long _Id_Author;
            [DebuggerHidden][DbMappedField("NickName", IsNullable = true)]             public string NickName             { [DebuggerStepThrough] get { return             _NickName; } [DebuggerStepThrough] set {            SetProperty(ref                   _NickName, value); } } string _NickName;
            [DebuggerHidden][DbMappedField("ArrivingTime")]                            public Byte[] ArrivingTime         { [DebuggerStepThrough] get { return         _ArrivingTime; } [DebuggerStepThrough] set {            SetProperty(ref               _ArrivingTime, value); } } Byte[] _ArrivingTime;
            [DebuggerHidden][DbMappedField("ArrivingTimeNullable", IsNullable = true)] public Byte[] ArrivingTimeNullable { [DebuggerStepThrough] get { return _ArrivingTimeNullable; } [DebuggerStepThrough] set {            SetProperty(ref       _ArrivingTimeNullable, value); } } Byte[] _ArrivingTimeNullable;
    
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByBooks_TheAuthor_IdIds { get { return Book.GetIdsWhere("TheAuthor_Id = " + Id_Author); } }
            public List<Book> RefByBooks_TheAuthor_Id { get { return (List<Book>)GeneratedDAO.Instance.GetAllByDbMapper<Book, IdTuple<long>>(RefByBooks_TheAuthor_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByBooks_CoAuthor_IdIds { get { return Book.GetIdsWhere("CoAuthor_Id = " + Id_Author); } }
            public List<Book> RefByBooks_CoAuthor_Id { get { return (List<Book>)GeneratedDAO.Instance.GetAllByDbMapper<Book, IdTuple<long>>(RefByBooks_CoAuthor_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByAuthor_Ints_Author_IdIds { get { return Author_Int.GetIdsWhere("Author_Id = " + Id_Author); } }
            public List<Author_Int> RefByAuthor_Ints_Author_Id { get { return (List<Author_Int>)GeneratedDAO.Instance.GetAllByDbMapper<Author_Int, IdTuple<long>>(RefByAuthor_Ints_Author_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByAuthor_Times_Author_IdIds { get { return Author_Time.GetIdsWhere("Author_Id = " + Id_Author); } }
            public List<Author_Time> RefByAuthor_Times_Author_Id { get { return (List<Author_Time>)GeneratedDAO.Instance.GetAllByDbMapper<Author_Time, IdTuple<long>>(RefByAuthor_Times_Author_IdIds); } }
    
    
            public Author()
            {
            }
            public Author(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Author> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Author();
                    item._Id_Author = (long)reader.GetValue(0);
                    item._NickName = reader.IsDBNull(1) ? null : (string)reader.GetValue(1);
                    item._ArrivingTime = (Byte[])reader.GetValue(2);
                    item._ArrivingTimeNullable = reader.IsDBNull(3) ? null : (Byte[])reader.GetValue(3);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (4): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 4)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Author Clone() { return (Author)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Author(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Author)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _NickName = from._NickName;
                _ArrivingTime = from._ArrivingTime;
                _ArrivingTimeNullable = from._ArrivingTimeNullable;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyAuthor
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public   long Id_Author            { get; set; }
                public string NickName             { get; set; }
                public Byte[] ArrivingTime         { get; set; }
                public Byte[] ArrivingTimeNullable { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.NickName = _NickName;
                mem.ArrivingTime = _ArrivingTime;
                mem.ArrivingTimeNullable = _ArrivingTimeNullable;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _NickName = mem.NickName;
                _ArrivingTime = mem.ArrivingTime;
                _ArrivingTimeNullable = mem.ArrivingTimeNullable;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyAuthor
            {
                  long Id_Author            { get; }
            string NickName             { get; }
            Byte[] ArrivingTime         { get; }
            Byte[] ArrivingTimeNullable { get; }
    
    
            }
            public partial interface Author : ReadOnlyAuthor
            {
                    IdTuple<long>? ClosedId             { get; }
            new           long Id_Author            { get; set; }
            new         string NickName             { get; set; }
            new         Byte[] ArrivingTime         { get; set; }
            new         Byte[] ArrivingTimeNullable { get; set; }
    
    
            }
        }
    }
}
