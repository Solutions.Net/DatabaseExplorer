using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Book_References")]
        [MetadataType(typeof(Book_Reference.Metadata))]
        public sealed partial class Book_Reference : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Book_Reference
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Book_References);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Book_References = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Book_Reference.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_book_references_)
            {
                return new IdTuple<long>(id_book_references_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Book_References FROM dbo.Book_References" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Book_References"])));
            }
    
    
            public static List<Book_Reference> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Book_Reference> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Book_Reference, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Book_Reference> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Book_Reference, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Book_References", IsPK = true)] public   long Id_Book_References { [DebuggerStepThrough] get { return _Id_Book_References; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Id_Book_References, value, RaiseIdChanged); } }   long _Id_Book_References;
            [DebuggerHidden][DbMappedField("Owner_Id")]                        public   long Owner_Id           { [DebuggerStepThrough] get { return           _Owner_Id; } [DebuggerStepThrough] set {                                        SetProperty(ref                            _Owner_Id, value); } }   long _Owner_Id;
            [DebuggerHidden][DbMappedField("Key"), DbMaxLength(446)]           public string Key                { [DebuggerStepThrough] get { return                _Key; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 446);          SetProperty(ref                                 _Key, value); } } string _Key;
            [DebuggerHidden][DbMappedField("Reference_Id")]                    public   long Reference_Id       { [DebuggerStepThrough] get { return       _Reference_Id; } [DebuggerStepThrough] set {                                        SetProperty(ref                        _Reference_Id, value); } }   long _Reference_Id;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Books__Owner_IdIds { get { return Book.GetIdsWhere("Id_Book = " + Owner_Id); } }
            public List<Book> Via_Books__Owner_Id { get { return (List<Book>)GeneratedDAO.Instance.GetAllByDbMapper<Book, IdTuple<long>>(Via_Books__Owner_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Books__Reference_IdIds { get { return Book.GetIdsWhere("Id_Book = " + Reference_Id); } }
            public List<Book> Via_Books__Reference_Id { get { return (List<Book>)GeneratedDAO.Instance.GetAllByDbMapper<Book, IdTuple<long>>(Via_Books__Reference_IdIds); } }
    
    
    
            public Book_Reference()
            {
            }
            public Book_Reference(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
                _Key = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Book_Reference> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Book_Reference();
                    item._Id_Book_References = (long)reader.GetValue(0);
                    item._Owner_Id = (long)reader.GetValue(1);
                    item._Key = (string)reader.GetValue(2);
                    item._Reference_Id = (long)reader.GetValue(3);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (4): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 4)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Book_Reference Clone() { return (Book_Reference)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Book_Reference(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Book_Reference)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Owner_Id = from._Owner_Id;
                _Key = from._Key;
                _Reference_Id = from._Reference_Id;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyBook_Reference
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public   long Id_Book_References { get; set; }
                public   long Owner_Id           { get; set; }
                public string Key                { get; set; }
                public   long Reference_Id       { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Owner_Id = _Owner_Id;
                mem.Key = _Key;
                mem.Reference_Id = _Reference_Id;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Owner_Id = mem.Owner_Id;
                _Key = mem.Key;
                _Reference_Id = mem.Reference_Id;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyBook_Reference
            {
                  long Id_Book_References { get; }
              long Owner_Id           { get; }
            string Key                { get; }
              long Reference_Id       { get; }
    
    
            }
            public partial interface Book_Reference : ReadOnlyBook_Reference
            {
                    IdTuple<long>? ClosedId           { get; }
            new           long Id_Book_References { get; set; }
            new           long Owner_Id           { get; set; }
            new         string Key                { get; set; }
            new           long Reference_Id       { get; set; }
    
    
            }
        }
    }
}
