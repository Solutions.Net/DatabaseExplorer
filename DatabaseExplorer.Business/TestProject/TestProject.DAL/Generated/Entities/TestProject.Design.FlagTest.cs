using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("dbo.TestProject.Design.FlagTests")]
        [MetadataType(typeof(TestProject.Design.FlagTest.Metadata))]
        public sealed partial class TestProject.Design.FlagTest : GeneratedDALEntity<IdTuple<int>>, HasPropertiesOf.Dbo.TestProject.Design.FlagTest
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(Id_TestProject_Design_FlagTests);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_TestProject_Design_FlagTests = (int)value.Value.Keys[0];
                }
            }
            IdTuple<int>? HasPropertiesOf.Dbo.TestProject.Design.FlagTest.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int id_testproject_design_flagtests_)
            {
                return new IdTuple<int>(id_testproject_design_flagtests_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_TestProject.Design.FlagTests FROM dbo.TestProject.Design.FlagTests" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["Id_TestProject.Design.FlagTests"])));
            }
    
    
            public static List<TestProject.Design.FlagTest> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<TestProject.Design.FlagTest> GetEntitiesWithIds(IEnumerable<IdTuple<int>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<TestProject.Design.FlagTest, IdTuple<int>>(ids);
                return entities;
            }
    
    
            public static List<TestProject.Design.FlagTest> GetEntitiesWithIds(IEnumerable<int> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<TestProject.Design.FlagTest, IdTuple<int>>(ids.Select(id => new IdTuple<int>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_TestProject.Design.FlagTests", IsPK = true)] public    int Id_TestProject_Design_FlagTests { [DebuggerStepThrough] get { return _Id_TestProject_Design_FlagTests; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_TestProject_Design_FlagTests, value, RaiseIdChanged); } }    int _Id_TestProject_Design_FlagTests;
            [DebuggerHidden][DbMappedField("Name")]                                         public string Name                            { [DebuggerStepThrough] get { return                            _Name; } [DebuggerStepThrough] set {            SetProperty(ref                                             _Name, value); } } string _Name;
    
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByAAA_Dicos_Dico_IdIds { get { return AAA_Dico.GetIdsWhere("Dico_Id = " + Id_TestProject_Design_FlagTests); } }
            public List<AAA_Dico> RefByAAA_Dicos_Dico_Id { get { return (List<AAA_Dico>)GeneratedDAO.Instance.GetAllByDbMapper<AAA_Dico, IdTuple<long>>(RefByAAA_Dicos_Dico_IdIds); } }
    
    
            public TestProject.Design.FlagTest()
            {
            }
            public TestProject.Design.FlagTest(bool initializeModelValues, IdTuple<int>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<TestProject.Design.FlagTest> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new TestProject.Design.FlagTest();
                    item._Id_TestProject_Design_FlagTests = (int)reader.GetValue(0);
                    item._Name = (string)reader.GetValue(1);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (2): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 2)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public TestProject.Design.FlagTest Clone() { return (TestProject.Design.FlagTest)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<int>> CreateNewInstance() { return new TestProject.Design.FlagTest(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<int>> source)
            {
                var from = (TestProject.Design.FlagTest)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Name = from._Name;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface TestProject.Design.FlagTest
            {
                IdTuple<int>? ClosedId                        { get; }
                      int Id_TestProject_Design_FlagTests { get; set; }
                   string Name                            { get; set; }
    
    
            }
        }
    }
}
