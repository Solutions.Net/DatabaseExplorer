using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Address")]
        [MetadataType(typeof(Address.Metadata))]
        public sealed partial class Address : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Address
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Address);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Address = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Address.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_address_)
            {
                return new IdTuple<long>(id_address_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Address FROM dbo.Address" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Address"])));
            }
    
    
            public static List<Address> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Address> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Address, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Address> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Address, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Address", IsPK = true)]                    public   long Id_Address              { [DebuggerStepThrough] get { return              _Id_Address; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Address, value, RaiseIdChanged); } }   long _Id_Address;
            [DebuggerHidden][DbMappedField("AddressLine1", IsNullable = true)]            public string AddressLine1            { [DebuggerStepThrough] get { return            _AddressLine1; } [DebuggerStepThrough] set {            SetProperty(ref                _AddressLine1, value); } } string _AddressLine1;
            [DebuggerHidden][DbMappedField("AddressLine2", IsNullable = true)]            public string AddressLine2            { [DebuggerStepThrough] get { return            _AddressLine2; } [DebuggerStepThrough] set {            SetProperty(ref                _AddressLine2, value); } } string _AddressLine2;
            [DebuggerHidden][DbMappedField("City", IsNullable = true)]                    public string City                    { [DebuggerStepThrough] get { return                    _City; } [DebuggerStepThrough] set {            SetProperty(ref                        _City, value); } } string _City;
            [DebuggerHidden][DbMappedField("StateOrProvinceOrRegion", IsNullable = true)] public string StateOrProvinceOrRegion { [DebuggerStepThrough] get { return _StateOrProvinceOrRegion; } [DebuggerStepThrough] set {            SetProperty(ref     _StateOrProvinceOrRegion, value); } } string _StateOrProvinceOrRegion;
            [DebuggerHidden][DbMappedField("ZipPostalCode", IsNullable = true)]           public string ZipPostalCode           { [DebuggerStepThrough] get { return           _ZipPostalCode; } [DebuggerStepThrough] set {            SetProperty(ref               _ZipPostalCode, value); } } string _ZipPostalCode;
            [DebuggerHidden][DbMappedField("Countr", IsNullable = true)]                  public string Countr                  { [DebuggerStepThrough] get { return                  _Countr; } [DebuggerStepThrough] set {            SetProperty(ref                      _Countr, value); } } string _Countr;
    
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByPeople_Address_IdIds { get { return Person.GetIdsWhere("Address_Id = " + Id_Address); } }
            public List<Person> RefByPeople_Address_Id { get { return (List<Person>)GeneratedDAO.Instance.GetAllByDbMapper<Person, IdTuple<long>>(RefByPeople_Address_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByPeople_Address2_IdIds { get { return Person.GetIdsWhere("Address2_Id = " + Id_Address); } }
            public List<Person> RefByPeople_Address2_Id { get { return (List<Person>)GeneratedDAO.Instance.GetAllByDbMapper<Person, IdTuple<long>>(RefByPeople_Address2_IdIds); } }
    
    
            public Address()
            {
            }
            public Address(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Address> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Address();
                    item._Id_Address = (long)reader.GetValue(0);
                    item._AddressLine1 = reader.IsDBNull(1) ? null : (string)reader.GetValue(1);
                    item._AddressLine2 = reader.IsDBNull(2) ? null : (string)reader.GetValue(2);
                    item._City = reader.IsDBNull(3) ? null : (string)reader.GetValue(3);
                    item._StateOrProvinceOrRegion = reader.IsDBNull(4) ? null : (string)reader.GetValue(4);
                    item._ZipPostalCode = reader.IsDBNull(5) ? null : (string)reader.GetValue(5);
                    item._Countr = reader.IsDBNull(6) ? null : (string)reader.GetValue(6);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (7): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 7)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Address Clone() { return (Address)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Address(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Address)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _AddressLine1 = from._AddressLine1;
                _AddressLine2 = from._AddressLine2;
                _City = from._City;
                _StateOrProvinceOrRegion = from._StateOrProvinceOrRegion;
                _ZipPostalCode = from._ZipPostalCode;
                _Countr = from._Countr;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyAddress
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public   long Id_Address              { get; set; }
                public string AddressLine1            { get; set; }
                public string AddressLine2            { get; set; }
                public string City                    { get; set; }
                public string StateOrProvinceOrRegion { get; set; }
                public string ZipPostalCode           { get; set; }
                public string Countr                  { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.AddressLine1 = _AddressLine1;
                mem.AddressLine2 = _AddressLine2;
                mem.City = _City;
                mem.StateOrProvinceOrRegion = _StateOrProvinceOrRegion;
                mem.ZipPostalCode = _ZipPostalCode;
                mem.Countr = _Countr;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _AddressLine1 = mem.AddressLine1;
                _AddressLine2 = mem.AddressLine2;
                _City = mem.City;
                _StateOrProvinceOrRegion = mem.StateOrProvinceOrRegion;
                _ZipPostalCode = mem.ZipPostalCode;
                _Countr = mem.Countr;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyAddress
            {
                  long Id_Address              { get; }
            string AddressLine1            { get; }
            string AddressLine2            { get; }
            string City                    { get; }
            string StateOrProvinceOrRegion { get; }
            string ZipPostalCode           { get; }
            string Countr                  { get; }
    
    
            }
            public partial interface Address : ReadOnlyAddress
            {
                    IdTuple<long>? ClosedId                { get; }
            new           long Id_Address              { get; set; }
            new         string AddressLine1            { get; set; }
            new         string AddressLine2            { get; set; }
            new         string City                    { get; set; }
            new         string StateOrProvinceOrRegion { get; set; }
            new         string ZipPostalCode           { get; set; }
            new         string Countr                  { get; set; }
    
    
            }
        }
    }
}
