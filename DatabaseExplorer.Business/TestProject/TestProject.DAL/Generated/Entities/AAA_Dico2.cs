using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Forms;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("dbo.AAA_Dico2")]
        [MetadataType(typeof(AAA_Dico2.Metadata))]
        public sealed partial class AAA_Dico2 : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.AAA_Dico2
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_AAA_Dico2);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_AAA_Dico2 = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.AAA_Dico2.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_aaa_dico2_)
            {
                return new IdTuple<long>(id_aaa_dico2_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_AAA_Dico2 FROM dbo.AAA_Dico2" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_AAA_Dico2"])));
            }
    
    
            public static List<AAA_Dico2> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<AAA_Dico2> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<AAA_Dico2, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<AAA_Dico2> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<AAA_Dico2, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_AAA_Dico2", IsPK = true)] public   long Id_AAA_Dico2 { [DebuggerStepThrough] get { return _Id_AAA_Dico2; } [DebuggerStepThrough] set {                               SetTechnicalProperty(ref  _Id_AAA_Dico2, value, RaiseIdChanged); } }   long _Id_AAA_Dico2;
            [DebuggerHidden][DbMappedField("Owner_Id")]                  public   long Owner_Id     { [DebuggerStepThrough] get { return     _Owner_Id; } [DebuggerStepThrough] set {                                        SetProperty(ref                      _Owner_Id, value); } }   long _Owner_Id;
            [DebuggerHidden][DbMappedField("Key"), DbMaxLength(446)]     public string Key          { [DebuggerStepThrough] get { return          _Key; } [DebuggerStepThrough] set {  ChkFieldLen(ref value, 446);          SetProperty(ref                           _Key, value); } } string _Key;
            [DebuggerHidden][DbMappedField("Dico2")]                     public string Dico2        { [DebuggerStepThrough] get { return        _Dico2; } [DebuggerStepThrough] set {                                        SetProperty(ref                         _Dico2, value); } } string _Dico2;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_AAAS__Owner_IdIds { get { return AAA.GetIdsWhere("Id_AAA = " + Owner_Id); } }
            public List<AAA> Via_AAAS__Owner_Id { get { return (List<AAA>)GeneratedDAO.Instance.GetAllByDbMapper<AAA, IdTuple<long>>(Via_AAAS__Owner_IdIds); } }
    
    
    
            public AAA_Dico2()
            {
            }
            public AAA_Dico2(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
                _Key = string.Empty;
                _Dico2 = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<AAA_Dico2> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new AAA_Dico2();
                    item._Id_AAA_Dico2 = (long)reader.GetValue(0);
                    item._Owner_Id = (long)reader.GetValue(1);
                    item._Key = (string)reader.GetValue(2);
                    item._Dico2 = (string)reader.GetValue(3);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (4): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 4)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public AAA_Dico2 Clone() { return (AAA_Dico2)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new AAA_Dico2(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (AAA_Dico2)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Owner_Id = from._Owner_Id;
                _Key = from._Key;
                _Dico2 = from._Dico2;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface AAA_Dico2
            {
                IdTuple<long>? ClosedId     { get; }
                      long Id_AAA_Dico2 { get; set; }
                      long Owner_Id     { get; set; }
                    string Key          { get; set; }
                    string Dico2        { get; set; }
    
    
            }
        }
    }
}
