using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Author_Times")]
        [MetadataType(typeof(Author_Time.Metadata))]
        public sealed partial class Author_Time : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Author_Time
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Author_Times);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Author_Times = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Author_Time.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_author_times_)
            {
                return new IdTuple<long>(id_author_times_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Author_Times FROM dbo.Author_Times" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Author_Times"])));
            }
    
    
            public static List<Author_Time> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Author_Time> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Author_Time, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Author_Time> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Author_Time, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Author_Times", IsPK = true)] public   long Id_Author_Times { [DebuggerStepThrough] get { return _Id_Author_Times; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Author_Times, value, RaiseIdChanged); } }   long _Id_Author_Times;
            [DebuggerHidden][DbMappedField("Author_Id")]                    public   long Author_Id       { [DebuggerStepThrough] get { return       _Author_Id; } [DebuggerStepThrough] set {            SetProperty(ref                        _Author_Id, value); } }   long _Author_Id;
            [DebuggerHidden][DbMappedField("Time")]                         public Byte[] Time            { [DebuggerStepThrough] get { return            _Time; } [DebuggerStepThrough] set {            SetProperty(ref                             _Time, value); } } Byte[] _Time;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Authors__Author_IdIds { get { return Author.GetIdsWhere("Id_Author = " + Author_Id); } }
            public List<Author> Via_Authors__Author_Id { get { return (List<Author>)GeneratedDAO.Instance.GetAllByDbMapper<Author, IdTuple<long>>(Via_Authors__Author_IdIds); } }
    
    
    
            public Author_Time()
            {
            }
            public Author_Time(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Author_Time> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Author_Time();
                    item._Id_Author_Times = (long)reader.GetValue(0);
                    item._Author_Id = (long)reader.GetValue(1);
                    item._Time = (Byte[])reader.GetValue(2);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (3): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 3)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Author_Time Clone() { return (Author_Time)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Author_Time(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Author_Time)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Author_Id = from._Author_Id;
                _Time = from._Time;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyAuthor_Time
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public   long Id_Author_Times { get; set; }
                public   long Author_Id       { get; set; }
                public Byte[] Time            { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Author_Id = _Author_Id;
                mem.Time = _Time;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Author_Id = mem.Author_Id;
                _Time = mem.Time;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyAuthor_Time
            {
                  long Id_Author_Times { get; }
              long Author_Id       { get; }
            Byte[] Time            { get; }
    
    
            }
            public partial interface Author_Time : ReadOnlyAuthor_Time
            {
                    IdTuple<long>? ClosedId        { get; }
            new           long Id_Author_Times { get; set; }
            new           long Author_Id       { get; set; }
            new         Byte[] Time            { get; set; }
    
    
            }
        }
    }
}
