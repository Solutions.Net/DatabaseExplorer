using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("LogError")]
        [MetadataType(typeof(LogError.Metadata))]
        public sealed partial class LogError : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.LogError
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_LogError);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_LogError = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.LogError.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_logerror_)
            {
                return new IdTuple<long>(id_logerror_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_LogError FROM dbo.LogError" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_LogError"])));
            }
    
    
            public static List<LogError> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<LogError> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<LogError, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<LogError> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<LogError, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_LogError", IsPK = true)]        public     long Id_LogError  { [DebuggerStepThrough] get { return  _Id_LogError; } [DebuggerStepThrough] set {                                                     SetTechnicalProperty(ref  _Id_LogError, value, RaiseIdChanged); } }     long _Id_LogError;
            [DebuggerHidden][DbMappedField("When")]                            public DateTime When         { [DebuggerStepThrough] get { return         _When; } [DebuggerStepThrough] set {  ChkRange(value, MinValueOf.When, MaxValueOf.When);          SetProperty(ref                         _When, value); } } DateTime _When;
            [DebuggerHidden][DbMappedField("Method", IsNullable = true)]       public   string Method       { [DebuggerStepThrough] get { return       _Method; } [DebuggerStepThrough] set {                                                              SetProperty(ref                       _Method, value); } }   string _Method;
            [DebuggerHidden][DbMappedField("Args", IsNullable = true)]         public   string Args         { [DebuggerStepThrough] get { return         _Args; } [DebuggerStepThrough] set {                                                              SetProperty(ref                         _Args, value); } }   string _Args;
            [DebuggerHidden][DbMappedField("ErrorMessage", IsNullable = true)] public   string ErrorMessage { [DebuggerStepThrough] get { return _ErrorMessage; } [DebuggerStepThrough] set {                                                              SetProperty(ref                 _ErrorMessage, value); } }   string _ErrorMessage;
            [DebuggerHidden][DbMappedField("FullLog", IsNullable = true)]      public   string FullLog      { [DebuggerStepThrough] get { return      _FullLog; } [DebuggerStepThrough] set {                                                              SetProperty(ref                      _FullLog, value); } }   string _FullLog;
    
    
    
    
            public LogError()
            {
            }
            public LogError(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<LogError> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new LogError();
                    item._Id_LogError = (long)reader.GetValue(0);
                    item._When = (DateTime)reader.GetValue(1);
                    item._Method = reader.IsDBNull(2) ? null : (string)reader.GetValue(2);
                    item._Args = reader.IsDBNull(3) ? null : (string)reader.GetValue(3);
                    item._ErrorMessage = reader.IsDBNull(4) ? null : (string)reader.GetValue(4);
                    item._FullLog = reader.IsDBNull(5) ? null : (string)reader.GetValue(5);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (6): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 6)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public LogError Clone() { return (LogError)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new LogError(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (LogError)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _When = from._When;
                _Method = from._Method;
                _Args = from._Args;
                _ErrorMessage = from._ErrorMessage;
                _FullLog = from._FullLog;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyLogError
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public     long Id_LogError  { get; set; }
                public DateTime When         { get; set; }
                public   string Method       { get; set; }
                public   string Args         { get; set; }
                public   string ErrorMessage { get; set; }
                public   string FullLog      { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.When = _When;
                mem.Method = _Method;
                mem.Args = _Args;
                mem.ErrorMessage = _ErrorMessage;
                mem.FullLog = _FullLog;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _When = mem.When;
                _Method = mem.Method;
                _Args = mem.Args;
                _ErrorMessage = mem.ErrorMessage;
                _FullLog = mem.FullLog;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime When = new DateTime(0); // 0001-01-01T00:00:00.0000000
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime When = new DateTime(3155378975999990000); // 9999-12-31T23:59:59.9990000
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyLogError
            {
                    long Id_LogError  { get; }
            DateTime When         { get; }
              string Method       { get; }
              string Args         { get; }
              string ErrorMessage { get; }
              string FullLog      { get; }
    
    
            }
            public partial interface LogError : ReadOnlyLogError
            {
                    IdTuple<long>? ClosedId     { get; }
            new           long Id_LogError  { get; set; }
            new       DateTime When         { get; set; }
            new         string Method       { get; set; }
            new         string Args         { get; set; }
            new         string ErrorMessage { get; set; }
            new         string FullLog      { get; set; }
    
    
            }
        }
    }
}
