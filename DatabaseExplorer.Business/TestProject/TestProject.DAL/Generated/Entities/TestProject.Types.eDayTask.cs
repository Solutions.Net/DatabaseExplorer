using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("TestProject.Types.eDayTask")]
        [MetadataType(typeof(TestProject.Types.eDayTask.Metadata))]
        public sealed partial class TestProject.Types.eDayTask : GeneratedDALEntity<IdTuple<int>>, HasPropertiesOf.Dbo.TestProject.Types.eDayTask
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(Id_TestProject_Types_eDayTask);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_TestProject_Types_eDayTask = (int)value.Value.Keys[0];
                }
            }
            IdTuple<int>? HasPropertiesOf.Dbo.TestProject.Types.eDayTask.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int id_testproject_types_edaytask_)
            {
                return new IdTuple<int>(id_testproject_types_edaytask_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_TestProject.Types.eDayTask FROM dbo.TestProject.Types.eDayTask" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["Id_TestProject.Types.eDayTask"])));
            }
    
    
            public static List<TestProject.Types.eDayTask> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<TestProject.Types.eDayTask> GetEntitiesWithIds(IEnumerable<IdTuple<int>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<TestProject.Types.eDayTask, IdTuple<int>>(ids);
                return entities;
            }
    
    
            public static List<TestProject.Types.eDayTask> GetEntitiesWithIds(IEnumerable<int> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<TestProject.Types.eDayTask, IdTuple<int>>(ids.Select(id => new IdTuple<int>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_TestProject.Types.eDayTask", IsPK = true)] public    int Id_TestProject_Types_eDayTask { [DebuggerStepThrough] get { return _Id_TestProject_Types_eDayTask; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_TestProject_Types_eDayTask, value, RaiseIdChanged); } }    int _Id_TestProject_Types_eDayTask;
            [DebuggerHidden][DbMappedField("Name")]                                       public string Name                          { [DebuggerStepThrough] get { return                          _Name; } [DebuggerStepThrough] set {            SetProperty(ref                                           _Name, value); } } string _Name;
    
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByPerson_Histories_History_IdIds { get { return Person_History.GetIdsWhere("History_Id = " + Id_TestProject_Types_eDayTask); } }
            public List<Person_History> RefByPerson_Histories_History_Id { get { return (List<Person_History>)GeneratedDAO.Instance.GetAllByDbMapper<Person_History, IdTuple<long>>(RefByPerson_Histories_History_IdIds); } }
    
    
            public TestProject.Types.eDayTask()
            {
            }
            public TestProject.Types.eDayTask(bool initializeModelValues, IdTuple<int>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<TestProject.Types.eDayTask> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new TestProject.Types.eDayTask();
                    item._Id_TestProject_Types_eDayTask = (int)reader.GetValue(0);
                    item._Name = (string)reader.GetValue(1);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (2): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 2)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public TestProject.Types.eDayTask Clone() { return (TestProject.Types.eDayTask)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<int>> CreateNewInstance() { return new TestProject.Types.eDayTask(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<int>> source)
            {
                var from = (TestProject.Types.eDayTask)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Name = from._Name;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyTestProject.Types.eDayTask
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public    int Id_TestProject_Types_eDayTask { get; set; }
                public string Name                          { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Name = _Name;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Name = mem.Name;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyTestProject.Types.eDayTask
            {
                   int Id_TestProject_Types_eDayTask { get; }
            string Name                          { get; }
    
    
            }
            public partial interface TestProject.Types.eDayTask : ReadOnlyTestProject.Types.eDayTask
            {
                    IdTuple<int>? ClosedId                      { get; }
            new           int Id_TestProject_Types_eDayTask { get; set; }
            new        string Name                          { get; set; }
    
    
            }
        }
    }
}
