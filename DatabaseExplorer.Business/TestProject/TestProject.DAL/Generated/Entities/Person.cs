using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Person")]
        [MetadataType(typeof(Person.Metadata))]
        public sealed partial class Person : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Person
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Person);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Person = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Person.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_person_)
            {
                return new IdTuple<long>(id_person_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Person FROM dbo.Person" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Person"])));
            }
    
    
            public static List<Person> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Person> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Person, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Person> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Person, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Person", IsPK = true)]         public   long Id_Person   { [DebuggerStepThrough] get { return   _Id_Person; } [DebuggerStepThrough] set {   SetTechnicalProperty(ref  _Id_Person, value, RaiseIdChanged); } }   long _Id_Person;
            [DebuggerHidden][DbMappedField("Firstname", IsNullable = true)]   public string Firstname   { [DebuggerStepThrough] get { return   _Firstname; } [DebuggerStepThrough] set {            SetProperty(ref                  _Firstname, value); } } string _Firstname;
            [DebuggerHidden][DbMappedField("Lastname", IsNullable = true)]    public string Lastname    { [DebuggerStepThrough] get { return    _Lastname; } [DebuggerStepThrough] set {            SetProperty(ref                   _Lastname, value); } } string _Lastname;
            [DebuggerHidden][DbMappedField("Address_Id")]                     public   long Address_Id  { [DebuggerStepThrough] get { return  _Address_Id; } [DebuggerStepThrough] set {            SetProperty(ref                 _Address_Id, value); } }   long _Address_Id;
            [DebuggerHidden][DbMappedField("Address2_Id", IsNullable = true)] public  long? Address2_Id { [DebuggerStepThrough] get { return _Address2_Id; } [DebuggerStepThrough] set {            SetProperty(ref                _Address2_Id, value); } }  long? _Address2_Id;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Addresses__Address_IdIds { get { return Address.GetIdsWhere("Id_Address = " + Address_Id); } }
            public List<Address> Via_Addresses__Address_Id { get { return (List<Address>)GeneratedDAO.Instance.GetAllByDbMapper<Address, IdTuple<long>>(Via_Addresses__Address_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_Addresses__Address2_IdIds { get { return Address.GetIdsWhere(Address2_Id == null ? "0=1" : "Id_Address = " + Address2_Id); } }
            public List<Address> Via_Addresses__Address2_Id { get { return (List<Address>)GeneratedDAO.Instance.GetAllByDbMapper<Address, IdTuple<long>>(Via_Addresses__Address2_IdIds); } }
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByPerson_Friends_Person_IdIds { get { return Person_Friend.GetIdsWhere("Person_Id = " + Id_Person); } }
            public List<Person_Friend> RefByPerson_Friends_Person_Id { get { return (List<Person_Friend>)GeneratedDAO.Instance.GetAllByDbMapper<Person_Friend, IdTuple<long>>(RefByPerson_Friends_Person_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByPerson_Friends_Friend_IdIds { get { return Person_Friend.GetIdsWhere("Friend_Id = " + Id_Person); } }
            public List<Person_Friend> RefByPerson_Friends_Friend_Id { get { return (List<Person_Friend>)GeneratedDAO.Instance.GetAllByDbMapper<Person_Friend, IdTuple<long>>(RefByPerson_Friends_Friend_IdIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<long>> RefByPerson_Histories_Owner_IdIds { get { return Person_History.GetIdsWhere("Owner_Id = " + Id_Person); } }
            public List<Person_History> RefByPerson_Histories_Owner_Id { get { return (List<Person_History>)GeneratedDAO.Instance.GetAllByDbMapper<Person_History, IdTuple<long>>(RefByPerson_Histories_Owner_IdIds); } }
    
    
            public Person()
            {
            }
            public Person(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Person> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Person();
                    item._Id_Person = (long)reader.GetValue(0);
                    item._Firstname = reader.IsDBNull(1) ? null : (string)reader.GetValue(1);
                    item._Lastname = reader.IsDBNull(2) ? null : (string)reader.GetValue(2);
                    item._Address_Id = (long)reader.GetValue(3);
                    item._Address2_Id = reader.IsDBNull(4) ? null : (long?)reader.GetValue(4);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (5): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 5)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Person Clone() { return (Person)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Person(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Person)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Firstname = from._Firstname;
                _Lastname = from._Lastname;
                _Address_Id = from._Address_Id;
                _Address2_Id = from._Address2_Id;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyPerson
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public   long Id_Person   { get; set; }
                public string Firstname   { get; set; }
                public string Lastname    { get; set; }
                public   long Address_Id  { get; set; }
                public  long? Address2_Id { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Firstname = _Firstname;
                mem.Lastname = _Lastname;
                mem.Address_Id = _Address_Id;
                mem.Address2_Id = _Address2_Id;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Firstname = mem.Firstname;
                _Lastname = mem.Lastname;
                _Address_Id = mem.Address_Id;
                _Address2_Id = mem.Address2_Id;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyPerson
            {
                  long Id_Person   { get; }
            string Firstname   { get; }
            string Lastname    { get; }
              long Address_Id  { get; }
             long? Address2_Id { get; }
    
    
            }
            public partial interface Person : ReadOnlyPerson
            {
                    IdTuple<long>? ClosedId    { get; }
            new           long Id_Person   { get; set; }
            new         string Firstname   { get; set; }
            new         string Lastname    { get; set; }
            new           long Address_Id  { get; set; }
            new          long? Address2_Id { get; set; }
    
    
            }
        }
    }
}
