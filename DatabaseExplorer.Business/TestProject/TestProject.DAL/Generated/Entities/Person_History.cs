using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

using GenericDALBasics;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;


namespace TestProject.DAL
{
    namespace Dbo
    {
        [DbMappedTable("Person_History")]
        [MetadataType(typeof(Person_History.Metadata))]
        public sealed partial class Person_History : GeneratedDALEntity<IdTuple<long>>, HasPropertiesOf.Dbo.Person_History
        {
            protected override IdTuple<long>? ClosedId
            {
                get
                {
                    return new IdTuple<long>(Id_Person_History);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id_Person_History = (long)value.Value.Keys[0];
                }
            }
            IdTuple<long>? HasPropertiesOf.Dbo.Person_History.ClosedId { get { return ClosedId; } }
            public static readonly Type IdType = typeof(IdTuple<long>);
    
            public static IdTuple<long> MakeClosedId(long id_person_history_)
            {
                return new IdTuple<long>(id_person_history_);
            }
    
    
    
            public static IEnumerable<IdTuple<long>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Id_Person_History FROM dbo.Person_History" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                        row => new IdTuple<long>(GetValueAs<long>(row["Id_Person_History"])));
            }
    
    
            public static List<Person_History> GetEntitiesWhere(string sqlFilter)
            {
                return GetEntitiesWithIds(GetIdsWhere(sqlFilter));
            }
    
    
            public static List<Person_History> GetEntitiesWithIds(IEnumerable<IdTuple<long>> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Person_History, IdTuple<long>>(ids);
                return entities;
            }
    
    
            public static List<Person_History> GetEntitiesWithIds(IEnumerable<long> ids)
            {
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Person_History, IdTuple<long>>(ids.Select(id => new IdTuple<long>(id)));
                return entities;
            }
    
    
            [DebuggerHidden][DbMappedField("Id_Person_History", IsPK = true)] public     long Id_Person_History { [DebuggerStepThrough] get { return _Id_Person_History; } [DebuggerStepThrough] set {                                                   SetTechnicalProperty(ref  _Id_Person_History, value, RaiseIdChanged); } }     long _Id_Person_History;
            [DebuggerHidden][DbMappedField("Owner_Id")]                       public     long Owner_Id          { [DebuggerStepThrough] get { return          _Owner_Id; } [DebuggerStepThrough] set {                                                            SetProperty(ref                           _Owner_Id, value); } }     long _Owner_Id;
            [DebuggerHidden][DbMappedField("Key")]                            public DateTime Key               { [DebuggerStepThrough] get { return               _Key; } [DebuggerStepThrough] set {  ChkRange(value, MinValueOf.Key, MaxValueOf.Key);          SetProperty(ref                                _Key, value); } } DateTime _Key;
            [DebuggerHidden][DbMappedField("History_Id")]                     public      int History_Id        { [DebuggerStepThrough] get { return        _History_Id; } [DebuggerStepThrough] set {                                                            SetProperty(ref                         _History_Id, value); } }      int _History_Id;
    
            [Browsable(false)] public IEnumerable<IdTuple<long>> Via_People__Owner_IdIds { get { return Person.GetIdsWhere("Id_Person = " + Owner_Id); } }
            public List<Person> Via_People__Owner_Id { get { return (List<Person>)GeneratedDAO.Instance.GetAllByDbMapper<Person, IdTuple<long>>(Via_People__Owner_IdIds); } }
    
    
    
            public Person_History()
            {
            }
            public Person_History(bool initializeModelValues, IdTuple<long>? id = null)
                : base(id)
            {
                if (!initializeModelValues)
                    return;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.Common.DbDataReader reader, List<Person_History> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Person_History();
                    item._Id_Person_History = (long)reader.GetValue(0);
                    item._Owner_Id = (long)reader.GetValue(1);
                    item._Key = (DateTime)reader.GetValue(2);
                    item._History_Id = (int)reader.GetValue(3);
                    item.InitializeState(eState.Synchronized);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (4): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 4)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Person_History Clone() { return (Person_History)(this as ICloneable).Clone(); }
            protected override GenericGeneratedDALEntity<IdTuple<long>> CreateNewInstance() { return new Person_History(); }
            public override void CopyAllFieldsFrom(GenericGeneratedDALEntity<IdTuple<long>> source)
            {
                var from = (Person_History)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Owner_Id = from._Owner_Id;
                _Key = from._Key;
                _History_Id = from._History_Id;
            }
    
            #endregion
    
    
            #region Memento
    
            public new interface IMemento : PropertyNotifierObject.IMemento, HasPropertiesOf.Dbo.ReadOnlyPerson_History
            {
            }
            /*protected*/ new class Memento : PropertyNotifierObject.Memento, IMemento
            {
                public     long Id_Person_History { get; set; }
                public     long Owner_Id          { get; set; }
                public DateTime Key               { get; set; }
                public      int History_Id        { get; set; }
            }
            public override TechnicalTools.Model.IMemento CreateMemento()
            {
                var mem = new Memento();
                FillMemento(mem);
                return mem;
            }
            /*protected*/ void FillMemento(Memento mem)
            {
                base.FillMemento(mem);
    
                mem.Owner_Id = _Owner_Id;
                mem.Key = _Key;
                mem.History_Id = _History_Id;
            }
            protected override void RestoreMemento(TechnicalTools.Model.IMemento mem_)
            {
                var mem = (Memento)mem_;
                base.RestoreMemento(mem);
    
                _Owner_Id = mem.Owner_Id;
                _Key = mem.Key;
                _History_Id = mem.History_Id;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime Key = new DateTime(0); // 0001-01-01T00:00:00.0000000
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime Key = new DateTime(3155378975999990000); // 9999-12-31T23:59:59.9990000
            }
    
            #endregion
            internal static class Metadata
            {
    
            }
        }
    }
    namespace HasPropertiesOf
    {
        namespace Dbo
        {
            public partial interface ReadOnlyPerson_History
            {
                    long Id_Person_History { get; }
                long Owner_Id          { get; }
            DateTime Key               { get; }
                 int History_Id        { get; }
    
    
            }
            public partial interface Person_History : ReadOnlyPerson_History
            {
                    IdTuple<long>? ClosedId          { get; }
            new           long Id_Person_History { get; set; }
            new           long Owner_Id          { get; set; }
            new       DateTime Key               { get; set; }
            new            int History_Id        { get; set; }
    
    
            }
        }
    }
}
