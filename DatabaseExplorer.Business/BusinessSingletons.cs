﻿using System;

using SuperBase = ApplicationBase.Business;
using Base = ApplicationBase.Business;


namespace DatabaseExplorer.Business
{
    public class BusinessSingletons : Base.BusinessSingletons
    {
        // Please See "Note 01" in file "ApplicationBase.Common/Code Design Notes.txt"
        public new static BusinessSingletons Instance
        {
            get { return _Instance/* ?? (BootstrapConfig)ApplicationBase.Common.BootstrapConfig.Instance*/; }
            set
            {
                _Instance = value;
                if (Base.BusinessSingletons.Instance == null ||
                    value != null && Base.BusinessSingletons.Instance.GetType().IsAssignableFrom(value.GetType()))
                    Base.BusinessSingletons.Instance = value;
            }
        }
        static BusinessSingletons _Instance;
        static BusinessSingletons()
        {
            Instance = new BusinessSingletons();
        }

        public    new                BusinessFactory GetBusinessFactory()    { return (BusinessFactory)base.GetBusinessFactory(); }
        protected override SuperBase.BusinessFactory CreateBusinessFactory() { return new BusinessFactory(); }
    }
}
