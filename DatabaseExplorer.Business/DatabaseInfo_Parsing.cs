﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;

//using DbObject = DatabaseExplorer.DAL.Old.SQLProcedureModelManager.DbObject;
//using DbObjectType = DatabaseExplorer.DAL.Old.SQLProcedureModelManager.DbObjectType;


namespace DatabaseExplorer.DAL
{
    public class DatabaseInfo_Parsing
    {
        public SqlParser DefaultParser { get; private set; }


        public DatabaseInfo_Parsing()
        {
            DefaultParser = new SqlParser(this);

        }



        //public bool Save(string filename, bool embed_files = true)
        //{
        //    XElement root;
        //    bool res = Save(out root, embed_files, Path.GetDirectoryName(filename));
        //    root.Save(filename);
        //    return res;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folder_path_for_files"></param>
        /// <returns>True si la sauvegarde est complete, false si elle a été annulée</returns>
        //private bool Save(out XElement root, bool embed_files = true, string folder_path_for_files = "")
        //{
        //    if (folder_path_for_files.EndsWith("" + Path.DirectorySeparatorChar))
        //        folder_path_for_files = folder_path_for_files.Remove(folder_path_for_files.Length - 1);
        //    root = new XElement("SqlProject");

        //    var nConnection = new XElement("Connection");
        //    Connection.ExportElement(nConnection);
        //    root.Add(nConnection);

        //    foreach (var Lst in ExportableLists)
        //    {
        //        var nLst = new XElement(Lst.Folder);
        //        root.Add(nLst);

        //        foreach (SqlObject obj in Lst.Objects)
        //        {
        //            string file = folder_path_for_files + Path.DirectorySeparatorChar + Lst.Folder
        //                    + Path.DirectorySeparatorChar + obj.RawInfo.Name + ".sql";

        //            string source = null;
        //            if (!embed_files)
        //            {
        //                if (File.Exists(file))
        //                {
        //                    var res = MessageBox.Show("Ecraser le fichier \"" + file + "\" ?", Application.ProductName, MessageBoxButtons.YesNoCancel);
        //                    if (res == DialogResult.Cancel)
        //                        return false;
        //                    if (res == DialogResult.No)
        //                        continue;
        //                }
        //                File.WriteAllText(file, obj.RawInfo.Source);
        //                source = obj.RawInfo.Source;
        //                obj.RawInfo.Source = "";
        //            }
        //            XElement nObj = obj.RawInfo.Export();
        //            if (!embed_files)
        //            {
        //                nObj.Add(new XAttribute("SourceFile", file));
        //                obj.RawInfo.Source = source;
        //            }
        //            nLst.Add(nObj);
        //        }

        //    }
        //    return true;
        //}

        public void LoadFromFolder(string folder_path, XElement root)
        {
            if (folder_path.EndsWith("" + Path.DirectorySeparatorChar))
                folder_path = folder_path.Remove(folder_path.Length - 1);

            var nConnection = root.Element("Connection");
            //Connection = new ConnectionSetting();
            //Connection.ImportElement(nConnection);

            //foreach (var Lst in ExportableLists)
            //{
            //    var nLst = root.Element(Lst.Folder);

            //    string[] files = Directory.GetFiles(folder_path + Path.DirectorySeparatorChar + Lst.Folder,
            //                                        "*.sql");
            //    Lst.Objects.Clear(); // Nettoie la liste qu'on doit remplir
            //    var nLstElements = nLst.Elements("DbOjbect").ToList();
            //    foreach (var file in files)
            //    {
            //        var nObj = nLstElements.First();

            //        var db_obj = DbObject.FromXElement(nObj);
            //        var attSourceFile = nObj.Attribute("SourceFile");
            //        if (attSourceFile != null)
            //            db_obj.Source = File.ReadAllText(db_obj.Source);
            //        Lst.Objects.Add(db_obj);

            //        nLstElements.RemoveAt(0);
            //    }
            //}
        }


    }
}
