﻿using System;
using System.Collections.Generic;
using System.Linq;

using Antlr4.Runtime;
using Antlr4.Runtime.Tree;

using SqlParsingGenerator;

using DatabaseExplorer.Business.Graphviz;


namespace DatabaseExplorer.Business.SqlAnalysing
{

    // A Lire pour accelerer le parsing : 
    // https://github.com/parrt/antlr4/blob/case-insensitivity-doc/doc/resources/CaseChangingCharStream.cs
    public class SqlAnalyser
    {
        public ScriptAnalysisResult Analyse(string sqlScript)
        {
            //var results = new Dictionary<string, ScriptAnalysisResult>();
            //foreach (var ruleName in TSqlParser.ruleNames)
            //{
                //var inputStream = new StreamReader(Console.OpenStandardInput());
                var input = new AntlrInputStream(sqlScript); // new AntlrInputStream(inputStream.ReadLine());
                // Upper because parser does not handle lower character
                var upper = new CaseChangingCharStream(input, true);
                var lexer = new TSqlLexer(upper);
                var tokens = new CommonTokenStream(lexer);
                var parser = new TSqlParser(tokens);

                var errorListener = new ErrorListener();
                lexer.RemoveErrorListeners();
                lexer.AddErrorListener(errorListener);
                parser.RemoveErrorListeners();
                parser.AddErrorListener(errorListener);

                parser.ErrorHandler = new ErrorStrategy();

                // Lance le parsing.
                // Note : on est supposé savoir quel regle doit etre parsé
                // Astuce ici pour itérer sur toutes les règles de niveau 0: https://stackoverflow.com/questions/41261553/antlr-get-first-production
                //var m = parser.GetType().GetMethod(ruleName);
                //IParseTree tree = (IParseTree)m.Invoke(parser, null);
                IParseTree tree = parser.batch(); // Lance le parsing
                                                  //Console.WriteLine(tree);

            // from http://stackoverflow.com/questions/15050137/once-grammar-is-complete-whats-the-best-way-to-walk-an-antlr-v4-tree?rq=1
            //ParseTreeWalker.Default.Walk(new VBListener(), tree);

                //if (parser.NumberOfSyntaxErrors == 0)
                //{
                    //results.Add(ruleName,
                    return 
                    new ScriptAnalysisResult()
                    {
                        ErrorCount = parser.NumberOfSyntaxErrors,
                        Errors = errorListener.Errors.ToList(),
                        Parser = parser,
                        Tree = tree,
                    };
                    //);
                //}
            //}
            //return results.FirstOrDefault().Value;
        }


        public class ScriptAnalysisResult
        {
            public   int          ErrorCount { get; internal set; }
            public   List<string> Errors     { get; internal set; }

            internal Parser       Parser     { get; set; }
            public   IParseTree   Tree       { get; internal set; }

            public string GetParsingDetailInfo() { return Tree.ToStringTree(Parser); }

            public string GetDotDisplay() { return _DotDisplay = _DotDisplay ?? (Tree == null ? null 
                                                               : new DotGenerator().ConvertToGraphiz(Tree).ToDot()); }
            string _DotDisplay;
        }

        public class MyVisitor : TSqlParserBaseVisitor<int>
        {
        }
    }
}
