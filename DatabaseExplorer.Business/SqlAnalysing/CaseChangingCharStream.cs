﻿using System;

using Antlr4.Runtime;
using Antlr4.Runtime.Misc;


namespace DatabaseExplorer.Business.SqlAnalysing
{
    /// <summary>
    /// From https://github.com/antlr/grammars-v4/tree/master/tsql
    /// then https://github.com/parrt/antlr4/blob/case-insensitivity-doc/doc/resources/CaseChangingCharStream.cs
    /// This class supports case-insensitive lexing by wrapping an existing
    /// <see cref="ICharStream"/> and forcing the lexer to see either upper or
    /// lowercase characters. Grammar literals should then be either upper or
    /// lower case such as 'BEGIN' or 'begin'. The text of the character
    /// stream is unaffected. Example: input 'BeGiN' would match lexer rule
    /// 'BEGIN' if constructor parameter upper=true but getText() would return
    /// 'BeGiN'.
    /// </summary>
    public class CaseChangingCharStream : ICharStream
    {
        /// <summary>
        /// Constructs a new CaseChangingCharStream wrapping the given <paramref name="stream"/> forcing
        /// all characters to upper case or lower case.
        /// </summary>
        /// <param name="stream">The stream to wrap.</param>
        /// <param name="upper">If true force each symbol to upper case, otherwise force to lower.</param>
        public CaseChangingCharStream(ICharStream stream, bool upper)
        {
            _stream = stream;
            _upper = upper;
        }
        readonly ICharStream _stream;
        readonly bool _upper;

        [return: NotNull] public string GetText([NotNull] Interval interval) { return _stream.GetText(interval); }

        public int La(int i)
        {
            int c = _stream.La(i);

            if (c <= 0)
                return c;

            char o = (char)c;

            if (_upper)
                return char.ToUpperInvariant(o);

            return char.ToLowerInvariant(o);
        }

        public int    Index               { get { return _stream.Index; } }
        public int    Size                { get { return _stream.Size; } }
        public string SourceName          { get { return _stream.SourceName; } }
        public void   Consume()           { _stream.Consume(); }
        public int    Mark()              { return _stream.Mark(); }
        public void   Release(int marker) { _stream.Release(marker); }
        public void   Seek(int index)     { _stream.Seek(index); }
    }
}
