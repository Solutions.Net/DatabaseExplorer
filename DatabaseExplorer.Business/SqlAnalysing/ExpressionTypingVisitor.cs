﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

using ANTLR_Test;
using Antlr4.Runtime.Tree;
using ANTLR4_Grammar;

using TechnicalTools.Diagnostics;

using DotNode = DatabaseExplorer.Business.Graphviz.Node;


namespace DatabaseExplorer.Business.SqlAnalysing
{
    public class ExpressionTypingVisitor : VisualBasic6BaseVisitor<object> // CalculatorBaseVisitor<Node>
    {
        public class TypeInfo
        {
            public Type Type { get; set; }
            public bool IsAssignable { get; set; }
            public IParseTree Declaration { get; set; }
        }

        public class Scope
        {
            public Scope Parent { get; private set; }
            Dictionary<string, IParseTree> _declarations = new Dictionary<string, IParseTree>();

            public Scope(Scope parent)
            {
                Parent = parent;
            }

            IParseTree GetDeclaration(string name)
            {
                Debug.Assert(!string.IsNullOrWhiteSpace(name));
                Debug.Assert(name == name.Trim());

                IParseTree node = null;
                if (_declarations.TryGetValue(name.Trim(), out node))
                    return node;
                Debug.Assert(Parent != null, "Not Found :(");
                return Parent.GetDeclaration(name);
            }

            public void Add(string name, IParseTree node)
            {
                Debug.Assert(!string.IsNullOrWhiteSpace(name));
                Debug.Assert(name == name.Trim());

                IParseTree n = null;
                Debug.Assert(!_declarations.TryGetValue(name, out n));
                _declarations.Add(name, node);
            }
        }
        Stack<Scope> _Scopes = new Stack<Scope>();

        public ExpressionTypingVisitor()
        {
        }

        public ParseTreeProperty<TypeInfo> Run(IParseTree root)
        {
            _Scopes.Clear();
            _Scopes.Push(new Scope(null));
            _TypeInfos = new ParseTreeProperty<TypeInfo>();
            root.Accept(this);
            return _TypeInfos;
        }

        ParseTreeProperty<TypeInfo> _TypeInfos;

        public void NotHandledNode(IParseTree node)
        {
            string methodToOverride = "Visit" + node.GetType().Name.Replace("Context", "");
            DebugTools.Break();
        }

        public override object VisitTerminal(ITerminalNode node)
        {
           // String tokenName = VisualBasic6Lexer.tokenNames .getSymbolicName(type);
            return base.VisitTerminal(node);
        }

        public override object VisitStartRule(VisualBasic6Parser.StartRuleContext context) { return base.VisitStartRule(context); }
        public override object VisitModule(VisualBasic6Parser.ModuleContext context) { return base.VisitModule(context); }
        public override object VisitModuleBody(VisualBasic6Parser.ModuleBodyContext context) { return base.VisitModuleBody(context); }
        public override object VisitModuleBodyElement(VisualBasic6Parser.ModuleBodyElementContext context) { return base.VisitModuleBodyElement(context); }
        public override object VisitSubStmt(VisualBasic6Parser.SubStmtContext context) 
        {
            // On part du principe que comme c'est du VBscript, la declaration a toujours lieu avant l'utilisation
            // (Ce qui n'est pas vrai dans les langages moderne)
            _Scopes.Peek().Add(context.ambiguousIdentifier().GetText(), context);
            _Scopes.Push(new Scope(_Scopes.Peek()));
            try
            {
                foreach (var arg in context.argList().GetRuleContexts<VisualBasic6Parser.ArgContext>())
                {
                    _Scopes.Peek().Add(arg.ambiguousIdentifier().GetText(), arg);
                }
                return base.VisitSubStmt(context); 
            }
            finally
            {
                _Scopes.Pop();
            }
        }
        

        public override object VisitFunctionStmt(VisualBasic6Parser.FunctionStmtContext context)
        {
            // On part du principe que comme c'est du VBscript, la declaration a toujours lieu avant l'utilisation
            // (Ce qui n'est pas vrai dans les langages moderne)
            _Scopes.Peek().Add(context.ambiguousIdentifier().GetText(), context);
            _Scopes.Push(new Scope(_Scopes.Peek()));
            try
            {
                foreach (var arg in context.argList().GetRuleContexts<VisualBasic6Parser.ArgContext>())
                {
                    _Scopes.Peek().Add(arg.ambiguousIdentifier().GetText(), arg);
                }
                return base.VisitFunctionStmt(context);
            }
            finally
            {
                _Scopes.Pop();
            }
        }

        public override object VisitVariableStmt(VisualBasic6Parser.VariableStmtContext context)
        {
            foreach (var variable in context.variableListStmt().GetRuleContexts<VisualBasic6Parser.VariableSubStmtContext>())
                _Scopes.Peek().Add(variable.ambiguousIdentifier().GetText(), variable);
            return base.VisitVariableStmt(context);
        }

        public override object VisitAmbiguousIdentifier(VisualBasic6Parser.AmbiguousIdentifierContext context) { return base.VisitAmbiguousIdentifier(context); }
        public override object VisitArgList(VisualBasic6Parser.ArgListContext context) { return base.VisitArgList(context); }
        public override object VisitBlock(VisualBasic6Parser.BlockContext context) { return base.VisitBlock(context); }
        public override object VisitBlockStmt(VisualBasic6Parser.BlockStmtContext context) { return base.VisitBlockStmt(context); }
        public override object VisitSelectCaseStmt(VisualBasic6Parser.SelectCaseStmtContext context) {

            return base.VisitSelectCaseStmt(context); 
        
        }
        public override object VisitVsICS(VisualBasic6Parser.VsICSContext context) { return base.VisitVsICS(context); }
        public override object VisitImplicitCallStmt_InStmt(VisualBasic6Parser.ImplicitCallStmt_InStmtContext context) { return base.VisitImplicitCallStmt_InStmt(context); }
        public override object VisitICS_S_VariableOrProcedureCall(VisualBasic6Parser.ICS_S_VariableOrProcedureCallContext context) { return base.VisitICS_S_VariableOrProcedureCall(context); }
        public override object VisitSC_Case(VisualBasic6Parser.SC_CaseContext context) { return base.VisitSC_Case(context); }
        public override object VisitCaseCondValue(VisualBasic6Parser.CaseCondValueContext context) { return base.VisitCaseCondValue(context); }
        public override object VisitVsLiteral(VisualBasic6Parser.VsLiteralContext context) { return base.VisitVsLiteral(context); }
        public override object VisitLetStmt(VisualBasic6Parser.LetStmtContext context) { return base.VisitLetStmt(context); }
        public override object VisitICS_S_ProcedureOrArrayCall(VisualBasic6Parser.ICS_S_ProcedureOrArrayCallContext context) { return base.VisitICS_S_ProcedureOrArrayCall(context); }
        public override object VisitArgsCall(VisualBasic6Parser.ArgsCallContext context) { return base.VisitArgsCall(context); }
        public override object VisitArgCall(VisualBasic6Parser.ArgCallContext context) { return base.VisitArgCall(context); }
        public override object VisitArg(VisualBasic6Parser.ArgContext context) { return base.VisitArg(context); }

        public override object VisitSeekStmt(VisualBasic6Parser.SeekStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitSeekStmt(context);
        }

        public override object VisitDeleteSettingStmt(VisualBasic6Parser.DeleteSettingStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitDeleteSettingStmt(context);
        }

        public override object VisitConstStmt(VisualBasic6Parser.ConstStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitConstStmt(context);
        }

        public override object VisitECS_MemberProcedureCall(VisualBasic6Parser.ECS_MemberProcedureCallContext context)
        {
            NotHandledNode(context);
            return base.VisitECS_MemberProcedureCall(context);
        }

        public override object VisitSetattrStmt(VisualBasic6Parser.SetattrStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitSetattrStmt(context);
        }

        public override object VisitArgDefaultValue(VisualBasic6Parser.ArgDefaultValueContext context)
        {
            NotHandledNode(context);
            return base.VisitArgDefaultValue(context);
        }

        public override object VisitPropertyLetStmt(VisualBasic6Parser.PropertyLetStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitPropertyLetStmt(context);
        }

        public override object VisitModuleAttributes(VisualBasic6Parser.ModuleAttributesContext context)
        {
            NotHandledNode(context);
            return base.VisitModuleAttributes(context);
        }

        public override object VisitTypeStmt_Element(VisualBasic6Parser.TypeStmt_ElementContext context)
        {
            NotHandledNode(context);
            return base.VisitTypeStmt_Element(context);
        }

        public override object VisitType(VisualBasic6Parser.TypeContext context)
        {
            NotHandledNode(context);
            return base.VisitType(context);
        }

        public override object VisitRsetStmt(VisualBasic6Parser.RsetStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitRsetStmt(context);
        }

        public override object VisitInputStmt(VisualBasic6Parser.InputStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitInputStmt(context);
        }

        public override object VisitVsAdd(VisualBasic6Parser.VsAddContext context)
        {
            NotHandledNode(context);
            return base.VisitVsAdd(context);
        }

        public override object VisitLsetStmt(VisualBasic6Parser.LsetStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitLsetStmt(context);
        }

        public override object VisitDeclareStmt(VisualBasic6Parser.DeclareStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitDeclareStmt(context);
        }

        public override object VisitVsLt(VisualBasic6Parser.VsLtContext context)
        {
            NotHandledNode(context);
            return base.VisitVsLt(context);
        }

        public override object VisitImplicitCallStmt_InBlock(VisualBasic6Parser.ImplicitCallStmt_InBlockContext context)
        {
            NotHandledNode(context);
            return base.VisitImplicitCallStmt_InBlock(context);
        }

        public override object VisitResetStmt(VisualBasic6Parser.ResetStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitResetStmt(context);
        }

        public override object VisitVsNew(VisualBasic6Parser.VsNewContext context)
        {
            NotHandledNode(context);
            return base.VisitVsNew(context);
        }

        public override object VisitTimeStmt(VisualBasic6Parser.TimeStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitTimeStmt(context);
        }



        public override object VisitSetStmt(VisualBasic6Parser.SetStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitSetStmt(context);
        }

        public override object VisitVsNegation(VisualBasic6Parser.VsNegationContext context)
        {
            NotHandledNode(context);
            return base.VisitVsNegation(context);
        }

        public override object VisitOnErrorStmt(VisualBasic6Parser.OnErrorStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitOnErrorStmt(context);
        }

        public override object VisitFieldLength(VisualBasic6Parser.FieldLengthContext context)
        {
            NotHandledNode(context);
            return base.VisitFieldLength(context);
        }

        public override object VisitVsLike(VisualBasic6Parser.VsLikeContext context)
        {
            NotHandledNode(context);
            return base.VisitVsLike(context);
        }

        public override object VisitVsDiv(VisualBasic6Parser.VsDivContext context)
        {
            NotHandledNode(context);
            return base.VisitVsDiv(context);
        }

        public override object VisitVsPlus(VisualBasic6Parser.VsPlusContext context)
        {
            NotHandledNode(context);
            return base.VisitVsPlus(context);
        }

        public override object VisitECS_ProcedureCall(VisualBasic6Parser.ECS_ProcedureCallContext context)
        {
            NotHandledNode(context);
            return base.VisitECS_ProcedureCall(context);
        }

        public override object VisitDictionaryCallStmt(VisualBasic6Parser.DictionaryCallStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitDictionaryCallStmt(context);
        }

        public override object VisitGoSubStmt(VisualBasic6Parser.GoSubStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitGoSubStmt(context);
        }

        public override object VisitRedimSubStmt(VisualBasic6Parser.RedimSubStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitRedimSubStmt(context);
        }

        public override object VisitAttributeStmt(VisualBasic6Parser.AttributeStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitAttributeStmt(context);
        }

        public override object VisitEnumerationStmt_Constant(VisualBasic6Parser.EnumerationStmt_ConstantContext context)
        {
            NotHandledNode(context);
            return base.VisitEnumerationStmt_Constant(context);
        }



        public override object VisitComplexType(VisualBasic6Parser.ComplexTypeContext context)
        {
            NotHandledNode(context);
            return base.VisitComplexType(context);
        }

        public override object VisitModuleHeader(VisualBasic6Parser.ModuleHeaderContext context)
        {
            NotHandledNode(context);
            return base.VisitModuleHeader(context);
        }



        public override object VisitVsNeq(VisualBasic6Parser.VsNeqContext context)
        {
            NotHandledNode(context);
            return base.VisitVsNeq(context);
        }



        public override object VisitExplicitCallStmt(VisualBasic6Parser.ExplicitCallStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitExplicitCallStmt(context);
        }

        public override object VisitOnGoSubStmt(VisualBasic6Parser.OnGoSubStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitOnGoSubStmt(context);
        }

        public override object VisitICS_B_MemberProcedureCall(VisualBasic6Parser.ICS_B_MemberProcedureCallContext context)
        {
            NotHandledNode(context);
            return base.VisitICS_B_MemberProcedureCall(context);
        }

        public override object VisitFilecopyStmt(VisualBasic6Parser.FilecopyStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitFilecopyStmt(context);
        }

        public override object VisitOutputList(VisualBasic6Parser.OutputListContext context)
        {
            NotHandledNode(context);
            return base.VisitOutputList(context);
        }





        public override object VisitWidthStmt(VisualBasic6Parser.WidthStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitWidthStmt(context);
        }

        public override object VisitWithStmt(VisualBasic6Parser.WithStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitWithStmt(context);
        }



        public override object VisitNameStmt(VisualBasic6Parser.NameStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitNameStmt(context);
        }

        public override object VisitTypeHint(VisualBasic6Parser.TypeHintContext context)
        {
            NotHandledNode(context);
            return base.VisitTypeHint(context);
        }

        public override object VisitConstSubStmt(VisualBasic6Parser.ConstSubStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitConstSubStmt(context);
        }

        public override object VisitDateStmt(VisualBasic6Parser.DateStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitDateStmt(context);
        }

        public override object VisitOptionCompareStmt(VisualBasic6Parser.OptionCompareStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitOptionCompareStmt(context);
        }

        public override object VisitRedimStmt(VisualBasic6Parser.RedimStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitRedimStmt(context);
        }

        public override object VisitSaveSettingStmt(VisualBasic6Parser.SaveSettingStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitSaveSettingStmt(context);
        }



        public override object VisitAsTypeClause(VisualBasic6Parser.AsTypeClauseContext context)
        {
            NotHandledNode(context);
            return base.VisitAsTypeClause(context);
        }

        public override object VisitErrorStmt(VisualBasic6Parser.ErrorStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitErrorStmt(context);
        }

        public override object VisitVsAddressOf(VisualBasic6Parser.VsAddressOfContext context)
        {
            NotHandledNode(context);
            return base.VisitVsAddressOf(context);
        }



        public override object VisitIfElseBlockStmt(VisualBasic6Parser.IfElseBlockStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitIfElseBlockStmt(context);
        }

        public override object VisitVsMult(VisualBasic6Parser.VsMultContext context)
        {
            NotHandledNode(context);
            return base.VisitVsMult(context);
        }

        public override object VisitEventStmt(VisualBasic6Parser.EventStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitEventStmt(context);
        }

        public override object VisitMkdirStmt(VisualBasic6Parser.MkdirStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitMkdirStmt(context);
        }

        public override object VisitLockStmt(VisualBasic6Parser.LockStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitLockStmt(context);
        }

        public override object VisitResumeStmt(VisualBasic6Parser.ResumeStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitResumeStmt(context);
        }

        public override object VisitSendkeysStmt(VisualBasic6Parser.SendkeysStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitSendkeysStmt(context);
        }

        public override object VisitOptionExplicitStmt(VisualBasic6Parser.OptionExplicitStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitOptionExplicitStmt(context);
        }

        public override object VisitVsNot(VisualBasic6Parser.VsNotContext context)
        {
            NotHandledNode(context);
            return base.VisitVsNot(context);
        }

        public override object VisitChdriveStmt(VisualBasic6Parser.ChdriveStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitChdriveStmt(context);
        }

        public override object VisitDeftypeStmt(VisualBasic6Parser.DeftypeStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitDeftypeStmt(context);
        }

        public override object VisitRandomizeStmt(VisualBasic6Parser.RandomizeStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitRandomizeStmt(context);
        }

        public override object VisitWriteStmt(VisualBasic6Parser.WriteStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitWriteStmt(context);
        }

        public override object VisitVsAnd(VisualBasic6Parser.VsAndContext context)
        {
            NotHandledNode(context);
            return base.VisitVsAnd(context);
        }

        public override object VisitEndStmt(VisualBasic6Parser.EndStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitEndStmt(context);
        }

        public override object VisitBlockIfThenElse(VisualBasic6Parser.BlockIfThenElseContext context)
        {
            NotHandledNode(context);
            return base.VisitBlockIfThenElse(context);
        }

        public override object VisitSavepictureStmt(VisualBasic6Parser.SavepictureStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitSavepictureStmt(context);
        }

        public override object VisitVsAmp(VisualBasic6Parser.VsAmpContext context)
        {
            NotHandledNode(context);
            return base.VisitVsAmp(context);
        }

        public override object VisitAmbiguousKeyword(VisualBasic6Parser.AmbiguousKeywordContext context)
        {
            NotHandledNode(context);
            return base.VisitAmbiguousKeyword(context);
        }

        public override object VisitForNextStmt(VisualBasic6Parser.ForNextStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitForNextStmt(context);
        }

        public override object VisitCaseCondTo(VisualBasic6Parser.CaseCondToContext context)
        {
            NotHandledNode(context);
            return base.VisitCaseCondTo(context);
        }

        public override object VisitVsMinus(VisualBasic6Parser.VsMinusContext context)
        {
            NotHandledNode(context);
            return base.VisitVsMinus(context);
        }

        public override object VisitCertainIdentifier(VisualBasic6Parser.CertainIdentifierContext context)
        {
            NotHandledNode(context);
            return base.VisitCertainIdentifier(context);
        }

        public override object VisitVsImp(VisualBasic6Parser.VsImpContext context)
        {
            NotHandledNode(context);
            return base.VisitVsImp(context);
        }

        public override object VisitICS_S_MembersCall(VisualBasic6Parser.ICS_S_MembersCallContext context)
        {
            NotHandledNode(context);
            return base.VisitICS_S_MembersCall(context);
        }

        public override object VisitForEachStmt(VisualBasic6Parser.ForEachStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitForEachStmt(context);
        }

        public override object VisitMacroElseBlockStmt(VisualBasic6Parser.MacroElseBlockStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitMacroElseBlockStmt(context);
        }

        public override object VisitVsEq(VisualBasic6Parser.VsEqContext context)
        {
            NotHandledNode(context);
            return base.VisitVsEq(context);
        }

        public override object VisitExitStmt(VisualBasic6Parser.ExitStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitExitStmt(context);
        }



        public override object VisitVsStruct(VisualBasic6Parser.VsStructContext context)
        {
            NotHandledNode(context);
            return base.VisitVsStruct(context);
        }

        public override object VisitSubscripts(VisualBasic6Parser.SubscriptsContext context)
        {
            NotHandledNode(context);
            return base.VisitSubscripts(context);
        }

        public override object VisitLetterrange(VisualBasic6Parser.LetterrangeContext context)
        {
            NotHandledNode(context);
            return base.VisitLetterrange(context);
        }


        public override object VisitPropertySetStmt(VisualBasic6Parser.PropertySetStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitPropertySetStmt(context);
        }



        public override object VisitOptionBaseStmt(VisualBasic6Parser.OptionBaseStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitOptionBaseStmt(context);
        }

        public override object VisitChdirStmt(VisualBasic6Parser.ChdirStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitChdirStmt(context);
        }

        public override object VisitLineInputStmt(VisualBasic6Parser.LineInputStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitLineInputStmt(context);
        }

        public override object VisitTypeStmt(VisualBasic6Parser.TypeStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitTypeStmt(context);
        }

        public override object VisitInlineIfThenElse(VisualBasic6Parser.InlineIfThenElseContext context)
        {
            NotHandledNode(context);
            return base.VisitInlineIfThenElse(context);
        }

        public override object VisitICS_S_MemberCall(VisualBasic6Parser.ICS_S_MemberCallContext context)
        {
            NotHandledNode(context);
            return base.VisitICS_S_MemberCall(context);
        }

        public override object VisitOutputList_Expression(VisualBasic6Parser.OutputList_ExpressionContext context)
        {
            NotHandledNode(context);
            return base.VisitOutputList_Expression(context);
        }

        public override object VisitTypeOfStmt(VisualBasic6Parser.TypeOfStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitTypeOfStmt(context);
        }



        public override object VisitOptionPrivateModuleStmt(VisualBasic6Parser.OptionPrivateModuleStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitOptionPrivateModuleStmt(context);
        }

        public override object VisitPutStmt(VisualBasic6Parser.PutStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitPutStmt(context);
        }

        public override object VisitICS_S_DictionaryCall(VisualBasic6Parser.ICS_S_DictionaryCallContext context)
        {
            NotHandledNode(context);
            return base.VisitICS_S_DictionaryCall(context);
        }

        public override object VisitUnloadStmt(VisualBasic6Parser.UnloadStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitUnloadStmt(context);
        }

        public override object VisitVsAssign(VisualBasic6Parser.VsAssignContext context)
        {
            NotHandledNode(context);
            return base.VisitVsAssign(context);
        }



        public override object VisitMacroIfBlockStmt(VisualBasic6Parser.MacroIfBlockStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitMacroIfBlockStmt(context);
        }

        public override object VisitSubscript(VisualBasic6Parser.SubscriptContext context)
        {
            NotHandledNode(context);
            return base.VisitSubscript(context);
        }

        public override object VisitVisibility(VisualBasic6Parser.VisibilityContext context)
        {
            NotHandledNode(context);
            return base.VisitVisibility(context);
        }

        public override object VisitBeepStmt(VisualBasic6Parser.BeepStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitBeepStmt(context);
        }

        public override object VisitVsTypeOf(VisualBasic6Parser.VsTypeOfContext context)
        {
            NotHandledNode(context);
            return base.VisitVsTypeOf(context);
        }

        public override object VisitComparisonOperator(VisualBasic6Parser.ComparisonOperatorContext context)
        {
            NotHandledNode(context);
            return base.VisitComparisonOperator(context);
        }



        public override object VisitWhileWendStmt(VisualBasic6Parser.WhileWendStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitWhileWendStmt(context);
        }


        public override object VisitMacroElseIfBlockStmt(VisualBasic6Parser.MacroElseIfBlockStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitMacroElseIfBlockStmt(context);
        }

        public override object VisitReturnStmt(VisualBasic6Parser.ReturnStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitReturnStmt(context);
        }

        public override object VisitIfConditionStmt(VisualBasic6Parser.IfConditionStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitIfConditionStmt(context);
        }

        public override object VisitModuleBlock(VisualBasic6Parser.ModuleBlockContext context)
        {
            NotHandledNode(context);
            return base.VisitModuleBlock(context);
        }

        public override object VisitVsLeq(VisualBasic6Parser.VsLeqContext context)
        {
            NotHandledNode(context);
            return base.VisitVsLeq(context);
        }

        public override object VisitVsMod(VisualBasic6Parser.VsModContext context)
        {
            NotHandledNode(context);
            return base.VisitVsMod(context);
        }

        public override object VisitKillStmt(VisualBasic6Parser.KillStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitKillStmt(context);
        }

        public override object VisitVsOr(VisualBasic6Parser.VsOrContext context)
        {
            NotHandledNode(context);
            return base.VisitVsOr(context);
        }

        public override object VisitModuleOptions(VisualBasic6Parser.ModuleOptionsContext context)
        {
            NotHandledNode(context);
            return base.VisitModuleOptions(context);
        }

        public override object VisitRmdirStmt(VisualBasic6Parser.RmdirStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitRmdirStmt(context);
        }

        public override object VisitVariableSubStmt(VisualBasic6Parser.VariableSubStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitVariableSubStmt(context);
        }

        public override object VisitCaseCondElse(VisualBasic6Parser.CaseCondElseContext context)
        {
            NotHandledNode(context);
            return base.VisitCaseCondElse(context);
        }

        public override object VisitAppactivateStmt(VisualBasic6Parser.AppactivateStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitAppactivateStmt(context);
        }

        public override object VisitGetStmt(VisualBasic6Parser.GetStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitGetStmt(context);
        }

        public override object VisitRaiseEventStmt(VisualBasic6Parser.RaiseEventStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitRaiseEventStmt(context);
        }

        public override object VisitVsGt(VisualBasic6Parser.VsGtContext context)
        {
            NotHandledNode(context);
            return base.VisitVsGt(context);
        }

        public override object VisitLineLabel(VisualBasic6Parser.LineLabelContext context)
        {
            NotHandledNode(context);
            return base.VisitLineLabel(context);
        }

        public override object VisitCaseCondIs(VisualBasic6Parser.CaseCondIsContext context)
        {
            NotHandledNode(context);
            return base.VisitCaseCondIs(context);
        }

        public override object VisitOnGoToStmt(VisualBasic6Parser.OnGoToStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitOnGoToStmt(context);
        }



        public override object VisitModuleConfigElement(VisualBasic6Parser.ModuleConfigElementContext context)
        {
            NotHandledNode(context);
            return base.VisitModuleConfigElement(context);
        }

        public override object VisitPropertyGetStmt(VisualBasic6Parser.PropertyGetStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitPropertyGetStmt(context);
        }

        public override object VisitLoadStmt(VisualBasic6Parser.LoadStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitLoadStmt(context);
        }

        public override object VisitBaseType(VisualBasic6Parser.BaseTypeContext context)
        {
            NotHandledNode(context);
            return base.VisitBaseType(context);
        }



        public override object VisitOpenStmt(VisualBasic6Parser.OpenStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitOpenStmt(context);
        }

        public override object VisitIfBlockStmt(VisualBasic6Parser.IfBlockStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitIfBlockStmt(context);
        }

        public override object VisitImplementsStmt(VisualBasic6Parser.ImplementsStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitImplementsStmt(context);
        }

        public override object VisitCloseStmt(VisualBasic6Parser.CloseStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitCloseStmt(context);
        }

        public override object VisitIfElseIfBlockStmt(VisualBasic6Parser.IfElseIfBlockStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitIfElseIfBlockStmt(context);
        }

        public override object VisitStopStmt(VisualBasic6Parser.StopStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitStopStmt(context);
        }

        public override object VisitVsGeq(VisualBasic6Parser.VsGeqContext context)
        {
            NotHandledNode(context);
            return base.VisitVsGeq(context);
        }



        public override object VisitVariableListStmt(VisualBasic6Parser.VariableListStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitVariableListStmt(context);
        }

        public override object VisitICS_B_ProcedureCall(VisualBasic6Parser.ICS_B_ProcedureCallContext context)
        {
            NotHandledNode(context);
            return base.VisitICS_B_ProcedureCall(context);
        }

        public override object VisitUnlockStmt(VisualBasic6Parser.UnlockStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitUnlockStmt(context);
        }

        public override object VisitVsXor(VisualBasic6Parser.VsXorContext context)
        {
            NotHandledNode(context);
            return base.VisitVsXor(context);
        }

        public override object VisitGoToStmt(VisualBasic6Parser.GoToStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitGoToStmt(context);
        }

        public override object VisitMidStmt(VisualBasic6Parser.MidStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitMidStmt(context);
        }

        public override object VisitMacroIfThenElseStmt(VisualBasic6Parser.MacroIfThenElseStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitMacroIfThenElseStmt(context);
        }

        public override object VisitVsPow(VisualBasic6Parser.VsPowContext context)
        {
            NotHandledNode(context);
            return base.VisitVsPow(context);
        }

        public override object VisitVsIs(VisualBasic6Parser.VsIsContext context)
        {
            NotHandledNode(context);
            return base.VisitVsIs(context);
        }

        public override object VisitPrintStmt(VisualBasic6Parser.PrintStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitPrintStmt(context);
        }

        public override object VisitModuleConfig(VisualBasic6Parser.ModuleConfigContext context)
        {
            NotHandledNode(context);
            return base.VisitModuleConfig(context);
        }

        public override object VisitDoLoopStmt(VisualBasic6Parser.DoLoopStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitDoLoopStmt(context);
        }





        public override object VisitEraseStmt(VisualBasic6Parser.EraseStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitEraseStmt(context);
        }

        public override object VisitEnumerationStmt(VisualBasic6Parser.EnumerationStmtContext context)
        {
            NotHandledNode(context);
            return base.VisitEnumerationStmt(context);
        }



        public override object VisitVsEqv(VisualBasic6Parser.VsEqvContext context)
        {
            NotHandledNode(context);
            return base.VisitVsEqv(context);
        }



        public override object VisitVsMid(VisualBasic6Parser.VsMidContext context)
        {
            NotHandledNode(context);
            return base.VisitVsMid(context);
        }
    }
}

