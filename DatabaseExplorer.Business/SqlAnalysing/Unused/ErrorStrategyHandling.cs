﻿using System;

using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Dfa;
using Antlr4.Runtime.Sharpen;

namespace DatabaseExplorer.Business.SqlAnalysing
{
    // http://stackoverflow.com/questions/18550031/how-to-control-error-handling-and-synchronization-in-antlr-4-c-sharp
    // A custom error strategy is an advanced feature with limited documentation. 
    // It's expected that the implementer is already an expert in ANTLR 4's implementation 
    // of the Adaptive LL(*) parsing algorithm (we're talking researcher-level understanding).
    public class ErrorStrategy : DefaultErrorStrategy
    {
        public override void Sync(Antlr4.Runtime.Parser recognizer)
        {
            base.Sync(recognizer);
            //if (recognizer.Context is ...)
            //    base.Sync(recognizer);
        }
    }
}