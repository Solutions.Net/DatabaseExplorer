﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Antlr4.Runtime;
using Antlr4.Runtime.Atn;
using Antlr4.Runtime.Dfa;
using Antlr4.Runtime.Sharpen;

namespace DatabaseExplorer.Business.SqlAnalysing
{
    // http://stackoverflow.com/questions/18501077/add-events-to-antlr-4-c-sharp-generated-code
    public class ErrorListener : BaseErrorListener, IAntlrErrorListener<int>
    {
        public readonly List<string> Errors = new List<string>();

        public override void ReportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, bool exact, BitSet ambigAlts, ATNConfigSet configs)
        {

        }
        public override void ReportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, SimulatorState conflictState)
        {

        }
        public override void ReportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, SimulatorState acceptState)
        {
            //   Errors.Add(line.ToString() + charPositionInLine.ToString() + ": " + msg);
        }
        public override void SyntaxError(IRecognizer recognizer, IToken offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e)
        {
            Errors.Add(line.ToString() + ":" + charPositionInLine.ToString() + ": " + msg);
        }

        public void SyntaxError(IRecognizer recognizer, int offendingSymbol, int line, int charPositionInLine, string msg, RecognitionException e)
        {
            Errors.Add(line.ToString() + ":" + charPositionInLine.ToString() + ": " + msg);
        }
    }

}