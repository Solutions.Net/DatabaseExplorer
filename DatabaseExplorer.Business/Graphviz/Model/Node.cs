﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;


namespace DatabaseExplorer.Business.Graphviz
{
    public class Node : DotObject
    {
        public string    Name      { get; set; }
        public LabelComposite     Label     { get; set; }
        public NodeShape Shape     { get; set; }
        public bool      Filled    { get; set; }
        public int?      PenWidth  { get; set; }
        public Color?    FillColor { get; set; }
        public string    FontName  { get; set; }

        public IEnumerable<Edge> Edges { get { return _EdgesStarting.Concat(_EdgesArriving).Distinct().ToList(); } }

        public IEnumerable<Edge> EdgesStarting  { get { return _EdgesStarting.AsReadOnly(); } }
        readonly List<Edge>      _EdgesStarting = new List<Edge>();

        public IEnumerable<Edge> EdgesArriving { get { return _EdgesArriving.AsReadOnly(); } }
        readonly List<Edge>      _EdgesArriving = new List<Edge>();

        public Graph Owner
        {
            get { return _Owner; }
            internal set
            {
                if (_Owner == value)
                    return;
                if (_Owner != null)
                    _Owner.RemoveNode(this);
                _Owner = value;
                if (_Owner != null)
                    _Owner.AddNode(this);
            }
        }
        Graph _Owner;

        public Node()
        {
        }

        public Node(string label)
        {
            Label = new Label(label);
        }

        public Edge AddEdgeTo(Node node, LabelComposite part = null, string edgeLabel = null)
        {
            Edge e = new Edge(edgeLabel);
            e.Origin = this;
            e.OriginPart = part;
            e.Target = node;
            return e;
        }

        public void Remove(Edge edge)
        {
            edge.DetachFromGraph();
        }

        internal void DetachFromGraph(bool keep_edges = false)
        {
            foreach (Edge e in _EdgesStarting)
                e.DetachFromGraph(keep_edges ? this : null);
            foreach (Edge e in _EdgesArriving)
                e.DetachFromGraph(keep_edges ? this : null);
            if (!keep_edges)
            {
                _EdgesStarting.Clear();
                _EdgesArriving.Clear();
            }
            Owner = null;
        }

        protected internal void AddStarting(Edge byThisEdge)
        {
            _EdgesStarting.Add(byThisEdge);
        }
        protected internal void RemoveArriving(Edge byThisEdge)
        {
            _EdgesArriving.Remove(byThisEdge);
        }
        protected internal void AddArriving(Edge byThisEdge)
        {
            _EdgesArriving.Add(byThisEdge);
        }
        protected internal void RemoveStarting(Edge byThisEdge)
        {
            _EdgesStarting.Remove(byThisEdge);
        }

    }


    
}
