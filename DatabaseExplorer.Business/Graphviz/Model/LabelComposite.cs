﻿using System;
using System.Collections.Generic;
using System.Web;


namespace DatabaseExplorer.Business.Graphviz
{
    public abstract class LabelComposite
    {
        protected internal DotLabelList Parent { get; set; }

        public abstract string ToHtml();
        public abstract string ToDotFormat(LabelOrientation parentOrientation, Dictionary<LabelComposite, string> portNames);
        
        protected static string EscapeForHtml(string str)
        {
            return HttpUtility.HtmlEncode(str);
        }

        protected static string EscapeForDotFormat(string str)
        {
            return GraphExporter.EscapeString(str)
                                .Replace("|", @"\|")
                                .Replace("<", @"\<")
                                .Replace(">", @"\>")
                                .Replace("{", @"\{")
                                .Replace("}", @"\}");
        }
    }
}
