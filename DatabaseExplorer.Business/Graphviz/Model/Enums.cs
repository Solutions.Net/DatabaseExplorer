﻿using System;


namespace DatabaseExplorer.Business.Graphviz
{
    public enum GraphLayoutDirection
    {
        TopBottom = 0,
        LeftRight,
        BottomTop,
        RightLeft
    }

    // From http://www.graphviz.org/content/node-shapes
    public enum NodeShape
    {
        Box,                  Polygon,        Ellipse,            Oval,
        Circle,                 Point,            Egg,        Triangle,
        Plaintext,              Plain,        Diamond,       Trapezium,
        Parallelogram,          House,       Pentagon,         Hexagon,
        Septagon,             Octagon,   Doublecircle,   Doubleoctagon,  
        Tripleoctagon,    InvTriangle,   InvTrapezium,        InvHouse,  
        Mdiamond,             Msquare,        Mcircle,            Rect,
        Rectangle,             Square,           Star,            None,  
        Underline,               Note,            Tab,          Folder,  
        Box3d,              Component,       Promoter,             Cds,  
        Terminator,               Utr,     Primersite, Restrictionsite,  
        Fivepoverhang, Threepoverhang,      Noverhang,        Assembly,  
        Signature,          Insulator,       Ribosite,         Rnastab,  
        Proteasesite,     Proteinstab,      Rpromoter,          Rarrow,  
        Larrow,             Lpromoter,
        // Not explicitely in doc :(
        Mrecord
    }

    // From http://www.graphviz.org/content/arrow-shapes
    public enum ArrowSide
    {
        Both = 0,
        Left,
        Right
    }
    
    // From http://www.graphviz.org/content/arrow-shapes
    public enum ArrowShapeKind
    {
        Unspecified = 0, // pour être traité differement en fonction du type de graphe
        None,
        Normal,
        Vee,
        Diamond,
        Curve,
        Dot,
        Box,
        Tee,
        // Reversed
        /// <summary> reversed of Normal </summary>
        Inv,
        /// <summary> reversed of Vee </summary>
        Crow,
        /// <summary> reversed of Curve </summary>
        Icurve,
    }
}
