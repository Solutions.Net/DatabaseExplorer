﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace DatabaseExplorer.Business.Graphviz
{
    public class Edge : DotObject
    {
        public string           Label  { get; set; }

        // La premiere Shape est celle la plus proche du noeud
        public List<ArrowShape> ShapesAtOrigin { get; set; }
        public ArrowShape       ShapeAtOrigin  { get { return ShapesAtOrigin.First(); } 
                                                 set { ShapesAtOrigin.Clear(); ShapesAtOrigin.Add(value); } }

        // La premiere Shape est celle la plus proche du noeud
        public List<ArrowShape> ShapesAtTarget { get; set; }
        public ArrowShape       ShapeAtTarget  { get { return ShapesAtTarget.First(); } 
                                                 set { ShapesAtTarget.Clear(); ShapesAtTarget.Add(value); } }

        public Node             Origin          { get { return _Origin; } 
                                                  set
                                                  {
                                                      if (_Origin == value)
                                                          return;
                                                      if (_Origin != null)
                                                          _Origin.RemoveStarting(this);
                                                      _Origin = value;
                                                      OriginPart = null;
                                                      if (value != null)
                                                          _Origin.AddStarting(this);
                                                  }
                                                }
        Node _Origin;
        public LabelComposite            OriginPart { get; set; }
        
        public Node             Target          { get { return _Target; } 
                                                  set
                                                  {
                                                      if (_Target == value)
                                                          return;
                                                      if (_Target != null)
                                                          _Target.RemoveArriving(this);
                                                      _Target = value;
                                                      TargetPart = null;
                                                      if (value != null)
                                                          _Target.AddArriving(this);
                                                  }
                                                }
        Node _Target;
        public LabelComposite        TargetPart { get; set; }

        public Edge()
        { }

        public Edge(string label)
        {
        }

        internal void DetachFromGraph(Node nodeRefToPreserve = null)
        {
            if (Origin != nodeRefToPreserve && Origin != null)
                Origin = null;
            if (Target != nodeRefToPreserve && Target != null)
                Target = null;
        }

    }
}
