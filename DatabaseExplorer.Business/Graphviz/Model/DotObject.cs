﻿using System;
using System.Collections.Generic;


namespace DatabaseExplorer.Business.Graphviz
{
    public abstract class DotObject
    {
        public readonly Dictionary<string, string> Attributes = new Dictionary<string, string>();
    }
}
