﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace DatabaseExplorer.Business.Graphviz
{
    public class DotLabelList : LabelComposite
    {
        public IReadOnlyList<LabelComposite>  Blocks { get { return _Blocks.AsReadOnly(); } }
        readonly List<LabelComposite>        _Blocks;

        public LabelOrientation      Orientation { get; set; }

        public DotLabelList()
        {
            _Blocks = new List<LabelComposite>();
        }
        public DotLabelList(bool vertical = false)
            : this()
        {
        }
        public DotLabelList(params LabelComposite[] labels)
            : this((IEnumerable<LabelComposite>) labels)
        {
        }
        public DotLabelList(IEnumerable<LabelComposite> labels)
            : this()
        {
            foreach (LabelComposite lbl in labels)
                Add(lbl);
        }
        public DotLabelList(IEnumerable<string> labels)
            : this()
        {
            foreach (string lbl in labels)
                Add(new Label(lbl));
        }
        public DotLabelList(bool vertical = false, params LabelComposite[] labels)
            : this(vertical, (IEnumerable<LabelComposite>) labels)
        {
        }
        public DotLabelList(bool vertical = false, IEnumerable<LabelComposite> labels = null)
            : this(vertical)
        {
            if (labels != null)
                foreach (LabelComposite lbl in labels)
                    Add(lbl);
        }

        public int Count
        {
            get { return _Blocks.Count; }
        }
        public LabelComposite this[int index]
        {
            get { return _Blocks[index]; }
            set { Remove(_Blocks[index]); Insert(index, value); }
        }

        public Label Add(string lbl)
        {
            var lblStr = new Label(lbl);
            Add(lblStr);
            return lblStr;
        }
        public void Add(LabelComposite lbl)
        {
            Insert(_Blocks.Count, lbl);
        }
        public void Insert(int index, LabelComposite lbl)
        {
            if (lbl.Parent != null)
                lbl.Parent.Remove(lbl);
            lbl.Parent = this;
            _Blocks.Insert(index, lbl);
        }
        public void Remove(LabelComposite lbl)
        {
            lbl.Parent = null;
            _Blocks.Remove(lbl);
        }

        public override string ToHtml()
        {
            string nl = Environment.NewLine; // Pour debug seulement
                
            string res = "<table border=\"0\" cellborder=\"0\" cellpadding=\"3\" bgcolor=\"white\">";

            var cells = Blocks.Select(b => "<td align=\"left\" bgcolor=\"white\" port=\"r2\">" + nl +
                                           b.ToHtml() + nl +
                                           "</td>")
                              .ToList();
            if (Orientation == LabelOrientation.Vertical)
                foreach (string cell in cells)
                    res += nl + "<tr>" + cell + "</tr>";
            else
            {
                res += nl + "<tr>";
                foreach (string cell in cells)
                    res += nl + cell;
                res += nl + "</tr>";
            }
            res += nl + "</table>";

            res = res.Replace(nl, string.Empty);
            return res;
        }

        public override string ToDotFormat(LabelOrientation parentOrientation, Dictionary<LabelComposite, string> portNames)
        {
            Debug.Assert(parentOrientation != LabelOrientation.ReverseParent);
            var orientationToDo = Orientation == LabelOrientation.ReverseParent
                                   ? parentOrientation.Reverse()
                                   : Orientation;

            // Behavior described here : http://www.graphviz.org/content/node-shapes
            return (orientationToDo != parentOrientation ? "{" : string.Empty) +
                    string.Join(" | ", _Blocks.Select(b => b.ToDotFormat(orientationToDo, portNames))) +
                    (orientationToDo != parentOrientation ? "}" : string.Empty);
        }

    }

    public enum LabelOrientation
    {
        ReverseParent = 0,
        Vertical,
        Horizontal
    }

    public static class LabelOrientation_Extensions
    {
        public static LabelOrientation Reverse(this LabelOrientation o)
        {
            if (o == LabelOrientation.ReverseParent)
                return LabelOrientation.ReverseParent;
            if (o == LabelOrientation.Vertical)
                return LabelOrientation.Horizontal;
            return LabelOrientation.Vertical;
        }

    }
}
