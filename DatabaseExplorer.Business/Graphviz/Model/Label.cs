﻿using System;
using System.Collections.Generic;


namespace DatabaseExplorer.Business.Graphviz
{
    public class Label : LabelComposite
    {
        public string Text { get; set; }

        public Label(string text)
        {
            Text = text;
        }

        public override string ToHtml()
        {
            return EscapeForHtml(Text);
        }
        public override string ToDotFormat(LabelOrientation parentOrientation, Dictionary<LabelComposite, string> portNames)
        {
            string res = EscapeForDotFormat(Text);
            string portName;
            if (portNames.TryGetValue(this, out portName))
                res = "<" + portName + "> " + res;
            return res;
        }
    }
}
