﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace DatabaseExplorer.Business.Graphviz
{
    public class Graph : DotObject
    {
        public string Name       { get; set; }
        public bool   IsDirected { get; set; }

        public GraphLayoutDirection LayoutDirection { get; set; }


        public IReadOnlyList<Node> Nodes { get { return _Nodes.AsReadOnly(); } }
        readonly List<Node> _Nodes = new List<Node>();

        public List<Edge> Edges { get { return _Nodes.SelectMany(n => n.Edges).Distinct().ToList(); } }

        public Graph(string name = null)
        {
            Name = name;
        }
              
        public Graph Add(params Node[] nodes)
        {
            foreach (var n in nodes)
            {
                n.Owner = this;
                foreach (var e in n.EdgesStarting)
                    if (e.Target != null)
                        Add(e.Target);
            }

            return this;
        }
        protected internal void AddNode(Node node)
        {
            _Nodes.Add(node);
        }
       
        public void Remove(Node node)
        {
            node.DetachFromGraph();
        }
        protected internal void RemoveNode(Node node)
        {
            _Nodes.Remove(node);
        }

   
        public string ToDot()
        {
            return new GraphExporter().Export(this);
        }
    }


}
