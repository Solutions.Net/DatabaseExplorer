﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;


namespace DatabaseExplorer.Business.Graphviz
{
    public class GraphExporter
    {
        public bool PreserveNodeLabelOrientation { get; set; }
        public bool UseHtmlFormat { get; set; }

        public GraphExporter()
        {
            PreserveNodeLabelOrientation = true;
        }
        

        public string Export(Graph g)
        {
            CheckGraphIsValidToExport(g);
            BeforeExportGraph(g);
            return ToDot(g);
        }

        protected virtual void CheckGraphIsValidToExport(Graph g)
        {
            var nodeDoublons = g.Nodes
                                .GroupBy(n => string.IsNullOrWhiteSpace(n.Name) ? null : n.Name.Trim())
                                .Where(grp => grp.Key != null)
                                .Where(grp => grp.Count() >= 2)
                                .ToList();
            if (nodeDoublons.Count > 0)
                throw new Exception("Some nodes have the same name : " +
                                    nodeDoublons.Select(grp => nl + " - " + grp.Key));
        }

        protected virtual void BeforeExportGraph(Graph g)
        {
            _nodesNames = new Dictionary<Node, string>();
            int seed = 0;

            foreach (var n in g.Nodes)
            {
                if (n.Name != null)
                    _nodesNames.Add(n, n.Name.Trim());

                string uniqueNodeName = null;
                do
                    uniqueNodeName = "n" + ++seed;
                while (_nodesNames.ContainsValue(uniqueNodeName));
                _nodesNames.Add(n, uniqueNodeName);
            }

            _portNames = new Dictionary<LabelComposite, string>();
            var portNameSeeds = new Dictionary<Node, int>();
            Func<Node, LabelComposite, string> addPortName = (n, lbl) =>
                {
                    string res;
                    if (_portNames.TryGetValue(lbl, out res))
                        return res;
                    seed = 0;
                    portNameSeeds.TryGetValue(n, out seed);
                    res = "f" + ++seed;
                    portNameSeeds[n] = seed;
                    _portNames.Add(lbl, res);
                    return "";  
                };

            foreach (Edge e in g.Edges)
            {
                if (e.OriginPart != null)
                    addPortName(e.Origin, e.OriginPart);
                    
                if (e.TargetPart != null)
                    addPortName(e.Target, e.TargetPart);
            }

        }
        Dictionary<LabelComposite, string> _portNames;
        Dictionary<Node, string> _nodesNames;

        protected virtual string ToDot(Graph g)
        {
            string res = string.Empty;

            res += (g.IsDirected ? "di" : "") + "graph " + "g" + g.Name + nl;
            res += "{" + nl;

            var attributs = new Dictionary<string, string>();

            if (g.LayoutDirection != GraphLayoutDirection.TopBottom) // Default
                attributs.Add("RankDir", 
                      g.LayoutDirection == GraphLayoutDirection.LeftRight ? "LR" 
                    : g.LayoutDirection == GraphLayoutDirection.RightLeft ? "RL" 
                    : g.LayoutDirection == GraphLayoutDirection.RightLeft ? "BT" 
                    : "TB");

            if (attributs.Count > 0)
            {
                res += "graph [ ";
                res += string.Join(", ", attributs.Select(pair => pair.Key + "=\"" + EscapeString(pair.Value) + "\""));
                res += " ];" + nl + nl;
            }

            foreach (var n in g.Nodes)
            {
                res += ToDot(n) + nl;
                foreach (var e in n.EdgesArriving)
                    res += ToDot(e) + nl;
                res += nl;
            }

            res += "}";

            return res;
        }

        public virtual string ToDot(Node n)
        {
            string nodeDeclaration = _nodesNames[n] + " [ ";
            var attributs = new Dictionary<string, string>();
            if (n.Shape != NodeShape.Box)
                attributs.Add("shape", n.Shape.ToString());
            if (n.Filled)
                attributs.Add("style", "filled");
            if (n.PenWidth.HasValue)
                attributs.Add("penwidth", n.PenWidth.Value.ToString(CultureInfo.InvariantCulture));
            if (n.FillColor.HasValue)
                attributs.Add("fillcolor", n.FillColor.Value.Name);
            if (n.FontName != null)
                attributs.Add("fontname", n.FontName.Trim());

            nodeDeclaration += string.Join(", ", attributs.Select(pair => pair.Key + "=\"" + EscapeString(pair.Value) + "\""));

            if (n.Label != null)
            {
                if (attributs.Count > 0)
                    nodeDeclaration += ", ";

                nodeDeclaration += "label=";

                if (UseHtmlFormat)
                    nodeDeclaration += "<" + n.Label.ToHtml() + ">";
                else
                {
                    LabelOrientation currentOrientaiton = PreserveNodeLabelOrientation ||
                                                          n.Owner.LayoutDirection == GraphLayoutDirection.TopBottom ||
                                                          n.Owner.LayoutDirection == GraphLayoutDirection.BottomTop
                                                          ? LabelOrientation.Vertical
                                                          : LabelOrientation.Horizontal;
                    nodeDeclaration += "\"" + n.Label.ToDotFormat(currentOrientaiton, _portNames) + "\"";
                }
            }

            nodeDeclaration += " ];";

            return nodeDeclaration;
        }


        public virtual string ToDot(Edge e)
        {
            string res = _nodesNames[e.Origin];

            if (e.OriginPart != null)
                res += ":" + _portNames[e.OriginPart];
            res += " -> ";

            res += _nodesNames[e.Target];

            if (e.TargetPart != null)
                res += ":" + _portNames[e.TargetPart];

            if (e.Label != null)
            {
                res += " [label=\"" + EscapeString(e.Label) + "\"]";
            }
            return res + ";";
        }
        


        protected internal static string EscapeString(string str)
        {
            return str.Replace("\\", @"\\\")
                      .Replace("\"", "\\\"")
                      .Replace("\n", @"\n")
                      .Replace("\r", @"\r");
        }
        protected internal static string RemoveControlChars(string str)
        {
            // From http://stackoverflow.com/questions/4500870/how-to-remove-control-chars-from-utf8-string/4501246#4501246
            return Regex.Replace(str, @"\p{C}+", ""); 
        }

        protected static readonly string nl = Environment.NewLine;
    }


}
