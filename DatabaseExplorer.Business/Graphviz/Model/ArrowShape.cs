﻿using System;


namespace DatabaseExplorer.Business.Graphviz
{
    public struct ArrowShape
    {
        public readonly ArrowShapeKind Kind;
        public readonly ArrowSide      Side;
        public readonly bool           Open;

        public          bool          Filled { get { return !Open; } }

        public ArrowShape(ArrowShapeKind kind, ArrowSide side = ArrowSide.Both, bool open = false)
        {
            Kind = kind;
            Side = ArrowSide.Both;
            Open = false;
        }

        public static implicit operator ArrowShape(ArrowShapeKind kind)
        {
            return new ArrowShape(kind);
        }

        /// <summary> Handy function to handle case when shape is ArrowShapeKind.Unspecified by returning a substitute kind</summary>
        public ArrowShapeKind KindOr(ArrowShapeKind kind_if_unspecified)
        {
            return Kind == ArrowShapeKind.Unspecified ? kind_if_unspecified : Kind;
        }

        public bool IsMeaningful()
        {
            var kind = KindOr(ArrowShapeKind.None);

            return (!Open || kind == ArrowShapeKind.Box ||
                             kind == ArrowShapeKind.Diamond ||
                             kind == ArrowShapeKind.Inv ||
                             kind == ArrowShapeKind.Normal ||
                             kind == ArrowShapeKind.Dot)
                 && (Side == ArrowSide.Both || kind != ArrowShapeKind.None && 
                                               kind != ArrowShapeKind.Dot);
        }
    }

   
}
