﻿using System;
using System.Drawing;

using Antlr4.Runtime.Tree;


namespace DatabaseExplorer.Business.Graphviz
{
    public class DotGenerator : AbstractParseTreeVisitor<Node>
    {
        public Graph ConvertToGraphiz(IParseTree tree)
        {
            Graph graph = new Graph()
            {
                IsDirected = true,
                Name = "AST",
                LayoutDirection = GraphLayoutDirection.TopBottom,
            };
            Node root = Visit(tree);
            graph.Add(root);
            return graph;
        }

        
        public override Node Visit(IParseTree node)
        {
            // To not pollute output with a lot of node we dont display terminal node in their own node.
            // Instead, we display them in their parent node's label.
            if (node is ITerminalNode)
                return null; 

            Node dotNode = new Node()
            {
                Shape = NodeShape.Mrecord,
                Filled = true,
                PenWidth = 1,
                FillColor = Color.White
            };
            
                
            var label = new DotLabelList();
            dotNode.Label = label;
            label.Add(node.GetType().Name.Replace("Context", ""));

            string code = node.GetText();
            
            if (code.Length - code.Replace("\n", "").Length > 5)
            {
                int cutindex = code.IndexOfAny(new char[] { '\r', '\n' });
                string startcode = code.Substring(0, cutindex);

                cutindex = code.LastIndexOfAny(new char[] { '\r', '\n' }, code.Length-1);
                string endcode = code.Substring(cutindex + 1);
                code = startcode + Environment.NewLine + "[...]" + Environment.NewLine + endcode;
            }

            label.Add(code);
            DotLabelList subLabels = new DotLabelList();
            label.Add(subLabels);

            for (int i = 0; i < node.ChildCount; ++i)
            {
                var childNode = node.GetChild(i);
                var subDotNode = Visit(childNode);

                if (subDotNode == null)
                    subLabels.Add(childNode.GetText());
                else
                    dotNode.AddEdgeTo(subDotNode, subLabels.Add("   "));
            }

            return dotNode;
        }

        public override Node VisitChildren(IRuleNode node)
        {
            return base.VisitChildren(node);
        }

        public override Node VisitErrorNode(IErrorNode node)
        {
            return base.VisitErrorNode(node);
        }

        public override Node VisitTerminal(ITerminalNode node)
        {
            return base.VisitTerminal(node);
        }

        
      
    }
}