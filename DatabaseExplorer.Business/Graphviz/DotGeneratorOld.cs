﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Antlr4.Runtime.Tree;

namespace DatabaseExplorer.Business.Graphviz
{
    public class DotGeneratorOld : AbstractParseTreeVisitor<int>
    {
        readonly StringBuilder _nodes = new StringBuilder();
        readonly StringBuilder _edges = new StringBuilder();

        readonly Dictionary<IParseTree, string> _nodeNames = new Dictionary<IParseTree, string>();

        public string Dot
        {
            get
            {
                string nl = Environment.NewLine;
                return "" + 
                    "digraph g {" + nl +
                    "    graph [ rankdir = \"TB\" ];" + nl +
                    "    node [ fontsize = \"16\", shape = \"ellipse\" ];" + nl +
                    "    edge [ ];" + nl + 
                    nl  +
                    _nodes + nl + 
                    nl +
                    _edges + nl +
                    "}" + nl;
            }
        }


        public string ConvertToDot(IParseTree tree)
        {
            Visit(tree);
            return Dot;
        }
        public override int Visit(IParseTree node)
        {
            return base.Visit(node);
        }

        public override int VisitChildren(IRuleNode node)
        {
            AnyNode(node);
            //_nodes.AppendLine(node.GetType().Name.Replace("Context", "") + "  with \"" + node.GetText() + "\"");
            //for(int i = 0; i < node.ChildCount; ++i)
            //    _nodes.AppendLine("   - " + node.GetChild(i).GetText());

            return 0;
        }

        public override int VisitErrorNode(IErrorNode node)
        {
            AnyNode(node);
            return 0;
        }

        public override int VisitTerminal(ITerminalNode node)
        {
            AnyNode(node);
            //return base.VisitTerminal(node);
            return 0;
        }

        void AnyNode(IParseTree node)
        {
            string nodeName;

            if (!_nodeNames.TryGetValue(node, out nodeName))
            {
                nodeName = "n" + (_nodeNames.Count + 1);
                string display = node.GetType().Name.Replace("Context", "");
                _nodes.AppendLine(nodeName + " [ shape = \"record\", label = \"" + display + "\" ];");
                _nodeNames.Add(node, nodeName);
            }

            //var res = base.VisitChildren(node);

            for (int i = 0; i < node.ChildCount; ++i)
            {
                Visit(node.GetChild(i));

                string nodeTo = _nodeNames[node.GetChild(i)];
                _edges.AppendLine(nodeName + " -> " + nodeTo);
            }
             
        }
    }
}