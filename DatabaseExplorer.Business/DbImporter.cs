﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;

using TechnicalTools;
using TechnicalTools.Model;

using DatabaseExplorer.DAL;


namespace DatabaseExplorer.Business
{
    public partial class DbImporter
    {
        public DatabaseInfo Import(string connectionString, Action<string> onWarning)
        {
            var dbInfo = new DatabaseInfo();

            List<ColumnInfo> tableInfos;
            List<PKRelationInfo> pkInfos;
            List<FKRelationInfo> fkInfos;
            using (var con = new SqlConnection(connectionString))
            {
                con.Open();
                tableInfos = ColumnInfo.GetAllMetaDataInfos(con);
                pkInfos = PKRelationInfo.GetAllPrimaryKeyInfos(con);
                fkInfos = FKRelationInfo.GetAllForeignKeyInfos(con, onWarning);
            }

            // Nettoyage de certaines contraintes en double
            var duplicateFKs = new List<FKRelationInfo>();
            bool hasDuplicateForeignKeys = false;
            foreach (var grp in fkInfos.GroupBy(fk => new {fk.ForeignSchemaName, fk.ForeignTableName, fk.ForeignColumnName, fk.ReferencedSchemaName, fk.ReferencedTableName, fk.ReferencedColumnName}))
            {
                int cntBefore = duplicateFKs.Count;
                foreach (var fk in grp)
                    if (fk.ForDeleteCascade && grp.Count() >= 2)
                        duplicateFKs.Add(fk);
                if (grp.Count() - 1 != duplicateFKs.Count - cntBefore) // On devrait avoir suppimé toutes les FK de ce groupe sauf une
                {
                    var remaining = grp.Except(duplicateFKs).ToList();
                    if (!hasDuplicateForeignKeys)
                    {
                        hasDuplicateForeignKeys = true;
                        onWarning("Some Foreign key seems duplicated (ie: strictly identical): ");
                    }
                    onWarning("   - " + remaining.Skip(1).Select(fk => fk.ConstraintName).Join() + " is/are equal to " + remaining.First().ConstraintName);
                    duplicateFKs.AddRange(remaining.Skip(1));
                }
                Debug.Assert(grp.Count() - 1 == duplicateFKs.Count - cntBefore); // Maintenant ca devrait toujours être vrai
            }
            foreach (var duplicateFk in duplicateFKs)
                fkInfos.Remove(duplicateFk);


            dbInfo.Schemas.Clear();
            foreach (var schemaTables in tableInfos.GroupBy(info => info.SchemaName))
            {
                var schema = new Schema(null)
                {
                    Name = schemaTables.Key,
                };
                foreach (var tableColumns in schemaTables.GroupBy(info => info.TableName))
                {
                    var table = new Table()
                    {
                        Owner = schema,
                        Name = tableColumns.Key,
                        Description = tableColumns.First().TableDescription,
                        Columns = new List<Column>(),
                        CreationDate = tableColumns.First().TableCreationDate,
                        ChangeDate = tableColumns.First().TableChangeDate
                    };
                    foreach (var colInfo in tableColumns)
                    {
                        var doublon = table.Columns.FirstOrDefault(col => col.Name == colInfo.ColumnName);
                        Debug.Assert(doublon == null);

                        table.Columns.Add(new Column()
                            {
                                Owner = table,
                                Name = colInfo.ColumnName,
                                SqlType = SqlType.FromSqlType(colInfo.ColumnDataType),
                                SqlTypeAsString = colInfo.ColumnDataType,
                                OrdinalPosition = colInfo.ColumnOrdinalPosition,
                                IsNullable = colInfo.ColumnIsNullable,
                                IsIdentity = colInfo.ColumnIsIdentity,
                                IsComputed = colInfo.ColumnIsComputed,
                                IsFileStream = colInfo.ColumnIsFileStream,
                                IsSparse = colInfo.ColumnIsSparse,
                                IsColumnSet = colInfo.ColumnIsInSet,
                                DefaultValue = colInfo.ColumnDefaultValue,
                                IsAutoIncremented = false,
                                // Quand col.ColumnDataLength.Value == -1 cela Signifie que le type est nullable et limité a int.MaxValue character
                                HasLengthLimit = colInfo.ColumnDataLength > 0,
                                ArrayLength = colInfo.ColumnDataLength > 0 ? colInfo.ColumnDataLength.Value : 0
                            });
                    }
                    schema.Tables.Add(table);
                }
                dbInfo.Schemas.Add(schema);
            }

            foreach (var tablesBySchema in pkInfos.GroupBy(info => info.SchemaName))
                foreach (var tablePkColumns in tablesBySchema.GroupBy(info => info.TableName))
                {
                    var table = dbInfo.FindTable(tablePkColumns.Key, tablesBySchema.Key);
                    table.Pk = new ConstraintPK()
                    {
                        Columns = tablePkColumns.Select(colInfo => table.Columns.First(col => col.Name == colInfo.PkColumnName)).ToList()
                    };
                }

            foreach (var table in dbInfo.Tables)
            {
                table.FkConstraints = new List<ConstraintFK>();
                table.FkReferencing = new List<ConstraintFK>();
            }
            foreach (var tablesBySchema in fkInfos.GroupBy(info => info.ForeignSchemaName))
                foreach (var tableFkColumns in tablesBySchema.GroupBy(info => info.ForeignTableName))
                {
                    var table = dbInfo.FindTable(tableFkColumns.Key, tablesBySchema.Key);
                    foreach (var fkColsGrp in tableFkColumns.GroupBy(fkColInfo => fkColInfo.ConstraintName))
                    {
                        var referencedTable = dbInfo.FindTable(fkColsGrp.First().ReferencedTableName, fkColsGrp.First().ReferencedSchemaName);
                        var fk = new ConstraintFK()
                            {
                                Owner = table,
                                ForeignColumns = table.Columns.Where(col => fkColsGrp.Any(fkCol => col.Name == fkCol.ForeignColumnName)).ToArray(),
                                ReferencedColumns = referencedTable.Columns.Where(col => fkColsGrp.Any(fkCol => col.Name == fkCol.ReferencedColumnName)).ToArray()
                            };
                        table.FkConstraints.Add(fk);
                        referencedTable.FkReferencing.Add(fk);
                    }
                }

            return dbInfo;
        }

        class ColumnInfo
        {
            public string SchemaName { get; set; }
            public string TableName { get; set; }
            public string TableDescription { get; set; }
            public string ColumnName { get; set; }
            public string ColumnDataType { get; set; }
            public int? ColumnDataLength { get; set; }
            public bool ColumnIsNullable { get; set; }
            public bool ColumnIsIdentity { get; set; }
            public bool ColumnIsComputed { get; set; }
            public bool ColumnIsFileStream { get; set; }
            public bool ColumnIsSparse { get; set; }
            public bool ColumnIsInSet { get; set; }
            public string ColumnDefaultValue { get; set; }
            public int ColumnOrdinalPosition { get; set; }
            public DateTime TableCreationDate { get; set; }
            public DateTime TableChangeDate { get; set; }

            public static List<ColumnInfo> GetAllMetaDataInfos(SqlConnection con)
            {
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = SqlQueryGetTableInfo(con);
                    //cmd.Parameters.AddWithValue("@id", index);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var ordinals = new
                        {
                            SchemaName = reader.GetOrdinal("SchemaName"),
                            TableName = reader.GetOrdinal("TableName"),
                            TableDescription = reader.GetOrdinal("TableDescription"),
                            ColumnName = reader.GetOrdinal("ColumnName"),
                            ColumnDataType = reader.GetOrdinal("ColumnDataType"),
                            ColumnDataLength = reader.GetOrdinal("ColumnDataLength"),
                            ColumnIsNullable = reader.GetOrdinal("ColumnIsNullable"),
                            ColumnIsIdentity = reader.GetOrdinal("ColumnIsIdentity"),
                            ColumnIsComputed = reader.GetOrdinal("ColumnIsComputed"),
                            ColumnIsFileStream = reader.GetOrdinal("ColumnIsFileStream"),
                            ColumnIsSparse = reader.GetOrdinal("ColumnIsSparse"),
                            ColumnIsInSet = reader.GetOrdinal("ColumnIsInSet"),
                            ColumnDefaultValue = reader.GetOrdinal("ColumnDefaultValue"),
                            ColumnOrdinalPosition = reader.GetOrdinal("ColumnOrdinalPosition"),
                            TableCreationDate = reader.GetOrdinal("TableCreationDate"),
                            TableChangeDate = reader.GetOrdinal("TableChangeDate")
                        };

                        var infos = new List<ColumnInfo>();
                        while (reader.Read())
                            infos.Add(new ColumnInfo()
                            {
                                SchemaName = GetValueAs<string>(reader.GetValue(ordinals.SchemaName)),
                                TableName = GetValueAs<string>(reader.GetValue(ordinals.TableName)),
                                TableDescription = GetValueAs<string>(reader.GetValue(ordinals.TableDescription)),
                                ColumnName = GetValueAs<string>(reader.GetValue(ordinals.ColumnName)),
                                ColumnDataType = GetValueAs<string>(reader.GetValue(ordinals.ColumnDataType)),
                                ColumnDataLength = GetValueAs<int?>(reader.GetValue(ordinals.ColumnDataLength)),
                                ColumnIsNullable = GetValueAs<string>(reader.GetValue(ordinals.ColumnIsNullable)) != "NO",
                                ColumnIsIdentity = GetValueAs<bool>(reader.GetValue(ordinals.ColumnIsIdentity)),
                                ColumnIsComputed = GetValueAs<bool>(reader.GetValue(ordinals.ColumnIsComputed)),
                                ColumnIsFileStream = GetValueAs<bool>(reader.GetValue(ordinals.ColumnIsFileStream)),
                                ColumnIsSparse = GetValueAs<bool>(reader.GetValue(ordinals.ColumnIsSparse)),
                                ColumnIsInSet = GetValueAs<bool>(reader.GetValue(ordinals.ColumnIsInSet)),
                                ColumnDefaultValue = GetValueAs<string>(reader.GetValue(ordinals.ColumnDefaultValue)),
                                ColumnOrdinalPosition = GetValueAs<int>(reader.GetValue(ordinals.ColumnOrdinalPosition)),
                                TableCreationDate = GetValueAs<DateTime>(reader.GetValue(ordinals.TableCreationDate)),
                                TableChangeDate = GetValueAs<DateTime>(reader.GetValue(ordinals.TableChangeDate))
                            });
                        if (infos.Count == 0)
                            throw new UserUnderstandableException("Current login has no right to select information_schema.tables!", null);
                        return infos;
                    }
                }
            }



            static string SqlQueryGetTableInfo(SqlConnection con)
            {
                if (__SqlQueryGetTableInfo != null)
                    return __SqlQueryGetTableInfo;

                // Add the PK in definition so we can easily do "sql inner joins" later
                string viewDefinition = "SELECT " + Environment.NewLine 
                                      + " c.object_id," + Environment.NewLine
                                      + " c.column_id," + Environment.NewLine;
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "sp_helptext 'INFORMATION_SCHEMA.COLUMNS'";
                    int firstLineCountToSkip = 3;
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read())
                            if (--firstLineCountToSkip < 0) // Skip parts "create view ...", "AS" and "select"
                                viewDefinition += GetValueAs<string>(reader.GetValue(0));
                }

                // from http://stackoverflow.com/questions/887370/sql-server-extract-table-meta-data-description-fields-and-their-data-types
                // Except for information_schema_columns
                __SqlQueryGetTableInfo = "; with information_schema_columns as ( " + viewDefinition + ")" +
@"select 
    SchemaName = tbl.table_schema,
    TableName = tbl.table_name, 
    TableDescription = prop.value,
    ColumnName = col.column_name, 
    ColumnDataType = col.data_type,
    ColumnDataLength = col.character_maximum_length,
    ColumnIsNullable = col.IS_NULLABLE,
    ColumnIsIdentity = colExtra.is_identity,
    ColumnIsComputed = colExtra.is_computed,
    ColumnIsFileStream = colExtra.is_filestream,
    ColumnIsSparse = colExtra.is_sparse,
    ColumnIsInSet = colExtra.is_column_set,
    ColumnDefaultValue = col.COLUMN_DEFAULT,
    ColumnOrdinalPosition = col.ORDINAL_POSITION,
    TableCreationDate = stbl.create_date,
    TableChangeDate = stbl.modify_date
    
FROM       information_schema.tables tbl
INNER JOIN information_schema_columns col -- is supposed to be strictly equivalent to 'information_schema . columns'
                                            ON col.table_name = tbl.table_name AND col.TABLE_SCHEMA = tbl.TABLE_SCHEMA
-- Doc is here https://docs.microsoft.com/fr-fr/sql/relational-databases/system-catalog-views/sys-columns-transact-sql?view=sql-server-2017
INNER JOIN sys.columns colExtra   ON colExtra.object_id = col.object_id and colExtra.column_id = col.column_id
LEFT JOIN  sys.extended_properties prop     ON prop.major_id = object_id(tbl.table_schema + '.' + tbl.table_name) 
                                           AND prop.minor_id = 0
                                           AND prop.name = 'MS_Description' 
LEFT OUTER JOIN  sys.tables stbl ON OBJECT_SCHEMA_NAME(stbl.object_id) = tbl.table_schema and OBJECT_NAME(stbl.object_id) = tbl.table_name 
                                and stbl.type = 'U' -- ignore views
WHERE tbl.table_type = 'base table' -- ignore views";

                return __SqlQueryGetTableInfo;
            }
            static string __SqlQueryGetTableInfo;
        }


        class PKRelationInfo
        {
            public string SchemaName { get; set; }
            public string TableName { get; set; }
            public string PkColumnName { get; set; }


            public static List<PKRelationInfo> GetAllPrimaryKeyInfos(SqlConnection con)
            {
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = SqlRequestGetAllPrimaryKeys;
                    //cmd.Parameters.AddWithValue("@id", index);
                    using (var reader = cmd.ExecuteReader())
                    {
                        var ordinals = new
                        {
                            SchemaName = reader.GetOrdinal("SCHEMA_NAME"),
                            TableName = reader.GetOrdinal("TABLE_NAME"),
                            PkColumnName = reader.GetOrdinal("PK_COLUMN_NAME"),
                        };

                        var infos = new List<PKRelationInfo>();
                        while (reader.Read())
                            infos.Add(new PKRelationInfo()
                            {
                                SchemaName = GetValueAs<string>(reader.GetValue(ordinals.SchemaName)),
                                TableName = GetValueAs<string>(reader.GetValue(ordinals.TableName)),
                                PkColumnName = GetValueAs<string>(reader.GetValue(ordinals.PkColumnName)),
                            });
                        return infos;
                    }
                }
            }


            static string SqlRequestGetAllPrimaryKeys
            {
                get
                {
                    return // from http://stackoverflow.com/questions/887370/sql-server-extract-table-meta-data-description-fields-and-their-data-types
 @"SELECT 
	KU.TABLE_SCHEMA as SCHEMA_NAME,
	KU.table_name as TABLE_NAME,
	column_name as PK_COLUMN_NAME
FROM        INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
INNER JOIN  INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' 
                                                     AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME";
                }
            }


        }
        class FKRelationInfo
        {
            public string ConstraintName { get; set; }
            public string ForeignSchemaName { get; set; }
            public string ForeignTableName { get; set; }
            public string ForeignColumnName { get; set; }
            public string ReferencedSchemaName { get; set; }
            public string ReferencedTableName { get; set; }
            public string ReferencedColumnName { get; set; }
            public bool   ForDeleteCascade { get; set; }

            public static List<FKRelationInfo> GetAllForeignKeyInfos(SqlConnection con, Action<string> onWarning = null)
            {
                onWarning = onWarning ?? (warning => { });
                var result = new List<List<FKRelationInfo>>();
                foreach(var cmdText in new[] {SqlGetForeignKeysCleanlyDefined, SqlGetForeignKeysAll})
                    using (var cmd = con.CreateCommand())
                    {
                        cmd.CommandText = cmdText;
                        //cmd.Parameters.AddWithValue("@id", index);
                        using (var reader = cmd.ExecuteReader())
                        {
                            var ordinals = new
                            {
                                ConstraintName = reader.GetOrdinal("FK_CONSTRAINT_NAME"),
                                ForeignSchemaName = reader.GetOrdinal("FK_SCHEMA_NAME"),
                                ForeignTableName = reader.GetOrdinal("FK_TABLE_NAME"),
                                ForeignColumnName = reader.GetOrdinal("FK_COLUMN_NAME"),
                                ReferencedSchemaName = reader.GetOrdinal("REFERENCED_SCHEMA_NAME"),
                                ReferencedTableName = reader.GetOrdinal("REFERENCED_TABLE_NAME"),
                                ReferencedColumnName = reader.GetOrdinal("REFERENCED_COLUMN_NAME"),
                                ForDeleteCascade = reader.GetOrdinal("FOR_DELETE_CASCADE")
                            };

                            var infos = new List<FKRelationInfo>();
                            while (reader.Read())
                                infos.Add(new FKRelationInfo()
                                {
                                    ConstraintName = GetValueAs<string>(reader.GetValue(ordinals.ConstraintName)),
                                    ForeignSchemaName = GetValueAs<string>(reader.GetValue(ordinals.ForeignSchemaName)),
                                    ForeignTableName = GetValueAs<string>(reader.GetValue(ordinals.ForeignTableName)),
                                    ForeignColumnName = GetValueAs<string>(reader.GetValue(ordinals.ForeignColumnName)),
                                    ReferencedSchemaName = GetValueAs<string>(reader.GetValue(ordinals.ReferencedSchemaName)),
                                    ReferencedTableName = GetValueAs<string>(reader.GetValue(ordinals.ReferencedTableName)),
                                    ReferencedColumnName = GetValueAs<string>(reader.GetValue(ordinals.ReferencedColumnName)),
                                    ForDeleteCascade = GetValueAs<bool>(reader.GetValue(ordinals.ForDeleteCascade))
                                });
                            result.Add(infos);
                        }
                    }
                var cleanlyDefinedFKNames = result[0].GroupBy(relation => relation.ConstraintName) // plusieurs colonnes peuvent etre dans la meme contraintes
                                                     .Select(grp => grp.First())
                                                     .ToDictionary(fk => fk.ConstraintName, fk => fk.ConstraintName);
                var notCleanlyDefinedForeignKey = result[1].Where(fk => !cleanlyDefinedFKNames.ContainsKey(fk.ConstraintName)).ToList();
                if (notCleanlyDefinedForeignKey.Any())
                {
                    onWarning("These constraint are not cleanly defined as Foreign Key but should be :");
                    foreach(var fk_as_ix in notCleanlyDefinedForeignKey)
                        onWarning("    - " + fk_as_ix.ConstraintName + " on table " + fk_as_ix.ForeignTableName);
                }

                return result[0];
            }


            static string SqlGetForeignKeysCleanlyDefined
            {
                get
                {
                    return // http://stackoverflow.com/questions/3907879/sql-server-howto-get-foreign-key-reference-from-information-schema
 @"    
SELECT  
     KCU1.CONSTRAINT_NAME AS FK_CONSTRAINT_NAME
    ,KCU1.TABLE_SCHEMA FK_SCHEMA_NAME
    ,KCU1.TABLE_NAME AS FK_TABLE_NAME 
    ,KCU1.COLUMN_NAME AS FK_COLUMN_NAME 
    --,KCU2.CONSTRAINT_NAME AS REFERENCED_CONSTRAINT_NAME 
    ,KCU2.TABLE_SCHEMA REFERENCED_SCHEMA_NAME
    ,KCU2.TABLE_NAME AS REFERENCED_TABLE_NAME 
    ,KCU2.COLUMN_NAME AS REFERENCED_COLUMN_NAME  
    , Cast(CASE when RC.DELETE_RULE = 'CASCADE' THEN 1 ELSE 0 END as Bit) as 'FOR_DELETE_CASCADE'-- en général 'NO ACTION'
FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS RC 

INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU1 
    ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG  
    AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA 
    AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME 

INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KCU2 
    ON KCU2.CONSTRAINT_CATALOG = RC.UNIQUE_CONSTRAINT_CATALOG  
    AND KCU2.CONSTRAINT_SCHEMA = RC.UNIQUE_CONSTRAINT_SCHEMA 
    AND KCU2.CONSTRAINT_NAME = RC.UNIQUE_CONSTRAINT_NAME 
    AND KCU2.ORDINAL_POSITION = KCU1.ORDINAL_POSITION";
                }
            }

            // Cette version à l'avantage de donner aussi les foreign key referencant les colonnes unique mais non declare comme primary key
            static string SqlGetForeignKeysAll
            {
                get
                {
                    return
 @"SELECT
    fk.name 'FK_CONSTRAINT_NAME',
    SCHEMA_NAME(tp.schema_id) 'FK_SCHEMA_NAME',
    tp.name 'FK_TABLE_NAME',
    cp.name 'FK_COLUMN_NAME', 
    SCHEMA_NAME(tr.schema_id) 'REFERENCED_SCHEMA_NAME',
    tr.name 'REFERENCED_TABLE_NAME',
    cr.name 'REFERENCED_COLUMN_NAME',  
    Cast(fk.delete_referential_action as Bit) 'FOR_DELETE_CASCADE' -- cf http://www.techonthenet.com/sql_server/foreign_keys/foreign_delete.php
FROM 
    sys.foreign_keys fk
INNER JOIN  sys.tables tp ON fk.parent_object_id = tp.object_id
INNER JOIN  sys.tables tr ON fk.referenced_object_id = tr.object_id
INNER JOIN  sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
INNER JOIN  sys.columns cp ON fkc.parent_column_id = cp.column_id AND fkc.parent_object_id = cp.object_id
INNER JOIN  sys.columns cr ON fkc.referenced_column_id = cr.column_id AND fkc.referenced_object_id = cr.object_id";
                }
            }
        }

        protected internal static T GetValueAs<T>(object value)
        {
            if (value is DBNull)
            {
                if (typeof(T).IsGenericType && typeof(T).GetGenericTypeDefinition() == typeof(Nullable<>))
                    return (T)(object)null;
                if (!typeof(T).IsValueType)
                    return (T)(object)null;
            }
            return (T)value;
        }


    }
}
