﻿using System;

using TechnicalTools.Model;

using DatabaseExplorer.Common;
//using DatabaseExplorer.DAL.DatabaseExplorer.Dbo;


namespace DatabaseExplorer.Business.Automation
{
    public abstract class DECommandLineTask<TConfig> : ApplicationBase.Business.Automation.CommandLineTask
        where TConfig : Business.Config
    {
        protected new TConfig Cfg { get { return (TConfig)base.Cfg; } }

        protected DECommandLineTask(TConfig cfg, DateTime atDate)
            : base(cfg, atDate)
        {
            if (Cfg?.IsLoaded ?? false)
            {
                if (!string.IsNullOrWhiteSpace(cfg.Automation.Bot_Login) ||
                    !string.IsNullOrWhiteSpace(cfg.Automation.Bot_Password))
                    throw new TechnicalException("Automation account is not valid!", null);
            }
        }
    }
}
