﻿using System;

using TechnicalTools.Diagnostics;
using TechnicalTools.Model;

using ApplicationBase.Common;


namespace LMT.Business.Automation
{
    class DefaultExceptionHandler : OldLogManager.IExceptionHandler
    {
        public OldLogManager.IExceptionHandler DefaultExceptionManagement { get; set; } // == null on mainform

        public DefaultExceptionHandler()
        {
            UnexpectedExceptionManager.NewUnhandledException += (_, e) => HandleException(UnknownAction, e.Exception);
        }
        public const string UnknownAction = "Unknown action or background task";


        public void HandleException(string actionName, Exception ex)
        {
            string text = "Error during " + (actionName == UnknownAction ? UnknownAction : $"action \"{actionName}\"");
            if (ex is SilentBusinessException)
            {
                OldLogManager.logError(text + " : " + ExceptionManager.GetPrettyMessageFromExceptionForUser(ex));
                return;
            }

            if (ex is UserUnderstandableException)
            {
                string prettyMsg = ExceptionManager.GetPrettyMessageFromExceptionForUser(ex);
                OldLogManager.logError(text + " : " + prettyMsg);
            }
            else
            {
                OldLogManager.logError(text + " : " + ExceptionManager.GetPrettyMessageFromExceptionForUser(ex));
            }
        }
    }

}
