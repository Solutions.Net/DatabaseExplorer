﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Xml.Linq;

using TechnicalTools;
using TechnicalTools.Model.Cache;

using DataMapper;


namespace DatabaseExplorer.Business.Automation
{
    public class ImportEuropeanCentralBankEuroRates : DECommandLineTask<Business.Config>
    {
        //[Argument(IsMandatory = true)]
        //protected string FromLocalPath { get; set; }

        //[Argument]
        //protected bool AnyProperty { get; set; }

        //[Argument]
        //protected int MaxItem { get; set; } = 62;

        public ImportEuropeanCentralBankEuroRates(Business.Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }

        public override int DoWork()
        {
            using (var client = new WebClient())
            {
                // D'apres https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/index.en.html
                // Ce contenu est mis a jour vers 16h tous les jours ouvrables (ie: tous les jour sauf samedi, dimanche et certains jours feriés internationale)
                var xmlContent = client.DownloadString("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml");
                var xdoc = XDocument.Parse(xmlContent);
                XNamespace ns = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref";
                //XNamespace nsGesme = "http://www.gesmes.org/xml/2002-08-01";
                // ReSharper disable PossibleNullReferenceException
                var rates = xdoc.Root.Element(ns + "Cube")
                                     .Elements(ns + "Cube")
                                     .SelectMany(nDayCube => nDayCube.Elements(ns + "Cube")
                                                             .Select(nRate => new ExchangeRate()
                                                             {
                                                                 Day = DateTime.ParseExact(nDayCube.Attribute("time").Value, "yyyy-MM-dd", CultureInfo.InvariantCulture),
                                                                 Ccy = nRate.Attribute("currency").Value,
                                                                 Rate = decimal.Parse(nRate.Attribute("rate").Value, CultureInfo.InvariantCulture)
                                                             }))
                                     .Where(ce => ce.Day < AtDate)
                                     .GroupByToDictionary(ce => ce.Day);
                // ReSharper restore PossibleNullReferenceException
                DateTime oldestDay = rates.OrderBy(kvp => kvp.Key).First().Key;
                var existingRates = new List<ExchangeRate>()
                //                  DB.Ref.LoadCollection<ExchangeRate>(DB.Ref.Mapper.GetColumnNameFor((ExchangeRate ce) => ce.Day) + " >= '" + oldestDay.ToString("yyyyMMdd") + "'")
                                          .GroupByToDictionary(ce => ce.Day);
                foreach (var ratesByDay in rates.OrderBy(kvp => kvp.Key))
                    if (!existingRates.ContainsKey(ratesByDay.Key))
                    {
                        Console.WriteLine($"Importing EUR exchange rates for day " + ratesByDay.Key.ToString("yyyy-MM-dd"));
                        //DB.Ref.CreateInDatabaseCollection(ratesByDay.Value);
                    }
                        
            }
            return SUCCESS;
        }
    }

    public sealed class ExchangeRate : BaseDTO<DateTime, string>
    {
        [DebuggerHidden][DbMappedField("date", IsPK = true, KeyOrderIndex = 1)]                                        public DateTime Day  { get; set; }
        [DebuggerHidden][DbMappedField("currency", IsPK = true, KeyOrderIndex = 2), DbMaxLength(3, MinimumLength = 3)] public string   Ccy  { get; set; }
        [DebuggerHidden][DbMappedField("rate")]                                                                        public decimal  Rate { get; set; }

        protected override IdTuple<DateTime, string> ClosedId
        {
            get { return new IdTuple<DateTime, string>(Day, Ccy); }
            set { Day = value.Id1; Ccy = value.Id2; }
        }

        public ExchangeRate()
            : this(true)
        {
        }
        public ExchangeRate(bool initializeModelValues)
        {
            if (!initializeModelValues)
                return;
            Day = new DateTime(1, 1, 1);
            Ccy = "XXX"; // Default until changed
            Rate = 0;
        }
    
        #region Cloneable
    
        public new ExchangeRate Clone() { return (ExchangeRate)(this as ICloneable).Clone(); }
        protected override BaseDTO CreateNewInstance() { return new ExchangeRate(); }
        public override void CopyAllFieldsFrom(BaseDTO source)
        {
            var from = (ExchangeRate)source;
            Day = from.Day;
            Ccy = from.Ccy;
            Rate = from.Rate;
        }

        #endregion
    }
}
