﻿using System;
using System.Threading;

using ApplicationBase.Business;

using Base = ApplicationBase.Business;


namespace DatabaseExplorer.Business
{
    public class BusinessFactory : ApplicationBase.Business.BusinessFactory
    {
        public BusinessFactory()
        {
        }

        public override                      Base.AuthenticationManager  CreateAuthenticationManager(ApplicationBase.Deployment.Data.EnvironmentConfig cfg)
                                                                                                                                           { return base.CreateAuthenticationManager(cfg); }
        public override                      Base.ConfigOfUser           CreateConfigOfUser(Base.AuthenticationManager m,
                                                                                            Base.ConfigOfUser.StorageTraits traits = null) { return base.CreateConfigOfUser(m); }
        public override ApplicationBase.Deployment.Deployer              CreateDeployer(ApplicationBase.Common.BootstrapConfig c)          { return base.CreateDeployer(c); }
        public override                      Base.ReleaseNotifier        CreateReleaseNotifier(ApplicationBase.Deployment.Config c, 
                                                                                               bool canSeeTechnicalRelease)                { return base.CreateReleaseNotifier(c, canSeeTechnicalRelease); }

        public override           Base.Dialoguing.Dialoguer              CreateDialoguer(string connectionStringDialoguerListener, 
                                                                                         string connectionStringDialoguerStarter,
                                                                                         Base.AuthenticationManager m,
                                                                                         SynchronizationContext context)                   { return base.CreateDialoguer(connectionStringDialoguerListener, connectionStringDialoguerStarter, m, context); }
        public override           Base.Dialoguing.Messenger              CreateMessenger(Base.Dialoguing.Dialoguer d)                      { return base.CreateMessenger(d); }
        public override           Base.Dialoguing.DataChangeEventManager CreateDataChangeEventManager(DataMapper.IDbMapper m,
                                                                                                      Base.Dialoguing.Dialoguer d)         { return base.CreateDataChangeEventManager(m, d); }
        public override                      Base.CommentPatternSuperSet CreateCommentPatternSuperSet()                                    { return base.CreateCommentPatternSuperSet(); }
    }
}
