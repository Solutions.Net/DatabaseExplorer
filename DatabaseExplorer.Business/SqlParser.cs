﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DatabaseExplorer.DAL
{
    public class SqlParser
    {
        public readonly StringBuilder Logs = new StringBuilder();
        public readonly StringBuilder Warnings = new StringBuilder();


        public EventHandler<EventLogArgs> Log;
        public EventHandler<EventLogArgs> Warn;


        public DatabaseInfo_Parsing Project { get; private set; }


        public SqlParser(DatabaseInfo_Parsing project)
        {
            Project = project;
            Log += (_, e) => Logs.Append(e.Message);
            Warn += (_, e) => Warnings.Append(e.Message);
        }

        protected void RaiseLog(string message)
        {
            if (Log != null)
                Log(this, new EventLogArgs(message));
        }
        protected void RaiseWarn(string message)
        {
            if (Warn != null)
                Warn(this, new EventLogArgs(message));
        }

        //public void ParseTable(Table table_to_parse)
        //{
        //    //TGSqlParser parser = ParseAndGetAST(table_to_parse.RawInfo.Source, table_to_parse.Name);
        //    //// Permet de visualiser l'AST généré via le Parsing
        //    ////if (table_to_parse.Name == "AssetCutOffs")
        //    ////{
        //    ////    var ast_exporter = new AST_Graphviz_Exporter();
        //    ////    string dot_code = ast_exporter.Export(parser.SqlStatements);
        //    ////}

        //    //// Exporte les info pertinentes dans des types de données clairs.
        //    //var v = new TableImporterVisitor();
        //    ////if (table_to_parse.Name == "Assets")
        //    ////{
        //    ////    table_to_parse.RawInfo.Source = "CREATE TABLE [dbo].[Assets](\r\n	[Asset_ID] [int] IDENTITY(1,1) NOT NULL,\r\n	[ISINCode] [nvarchar](50) NOT NULL,\r\n	[Designation] [nvarchar](120) NOT NULL,\r\n	[InstrumentType_ID] [int] NOT NULL,\r\n	[AssetType_ID] [int] NULL,\r\n	[AssetCategory_ID] [int] NULL,\r\n	[Issuer_ID] [int] NULL,\r\n	[Guarantor_ID] [int] NULL,\r\n	[IssueDate] [datetime] NULL,\r\n	[SettlementDate] [datetime] NULL,\r\n	[MaturityDate] [datetime] NULL,\r\n	[FaceValue] [float] NULL,\r\n	[IssueAmount] [float] NULL,\r\n	[RedemptionPrice] [float] NULL,\r\n	[IssuePrice] [float] NULL,\r\n	[Quotity] [int] NULL,\r\n	[HandlingQuotes] [bit] NOT NULL,\r\n	[QuoteDelay] [int] NOT NULL,\r\n	[Currency_ID] [int] NOT NULL,\r\n	[Source_ID] [int] NULL,\r\n	[Place_ID] [int] NULL,\r\n	[MarketPlace_ID] [int] NULL,\r\n	[IssueType_ID] [int] NULL,\r\n	[IssueQty] [float] NULL,\r\n	[AssetStatus_ID] [int] NULL,\r\n	[AssetLocked] [bit] NOT NULL,\r\n	[TickSize] [float] NULL,\r\n	[TickValue] [float] NULL,\r\n	[DailyPriceLimit] [int] NULL,\r\n	[ValuePoint] [float] NULL,\r\n	[FirstTradingDate] [datetime] NULL,\r\n	[LastTradingDate] [datetime] NULL,\r\n	[UnitBrokerageFee] [float] NULL,\r\n	[DealingMode_ID] [int] NOT NULL,\r\n	[SettlementOffset] [int] NULL,\r\n	[DefaultClearer_ID] [int] NULL,\r\n	[IssueSize_ID] [int] NULL,\r\n	[IssueSeniority_ID] [int] NULL,\r\n	[AssetStyle_ID] [int] NULL,\r\n	[AccountingCategory_ID] [int] NULL,\r\n	[EntryDate] [datetime] NULL,\r\n	[Withholdingtax] [bit] NULL,\r\n	[CurvePointNature_ID] [int] NULL,\r\n	[ValuationRule_ID] [int] NULL,\r\n	[PricingMethod_ID] [int] NULL,\r\n	[HandlingRatings] [bit] NOT NULL,\r\n	[RatingAgency_ID] [int] NULL,\r\n	[QuotePeriod_ID] [int] NULL,\r\n	[QuoteRollConvention_ID] [int] NULL,\r\n	[BrokerMarginRate] [float] NULL,\r\n	[AssetClass_ID] [int] NULL,\r\n	[AssetStrategy_ID] [int] NULL,\r\n	[QuoteBusinessDaysConvention_ID] [int] NULL,\r\n	[AssetZone_ID] [int] NULL,\r\n	[FiscalCategory_ID] [int] NULL,\r\n	[MainInstrument_ID] [int] NULL,\r\n	[MaximumHoldingsInfunds] [float] NULL,\r\n	[FundLegalNature_ID] [int] NULL,\r\n	[FundIsFeeder] [bit] NULL,\r\n	[FundMasterLegalNature_ID] [int] NULL,\r\n	[MinFaceAmount] [float] NULL,\r\n	[AIFFundType_ID] [int] NULL,\r\n	[AIFDomicilingCountry_ID] [int] NULL,\r\n CONSTRAINT [PK_Assets] PRIMARY KEY CLUSTERED \r\n(\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n	[Asset_ID] ASC\r\n)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]\r\n) ON [PRIMARY]";
        //    ////}
        //    //v.OnCreateTable = name =>
        //    //{
        //    //    if (table_to_parse.RawInfo.Name == name)
        //    //        return table_to_parse;
        //    //    var already_existing_table = table_to_parse.Project.Tables.FirstOrDefault(t => t.RawInfo != null && t.RawInfo.Name == name ||
        //    //                                                                                   t.RawInfo == null && t.Name == name); // On ne test t.Name que si RawInfo == null afin d'eviter le lazy parsing
        //    //    if (already_existing_table != null)
        //    //        return already_existing_table;

        //    //    var new_table = new SqlTable(table_to_parse.RawInfo) { Project = table_to_parse.Project, Name = name };
        //    //    new_table.Project.Tables.Add(new_table);

        //    //    return new_table;
        //    //};

        //    //v.Export(parser.SqlStatements); // Fill v.Tables
        //    //Debug.Assert(v.Tables.Count == 1, "Pas prévu encore un cas différent pour le code en dessous");
        //}

        //public void ParseForeignKey(ConstraintFK foreign_key)
        //{
        //    //TGSqlParser parser = ParseAndGetAST(foreign_key.RawInfo.Source, foreign_key.RawInfo.Name);
        //    //if (parser == null)
        //    //{
        //    //    if (Debugger.IsAttached)
        //    //        Debugger.Break();
        //    //    return;
        //    //}
        //    //// Permet de visualiser l'AST généré via le Parsing
        //    //AST_Graphviz_Exporter ast_exporter = new AST_Graphviz_Exporter();
        //    //string dot_code = ast_exporter.Export(parser.SqlStatements);

        //    //// Exporte les info pertinentes dans des types de données clairs.
        //    //var v = new TableKeyImporterVisitor(foreign_key.Project.Tables);
        //    //v.Warn += (_, e) => RaiseWarn(e.Message);
        //    //v.OnCreateKeyRelation += name =>
        //    //{
        //    //    if (name == foreign_key.RawInfo.Name)
        //    //        return foreign_key;
        //    //    var already_existing_fk = foreign_key.Project.ForeignKeyConstraints.FirstOrDefault(t => t.RawInfo != null && t.RawInfo.Name == name ||
        //    //                                                                                            t.RawInfo == null && t.Name == name); // On ne test t.Name que si RawInfo == null afin d'eviter le lazy parsing
        //    //    if (already_existing_fk != null)
        //    //        return already_existing_fk;


        //    //    var new_fk = new SqlKeyRelation(foreign_key.RawInfo) { Project = foreign_key.Project, Name = name };
        //    //    new_fk.Project.ForeignKeyConstraints.Add(new_fk);
        //    //    return new_fk;
        //    //};
        //    //v.FindRelations(parser.SqlStatements); // Fill v.KeyRelations
        //}

        ///*public*/
        //void ParseAll()
        //{
        //    foreach (var table in Project.Tables)
        //        table.EnsureIsParsed();

        //    foreach (var fkey in Project.ForeignKeyConstraints)
        //        fkey.EnsureIsParsed();


        //    //var proc_with_problems = new List<string>();
        //    //foreach (var proc in Project.Procedures)
        //    //{
        //    //    TGSqlParser parser = ParseAnGetAST(proc.RawInfo.Source, proc.RawInfo.Name, logger);
        //    //    if (parser == null)
        //    //    {
        //    //        proc_with_problems.Add(proc.RawInfo.Name);
        //    //        continue;
        //    //    }
        //    //    // Permet de visualiser l'AST généré via le Parsing
        //    //    //AST_Graphviz_Exporter ast_exporter = new AST_Graphviz_Exporter();
        //    //    //string dot_code = ast_exporter.Export(parser.SqlStatements);

        //    //    // Exporte les info pertinentes dans des types de données clairs.
        //    //    var v = new TableKeyImporterVisitor(results.Tables);
        //    //    v.FindRelations(proc.RawInfo, parser.SqlStatements); // Fill v.KeyRelations
        //    //    results.KeyRelations.AddRange(v.KeyRelations);
        //    //    if (warning_logger != null && !string.IsNullOrEmpty(v.Warnings))
        //    //        warning_logger(v.Warnings);
        //    //}

        //    //var view_with_problems = new List<string>();
        //    //foreach (var view in Project.Views)
        //    //{
        //    //    TGSqlParser parser = ParseAnGetAST(view.RawInfo.Source, view.Name, logger);
        //    //    if (parser == null)
        //    //    {
        //    //        view_with_problems.Add(view.Name);
        //    //        continue;
        //    //    }
        //    //    // Permet de visualiser l'AST généré via le Parsing
        //    //    //AST_Graphviz_Exporter ast_exporter = new AST_Graphviz_Exporter();
        //    //    //string dot_code = ast_exporter.Export(parser.SqlStatements);

        //    //    // Exporte les info pertinentes dans des types de données clairs.
        //    //    var v = new TableKeyImporterVisitor(results.Tables);
        //    //    v.FindRelations(view.RawInfo, parser.SqlStatements); // Fill v.KeyRelations
        //    //    results.KeyRelations.AddRange(v.KeyRelations);

        //    //    if (warning_logger != null && !string.IsNullOrEmpty(v.Warnings))
        //    //        warning_logger(v.Warnings);
        //    //}

        //    CleanUselessRelations();
        //}

        //private TGSqlParser ParseAndGetAST(string sql_code, string source_name)
        //{
        //    //sql_code = sql_code.Replace("Kronos.Types.", "");
        //    //sql_code = sql_code.Replace("PK__Kronos.", "PK__");

        //    RaiseLog(string.Format("Parsing {0}...", source_name));

        //    // See http://sqlparser.com/kb/javadoc/overview-summary.html
        //    TGSqlParser tGSqlParser = new TGSqlParser(TDbVendor.DbVMssql);
        //    tGSqlParser.SqlText.Text = sql_code;
        //    /*
        //    MatchCollection ms = Regex.Matches(sql_code, "^GO\r?$", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        //    int start = 0; 
        //    int end = ms.Count - 1;
        //    while (true)
        //    {
        //        int mid = (end + start) / 2;
        //        if (mid == start)
        //            break;
        //        string sql;
        //        int error = 0;
        //        try {
        //            sql = sql_code.Substring(ms[start].Index + 2, ms[mid].Index + ms[mid].Length - ms[start].Index - 2);
        //            tGSqlParser.SqlText.Text = sql;
        //            error = tGSqlParser.Parse(); } catch { error = -1; }
        //        if (error == -1)
        //        {
        //            end = mid;
        //            continue;
        //        }
        //        try {
        //            sql = sql_code.Substring(ms[mid].Index + 2, ms[end].Index + ms[end].Length - ms[mid].Index - 2);
        //            tGSqlParser.SqlText.Text = sql;
        //            error = tGSqlParser.Parse(); } catch { error = -1; }
        //        if (error == -1)
        //        {
        //            start = mid;
        //            continue;
        //        }
        //    }
        //    */

        //    int has_error = tGSqlParser.Parse();
        //    /*
        //    string toto = "";
        //    foreach (var statement in tGSqlParser.SqlStatements)
        //        toto += statement.GetType().Name + Environment.NewLine;
        //    */
        //    if (has_error != 0)
        //    {
        //        RaiseLog("\t Failed :(" + Environment.NewLine);
        //        RaiseLog("  (Nombre d'erreur : " + tGSqlParser.ErrorCount + Environment.NewLine);
        //        RaiseLog(",  Message : " + tGSqlParser.ErrorMessages + Environment.NewLine);
        //        return null;
        //    }
        //    RaiseLog("\t Success :)" + Environment.NewLine);
        //    return tGSqlParser;
        //}

        public void CleanUselessRelations()
        {
            //var lst = CleanUselessRelations(Project.ForeignKeyConstraints);
            //Project.ForeignKeyConstraints.Clear();
            //Project.ForeignKeyConstraints.AddRange(lst);
        }

        //public static List<ConstraintFK> CleanUselessRelations(List<ConstraintFK> keyRelations)
        //{
        //    var keyRelations_cleaned = new Dictionary<string, ConstraintFK>();
        //    foreach (ConstraintFK relation in keyRelations)
        //    {
        //        string relation_as_text = "";
        //        int cmpTable = string.Compare(relation.PrimaryKeyField.Table.Name, relation.ForeignKeyField.Table.Name);
        //        int cmpField = string.Compare(relation.PrimaryKeyField.Name, relation.ForeignKeyField.Name);
        //        if (cmpTable == 0 && cmpField == 0)
        //            continue; // La relation n'a aucun interet
        //        if (cmpTable == -1 || cmpTable == 0 && cmpField < 0)
        //            relation_as_text = relation.ToString();
        //        else
        //            relation_as_text = relation.ToStringReversed();
        //        if (!keyRelations_cleaned.ContainsKey(relation_as_text))
        //            keyRelations_cleaned.Add(relation_as_text, relation);
        //    }
        //    return keyRelations_cleaned.Values.ToList();
        //}


    }

    public class EventLogArgs : EventArgs
    {
        public string Message { get; private set; }
        public EventLogArgs(string message)
        {
            Message = message;
        }
    }
}
