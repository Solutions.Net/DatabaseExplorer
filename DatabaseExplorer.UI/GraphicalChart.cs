﻿using System;
using System.Drawing;

using TechnicalTools;

using ApplicationBase.Deployment.Data;

using DatabaseExplorer.Common;


namespace DatabaseExplorer.UI
{
    public static class GraphicalChart
    {
        public static readonly Color GreenOfficial = Color.FromArgb(87, 237, 125); // #57ED7D
        public static readonly Color GreenOfficialDarker = Color.FromArgb(15, 214, 67); // #0FD643 from http://paletton.com/#uid=12U0u0kkgS38LYTf9VIoPNitLGt
        public static readonly Color GreenOfficialLighter = Color.FromArgb(182, 251, 200); // #B6FBC8 from http://paletton.com/#uid=12U0u0kkgS38LYTf9VIoPNitLGt

        public static readonly Color BlueGreyOfficial = Color.FromArgb(55, 61, 87); // #373D57
        public static readonly Color Green = Color.FromArgb(69, 241, 114); // 87,237,125
        public static readonly Color Text  = Color.FromArgb(84, 87, 118);
        

        public static readonly Color ReportsForegroundSelected = Color.White;
        public static readonly Color ReportsForeground = Color.Black;
        public static readonly Color ReportsBackgroundSelected = Color.FromKnownColor(KnownColor.Highlight);
        public static readonly Color WhiteBackground = Color.White;
        public static readonly Color LightGrayBackground = Color.WhiteSmoke;
        public static readonly Color GreenBackground = Color.GreenYellow;

        public static readonly Color EditableFieldBackground = Color.MintCream;

        //Beau Indigo : #4B13D6 // 75,19,214
        //Violet acier : 4b4088

        #region MoneyIn Color

        public static readonly SolidBrush reportsForegroundBrushSelected = new SolidBrush(ReportsForegroundSelected);
        public static readonly SolidBrush reportsForegroundBrush = new SolidBrush(ReportsForeground);
        public static readonly SolidBrush reportsBackgroundBrushSelected = new SolidBrush(ReportsBackgroundSelected);
        public static readonly SolidBrush whiteBackgroundBrush = new SolidBrush(WhiteBackground);
        public static readonly SolidBrush lightGrayBackgroundBrush = new SolidBrush(LightGrayBackground);
        public static readonly SolidBrush greenBackgroundBrush = new SolidBrush(GreenBackground);

        #endregion


        public static Color GetColor(eEnvironment env)
        {
            return DB.Config.Environment.Domain == eEnvironment.Prod ? Green
                 : DB.Config.Environment.Domain == eEnvironment.Sandbox ? Color.Goldenrod
                 : DB.Config.Environment.Domain == eEnvironment.PreProd ? Color.DodgerBlue
                 : DB.Config.Environment.Domain == eEnvironment.Dev ? Text
                 : DB.Config.Environment.Domain == eEnvironment.LocalNoDB ? Color.LightSkyBlue
                 : ((Color?)null).ThrowIfNull($"Unknown Domain {DB.Config.Environment.Domain}!");
        }

        public static Color ColorFulBlinkingForUpdateAvailable
        {
            get
            {
                return GreenOfficial;
            }
        }
    }
}
