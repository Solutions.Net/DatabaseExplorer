﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using TechnicalTools.UI;

using ApplicationBase.UI;


namespace DatabaseExplorer.UI
{
    public partial class TestView : ApplicationBaseView
    {
        [Obsolete("For designer only", true)]
        protected TestView()
        : this(new object())
        {
        }
        public TestView(object _)      
         : base(_, ApplicationBase.DAL.Users.Feature.CanAccessExperimentalParts)
        {
            InitializeComponent();
            Text = "Tests";
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
            btnTestLogSync.Enabled = UserCanEdit;
            btnTestLogASync.Enabled = UserCanEdit;
        }

        #region Test des logs

        // Une methode thread safe pour logger et analyser dans que lthread s'est executé
        void log(string msg)
        {
            int id = Thread.CurrentThread.ManagedThreadId;
            mmoLogTestLogs.BeginInvoke((Action)(() => mmoLogTestLogs.Text += "Thread " + id + " : " + msg + Environment.NewLine));
        }

        private void btnTestLogSync_Click(object sender, EventArgs e)
        {
            DoUserAction(() =>
            {
                //Debug.Assert(SynchronizationContext.Current != null, "Must be run in GUI Thread !");
                //log("Begin Click Code");
                //int value = GetSomeData();
                //log("End Click Code");
            });
        }

        private void btnTestLogASync_Click(object sender, EventArgs e)
        {
            log("Test");
            //var scheduler = new ParallelTaskScheduler();
            //var t = new Task(async () =>
            //{
            //    Debug.Assert(SynchronizationContext.Current != null, "Must be run in GUI Thread !");
            //    log("Begin Click Code");
            //    int value = await Task.Run((Func<int>)GetSomeData);
            //    log("End Click Code");
            //});
            //try
            //{
            //    t.Start(scheduler);
            //}
            //catch
            //{
            //}




            //LogTask(async () =>
            //{
            //    Debug.Assert(SynchronizationContext.Current != null, "Must be run in GUI Thread !");
            //    log("Begin Click Code");
            //    int value = await Task.Run((Func<int>)GetSomeData);
            //    log("End Click Code");
            //});
        }


        public void DoUserAction(Action action)
        {
            Debug.Assert(SynchronizationContext.Current != null, "Must be call in GUI Thread !");
            log("Begin User Action");
            Exception exception = null;
            try
            {
                action();
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            log("End User Action" + (exception == null ? "" : " with error : " + exception.Message));
        }


        public Task DoUserAction<TTask>(Func<TTask> getUITask)
            where TTask : Task // do not remove, because if we want to user DoUserAction(Action)   and the action has a throw line in it, the compiler wants to call DoUserAction<TTask>(Func<TTask>. This code prevent this unintuitive behaviour (see http://stackoverflow.com/questions/23486452/lamdba-of-x-throw-inferred-to-match-funct-task-in-overloaded-metho)
        {
            Debug.Assert(SynchronizationContext.Current != null, "Must be call in GUI Thread !");
            log("Begin User Action");
            //.StartNew(getUITask).Unwrap();
            var uit = Task.Factory.StartNew(() => LogTask((Task)getUITask()), CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskScheduler.FromCurrentSynchronizationContext())
                .Unwrap()
                .ContinueWith(t =>
                {
                    Debug.Assert(SynchronizationContext.Current != null, "Should be run in GUI Thread !");
                    var exception = t.Exception == null ? null : t.Exception.InnerExceptions.Count == 1 ? t.Exception.InnerException : t.Exception;
                    log("End User Action" + (exception == null ? "" : " with error : " + exception.Message));

                }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
            return uit;
        }


        static readonly Func<Task, TaskScheduler> GetTaskSchedulerOf = Build_GetTaskSchedulerOf();
        static Func<Task, TaskScheduler> Build_GetTaskSchedulerOf()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return null;
            var p = typeof(Task).GetProperty("ExecutingTaskScheduler", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            if (p == null)
                throw new Exception("Find the new way to find the ExecutingTaskScheduler of a task!");
            return (t) => (TaskScheduler)p.GetGetMethod(true).Invoke(t, null);
        }
        static readonly Func<Task> GetCurrentTask = Build_GetCurrentTask();
        static Func<Task> Build_GetCurrentTask()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return null;
            var p = typeof(Task).GetProperty("InternalCurrent", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            if (p == null)
                throw new Exception("Find the new way to find the current task executing!");
            return () => (Task)p.GetGetMethod(true).Invoke(null, null);
        }

        public Task LogTask(Task task)
        {
            Task wrappingTask = null;
            wrappingTask = new Task((Action)(() =>
            {
                log("Begin LogAsync");
                var schedulerUsedByCaller = GetTaskSchedulerOf(wrappingTask);
                Debug.Assert(schedulerUsedByCaller != null);
                task.Start(schedulerUsedByCaller);
                task.Wait();
                var exception = task.Exception == null ? null : task.Exception.InnerExceptions.Count == 1 ? task.Exception.InnerException : task.Exception;
                log("End LogAsync" + (exception == null ? "" : " with error : " + exception.Message));
            }));
            return wrappingTask;
        }
        //public async void LogCall<TTask>(Func<TTask> getUITask)
        //    where TTask : Task // do not remove, because if we want to user LogCall(Action)   and the action has a throw line in it, the compiler wants to call LogCall<TTask>(Func<TTask>. This code prevent this unintuitive behaviour (see http://stackoverflow.com/questions/23486452/lamdba-of-x-throw-inferred-to-match-funct-task-in-overloaded-metho)
        //{

        //}
        public Task LogTask(Func<Task> func)
        {
            log("Begin LogAsync");
            var t = func(); // schedule the task to be run !
            t.ContinueWith(tt =>
            {
                var exception = t.Exception == null ? null : t.Exception.InnerExceptions.Count == 1 ? t.Exception.InnerException : t.Exception;
                log("End LogAsync" + (exception == null ? "" : " with error " + exception.Message));
            });
            return t;
            //var wrappingTask = LogTask(t);
            //var curTask = GetCurrentTask();
            //if (curTask == null)
            //    wrappingTask.Start();
            //else
            //{
            //    var schedulerUsedByCaller = GetTaskSchedulerOf(wrappingTask);
            //    Debug.Assert(schedulerUsedByCaller != null);
            //    wrappingTask.Start(schedulerUsedByCaller);
            //}
            //wrappingTask.Wait();
            //return (T)(object)t;
        }
        public T LogCall<T>(Func<T> func) where T : struct
        {
            log("Begin LogCall Sync");
            Exception exception = null;
            try
            {
                return func();
            }
            catch (Exception ex)
            {
                exception = ex;
                throw;
            }
            finally
            {
                log("End LogCall Sync" + (exception == null ? "" : " with error " + exception.Message));
            }
        }

        //private static readonly TaskFactory myTaskFactory = new TaskFactory(CancellationToken.None, TaskCreationOptions.DenyChildAttach, TaskContinuationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());

        //static bool ExecuteAndInterceptErrorOnUserAction(string actionName, Action action)
        //{
        //    try
        //    {
        //        action();
        //        Console.WriteLine("Action is OK");
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Action has thrown");
        //    }
        //    return false;
        //}
        #endregion Test des logs

        private void simpleButton1_Click(object sender, EventArgs e)
        {
        }
    }
}