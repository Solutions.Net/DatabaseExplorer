﻿namespace DatabaseExplorer.UI.Views
{
    sealed partial class DbConnectionGraph
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.jsGraph = new InteractiveJavascriptGraph.JsGraph.JsGraphControl();
            this.btnTurnPhysicsOnOff = new DevExpress.XtraEditors.SimpleButton();
            this.btnHierarchical = new DevExpress.XtraEditors.SimpleButton();
            this.btnSeeCycles = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // jsGraph
            // 
            this.jsGraph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jsGraph.Location = new System.Drawing.Point(3, 4);
            this.jsGraph.Name = "jsGraph";
            this.jsGraph.Size = new System.Drawing.Size(1180, 590);
            this.jsGraph.TabIndex = 1;
            // 
            // btnTurnPhysicsOnOff
            // 
            this.btnTurnPhysicsOnOff.Location = new System.Drawing.Point(6, 4);
            this.btnTurnPhysicsOnOff.Name = "btnTurnPhysicsOnOff";
            this.btnTurnPhysicsOnOff.Size = new System.Drawing.Size(122, 23);
            this.btnTurnPhysicsOnOff.TabIndex = 2;
            this.btnTurnPhysicsOnOff.Text = "Turn physics On / Off";
            this.btnTurnPhysicsOnOff.Click += new System.EventHandler(this.btnTurnPhysicsOnOff_Click);
            // 
            // btnHierarchical
            // 
            this.btnHierarchical.Location = new System.Drawing.Point(134, 4);
            this.btnHierarchical.Name = "btnHierarchical";
            this.btnHierarchical.Size = new System.Drawing.Size(98, 23);
            this.btnHierarchical.TabIndex = 3;
            this.btnHierarchical.Text = "Hierarchical";
            this.btnHierarchical.Click += new System.EventHandler(this.btnHierarchical_Click);
            // 
            // btnSeeCycles
            // 
            this.btnSeeCycles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSeeCycles.Location = new System.Drawing.Point(1021, 568);
            this.btnSeeCycles.Name = "btnSeeCycles";
            this.btnSeeCycles.Size = new System.Drawing.Size(75, 23);
            this.btnSeeCycles.TabIndex = 4;
            this.btnSeeCycles.Text = "See Cycles";
            this.btnSeeCycles.Click += new System.EventHandler(this.btnSeeCycles_Click);
            // 
            // DbConnectionGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSeeCycles);
            this.Controls.Add(this.btnHierarchical);
            this.Controls.Add(this.btnTurnPhysicsOnOff);
            this.Controls.Add(this.jsGraph);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "DbConnectionGraph";
            this.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Size = new System.Drawing.Size(1186, 598);
            this.ResumeLayout(false);

        }

        #endregion

        private InteractiveJavascriptGraph.JsGraph.JsGraphControl jsGraph;
        private DevExpress.XtraEditors.SimpleButton btnTurnPhysicsOnOff;
        private DevExpress.XtraEditors.SimpleButton btnHierarchical;
        private DevExpress.XtraEditors.SimpleButton btnSeeCycles;


    }
}