﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Tools;
using TechnicalTools.UI.DX;

using ApplicationBase.UI;

using DatabaseExplorer.Business.Model.Local;
using DatabaseExplorer.Business;


namespace DatabaseExplorer.UI.Views
{
    public sealed partial class ProjectViewNew : ApplicationBaseViewComposite
    {
        public Project Project { get; private set; }

        public static string LastConnectionString
        {
            get
            {
                try
                {
                    return Settings.Default.LastConnectionString;
                }
                catch
                {
                    return string.Empty;
                }
            }
            set
            {
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                {
                    Settings.Default.LastConnectionString = value?.Trim();
                    Settings.Default.Save();
                });
            }
        }

        public static string LastGeneratedCodeFolder
        {
            get
            {
                try
                {
                    return Settings.Default.LastGeneratedCodeFolder;
                }
                catch
                {
                    return string.Empty;
                }
            }
            set
            {
                ExceptionManager.Instance.EnsureNoExceptionButIgnoreIt(() =>
                {
                    Settings.Default.LastGeneratedCodeFolder = value?.Trim();
                    Settings.Default.Save();
                });
            }
        }

        public ProjectViewNew(Project project)
            : base(project, ApplicationBase.DAL.Users.Feature.AllowEverybody)
        {
            Project = project;
            Text = Project.Connection.InitialCatalog + " " + Project.Connection.DataSource;
            InitializeComponent();
            //if (!Environment.UserName.In("mlabau", "mickael.labau", "Astyan") || !Debugger.IsAttached)
            //{
                //mnuTracker.Visibility = BarItemVisibility.Never;
                //mnuKronos.Visibility = BarItemVisibility.Never;
            //}
            RefreshVisibilitiesAndEnabilities();
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(DbExplorerViewNew));

        void RefreshVisibilitiesAndEnabilities()
        {
            mnuGeneric_GenerateDALAndExplore.Enabled = Project != null;
            mnuGeneric_SeeDALGeneratedCode.Enabled = Project != null;
            mnuGeneric_GenerateDALToFolder.Enabled = Project != null;
            mnuGeneric_ProcessRuntimeCheckedConstraints.Enabled = Project != null;
            mnuGeneric_SeeSqlDependencies.Enabled = Project != null;
            mnuGeneric_SeeConnectionGraph.Enabled = Project != null;
            mnuGeneric_OpenDatabaseView.Enabled = Project != null;
            mnuGeneric_ExportSQLToFolder.Enabled = Project != null;
        }

        void ShowCodeInNotepad()
        {
            var mmo = new TechnicalTools.UI.DX.Controls.MemoEdit { Dock = DockStyle.Fill };
            this.DockAsDefault(mmo, "Generated source code");
            // A faire apres panel.Controls.Add, sinon le Add est treeees long
            mmo.BeginInvoke((Action)(() => mmo.Text = Project.GeneratedSourceCode.Values.Join(Environment.NewLine + Environment.NewLine + Environment.NewLine)));
            
        }

        void ShowConstraintsInNotepad()
        {
            var mmo = new TechnicalTools.UI.DX.Controls.MemoEdit { Dock = DockStyle.Fill };
            this.DockAsDefault(mmo, "Constraints to check at runtime");
            // A faire apres panel.Controls.Add, sinon le Add est treeees long
            mmo.Text = Project.RuntimeCheckedConstraints.Join(Environment.NewLine + Environment.NewLine + Environment.NewLine);
        }

        #region Demo

        private void mnuGenericGenerateDALAndExplore_ItemClick(object sender, ItemClickEventArgs e)
        {
            OpenExplorerFormFromProject();
        }

        private void mnuGenericSeeGeneratedCode_ItemClick(object sender, ItemClickEventArgs e)
        {
            GenerateDemoSourceCode(false, ShowCodeInNotepad);
        }

        private void mnuGeneric_GenerateDALToFolder_ItemClick(object sender, ItemClickEventArgs e)
        {
            saveFolderDialog.SelectedPath = ProjectViewNew.LastGeneratedCodeFolder;
            if (DialogResult.OK != saveFolderDialog.ShowDialog())
                return;
            ProjectViewNew.LastGeneratedCodeFolder = saveFolderDialog.SelectedPath;
            string path = saveFolderDialog.SelectedPath;

            this.ShowBusyWhileDoing("Generating DAL source code for LMT",
                                    pr => Project.GenerateSourceCodeFilesForLMT(true))
                .ContinueWithUIWorkOnSuccess(() =>
                {
                    foreach (var kvp in Project.GeneratedSourceCode)
                    {
                        string filename = Path.Combine(path, kvp.Key);
                        File.WriteAllText(filename, kvp.Value);
                    }
                    //CompileDALAndOpenExplorerForm();
                });
        }
        void OpenExplorerFormFromProject()
        {
            GenerateDemoSourceCode(false, CompileDALAndOpenExplorerForm);
        }
        void GenerateDemoSourceCode(bool oneFilePerClass, Action onSuccess = null)
        {
            this.ShowBusyWhileDoing("Generating code source from database...",
                                    pr => Project.GenerateDALSourceCodeFilesForLMT("DatabaseExplorer.DynamicTypes" + ++_generatedDalCount, oneFilePerClass, true))
                                .ContinueWithUIWorkOnSuccess(onSuccess);
        }
        static int _generatedDalCount;

        void CompileDALAndOpenExplorerForm()
        {
            Project.OnCompilationError -= OnCompilationError;
            Project.OnCompilationError += OnCompilationError;
            Project.IsNewVersion = true;
            this.ShowBusyWhileDoing("Compiling...", pr => Project.CompileOneFileDALForDemo())
                .ContinueWithUIWorkOnSuccess(OpenExplorerForm);
            //Project.CompileOneFileDALForDemo();
            //OpenExplorerForm();
        }

        void OnCompilationError(object sender, DynamicTypeAssemblyGeneratorBase.CompilationErrorEventArgs e)
        {
            this.BeginInvoke((Action)(() =>
            {
                if (e.Exception != null)
                    BusEvents.Instance.RaiseUserUnderstandableMessage("Compilation failed because" + Environment.NewLine +
                                                                      ExceptionManager.Instance.Format(e.Exception, true), _log);
                var mmo = new TechnicalTools.UI.DX.Controls.MemoEdit { Dock = DockStyle.Fill };
                this.DockAsDefault(mmo, "Source code");
                // A faire apres panel.Controls.Add, sinon le Add est treeees long
                mmo.Text = e.SourceCode;
                var dockPanel = mmo.Parent.Parent as DockPanel;
                dockPanel.ClosingPanel += (_, __) => { e.SourceCode = mmo.Text; e.Retry = true; fixCodeLock.Set(); };
                dockPanel.DockedAsTabbedDocument = false;
                dockPanel.MakeFloat();
            }));
            fixCodeLock.WaitOne();
        }
        AutoResetEvent fixCodeLock = new AutoResetEvent(false);


        void OpenExplorerForm()
        {
            var explorer = new DbExplorerViewNew(Project);
            this.DockAsDefault(explorer);
        }

        private void mnuGeneric_ProcessRuntimeCheckedConstraints_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.ShowBusyWhileDoing("Processing runtime checked constraints...",
                                 pr => Project.ProcessRuntimeCheckedConstraints(Project.DbInfos.Tables),
                                 ShowConstraintsInNotepad);
        }
        
        private void mnuGeneric_SeeDependenciesBetweenProcStoc_ItemClick(object sender, ItemClickEventArgs e)
        {
            string entityName = _lastEntityName;
            if (DialogResult.OK != InputBox.ShowDialog(this, ref entityName, "Afficher seulement les ancêtres ou les enfants de :", "Filtre...", validateOnEnter: true))
                return;
            _lastEntityName = entityName;

            this.ShowBusyWhileDoingUIWorkInPlace(pr =>
            {
                var explorer = new DbDependenciesGraph(Project, entityName);
                this.DockAsDefault(explorer);
            });
        }
        string _lastEntityName;
        private void mnuGeneric_SeeConnectionGraph_ItemClick(object sender, ItemClickEventArgs e)
        {
            var explorer = new DbConnectionGraph(Project);
            this.DockAsDefault(explorer);
        } 


        private void mnuGeneric_OpenDatabaseView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var dbWorkSpaceView = new DbWorkSpaceView(Project.DbInfos);
            this.DockAsDefault(dbWorkSpaceView);
        }

        private void mnuGeneric_ExportSQLToFolder_ItemClick(object sender, ItemClickEventArgs _)
        {
            Project.TrySomething();
            if (Project != null)
                return;

            var dbi = Project.DbInfos;
            //foreach (var table in dbi.Tables)
            //{
            //    var analyser = new Business.SqlAnalysing.SqlAnalyser();
            //    var res = analyser.Analyse(table.RawInfo.Source);
            //    var dot = res.GetDotDisplay();
            //}
            if (Project != null)
                return;
            //saveFolderDialog.SelectedPath = ProjectViewNew.LastGeneratedCodeFolder;
            //if (DialogResult.OK != saveFolderDialog.ShowDialog())
            //    return;
            //ProjectViewNew.LastGeneratedCodeFolder = saveFolderDialog.SelectedPath;

            //Project.ExportAllSqlObjectToFolder(saveFolderDialog.SelectedPath);
            //MessageBox.Show("Export effectué avec succès !");

            Regex reDbReading = new Regex(@"reader[.](IsDBNull|GetValue)\(.*");
            Func<string, string> cleanCode = code =>
            {
                var new_code = new StringBuilder(code.Length);
                var matches = reDbReading.Matches(code);
                var lastIndex = 0;
                foreach (Match m in matches)
                {
                    new_code.Append(code.Substring(lastIndex, m.Index - lastIndex));
                    new_code.Append("null;" + Environment.NewLine);
                    lastIndex = m.Index + m.Length;
                }
                new_code.Append(code.Substring(lastIndex));
                return new_code.ToString();
            };
            var baseFolder = @"C:\Projects\DBSchemaDump\CsCodes";

            using (SemaphoreSlim concurrencySemaphore = new SemaphoreSlim(25))
            {
                var codes = Task.WhenAll(PartnerDbInfos.Select(p => Task.Factory.StartNew(() =>
                {
                    var project = new Project();
                    string code = "";
                    Exception exception = null;
                    string hash = "ERROR";
                    try
                    {
                        project.Connection = new System.Data.SqlClient.SqlConnectionStringBuilder(p.Value);
                        concurrencySemaphore.Wait();
                        project.RunImport();

                        //foreach (var schema in project.DbInfos.Schemas)
                        //    foreach (var table in schema.Tables().OrderBy(t => t.FullName()))
                        //        table.Columns = table.Columns.OrderBy(c => c.Name).ToList();

                        project.GenerateSourceCodeFilesForTracker(false);
                        project.GenerateDALSourceCodeFiles("test");
                        code = cleanCode(project.GeneratedSourceCode.First().Value);
                        hash = code.ToMD5Hash();
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                        code = ex.ToString();
                        hash = "ERROR";
                    }
                    finally
                    {
                        concurrencySemaphore.Release();
                        var folder = baseFolder + @"\" + hash;
                        Directory.CreateDirectory(folder);
                        File.WriteAllText(folder + @"\" + p.Key + ".cs", code);
                    }
                }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default)).ToArray());
                codes.Wait();
            }

            GC.Collect();
        }
        
        static Dictionary<string, string> PartnerDbInfos
        {
            get
            {
                if (_PartnerDbInfos == null)
                {
                    _PartnerDbInfos = new Dictionary<string, string>();
                    var filename = "databases.csv";
                    if (File.Exists(filename))
                        foreach (var line in File.ReadAllLines(filename).Skip(1))
                        {
                            var parts = line.Split('\t');
                            string name = parts[0];
                            var builder = new SqlConnectionStringBuilder();
                            builder.IntegratedSecurity = bool.Parse(parts[1]);
                            builder.DataSource = parts[2];
                            builder.InitialCatalog = parts[3];
                            builder.UserID = parts[4];
                            builder.Password = parts[5];
                            _PartnerDbInfos.Add(name, builder.ConnectionString);
                        }
                }
                return _PartnerDbInfos;
            }
        }
        static Dictionary<string, string> _PartnerDbInfos;

        #endregion Demo

    }

}