﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;

using Microsoft.SqlServer.Management.Smo;

using InteractiveJavascriptGraph.JsGraph;

using TechnicalTools;
using TechnicalTools.Algorithm.Graph.TopologicalOrder;
using TechnicalTools.UI.DX;

using ApplicationBase.UI;

using DatabaseExplorer.Business.Model.Local;
using DatabaseExplorer.Business.Extensions;
using TechnicalTools.Diagnostics;

namespace DatabaseExplorer.UI.Views
{
    public sealed partial class DbConnectionGraph : ApplicationBaseView
    {
        readonly Project _project;
        //ConnectionProvider _connectionProvider;

        public DbConnectionGraph(Project project)
            : base(project, ApplicationBase.DAL.Users.Feature.AllowEverybody)
        {
            _project = project;
            Text = "Connection Graph";
            InitializeComponent();
            Load += (_, __) => BeginInvoke((Action)BuildGraphAndDisplayIt);
        }


        private void btnSeeCycles_Click(object sender, EventArgs e)
        {
            string report = CycleDetection.GetCycleAsString(_project.DbInfos.Tables, t => t.Name, t => t.FkConstraints(), fk => fk.ReferencedColumns.First().OwnerTable(), fk => fk.Name);
        }

        void BuildGraphAndDisplayIt()
        {
            var g = new Graph();
            foreach (var t in _project.DbInfos.Tables)
                g.Nodes.Add(t, new Node() {Table = t});
            foreach (var t in _project.DbInfos.Tables)
                foreach (var fk in t.FkConstraints())
                    g.Edges.Add(fk, new Edge() {Graph = g, FK = fk});

            jsGraph.DisplayGraph(g.Nodes.Values, g.Edges.Values);
            jsGraph.EventManager.Click += EventManagerOnClick;

            // Evite que le processus d'arrière plan reste en execution et empeche l'appli de se fermer
            Disposed += (_, __) => ExceptionManager.Instance.IgnoreException(() => jsGraph.TurnPhysics(false));
        }

        void EventManagerOnClick(object sender, JsClickEventArgs jsClickEventArgs)
        {
            // MessageBox.Show("Event Click: " + Environment.NewLine + jsClickEventArgs.OriginalJSON);
        }
        class Graph
        {
            public readonly Dictionary<Table, Node>        Nodes = new Dictionary<Table, Node>();
            public readonly Dictionary<ConstraintFK, Edge> Edges = new Dictionary<ConstraintFK, Edge>();
        }
        class Node : INode
        {
            public Graph  Graph   { get; set; }
            public Table  Table   { get; set; }
            public string Label   { get { return Table.Name; } }
            public string ToolTip { get; set; }
            public int?   Size    { get { return 20 + Table.Columns.Count; } }

            public string  Shape       { get { return null; } }
            public string  Color       { get { return null; } }
            public int?    SpaceAround { get { return null; } }
            public PointF? Position    { get { return null; } }
        }

        class Edge : IEdge
        {
            public Graph        Graph    { get; set; }
            public ConstraintFK FK       { get; set; }
            public INode        From     { get { return Graph.Nodes[FK.ForeignColumns.First().OwnerTable()]; } }
            public INode        To       { get { return Graph.Nodes[FK.ReferencedColumns.First().OwnerTable()]; } }
            public string       Label    { get { return FK.ForeignColumns.Select(fc => fc.Name).Join(); } }
            public string       ToolTip  { get { return "To : " + FK.ReferencedColumns.Select(fc => fc.Name).Join(); } }
            public int?         Width    { get { return FK.ForeignColumns.Length; } }
        }

        private void btnTurnPhysicsOnOff_Click(object sender, EventArgs e)
        {
            this.ShowBusyWhileDoingUIWorkInPlace("Turning on / off physics engine", pr =>
            {
                jsGraph.TurnPhysics(true);
            });
        }

        private void btnHierarchical_Click(object sender, EventArgs e)
        {
            this.ShowBusyWhileDoingUIWorkInPlace("Turning graph layout to \"hierarchical\"", pr =>
            {
                jsGraph.TurnToHierarchical(true);
            });
        }

    }
}
