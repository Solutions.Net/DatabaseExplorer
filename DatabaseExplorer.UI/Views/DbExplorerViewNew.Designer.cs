﻿namespace DatabaseExplorer.UI.Views
{
    sealed partial class DbExplorerViewNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lueTypes = new DevExpress.XtraEditors.LookUpEdit();
            this.lblWhatToDisplay = new DevExpress.XtraEditors.LabelControl();
            this.pnlTop = new DevExpress.XtraEditors.PanelControl();
            this.chkUseMultiThreadForSelect = new DevExpress.XtraEditors.CheckEdit();
            this.chkLetGridExploreNeighboringData = new DevExpress.XtraEditors.CheckEdit();
            this.btnDuplicate = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.gc = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gv = new TechnicalTools.UI.DX.EnhancedGridView();
            ((System.ComponentModel.ISupportInitialize)(this.lueTypes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTop)).BeginInit();
            this.pnlTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseMultiThreadForSelect.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLetGridExploreNeighboringData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).BeginInit();
            this.SuspendLayout();
            // 
            // lueTypes
            // 
            this.lueTypes.Location = new System.Drawing.Point(54, 4);
            this.lueTypes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lueTypes.Name = "lueTypes";
            this.lueTypes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lueTypes.Properties.NullText = "";
            this.lueTypes.Size = new System.Drawing.Size(184, 20);
            this.lueTypes.TabIndex = 0;
            this.lueTypes.EditValueChanged += new System.EventHandler(this.lueTypes_EditValueChanged);
            // 
            // lblWhatToDisplay
            // 
            this.lblWhatToDisplay.Location = new System.Drawing.Point(5, 7);
            this.lblWhatToDisplay.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lblWhatToDisplay.Name = "lblWhatToDisplay";
            this.lblWhatToDisplay.Size = new System.Drawing.Size(45, 13);
            this.lblWhatToDisplay.TabIndex = 1;
            this.lblWhatToDisplay.Text = "Afficher :";
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.chkUseMultiThreadForSelect);
            this.pnlTop.Controls.Add(this.chkLetGridExploreNeighboringData);
            this.pnlTop.Controls.Add(this.btnDuplicate);
            this.pnlTop.Controls.Add(this.lueTypes);
            this.pnlTop.Controls.Add(this.lblWhatToDisplay);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(3, 4);
            this.pnlTop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(1180, 33);
            this.pnlTop.TabIndex = 2;
            // 
            // chkUseMultiThreadForSelect
            // 
            this.chkUseMultiThreadForSelect.Location = new System.Drawing.Point(550, 4);
            this.chkUseMultiThreadForSelect.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkUseMultiThreadForSelect.Name = "chkUseMultiThreadForSelect";
            this.chkUseMultiThreadForSelect.Properties.Caption = "Utiliser le multithreading (nécessite une carte reseau 1gb pour réellement voir l" +
    "a différence)";
            this.chkUseMultiThreadForSelect.Size = new System.Drawing.Size(467, 19);
            this.chkUseMultiThreadForSelect.TabIndex = 5;
            this.chkUseMultiThreadForSelect.Visible = false;
            this.chkUseMultiThreadForSelect.CheckedChanged += new System.EventHandler(this.chkUseMultiThreadForSelect_CheckedChanged);
            // 
            // chkLetGridExploreNeighboringData
            // 
            this.chkLetGridExploreNeighboringData.Location = new System.Drawing.Point(244, 5);
            this.chkLetGridExploreNeighboringData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.chkLetGridExploreNeighboringData.Name = "chkLetGridExploreNeighboringData";
            this.chkLetGridExploreNeighboringData.Properties.Caption = "Explorer les données voisines de chaque ligne (plus lent)";
            this.chkLetGridExploreNeighboringData.Size = new System.Drawing.Size(300, 19);
            this.chkLetGridExploreNeighboringData.TabIndex = 4;
            this.chkLetGridExploreNeighboringData.Visible = false;
            this.chkLetGridExploreNeighboringData.CheckedChanged += new System.EventHandler(this.chkLetGridExploreNeighboringData_CheckedChanged);
            // 
            // btnDuplicate
            // 
            this.btnDuplicate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDuplicate.Location = new System.Drawing.Point(1069, 2);
            this.btnDuplicate.Name = "btnDuplicate";
            this.btnDuplicate.Size = new System.Drawing.Size(106, 23);
            this.btnDuplicate.TabIndex = 3;
            this.btnDuplicate.Text = "Dupliquer la vue";
            this.btnDuplicate.Click += new System.EventHandler(this.btnDuplicate_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.gc);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(3, 37);
            this.panelControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(1180, 557);
            this.panelControl2.TabIndex = 3;
            // 
            // gc
            // 
            this.gc.Cursor = System.Windows.Forms.Cursors.Default;
            this.gc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gc.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gc.Location = new System.Drawing.Point(2, 2);
            this.gc.MainView = this.gv;
            this.gc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gc.Name = "gc";
            this.gc.Size = new System.Drawing.Size(1176, 553);
            this.gc.TabIndex = 0;
            this.gc.UseEmbeddedNavigator = true;
            this.gc.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gv});
            this.gc.ViewRegistered += new DevExpress.XtraGrid.ViewOperationEventHandler(this.gc_ViewRegistered);
            // 
            // gv
            // 
            this.gv.GridControl = this.gc;
            this.gv.Name = "gv";
            this.gv.OptionsBehavior.ReadOnly = true;
            // 
            // DbExplorerViewNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.pnlTop);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "DbExplorerViewNew";
            this.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Size = new System.Drawing.Size(1186, 598);
            this.Load += new System.EventHandler(this.DbExplorerViewNew_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lueTypes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlTop)).EndInit();
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseMultiThreadForSelect.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLetGridExploreNeighboringData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit lueTypes;
        private DevExpress.XtraEditors.LabelControl lblWhatToDisplay;
        private DevExpress.XtraEditors.PanelControl pnlTop;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private TechnicalTools.UI.DX.EnhancedGridControl gc;
        private TechnicalTools.UI.DX.EnhancedGridView gv;
        private DevExpress.XtraEditors.SimpleButton btnDuplicate;
        private DevExpress.XtraEditors.CheckEdit chkLetGridExploreNeighboringData;
        private DevExpress.XtraEditors.CheckEdit chkUseMultiThreadForSelect;

    }
}