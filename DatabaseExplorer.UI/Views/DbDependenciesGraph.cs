﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Windows.Forms;

using TechnicalTools;
using TechnicalTools.Algorithm.Graph.TopologicalOrder;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.UI.DX;
using TechnicalTools.UI;

using ApplicationBase.UI;

using DatabaseExplorer.Business.Model.Local;
using DatabaseExplorer.Business;

using InteractiveJavascriptGraph.JsGraph;
using Microsoft.SqlServer.Management.Smo;
using DatabaseExplorer.Business.Extensions;
using DatabaseExplorer.DAL;

namespace DatabaseExplorer.UI.Views
{
    public sealed partial class DbDependenciesGraph : ApplicationBaseView
    {
        readonly Project _project;
        //ConnectionProvider _connectionProvider;
        Graph _graph;
        string _filterEntityName;

        public DbDependenciesGraph(Project project, string filterEntityName)
            : base(project, ApplicationBase.DAL.Users.Feature.AllowEverybody)
        {
            _project = project;
            _filterEntityName = filterEntityName;
            Text = "Dependencies Graph" + (string.IsNullOrWhiteSpace(filterEntityName) ? "" : " \"" + filterEntityName + "\"");
            // It seems to work now !
            //if (!DesignTimeHelper.IsInDesignMode && System.IntPtr.Size > 4) // Si 64 bit au runtime
            //    throw new UserUnderstandableException("Feature not available in 64 bit", null);
            InitializeComponent();
            Load += (_, __) => BeginInvoke((Action)BuildGraphAndDisplayIt);
        }


        private void btnSeeCycles_Click(object sender, EventArgs e)
        {
            string report = CycleDetection.GetCycleAsString(_project.DbInfos.Tables, t => t.Name, t => t.FkConstraints(), fk => fk.ReferencedColumns.First().OwnerTable(), fk => fk.Name);
        }

        void BuildGraphAndDisplayIt()
        {
            var deps = _project.DbInfos.GetAllDependencies();
            //VIEW, USER_TABLE, SQL_STORED_PROCEDURE, REFERENCED OBJECT DOESN'T EXIST, SQL_SCALAR_FUNCTION, SQL_TABLE_VALUED_FUNCTION
            //var ttt = deps.Select(d => d.referenced.object_type).Distinct().ToList();
            //var ttt2 = deps.Select(d => d.referencing.object_type).Distinct().ToList();

            _graph = new Graph();
            foreach (var d in deps)
            {
                if (!_graph.Nodes.ContainsKey(d.DependentObject))
                    _graph.Nodes.Add(d.DependentObject, new Node() { Graph = _graph, DObject = d.DependentObject });
                if (!_graph.Nodes.ContainsKey(d.DependencyObject))
                    _graph.Nodes.Add(d.DependencyObject, new Node() { Graph = _graph, DObject = d.DependencyObject });
                _graph.Edges.Add(d, new Edge() { Graph = _graph, Dependency = d });

                _graph.Nodes[d.DependentObject].AsReferencingIn.Add(_graph.Edges[d]);
                _graph.Nodes[d.DependencyObject].AsReferencedIn.Add(_graph.Edges[d]);
            }


            var nodesToShow = _graph.Nodes.Values.ToList().ToDictionary(n => n, n => true);
            var edgesToShow = _graph.Edges.Values.ToList().ToDictionary(e => e, e => true);
            if (!string.IsNullOrWhiteSpace(_filterEntityName))
            {
                // Calcule les noeuds a afficher absolument
                var toShow = nodesToShow.Keys.Where(n => n.DObject.Name.ToLower().Contains(_filterEntityName.ToLower())).ToList();
                nodesToShow.Clear();
                Action<Node, bool> contamine = null;
                contamine = (node, sens) =>
                {
                    if (nodesToShow.ContainsKey(node))
                        return; // Noeud deja visité, arrrete la recursion
                    nodesToShow[node] = true;
                    if (sens)
                        foreach (var e in node.AsReferencedIn)
                            contamine((Node)e.From, sens);
                    else
                        foreach (var e in node.AsReferencingIn)
                            contamine((Node)e.To, sens);
                };
                foreach (var rootNode in toShow)
                {
                    contamine(rootNode, false);
                    nodesToShow.Remove(rootNode);
                    contamine(rootNode, true);
                }

                edgesToShow = _graph.Edges.Values.ToDictionary(e => e, e => nodesToShow.ContainsKey((Node)e.From) && nodesToShow.ContainsKey((Node)e.To));
            }

            var positionsByNodes = FindNodePositions(nodesToShow.Where(kvp => kvp.Value).Select(kvp => kvp.Key), edgesToShow.Where(kvp => kvp.Value).Select(kvp => kvp.Key));
            foreach (var kvp in positionsByNodes)
                ((Node)kvp.Key).Position = kvp.Value;

            jsGraph.DisplayGraph(nodesToShow.Where(kvp => kvp.Value).Select(kvp => kvp.Key), edgesToShow.Where(kvp => kvp.Value).Select(kvp => kvp.Key));
            jsGraph.EventManager.DoubleClick += EventManagerOnDoubleClick;
            jsGraph.EventManager.Context += EventManagerOnContext;
            jsGraph.EventManager.DisplayEventsOnConsole = true;

            if (nodesToShow.Where(kvp => kvp.Value).Count() <= 20)
                btnTurnPhysicsOnOff.PerformClick(); // Ne fonctionne pas, voir un exemple ici pour mieux faire https://gist.github.com/amaitland/40394439ddefdf4e66b7

            // Evite que le processus d'arrière plan reste en execution et empeche l'appli de se fermer
            Disposed += (_, __) => ExceptionManager.Instance.IgnoreException(() => jsGraph.TurnPhysics(false));
            //string errors = deps.Where(d => d.DependencyObject.GetType() == "ERROR")
            //                    .Select(d => d.referencing.ShortType + " " + d.referencing.entity_name + " reference "
            //                               + " \"" + d.referenced.entity_name + "\" qui n'existe plus !")
            //                    .Join(Environment.NewLine);
            //if (!string.IsNullOrWhiteSpace(errors))
            //    this.Postpone(() => MessageBox.Show(errors));


        }

        static Dictionary<INode, PointF> FindNodePositions(IEnumerable<INode> nodes, IEnumerable<IEdge> edges)
        {
            var result = new Dictionary<INode, PointF>();
            var nodeIds = new Dictionary<INode, int>();
            var nodeByIds = new Dictionary<int, INode>();
            int IdSeed = 0;
            Func<INode, int> getId = node => nodeIds.GetOrCreate(node, () => { ++IdSeed; nodeByIds.Add(IdSeed, node); return IdSeed; });

            var tmpFile = Path.GetTempFileName();
            var nl = Environment.NewLine;
            string dotGraph = "digraph T {" + nl + 
                              "  graph [ranksep=4];" + nl + // ranksep = espace entre chaque etage de l'arbre, nodesep // espac eentre chaquen oeud d'un mme etage,
                              nodes.Select(n => "n" + getId(n) + " [label=\"" + n.Label + "\"];").Join(nl) +
                              nl+nl+nl+
                              edges.Select(e => "n" + getId(e.From) + " -> n" + getId(e.To) + " [label=\"" + e.Label + "\"];").Join(nl) +
                              nl +
                              "}";
            File.AppendAllText(tmpFile, dotGraph);

            try
            {
                ProcessStartInfo startInfo = new ProcessStartInfo
                {
                    FileName = @"Tools\graphviz\dot.exe",
                    Arguments = "-Tpng -Tplain \"" + tmpFile + "\" -O",
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };

                Process p = new Process
                {
                    StartInfo = startInfo,
                    EnableRaisingEvents = true
                };

                p.Start();
                p.WaitForExit();
                File.Delete(tmpFile);
            }
            catch (Exception)
            {
                DebugTools.Break();
                return result;
            }

            List<string> nodeDatas = File.ReadLines(tmpFile + ".plain").NotBlank().ToList();
            float maxY = 0;
            decimal spaceFactor = 100;
            foreach (var nodeData in nodeDatas)
            {
                var elts = nodeData.Split(new[] { ' ' });
                if (elts[0] == "node")
                {
                    int nodeid = int.Parse(elts[1].Substring(1)); // Remove 'n'
                    var node = nodeByIds[nodeid];
                    
                    decimal x = decimal.Parse(elts[2], System.Globalization.CultureInfo.InvariantCulture);
                    decimal y = decimal.Parse(elts[3], System.Globalization.CultureInfo.InvariantCulture);
                    result.Add(node, new PointF((float)(x * spaceFactor), (float)(y * spaceFactor)));
                    maxY = Math.Max(maxY, result[node].Y);
                }
            }
            // Inverse l'axe Y
            foreach (var kvp in result.ToList())
                result[kvp.Key] = new PointF(kvp.Value.X, maxY - kvp.Value.Y);

            File.Delete(tmpFile + ".plain");
            File.Delete(tmpFile + ".png");

            return result;
        }

        void EventManagerOnDoubleClick(object sender, JsClickEventArgs e)
        {
            if (e.Nodes.Count == 0)
                return;
            var node = e.Nodes[0] as Node;
            Debug.Assert(node != null);
            var newGraph = new DbDependenciesGraph(_project, node.DObject.Name);
            this.DockAsDefault(newGraph);
        }
        void EventManagerOnContext(object sender, JsContextEventArgs e)
        {
            var mnu = new ContextMenu();
            mnu.MenuItems.Add(new MenuItem("test"));
            mnu.Show(jsGraph, new Point((int)e.Pointer.Absolute.X, (int)e.Pointer.Absolute.Y));
        }

        class Graph
        {
            public readonly Dictionary<ScriptSchemaObjectBase, Node>  Nodes = new Dictionary<ScriptSchemaObjectBase, Node>();
            public readonly Dictionary<DatabaseInfo.Dependency, Edge>        Edges = new Dictionary<DatabaseInfo.Dependency, Edge>();
        }
        class Node : INode
        {
            public Graph                  Graph       { get; set; }
            public ScriptSchemaObjectBase DObject     { get; set; }
            public string                 Label       { get { return DObject.GetType().Name + " " + DObject.Name; } }
            public string                 ToolTip     { get; set; }
            public int?                   Size        { get { return 20 /*+ Table.Columns.Count*/; } }

            public string                 Shape       { get { return null; } }
            public string                 Color       { get { return DObject.GetType() == typeof(Table) ? "yellow" : DObject.GetType() == typeof(Microsoft.SqlServer.Management.Smo.View) ? "#C2FABC" : INode_Extensions.DefaultColor; } }
            public int?                   SpaceAround { get { return 2 + (AsReferencingIn.Count + AsReferencedIn.Count) / 6; } }
            public PointF?                Position    { get; set; }

            public readonly List<Edge> AsReferencingIn = new List<Edge>();
            public readonly List<Edge> AsReferencedIn = new List<Edge>();
        }

        class Edge : IEdge
        {
            public Graph                   Graph      { get; set; }
            public DatabaseInfo.Dependency Dependency { get; set; }
            public INode                   From       { get { return Graph.Nodes[Dependency.DependentObject]; } }
            public INode                   To         { get { return Graph.Nodes[Dependency.DependencyObject]; } }
            public string                  Label      { get { return ""; } }
            public string                  ToolTip    { get { return ""; } }
            public int?                    Width      { get { return 1; } }
        }

        private void btnTurnPhysicsOnOff_Click(object sender, EventArgs e)
        {
            //try
            //{
                jsGraph.TurnPhysics(true);
            //}
            //catch (Exception ex)
            //{
            //    ShowError(ex, "Cannot turn on/off physics engine now ! Please wait a little");
            //}
        }

        private void btnHierarchical_Click(object sender, EventArgs e)
        {
            //jsGraph.DoAsSoonAsBrowserIsInitialized(() => jsGraph.TurnPhysics(true));
            //try
            //{
                jsGraph.TurnToHierarchical(true);
            //}
            //catch (Exception ex)
            //{
            //    ShowError(ex, "Cannot turn on/off physics engine now ! Please wait a little");
            //}
        }

    }
}
