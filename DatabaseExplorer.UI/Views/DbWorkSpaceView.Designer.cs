﻿namespace DatabaseExplorer.UI.Views
{
    public partial class DbWorkSpaceView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeViewBound = new DevExpress.XtraTreeList.TreeList();
            this.colName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.lytMain = new DevExpress.XtraLayout.LayoutControl();
            this.tabContainer = new TechnicalTools.UI.DX.XtraUserControlComposite();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleSeparator1 = new DevExpress.XtraLayout.SimpleSeparator();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.treeViewBound)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytMain)).BeginInit();
            this.lytMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // treeViewBound
            // 
            this.treeViewBound.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.colName});
            this.treeViewBound.Location = new System.Drawing.Point(12, 12);
            this.treeViewBound.Name = "treeViewBound";
            this.treeViewBound.OptionsView.ShowColumns = false;
            this.treeViewBound.OptionsView.ShowIndicator = false;
            this.treeViewBound.ParentFieldName = "ParentNode";
            this.treeViewBound.RootValue = null;
            this.treeViewBound.Size = new System.Drawing.Size(192, 478);
            this.treeViewBound.TabIndex = 2;
            // 
            // colName
            // 
            this.colName.Caption = "Name";
            this.colName.FieldName = " ";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // lytMain
            // 
            this.lytMain.Controls.Add(this.tabContainer);
            this.lytMain.Controls.Add(this.treeViewBound);
            this.lytMain.Location = new System.Drawing.Point(3, 3);
            this.lytMain.Name = "lytMain";
            this.lytMain.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1066, 227, 450, 400);
            this.lytMain.Root = this.layoutControlGroup1;
            this.lytMain.Size = new System.Drawing.Size(702, 502);
            this.lytMain.TabIndex = 3;
            this.lytMain.Text = "lytMain";
            // 
            // tabContainer
            // 
            this.tabContainer.Location = new System.Drawing.Point(208, 12);
            this.tabContainer.Name = "tabContainer";
            this.tabContainer.Size = new System.Drawing.Size(480, 478);
            this.tabContainer.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.simpleSeparator1,
            this.layoutControlItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(702, 502);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.treeViewBound;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(196, 482);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // simpleSeparator1
            // 
            this.simpleSeparator1.AllowHotTrack = false;
            this.simpleSeparator1.Location = new System.Drawing.Point(680, 0);
            this.simpleSeparator1.Name = "simpleSeparator1";
            this.simpleSeparator1.Size = new System.Drawing.Size(2, 482);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.tabContainer;
            this.layoutControlItem2.Location = new System.Drawing.Point(196, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(484, 482);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // DbWorkSpaceView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lytMain);
            this.Name = "DbWorkSpaceView";
            this.Size = new System.Drawing.Size(727, 531);
            this.Load += new System.EventHandler(this.DbWorkSpaceView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.treeViewBound)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lytMain)).EndInit();
            this.lytMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.simpleSeparator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraTreeList.TreeList treeViewBound;
        private DevExpress.XtraLayout.LayoutControl lytMain;
        private TechnicalTools.UI.DX.XtraUserControlComposite tabContainer;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.SimpleSeparator simpleSeparator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colName;
    }
}
