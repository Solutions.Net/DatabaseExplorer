﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Logs;
using TechnicalTools.Model;
using TechnicalTools.UI.DX;
using TechnicalTools.Tools;

using ApplicationBase.UI;

using DatabaseExplorer.Business.Model.Local;
using DatabaseExplorer.Business.BaseDALClass;
using DataMapper;
using DatabaseExplorer.Business;

namespace DatabaseExplorer.UI.Views
{
    public sealed partial class DbExplorerView : ApplicationBaseView
    {
        readonly Project _project;
        //ConnectionProvider _connectionProvider;

        public DbExplorerView(Project project)
            : base(project, ApplicationBase.DAL.Users.Feature.AllowEverybody)
        {
            _project = project;
            Text = "Explorer";
            InitializeComponent();
            chkLetGridExploreNeighboringData_CheckedChanged(null, null);
            chkColumnAutoWidth_CheckedChanged(null, null);
            //gv.OptionsDetail.AllowExpandEmptyDetails = true; // Inutile de montrer les tab vide, sauf si on peut rajouter des enregistrements
        }
        static readonly ILogger _log = LogManager.Default.CreateLogger(typeof(DbExplorerView));

        private void chkColumnAutoWidth_CheckedChanged(object sender, EventArgs e)
        {
            gv.OptionsView.ColumnAutoWidth = chkConstraintColumnToForm.Checked;
            gv.BestFitMaxRowCount = 25;
            gv.BestFitColumns();
        }
        private void chkLetGridExploreNeighboringData_CheckedChanged(object sender, EventArgs e)
        {
            gv.OptionsDetail.EnableMasterViewMode = chkLetGridExploreNeighboringData.Checked;
        }

        private void DbExplorerView_Load(object sender, EventArgs e)
        {
            //_connectionProvider = new ConnectionProvider(_project.Settings.ConnectionString);
            //Disposed += frmExploreAllData_Disposed;

            lueTypes.Properties.UseDropDownRowsAsMaxCount = true;
            lueTypes.FillWithObjects(_project.GeneratedDAO.AllTypes.Keys, t => t.Name, true);
            lueTypes.UnselectAnyObject();

            DisplayTableCounts().ConfigureAwait(false);
        }

        async private Task DisplayTableCounts()
        {
            await Task.Factory.StartNew(() => DbDynamicStatistics.GetStatisticsFor(_project.GeneratedDAO));
            // Sinon les exception sont silencieuses !
            try
            {
                lueTypes.FillWithObjects(_project.GeneratedDAO.AllTypes.Keys, t => t.Name + " (" + DbDynamicStatistics.GetStatisticsFor(_project.GeneratedDAO).GetRowCountFor(_project.GeneratedDAO, _project.GeneratedDAO.Mapper.GetTableMapping(t).TableName) + ")", true);
            }
            catch (Exception)
            {
                DebugTools.Break();
            }
            //var allTasks = _project.GeneratedDAO.AllTypes.Keys.Select(t => Task.Factory.StartNew(() =>
            //{
            //    System.Threading.Thread.Sleep(2000);
            //    var mtable = DbMapper.GetTableMapping(t);
            //    return _project.GeneratedDAO.WithConnection(con =>
            //    {
            //        return new KeyValuePair<Type, long>(t, Convert.ToInt64(con.ExecuteScalar("SELECT count(*) FROM " + mtable.TableName))); // pas de crochet car [dbo.table] ne fonctionnerait pas
            //    });
            //})).ToArray();

            //var counts = (await Task.WhenAll(allTasks)).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            //ExecuteAndDisplayException("Display table count", () => // Sinon les exception sont silencieuses !
            //{
            //    lueTypes.FillWithObjects(counts.Keys, t => t.Name + " (" + counts[t] + ")", true);
            //});
        }

        private void lueTypes_EditValueChanged(object sender, EventArgs e)
        {
            gc.DataSource = null;
            gv.Columns.Clear();
            var type = lueTypes.SelectedValueAsObject<Type>();
            if (type == null)
                return;
            //this.Enabled = false; // La fenetre est normalement bloqué par le controle generique d'attente ci dessous, mais l'utilisateur peut utiliser sa molette car le focus est toujours sur lueTypes
            WaitControlGeneric.ShowOnControl(this, "Loading...", "it can take long if data are not (or not anymore) in cache",
                                            () =>
                                            { return type.ToSmartString().StartsWith(nameof(BaseDTO<string>) + ".<" + nameof(IdAsRef<string>) + "<")
                                                ? _project.GeneratedDAO.GetAllObjects(type) // gere le cas ou la table n'as pas de PK
                                                : (System.Collections.IList)type.GetMethod("GetEntitiesWhere").Invoke(null, new object[] { null });
                                            },
                                             DisplayNewDataSource,
                                             ex => { if (ex != null) BusEvents.Instance.RaiseUserUnderstandableMessage(ExceptionManager.Instance.Format(ex), _log); });


            //var keywords = ((System.Collections.IList) type.GetMethod("GetEntitiesWhere").Invoke(null, new object[] {null})).Cast<dynamic>().ToList();
            //string path = @"D:\Scripts\Tracker_DEMO_30_DEV\keywords"
            //foreach(var keyword in keywords)
            //{
                
            //}
        }
        
        void DisplayNewDataSource(object newDataSource)
        {
            //gv.PopulateColumns(lst);
            gc.DataSource = null;
            var sw = new Stopwatch();
            sw.Start();
            gc.DataSource = newDataSource; // utilise DbMapper.GetCn car les propriete de type collection sont exploré par DevExpress
            HideTechnicalColumns(newDataSource, gv);
            BeginInvoke((Action)(() =>
                {
                    sw.Stop();
                    Debug.WriteLine("Display in grid : " + sw.Elapsed.ToHumanReadableShortNotation());
                }));
        }

        private void gc_ViewRegistered(object sender, ViewOperationEventArgs e)
        {
            // essayez en priorite : https://www.devexpress.com/Support/Center/Question/Details/Q232865  (provient de http://stackoverflow.com/questions/11122316/two-level-deep-master-details-for-entity-framework)
            //https://www.devexpress.com/Support/Center/Example/Details/E2064
            // https://www.devexpress.com/Support/Center/Example/Details/E2290
            // https://www.devexpress.com/Support/Center/Question/Details/T119766
            var view = e.View as GridView;
            if (view == null || !view.IsDetailView)
                return;
            //string levelName = view.LevelName;
            if (view.DataSource == null)
                return;
            var propType = view.DataSource.GetType();
            if (propType.IsGenericType && propType.GetGenericTypeDefinition() == typeof(List<>))
            {
                // view.DataSource = null;
            }
            HideTechnicalColumns(view.DataSource, view);
        }

        internal static void HideTechnicalColumns(object lst, GridView view)
        {
            var itemType = lst.GetType().GenericTypeArguments[0];
            var pnoType = typeof(PropertyNotifierObject);
            var lType = typeof(List<>);
            Type entityType = itemType.BaseType;
            while (entityType.IsGenericType && entityType.GetGenericTypeDefinition() != typeof(BaseDTO<>))
                entityType = entityType.BaseType;
            var allTechnicalTypes = new[] { pnoType, lType, entityType };
            if (allTechnicalTypes.Any(itemType.IsSubclassOf))
            {
                var pis = itemType.GetProperties();
                foreach (DevExpress.XtraGrid.Columns.GridColumn col in view.Columns)
                    if (!pis.Any(pi => pi.Name == col.FieldName && pi.DeclaringType.NotIn(pnoType, lType, entityType)))
                        col.Visible = false;
            }
            view.BestFitMaxRowCount = 25;
            view.BestFitColumns();
        }

        private void chkUseMultiThreadForSelect_CheckedChanged(object sender, EventArgs e)
        {
            _project.GeneratedDAO.EnabledMultiThreading = chkUseMultiThreadForSelect.Checked;
        }

        private void btnDuplicate_Click(object sender, EventArgs e)
        {
            //this.DockAsDefault(new DevExpress.XtraEditors.MemoEdit() {Text = "test"});


        }
    }
}
