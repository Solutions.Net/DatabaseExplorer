﻿namespace DatabaseExplorer.UI.Views
{
    sealed partial class ProjectViewNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barManager = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.mnuGeneric = new DevExpress.XtraBars.BarSubItem();
            this.mnuGeneric_GenerateDALAndExplore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGeneric_SeeDALGeneratedCode = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGeneric_GenerateDALToFolder = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGeneric_ProcessRuntimeCheckedConstraints = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGeneric_SeeSqlDependencies = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGeneric_SeeConnectionGraph = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGeneric_ExportSQLToFolder = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.saveFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.mnuGeneric_OpenDatabaseView = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mnuGeneric,
            this.mnuGeneric_GenerateDALAndExplore,
            this.mnuGeneric_SeeDALGeneratedCode,
            this.mnuGeneric_ProcessRuntimeCheckedConstraints,
            this.mnuGeneric_SeeSqlDependencies,
            this.mnuGeneric_SeeConnectionGraph,
            this.mnuGeneric_GenerateDALToFolder,
            this.mnuGeneric_ExportSQLToFolder,
            this.mnuGeneric_OpenDatabaseView});
            this.barManager.MainMenu = this.bar1;
            this.barManager.MaxItemId = 41;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // mnuGeneric
            // 
            this.mnuGeneric.Caption = "Generic";
            this.mnuGeneric.Id = 22;
            this.mnuGeneric.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.mnuGeneric_GenerateDALAndExplore, "Generate DAL and Explore"),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric_SeeDALGeneratedCode),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric_GenerateDALToFolder),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric_ProcessRuntimeCheckedConstraints),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric_SeeSqlDependencies),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric_SeeConnectionGraph),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric_OpenDatabaseView),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric_ExportSQLToFolder)});
            this.mnuGeneric.Name = "mnuGeneric";
            // 
            // mnuGeneric_GenerateDALAndExplore
            // 
            this.mnuGeneric_GenerateDALAndExplore.Caption = "Generate DAL and Explore";
            this.mnuGeneric_GenerateDALAndExplore.Enabled = false;
            this.mnuGeneric_GenerateDALAndExplore.Id = 13;
            this.mnuGeneric_GenerateDALAndExplore.Name = "mnuGeneric_GenerateDALAndExplore";
            this.mnuGeneric_GenerateDALAndExplore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericGenerateDALAndExplore_ItemClick);
            // 
            // mnuGeneric_SeeDALGeneratedCode
            // 
            this.mnuGeneric_SeeDALGeneratedCode.Caption = "See DAL Generated Code";
            this.mnuGeneric_SeeDALGeneratedCode.Enabled = false;
            this.mnuGeneric_SeeDALGeneratedCode.Id = 14;
            this.mnuGeneric_SeeDALGeneratedCode.Name = "mnuGeneric_SeeDALGeneratedCode";
            this.mnuGeneric_SeeDALGeneratedCode.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericSeeGeneratedCode_ItemClick);
            // 
            // mnuGeneric_GenerateDALToFolder
            // 
            this.mnuGeneric_GenerateDALToFolder.Caption = "Generate DAL to folder";
            this.mnuGeneric_GenerateDALToFolder.Enabled = false;
            this.mnuGeneric_GenerateDALToFolder.Id = 38;
            this.mnuGeneric_GenerateDALToFolder.Name = "mnuGeneric_GenerateDALToFolder";
            this.mnuGeneric_GenerateDALToFolder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGeneric_GenerateDALToFolder_ItemClick);
            // 
            // mnuGeneric_ProcessRuntimeCheckedConstraints
            // 
            this.mnuGeneric_ProcessRuntimeCheckedConstraints.Caption = "Process runtime checked constraints";
            this.mnuGeneric_ProcessRuntimeCheckedConstraints.Enabled = false;
            this.mnuGeneric_ProcessRuntimeCheckedConstraints.Id = 24;
            this.mnuGeneric_ProcessRuntimeCheckedConstraints.Name = "mnuGeneric_ProcessRuntimeCheckedConstraints";
            this.mnuGeneric_ProcessRuntimeCheckedConstraints.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGeneric_ProcessRuntimeCheckedConstraints_ItemClick);
            // 
            // mnuGeneric_SeeSqlDependencies
            // 
            this.mnuGeneric_SeeSqlDependencies.Caption = "See Sql Dependency between proc";
            this.mnuGeneric_SeeSqlDependencies.Enabled = false;
            this.mnuGeneric_SeeSqlDependencies.Id = 24;
            this.mnuGeneric_SeeSqlDependencies.Name = "mnuGeneric_SeeSqlDependencies";
            this.mnuGeneric_SeeSqlDependencies.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGeneric_SeeDependenciesBetweenProcStoc_ItemClick);
            // 
            // mnuGeneric_SeeConnectionGraph
            // 
            this.mnuGeneric_SeeConnectionGraph.Caption = "See connection graph";
            this.mnuGeneric_SeeConnectionGraph.Enabled = false;
            this.mnuGeneric_SeeConnectionGraph.Id = 26;
            this.mnuGeneric_SeeConnectionGraph.Name = "mnuGeneric_SeeConnectionGraph";
            this.mnuGeneric_SeeConnectionGraph.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGeneric_SeeConnectionGraph_ItemClick);
            // 
            // mnuGeneric_ExportSQLToFolder
            // 
            this.mnuGeneric_ExportSQLToFolder.Caption = "Export SQL structure to folder";
            this.mnuGeneric_ExportSQLToFolder.Enabled = false;
            this.mnuGeneric_ExportSQLToFolder.Id = 39;
            this.mnuGeneric_ExportSQLToFolder.Name = "mnuGeneric_ExportSQLToFolder";
            this.mnuGeneric_ExportSQLToFolder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGeneric_ExportSQLToFolder_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager;
            this.barDockControlTop.Size = new System.Drawing.Size(808, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 372);
            this.barDockControlBottom.Manager = this.barManager;
            this.barDockControlBottom.Size = new System.Drawing.Size(808, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Manager = this.barManager;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 346);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(808, 26);
            this.barDockControlRight.Manager = this.barManager;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 346);
            // 
            // mnuGeneric_OpenDatabaseView
            // 
            this.mnuGeneric_OpenDatabaseView.Caption = "Open Database View";
            this.mnuGeneric_OpenDatabaseView.Enabled = false;
            this.mnuGeneric_OpenDatabaseView.Id = 40;
            this.mnuGeneric_OpenDatabaseView.Name = "mnuGeneric_OpenDatabaseView";
            this.mnuGeneric_OpenDatabaseView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGeneric_OpenDatabaseView_ItemClick);
            // 
            // ProjectViewNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ProjectViewNew";
            this.Size = new System.Drawing.Size(808, 372);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager;

        private System.Windows.Forms.FolderBrowserDialog saveFolderDialog;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarSubItem mnuGeneric;
        private DevExpress.XtraBars.BarButtonItem mnuGeneric_GenerateDALAndExplore;
        private DevExpress.XtraBars.BarButtonItem mnuGeneric_SeeDALGeneratedCode;
        private DevExpress.XtraBars.BarButtonItem mnuGeneric_ProcessRuntimeCheckedConstraints;
        private DevExpress.XtraBars.BarButtonItem mnuGeneric_SeeSqlDependencies;
        private DevExpress.XtraBars.BarButtonItem mnuGeneric_SeeConnectionGraph;
        private DevExpress.XtraBars.BarButtonItem mnuGeneric_GenerateDALToFolder;
        private DevExpress.XtraBars.BarButtonItem mnuGeneric_ExportSQLToFolder;
        private DevExpress.XtraBars.BarButtonItem mnuGeneric_OpenDatabaseView;
    }
}
