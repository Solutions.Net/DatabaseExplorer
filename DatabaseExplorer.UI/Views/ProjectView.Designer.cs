﻿namespace DatabaseExplorer.UI.Views
{
    sealed partial class ProjectView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.mnuGeneric = new DevExpress.XtraBars.BarSubItem();
            this.mnuGenericGenerateDALAndExplore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGenericSeeGeneratedCode = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGenericProcessRuntimeCheckedConstraints = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGenericSeeSqlDependencies = new DevExpress.XtraBars.BarButtonItem();
            this.mnuGenericSeeConnectionGraph = new DevExpress.XtraBars.BarButtonItem();
            this.mnuTracker = new DevExpress.XtraBars.BarSubItem();
            this.mnuTrackerGenerateDALAndExplore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuTrackerSeeGeneratedCode = new DevExpress.XtraBars.BarButtonItem();
            this.mnuTrackerProcessRuntimeCheckedConstraints = new DevExpress.XtraBars.BarButtonItem();
            this.mnuKronos = new DevExpress.XtraBars.BarSubItem();
            this.mnuKronosGenerateDALExportAndExplore = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.mnuGenericExportSQLToFolder = new DevExpress.XtraBars.BarButtonItem();
            this.saveFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.mnuLMT = new DevExpress.XtraBars.BarSubItem();
            this.mnuLMT_GenerateDALToFolder = new DevExpress.XtraBars.BarButtonItem();
            this.mnuLMT_GenerateAndExplore = new DevExpress.XtraBars.BarButtonItem();
            this.mnuLMT_SeeGeneratedCode = new DevExpress.XtraBars.BarButtonItem();
            this.mnuLMT_ProcessRuntimeCheckedConstraints = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager
            // 
            this.barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.mnuGenericGenerateDALAndExplore,
            this.mnuGenericSeeGeneratedCode,
            this.mnuTrackerGenerateDALAndExplore,
            this.mnuTrackerSeeGeneratedCode,
            this.mnuGeneric,
            this.mnuTracker,
            this.mnuGenericProcessRuntimeCheckedConstraints,
            this.mnuGenericSeeSqlDependencies,
            this.mnuTrackerProcessRuntimeCheckedConstraints,
            this.mnuGenericExportSQLToFolder,
            this.mnuGenericSeeConnectionGraph,
            this.mnuKronosGenerateDALExportAndExplore,
            this.mnuKronos,
            this.mnuLMT,
            this.mnuLMT_GenerateDALToFolder,
            this.mnuLMT_GenerateAndExplore,
            this.mnuLMT_SeeGeneratedCode,
            this.mnuLMT_ProcessRuntimeCheckedConstraints});
            this.barManager.MainMenu = this.bar1;
            this.barManager.MaxItemId = 38;
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 2";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGeneric),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuTracker),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuKronos),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuLMT)});
            this.bar1.OptionsBar.MultiLine = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Custom 2";
            // 
            // mnuGeneric
            // 
            this.mnuGeneric.Caption = "Generic";
            this.mnuGeneric.Id = 22;
            this.mnuGeneric.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.mnuGenericGenerateDALAndExplore, "Generate DAL and Explore"),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGenericSeeGeneratedCode),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGenericProcessRuntimeCheckedConstraints),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGenericSeeSqlDependencies),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuGenericSeeConnectionGraph)});
            this.mnuGeneric.Name = "mnuGeneric";
            // 
            // mnuGenericGenerateDALAndExplore
            // 
            this.mnuGenericGenerateDALAndExplore.Caption = "Generate DAL and Explore";
            this.mnuGenericGenerateDALAndExplore.Enabled = false;
            this.mnuGenericGenerateDALAndExplore.Id = 13;
            this.mnuGenericGenerateDALAndExplore.Name = "mnuGenericGenerateDALAndExplore";
            this.mnuGenericGenerateDALAndExplore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericGenerateDALAndExplore_ItemClick);
            // 
            // mnuGenericSeeGeneratedCode
            // 
            this.mnuGenericSeeGeneratedCode.Caption = "See Generated Code";
            this.mnuGenericSeeGeneratedCode.Enabled = false;
            this.mnuGenericSeeGeneratedCode.Id = 14;
            this.mnuGenericSeeGeneratedCode.Name = "mnuGenericSeeGeneratedCode";
            this.mnuGenericSeeGeneratedCode.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericSeeGeneratedCode_ItemClick);
            // 
            // mnuGenericProcessRuntimeCheckedConstraints
            // 
            this.mnuGenericProcessRuntimeCheckedConstraints.Caption = "Process runtime checked constraints";
            this.mnuGenericProcessRuntimeCheckedConstraints.Id = 24;
            this.mnuGenericProcessRuntimeCheckedConstraints.Name = "mnuGenericProcessRuntimeCheckedConstraints";
            this.mnuGenericProcessRuntimeCheckedConstraints.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericProcessRuntimeCheckedConstraints_ItemClick);
            // 
            // mnuGenericSeeSqlDependencies
            // 
            this.mnuGenericSeeSqlDependencies.Caption = "See Sql Dependency between proc";
            this.mnuGenericSeeSqlDependencies.Id = 24;
            this.mnuGenericSeeSqlDependencies.Name = "mnuGenericSeeSqlDependencies";
            this.mnuGenericSeeSqlDependencies.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericSeeDependenciesBetweenProcStoc_ItemClick);
            // 
            // mnuGenericSeeConnectionGraph
            // 
            this.mnuGenericSeeConnectionGraph.Caption = "See connection graph";
            this.mnuGenericSeeConnectionGraph.Id = 26;
            this.mnuGenericSeeConnectionGraph.Name = "mnuGenericSeeConnectionGraph";
            this.mnuGenericSeeConnectionGraph.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericSeeConnectionGraph_ItemClick);
            // 
            // mnuTracker
            // 
            this.mnuTracker.Caption = "Tracker";
            this.mnuTracker.Id = 23;
            this.mnuTracker.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.mnuTrackerGenerateDALAndExplore, "Generate DAL, export to folder, and explore..."),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.mnuTrackerSeeGeneratedCode, "See generated code for Tracker"),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuTrackerProcessRuntimeCheckedConstraints)});
            this.mnuTracker.Name = "mnuTracker";
            // 
            // mnuTrackerGenerateDALAndExplore
            // 
            this.mnuTrackerGenerateDALAndExplore.Caption = "Generate DAL For Tracker";
            this.mnuTrackerGenerateDALAndExplore.Enabled = false;
            this.mnuTrackerGenerateDALAndExplore.Id = 15;
            this.mnuTrackerGenerateDALAndExplore.Name = "mnuTrackerGenerateDALAndExplore";
            this.mnuTrackerGenerateDALAndExplore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuTrackerGenerateDALAndExplore_ItemClick);
            // 
            // mnuTrackerSeeGeneratedCode
            // 
            this.mnuTrackerSeeGeneratedCode.Caption = "See generated code for Tracker";
            this.mnuTrackerSeeGeneratedCode.Enabled = false;
            this.mnuTrackerSeeGeneratedCode.Id = 16;
            this.mnuTrackerSeeGeneratedCode.Name = "mnuTrackerSeeGeneratedCode";
            this.mnuTrackerSeeGeneratedCode.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuTrackerSeeGeneratedCode_ItemClick);
            // 
            // mnuTrackerProcessRuntimeCheckedConstraints
            // 
            this.mnuTrackerProcessRuntimeCheckedConstraints.Caption = "Process runtime checked constraints";
            this.mnuTrackerProcessRuntimeCheckedConstraints.Id = 25;
            this.mnuTrackerProcessRuntimeCheckedConstraints.Name = "mnuTrackerProcessRuntimeCheckedConstraints";
            this.mnuTrackerProcessRuntimeCheckedConstraints.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuTrackerProcessRuntimeCheckedConstraints_ItemClick);
            // 
            // mnuKronos
            // 
            this.mnuKronos.Caption = "Kronos";
            this.mnuKronos.Id = 30;
            this.mnuKronos.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Caption, this.mnuKronosGenerateDALExportAndExplore, "Generate DAL, export to folder, and explore...")});
            this.mnuKronos.Name = "mnuKronos";
            // 
            // mnuKronosGenerateDALExportAndExplore
            // 
            this.mnuKronosGenerateDALExportAndExplore.Caption = "Generate DAL For Kronos ";
            this.mnuKronosGenerateDALExportAndExplore.Enabled = false;
            this.mnuKronosGenerateDALExportAndExplore.Id = 28;
            this.mnuKronosGenerateDALExportAndExplore.Name = "mnuKronosGenerateDALExportAndExplore";
            this.mnuKronosGenerateDALExportAndExplore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericGenerateDALExportAndExplore_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager;
            this.barDockControlTop.Size = new System.Drawing.Size(808, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 372);
            this.barDockControlBottom.Manager = this.barManager;
            this.barDockControlBottom.Size = new System.Drawing.Size(808, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Manager = this.barManager;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 350);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(808, 22);
            this.barDockControlRight.Manager = this.barManager;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 350);
            // 
            // mnuGenericExportSQLToFolder
            // 
            this.mnuGenericExportSQLToFolder.Caption = "Export SQL structure To folder";
            this.mnuGenericExportSQLToFolder.Id = 27;
            this.mnuGenericExportSQLToFolder.Name = "mnuGenericExportSQLToFolder";
            this.mnuGenericExportSQLToFolder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuGenericExportSQLToFolder_ItemClick);
            // 
            // mnuLMT
            // 
            this.mnuLMT.Caption = "LMT";
            this.mnuLMT.Id = 33;
            this.mnuLMT.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuLMT_SeeGeneratedCode),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuLMT_GenerateDALToFolder),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuLMT_GenerateAndExplore),
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuLMT_ProcessRuntimeCheckedConstraints)});
            this.mnuLMT.Name = "mnuLMT";
            // 
            // mnuLMT_GenerateDALToFolder
            // 
            this.mnuLMT_GenerateDALToFolder.Caption = "Generate DAL to folder";
            this.mnuLMT_GenerateDALToFolder.Id = 34;
            this.mnuLMT_GenerateDALToFolder.Name = "mnuLMT_GenerateDALToFolder";
            this.mnuLMT_GenerateDALToFolder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuLMT_GenerateDALToFolder_ItemClick);
            // 
            // mnuLMT_GenerateAndExplore
            // 
            this.mnuLMT_GenerateAndExplore.Caption = "Explore";
            this.mnuLMT_GenerateAndExplore.Id = 35;
            this.mnuLMT_GenerateAndExplore.Name = "mnuLMT_GenerateAndExplore";
            this.mnuLMT_GenerateAndExplore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuLMT_GenerateAndExplore_ItemClick);
            // 
            // mnuLMT_SeeGeneratedCode
            // 
            this.mnuLMT_SeeGeneratedCode.Caption = "See Generated Code";
            this.mnuLMT_SeeGeneratedCode.Id = 36;
            this.mnuLMT_SeeGeneratedCode.Name = "mnuLMT_SeeGeneratedCode";
            this.mnuLMT_SeeGeneratedCode.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuLMT_SeeGeneratedCode_ItemClick);
            // 
            // mnuLMT_ProcessRuntimeCheckedConstraints
            // 
            this.mnuLMT_ProcessRuntimeCheckedConstraints.Caption = "Process runtime checked constraints";
            this.mnuLMT_ProcessRuntimeCheckedConstraints.Id = 37;
            this.mnuLMT_ProcessRuntimeCheckedConstraints.Name = "mnuLMT_ProcessRuntimeCheckedConstraints";
            this.mnuLMT_ProcessRuntimeCheckedConstraints.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuLMT_ProcessRuntimeCheckedConstraints_ItemClick);
            // 
            // ProjectView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "ProjectView";
            this.Size = new System.Drawing.Size(808, 372);
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarButtonItem mnuGenericGenerateDALAndExplore;
        private DevExpress.XtraBars.BarButtonItem mnuGenericSeeGeneratedCode;
        private DevExpress.XtraBars.BarButtonItem mnuTrackerGenerateDALAndExplore;
        private DevExpress.XtraBars.BarButtonItem mnuTrackerSeeGeneratedCode;
        private DevExpress.XtraBars.BarSubItem mnuGeneric;
        private DevExpress.XtraBars.BarSubItem mnuTracker;
        private DevExpress.XtraBars.BarButtonItem mnuGenericProcessRuntimeCheckedConstraints;
        private DevExpress.XtraBars.BarButtonItem mnuGenericSeeSqlDependencies;
        private DevExpress.XtraBars.BarButtonItem mnuTrackerProcessRuntimeCheckedConstraints;
        private System.Windows.Forms.FolderBrowserDialog saveFolderDialog;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarButtonItem mnuGenericSeeConnectionGraph;
        private DevExpress.XtraBars.BarButtonItem mnuGenericExportSQLToFolder;
        private DevExpress.XtraBars.BarButtonItem mnuKronosGenerateDALExportAndExplore;
        private DevExpress.XtraBars.BarSubItem mnuKronos;
        private DevExpress.XtraBars.BarSubItem mnuLMT;
        private DevExpress.XtraBars.BarButtonItem mnuLMT_GenerateDALToFolder;
        private DevExpress.XtraBars.BarButtonItem mnuLMT_GenerateAndExplore;
        private DevExpress.XtraBars.BarButtonItem mnuLMT_SeeGeneratedCode;
        private DevExpress.XtraBars.BarButtonItem mnuLMT_ProcessRuntimeCheckedConstraints;
    }
}
