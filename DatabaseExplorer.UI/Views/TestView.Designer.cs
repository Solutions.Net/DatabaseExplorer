﻿namespace DatabaseExplorer.UI
{
    partial class TestView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.gcTests = new TechnicalTools.UI.DX.EnhancedGridControl();
            this.gvTests = new TechnicalTools.UI.DX.EnhancedGridView();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.mmoLogTestLogs = new DevExpress.XtraEditors.MemoEdit();
            this.btnTestLogASync = new DevExpress.XtraEditors.SimpleButton();
            this.btnTestLogSync = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmoLogTestLogs.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.gcTests);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.mmoLogTestLogs);
            this.layoutControl1.Controls.Add(this.btnTestLogASync);
            this.layoutControl1.Controls.Add(this.btnTestLogSync);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1181, 289, 452, 526);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(810, 484);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // gcTests
            // 
            this.gcTests.Location = new System.Drawing.Point(12, 132);
            this.gcTests.MainView = this.gvTests;
            this.gcTests.Name = "gcTests";
            this.gcTests.Size = new System.Drawing.Size(786, 340);
            this.gcTests.TabIndex = 16;
            this.gcTests.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTests});
            // 
            // gvTests
            // 
            this.gvTests.GridControl = this.gcTests;
            this.gvTests.Name = "gvTests";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 106);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(786, 22);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 15;
            this.simpleButton1.Text = "Do something (that may cause alteration on database !!!)";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // mmoLogTestLogs
            // 
            this.mmoLogTestLogs.Location = new System.Drawing.Point(176, 42);
            this.mmoLogTestLogs.Name = "mmoLogTestLogs";
            this.mmoLogTestLogs.Size = new System.Drawing.Size(610, 48);
            this.mmoLogTestLogs.StyleController = this.layoutControl1;
            this.mmoLogTestLogs.TabIndex = 9;
            // 
            // btnTestLogASync
            // 
            this.btnTestLogASync.Location = new System.Drawing.Point(24, 68);
            this.btnTestLogASync.Name = "btnTestLogASync";
            this.btnTestLogASync.Size = new System.Drawing.Size(148, 22);
            this.btnTestLogASync.StyleController = this.layoutControl1;
            this.btnTestLogASync.TabIndex = 7;
            this.btnTestLogASync.Text = "Test Log Async";
            this.btnTestLogASync.Click += new System.EventHandler(this.btnTestLogASync_Click);
            // 
            // btnTestLogSync
            // 
            this.btnTestLogSync.Location = new System.Drawing.Point(24, 42);
            this.btnTestLogSync.Name = "btnTestLogSync";
            this.btnTestLogSync.Size = new System.Drawing.Size(148, 22);
            this.btnTestLogSync.StyleController = this.layoutControl1;
            this.btnTestLogSync.TabIndex = 8;
            this.btnTestLogSync.Text = "Test Log Sync";
            this.btnTestLogSync.Click += new System.EventHandler(this.btnTestLogSync_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem1,
            this.layoutControlItem9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(810, 484);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem8,
            this.layoutControlItem7});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(790, 94);
            this.layoutControlGroup2.Text = "Log Test";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnTestLogSync;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(152, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(152, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(152, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.mmoLogTestLogs;
            this.layoutControlItem8.Location = new System.Drawing.Point(152, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(614, 52);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnTestLogASync;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(152, 26);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(152, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(152, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 94);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(790, 26);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.gcTests;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 120);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(790, 344);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // TestView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "TestView";
            this.Size = new System.Drawing.Size(810, 484);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mmoLogTestLogs.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.MemoEdit mmoLogTestLogs;
        private DevExpress.XtraEditors.SimpleButton btnTestLogASync;
        private DevExpress.XtraEditors.SimpleButton btnTestLogSync;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private TechnicalTools.UI.DX.EnhancedGridControl gcTests;
        private TechnicalTools.UI.DX.EnhancedGridView gvTests;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
    }
}
