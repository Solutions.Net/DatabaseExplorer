﻿using System;
using System.Linq;

using TechnicalTools;
using TechnicalTools.UI;

using ApplicationBase.UI;

using DatabaseExplorer.DAL;
using DatabaseExplorer.Business.Extensions;

namespace DatabaseExplorer.UI.Views
{
    public sealed partial class DbWorkSpaceView : ApplicationBaseView
    {
        public DatabaseInfo Db { get { return (DatabaseInfo)BusinessObject; } }

        [Obsolete("Designer Only", true)]
        public DbWorkSpaceView()
            : this (null)
        {
        }

        public DbWorkSpaceView(DatabaseInfo db)
            : base(db, ApplicationBase.DAL.Users.Feature.AllowEverybody)
        {
            InitializeComponent();
            if (DesignTimeHelper.IsInDesignMode)
                return;
            colName.FieldName = nameof(Node.Name);
        }

        protected override void RefreshEnabilitiesAndVisibilities()
        {
        }

        private void DbWorkSpaceView_Load(object sender, EventArgs e)
        {
            var nDb = new Node() { Name = Db.SqlServerVersion };
            var allNodes = nDb.WrapInList();

            foreach (var schema in Db.Schemas.OrderBy(s => s.Name))
            {
                var nSchema = new Node() { Name = schema.Name, ParentNode = nDb, BusinessObject = Db };
                allNodes.Add(nSchema);

                var nTables = new Node() { Name = "Tables", ParentNode = nSchema };
                allNodes.Add(nTables);
                foreach (var table in schema.Tables().OrderBy(s => s.Name))
                    allNodes.Add(new Node() { Name = table.Name, ParentNode = nTables, BusinessObject = table });
                var nViews = new Node() { Name = "Views", ParentNode = nSchema };
                allNodes.Add(nViews);
                foreach (var view in schema.Views().OrderBy(s => s.Name))
                    allNodes.Add(new Node() { Name = view.Name, ParentNode = nViews, BusinessObject = view });
                var nStoredProcedures = new Node() { Name = "Stored Procedures", ParentNode = nSchema };
                allNodes.Add(nStoredProcedures);
                foreach (var procedure in schema.StoredProcedures().OrderBy(s => s.Name))
                    allNodes.Add(new Node() { Name = procedure.Name, ParentNode = nStoredProcedures, BusinessObject = procedure });
                var nUserDefinedFunctions = new Node() { Name = "User Defined Functions", ParentNode = nSchema };
                allNodes.Add(nUserDefinedFunctions);
                foreach (var udf in schema.UserDefinedFunctions().OrderBy(s => s.Name))
                    allNodes.Add(new Node() { Name = udf.Name, ParentNode = nUserDefinedFunctions, BusinessObject = udf });
                //var nTableValuedFunctionsInline = new Node() { Name = "Table-valued Functions Inline", ParentNode = nSchema };
                //allNodes.Add(nTableValuedFunctionsInline);
                //foreach (var itvf in schema.InlineTableFunction.OrderBy(s => s.Name))
                //    allNodes.Add(new Node() { Name = itvf.Name, ParentNode = nTableValuedFunctionsInline, BusinessObject = itvf });
                //var nScalarValuedFunctions = new Node() { Name = "Scalar-valued Functions", ParentNode = nSchema };
                //allNodes.Add(nScalarValuedFunctions);
                //foreach (var svf in schema.ScalarFunctions.OrderBy(s => s.Name))
                //    allNodes.Add(new Node() { Name = svf.Name, ParentNode = nScalarValuedFunctions, BusinessObject = svf });
                //var nAggregateFunctions = new Node() { Name = "Aggregate Functions", ParentNode = nSchema };
                //allNodes.Add(nAggregateFunctions);
                //foreach (var agg in schema.AggregateFunctions.OrderBy(s => s.Name))
                //    allNodes.Add(new Node() { Name = agg.Name, ParentNode = nAggregateFunctions, BusinessObject = agg });
            }


            treeViewBound.ParentFieldName = nameof(Node.ParentNode);
            treeViewBound.KeyFieldName = nameof(Node.Myself);
            treeViewBound.DataSource = allNodes;
            treeViewBound.ForceInitialize();
            treeViewBound.ExpandAll();
        }


        class Node
        {
            public Node   Myself         { get { return this; } }
            public Node   ParentNode     { get; internal set; }
            public string Name           { get; internal set; }
            public object BusinessObject { get; internal set; }
        }

    }

}
