﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using TechnicalTools;
using TechnicalTools.Model;
using TechnicalTools.Model.Cache;
using TechnicalTools.Tools.Sql;

using DAL_Generator.Model;
using GenericDALBasics;


namespace DatabaseExplorer.CustomersDAL
{
    namespace Dbo
    {
        [DbMappedTable("dbo.Settings")]
        public sealed partial class Setting : GeneratedEntity<IdTuple<int>> 
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(Setting_ID);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Setting_ID = (int)value.Value.Keys[0];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int setting_id_)
            {
                return new IdTuple<int>(setting_id_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Setting_ID FROM dbo.Settings" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["Setting_ID"])));
            }
    
    
            public static List<Setting> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Setting, IdTuple<int>>(ids);
                return entities;
            }
    
    
            [DbMappedField("Setting_ID", IsPK = true)]       public    int Setting_ID     { get { return     _Setting_ID; } set {                               SetTechnicalProperty(ref      _Setting_ID, value); RaiseIdChanged(); } }    int _Setting_ID;
            [DbMappedField("Environment_ID")]                public    int Environment_ID { get { return _Environment_ID; } set {                                        SetProperty(ref  _Environment_ID, value); } }                      int _Environment_ID;
            [DbMappedField("Entity"), DbMaxLength(50)]       public string Entity         { get { return         _Entity; } set {   ChkFieldLen(ref value, 50);          SetProperty(ref          _Entity, value); } }                   string _Entity;
            [DbMappedField("Record_ID")]                     public   int? Record_ID      { get { return      _Record_ID; } set {                                        SetProperty(ref       _Record_ID, value); } }                     int? _Record_ID;
            [DbMappedField("Cat1"), DbMaxLength(100)]        public string Cat1           { get { return           _Cat1; } set {  ChkFieldLen(ref value, 100);          SetProperty(ref            _Cat1, value); } }                   string _Cat1;
            [DbMappedField("Cat2"), DbMaxLength(100)]        public string Cat2           { get { return           _Cat2; } set {  ChkFieldLen(ref value, 100);          SetProperty(ref            _Cat2, value); } }                   string _Cat2;
            [DbMappedField("Name"), DbMaxLength(255)]        public string Name           { get { return           _Name; } set {  ChkFieldLen(ref value, 255);          SetProperty(ref            _Name, value); } }                   string _Name;
            [DbMappedField("Script")]                        public string Script         { get { return         _Script; } set {                                        SetProperty(ref          _Script, value); } }                   string _Script;
            [DbMappedField("Restriction"), DbMaxLength(100)] public string Restriction    { get { return    _Restriction; } set {  ChkFieldLen(ref value, 100);          SetProperty(ref     _Restriction, value); } }                   string _Restriction;
            [DbMappedField("SLOC")]                          public   int? SLOC           { get { return           _SLOC; } set {                                        SetProperty(ref            _SLOC, value); } }                     int? _SLOC;
    
    
    
    
            public Setting()
            {
            }
            public Setting(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Entity = string.Empty;
                _Cat1 = string.Empty;
                _Name = string.Empty;
                _Script = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<Setting> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Setting();
                    item._Setting_ID = (int)reader.GetValue(0);
                    item._Environment_ID = (int)reader.GetValue(1);
                    item._Entity = (string)reader.GetValue(2);
                    item._Record_ID = reader.IsDBNull(3) ? null : (int?)reader.GetValue(3);
                    item._Cat1 = (string)reader.GetValue(4);
                    item._Cat2 = reader.IsDBNull(5) ? null : (string)reader.GetValue(5);
                    item._Name = (string)reader.GetValue(6);
                    item._Script = (string)reader.GetValue(7);
                    item._Restriction = reader.IsDBNull(8) ? null : (string)reader.GetValue(8);
                    item._SLOC = reader.IsDBNull(9) ? null : (int?)reader.GetValue(9);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (10): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 10)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Setting Clone() { return (Setting)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int>> CreateNewInstance() { return new Setting(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int>> source)
            {
                var from = (Setting)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Environment_ID = from._Environment_ID;
                _Entity = from._Entity;
                _Record_ID = from._Record_ID;
                _Cat1 = from._Cat1;
                _Cat2 = from._Cat2;
                _Name = from._Name;
                _Script = from._Script;
                _Restriction = from._Restriction;
                _SLOC = from._SLOC;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.Logs")]
        public sealed partial class Log : GeneratedEntity<IdTuple<int>> 
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(Log_ID);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Log_ID = (int)value.Value.Keys[0];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int log_id_)
            {
                return new IdTuple<int>(log_id_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Log_ID FROM dbo.Logs" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["Log_ID"])));
            }
    
    
            public static List<Log> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Log, IdTuple<int>>(ids);
                return entities;
            }
    
    
            [DbMappedField("Log_ID", IsPK = true)]       public      int Log_ID         { get { return         _Log_ID; } set {                                                                   SetTechnicalProperty(ref          _Log_ID, value); RaiseIdChanged(); } }      int _Log_ID;
            [DbMappedField("Environment_ID")]            public      int Environment_ID { get { return _Environment_ID; } set {                                                                            SetProperty(ref  _Environment_ID, value); } }                        int _Environment_ID;
            [DbMappedField("LogDateTime")]               public DateTime LogDateTime    { get { return    _LogDateTime; } set {  ChkRange(value, MinValueOf.LogDateTime, MaxValueOf.LogDateTime);          SetProperty(ref     _LogDateTime, value); } }                   DateTime _LogDateTime;
            [DbMappedField("HostName"), DbMaxLength(50)] public   string HostName       { get { return       _HostName; } set {                                       ChkFieldLen(ref value, 50);          SetProperty(ref        _HostName, value); } }                     string _HostName;
            [DbMappedField("UserName"), DbMaxLength(50)] public   string UserName       { get { return       _UserName; } set {                                       ChkFieldLen(ref value, 50);          SetProperty(ref        _UserName, value); } }                     string _UserName;
            [DbMappedField("Comments")]                  public   string Comments       { get { return       _Comments; } set {                                                                            SetProperty(ref        _Comments, value); } }                     string _Comments;
    
            [Browsable(false)] public IEnumerable<IdTuple<int>> ViaEnvironments_Environment_IDIds { get { return Environment.GetIdsWhere("Environment_ID = " + Environment_ID); } }
            public List<Environment> ViaEnvironments_Environment_ID { get { return (List<Environment>)GeneratedDAO.Instance.GetAllByDbMapper<Environment, IdTuple<int>>(ViaEnvironments_Environment_IDIds); } }
    
    
    
            public Log()
            {
            }
            public Log(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _HostName = string.Empty;
                _UserName = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<Log> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Log();
                    item._Log_ID = (int)reader.GetValue(0);
                    item._Environment_ID = (int)reader.GetValue(1);
                    item._LogDateTime = (DateTime)reader.GetValue(2);
                    item._HostName = (string)reader.GetValue(3);
                    item._UserName = (string)reader.GetValue(4);
                    item._Comments = reader.IsDBNull(5) ? null : (string)reader.GetValue(5);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (6): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 6)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Log Clone() { return (Log)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int>> CreateNewInstance() { return new Log(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int>> source)
            {
                var from = (Log)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Environment_ID = from._Environment_ID;
                _LogDateTime = from._LogDateTime;
                _HostName = from._HostName;
                _UserName = from._UserName;
                _Comments = from._Comments;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime LogDateTime = new DateTime(552877920000000000);
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime LogDateTime = new DateTime(3155378975999970000);
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.EnvironmentTypes")]
        public sealed partial class EnvironmentType : GeneratedEntity<IdTuple<int>> 
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(EnvironmentType_id);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    EnvironmentType_id = (int)value.Value.Keys[0];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int environmenttype_id_)
            {
                return new IdTuple<int>(environmenttype_id_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT EnvironmentType_id FROM dbo.EnvironmentTypes" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["EnvironmentType_id"])));
            }
    
    
            public static List<EnvironmentType> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<EnvironmentType, IdTuple<int>>(ids);
                return entities;
            }
    
    
            [DbMappedField("EnvironmentType_id", IsPK = true)]  public    int EnvironmentType_id { get { return _EnvironmentType_id; } set {                              SetTechnicalProperty(ref  _EnvironmentType_id, value); RaiseIdChanged(); } }    int _EnvironmentType_id;
            [DbMappedField("EnvironmentType"), DbMaxLength(20)] public string Name               { get { return               _Name; } set {  ChkFieldLen(ref value, 20);          SetProperty(ref                _Name, value); } }                   string _Name;
    
    
            [Browsable(false)] public IEnumerable<IdTuple<int>> RefByHistories_EnvironmentType_idIds { get { return History.GetIdsWhere("EnvironmentType_id = " + EnvironmentType_id); } }
            public List<History> RefByHistories_EnvironmentType_id { get { return (List<History>)GeneratedDAO.Instance.GetAllByDbMapper<History, IdTuple<int>>(RefByHistories_EnvironmentType_idIds); } }
    
    
            public EnvironmentType()
            {
            }
            public EnvironmentType(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<EnvironmentType> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new EnvironmentType();
                    item._EnvironmentType_id = (int)reader.GetValue(0);
                    item._Name = (string)reader.GetValue(1);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (2): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 2)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public EnvironmentType Clone() { return (EnvironmentType)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int>> CreateNewInstance() { return new EnvironmentType(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int>> source)
            {
                var from = (EnvironmentType)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Name = from._Name;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.Environments")]
        public sealed partial class Environment : GeneratedEntity<IdTuple<int>> 
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(Environment_ID);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Environment_ID = (int)value.Value.Keys[0];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int environment_id_)
            {
                return new IdTuple<int>(environment_id_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Environment_ID FROM dbo.Environments" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["Environment_ID"])));
            }
    
    
            public static List<Environment> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Environment, IdTuple<int>>(ids);
                return entities;
            }
    
    
            [DbMappedField("Environment_ID", IsPK = true)]        public       int Environment_ID     { get { return     _Environment_ID; } set {                                                                                 SetTechnicalProperty(ref      _Environment_ID, value); RaiseIdChanged(); } }       int _Environment_ID;
            [DbMappedField("Customer_ID")]                        public       int Customer_ID        { get { return        _Customer_ID; } set {                                                                                          SetProperty(ref         _Customer_ID, value); } }                         int _Customer_ID;
            [DbMappedField("Environment"), DbMaxLength(10)]       public    string Name               { get { return               _Name; } set {                                                     ChkFieldLen(ref value, 10);          SetProperty(ref                _Name, value); } }                      string _Name;
            [DbMappedField("Version"), DbMaxLength(50)]           public    string Version            { get { return            _Version; } set {                                                     ChkFieldLen(ref value, 50);          SetProperty(ref             _Version, value); } }                      string _Version;
            [DbMappedField("SubVersion"), DbMaxLength(50)]        public    string SubVersion         { get { return         _SubVersion; } set {                                                     ChkFieldLen(ref value, 50);          SetProperty(ref          _SubVersion, value); } }                      string _SubVersion;
            [DbMappedField("ServerName"), DbMaxLength(50)]        public    string ServerName         { get { return         _ServerName; } set {                                                     ChkFieldLen(ref value, 50);          SetProperty(ref          _ServerName, value); } }                      string _ServerName;
            [DbMappedField("DataBaseName"), DbMaxLength(30)]      public    string DataBaseName       { get { return       _DataBaseName; } set {                                                     ChkFieldLen(ref value, 30);          SetProperty(ref        _DataBaseName, value); } }                      string _DataBaseName;
            [DbMappedField("AlternativeExe"), DbMaxLength(9)]     public    string AlternativeExe     { get { return     _AlternativeExe; } set {                                                      ChkFieldLen(ref value, 9);          SetProperty(ref      _AlternativeExe, value); } }                      string _AlternativeExe;
            [DbMappedField("TSTSearch")]                          public      bool TSTSearch          { get { return          _TSTSearch; } set {                                                                                          SetProperty(ref           _TSTSearch, value); } }                        bool _TSTSearch;
            [DbMappedField("LastSearchComplete")]                 public DateTime? LastSearchComplete { get { return _LastSearchComplete; } set {  ChkRange(value, MinValueOf.LastSearchComplete, MaxValueOf.LastSearchComplete);          SetProperty(ref  _LastSearchComplete, value); } }                   DateTime? _LastSearchComplete;
            [DbMappedField("LastSearchError"), DbMaxLength(8000)] public    string LastSearchError    { get { return    _LastSearchError; } set {                                                   ChkFieldLen(ref value, 8000);          SetProperty(ref     _LastSearchError, value); } }                      string _LastSearchError;
            [DbMappedField("ScriptStats_Bytes")]                  public      int? ScriptStats_Bytes  { get { return  _ScriptStats_Bytes; } set {                                                                                          SetProperty(ref   _ScriptStats_Bytes, value); } }                        int? _ScriptStats_Bytes;
            [DbMappedField("ScriptStats_SLOC")]                   public      int? ScriptStats_SLOC   { get { return   _ScriptStats_SLOC; } set {                                                                                          SetProperty(ref    _ScriptStats_SLOC, value); } }                        int? _ScriptStats_SLOC;
            [DbMappedField("PatchLevel"), DbMaxLength(20)]        public    string PatchLevel         { get { return         _PatchLevel; } set {                                                     ChkFieldLen(ref value, 20);          SetProperty(ref          _PatchLevel, value); } }                      string _PatchLevel;
            [DbMappedField("EnvironmentType_ID")]                 public      int? EnvironmentType_ID { get { return _EnvironmentType_ID; } set {                                                                                          SetProperty(ref  _EnvironmentType_ID, value); } }                        int? _EnvironmentType_ID;
    
            [Browsable(false)] public IEnumerable<IdTuple<int>> ViaCustomers_Customer_IDIds { get { return Customer.GetIdsWhere("Customer_ID = " + Customer_ID); } }
            public List<Customer> ViaCustomers_Customer_ID { get { return (List<Customer>)GeneratedDAO.Instance.GetAllByDbMapper<Customer, IdTuple<int>>(ViaCustomers_Customer_IDIds); } }
    
            [Browsable(false)] public IEnumerable<IdTuple<int>> RefByLogs_Environment_IDIds { get { return Log.GetIdsWhere("Environment_ID = " + Environment_ID); } }
            public List<Log> RefByLogs_Environment_ID { get { return (List<Log>)GeneratedDAO.Instance.GetAllByDbMapper<Log, IdTuple<int>>(RefByLogs_Environment_IDIds); } }
    
    
            public Environment()
            {
            }
            public Environment(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
                _AlternativeExe = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<Environment> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Environment();
                    item._Environment_ID = (int)reader.GetValue(0);
                    item._Customer_ID = (int)reader.GetValue(1);
                    item._Name = (string)reader.GetValue(2);
                    item._Version = reader.IsDBNull(3) ? null : (string)reader.GetValue(3);
                    item._SubVersion = reader.IsDBNull(4) ? null : (string)reader.GetValue(4);
                    item._ServerName = reader.IsDBNull(5) ? null : (string)reader.GetValue(5);
                    item._DataBaseName = reader.IsDBNull(6) ? null : (string)reader.GetValue(6);
                    item._AlternativeExe = (string)reader.GetValue(7);
                    item._TSTSearch = (bool)reader.GetValue(8);
                    item._LastSearchComplete = reader.IsDBNull(9) ? null : (DateTime?)reader.GetValue(9);
                    item._LastSearchError = reader.IsDBNull(10) ? null : (string)reader.GetValue(10);
                    item._ScriptStats_Bytes = reader.IsDBNull(11) ? null : (int?)reader.GetValue(11);
                    item._ScriptStats_SLOC = reader.IsDBNull(12) ? null : (int?)reader.GetValue(12);
                    item._PatchLevel = reader.IsDBNull(13) ? null : (string)reader.GetValue(13);
                    item._EnvironmentType_ID = reader.IsDBNull(14) ? null : (int?)reader.GetValue(14);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (15): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 15)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Environment Clone() { return (Environment)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int>> CreateNewInstance() { return new Environment(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int>> source)
            {
                var from = (Environment)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Customer_ID = from._Customer_ID;
                _Name = from._Name;
                _Version = from._Version;
                _SubVersion = from._SubVersion;
                _ServerName = from._ServerName;
                _DataBaseName = from._DataBaseName;
                _AlternativeExe = from._AlternativeExe;
                _TSTSearch = from._TSTSearch;
                _LastSearchComplete = from._LastSearchComplete;
                _LastSearchError = from._LastSearchError;
                _ScriptStats_Bytes = from._ScriptStats_Bytes;
                _ScriptStats_SLOC = from._ScriptStats_SLOC;
                _PatchLevel = from._PatchLevel;
                _EnvironmentType_ID = from._EnvironmentType_ID;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime LastSearchComplete = new DateTime(552877920000000000);
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime LastSearchComplete = new DateTime(3155378975999970000);
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.UserEnvironments")]
        public sealed partial class UserEnvironment : GeneratedEntity<IdTuple<string, string, string>> 
        {
            protected override IdTuple<string, string, string>? ClosedId
            {
                get
                {
                    return new IdTuple<string, string, string>(Name, SysUser, Version);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Name = (string)value.Value.Keys[0];
                    SysUser = (string)value.Value.Keys[1];
                    Version = (string)value.Value.Keys[2];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<string, string, string>);
    
            public static IdTuple<string, string, string> MakeClosedId(string name_, string sysuser_, string version_)
            {
                return new IdTuple<string, string, string>(name_, sysuser_, version_);
            }
    
    
    
            public static IEnumerable<IdTuple<string, string, string>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Name, SysUser, Version FROM dbo.UserEnvironments" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                                          row => new IdTuple<string, string, string>(GetValueAs<string>(row["Name"]), GetValueAs<string>(row["SysUser"]), GetValueAs<string>(row["Version"])));
            }
    
    
            public static List<UserEnvironment> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<UserEnvironment, IdTuple<string, string, string>>(ids);
                return entities;
            }
    
    
            [DbMappedField("SysUser", IsPK = true), DbMaxLength(50)] public string SysUser      { get { return      _SysUser; } set {  ChkFieldLen(ref value, 50); SetTechnicalProperty(ref       _SysUser, value); RaiseIdChanged(); } } string _SysUser;
            [DbMappedField("Name", IsPK = true), DbMaxLength(50)]    public string Name         { get { return         _Name; } set {  ChkFieldLen(ref value, 50); SetTechnicalProperty(ref          _Name, value); RaiseIdChanged(); } } string _Name;
            [DbMappedField("Version", IsPK = true), DbMaxLength(10)] public string Version      { get { return      _Version; } set {  ChkFieldLen(ref value, 10); SetTechnicalProperty(ref       _Version, value); RaiseIdChanged(); } } string _Version;
            [DbMappedField("ServerName"), DbMaxLength(50)]           public string ServerName   { get { return   _ServerName; } set {  ChkFieldLen(ref value, 50);          SetProperty(ref    _ServerName, value); } }                   string _ServerName;
            [DbMappedField("DatabaseName"), DbMaxLength(50)]         public string DatabaseName { get { return _DatabaseName; } set {  ChkFieldLen(ref value, 50);          SetProperty(ref  _DatabaseName, value); } }                   string _DatabaseName;
            [DbMappedField("SQLLogin"), DbMaxLength(30)]             public string SQLLogin     { get { return     _SQLLogin; } set {  ChkFieldLen(ref value, 30);          SetProperty(ref      _SQLLogin, value); } }                   string _SQLLogin;
            [DbMappedField("SQLPassword"), DbMaxLength(30)]          public string SQLPassword  { get { return  _SQLPassword; } set {  ChkFieldLen(ref value, 30);          SetProperty(ref   _SQLPassword, value); } }                   string _SQLPassword;
    
    
    
    
            public UserEnvironment()
            {
            }
            public UserEnvironment(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _SysUser = string.Empty;
                _Name = string.Empty;
                _Version = string.Empty;
                _ServerName = string.Empty;
                _DatabaseName = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<UserEnvironment> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new UserEnvironment();
                    item._SysUser = (string)reader.GetValue(0);
                    item._Name = (string)reader.GetValue(1);
                    item._Version = (string)reader.GetValue(2);
                    item._ServerName = (string)reader.GetValue(3);
                    item._DatabaseName = (string)reader.GetValue(4);
                    item._SQLLogin = reader.IsDBNull(5) ? null : (string)reader.GetValue(5);
                    item._SQLPassword = reader.IsDBNull(6) ? null : (string)reader.GetValue(6);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (7): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 7)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public UserEnvironment Clone() { return (UserEnvironment)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<string, string, string>> CreateNewInstance() { return new UserEnvironment(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<string, string, string>> source)
            {
                var from = (UserEnvironment)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _ServerName = from._ServerName;
                _DatabaseName = from._DatabaseName;
                _SQLLogin = from._SQLLogin;
                _SQLPassword = from._SQLPassword;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.History")]
        public sealed partial class History : GeneratedEntity<IdTuple<int>> 
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(History_id);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    History_id = (int)value.Value.Keys[0];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int history_id_)
            {
                return new IdTuple<int>(history_id_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT History_id FROM dbo.History" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["History_id"])));
            }
    
    
            public static List<History> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<History, IdTuple<int>>(ids);
                return entities;
            }
    
    
            [DbMappedField("History_id", IsPK = true)]          public       int History_id         { get { return         _History_id; } set {                                                                      SetTechnicalProperty(ref          _History_id, value); RaiseIdChanged(); } }       int _History_id;
            [DbMappedField("Customer_id")]                      public       int Customer_id        { get { return        _Customer_id; } set {                                                                               SetProperty(ref         _Customer_id, value); } }                         int _Customer_id;
            [DbMappedField("EnvironmentType_id")]               public       int EnvironmentType_id { get { return _EnvironmentType_id; } set {                                                                               SetProperty(ref  _EnvironmentType_id, value); } }                         int _EnvironmentType_id;
            [DbMappedField("Version"), DbMaxLength(50)]         public    string Version            { get { return            _Version; } set {                                          ChkFieldLen(ref value, 50);          SetProperty(ref             _Version, value); } }                      string _Version;
            [DbMappedField("Branch"), DbMaxLength(15)]          public    string Branch             { get { return             _Branch; } set {                                          ChkFieldLen(ref value, 15);          SetProperty(ref              _Branch, value); } }                      string _Branch;
            [DbMappedField("ModifiedBy"), DbMaxLength(50)]      public    string ModifiedBy         { get { return         _ModifiedBy; } set {                                          ChkFieldLen(ref value, 50);          SetProperty(ref          _ModifiedBy, value); } }                      string _ModifiedBy;
            [DbMappedField("EffectiveDate")]                    public DateTime? EffectiveDate      { get { return      _EffectiveDate; } set { ChkRange(value, MinValueOf.EffectiveDate, MaxValueOf.EffectiveDate);          SetProperty(ref       _EffectiveDate, value); } }                   DateTime? _EffectiveDate;
            [DbMappedField("EntryDate")]                        public  DateTime EntryDate          { get { return          _EntryDate; } set {         ChkRange(value, MinValueOf.EntryDate, MaxValueOf.EntryDate);          SetProperty(ref           _EntryDate, value); } }                    DateTime _EntryDate;
            [DbMappedField("Comment"), DbMaxLength(2147483647)] public    string Comment            { get { return            _Comment; } set {                                  ChkFieldLen(ref value, 2147483647);          SetProperty(ref             _Comment, value); } }                      string _Comment;
    
            [Browsable(false)] public IEnumerable<IdTuple<int>> ViaCustomers_Customer_idIds { get { return Customer.GetIdsWhere("Customer_ID = " + Customer_id); } }
            public List<Customer> ViaCustomers_Customer_id { get { return (List<Customer>)GeneratedDAO.Instance.GetAllByDbMapper<Customer, IdTuple<int>>(ViaCustomers_Customer_idIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<int>> ViaEnvironmentTypes_EnvironmentType_idIds { get { return EnvironmentType.GetIdsWhere("EnvironmentType_id = " + EnvironmentType_id); } }
            public List<EnvironmentType> ViaEnvironmentTypes_EnvironmentType_id { get { return (List<EnvironmentType>)GeneratedDAO.Instance.GetAllByDbMapper<EnvironmentType, IdTuple<int>>(ViaEnvironmentTypes_EnvironmentType_idIds); } }
    
    
    
            public History()
            {
            }
            public History(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Version = string.Empty;
                _ModifiedBy = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<History> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new History();
                    item._History_id = (int)reader.GetValue(0);
                    item._Customer_id = (int)reader.GetValue(1);
                    item._EnvironmentType_id = (int)reader.GetValue(2);
                    item._Version = (string)reader.GetValue(3);
                    item._Branch = reader.IsDBNull(4) ? null : (string)reader.GetValue(4);
                    item._ModifiedBy = (string)reader.GetValue(5);
                    item._EffectiveDate = reader.IsDBNull(6) ? null : (DateTime?)reader.GetValue(6);
                    item._EntryDate = (DateTime)reader.GetValue(7);
                    item._Comment = reader.IsDBNull(8) ? null : (string)reader.GetValue(8);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (9): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 9)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public History Clone() { return (History)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int>> CreateNewInstance() { return new History(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int>> source)
            {
                var from = (History)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Customer_id = from._Customer_id;
                _EnvironmentType_id = from._EnvironmentType_id;
                _Version = from._Version;
                _Branch = from._Branch;
                _ModifiedBy = from._ModifiedBy;
                _EffectiveDate = from._EffectiveDate;
                _EntryDate = from._EntryDate;
                _Comment = from._Comment;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
                public static readonly DateTime EffectiveDate = new DateTime(552877920000000000);
                public static readonly DateTime EntryDate = new DateTime(552877920000000000);
            }
    
            static class MaxValueOf
            {
                public static readonly DateTime EffectiveDate = new DateTime(3155378975999970000);
                public static readonly DateTime EntryDate = new DateTime(3155378975999970000);
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.Customers")]
        public sealed partial class Customer : GeneratedEntity<IdTuple<int>> 
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(Customer_ID);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Customer_ID = (int)value.Value.Keys[0];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int customer_id_)
            {
                return new IdTuple<int>(customer_id_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Customer_ID FROM dbo.Customers" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["Customer_ID"])));
            }
    
    
            public static List<Customer> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Customer, IdTuple<int>>(ids);
                return entities;
            }
    
    
            [DbMappedField("Customer_ID", IsPK = true)]     public    int Customer_ID        { get { return        _Customer_ID; } set {                              SetTechnicalProperty(ref         _Customer_ID, value); RaiseIdChanged(); } }    int _Customer_ID;
            [DbMappedField("Name"), DbMaxLength(20)]        public string Name               { get { return               _Name; } set {  ChkFieldLen(ref value, 20);          SetProperty(ref                _Name, value); } }                   string _Name;
            [DbMappedField("LDAPName"), DbMaxLength(30)]    public string LDAPName           { get { return           _LDAPName; } set {  ChkFieldLen(ref value, 30);          SetProperty(ref            _LDAPName, value); } }                   string _LDAPName;
            [DbMappedField("NoDLL")]                        public   bool NoDLL              { get { return              _NoDLL; } set {                                       SetProperty(ref               _NoDLL, value); } }                     bool _NoDLL;
            [DbMappedField("HasDocWriter")]                 public   bool HasDocWriter       { get { return       _HasDocWriter; } set {                                       SetProperty(ref        _HasDocWriter, value); } }                     bool _HasDocWriter;
            [DbMappedField("HasSwift")]                     public   bool HasSwift           { get { return           _HasSwift; } set {                                       SetProperty(ref            _HasSwift, value); } }                     bool _HasSwift;
            [DbMappedField("HasReportLauncher")]            public   bool HasReportLauncher  { get { return  _HasReportLauncher; } set {                                       SetProperty(ref   _HasReportLauncher, value); } }                     bool _HasReportLauncher;
            [DbMappedField("HasWorkstationDLL")]            public   bool HasWorkstationDLL  { get { return  _HasWorkstationDLL; } set {                                       SetProperty(ref   _HasWorkstationDLL, value); } }                     bool _HasWorkstationDLL;
            [DbMappedField("HasDocWriterClient")]           public   bool HasDocWriterClient { get { return _HasDocWriterClient; } set {                                       SetProperty(ref  _HasDocWriterClient, value); } }                     bool _HasDocWriterClient;
            [DbMappedField("HasFIX")]                       public   bool HasFIX             { get { return             _HasFIX; } set {                                       SetProperty(ref              _HasFIX, value); } }                     bool _HasFIX;
            [DbMappedField("HasAIM")]                       public   bool HasAIM             { get { return             _HasAIM; } set {                                       SetProperty(ref              _HasAIM, value); } }                     bool _HasAIM;
            [DbMappedField("HAsMQ")]                        public   bool HAsMQ              { get { return              _HAsMQ; } set {                                       SetProperty(ref               _HAsMQ, value); } }                     bool _HAsMQ;
            [DbMappedField("HelpLanguage"), DbMaxLength(2)] public string HelpLanguage       { get { return       _HelpLanguage; } set {   ChkFieldLen(ref value, 2);          SetProperty(ref        _HelpLanguage, value); } }                   string _HelpLanguage;
    
    
            [Browsable(false)] public IEnumerable<IdTuple<int>> RefByEnvironments_Customer_IDIds { get { return Environment.GetIdsWhere("Customer_ID = " + Customer_ID); } }
            public List<Environment> RefByEnvironments_Customer_ID { get { return (List<Environment>)GeneratedDAO.Instance.GetAllByDbMapper<Environment, IdTuple<int>>(RefByEnvironments_Customer_IDIds); } }
            [Browsable(false)] public IEnumerable<IdTuple<int>> RefByHistories_Customer_idIds { get { return History.GetIdsWhere("Customer_id = " + Customer_ID); } }
            public List<History> RefByHistories_Customer_id { get { return (List<History>)GeneratedDAO.Instance.GetAllByDbMapper<History, IdTuple<int>>(RefByHistories_Customer_idIds); } }
    
    
            public Customer()
            {
            }
            public Customer(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<Customer> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Customer();
                    item._Customer_ID = (int)reader.GetValue(0);
                    item._Name = (string)reader.GetValue(1);
                    item._LDAPName = reader.IsDBNull(2) ? null : (string)reader.GetValue(2);
                    item._NoDLL = (bool)reader.GetValue(3);
                    item._HasDocWriter = (bool)reader.GetValue(4);
                    item._HasSwift = (bool)reader.GetValue(5);
                    item._HasReportLauncher = (bool)reader.GetValue(6);
                    item._HasWorkstationDLL = (bool)reader.GetValue(7);
                    item._HasDocWriterClient = (bool)reader.GetValue(8);
                    item._HasFIX = (bool)reader.GetValue(9);
                    item._HasAIM = (bool)reader.GetValue(10);
                    item._HAsMQ = (bool)reader.GetValue(11);
                    item._HelpLanguage = reader.IsDBNull(12) ? null : (string)reader.GetValue(12);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (13): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 13)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Customer Clone() { return (Customer)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int>> CreateNewInstance() { return new Customer(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int>> source)
            {
                var from = (Customer)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Name = from._Name;
                _LDAPName = from._LDAPName;
                _NoDLL = from._NoDLL;
                _HasDocWriter = from._HasDocWriter;
                _HasSwift = from._HasSwift;
                _HasReportLauncher = from._HasReportLauncher;
                _HasWorkstationDLL = from._HasWorkstationDLL;
                _HasDocWriterClient = from._HasDocWriterClient;
                _HasFIX = from._HasFIX;
                _HasAIM = from._HasAIM;
                _HAsMQ = from._HAsMQ;
                _HelpLanguage = from._HelpLanguage;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.Preferences")]
        public sealed partial class Preference : GeneratedEntity<IdTuple<int, string, string>> 
        {
            protected override IdTuple<int, string, string>? ClosedId
            {
                get
                {
                    return new IdTuple<int, string, string>(Customer_ID, Environment, SysUser);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Customer_ID = (int)value.Value.Keys[0];
                    Environment = (string)value.Value.Keys[1];
                    SysUser = (string)value.Value.Keys[2];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int, string, string>);
    
            public static IdTuple<int, string, string> MakeClosedId(int customer_id_, string environment_, string sysuser_)
            {
                return new IdTuple<int, string, string>(customer_id_, environment_, sysuser_);
            }
    
    
    
            public static IEnumerable<IdTuple<int, string, string>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Customer_ID, Environment, SysUser FROM dbo.Preferences" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                                       row => new IdTuple<int, string, string>(GetValueAs<int>(row["Customer_ID"]), GetValueAs<string>(row["Environment"]), GetValueAs<string>(row["SysUser"])));
            }
    
    
            public static List<Preference> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Preference, IdTuple<int, string, string>>(ids);
                return entities;
            }
    
    
            [DbMappedField("Customer_ID", IsPK = true)]                  public    int Customer_ID { get { return _Customer_ID; } set {                               SetTechnicalProperty(ref  _Customer_ID, value); RaiseIdChanged(); } }    int _Customer_ID;
            [DbMappedField("SysUser", IsPK = true), DbMaxLength(50)]     public string SysUser     { get { return     _SysUser; } set {   ChkFieldLen(ref value, 50); SetTechnicalProperty(ref      _SysUser, value); RaiseIdChanged(); } } string _SysUser;
            [DbMappedField("Environment", IsPK = true), DbMaxLength(10)] public string Environment { get { return _Environment; } set {   ChkFieldLen(ref value, 10); SetTechnicalProperty(ref  _Environment, value); RaiseIdChanged(); } } string _Environment;
            [DbMappedField("Login"), DbMaxLength(100)]                   public string Login       { get { return       _Login; } set {  ChkFieldLen(ref value, 100);          SetProperty(ref        _Login, value); } }                   string _Login;
            [DbMappedField("LatestBuild")]                               public   bool LatestBuild { get { return _LatestBuild; } set {                                        SetProperty(ref  _LatestBuild, value); } }                     bool _LatestBuild;
            [DbMappedField("DevMode")]                                   public   bool DevMode     { get { return     _DevMode; } set {                                        SetProperty(ref      _DevMode, value); } }                     bool _DevMode;
    
    
    
    
            public Preference()
            {
            }
            public Preference(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _SysUser = string.Empty;
                _Environment = string.Empty;
                _Login = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<Preference> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Preference();
                    item._Customer_ID = (int)reader.GetValue(0);
                    item._SysUser = (string)reader.GetValue(1);
                    item._Environment = (string)reader.GetValue(2);
                    item._Login = (string)reader.GetValue(3);
                    item._LatestBuild = (bool)reader.GetValue(4);
                    item._DevMode = (bool)reader.GetValue(5);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (6): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 6)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Preference Clone() { return (Preference)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int, string, string>> CreateNewInstance() { return new Preference(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int, string, string>> source)
            {
                var from = (Preference)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Login = from._Login;
                _LatestBuild = from._LatestBuild;
                _DevMode = from._DevMode;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.CompareSettings")]
        public sealed partial class CompareSetting : GeneratedEntity<IdTuple<string, bool>> 
        {
            protected override IdTuple<string, bool>? ClosedId
            {
                get
                {
                    return new IdTuple<string, bool>(Name, TwoQueries);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Name = (string)value.Value.Keys[0];
                    TwoQueries = (bool)value.Value.Keys[1];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<string, bool>);
    
            public static IdTuple<string, bool> MakeClosedId(string name_, bool twoqueries_)
            {
                return new IdTuple<string, bool>(name_, twoqueries_);
            }
    
    
    
            public static IEnumerable<IdTuple<string, bool>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT Name, TwoQueries FROM dbo.CompareSettings" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                                row => new IdTuple<string, bool>(GetValueAs<string>(row["Name"]), GetValueAs<bool>(row["TwoQueries"])));
            }
    
    
            public static List<CompareSetting> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<CompareSetting, IdTuple<string, bool>>(ids);
                return entities;
            }
    
    
            [DbMappedField("Name", IsPK = true), DbMaxLength(255)] public string Name       { get { return       _Name; } set {  ChkFieldLen(ref value, 255); SetTechnicalProperty(ref        _Name, value); RaiseIdChanged(); } } string _Name;
            [DbMappedField("TwoQueries", IsPK = true)]             public   bool TwoQueries { get { return _TwoQueries; } set {                               SetTechnicalProperty(ref  _TwoQueries, value); RaiseIdChanged(); } }   bool _TwoQueries;
            [DbMappedField("Definition")]                          public string Definition { get { return _Definition; } set {                                        SetProperty(ref  _Definition, value); } }                   string _Definition;
    
    
    
    
            public CompareSetting()
            {
            }
            public CompareSetting(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
                _Definition = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<CompareSetting> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new CompareSetting();
                    item._Name = (string)reader.GetValue(0);
                    item._TwoQueries = (bool)reader.GetValue(1);
                    item._Definition = (string)reader.GetValue(2);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (3): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 3)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public CompareSetting Clone() { return (CompareSetting)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<string, bool>> CreateNewInstance() { return new CompareSetting(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<string, bool>> source)
            {
                var from = (CompareSetting)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Definition = from._Definition;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.sysdiagrams")]
        public sealed partial class Sysdiagram : GeneratedEntity<IdTuple<int>> 
        {
            protected override IdTuple<int>? ClosedId
            {
                get
                {
                    return new IdTuple<int>(Diagram_id);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Diagram_id = (int)value.Value.Keys[0];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int>);
    
            public static IdTuple<int> MakeClosedId(int diagram_id_)
            {
                return new IdTuple<int>(diagram_id_);
            }
    
    
    
            public static IEnumerable<IdTuple<int>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT diagram_id FROM dbo.sysdiagrams" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                       row => new IdTuple<int>(GetValueAs<int>(row["diagram_id"])));
            }
    
    
            public static List<Sysdiagram> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Sysdiagram, IdTuple<int>>(ids);
                return entities;
            }
    
    
            [DbMappedField("name"), DbMaxLength(128)]  public string Name         { get { return         _Name; } set {  ChkFieldLen(ref value, 128);          SetProperty(ref          _Name, value); } }                   string _Name;
            [DbMappedField("principal_id")]            public    int Principal_id { get { return _Principal_id; } set {                                        SetProperty(ref  _Principal_id, value); } }                      int _Principal_id;
            [DbMappedField("diagram_id", IsPK = true)] public    int Diagram_id   { get { return   _Diagram_id; } set {                               SetTechnicalProperty(ref    _Diagram_id, value); RaiseIdChanged(); } }    int _Diagram_id;
            [DbMappedField("version")]                 public   int? Version      { get { return      _Version; } set {                                        SetProperty(ref       _Version, value); } }                     int? _Version;
            [DbMappedField("definition")]              public Byte[] Definition   { get { return   _Definition; } set {                                        SetProperty(ref    _Definition, value); } }                   Byte[] _Definition;
    
    
    
    
            public Sysdiagram()
            {
            }
            public Sysdiagram(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Name = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<Sysdiagram> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Sysdiagram();
                    item._Name = (string)reader.GetValue(0);
                    item._Principal_id = (int)reader.GetValue(1);
                    item._Diagram_id = (int)reader.GetValue(2);
                    item._Version = reader.IsDBNull(3) ? null : (int?)reader.GetValue(3);
                    item._Definition = reader.IsDBNull(4) ? null : (Byte[])reader.GetValue(4);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (5): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 5)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Sysdiagram Clone() { return (Sysdiagram)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int>> CreateNewInstance() { return new Sysdiagram(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int>> source)
            {
                var from = (Sysdiagram)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Name = from._Name;
                _Principal_id = from._Principal_id;
                _Version = from._Version;
                _Definition = from._Definition;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
        }
    }
    

    namespace Dbo
    {
        [DbMappedTable("dbo.dtproperties")]
        public sealed partial class Dtproperty : GeneratedEntity<IdTuple<int, string>> 
        {
            protected override IdTuple<int, string>? ClosedId
            {
                get
                {
                    return new IdTuple<int, string>(Id, Property);
                }
                set
                {
                    System.Diagnostics.Debug.Assert(value.HasValue, "La modélisation de la DAL est normalement faite pour qu'il n'y ai jamais null. La nullabilite de ClosedId est du au fait que le framework a été développé pour d'autres besoins antérieurs.");
                    Id = (int)value.Value.Keys[0];
                    Property = (string)value.Value.Keys[1];
                }
            }
            public static readonly Type IdType = typeof(IdTuple<int, string>);
    
            public static IdTuple<int, string> MakeClosedId(int id_, string property_)
            {
                return new IdTuple<int, string>(id_, property_);
            }
    
    
    
            public static IEnumerable<IdTuple<int, string>> GetIdsWhere(string whereCondition)
            {
                return GetRowsAndConvert("SELECT id, property FROM dbo.dtproperties" + (string.IsNullOrWhiteSpace(whereCondition) ? "" : " WHERE " + whereCondition),
                                                               row => new IdTuple<int, string>(GetValueAs<int>(row["id"]), GetValueAs<string>(row["property"])));
            }
    
    
            public static List<Dtproperty> GetEntitiesWhere(string sqlFilter)
            {
                var ids = GetIdsWhere(sqlFilter);
                var entities = GeneratedDAO.Instance.GetAllByDbMapper<Dtproperty, IdTuple<int, string>>(ids);
                return entities;
            }
    
    
            [DbMappedField("id", IsPK = true)]                        public    int Id       { get { return       _Id; } set {                                      SetTechnicalProperty(ref        _Id, value); RaiseIdChanged(); } }    int _Id;
            [DbMappedField("objectid")]                               public   int? Objectid { get { return _Objectid; } set {                                               SetProperty(ref  _Objectid, value); } }                     int? _Objectid;
            [DbMappedField("property", IsPK = true), DbMaxLength(64)] public string Property { get { return _Property; } set {          ChkFieldLen(ref value, 64); SetTechnicalProperty(ref  _Property, value); RaiseIdChanged(); } } string _Property;
            [DbMappedField("value"), DbMaxLength(255)]                public string Value    { get { return    _Value; } set {         ChkFieldLen(ref value, 255);          SetProperty(ref     _Value, value); } }                   string _Value;
            [DbMappedField("uvalue"), DbMaxLength(255)]               public string Uvalue   { get { return   _Uvalue; } set {         ChkFieldLen(ref value, 255);          SetProperty(ref    _Uvalue, value); } }                   string _Uvalue;
            [DbMappedField("lvalue"), DbMaxLength(2147483647)]        public Byte[] Lvalue   { get { return   _Lvalue; } set {  ChkFieldLen(ref value, 2147483647);          SetProperty(ref    _Lvalue, value); } }                   Byte[] _Lvalue;
            [DbMappedField("version")]                                public    int Version  { get { return  _Version; } set {                                               SetProperty(ref   _Version, value); } }                      int _Version;
    
    
    
    
            public Dtproperty()
            {
            }
            public Dtproperty(bool initializeModelValues)
            {
                if (!initializeModelValues)
                    return;
                _Property = string.Empty;
    
                // ReSharper disable ConditionIsAlwaysTrueOrFalse;
                var withSpecificInitialization = (object)this as ISpecificInitialization;
                if (withSpecificInitialization != null) // Dépend si l'utilisateur implemente l'interface pour la classe courante dans un autre fichier (via le mot clef partial)
                   withSpecificInitialization.InitializeModelValuesUsingBusinessKnowledge();
                // ReSharper restore ConditionIsAlwaysTrueOrFalse;
            }
    
    
            public static void FillFromReader(System.Data.SqlClient.SqlDataReader reader, List<Dtproperty> items)
            {
                #if DEBUG
                int before = items.Count;
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                #endif
                while (reader.Read())
                {
                    var item = new Dtproperty();
                    item._Id = (int)reader.GetValue(0);
                    item._Objectid = reader.IsDBNull(1) ? null : (int?)reader.GetValue(1);
                    item._Property = (string)reader.GetValue(2);
                    item._Value = reader.IsDBNull(3) ? null : (string)reader.GetValue(3);
                    item._Uvalue = reader.IsDBNull(4) ? null : (string)reader.GetValue(4);
                    item._Lvalue = reader.IsDBNull(5) ? null : (Byte[])reader.GetValue(5);
                    item._Version = (int)reader.GetValue(6);
                    items.Add(item);
                }
                #if DEBUG
                if (items.Count - before > 0)
                {
                    System.Diagnostics.Debug.WriteLine("Chargement des données : " + sw.Elapsed.ToHumanReadableShortNotation());
                    System.Diagnostics.Debug.WriteLine("  soit, par propriété (7): " + new TimeSpan(sw.Elapsed.Ticks / ((items.Count - before) * 7)).ToString());
                }
                #endif
            }
    
    
            #region Cloneable
    
            public Dtproperty Clone() { return (Dtproperty)(this as ICloneable).Clone(); }
            protected override GeneratedEntityBase<IdTuple<int, string>> CreateNewInstance() { return new Dtproperty(); }
            public override void CopyAllFieldsFrom(GeneratedEntityBase<IdTuple<int, string>> source)
            {
                var from = (Dtproperty)source;
                // Note : Les champs appartenant à la PK ne sont pas copiés;
                _Objectid = from._Objectid;
                _Value = from._Value;
                _Uvalue = from._Uvalue;
                _Lvalue = from._Lvalue;
                _Version = from._Version;
            }
    
            #endregion
    
    
            #region Value range
    
            static class MinValueOf
            {
            }
    
            static class MaxValueOf
            {
            }
    
            #endregion
        }
    }
    

}
namespace DatabaseExplorer.CustomersDAL
{
    public abstract partial class GeneratedEntity<TId> : GeneratedEntityBase<TId>                                      
        where TId : struct, IEquatable<TId>                                                                                         
    {                                                                                                                               
                                                                                                                                    
        protected GeneratedEntity()                                                                            
        {                                                                                                                           
        }                                                                                                                           
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    
        protected static IEnumerable<T> GetRowsAndConvert<T>(string sqlQueryToGetRows, Func<System.Data.DataRow, T> convert)        
        {                                                                                                                           
            var dt = GeneratedDAO.Instance.GetDataTable(sqlQueryToGetRows);                                        
            return dt.AsEnumerable().Select(convert);                                                                               
        }                                                                                                                           
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    
                                                                                                                                
    }                                                                                                                               
    public partial class GeneratedDAO : GeneratedDAOBase
    {
        public static GeneratedDAO Instance  { get; private set; }

        protected GeneratedDAO(string connectionString)
            : base(connectionString)
        {
            if (Instance != null)
                throw new TechnicalException(GetType().Name + " is supposed to be a singleton! But some code try to instanciate a new one.", null);
            Instance = this;
        }


        public override List<Type> AllTypes { get {
            if (_allTypes != null)
                return _allTypes;
            _allTypes = new List<Type>();
            _allTypes.Add(typeof(Dbo.Setting));
            _allTypes.Add(typeof(Dbo.Log));
            _allTypes.Add(typeof(Dbo.EnvironmentType));
            _allTypes.Add(typeof(Dbo.Environment));
            _allTypes.Add(typeof(Dbo.UserEnvironment));
            _allTypes.Add(typeof(Dbo.History));
            _allTypes.Add(typeof(Dbo.Customer));
            _allTypes.Add(typeof(Dbo.Preference));
            _allTypes.Add(typeof(Dbo.CompareSetting));
            _allTypes.Add(typeof(Dbo.Sysdiagram));
            _allTypes.Add(typeof(Dbo.Dtproperty));
            return _allTypes;
        } }
        List<Type> _allTypes;
    }
}
