﻿using System;
using System.Collections.Generic;
using System.Linq;

using DatabaseExplorer.CustomersDAL.Dbo;



namespace DatabaseExplorer.CustomersDAL
{
    public class CustomerAllMetadata
    {
        public List<Setting>          Settings         { get; set; }
        public List<Log>              Logs             { get; set; }
        public List<Dbo.Environment>  Environments     { get; set; }
        public List<EnvironmentType>  EnvironmentTypes { get; set; }
        public List<History>          History          { get; set; }
        public List<Customer>         Customers        { get; set; }
        public List<Preference>       Preferences      { get; set; }
        public List<CompareSetting>   CompareSettings  { get; set; }

        static CustomerAllMetadata()
        {
            var appSettings = new Properties.Settings();
            DAO.CreateSingleton(appSettings.CustomersConnection);
        }

        public void LoadAll()
        {
            Settings = Setting.GetEntitiesWhere(null);
            Logs = Log.GetEntitiesWhere(null);
            Environments = Dbo.Environment.GetEntitiesWhere(null);
            EnvironmentTypes = EnvironmentType.GetEntitiesWhere(null);
            History = Dbo.History.GetEntitiesWhere(null);
            Customers = Customer.GetEntitiesWhere(null);
            Preferences = Preference.GetEntitiesWhere(null);
            CompareSettings = CompareSetting.GetEntitiesWhere(null);
        }


        public List<SearchEnvironQry> SearchEnvironQry { get { return SearchableEnv.Cast<SearchEnvironQry>().ToList(); } }
        public List<SearchableEnv> SearchableEnv
        {
            get
            {
                return Environments.Where(e => e.TSTSearch)
                                   .Join(Customers, e => e.Customer_ID, c => c.Customer_ID,
                                        (e, c) => new SearchableEnv(e, c))
                                   .ToList();
            }
        }
        public List<SearchableEnv> ListEnvStatus { get { return SearchableEnv; } }
    }

    public class SearchEnvironQry
    {
        protected Dbo.Environment E;
        protected     Customer    C;

        public SearchEnvironQry(Dbo.Environment e, Customer c)
        {
            E = e;
            C = c;
        }

        public string Name          { get { return C.Name; } }
        public string Environment   { get { return E.Name; } }
        public string Version       { get { return E.Version; } }
        public string ServerName    { get { return E.ServerName; } }
        public string DataBaseName  { get { return E.DataBaseName; } }
    }
    public class SearchableEnv : SearchEnvironQry
    {
        public SearchableEnv(Dbo.Environment e, Customer c)
            : base(e, c)
        {
        }
        
        public int       Environment_ID     { get { return E.Environment_ID; } }
        public int       Customer_ID        { get { return E.Customer_ID; } }
        public string    SubVersion         { get { return E.SubVersion; } }
        public string    AlternativeExe     { get { return E.AlternativeExe; } }
        public bool      TSTSearch          { get { return E.TSTSearch; } }
        public DateTime? LastSearchComplete { get { return E.LastSearchComplete; } } /*alias */ public DateTime? LastUpdate { get { return E.LastSearchComplete; } }
        public string    LastSearchError    { get { return E.LastSearchError; } }    /*alias */ public string    Status     { get { return E.LastSearchError; } }
        public int?      ScriptStats_Bytes  { get { return E.ScriptStats_Bytes; } }
        public int?      ScriptStats_SLOC   { get { return E.ScriptStats_SLOC; } }   /*alias */ public int?      LinesCount { get { return E.ScriptStats_SLOC; } }
        public string    PatchLevel         { get { return E.PatchLevel; } }
        public int?      EnvironmentType_ID { get { return E.EnvironmentType_ID; } }
        public string    Customer           { get { return C.Name; } }
        public string    FullName           { get { return C.Name + " - " + E.Name; } }
    }
}
