﻿using System;

using TechnicalTools.Model;


namespace DatabaseExplorer.CustomersDAL
{
    public sealed class DAO : GeneratedDAO
    {                                                                                              
        public static new DAO Instance { get { return (DAO) GeneratedDAO.Instance; } }             
                                                                                                                   
        public static void CreateSingleton(string connectionString)                                
        {                                                                                          
            var instance = new DAO(connectionString);                                              
            if (Instance != instance)                                                              
                throw new TechnicalException("Unexpected behavior !", null);                     
        }                                                                                          
                                                                                                                   
        private DAO(string connectionString)                                                       
            : base(connectionString)                                                               
        {                                                                                          
        }                                                                                          
    }                                                                                              
                
}
