using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraBars.Localization;

using TechnicalTools;
using TechnicalTools.Diagnostics;
using TechnicalTools.Model;
using TechnicalTools.UI;
using TechnicalTools.UI.DX;
using TechnicalTools.UI.DX.BaseClasses;

using ApplicationBase.DAL;
using ApplicationBase.DAL.Users;
using ApplicationBase.Business.FileImporting;
using ApplicationBase.UI;

using DatabaseExplorer.Common;
using DatabaseExplorer.Business;
using DatabaseExplorer.Business.Model.Local;
using DatabaseExplorer.UI.Forms;
using DatabaseExplorer.UI.Views;

using Config = DatabaseExplorer.Business.Config;


namespace DatabaseExplorer.UI
{
    public partial class MainForm : DevExpress.XtraBars.Ribbon.RibbonForm, IHasDockUserControl
    {
        readonly DefaultBehavior _defaultBehavior;

        public MainForm()
        {
            _defaultBehavior = new DefaultBehavior(this, InitializeComponent/*, new DESplashScreen(BootstrapConfig.Instance.ApplicationIcon)*/);
            if (DesignTimeHelper.IsInDesignMode)
                return;

            // Desactivé car pas génial (voir solution proposée par Devexpress) et ca embête les développeurs en mode debug dans VS
            //Task.Factory.StartNew(() =>
            //{
            //    Action<Func<XtraUserControlBase>> prepareView = createCtl =>
            //    {
            //        Task.Factory.StartNew(() =>
            //        {
            //            try
            //            {
            //                var container = new FormComposite();
            //                container.DockAsDefault(createCtl());
            //                container.Dispose();
            //            }
            //            catch // Should not throw but throw sometimes because of his : https://www.devexpress.com/Support/Center/Question/Details/T528848/how-to-avoid-delays-in-an-application-due-to-the-jit-compilation
            //            {
            //            }
            //        }, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);
            //    };
            //    prepareView(() => new ImportView(Config.Instance.Import));
            //    prepareView(() => new MoneyInView(MoneyIn.Instance));
            //    prepareView(() => new MoneyOutView(MoneyOut.Instance));
            //    prepareView(() => new IssueDirectDebitsSetView(IssueDirectDebitsSetView.EditedObject.Instance));
            //    GC.KeepAlive(DB.BankAccounts);
            //    GC.KeepAlive(DB.BlockedIbans);
            //    GC.KeepAlive(DB.ImpotsIbans);

            //    //var allCtlsWithHandle = views.Concat(views.SelectMany(view => view.AllSubControls()))
            //    //                   .Where(ctl => ctl.IsHandleCreated)
            //    //                   .ToList();
            //}, CancellationToken.None, TaskCreationOptions.LongRunning, TaskScheduler.Default);

            // from https://www.codeproject.com/Articles/31316/Pre-compile-pre-JIT-your-assembly-on-the-fly-or-tr
            //var jitter = new Thread(() =>
            //{
            //    foreach (var type in AppDomain.CurrentDomain.GetAssemblies().SelectMany(ass => ass.GetTypes()))
            //    {
            //        foreach (var method in type.GetMethods(BindingFlags.DeclaredOnly |
            //                            BindingFlags.NonPublic |
            //                            BindingFlags.Public | BindingFlags.Instance |
            //                            BindingFlags.Static))
            //        {
            //            System.Runtime.CompilerServices.RuntimeHelpers.PrepareMethod(method.MethodHandle);
            //        }
            //    }
            //});
            //jitter.Priority = ThreadPriority.Lowest;
            //jitter.Start();
        }

        void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // A etudier : pourquoi quand on ferme l'application (apres avoir afficher le graph des connexion) le programme est toujours en cours d'execution (pompe a message qui ne s'arrete pas)
            Application.Exit();
        }

        class DefaultBehavior : DefaultMainFormBehavior<MainForm>
        {
            public DefaultBehavior(MainForm owner, Action initializeComponent, Func<ISplashScreen> buildSplashScreen = null)
                : base(owner, initializeComponent, buildSplashScreen)
            {
            }

            protected override UserPreferenceErgonomyApplier CreateUserPreferenceErgonomyApplier()
            {
                return new UserPreferenceErgonomyApplier(BusinessSingletons.Instance.GetConfigOfUser(), Owner);
            }

            protected override void OnUserAuthenticated()
            {
                base.OnUserAuthenticated();
                Owner.OnUserAuthenticated();
            }

            public override void RefreshEnabilitiesAndVisibilities()
            {
                Owner.RefreshEnabilitiesAndVisibilities();
            }
            public override void BlinkBecauseNewVersion(bool toggleColor)
            {
                Owner.BlinkBecauseNewVersion(toggleColor);
            }
            public override bool DeployVersion(ApplicationBase.Deployment.Deployer deployer, Control owner, ApplicationBase.Deployment.Data.EnvironmentConfig env, Version versionToInstall)
            {
                // If we are here, the user is already connected to LMT...
                // so no need to re authenticate at this point because the user connection can already download LMT files
                deployer.Config.DataAccessor.IsAuthenticated = true;
                return base.DeployVersion(deployer, Owner, env, versionToInstall);
            }

            protected override void RefreshUserInfo(Login login)
            {
                Owner.RefreshUserInfo(login);
            }
        }

        // Est executé quand l'utilisateur est authentifié
        // Ce code contient également InitializeComponent (plutot que le constructeur) 
        // car le code des composants graphiques crée peut dépendre de l'utilisateur qui est connecté
        // Cela nous permet de gerer la mainform comme n'importe quel autre form ou vue
        void OnUserAuthenticated()
        {
            ribbon.ForceGraphicsInitialize(); // Evite que le ribbon n'apparaisse avec un delay trop grand

            // Affiche quelques données en bas
            var allConnections = new[] { DB.Config.ConnectionMain };
            barlblVersion.Caption = " " + Application.ProductName + " Version: " + ApplicationBase.Deployment.Deployer.GetCurrentVersion();
            barlblDomain.Caption = " Domain: " + Config.Instance.Environment.Domain;
            barlblEnv.Caption = " Env: " + Config.Instance.Environment.Name;
            barlblHostIp.Caption = allConnections.NotNull().Select(con => con.DataSource).Distinct().Join(" | ");
            barlblDatabase.Caption = allConnections.NotNull().Select(con => con.InitialCatalog).Distinct().Join(" | ");
            colorBar.BackColor = GraphicalChart.GetColor(DB.Config.Environment.Domain);

            SkinApplicatorHelper.InitSkinPopupMenu(mnuSkins, true, BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser.IsAdmin); // We consider theme skins as easter eggs skins for user ;-)
        }

        public void RefreshEnabilitiesAndVisibilities()
        {
            if (DesignTimeHelper.IsInDesignMode)
                return;
            if (InvokeRequired)
            {
                BeginInvoke((Action)RefreshEnabilitiesAndVisibilities);
                return;
            }

            #region Application des droits utilisateurs

            #region Business Screens

            #endregion Business Screens

            #region Technical views

            mnuUserSettings.Enabled = !BusinessSingletons.Instance.GetAuthenticationManager().CurrentUser.IsService;
            if (!ribbonPageGroupDevelopperTools.Visible && ribbonPageGroupDevelopperTools.ItemLinks.All(item => item.Item.Visibility == BarItemVisibility.Never))
                _checkDone = true;
            else if (!_checkDone)
                throw new TechnicalException("For security measure, all sensitive menus must be hidden by default" + Environment.NewLine +
                                             "(Because if a minor network exception occurs before going here, menus would be visible to user)!", null);
            bool isAdmin = BusinessSingletons.Instance.GetAuthenticationManager().CurrentUserReal.IsAdmin;
            bool isDevelopper = DebugTools.IsForDevelopper;
            var canDeploy = BusinessSingletons.Instance.GetAuthenticationManager().GetAccessFor(Feature.CanDeployApplication).HasFlag(eFeatureAccess.CanUse);
            var noLocalDB = Config.Instance.Environment.Domain != ApplicationBase.Deployment.Data.eEnvironment.LocalNoDB;
            mnuEnvironments.Visibility = canDeploy && noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            mnuDeploy.Visibility = isDevelopper && canDeploy && noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            mnuUsersProfilesRights.Visibility = isAdmin && noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            mnuLogs.Visibility = isAdmin && noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            mnuAppDialoguing.Visibility = isAdmin && noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            mnuTest.Visibility = isDevelopper && noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            ribbonPageGroupDevelopperTools.Visible = ribbonPageGroupDevelopperTools.ItemLinks.Any(item => item.Visible);

            var rndAccess = false;
            btnRnD_Test.Visibility = rndAccess ? BarItemVisibility.Always : BarItemVisibility.Never;

            barlblDomain.Visibility = noLocalDB ? BarItemVisibility.Always: BarItemVisibility.Never;
            barlblEnv.Visibility = noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            barlblHostIp.Visibility = noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            barlblDatabase.Visibility = noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;
            barlblLogin.Visibility = noLocalDB ? BarItemVisibility.Always : BarItemVisibility.Never;

            bbtnShowTrace.Visibility = BarItemVisibility.Always;
            bbtnShowInstancesInMemory.Visibility = BarItemVisibility.Always;

            #endregion Technical views

            #endregion Application des droits utilisateurs
        }
        bool _checkDone; // For impersonation...

        protected void RefreshUserInfo(Login login)
        {
            barlblLogin.Caption = "Authenticated as " + login.Identifiant;
        }

        void ribbon_ShowCustomizationMenu(object sender, DevExpress.XtraBars.Ribbon.RibbonCustomizationMenuEventArgs e)
        {
            foreach (BarItemLink itemLink in e.CustomizationMenu.ItemLinks)
            {
                if (itemLink.Caption.Equals(BarLocalizer.Active.GetLocalizedString(BarString.RibbonToolbarMinimizeRibbon)))
                {
                    itemLink.Visible = false;
                    break;
                }
            }
        }

        DockPanel IHasDockUserControl.DockAsDefault(BaseEdit ctl, string caption) { return compositeControl.DockAsDefault(ctl, caption); }
        DockPanel IHasDockUserControl.DockAsDefault(XtraUserControlBase ctl, string caption) { return compositeControl.DockAsDefault(ctl, caption); }

        #region Status Bar

        void bbtnShowTrace_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowDbTrace();
        }

        void bbtnShowInstancesInMemory_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowInstancesInMemory();
        }

        #endregion

        #region Auto Update 

        void barblbNewRelease_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.CheckAndInstallNewVersion();
        }
        void AnyStatusLabelAvoutEnvironment_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowEnvironmentChooser();
        }

        public void BlinkBecauseNewVersion(bool toggleColor)
        {
            barblbNewRelease.Visibility = BarItemVisibility.Always;
            var curBgColor = barblbNewRelease.ItemAppearance.Normal.BackColor;
            _lastColor = _lastColor ?? curBgColor;
            var color = curBgColor == GraphicalChart.GreenOfficial ? _lastColor.Value : GraphicalChart.GreenOfficial;
            barblbNewRelease.ItemAppearance.Normal.BackColor = color;
        }
        Color? _lastColor;

        #endregion Auto Update 
        
        #region Developpers menu

        void mnuUsersProfilesRights_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowUsersProfilesRights();
        }
        void mnuEnvironments_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowEnvironmentSettings();
        }
        void mnuLogs_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowAdminLogs();
        }
        void mnuDeploy_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowDeploymentTool();
        }

        void mnuTest_ItemClick(object sender, ItemClickEventArgs e)
        {
            var testView = new TestView(new object());
            (this as IHasDockUserControl).DockAsDefault(testView);
            testView.Focus();
        }

        #endregion

        #region Main Menu

        #region Base Views

        void btnReleaseNoteHistory_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowReleaseNoteHistory();
        }

        void mnuAppDialoguing_ItemClick(object sender, ItemClickEventArgs e)
        {
            _defaultBehavior.ShowDialoguer();
        }

        void btnAnalyseAnyDataFile_ItemClick(object sender, ItemClickEventArgs e)
        {
            UIControler.Instance.ShowObject(new AnyDataFileReader());
        }

        #endregion Base Views

        private void mnuOpenDatabase_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(ProjectView.LastConnectionString))
            {
                ProjectView.LastConnectionString = @"Data Source=localhost\SQLEXPRESS;Initial Catalog=AdventureWorks;Integrated Security=True;MultipleActiveResultSets=true;";
                ProjectViewNew.LastConnectionString = ProjectView.LastConnectionString;
            }
            var new_project = new Project
            {
                Connection = new System.Data.SqlClient.SqlConnectionStringBuilder(ProjectView.LastConnectionString)
            };

            
            var frm = new FrmDBImportWizard(new_project);
            if (DialogResult.OK == frm.ShowDialog())
            {
                ProjectView.LastConnectionString = new_project.Connection.ConnectionString;
                var view = new ProjectView(new_project);
                UIControler.Instance.ShowView(view);
                var viewNew = new ProjectViewNew(new_project);
                UIControler.Instance.ShowView(viewNew);
            }
        }


        #endregion Main Menu

    }
}