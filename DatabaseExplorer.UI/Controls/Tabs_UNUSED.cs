﻿using System;

using TechnicalTools.UI.DX;


namespace DatabaseExplorer.UI.Controls
{
    // From https://www.devexpress.com/Support/Center/Question/Details/Q351037
    public partial class Tabs : XtraUserControlBase
    {
        public Tabs()
        {
            InitializeComponent();
        }
    }
}
