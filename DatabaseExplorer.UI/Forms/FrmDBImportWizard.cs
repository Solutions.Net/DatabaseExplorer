﻿using System;
using System.Windows.Forms;

using TechnicalTools.UI.DX;

using DatabaseExplorer.Business.Model.Local;
using DatabaseExplorer.Business.Extensions;


namespace DatabaseExplorer.UI.Forms
{
    public partial class FrmDBImportWizard : FormBase
    {
        readonly Project _project;

        public FrmDBImportWizard(Project project = null)
        {
            InitializeComponent();
            _project = project;
            if (_project == null)
                return;
            RefreshView();
        }

        private void lblConnectionString_MouseDown(object sender, MouseEventArgs e)
        {
            // On utilise l'event MouseDown et pas click car lorsque la fenetres est affiche au milieu de la fenetre parent,
            // et que le click l'ayant declenché est relaché, ca declenche cet evenement
            //var frm = new frmConnectionCreation();
            //frm.ShowDialog();
            string connectionString = _project.Connection.ConnectionString;
            FrmConnectionStringEditor.Show(ref connectionString, this);
            _project.Connection.ConnectionString = connectionString;

            RefreshView();
        }

        void RefreshView()
        {
            var password = _project.Connection.Password;
            _project.Connection.Password = "********";
            lblConnectionString.Text = _project.Connection.ConnectionString;
            _project.Connection.Password = password;
            btnRunImport.Enabled = _project.Connection.IsValid();
        }
        private void lblConnectionStringTitle_MouseDown(object sender, MouseEventArgs e)
        {
            lblConnectionString_MouseDown(null, null);
        }

        private void btnRunImport_Click(object sender, EventArgs e)
        {
            RunImport(this, _project, onSuccess: () =>
                {
                    if (Modal) DialogResult = DialogResult.OK;
                    else       Close();
                });
        }
        internal static void RunImport(FormBase frm, Project project, Action onSuccess)
        {
            frm.ShowBusyWhileDoing("Importing metadata from database...", pr => project.RunImport())
                .ContinueWith(t =>
                {
                    if (!string.IsNullOrWhiteSpace(project.Warnings))
                        frm.BeginInvoke((Action)(() => MessageBox.Show(project.Warnings, "Warnings while loading database schema", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)));
                    if (t.Status == System.Threading.Tasks.TaskStatus.RanToCompletion)
                        frm.BeginInvoke((Action)(() => onSuccess?.Invoke()));
                });
        }
    }
}
