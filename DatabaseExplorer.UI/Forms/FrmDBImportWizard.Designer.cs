﻿namespace DatabaseExplorer.UI.Forms
{
    partial class FrmDBImportWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRunImport = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlConnectionString = new System.Windows.Forms.Panel();
            this.lblConnectionStringTitle = new System.Windows.Forms.Label();
            this.lblConnectionString = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlConnectionString.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRunImport
            // 
            this.btnRunImport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRunImport.Enabled = false;
            this.btnRunImport.Location = new System.Drawing.Point(0, 0);
            this.btnRunImport.Name = "btnRunImport";
            this.btnRunImport.Size = new System.Drawing.Size(664, 41);
            this.btnRunImport.TabIndex = 4;
            this.btnRunImport.Text = "Run Import";
            this.btnRunImport.UseVisualStyleBackColor = true;
            this.btnRunImport.Click += new System.EventHandler(this.btnRunImport_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRunImport);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(10, 64);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(664, 41);
            this.panel1.TabIndex = 5;
            // 
            // pnlConnectionString
            // 
            this.pnlConnectionString.Controls.Add(this.lblConnectionString);
            this.pnlConnectionString.Controls.Add(this.lblConnectionStringTitle);
            this.pnlConnectionString.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlConnectionString.Location = new System.Drawing.Point(10, 10);
            this.pnlConnectionString.MinimumSize = new System.Drawing.Size(0, 24);
            this.pnlConnectionString.Name = "pnlConnectionString";
            this.pnlConnectionString.Padding = new System.Windows.Forms.Padding(2);
            this.pnlConnectionString.Size = new System.Drawing.Size(664, 54);
            this.pnlConnectionString.TabIndex = 3;
            // 
            // lblConnectionStringTitle
            // 
            this.lblConnectionStringTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblConnectionStringTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnectionStringTitle.Location = new System.Drawing.Point(2, 2);
            this.lblConnectionStringTitle.Name = "lblConnectionStringTitle";
            this.lblConnectionStringTitle.Size = new System.Drawing.Size(660, 13);
            this.lblConnectionStringTitle.TabIndex = 0;
            this.lblConnectionStringTitle.Text = "Connection String (click on it to edit) :";
            this.lblConnectionStringTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblConnectionStringTitle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblConnectionStringTitle_MouseDown);
            // 
            // lblConnectionString
            // 
            this.lblConnectionString.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblConnectionString.Location = new System.Drawing.Point(2, 15);
            this.lblConnectionString.Name = "lblConnectionString";
            this.lblConnectionString.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.lblConnectionString.Size = new System.Drawing.Size(660, 20);
            this.lblConnectionString.TabIndex = 1;
            this.lblConnectionString.Text = "<click here to edit>";
            this.lblConnectionString.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblConnectionString.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lblConnectionString_MouseDown);
            // 
            // FrmDBImportWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 116);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnlConnectionString);
            this.Name = "FrmDBImportWizard";
            this.Padding = new System.Windows.Forms.Padding(10, 10, 10, 0);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Database Import Wizard";
            this.panel1.ResumeLayout(false);
            this.pnlConnectionString.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRunImport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblConnectionStringTitle;
        private System.Windows.Forms.Label lblConnectionString;
        private System.Windows.Forms.Panel pnlConnectionString;
    }
}