﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace DatabaseExplorer.UI.Forms
{
    public class FrmConnectionStringEditor
    {
        public static DialogResult Show(ref string connectionString, IWin32Window owner)
        {
            // C'est la Dll Microsoft.Data.ConnectionUI (installé via NuGet) qui fournit les classe de ce code
            // Lien officiel (mais sans download ni source : // https://connect.microsoft.com/VisualStudio/feedback/details/291885/microsoft-data-connectionui-dll-and-microsoft-data-connectionui-dialog-dll
            // Source officieuse (si un jour on est interesse) : https://github.com/kjbartel/ConnectionDialog
            // Lien nuget (Microsoft Data Connection Dialog) : https://www.nuget.org/packages/DataConnectionDialog/

            var dcd = new Microsoft.Data.ConnectionUI.DataConnectionDialog();
            Microsoft.Data.ConnectionUI.DataSource.AddStandardDataSources(dcd);

            // Ces deux lignes permettent de selectionner le provider. Sans elles une fenetre apparait afin de choisir d'abort le provider (Sql server ou oracle etc..)
            // Puis la fenêtre de dialogue correspondant au provider.
            dcd.SelectedDataSource = Microsoft.Data.ConnectionUI.DataSource.SqlDataSource;
            dcd.SelectedDataProvider = Microsoft.Data.ConnectionUI.DataProvider.SqlDataProvider;

            var hasProvider = connectionString.Contains("Provider=SQLOLEDB.1;");
            dcd.ConnectionString = connectionString.Replace("Provider=SQLOLEDB.1;", "");
            // Pour enregistrer la configuration dans un fichier (on en a pas besoin)
            //DataConnectionConfiguration dcs = new DataConnectionConfiguration(null);
            //dcs.LoadConfiguration(dcd);

            Debug.Assert(Thread.CurrentThread.GetApartmentState() == ApartmentState.STA, "Sinon le show ci-dessous bug et ne que montre la sous fenetre des propriétés !");
            while(true)
            {
                var res = Microsoft.Data.ConnectionUI.DataConnectionDialog.Show(dcd, owner); // Il ne faut pas utiliser ShowDialog d'apres la DLL elle meme
                if (res != DialogResult.OK)
                    return res;
                try
                {
                    using (var connection = new SqlConnection(dcd.ConnectionString))
                    {
                        connection.Open();
                        var cmd = new SqlCommand("SELECT * FROM sys.Tables", connection);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                            while (reader.Read())
                                break; // C'est Ok pas besoin d'aller plus loin
                    }
                    connectionString = (hasProvider ? "Provider=SQLOLEDB.1;" : "") + dcd.ConnectionString;
                    //dcs.SaveConfiguration(dcd);
                    return res;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Bad connection string", MessageBoxButtons.OK);
                }
            }
        }
    }
}
