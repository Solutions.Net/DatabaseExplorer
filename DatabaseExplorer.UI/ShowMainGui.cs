﻿using System;
using System.Windows.Forms;

using TechnicalTools.Automation;


namespace DatabaseExplorer.UI
{
    public class ShowMainGui : ApplicationBase.UI.ShowMainGuiBase
    {
        public ShowMainGui(ApplicationBase.Business.Config cfg, DateTime atDate)
            : base(cfg, atDate)
        {
        }

        protected override Form CreateMainForm()
        {
            //if (DebugTools.IsForDevelopper && Keyboard.IsKeyDown(Key.LeftAlt))
            //    return new MainTest();
            return new MainForm();
        }
    }
}
        