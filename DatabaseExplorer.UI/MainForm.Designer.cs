﻿namespace DatabaseExplorer.UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.ribbon = new TechnicalTools.UI.DX.Controls.EnhancedRibbonControl();
            this.mnuReferenceData = new DevExpress.XtraBars.BarSubItem();
            this.mnuReferenceDataHeaderStandard = new DevExpress.XtraBars.BarHeaderItem();
            this.mnuExplore = new DevExpress.XtraBars.BarSubItem();
            this.btnAnalyseAnyDataFile = new DevExpress.XtraBars.BarButtonItem();
            this.mnuOpenDatabase = new DevExpress.XtraBars.BarButtonItem();
            this.mnuUserSettings = new DevExpress.XtraBars.BarSubItem();
            this.mnuSkins = new DevExpress.XtraBars.BarSubItem();
            this.btnReleaseNoteHistory = new DevExpress.XtraBars.BarButtonItem();
            this.mnuTest = new DevExpress.XtraBars.BarButtonItem();
            this.mnuUsersProfilesRights = new DevExpress.XtraBars.BarButtonItem();
            this.mnuLogs = new DevExpress.XtraBars.BarButtonItem();
            this.mnuDeploy = new DevExpress.XtraBars.BarButtonItem();
            this.barlblVersion = new DevExpress.XtraBars.BarStaticItem();
            this.barlblDomain = new DevExpress.XtraBars.BarStaticItem();
            this.barlblEnv = new DevExpress.XtraBars.BarStaticItem();
            this.barlblConnectedTo = new DevExpress.XtraBars.BarStaticItem();
            this.barlblHostIp = new DevExpress.XtraBars.BarStaticItem();
            this.barlblDatabase = new DevExpress.XtraBars.BarStaticItem();
            this.barblbNewRelease = new DevExpress.XtraBars.BarStaticItem();
            this.barTaskStatus = new DevExpress.XtraBars.BarStaticItem();
            this.bbtnShowInstancesInMemory = new DevExpress.XtraBars.BarStaticItem();
            this.bbtnShowTrace = new DevExpress.XtraBars.BarStaticItem();
            this.barlblLogin = new DevExpress.XtraBars.BarStaticItem();
            this.mnuEnvironments = new DevExpress.XtraBars.BarButtonItem();
            this.mnuRnD = new DevExpress.XtraBars.BarSubItem();
            this.btnRnD_Test = new DevExpress.XtraBars.BarButtonItem();
            this.mnuAppDialoguing = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPageCategoryMain = new DevExpress.XtraBars.Ribbon.RibbonPageCategory();
            this.ribbonPageMain = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupMainScreens = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupUserSettings = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupDevelopperTools = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.compositeControl = new TechnicalTools.UI.DX.XtraUserControlComposite();
            this.barManagerMain = new DevExpress.XtraBars.BarManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.colorBar = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ApplicationCaption = "Database Explorer";
            this.ribbon.ApplicationIcon = ((System.Drawing.Bitmap)(resources.GetObject("ribbon.ApplicationIcon")));
            this.ribbon.DrawGroupCaptions = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.mnuReferenceData,
            this.mnuExplore,
            this.mnuUserSettings,
            this.mnuSkins,
            this.mnuReferenceDataHeaderStandard,
            this.mnuTest,
            this.mnuUsersProfilesRights,
            this.mnuLogs,
            this.mnuDeploy,
            this.barlblVersion,
            this.barlblDomain,
            this.barlblEnv,
            this.barlblConnectedTo,
            this.barlblHostIp,
            this.barlblDatabase,
            this.barblbNewRelease,
            this.barTaskStatus,
            this.bbtnShowInstancesInMemory,
            this.bbtnShowTrace,
            this.barlblLogin,
            this.mnuEnvironments,
            this.btnAnalyseAnyDataFile,
            this.mnuRnD,
            this.mnuAppDialoguing,
            this.btnRnD_Test,
            this.btnReleaseNoteHistory,
            this.mnuOpenDatabase});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 112;
            this.ribbon.Name = "ribbon";
            this.ribbon.PageCategories.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageCategory[] {
            this.ribbonPageCategoryMain});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.MacOffice;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowPageHeadersMode = DevExpress.XtraBars.Ribbon.ShowPageHeadersMode.Hide;
            this.ribbon.Size = new System.Drawing.Size(1270, 91);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden;
            this.ribbon.ShowCustomizationMenu += new DevExpress.XtraBars.Ribbon.RibbonCustomizationMenuEventHandler(this.ribbon_ShowCustomizationMenu);
            // 
            // mnuReferenceData
            // 
            this.mnuReferenceData.Caption = "Reference Data";
            this.mnuReferenceData.Id = 1;
            this.mnuReferenceData.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuReferenceData.ImageOptions.Image")));
            this.mnuReferenceData.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuReferenceData.ImageOptions.LargeImage")));
            this.mnuReferenceData.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuReferenceDataHeaderStandard)});
            this.mnuReferenceData.Name = "mnuReferenceData";
            this.mnuReferenceData.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // mnuReferenceDataHeaderStandard
            // 
            this.mnuReferenceDataHeaderStandard.Caption = "Standard";
            this.mnuReferenceDataHeaderStandard.Id = 61;
            this.mnuReferenceDataHeaderStandard.Name = "mnuReferenceDataHeaderStandard";
            // 
            // mnuExplore
            // 
            this.mnuExplore.Caption = "Explore";
            this.mnuExplore.Id = 7;
            this.mnuExplore.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuExplore.ImageOptions.Image")));
            this.mnuExplore.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuExplore.ImageOptions.LargeImage")));
            this.mnuExplore.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuOpenDatabase),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAnalyseAnyDataFile)});
            this.mnuExplore.Name = "mnuExplore";
            // 
            // btnAnalyseAnyDataFile
            // 
            this.btnAnalyseAnyDataFile.Caption = "Analyse any data file";
            this.btnAnalyseAnyDataFile.Id = 90;
            this.btnAnalyseAnyDataFile.Name = "btnAnalyseAnyDataFile";
            this.btnAnalyseAnyDataFile.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAnalyseAnyDataFile_ItemClick);
            // 
            // mnuOpenDatabase
            // 
            this.mnuOpenDatabase.Caption = "Open Database";
            this.mnuOpenDatabase.Id = 111;
            this.mnuOpenDatabase.Name = "mnuOpenDatabase";
            this.mnuOpenDatabase.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuOpenDatabase_ItemClick);
            // 
            // mnuUserSettings
            // 
            this.mnuUserSettings.Caption = "Settings";
            this.mnuUserSettings.Id = 21;
            this.mnuUserSettings.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuUserSettings.ImageOptions.Image")));
            this.mnuUserSettings.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuUserSettings.ImageOptions.LargeImage")));
            this.mnuUserSettings.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.mnuSkins),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnReleaseNoteHistory)});
            this.mnuUserSettings.Name = "mnuUserSettings";
            // 
            // mnuSkins
            // 
            this.mnuSkins.Caption = "Skins";
            this.mnuSkins.Id = 59;
            this.mnuSkins.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuSkins.ImageOptions.Image")));
            this.mnuSkins.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuSkins.ImageOptions.LargeImage")));
            this.mnuSkins.Name = "mnuSkins";
            // 
            // btnReleaseNoteHistory
            // 
            this.btnReleaseNoteHistory.Caption = "Release Notes history";
            this.btnReleaseNoteHistory.Id = 96;
            this.btnReleaseNoteHistory.Name = "btnReleaseNoteHistory";
            this.btnReleaseNoteHistory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReleaseNoteHistory_ItemClick);
            // 
            // mnuTest
            // 
            this.mnuTest.Caption = "Test";
            this.mnuTest.Id = 62;
            this.mnuTest.Name = "mnuTest";
            this.mnuTest.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuTest.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuTest_ItemClick);
            // 
            // mnuUsersProfilesRights
            // 
            this.mnuUsersProfilesRights.Caption = "Users && Profiles && Rights";
            this.mnuUsersProfilesRights.Id = 64;
            this.mnuUsersProfilesRights.Name = "mnuUsersProfilesRights";
            this.mnuUsersProfilesRights.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuUsersProfilesRights.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuUsersProfilesRights_ItemClick);
            // 
            // mnuLogs
            // 
            this.mnuLogs.Caption = "Logs";
            this.mnuLogs.Id = 65;
            this.mnuLogs.Name = "mnuLogs";
            this.mnuLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuLogs.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuLogs_ItemClick);
            // 
            // mnuDeploy
            // 
            this.mnuDeploy.Caption = "Deploy DatabaseExplorer";
            this.mnuDeploy.Id = 66;
            this.mnuDeploy.Name = "mnuDeploy";
            this.mnuDeploy.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuDeploy.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuDeploy_ItemClick);
            // 
            // barlblVersion
            // 
            this.barlblVersion.Caption = "Version : YY.MM.DD.HH";
            this.barlblVersion.Id = 69;
            this.barlblVersion.Name = "barlblVersion";
            this.barlblVersion.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblDomain
            // 
            this.barlblDomain.Caption = "Domain: ";
            this.barlblDomain.Id = 70;
            this.barlblDomain.Name = "barlblDomain";
            this.barlblDomain.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblEnv
            // 
            this.barlblEnv.Caption = "Env:";
            this.barlblEnv.Id = 71;
            this.barlblEnv.Name = "barlblEnv";
            this.barlblEnv.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblConnectedTo
            // 
            this.barlblConnectedTo.Caption = "Connected to:";
            this.barlblConnectedTo.Id = 72;
            this.barlblConnectedTo.Name = "barlblConnectedTo";
            this.barlblConnectedTo.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblHostIp
            // 
            this.barlblHostIp.Caption = "192.168.255.XX";
            this.barlblHostIp.Id = 73;
            this.barlblHostIp.Name = "barlblHostIp";
            this.barlblHostIp.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barlblDatabase
            // 
            this.barlblDatabase.Caption = "database name";
            this.barlblDatabase.Id = 74;
            this.barlblDatabase.Name = "barlblDatabase";
            this.barlblDatabase.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.AnyStatusLabelAvoutEnvironment_ItemDoubleClick);
            // 
            // barblbNewRelease
            // 
            this.barblbNewRelease.Caption = "*** New Version Available ! ***";
            this.barblbNewRelease.Id = 75;
            this.barblbNewRelease.Name = "barblbNewRelease";
            this.barblbNewRelease.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barblbNewRelease.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barblbNewRelease_ItemClick);
            // 
            // barTaskStatus
            // 
            this.barTaskStatus.Id = 76;
            this.barTaskStatus.Name = "barTaskStatus";
            // 
            // bbtnShowInstancesInMemory
            // 
            this.bbtnShowInstancesInMemory.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbtnShowInstancesInMemory.Caption = "Show object instances in memory";
            this.bbtnShowInstancesInMemory.Id = 77;
            this.bbtnShowInstancesInMemory.Name = "bbtnShowInstancesInMemory";
            this.bbtnShowInstancesInMemory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbtnShowInstancesInMemory_ItemClick);
            // 
            // bbtnShowTrace
            // 
            this.bbtnShowTrace.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bbtnShowTrace.Caption = "Show Database Trace";
            this.bbtnShowTrace.Id = 78;
            this.bbtnShowTrace.Name = "bbtnShowTrace";
            this.bbtnShowTrace.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbtnShowTrace_ItemClick);
            // 
            // barlblLogin
            // 
            this.barlblLogin.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barlblLogin.Caption = "Login";
            this.barlblLogin.Id = 79;
            this.barlblLogin.Name = "barlblLogin";
            // 
            // mnuEnvironments
            // 
            this.mnuEnvironments.Caption = "Environment Settings";
            this.mnuEnvironments.Id = 89;
            this.mnuEnvironments.Name = "mnuEnvironments";
            this.mnuEnvironments.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuEnvironments.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuEnvironments_ItemClick);
            // 
            // mnuRnD
            // 
            this.mnuRnD.Caption = "&R && D";
            this.mnuRnD.Id = 93;
            this.mnuRnD.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("mnuRnD.ImageOptions.Image")));
            this.mnuRnD.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("mnuRnD.ImageOptions.LargeImage")));
            this.mnuRnD.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnRnD_Test)});
            this.mnuRnD.Name = "mnuRnD";
            this.mnuRnD.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnRnD_Test
            // 
            this.btnRnD_Test.Caption = "Test";
            this.btnRnD_Test.Id = 95;
            this.btnRnD_Test.Name = "btnRnD_Test";
            // 
            // mnuAppDialoguing
            // 
            this.mnuAppDialoguing.Caption = "App Dialoguing";
            this.mnuAppDialoguing.Id = 94;
            this.mnuAppDialoguing.Name = "mnuAppDialoguing";
            this.mnuAppDialoguing.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.mnuAppDialoguing.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.mnuAppDialoguing_ItemClick);
            // 
            // ribbonPageCategoryMain
            // 
            this.ribbonPageCategoryMain.Name = "ribbonPageCategoryMain";
            this.ribbonPageCategoryMain.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageMain});
            this.ribbonPageCategoryMain.Text = "Main";
            // 
            // ribbonPageMain
            // 
            this.ribbonPageMain.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupMainScreens,
            this.ribbonPageGroupUserSettings,
            this.ribbonPageGroupDevelopperTools});
            this.ribbonPageMain.Name = "ribbonPageMain";
            this.ribbonPageMain.Text = "Main Screens";
            // 
            // ribbonPageGroupMainScreens
            // 
            this.ribbonPageGroupMainScreens.ItemLinks.Add(this.mnuReferenceData);
            this.ribbonPageGroupMainScreens.ItemLinks.Add(this.mnuExplore);
            this.ribbonPageGroupMainScreens.ItemLinks.Add(this.mnuRnD);
            this.ribbonPageGroupMainScreens.Name = "ribbonPageGroupMainScreens";
            this.ribbonPageGroupMainScreens.Text = "Main Screens";
            // 
            // ribbonPageGroupUserSettings
            // 
            this.ribbonPageGroupUserSettings.ItemLinks.Add(this.mnuUserSettings);
            this.ribbonPageGroupUserSettings.Name = "ribbonPageGroupUserSettings";
            this.ribbonPageGroupUserSettings.Tag = "AlignRight";
            this.ribbonPageGroupUserSettings.Text = "Your personnal settings";
            // 
            // ribbonPageGroupDevelopperTools
            // 
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuUsersProfilesRights);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuEnvironments);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuLogs);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuDeploy);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuAppDialoguing);
            this.ribbonPageGroupDevelopperTools.ItemLinks.Add(this.mnuTest);
            this.ribbonPageGroupDevelopperTools.Name = "ribbonPageGroupDevelopperTools";
            this.ribbonPageGroupDevelopperTools.Tag = "AlignRight";
            this.ribbonPageGroupDevelopperTools.Text = "Developpers tools";
            this.ribbonPageGroupDevelopperTools.Visible = false;
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barlblVersion);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblDomain);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblEnv);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblConnectedTo);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblHostIp);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblDatabase);
            this.ribbonStatusBar.ItemLinks.Add(this.barblbNewRelease);
            this.ribbonStatusBar.ItemLinks.Add(this.barTaskStatus);
            this.ribbonStatusBar.ItemLinks.Add(this.bbtnShowInstancesInMemory);
            this.ribbonStatusBar.ItemLinks.Add(this.bbtnShowTrace);
            this.ribbonStatusBar.ItemLinks.Add(this.barlblLogin);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 768);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1270, 31);
            // 
            // compositeControl
            // 
            this.compositeControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.compositeControl.Location = new System.Drawing.Point(0, 101);
            this.compositeControl.Name = "compositeControl";
            this.compositeControl.Size = new System.Drawing.Size(1270, 667);
            this.compositeControl.TabIndex = 2;
            // 
            // barManagerMain
            // 
            this.barManagerMain.DockControls.Add(this.barDockControlTop);
            this.barManagerMain.DockControls.Add(this.barDockControlBottom);
            this.barManagerMain.DockControls.Add(this.barDockControlLeft);
            this.barManagerMain.DockControls.Add(this.barDockControlRight);
            this.barManagerMain.Form = this;
            this.barManagerMain.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManagerMain;
            this.barDockControlTop.Size = new System.Drawing.Size(1270, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 799);
            this.barDockControlBottom.Manager = this.barManagerMain;
            this.barDockControlBottom.Size = new System.Drawing.Size(1270, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.barManagerMain;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 799);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1270, 0);
            this.barDockControlRight.Manager = this.barManagerMain;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 799);
            // 
            // colorBar
            // 
            this.colorBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.colorBar.EditValue = 0;
            this.colorBar.Enabled = false;
            this.colorBar.Location = new System.Drawing.Point(0, 91);
            this.colorBar.Name = "colorBar";
            this.colorBar.Properties.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(237)))), ((int)(((byte)(125)))));
            this.colorBar.Properties.EndColor = System.Drawing.Color.White;
            this.colorBar.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.UltraFlat;
            this.colorBar.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.colorBar.Properties.MarqueeAnimationSpeed = 1000;
            this.colorBar.Properties.MarqueeWidth = 1;
            this.colorBar.Properties.ProgressKind = DevExpress.XtraEditors.Controls.ProgressKind.Vertical;
            this.colorBar.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.colorBar.Properties.StartColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(237)))), ((int)(((byte)(125)))));
            this.colorBar.Properties.Stopped = true;
            this.colorBar.Size = new System.Drawing.Size(1270, 10);
            this.colorBar.TabIndex = 24;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1270, 799);
            this.Controls.Add(this.compositeControl);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.colorBar);
            this.Controls.Add(this.ribbon);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Database Explorer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManagerMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colorBar.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TechnicalTools.UI.DX.Controls.EnhancedRibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageMain;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupMainScreens;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupUserSettings;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarSubItem mnuReferenceData;
        private TechnicalTools.UI.DX.XtraUserControlComposite compositeControl;
        private DevExpress.XtraBars.BarManager barManagerMain;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarSubItem mnuExplore;
        private DevExpress.XtraBars.BarSubItem mnuUserSettings;
        private DevExpress.XtraBars.BarSubItem mnuSkins;
        private DevExpress.XtraBars.BarHeaderItem mnuReferenceDataHeaderStandard;
        private DevExpress.XtraBars.BarButtonItem mnuTest;
        private DevExpress.XtraBars.Ribbon.RibbonPageCategory ribbonPageCategoryMain;
        private DevExpress.XtraBars.BarButtonItem mnuUsersProfilesRights;
        private DevExpress.XtraBars.BarButtonItem mnuLogs;
        private DevExpress.XtraBars.BarButtonItem mnuDeploy;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupDevelopperTools;
        private DevExpress.XtraBars.BarStaticItem barlblVersion;
        private DevExpress.XtraBars.BarStaticItem barlblDomain;
        private DevExpress.XtraBars.BarStaticItem barlblEnv;
        private DevExpress.XtraBars.BarStaticItem barlblConnectedTo;
        private DevExpress.XtraBars.BarStaticItem barlblHostIp;
        private DevExpress.XtraBars.BarStaticItem barlblDatabase;
        private DevExpress.XtraBars.BarStaticItem barblbNewRelease;
        private DevExpress.XtraBars.BarStaticItem barTaskStatus;
        private DevExpress.XtraBars.BarStaticItem bbtnShowInstancesInMemory;
        private DevExpress.XtraBars.BarStaticItem bbtnShowTrace;
        private DevExpress.XtraBars.BarStaticItem barlblLogin;
        private DevExpress.XtraEditors.MarqueeProgressBarControl colorBar;
        private DevExpress.XtraBars.BarButtonItem mnuEnvironments;
        private DevExpress.XtraBars.BarButtonItem btnAnalyseAnyDataFile;
        private DevExpress.XtraBars.BarSubItem mnuRnD;
        private DevExpress.XtraBars.BarButtonItem mnuAppDialoguing;
        private DevExpress.XtraBars.BarButtonItem btnRnD_Test;
        private DevExpress.XtraBars.BarButtonItem btnReleaseNoteHistory;
        private DevExpress.XtraBars.BarButtonItem mnuOpenDatabase;
    }
}