@echo off

setlocal 
set nopause=0

:parsing_arg_loop
      :: Thanks to https://stackoverflow.com/a/34552964
      ::-------------------------- has argument ?
	  :: in %~1 - the ~ removes any wrapping " or '.
      if ["%~1"]==[""] (
        goto parsing_arg_end
      )
      ::-------------------------- argument exist ?
	  if ["%~1"]==["--nopause"] set nopause=1

      ::--------------------------
      shift
      goto parsing_arg_loop


:parsing_arg_end


echo.
echo  ** Loading git submodule (if any)
REM remove empty folder "ApplicationBase" so commandline "git submodule update ..." works 
REM (Why git create a empty folder on cloning ?! I thought git only handles files not folders...
REM  So how it is possible ?)
ROBOCOPY ApplicationBase ApplicationBase /S /MOVE >NUL
echo git submodule update --init --recursive
git submodule update --init --recursive

cd ApplicationBase
call bootstrap.bat --nopause
cd ..

echo ** Make all git repositories "standalone" **
"Git Submodule undo absorbgitdirs (Gui).exe" --noGui
echo ** Generating "No DX" version of solution/project files **
"Generate No DX solution files (Gui).exe" --noGui


REM Just symlink the folder "lib" to be able to compile without DX an to make script to generate valid "No DX" file
IF NOT EXIST "lib" mkdir "lib"
IF EXIST "lib\DevExpress" (
  echo ** Recreating symlink folder "lib\DevExpress"
  rmdir /S /Q "lib\DevExpress"
)
echo ** Create symlink "lib\DevExpress" so compilation works
echo mklink /j "lib\DevExpress" "ApplicationBase\lib\DevExpress"
     mklink /j "lib\DevExpress" "ApplicationBase\lib\DevExpress"



IF EXIST "ApplicationBase\packages" (
  echo ** Recreating symlink folder "ApplicationBase\packages"
  rmdir /S /Q "ApplicationBase\packages"
)
echo ** Create symlink needed by nuget in some submodules so solution can compile
echo mklink /j "ApplicationBase\packages" "packages"
     mklink /j "ApplicationBase\packages" "packages"

if ["%nopause%"] == ["0"] (
  echo DONE! Press any key to leave this script
  pause
)
endlocal
