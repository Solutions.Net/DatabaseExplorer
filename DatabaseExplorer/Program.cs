﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using TechnicalTools.Diagnostics;

using ApplicationBase.Business;
using System.Data.SqlClient;

namespace DatabaseExplorer
{
    public class Program : Business.Program
    {
        [STAThread]
        static int Main(string[] args_)
        {
            var program = new Program();
            return program.Run(args_);
        }
        protected override void Initialize()
        {
            base.Initialize();

            // Force instanciation of assembly so reflection can works later
            GC.KeepAlive(UI.UIControler.Instance);
        }
        protected override ApplicationBase.Common.BootstrapConfig CreateBootstrapConfig()
        {
            return new Common.BootstrapConfig(GetType());
        }
        protected override List<string> ReadTechnicalDataFromCommandLine(string[] args, out ApplicationBase.Common.BootstrapConfig bcfg, out ApplicationBase.Common.DebugConfig dcfg)
        {
            if (DebugTools.IsForDevelopper)
            {
                args = SetDefaultDomainAsArgument(args, ApplicationBase.Deployment.Data.eEnvironment.LocalNoDB);
                args = SetDefaultEnvNameAsArgument(args, Environment.UserName);
                args = SetDefaultCommandAsArgument(args, typeof(UI.ShowMainGui));
            }
            
            return base.ReadTechnicalDataFromCommandLine(args, out bcfg, out dcfg);
        }

        protected override ApplicationBase.Business.Automation.AutomationTaskBuilder CreateAutomationTaskBuilder(List<string> args, Config cfg)
        {
            return new AutomationTaskBuilderWithDefaultTaskForDevelopper(args, cfg);
        }
        protected class AutomationTaskBuilderWithDefaultTaskForDevelopper : ApplicationBase.Business.Automation.AutomationTaskBuilder
        {
            public AutomationTaskBuilderWithDefaultTaskForDevelopper(List<string> args, Config cfg)
                : base(args, cfg)
            {
            }
            protected override TechnicalTools.Automation.CommandLineTask CreateDefaultTask(DateTime date)
            {
                if (DebugTools.IsForDevelopper)
                    return new UI.ShowMainGui(Config.Instance, date);
                return null;
            }
        }
    }
}
