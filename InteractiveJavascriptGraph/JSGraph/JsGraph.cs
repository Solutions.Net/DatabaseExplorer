﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;
using System.Windows.Forms;

using CefSharp;
using CefSharp.WinForms;
using TechnicalTools.Diagnostics;

namespace InteractiveJavascriptGraph.JsGraph
{
    // article du 16 juin 2016 http://ourcodeworld.com/articles/read/173/how-to-use-cefsharp-chromium-embedded-framework-csharp-in-a-winforms-application
    // http://visjs.org/network_examples.html
    // http://visjs.org/examples/network/edgeStyles/arrows.html
    // http://visjs.org/examples/network/nodeStyles/shapes.html
    public partial class JsGraphControl : UserControl
    {
        List<INode> _nodes;
        List<IEdge> _edges;
        Dictionary<INode, int> _nodeIds;
        Dictionary<int, INode> _nodeByIds;

        CefSharp.WinForms.ChromiumWebBrowser _webBrowserFast;
        public CsEventManager EventManager { get; private set; }

        public JsGraphControl()
        {
            InitializeComponent();

            btnShowDebug.Visible = DebugTools.IsForDevelopper;
            btnExportImage.Visible = true;

            EventManager = new CsEventManager(this, "csEvents") { Customize = CustomizeEvent };
            Resize += OnResize;
        }

        private void CustomizeEvent(JsEventArgs e)
        {
            if (e is JsClickEventArgs)
            {
                var ee = e as JsClickEventArgs;
                ee.Nodes = ee.NodeIds.Select(id => _nodeByIds[int.Parse(id)]).ToList();
            }
        }

        void RefreshGui(Action action)
        {
            this.BeginInvoke(action);
        }

        public void DisplayGraph(IEnumerable<INode> nodes, IEnumerable<IEdge> edges)
        {
            _nodes = nodes.ToList();
            _edges = edges.ToList();
            string htmlDoc = ConstructDocument(_nodes, _edges);
            
            const string tmpFileName = "graph.html"; //Path.GetTempPath() + Guid.NewGuid() + ".html"; // pas de fichier temporaire pour le moment car les dependeance (html css, js) sont dans bin/debug
            File.WriteAllText(tmpFileName, htmlDoc);
            string pageLink = "file:///" + Path.GetFullPath(tmpFileName).Replace("#", "%23");
            //webBrowser.Navigate(pageLink);
            //_webBrowserFast.ExecuteScriptAsync("(function () { debugger; new CSharpAPI().MyMsgBox(\"Avant load !\"); })()");
            //_webBrowserFast.LoadHtml(htmlDoc, pageLink);
           // _webBrowserFast.RequestHandler = new RequestHandler();

            //_webBrowserFast.ExecuteScriptAsync("(function () { debugger; new CSharpAPI().MyMsgBox(\"apres load !\"); })()");
            _webBrowserFast.Load(pageLink);
            //_webBrowserFast.ExecuteScriptAsync("alert (typeof(csharpapi));");
            //_webBrowserFast.ExecuteScriptAsync(Environment.NewLine + "debugger;" + Environment.NewLine + " window.location = '" + pageLink.Replace("\\", "/") + "';" + Environment.NewLine );

            DoAsSoonAsBrowserIsInitialized(() => RefreshGui(() =>
            {
                btnShowDebug.Enabled = true;
                btnExportImage.Enabled = true;
                FitGraphToControlSize();
                // TODO : https://github.com/almende/vis/issues/215
            }));
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            label1.Visible = DesignMode;
            if (DesignMode)
                return;
            //var settings = new CefSettings
            //{
            //    //RemoteDebuggingPort = 8088 // run your app and point your browser to http://localhost:8088/
            //};
            //settings.RegisterScheme(new CefCustomScheme
            //{
            //    SchemeName = "MyCustomSchemeHandlerFactory",
            //    //SchemeHandlerFactory = new MyCustomSchemeHandlerFactory()
            //});
            //// autre flags http://peter.sh/experiments/chromium-command-line-switches/#winhttp-proxy-resolver
            //settings.CefCommandLineArgs.Add("no-proxy-server", "no-proxy-server"); // improve startup performance according to http://www.magpcss.org/ceforum/viewtopic.php?f=14&t=10760
            //Cef.Initialize(settings);

            // Si ca plante ici, c'est qu'il faut ajouter via nuget, le package "CefSharp.Winforms" dans la solution principale
            _webBrowserFast = new ChromiumWebBrowser("about:blank")
            {
                Dock = DockStyle.Fill
            };
            _webBrowserFast.BrowserSettings = new BrowserSettings()
            {
                FileAccessFromFileUrls = CefState.Enabled,
                UniversalAccessFromFileUrls = CefState.Enabled
            };
            _webBrowserFast.JavascriptObjectRepository.Settings.LegacyBindingEnabled = true;
            _webBrowserFast.IsBrowserInitializedChanged += WebBrowserFastOnIsBrowserInitializedChanged;

            CefSharpSettings.WcfEnabled = true;
            _webBrowserFast.JavascriptObjectRepository.Settings.LegacyBindingEnabled = true;
            _webBrowserFast.JavascriptObjectRepository.NameConverter = new CefSharp.JavascriptBinding.CamelCaseJavascriptNameConverter();
            _webBrowserFast.JavascriptObjectRepository.Register("csharpapi", new CSharpAPI(), isAsync: false, options: BindingOptions.DefaultBinder );
            _webBrowserFast.JavascriptObjectRepository.Register(EventManager.JsSingletonInstanceName, EventManager, isAsync: false, options: BindingOptions.DefaultBinder);
            
            Controls.Add(_webBrowserFast);
        }
        readonly List<Action> BrowserActions = new List<Action>();
        public void DoAsSoonAsBrowserIsInitialized(Action action)
        {
            if (_webBrowserFast.IsBrowserInitialized)
                action();
            else
                BrowserActions.Add(action);
        }
        void WebBrowserFastOnIsBrowserInitializedChanged(object sender, EventArgs e)
        {
            var browser = (ChromiumWebBrowser)sender;
            Debug.Assert(browser.IsBrowserInitialized);
            if (!browser.IsBrowserInitialized)
                return;
            var exceptions = new List<Exception>();
            foreach (var action in BrowserActions)
                try
                {
                    action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            if (exceptions.Count > 0)
                throw new AggregateException(exceptions);
        }
        
        void OnResize(object sender, EventArgs eventArgs)
        { 
            if (_webBrowserFast != null && _webBrowserFast.IsBrowserInitialized)
                FitGraphToControlSize();
        }
        void FitGraphToControlSize()
        {
            //Debug.Assert(_webBrowserFast.IsBrowserInitialized);
            //_webBrowserFast.ExecuteScriptAsync(
            //    "debugger;" + Environment.NewLine +
            //    "var canvas = document.getElementById('" + HtmlCanvasName + "');" + Environment.NewLine +
            //    "canvas.width = " + Width + ";" + Environment.NewLine +
            //    "canvas.height = " + Height + ";" + Environment.NewLine +
            //    "window.csharpapi.MyMsgBox(\"Redim done !\");" + Environment.NewLine);
        }

        public class CSharpAPI
        {
            public void MyMsgBox(string text)
            {
                MessageBox.Show(text);
            }
            public void ExportImage(string imgStream)
            {
                try
                {
                    const string header = "data:image/png;base64,";
                    Debug.Assert(imgStream.StartsWith(header));
                    byte[] data = Convert.FromBase64String(imgStream.Substring(header.Length));
                    var ms = new MemoryStream(data);
                    var bmp = new System.Drawing.Bitmap(ms);
                    bmp.Save("text.png", System.Drawing.Imaging.ImageFormat.Png);
                    //MessageBox.Show(text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Export failed !");
                }
            }
        }
        

        const string HtmlCanvasName = "canvas";
        string ConstructDocument(IEnumerable<INode> nodes, List<IEdge> edges)
        {
            string nl = Environment.NewLine;
            const string path = ".";
            string code =
                "<html><head>                                                                                                                                                                                                                                       " +
                nl + "  <title>Network | Basic usage</title>                                                                                                                                                                                                             " +
                nl + "                                                                                                                                                                                                                                                   " +
                //nl + "  <script async=\"\" src=\"//www.google-analytics.com/analytics.js\"></script>" +
                nl + "  <script type=\"text/javascript\" src=\"" + path + "/vis/vis.js\"></script>" +
                nl + "  <link href=\"" + path + "/vis/vis.css\" rel=\"stylesheet\" type=\"text/css\">                                                                                                                                                                     " +
                nl + "                                                                                                                                                                                                                                                   " +
                //nl + "  <style type=\"text/css\">                                                                                                                                                                                                                        " +
                //nl + "    #mynetwork {                                                                                                                                                                                                                                   " +
                //nl + "      width: 600px;                                                                                                                                                                                                                                " +
                //nl + "      height: 400px;                                                                                                                                                                                                                               " +
                //nl + "      border: 1px solid lightgray;                                                                                                                                                                                                                 " +
                //nl + "    }                                                                                                                                                                                                                                              " +
                //nl + "  </style>                                                                                                                                                                                                                                         " +
                nl + "</head>                                                                                                                                                                                                                                            " +
                nl + "<body>                                                                                                                                                                                                                                             " +
                nl + //"<p>                                                                                                                                                                                                                                                " +
                nl + //"  Create a simple network with some nodes and edges.                                                                                                                                                                                               " +
                nl + //"</p>                                                                                                                                                                                                                                               " +
                nl + "                                                                                                                                                                                                                                                   " +
                nl + "<div id=\"mynetwork\">                                                                                                                                                                                                                             " +
                nl + "   <div class=\"vis-network\" tabindex=\"900\" style=\"position: relative; overflow: hidden; touch-action: none; -webkit-user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); width: 100%; height: 100%;\">  " +
                nl + "      <canvas id=\"" + HtmlCanvasName + "\" style=\"position: relative; touch-action: none; -webkit-user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); width: 100%; height: 100%;\"></canvas>               " +
                nl + "   </div>                                                                                                                                                                                                                                          " +
                nl + "</div>                                                                                                                                                                                                                                             " +
                nl + "                                                                                                                                                                                                                                                   " +
                nl + "<script type=\"text/javascript\">                                                                                                                                                                                                                  " +
                nl + "   function escapeRegExp(str) { return str.replace(/([.*+?^=!:${}()|\\[\\]\\/\\\\])/g," + "\"\\\\$1\"); }" +
                nl + "   function replaceAll(str, find, replace) { return str.replace(new RegExp(escapeRegExp(find), 'g'), replace); }" +
                nl +
                nl + "  // create an array with nodes                                                                                                                                                                                                                    " +
                nl + "  var nodes = new vis.DataSet([                                                                                                                                                                                                                    ";
            _nodeIds = nodes.Select((node, index) => new { node, index })
                            .ToDictionary(kvp => kvp.node, kvp => kvp.index);
            _nodeByIds = _nodeIds.ToDictionary(kvp => kvp.Value, kvp => kvp.Key);

            foreach (var kvp in _nodeIds)
                code += nl + "    {"  +
                    string.Join("," , new [] {
                         "id: " + kvp.Value,
                         "label: \"" + kvp.Key.Label + "\"",
                         "shape:'" + (string.IsNullOrWhiteSpace(kvp.Key.Shape) ? "box" : kvp.Key.Shape) + "'",
                         (kvp.Key.Size.HasValue ? "size:" + kvp.Key.Size.Value : ""),
                         (kvp.Key.SpaceAround.HasValue ? "mass:" + kvp.Key.SpaceAround.Value : ""),
                         (string.IsNullOrWhiteSpace(kvp.Key.Color) ? "" : "color:'" + kvp.Key.Color + "'"),
                        (string.IsNullOrWhiteSpace(kvp.Key.ToolTip) ? "" : "title:'" + kvp.Key.ToolTip + "'"),
                        (kvp.Key.Position.HasValue ? "x:" + kvp.Key.Position.Value.X.ToString(CultureInfo.InvariantCulture) + ", y:" + kvp.Key.Position.Value.Y.ToString(CultureInfo.InvariantCulture) : "")
                     }.Where(str => !string.IsNullOrWhiteSpace(str)))
                     + "},";
            if (_nodeIds.Count > 0)
                code = code.Remove(code.Length - 1);
            code += nl + "  ]);                                                                                                                                                                                                                                              " +
                    nl + "                                                                                                                                                                                                                                                   " +
                    nl + "  // create an array with edges                                                                                                                                                                                                                    " +
                    nl + "  var edges = new vis.DataSet([                                                                                                                                                                                                                    ";
            foreach (var edge in edges)
                code += nl + "    { "
                        + "from: " + _nodeIds[edge.From] + ", " 
                        + "to: \"" + _nodeIds[edge.To] + "\", "
                        + "arrows:'to', "
                        + (edge.Width.HasValue ? "width:'" + edge.Width + "', " : "")
                        + (string.IsNullOrWhiteSpace(edge.ToolTip) ? "" : "title:'" + edge.ToolTip + "'")
                        + "},";
            if (edges.Any())
                code = code.Remove(code.Length - 1);

            code += nl + "  ]);                                                                                                                                                                                                                                              " +
                    nl + "                                                                                                                                                                                                                                                   " +
                    nl + "  // create a network                                                                                                                                                                                                                              " +
                    nl + "  var container = document.getElementById('mynetwork');                                                                                                                                                                                            " +
                    nl + "  var data = {                                                                                                                                                                                                                                     " +
                    nl + "    nodes: nodes,                                                                                                                                                                                                                                  " +
                    nl + "    edges: edges                                                                                                                                                                                                                                   " +
                    nl + "  };                                                                                                                                                                                                                                               " +
                    // Quand il y a beaucoup de noeud, il faut passer en improvedLayout false (car valeur par defaut : true)
                    //http://visjs.org/docs/network/#options
                    nl + "  var options = { layout : {  randomSeed:0, improvedLayout:true },  physics: { enabled:false}, stabilize: false };           // from http://visjs.org/docs/network/layout.html#                                                                                                                                " +
                    nl + "  var network = new vis.Network(container, data, options);                                                                                                                                                                                         " +
                    nl + EventManager.GetCodeForRegisteringJsEvents("network") +
                    nl + "</script>                                                                                                                                                                                                                                          " +
                    nl + "                                                                                                                                                                                                                                                   " +
                //nl + "<script src=\"../googleAnalytics.js\"></script>                                                                                                                                                                                                    " +
                    nl + "                                                                                                                                                                                                                                                   " +
                    nl + "                                                                                                                                                                                                                                                   " +
                    nl + "</body></html>                                                                                                                                                                                                                                     ";
            return string.Join(nl, code.Split(new [] { nl }, StringSplitOptions.None).Select(str => str.TrimEnd())); // Renvoie code en trimant les fin de lignes
        }
        public void TurnPhysics(bool on)
        {
            DoAsSoonAsBrowserIsInitialized(() =>
            {
                _webBrowserFast.ExecuteScriptAsync("network.setOptions ( { physics: {enabled:" + on.ToString().ToLower() + "} } );");
            });
            // pour arrêter la physique apres stabilisation
            //network.on("stabilizationIterationsDone", function () { network.setOptions({ physics: false });
        }


        private void btnShowDebug_Click(object sender, EventArgs e)
        {
            DoAsSoonAsBrowserIsInitialized(() =>
            {
                _webBrowserFast.ShowDevTools();
            });
        }



        public void TurnToHierarchical(bool p)
        {
            DoAsSoonAsBrowserIsInitialized(() =>
            {
                _webBrowserFast.ExecuteScriptAsync("network.setOptions ( { "
                                                   + "edges: { smooth: { type:'cubicBezier', forceDirection: 'vertical', roundness: 0.4 } },"
                                                   + "layout: { hierarchical: {  enabled: true, direction: 'UD' } } "
                                                   + "} );");
            });
        }

        private void btnExportImage_Click(object sender, EventArgs e)
        {
            DoAsSoonAsBrowserIsInitialized(() =>
            {
                float zoomFactor = 6;
                _webBrowserFast.ExecuteScriptAsync(Environment.NewLine +
                                                   "(function()" + Environment.NewLine +
                                                   "{" + Environment.NewLine +
                                                   "debugger;" + Environment.NewLine +
                                                   "var factor = " + zoomFactor.ToString(CultureInfo.InvariantCulture) + Environment.NewLine +
                                                   "var canvas = document.getElementsByTagName('canvas')[0];" + Environment.NewLine +
                                                   //"network.moveTo( { position: { x:0, y:0 } });" + Environment.NewLine +
                                                   "var scale = network.getScale();" + Environment.NewLine + // 1 = 100%, 0 = 0%
                                                   "var pos = network.getViewPosition();" + Environment.NewLine +
                                                   //"network.moveTo( { position: { x:pos.x, y:pos.y}, scale:scale  });" + Environment.NewLine +
                                                   "canvas.style.width = canvas.offsetWidth * factor;" + Environment.NewLine +
                                                   //"return;"  + Environment.NewLine +

                                                   "canvas.style.height = canvas.offsetHeight * factor;" + Environment.NewLine +


                                                   "network.redraw();" + Environment.NewLine +
                                                   "var img    = canvas.toDataURL('image/jpg');" + Environment.NewLine +
                                                   "window.csharpapi.ExportImage(img);" + Environment.NewLine +

                                                   "canvas.style.width = '100%';" + Environment.NewLine +
                                                   "canvas.style.height = '100%';" + Environment.NewLine +
                                                   //"network.moveTo( { position: pos, scale:scale });" + Environment.NewLine +
                                                   "})()" + Environment.NewLine
                                                   );
            });
        }
    }
}
