﻿using System;


namespace InteractiveJavascriptGraph.JsGraph
{
    public interface IEdge
    {
        INode From { get; }
        INode To { get; }
        string Label { get; }
        string ToolTip { get; }
        int? Width { get; }
    }
}
