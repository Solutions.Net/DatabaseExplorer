﻿using System;
using System.Collections.Generic;
using System.Drawing;


namespace InteractiveJavascriptGraph.JsGraph
{
    public class JsEventArgs : EventArgs
    {
        public string OriginalJSON { get; set; }
    }

    public class JsClickEventArgs : JsEventArgs
    {
        public string[] NodeIds { get; set; }
        public List<INode> Nodes { get; set; }
        string[] Edges { get; set; }

        public Pointer Pointer { get; set; }
    }
    public class JsContextEventArgs : JsEventArgs
    {
        public string[] NodeIds { get; set; }
        public INode[] Nodes { get; set; }
        string[] Edges { get; set; }

        public Pointer Pointer { get; set; }
    }

    public class Pointer
    {
        public Point  DOM      { get; set; }
        public PointD Canvas   { get; set; }
        public PointD Absolute { get; set; } // Custom / En plus par rapport à vis.js(
    }


    public struct PointD
    {
        public decimal X { get; set; }
        public decimal Y { get; set; }
    }
}
