﻿namespace InteractiveJavascriptGraph.JsGraph
{
    partial class JsGraphControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnShowDebug = new System.Windows.Forms.Button();
            this.btnExportImage = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 150);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cef Browser\r\n(no preview available at design time)";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnShowDebug
            // 
            this.btnShowDebug.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShowDebug.Enabled = false;
            this.btnShowDebug.Location = new System.Drawing.Point(95, 3);
            this.btnShowDebug.Name = "btnShowDebug";
            this.btnShowDebug.Size = new System.Drawing.Size(52, 23);
            this.btnShowDebug.TabIndex = 1;
            this.btnShowDebug.Text = "Debug";
            this.btnShowDebug.UseVisualStyleBackColor = true;
            this.btnShowDebug.Visible = false;
            this.btnShowDebug.Click += new System.EventHandler(this.btnShowDebug_Click);
            // 
            // btnExportImage
            // 
            this.btnExportImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportImage.Enabled = false;
            this.btnExportImage.Location = new System.Drawing.Point(3, 3);
            this.btnExportImage.Name = "btnExportImage";
            this.btnExportImage.Size = new System.Drawing.Size(83, 23);
            this.btnExportImage.TabIndex = 2;
            this.btnExportImage.Text = "Export image";
            this.btnExportImage.UseVisualStyleBackColor = true;
            this.btnExportImage.Visible = false;
            this.btnExportImage.Click += new System.EventHandler(this.btnExportImage_Click);
            // 
            // JsGraphControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnExportImage);
            this.Controls.Add(this.btnShowDebug);
            this.Controls.Add(this.label1);
            this.Name = "JsGraphControl";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnShowDebug;
        private System.Windows.Forms.Button btnExportImage;
    }
}
