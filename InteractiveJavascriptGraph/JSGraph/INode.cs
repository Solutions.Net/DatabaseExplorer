﻿using System;
using System.Drawing;

namespace InteractiveJavascriptGraph.JsGraph
{
    public interface INode
    { // http://visjs.org/docs/network/nodes.html
        string Label     { get; }
        string ToolTip   { get; }
        string Shape     { get; } // defaut : box
        int?   Size      { get; }
        string Color     { get; } // "#ffffff" or "red" or "rgb(120,32,14,1)"
        int? SpaceAround { get; } // utilise la propriet emass en fait
        PointF? Position { get; }
    }
    public static class INode_Extensions
    {
        public static readonly string DefaultColor = "#97C2FC";
    }

}
