﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Script.Serialization;
using System.Windows.Forms;

using TechnicalTools;


namespace InteractiveJavascriptGraph.JsGraph
{
    // Gère les events JS présentés ici : http://visjs.org/examples/network/events/interactionEvents.html
    public class CsEventManager
    {
        public string JsSingletonInstanceName { get; private set; }
        public bool DisplayEventsOnConsole { get; set; }

        public CsEventManager(Control ctl, string jsSingletonInstanceName)
        {
            _ctlOwner = ctl;
            JsSingletonInstanceName = jsSingletonInstanceName;
        }
        readonly Control              _ctlOwner;
        readonly JavaScriptSerializer _jss = new JavaScriptSerializer();

        T Deserialize<T>(string jsonEventArg)
            where T : JsEventArgs
        {
            var e = _jss.Deserialize<T>(jsonEventArg.Replace("\"nodes\": [", "\"nodeIds\": [")
                                                    .Replace("\"edges\": [", "\"edgeIds\": ["));
            e.OriginalJSON = jsonEventArg;
            if (Customize != null)
                Customize(e);
            return e;

            //dynamic e = jsonEventArg.ToExpando();
            //var d = e as IDictionary<string, object>;
            //d["nodeIds"] = e.nodes;
            //d["nodes"] = null;
            //d["OriginalJSON"] = jsonEventArg;
            //var jsSerializer = new JavaScriptSerializer();
            //var obj = jsSerializer.ConvertToType<JsClickEventArgs>(e);
        }
        internal Action<JsEventArgs> Customize;


        public void OnClick(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event Click: " + Environment.NewLine + jsonEventArg);

            if (Click != null)
            {
                var e = Deserialize<JsClickEventArgs>(jsonEventArg);
                _ctlOwner.BeginInvoke((Action)(() => Click(this, e)));
            }
        }
        public event EventHandler<JsClickEventArgs> Click;

        void Show(string msg)
        {
            //_ctlOwner.BeginInvoke((Action)(() => MessageBox.Show(msg)));
            if (Debugger.IsAttached)
                Debug.WriteLine(msg); // plus rapide que console
            else
                Console.WriteLine(msg);
        }

        public void OnDoubleClick(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event DoubleClick: " + Environment.NewLine + jsonEventArg);
            if (DoubleClick != null)
            {
                var e = Deserialize<JsClickEventArgs>(jsonEventArg);
                _ctlOwner.BeginInvoke((Action)(() => DoubleClick(this, e)));
            }
        }
        public event EventHandler<JsClickEventArgs> DoubleClick;

        public void OnContext(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event Context: " + Environment.NewLine + jsonEventArg);
            if (Context != null)
            {
                var e = Deserialize<JsContextEventArgs>(jsonEventArg);
                _ctlOwner.BeginInvoke((Action)(() => Context(this, e)));
            }
        }
        public event EventHandler<JsContextEventArgs> Context;
        public void OnDragStart(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event DragStart: " + Environment.NewLine + jsonEventArg);
        }
        public void OnDragging(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event Dragging: " + Environment.NewLine + jsonEventArg);
        }
        public void OnDragEnd(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event DragEnd: " + Environment.NewLine + jsonEventArg);
        }
        public void OnZoom(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event Zoom: " + Environment.NewLine + jsonEventArg);
        }
        public void OnShowToolTip(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event ShowPopup: " + Environment.NewLine + jsonEventArg);
        }
        public void OnHideToolTip()
        {
            if (DisplayEventsOnConsole)
                Show("Event OnHideToolTip: (no args) " + Environment.NewLine);
        }
        public void OnSelect(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event Select: " + Environment.NewLine + jsonEventArg);
        }

        public void OnSelectNode(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event SelectNode: " + Environment.NewLine + jsonEventArg);
        }
        public void OnDeselectNode(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event DeselectNode: " + Environment.NewLine + jsonEventArg);
        }
        public void OnHoverNode(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event HoverNode: " + Environment.NewLine + jsonEventArg);
        }
        public void OnBlurNode(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event BlurNode: " + Environment.NewLine + jsonEventArg);
        }

        public void OnSelectEdge(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                MessageBox.Show("Event SelectEdge: " + Environment.NewLine + jsonEventArg);
        }
        public void OnDeselectEdge(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event DeselectEdge: " + Environment.NewLine + jsonEventArg);
        }
        public void OnHoverEdge(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event HoverEdge: " + Environment.NewLine + jsonEventArg);
        }
        public void OnBlurEdge(string jsonEventArg)
        {
            if (DisplayEventsOnConsole)
                Show("Event BlurEdge: " + Environment.NewLine + jsonEventArg);
        }

        public string GetCodeForRegisteringJsEvents(string visNetworkJsVarName)
        {
            return @"
                var withAbsolutePosition = function(params)
                { // code inspired from from PrototypeJS via http://stackoverflow.com/a/1480137
                    var element = params.event.firstTarget || params.event.srcElement;
                    var top = 0, left = 0;
                    do
                    {
                        top += element.offsetTop || 0;
                        left += element.offsetLeft || 0;
                        element = element.offsetParent;
                    } while (element);

                    params.pointer['Absolute'] = ({ x: left + params.pointer.DOM.x, y: top + params.pointer.DOM.y });
                    return params;
                };
                " + visNetworkJsVarName + ".on('click', function (params) {" +
                //nl + "    params.event = '[original event]'; // hide data we don't care" +
                nl + "    " + JsSingletonInstanceName + ".OnClick(JSON.stringify(withAbsolutePosition(params), null, 4));" +
                nl + "});    " +
                nl + visNetworkJsVarName + ".on('doubleClick', function (params) {" +
                //nl + "    params.event = '[original event]'; // hide data we don't care" +
                nl + "    " + JsSingletonInstanceName + ".OnDoubleClick(JSON.stringify(withAbsolutePosition(params), null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('oncontext', function (params) {" +
                //nl + "    params.event = '[original event]'; // hide data we don't care" +
                nl + "    params.event.preventDefault()" + // prevent default context menu of browser to show
                nl + "    " + JsSingletonInstanceName + ".OnContext(JSON.stringify(withAbsolutePosition(params), null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('dragStart', function (params) {" +
                //nl + "    params.event = '[original event]'; // hide data we don't care" +
                nl + "    " + JsSingletonInstanceName + ".OnDragStart(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('dragging', function (params) {" +
                //nl + "    params.event = '[original event]'; // hide data we don't care" +
                nl + "    " + JsSingletonInstanceName + ".OnDragging(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('dragEnd', function (params) {" +
                //nl + "    params.event = '[original event]'; // hide data we don't care" +
                nl + "    " + JsSingletonInstanceName + ".OnDragEnd(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('zoom', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnZoom(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('showPopup', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnShowToolTip(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('hidePopup', function () {" +
                nl + "    " + JsSingletonInstanceName + ".OnHideToolTip();" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('select', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnSelect(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('selectNode', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnSelectNode(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('selectEdge', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnSelectEdge(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('deselectNode', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnDeselectNode(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('deselectEdge', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnDeselectEdge(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('hoverNode', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnHoverNode(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('hoverEdge', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnHoverEdge(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('blurNode', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnBlurNode(JSON.stringify(params, null, 4));" +
                nl + "});" +
                nl + visNetworkJsVarName + ".on('blurEdge', function (params) {" +
                nl + "    " + JsSingletonInstanceName + ".OnBlurEdge(JSON.stringify(params, null, 4));" +
                nl + "})";
        }

        string nl = Environment.NewLine;
    }
        
}
