﻿using System;
using System.Drawing;
using System.Windows.Forms;
using InteractiveJavascriptGraph.JsGraph;


namespace InteractiveJavascriptGraph
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }
        class Node : JsGraph.INode
        {
            public string Label   { get; set; }
            public string ToolTip { get; set; }

            public int?    Size        { get { return null; } }
            public string  Shape       { get { return null; } }
            public string  Color       { get { return null; } }
            public int?    SpaceAround { get { return null; } }
            public PointF? Position    { get { return null; } }
        }

        class Edge : JsGraph.IEdge
        {
            public JsGraph.INode From    { get; set; }
            public JsGraph.INode To      { get; set; }
            public string        Label   { get; set; }
            public string        ToolTip { get; set; }
            public int?          Width   { get { return null; } }
        }

        private void button1_Click(object sender, EventArgs _)
        {
            var t1     = new Node() {Label = "T1"};
            var t2     = new Node() {Label = "T2"};
            var t3     = new Node() {Label = "T3"};
            var center = new Node() {Label = "Center"};
            var outer  = new Node() {Label = "Outer"};
            var nodes = new[] { t1, t2 , t3 , center, outer};

            var edges = new[]
                {
                    new Edge() {From = t1, To = t2 }, new Edge() {From = t2, To = t3 }, new Edge() {From = t3, To = t1 },
                    new Edge() {From = center, To=t1 }, new Edge() {From = center, To=t2 }, new Edge() {From = center, To=t3 },
                    new Edge() {From = outer, To=t1 }, new Edge() {From = outer, To=t2 }, new Edge() {From = outer, To=t3 }, new Edge() {From = outer, To=center },
                };

            jsGraph.DisplayGraph(nodes, edges);
            jsGraph.EventManager.Click += EventManagerOnClick;
        }

        void EventManagerOnClick(object sender, JsClickEventArgs jsClickEventArgs)
        {
            //MessageBox.Show("Event Click: " + Environment.NewLine + jsClickEventArgs.OriginalJSON);
        }
    }
}
