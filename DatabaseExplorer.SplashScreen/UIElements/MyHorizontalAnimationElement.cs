﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Animation;

using DevExpress.Xpf.Editors;
using DevExpress.Xpf.Editors.Helpers;


namespace TrackerSplashScreen
{
    // From https://www.devexpress.com/Support/Center/Question/Details/Q413880
    public class MyHorizontalAnimationElement : Canvas
    {
        readonly Storyboard _storyboard = new Storyboard();
        ITargetChangedHelper<bool> IsVisibleChangedHelper { get; set; }


        public bool AllowAnimation
        {
            get { return (bool)GetValue(AllowAnimationProperty); }
            set { SetValue(AllowAnimationProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AllowAnimation.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AllowAnimationProperty =
            DependencyProperty.Register("AllowAnimation", typeof(bool), typeof(MyHorizontalAnimationElement),
            new PropertyMetadata(true, (d, e) => ((MyHorizontalAnimationElement)d).OnAllowAnimationChanged()));

        void OnAllowAnimationChanged()
        {
            if (AllowAnimation)
                _storyboard.Resume();
            else _storyboard.Pause();
        }


        public MyHorizontalAnimationElement()
        {
            Loaded += OnLoaded;
            SizeChanged += OnSizeChanged;
#if !SL
            IsVisibleChangedHelper = new EventToEventHelper<bool>();
            IsVisibleChanged += (d, e) => IsVisibleChangedHelper.RaiseTargetChanged((bool)e.NewValue);
#else
            IsVisibleChangedHelper = new BindingToEventHelper<bool>(this, VisibilityProperty, (value) => (Visibility)value == Visibility.Visible);
#endif
            IsVisibleChangedHelper.TargetChanged += (d, e) => OnIsVisibleChanged(e.Value);
            DevExpress.Xpf.Core.FrameworkElementHelper.SetIsClipped(this, true);
        }

        void OnIsVisibleChanged(bool isVisible)
        {
            if (isVisible)
                StartAnimation();
            else
                StopAnimation();
        }
        void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            StartAnimation();
        }
        void OnLoaded(object sender, RoutedEventArgs e)
        {
            var ed = (ProgressBarEdit)BaseEdit.GetOwnerEdit(this);
            SetBinding(AllowAnimationProperty, new Binding("IsEnabled") { Source = ed });
            StartAnimation();
        }
        void StopAnimation()
        {
            _storyboard.Stop();
            _storyboard.Children.Clear();
        }
        void StartAnimation()
        {
            if (!AllowAnimation) return;
            StopAnimation();
            if (Children.Count == 0)
                return;
            var content = (FrameworkElement)Children[0];

            double elementWidth = ActualWidth / 5;
            content.Width = elementWidth;
            content.Height = ActualHeight;
            double totalWidth = ActualWidth;
            Canvas.SetTop(content, 0);

            var animation = new DoubleAnimation
            {
                From = -elementWidth,
                To = totalWidth,
                RepeatBehavior = RepeatBehavior.Forever,
                Duration = new Duration(TimeSpan.FromSeconds(totalWidth / 100d))
            };
            Storyboard.SetTarget(animation, content);
            Storyboard.SetTargetProperty(animation, new PropertyPath(Canvas.LeftProperty));
            _storyboard.Children.Add(animation);
            _storyboard.Begin();
        }
    }

}
